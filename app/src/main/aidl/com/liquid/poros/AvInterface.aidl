// AvInterface.aidl
package com.liquid.poros;

interface AvInterface {

    //开启连麦
    void startAudioLink(String roomId,String imVideoToken,String sessionId,String avatarUrl,boolean isHost);

    //开启视频连麦
    void startAvLink();

    //当前连麦服务是否正在运行
    boolean isAvRunning();

    //关闭语音引擎
    void stopAudioLink();


    //关闭视频连麦
    void stopAvLink();

    //关闭或者开启麦克风
    void setMicrophoneMute(boolean mute);
}
