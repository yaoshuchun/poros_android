package com.liquid.porostwo.ability

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.liquid.base.utils.LogOut
import com.liquid.poros.utils.DeviceInfoUtils

/**
 * 进圈注册广播
 * 主要用来检测电量和广播
 */
class PorosReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        if (intent.action == Intent.ACTION_BATTERY_CHANGED) {
            val batteryLevel = intent.getIntExtra("level", 0) ///电池剩余电量
            val temperature = intent.getIntExtra("temperature", 0) / 10 ///获取电池温度
            DeviceInfoUtils.battery_rate = batteryLevel
            DeviceInfoUtils.temperature = temperature
            LogOut.debug("电池剩余电量:$batteryLevel 电池温度：$temperature")
        }
    }
}