package com.liquid.porostwo.ability

import com.blankj.utilcode.util.ActivityUtils
import com.liquid.poros.PorosApplication
import com.liquid.poros.analytics.utils.MultiTracker
import com.liquid.poros.entity.PlayloadInfo
import com.liquid.poros.utils.AccountUtil
import com.liquid.poros.utils.push.PushNotifyUtils.Companion.instance
import com.liquid.poros.widgets.Poast
import com.liquid.porostwo.ApplicationViewModel
import java.util.*

/**
 * 全局消息事件管理
 */
object AppEventManager {

    val applicationViewModel: ApplicationViewModel by lazy {
        PorosApplication.instance.getViewModel(ApplicationViewModel::class.java)
    }

    var currentAccountId:String ?=null

    /**
     * 初始化全局消息事件
     */
    fun init() {
        applicationViewModel.messageNotification.observeForever {
            showMessageNotification(it)
        }
    }

    /**
     * 当前的对方账号
     */
    fun currentAccount(accountId:String){

    }
    /**
     * 展示
     */
    fun showMessageNotification(playLoadInfo: PlayloadInfo){
        if (playLoadInfo.account != currentAccountId) {
            if (playLoadInfo.isTeam_notice) {
                val map = HashMap<String?, String?>()
                map["user_id"] = AccountUtil.getInstance().userId
                map["female_guest_id"] = playLoadInfo.account
                MultiTracker.onEvent("b_invite_mic_msg", map)
                Poast.makeText(ActivityUtils.getTopActivity(), playLoadInfo, Poast.LENGTH_LONG).show()
            } else {
                Poast.makeText(ActivityUtils.getTopActivity(), playLoadInfo, Poast.LENGTH_SHORT).show()
            }
        }
        instance.sentGeTuiNotify(playLoadInfo)
    }
}