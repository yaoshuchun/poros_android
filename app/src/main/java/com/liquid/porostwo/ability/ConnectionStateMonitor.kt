package com.liquid.porostwo.ability

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import com.blankj.utilcode.util.NetworkUtils
import com.liquid.poros.PorosApplication
import com.liquid.poros.analytics.utils.MultiTracker
import com.liquid.poros.utils.AccountUtil
import com.liquid.porostwo.ApplicationViewModel
import java.util.*

/**
 * 监听网络状态变化
 */
class ConnectionStateMonitor : ConnectivityManager.NetworkCallback() {

    var networkRequest: NetworkRequest? = null

    init {
        networkRequest = NetworkRequest.Builder()
            .addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
            .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
            .build()
    }

    fun enable(context: Context) {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        connectivityManager.registerNetworkCallback(networkRequest, this)
    }

    override fun onAvailable(network: Network?) {
        //**判断当前的网络连接状态是否可用*/
        val isConnected = NetworkUtils.isConnected()
        PorosApplication.instance.getViewModel(ApplicationViewModel::class.java).netWorkIsConnected.postValue(isConnected)
        PorosApplication.instance.notifyNetworkChanged(isConnected)
        val map = HashMap<String?, String?>()
        map["is_connect"] = if (isConnected) "true" else "false"
        map["tag"] = "connect_mic_ques"
        map["user_id"] = AccountUtil.getInstance().userId
        MultiTracker.onEvent("b_net_work_state", map)
    }
}