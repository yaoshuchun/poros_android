package com.liquid.porostwo.business.router

import android.content.Context
import com.alibaba.android.arouter.facade.Postcard
import com.alibaba.android.arouter.facade.annotation.Interceptor
import com.alibaba.android.arouter.facade.callback.InterceptorCallback
import com.alibaba.android.arouter.facade.template.IInterceptor
import com.liquid.base.utils.LogOut

/**
 * ARouter 拦截器
 */
@Interceptor(priority = 99)
class RouterIInterceptor : IInterceptor {
    override fun init(context: Context?) {
        LogOut.debug("RouterIInterceptor", "ARouter拦截器初始化")
    }

    override fun process(postcard: Postcard, callback: InterceptorCallback) {
        LogOut.debug("RouterIInterceptor", "ARouter拦截器执行拦截，URL= ${postcard.path}")
        callback.onContinue(postcard)
    }
}