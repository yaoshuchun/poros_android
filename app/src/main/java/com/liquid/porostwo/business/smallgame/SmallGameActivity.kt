package com.liquid.porostwo.business.smallgame

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.alibaba.android.arouter.facade.annotation.Autowired
import com.alibaba.android.arouter.facade.annotation.Route
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.request.RequestOptions
import com.liquid.im.kit.permission.MPermission
import com.liquid.im.kit.permission.annotation.OnMPermissionDenied
import com.liquid.im.kit.permission.annotation.OnMPermissionGranted
import com.liquid.im.kit.permission.annotation.OnMPermissionNeverAskAgain
import com.liquid.poros.R
import com.liquid.poros.constant.Constants
import com.liquid.poros.constant.TrackConstants
import com.liquid.poros.databinding.ActivitySmallGameBinding
import com.liquid.poros.entity.BaseBean
import com.liquid.poros.entity.SmallGameTeam
import com.liquid.poros.helper.AvHelper
import com.liquid.poros.helper.SimpleLiveRoomListener
import com.liquid.poros.ui.activity.WebViewActivity
import com.liquid.poros.ui.fragment.dialog.common.CommonActionListener
import com.liquid.poros.ui.fragment.dialog.common.CommonDialogBuilder
import com.liquid.poros.ui.fragment.dialog.common.CommonDialogFragment
import com.liquid.poros.utils.AccountUtil
import com.liquid.poros.utils.ToastUtil
import com.liquid.porostwo.base.BaseActivity
import com.liquid.porostwo.base.glide.GlideApp
import com.liquid.porostwo.business.constant.RouterUri
import com.liquid.porostwo.business.smallgame.viewmodel.SmallGameViewModel
import com.netease.lava.nertc.sdk.NERtc
import kotlinx.android.synthetic.main.activity_room_live_new.*
import kotlinx.android.synthetic.main.activity_small_game.*
import kotlinx.android.synthetic.main.activity_video_match_chatting.*
/**
 * 小游戏页面
 *
 */
@Route(path = RouterUri.SMALL_GAME)
class SmallGameActivity : BaseActivity<ActivitySmallGameBinding>() {
    companion object {
        const val PERMISSION_REQUEST_CODE = 0x100
    }
    //状态
    private val STATE_UNJOIN = 0 //未加入队伍 ，进到此页面后，此状态很短暂
    private val STATE_JOINED = 1 //已加入队伍  , 已加入  ==？  未准备
    private val STATE_READYED = 2//已准备
    private val STATE_PLAYING = 3//游戏中

    private var stateSelf = STATE_JOINED//自己进到此页面即是加入状态
    private var stateOther = STATE_UNJOIN//对方的状态

    private var otherIsRefused = false//对方是否拒绝
    private var canInvite = false
    private var mAvHelper:AvHelper?= null
    private val smallGameViewModel by lazy { getActivityViewModel(SmallGameViewModel::class.java) }
    @Autowired
    @JvmField
    var smallGameTeam:SmallGameTeam?=null

    private val roomListener  = object :SimpleLiveRoomListener(){
        override fun onJoinRoomFailed(resultCode: Int) {
            super.onJoinRoomFailed(resultCode)
            ToastUtil.show("开启语音通话出错，请稍后重试")
            smallGameViewModel.cancel()
        }

        override fun onJoinRoomSuccess(channelId: Long) {
            super.onJoinRoomSuccess(channelId)
            //调用接口，传ChannelId
            // smallGameViewModel?.setChnnelId(channelId.toString())
        }
    }
    override fun getPageId(): String {
        return TrackConstants.PAGE_SMALL_GAME
    }

    override fun needTransparent(): Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //TODO 测试 start ===
        mBinding.otherjoin.setOnClickListener {
            stateOther = STATE_JOINED
            showOtherJoinedView()
            showCanReadyView()
        }
        mBinding.otherready.setOnClickListener {
            stateOther = STATE_READYED
            mBinding.ivReadyedOther.visibility = View.VISIBLE
        }
        mBinding.selfready.setOnClickListener {
            stateSelf = STATE_READYED
            showSelfReadyedView()
            smallGameViewModel.stopTimer()
        }        
        mBinding.otherrefuse.setOnClickListener {
            otherIsRefused = true
            showOtherRefusedView()
        }
        //TODO 测试 end ===
        initListener()
        checkPermission()

    }

    private fun onPermissionChecked() {
        //如果是游戏接受者，则对方已加入

        init(smallGameTeam)
    }

    private fun initListener() {
        mBinding.ivBack.setOnClickListener { back() }
        mBinding.tvReday.setOnClickListener {
            //准备
            if (stateSelf == STATE_JOINED && stateOther != STATE_UNJOIN){
                //TODO smallGameViewModel.ready()
                stateSelf = STATE_READYED
                showSelfReadyedView()
                smallGameViewModel.stopTimer()
            } else if (canInvite){//邀请
                //TODO smallGameViewModel.invite()
                canInvite = false
                smallGameTeam?.let { it1 -> smallGameViewModel.startTimer(it1.watting_time) }
            }
        }
    }

    /**
     * 创建队伍成功
     */
    private fun init(smallGameTeam: SmallGameTeam?){
        updateView(smallGameTeam)
        initVoice(smallGameTeam)
        initObserver()
    }

    private fun initVoice(smallGameTeam: SmallGameTeam?){
        smallGameTeam?.let { team ->
            //注册消息接收器
            smallGameViewModel.registerObservers(true)
            //进入im聊天室 接收消息（对方已加入，对方已准备等）
            smallGameViewModel.enterChatRoom(team.im_room_id)

            //开始语音通话
            AccountUtil.getNERtcToken { rtcToken ->
                team.im_room_id?.let {
                    mAvHelper = AvHelper(it, rtcToken, false, roomListener)
                    mAvHelper!!.joinAvRoom(this, null, false)
                }
            }
        }
    }

    private fun updateView(smallGameTeam: SmallGameTeam?){
        smallGameTeam?.let {
            //如果是发起者
            if (it.role == SmallGameTeam.ROLE_LAUNCHER) {
                //等待对方加入倒计时View
                smallGameViewModel.startTimer(smallGameTeam.watting_time)
            } else {//如果是接收者
                showCanReadyView()
                stateOther = STATE_JOINED

            }
            //更新界面
            val options = RequestOptions()
                    .placeholder(R.mipmap.img_default_avatar)
                    .apply(RequestOptions.bitmapTransform(CircleCrop()))
                    .error(R.mipmap.img_default_avatar)
            GlideApp.with(this@SmallGameActivity)
                    .load(it.other_avatar_url)
                    .apply(options)
                    .into(mBinding.ivAvatarOther)
            mBinding.tvNickOther.text = it.other_nick_name
            GlideApp.with(this@SmallGameActivity)
                    .load(it.self_avatar_url)
                    .apply(options)
                    .into(mBinding.ivAvatarOther)
            mBinding.tvNickSelf.text = it.self_nick_name
            mBinding.tvGameDesc.text = it.game_desc
            GlideApp.with(this@SmallGameActivity)
                    .load(it.game_icon_url)
                    .into(mBinding.civIconGame)
            //查看奖池
            mBinding.tvSeePool.setOnClickListener {
                val bundle = Bundle()
                bundle.putString(Constants.TARGET_PATH, smallGameTeam.game_prize_pool_url)
                val intent = Intent(this@SmallGameActivity, WebViewActivity::class.java)
                intent.putExtras(bundle)
                startActivity(intent)
            }
        }

    }


    /**
     *
     */
    private fun exit(){
        //不再发送本地视频给远端（视频房间里的其他用户）

        finish()
    }



    private fun initObserver() {
        //对方已加入
        smallGameViewModel.otherJoinedMessage.observe(this, {
            stateOther = STATE_JOINED
            showOtherJoinedView()
            showCanReadyView()
        })
        //对方已准备
        smallGameViewModel.otherReadyedMessage.observe(this, {
            stateOther = STATE_READYED
            mBinding.ivReadyedOther.visibility = View.VISIBLE
        })

        //对方已拒绝
        smallGameViewModel.otherRefusedMessage.observe(this, {
            otherIsRefused = true
            showOtherRefusedView()
        })
        //对方已取消
        smallGameViewModel.otherCancelMessage.observe(this, {
            smallGameViewModel.cancel()
        })
        //自己已准备
        smallGameViewModel.selfReadyedData.observe(this, {
            stateSelf = STATE_READYED
            showSelfReadyedView()
            smallGameViewModel.stopTimer()
        })

        //倒计时
        smallGameViewModel.timerData.observe(this, { remainTime ->
            if (stateSelf == STATE_JOINED && stateOther == STATE_UNJOIN && !otherIsRefused) {
                mBinding.tvReday.text = "等待好友加入【${remainTime}s】"
                if (remainTime == 0L) {
                    canInvite = true
                    mBinding.tvReday.text = "邀请队友加入"
                }

            } else if (stateSelf == STATE_JOINED && stateOther != STATE_UNJOIN) {
                mBinding.tvRedayDesc.text = "请在【$remainTime】秒内完成准备，否则将退出游戏"
                //退出游戏
                if (remainTime == 0L) {
                    smallGameViewModel.cancel()
                }

            } else if (otherIsRefused) {
                mBinding.tvReday.text = "【$remainTime】秒后可重新邀请"
                //可以再次邀请
                if (remainTime == 0L) {
                    otherIsRefused = false
                    canInvite = true
                    mBinding.tvReday.text = "邀请队友加入"
                }
            }
        })

        //自己取消
        smallGameViewModel.selfCancelData.observe(this, {
            when (it.code) {
                BaseBean.SUCCESS -> {
                    finish()
                }
                else -> {
                    ToastUtil.show(it.msg)
                }
            }
        })

        //双方都已准备好
        smallGameViewModel.allReadyedMessage.observe(this, {
            //请求开始游戏接口，进入游戏中状态
            smallGameViewModel.startGame()
        })

        //开始游戏接口
        smallGameViewModel.startGameData.observe(this, {
            when (it.code) {
                BaseBean.SUCCESS -> {
                    stateSelf = STATE_PLAYING
                    stateOther = STATE_PLAYING
                }
                else -> {
                    ToastUtil.show(it.msg)
                }
            }
        })
        //邀请
        smallGameViewModel.inviteGameData.observe(this, {
            when (it.code) {
                BaseBean.SUCCESS -> {
                    canInvite = false
                    //开始等待队友加入计时
                    smallGameTeam?.let { team -> smallGameViewModel.startTimer(team.watting_time) }
                }
                else -> {
                    ToastUtil.show(it.msg)
                }
            }
        })
    }

    override fun onBackPressed() {
        back()
    }

    private fun back(){
        CommonDialogBuilder()
                .setWithCancel(true)
                .setTitle("提示")
                .setDesc("确认退出吗")
                .setCancel("取消")
                .setConfirm("确认")
                .setListener(object : CommonActionListener {
                    override fun onConfirm() {
                        //TODO smallGameViewModel.cancel()
                        finish()
                    }

                    override fun onCancel() {}
                }).create().show(supportFragmentManager, CommonDialogFragment::class.java.simpleName)
    }



    override fun onDestroy() {
        super.onDestroy()
        mAvHelper?.releaseAvRoom()
        smallGameViewModel.registerObservers(false)
        smallGameViewModel.stopTimer()
    }

    /**
     * 显示可以准备View
     */
    private fun showCanReadyView(){
        mBinding.tvStartDesc.visibility = View.VISIBLE
        mBinding.tvRedayDesc.visibility = View.VISIBLE
        mBinding.tvReday.setBackgroundResource(R.drawable.bg_00aaff_12)
        mBinding.tvReday.text = "准备"
        //开始准备计时
        smallGameTeam?.let { smallGameViewModel.startTimer(it.ready_time) }
    }

    /**
     * 显示自己已准备View
     */
    private fun showSelfReadyedView(){
        mBinding.tvRedayDesc.visibility = View.INVISIBLE
        mBinding.ivReadyedSelf.visibility = View.VISIBLE
        mBinding.tvReday.text = "已准备"
        mBinding.tvReday.setBackgroundResource(R.drawable.bg_22ffffff_12)
    }

    /**
     * 显示对方已加入聊天View
     */
    private fun showOtherJoinedView(){
        mBinding.rlOther.setBackgroundResource(R.drawable.bg_5b51cc_r90)
        mBinding.tvStateOther.text ="语音中"
    }

    /**
     * 显示对方已拒绝View
     */
    private fun showOtherRefusedView(){
        mBinding.rlOther.setBackgroundResource(R.drawable.bg_5b51cc_r90)
        mBinding.tvStateOther.text ="对方已拒绝"
        //拒绝后180后 可以重新邀请
        smallGameTeam?.let { smallGameViewModel.startTimer(it.refuse_time) }
    }

    /**
     * 游戏结束后退出到可准备状态
     */
    private fun backToCanReadyView(){
        //TODO 隐藏游戏中View
        mBinding.groupWaitAndReady.visibility = View.VISIBLE

        mBinding.ivReadyedOther.visibility = View.INVISIBLE
        mBinding.ivReadyedSelf.visibility = View.INVISIBLE

        showCanReadyView()
    }

    private fun checkPermission() {
        val lackPermissions = NERtc.checkPermission(this@SmallGameActivity)
        if (lackPermissions.isEmpty()) {
            onBasicPermissionSuccess()
        } else {
            val permissions = arrayOfNulls<String>(lackPermissions.size)
            for (i in lackPermissions.indices) {
                permissions[i] = lackPermissions[i]
            }
            MPermission.with(this@SmallGameActivity)
                    .setRequestCode(PERMISSION_REQUEST_CODE)
                    .permissions(*permissions)
                    .request()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        MPermission.onRequestPermissionsResult(this, requestCode, permissions, grantResults)
    }

    @OnMPermissionGranted(PERMISSION_REQUEST_CODE)
    fun onBasicPermissionSuccess() {
        onPermissionChecked()
    }

    @OnMPermissionDenied(PERMISSION_REQUEST_CODE)
    @OnMPermissionNeverAskAgain(PERMISSION_REQUEST_CODE)
    fun onBasicPermissionFailed() {
        ToastUtil.show("音视频通话所需权限未全部授权，部分功能可能无法正常运行！")
        exit()
    }
}