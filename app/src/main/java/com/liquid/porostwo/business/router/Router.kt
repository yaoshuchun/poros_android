package com.liquid.porostwo.business.router

import com.alibaba.android.arouter.launcher.ARouter
import com.liquid.poros.entity.SmallGameTeam
import com.liquid.porostwo.business.constant.RouterUri

/**
 * 所有Activity的跳转
 */
object Router {

    /**
     * 跳转到cp等级
     */
    fun toCPLevelActivity(sessionId: String,avaUrl: String,name: String ) {
        ARouter.getInstance().build(RouterUri.CP_LEVEL)
            .withString("sessionId", sessionId)
            .withString("avaUrl", avaUrl)
            .withString("name", name)
            .navigation()
    }

    /**
     * 跳转到设置页面
     */
    fun toSettingActivity() {
        ARouter.getInstance().build(RouterUri.SETTING).navigation()
    }

    /**
     * 跳转到答题小游戏界面
     */
    fun toSmallGameActivity(smallGameTeam: SmallGameTeam) {
        ARouter.getInstance().build(RouterUri.SMALL_GAME)
                .withSerializable("smallGameTeam",smallGameTeam)
                .navigation()
    }

}