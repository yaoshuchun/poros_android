package com.liquid.porostwo.business.cp

import android.content.Intent
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Rect
import android.os.Bundle
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.TextUtils
import android.text.style.ForegroundColorSpan
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.content.res.AppCompatResources
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.GridLayoutManager.SpanSizeLookup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ItemDecoration
import com.alibaba.android.arouter.facade.annotation.Autowired
import com.alibaba.android.arouter.facade.annotation.Route
import com.blankj.utilcode.util.ToastUtils
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.request.RequestOptions
import com.liquid.base.adapter.CommonAdapter
import com.liquid.base.adapter.CommonViewHolder
import com.liquid.poros.R
import com.liquid.poros.analytics.utils.MultiTracker
import com.liquid.poros.constant.TrackConstants
import com.liquid.poros.databinding.ActivityCpLevelBinding
import com.liquid.poros.entity.CPLevelInfo
import com.liquid.poros.entity.CPLevelInfo.Award
import com.liquid.porostwo.base.BaseActivity
import com.liquid.poros.helper.JinquanResourceHelper
import com.liquid.poros.ui.activity.DeclarationDetailActivity
import com.liquid.porostwo.business.cp.viewmodel.CPLevelViewModel
import com.liquid.poros.utils.AccountUtil
import com.liquid.poros.utils.ViewUtils
import com.liquid.poros.widgets.ObservableScrollView
import com.liquid.poros.widgets.ObservableScrollView.ScrollViewListener
import com.liquid.porostwo.base.glide.GlideApp
import com.liquid.porostwo.business.constant.RouterUri
import java.math.BigDecimal
import java.util.*

@Route(path = RouterUri.CP_LEVEL)
class CPLevelActivity : BaseActivity<ActivityCpLevelBinding>(), ScrollViewListener {
    @Autowired
    @JvmField
    var sessionId: String = ""

    @Autowired
    @JvmField
    var avaUrl: String = ""

    @Autowired
    @JvmField
    var name: String = ""

    private val cpLevelViewModel by lazy { getActivityViewModel(CPLevelViewModel::class.java) }

    override fun getPageId(): String {
        return TrackConstants.PAGE_CP_LEVEL
    }

    override fun needTransparent(): Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initObserve()
        initListener()
        cpLevelViewModel.getLevelInfo(sessionId)

        JinquanResourceHelper.getInstance(this@CPLevelActivity).setBackgroundResourceByAttr(
            mBinding.levelDetailTitle, R.attr.custom_attr_level_detail_tv_bg
        )
        JinquanResourceHelper.getInstance(this@CPLevelActivity).setBackgroundResourceByAttr(
            mBinding.levelRv, R.attr.custom_attr_level_detail_tv_bg
        )
    }

    private fun initListener() {
        mBinding.ivBack.setOnClickListener { finish() }
        mBinding.osvScrollView.setScrollViewListener(this)
    }

    private fun initObserve() {
        cpLevelViewModel.levelData.observe(this) {
            if (it.code == 0){
                onLevelInfo(it)
            }else{
                ToastUtils.showShort(it.msg)
            }
        }
    }

    private fun onLevelInfo(info: CPLevelInfo) {
        val options = RequestOptions()
            .placeholder(R.mipmap.img_default_avatar)
            .apply(RequestOptions.bitmapTransform(CircleCrop()))
            .error(R.mipmap.img_default_avatar)
        GlideApp.with(this@CPLevelActivity)
            .load(info.male_avatar_url)
            .apply(options)
            .into(mBinding.otherHeadIv)
        GlideApp.with(this@CPLevelActivity)
            .load(info.female_avatar_url)
            .apply(options)
            .into(mBinding.headIv)
        GlideApp.with(this@CPLevelActivity)
            .load(info.rank_info.rank_icon)
            .into(mBinding.currentLevelIv)
        GlideApp.with(this@CPLevelActivity)
            .load(info.rank_info.next_rank_icon)
            .into(mBinding.nextLevelIv)
        GlideApp.with(this@CPLevelActivity)
            .load(info.rank_info.rank_icon)
            .into(mBinding.levelIv)
        mBinding.levelTv.text = info.rank_info.name
        mBinding.currentLevelTv.text = info.rank_info.name
        mBinding.nextLevelTv.text = info.rank_info.next_rank_name
        mBinding.levelNeedTip.setTextColor(Color.parseColor(if (themeTag == 1) "#A5A9B2" else "#4C4C5F"))
        if (info.rank_info != null) {
            mBinding.nextLevelTv.visibility = View.VISIBLE
            mBinding.nextLevelIv.visibility = View.VISIBLE
            val bigDecimal = BigDecimal(info.rank_info.point_award)
            if (bigDecimal.toInt() > 0) {
                mBinding.levelNeedTip.text = SpannableStringBuilder("还差").apply {
                    append(SpannableString("【" + info.rank_info.need_score + "】").apply {
                        setSpan(
                            ForegroundColorSpan(Color.parseColor("#FF598C")),
                            0,
                            length,
                            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                        )
                    })
                    append("亲密值升级")
                }
                try {
                    mBinding.progressBar.max = bigDecimal.toInt()
                    mBinding.progressBar.progress =
                        BigDecimal(info.rank_info.point_award).toInt() - BigDecimal(info.rank_info.need_score).toInt()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            } else {
                mBinding.nextLevelTv.visibility = View.GONE
                mBinding.nextLevelIv.visibility = View.GONE

                mBinding.levelNeedTip.text =
                    SpannableStringBuilder("亲密值").append(SpannableString("【" + info.rank_info.cp_score + "】").apply {
                        setSpan(
                            ForegroundColorSpan(Color.parseColor("#FF598C")),
                            0,
                            length,
                            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                        )
                    })
                try {
                    mBinding.progressBar.max = 100
                    mBinding.progressBar.progress = 100
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
            if (!TextUtils.isEmpty(info.rank_info.next_gift_box_cnt)) {
                mBinding.tvNextGiftBoxCount.visibility = View.VISIBLE
                mBinding.tvNextGiftBoxCount.text = info.rank_info.next_gift_box_cnt
            } else {
                mBinding.tvNextGiftBoxCount.visibility = View.GONE
            }
        }
        inflateLevelDetail(info)
    }

    private fun inflateLevelDetail(info: CPLevelInfo?) {
        val award_list = info!!.award_list
        val gridLayoutManager = GridLayoutManager(this@CPLevelActivity, 6)
        mBinding.levelRv.layoutManager = gridLayoutManager
        if (award_list.size > 2) {
            award_list[award_list.size - 1].type = 2
            award_list[award_list.size - 2].type = 2
        }
        val mAdapter: CommonAdapter<Award> =
            object : CommonAdapter<Award>(this@CPLevelActivity, R.layout.item_level) {
                override fun convert(holder: CommonViewHolder, award: Award, pos: Int) {
                    JinquanResourceHelper.getInstance(this@CPLevelActivity)
                        .setBackgroundResourceByAttr(
                            holder.getView(
                                R.id.root
                            ), R.attr.custom_attr_app_bg
                        )
                    Glide.with(this@CPLevelActivity)
                        .load(award.group_icon)
                        .into((holder.getView<View>(R.id.level_icon) as ImageView))
                    holder.setText(R.id.name, award.name)
                    if (award.has_group) {
                        holder.setTextColor(
                            R.id.name,
                            Color.parseColor(if (themeTag == 1) "#181E25" else "#FFFFFF")
                        )
                    } else {
                        holder.setTextColor(
                            R.id.name,
                            Color.parseColor(if (themeTag == 1) "#CBCDD3" else "#4C4C5F")
                        )
                    }
                    val cpTv = holder.getView<TextView>(R.id.bt_cp_button)
                    if (info.is_test_group) {
                        holder.setVisibility(R.id.bt_cp_button, View.VISIBLE)
                        if (award.isHas_swear) {
                            cpTv.setTextColor(Color.parseColor("#ffffff"))
                            cpTv.background = AppCompatResources.getDrawable(
                                this@CPLevelActivity,
                                R.drawable.bg_cp_level_button_press
                            )
                            cpTv.isEnabled = true
                            cpTv.text = "CP宣言"
                        } else {
                            cpTv.text = "暂无宣言"
                            cpTv.isEnabled = false
                            cpTv.setTextColor(Color.parseColor(if (themeTag == 1) "#cbcdd3" else "#4C4C5F"))
                            if (themeTag == 1) {
                                cpTv.background = AppCompatResources.getDrawable(
                                    this@CPLevelActivity,
                                    R.drawable.bg_cp_level_button_normal
                                )
                            } else {
                                cpTv.background = AppCompatResources.getDrawable(
                                    this@CPLevelActivity,
                                    R.drawable.bg_cp_level_button_night
                                )
                            }
                        }
                    } else {
                        holder.setVisibility(R.id.bt_cp_button, View.GONE)
                    }
                    cpTv.setOnClickListener(View.OnClickListener {
                        if (ViewUtils.isFastClick()) {
                            return@OnClickListener
                        }
                        val bundle = Bundle()
                        bundle.putString("female_user_id", sessionId)
                        bundle.putString("female_user_head", avaUrl)
                        bundle.putString("female_user_name", name)
                        bundle.putInt("level", award.advance_level)
                        if (info.rank_info != null) {
                            bundle.putString("intimacy", info.rank_info.cp_score)
                        }
                        val intent1 = Intent(mContext, DeclarationDetailActivity::class.java)
                        intent1.putExtras(bundle)
                        mContext.startActivity(intent1)
                        val params = HashMap<String?, String?>()
                        params["user_id"] = AccountUtil.getInstance().userId
                        params["female_guest_id"] = sessionId
                        params["event_time"] = System.currentTimeMillis().toString()
                        params["rise_to"] = award.name
                        params["intimacy"] = info.rank_info.need_score
                        MultiTracker.onEvent(TrackConstants.B_CLICK_CP_LEVEL_PAGE_OATH, params)
                    })
                }

                override fun getItemViewType(position: Int): Int {
                    return award_list[position].type
                }
            }
        mAdapter.add(award_list)
        mBinding.levelRv.adapter = mAdapter
        gridLayoutManager.spanSizeLookup = object : SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                val itemViewType = mAdapter.getItemViewType(position)
                return if (itemViewType == 2) {
                    3
                } else {
                    2
                }
            }
        }
        mBinding.levelRv.addItemDecoration(object : ItemDecoration() {
            override fun getItemOffsets(
                outRect: Rect,
                view: View,
                parent: RecyclerView,
                state: RecyclerView.State
            ) {
                outRect[0, 0, 1] = 1
            }

            override fun onDraw(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
                super.onDraw(c, parent, state)
            }
        })
    }

    override fun onScrollChanged(
        scrollView: ObservableScrollView,
        x: Int,
        y: Int,
        oldx: Int,
        oldy: Int
    ) {
        if (y > mBinding.topBg.height - mBinding.llTitleLayout.height) {
            JinquanResourceHelper.getInstance(this@CPLevelActivity)
                .setImageResourceByAttr(mBinding.ivBack, R.attr.custom_attr_back)
            JinquanResourceHelper.getInstance(this@CPLevelActivity).setBackgroundResourceByAttr(
                mBinding.llTitleLayout, R.attr.custom_attr_app_bg
            )
        } else {
            mBinding.ivBack.setImageResource(R.mipmap.icon_user_center_back)
            mBinding.llTitleLayout.setBackgroundColor(Color.argb(0, 255, 255, 255))
        }
    }

}