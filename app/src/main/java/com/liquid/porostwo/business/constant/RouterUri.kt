package com.liquid.porostwo.business.constant

/**
 * 跳转URI
 */
object RouterUri {

    const val SCHEME_URI = "arouter://www.sdbox.com"

    private const val HOST = "/poros"
    //cp等级页面
    const val CP_LEVEL = "${HOST}/cp_level"

    //设置页面
    const val SETTING = "${HOST}/setting"

    //小游戏
    const val SMALL_GAME = "${HOST}/small_game"
}