package com.liquid.porostwo.business.setting.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.liquid.library.retrofitx.RetrofitX
import com.liquid.poros.entity.BaseBean
import com.liquid.poros.entity.CPLevelInfo
import com.liquid.poros.http.ApiUrl
import com.liquid.poros.http.observer.ObjectObserver
import com.liquid.poros.http.utils.postPoros

class SettingViewModel :ViewModel() {

    val logoutData:MutableLiveData<BaseBean>  = MutableLiveData<BaseBean>()

    fun logout(){
        RetrofitX.url(ApiUrl.LOGOUT)
            .postPoros()
            .subscribe(object : ObjectObserver<BaseBean>() {
                override fun success(result: BaseBean) {
                    logoutData.postValue(result)
                }

                override fun failed(result: BaseBean) {
                    logoutData.postValue(result)
                }
            })
    }
}