package com.liquid.porostwo.business.cp.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.liquid.library.retrofitx.RetrofitX
import com.liquid.poros.entity.CPLevelInfo
import com.liquid.poros.http.ApiUrl
import com.liquid.poros.http.observer.ObjectObserver
import com.liquid.poros.http.utils.postPoros

class CPLevelViewModel :ViewModel() {

    val levelData:MutableLiveData<CPLevelInfo>  = MutableLiveData<CPLevelInfo>()

    fun getLevelInfo(maleUserId:String){
        RetrofitX.url(ApiUrl.CP_LEVEL)
            .param("target_user_id", maleUserId)
            .postPoros()
            .subscribe(object : ObjectObserver<CPLevelInfo>() {
                override fun success(result: CPLevelInfo) {
                    levelData.postValue(result)
                }

                override fun failed(result: CPLevelInfo) {
                    levelData.postValue(result)
                }
            })
    }
}