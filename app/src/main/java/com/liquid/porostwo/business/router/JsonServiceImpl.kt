package com.liquid.porostwo.business.router

import android.content.Context
import com.alibaba.android.arouter.facade.annotation.Route
import com.alibaba.android.arouter.facade.service.SerializationService
import com.blankj.utilcode.util.GsonUtils
import java.lang.reflect.Type

/**
 * Used for json converter
 */
@Route(path = "/poros/json")
class JsonServiceImpl : SerializationService {
    override fun init(context: Context) {}
    override fun <T> json2Object(text: String, clazz: Class<T>): T {
        return GsonUtils.fromJson(text, clazz)
    }

    override fun object2Json(instance: Any): String {
        return GsonUtils.toJson(instance)
    }

    override fun <T> parseObject(input: String, clazz: Type): T {
        return GsonUtils.fromJson(input, clazz)
    }
}