package com.liquid.porostwo.business.smallgame.viewmodel

import android.os.CountDownTimer
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.liquid.base.utils.LogOut
import com.liquid.im.kit.custom.RoomActionAttachment
import com.liquid.library.retrofitx.RetrofitX
import com.liquid.poros.entity.BaseBean
import com.liquid.poros.entity.SmallGameTeam
import com.liquid.poros.helper.AvHelper
import com.liquid.poros.http.ApiUrl
import com.liquid.poros.http.observer.ObjectObserver
import com.liquid.poros.http.utils.postPoros
import com.netease.nimlib.sdk.AbortableFuture
import com.netease.nimlib.sdk.NIMClient
import com.netease.nimlib.sdk.Observer
import com.netease.nimlib.sdk.RequestCallback
import com.netease.nimlib.sdk.chatroom.ChatRoomService
import com.netease.nimlib.sdk.chatroom.ChatRoomServiceObserver
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage
import com.netease.nimlib.sdk.chatroom.model.EnterChatRoomData
import com.netease.nimlib.sdk.chatroom.model.EnterChatRoomResultData

class SmallGameViewModel :ViewModel() {


    //状态
    private val STATE_UNJOIN = 0 //未加入队伍 ，进到此页面后，此状态很短暂
    private val STATE_WAITING = 1 //已加入队伍,等待队友，如果是游戏接收者，因为发起者已经加入聊天，接收者加入后就是未准备状态，没有等待状态
    private val STATE_UNREADY = 2//未准备
    private val STATE_READYED = 3//已准备
    private val STATE_PLAYING = 4//游戏中
    private var state = STATE_UNJOIN

    //角色
    private val ROLE_LAUNCHER = 0 //游戏发起人
    private val ROLE_RECEIVER = 1 //游戏接收者
    private var role = ROLE_LAUNCHER


    private var mAvHelper:AvHelper?= null

//    val gameTeamData:MutableLiveData<SmallGameTeam>  = MutableLiveData<SmallGameTeam>()

    val selfReadyedData:MutableLiveData<BaseBean>  = MutableLiveData<BaseBean>()
    val selfCancelData:MutableLiveData<BaseBean>  = MutableLiveData<BaseBean>()
    val startGameData:MutableLiveData<BaseBean>  = MutableLiveData<BaseBean>()
    val inviteGameData:MutableLiveData<BaseBean>  = MutableLiveData<BaseBean>()

    var timerData:MutableLiveData<Long> = MutableLiveData()

    val otherJoinedMessage:MutableLiveData<Any>  = MutableLiveData<Any>()
    val otherReadyedMessage:MutableLiveData<Any>  = MutableLiveData<Any>()
    val otherCancelMessage:MutableLiveData<Any>  = MutableLiveData<Any>()
    val otherRefusedMessage:MutableLiveData<Any>  = MutableLiveData<Any>()

    val allReadyedMessage:MutableLiveData<Any>  = MutableLiveData<Any>()//都准备好了，可以进入游戏中
    val subjectMessage:MutableLiveData<Any>  = MutableLiveData<Any>()//题目

    /**
     * 准备游戏
     */
    fun ready(){
        RetrofitX.url(ApiUrl.READY_GAME_TEAM)
                .postPoros()
                .subscribe(object : ObjectObserver<SmallGameTeam>() {
                    override fun success(result: SmallGameTeam) {
                        otherReadyedMessage.postValue(result)

                    }

                    override fun failed(result: SmallGameTeam) {
                        otherReadyedMessage.postValue(result)

                    }
                })
    }
    /**
     * 注册消息接收器
     */
    fun registerObservers(register: Boolean) {
        NIMClient.getService(ChatRoomServiceObserver::class.java).observeReceiveMessage(incomingMessageObserver, register)
    }

    var incomingMessageObserver: Observer<List<ChatRoomMessage>> = Observer { messages -> onIncomingMessage(messages) }

    private fun onIncomingMessage(messages: List<ChatRoomMessage>?) {
        if (messages == null || messages.isEmpty()) {
            return
        }
        for (message in messages) {
            if (message.attachment is RoomActionAttachment) {
                val attachment = message.attachment as RoomActionAttachment

                if (attachment.action == 420) {
                    //1对方已加入
                    otherJoinedMessage.postValue(Any())
                }

                if (attachment.action == 421) {

                    otherReadyedMessage.postValue(Any())

                    //2对方已准备
                    //2.1自己已准备  --> 2.1.1进入游戏中
                    //2.2自己未准备  -->
                    //2.2.1准备后进入游戏中
                    //2.2.2超时未准备退出游戏


                }
                if (attachment.action == 422) {
                    otherCancelMessage.postValue(Any())

                }
                if (attachment.action == 423) {

                    //4 对方已拒绝
                    otherRefusedMessage.postValue(Any())
                }
                if (attachment.action == 424) {

                    //5 对方已取消（游戏前点击返回）
                    otherCancelMessage.postValue(Any())

                }
                if (attachment.action == 425) {

                    //6 都准备好了
                    allReadyedMessage.postValue(Any())
                }
                if (attachment.action == 426) {

                    //7 题目
                    subjectMessage.postValue(Any())
                }
            }
        }
    }

    /**
     * 进入im聊天室
     */
    fun enterChatRoom(roomId: String?) {
        val enterRequest: AbortableFuture<EnterChatRoomResultData?>? = NIMClient.getService(ChatRoomService::class.java).enterChatRoomEx(EnterChatRoomData(roomId), 10)
        enterRequest?.setCallback(object : RequestCallback<EnterChatRoomResultData?> {
            override fun onSuccess(result: EnterChatRoomResultData?) {
                LogOut.debug("test_match_log", "onSuccess")
            }

            override fun onFailed(code: Int) {
                LogOut.debug("test_match_log", "onFailed = $code")
            }

            override fun onException(exception: Throwable) {
                LogOut.debug("test_match_log", "onException")
            }
        })
    }

    private var timer: CountDownTimer? = null//计时器

    /**
     * remainTime 倒计时时长，单位：秒
     */
    fun startTimer(remainTime:Long) {
        stopTimer()
        timer = object : CountDownTimer(remainTime * 1000, 1000) {
            override fun onTick(millisUntilFinished: Long) {

                val time =  millisUntilFinished / 1000
                timerData.postValue(time)

            }

            override fun onFinish() {
                stopTimer()
            }
        }
        timer!!.start()
    }


    /**
     * 如果 有需要计时的item，开始计时
     */
    fun stopTimer() {
        if (null != timer) {
            timer!!.cancel()
            timer = null
        }
    }

    /**
     * 取消
     */
    fun cancel(){
        RetrofitX.url(ApiUrl.CANCEL_GAME_TEAM)
                .postPoros()
                .subscribe(object : ObjectObserver<SmallGameTeam>() {
                    override fun success(result: SmallGameTeam) {
                        selfCancelData.postValue(result)

                    }

                    override fun failed(result: SmallGameTeam) {
                        selfCancelData.postValue(result)
                    }
                })
    }

    /**
     * 开始游戏
     */
    fun startGame(){
        RetrofitX.url(ApiUrl.START_GAME)
                .postPoros()
                .subscribe(object : ObjectObserver<SmallGameTeam>() {
                    override fun success(result: SmallGameTeam) {
                        startGameData.postValue(result)

                    }

                    override fun failed(result: SmallGameTeam) {
                        startGameData.postValue(result)
                    }
                })
    }

    /**
     * 邀请
     */
    fun invite(){
        RetrofitX.url(ApiUrl.INVITE_GAME)
                .postPoros()
                .subscribe(object : ObjectObserver<SmallGameTeam>() {
                    override fun success(result: SmallGameTeam) {
                        inviteGameData.postValue(result)

                    }

                    override fun failed(result: SmallGameTeam) {
                        inviteGameData.postValue(result)
                    }
                })
    }
}