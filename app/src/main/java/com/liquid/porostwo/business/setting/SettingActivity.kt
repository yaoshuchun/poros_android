package com.liquid.porostwo.business.setting

import android.content.Intent
import android.os.Bundle
import androidx.core.view.isVisible
import com.alibaba.android.arouter.facade.annotation.Route
import com.blankj.utilcode.util.ToastUtils
import com.liquid.poros.BuildConfig
import com.liquid.poros.R
import com.liquid.poros.analytics.utils.MultiTracker
import com.liquid.poros.constant.TrackConstants
import com.liquid.poros.databinding.ActivitySettingBinding
import com.liquid.poros.sdk.HeartBeatUtils
import com.liquid.poros.ui.activity.LoginActivity
import com.liquid.poros.ui.activity.setting.AboutPorosActivity
import com.liquid.poros.ui.activity.setting.DebugInfoActivity
import com.liquid.poros.ui.fragment.dialog.common.CommonActionListener
import com.liquid.poros.ui.fragment.dialog.common.CommonDialogBuilder
import com.liquid.poros.ui.fragment.dialog.common.CommonDialogFragment
import com.liquid.poros.ui.fragment.dialog.mine.FeedBackDialogFragment
import com.liquid.poros.utils.AccountUtil
import com.liquid.poros.utils.AppConfigUtil
import com.liquid.poros.utils.SharePreferenceUtil
import com.liquid.porostwo.base.BaseActivity
import com.liquid.porostwo.business.constant.RouterUri
import com.liquid.porostwo.business.setting.viewmodel.SettingViewModel
import com.umeng.analytics.MobclickAgent

@Route(path = RouterUri.SETTING)
class SettingActivity : BaseActivity<ActivitySettingBinding>() {

    val settingViewModel: SettingViewModel by lazy { getActivityViewModel(SettingViewModel::class.java) }

    override fun getPageId(): String = TrackConstants.PAGE_SETTING
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initListener()
        initObserver()
        mBinding.tvGotoDebug.isVisible = BuildConfig.DEBUG
    }

    private fun initObserver() {
        settingViewModel.logoutData.observe(this) {
            if (it.code == 0) {
                logoutSucceed()
            } else {
                ToastUtils.showShort(it.msg)
            }
        }
    }

    private fun initListener() {
        mBinding.rlFeedBackGroup.setOnClickListener {
            FeedBackDialogFragment.newInstance()
                .show(supportFragmentManager, FeedBackDialogFragment::class.java.simpleName)
            MultiTracker.onEvent(TrackConstants.P_FEEDBACK)
        }

        mBinding.rlAboutPoros.setOnClickListener {
            startActivity(Intent(this@SettingActivity, AboutPorosActivity::class.java))
        }

        mBinding.tvGotoDebug.setOnClickListener {
            startActivity(Intent(this@SettingActivity, DebugInfoActivity::class.java))
        }

        mBinding.tvLogout.setOnClickListener {
            CommonDialogBuilder()
                .setWithCancel(true)
                .setTitle(getString(R.string.setting_logout_title))
                .setDesc(getString(R.string.setting_logout_content))
                .setCancel(getString(R.string.setting_stay_tip))
                .setConfirm(getString(R.string.setting_logout_tip))
                .setConfirmBg(R.drawable.bg_logout_confirm)
                .setListener(object : CommonActionListener {
                    override fun onConfirm() {
                        settingViewModel.logout()
                    }

                    override fun onCancel() {}
                }).create()
                .show(supportFragmentManager, CommonDialogFragment::class.java.simpleName)
        }
    }

    private fun logoutSucceed() {
        SharePreferenceUtil.removeKey(
            SharePreferenceUtil.FILE_USER_ACCOUNT_DATA,
            SharePreferenceUtil.KEY_USER_ACCOUNT_TOKEN
        )
        SharePreferenceUtil.removeKey(
            SharePreferenceUtil.FILE_USER_ACCOUNT_DATA,
            SharePreferenceUtil.KEY_USER_ACCOUNT_INFO
        )
        SharePreferenceUtil.removeKey(
            SharePreferenceUtil.FILE_USER_ACCOUNT_DATA,
            SharePreferenceUtil.FILE_USER_ACCOUNT_DATA
        )
        //友盟账号登出
        MobclickAgent.onProfileSignOff()
        AppConfigUtil.removeBaseConfig()
        AccountUtil.getInstance().userInfo = null
        startActivity(Intent(this@SettingActivity, LoginActivity::class.java).apply {
            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        })
        HeartBeatUtils.shutdown()
        finish()
    }
}