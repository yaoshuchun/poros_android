package com.liquid.porostwo.business.router

import android.app.Activity
import android.net.Uri
import android.os.Bundle
import com.alibaba.android.arouter.facade.Postcard
import com.alibaba.android.arouter.facade.callback.NavCallback
import com.alibaba.android.arouter.launcher.ARouter
import com.liquid.poros.R
import com.liquid.poros.utils.StatusBarUtil

/**
 * 用于跳转的Activity
 */
class SchemeFilterActivity : Activity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_scheme_filter)
        StatusBarUtil.setDarkMode(this)
        window.setBackgroundDrawableResource(R.color.transparent)

//        直接通过ARouter处理外部Uri
        val uri = intent.getStringExtra("uri")
        val build =
            if (!uri.isNullOrEmpty())
                ARouter.getInstance().build(Uri.parse(uri))
            else
                ARouter.getInstance().build(intent.data)
        build.navigation(this, object : NavCallback() {
            override fun onArrival(postcard: Postcard) {
                finish()
            }
        })
//        overridePendingTransition(R.anim.slide_in, 0)
    }
}