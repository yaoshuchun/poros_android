package com.liquid.porostwo.utils

import android.app.Activity
import android.app.Dialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.viewbinding.ViewBinding
import java.lang.reflect.ParameterizedType
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

inline fun <reified VB : ViewBinding> Activity.inflate() = lazy {
    inflateBinding<VB>(layoutInflater).apply { setContentView(root) }
}

inline fun <reified VB : ViewBinding> Dialog.inflate() = lazy {
    inflateBinding<VB>(layoutInflater).apply { setContentView(root) }
}

@Suppress("UNCHECKED_CAST")
inline fun <reified VB : ViewBinding> inflateBinding(layoutInflater: LayoutInflater) =
    VB::class.java.getMethod("inflate", LayoutInflater::class.java)
        .invoke(null, layoutInflater) as VB


/**
 * 获取泛型的运行时的真是类型
 */
fun <T> getGenericRealClass(subclass: Class<*>): Class<*> {
    val superclass = subclass.genericSuperclass
    if (superclass is Class<*>) {
        throw RuntimeException("Missing type parameter.")
    }
    val parameterized = superclass as ParameterizedType?
    return (parameterized!!.actualTypeArguments[0] as Class<*>)
}

/**
 * 根据类声明泛型获取ViewBinding实体类并初始化
 * [仅限Activity使用]
 */
fun <VB : ViewBinding> getBindingClassByGeneric(
    subclass: Class<*>,
    layoutInflater: LayoutInflater
): VB {
    return getGenericRealClass<VB>(subclass).getMethod(
        "inflate",
        LayoutInflater::class.java
    ).invoke(null, layoutInflater) as VB
}

/**
 * 根据类声明泛型获取ViewBinding实体类并初始化
 * [仅限Fragment使用]
 */
fun <VB : ViewBinding> getBindingClassByGeneric(
    subclass: Class<*>,
    layoutInflater: LayoutInflater,
    container: ViewGroup?
): VB {
    return getGenericRealClass<VB>(subclass).getMethod(
        "inflate",
        LayoutInflater::class.java,
        ViewGroup::class.java,
        Boolean::class.java
    ).invoke(null, layoutInflater, container, false) as VB
}


/**
 * Fragment 获取ViewBinding
 */
fun <VB : ViewBinding> Fragment.bindView() =
    FragmentBindingDelegate<VB>()

class FragmentBindingDelegate<VB : ViewBinding>() : ReadOnlyProperty<Fragment, VB> {

    private var isInitialized = false
    private var _binding: VB? = null
    private val binding: VB get() = _binding!!

    override fun getValue(thisRef: Fragment, property: KProperty<*>): VB {
        if (!isInitialized) {
            thisRef.viewLifecycleOwner.lifecycle.addObserver(object : LifecycleObserver {
                @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
                fun onDestroyView() {
                    _binding = null
                }
            })
            _binding = getGenericRealClass<VB>(javaClass).getMethod("bind", View::class.java)
                .invoke(null, thisRef.requireView()) as VB
            isInitialized = true
        }
        return binding
    }
}
