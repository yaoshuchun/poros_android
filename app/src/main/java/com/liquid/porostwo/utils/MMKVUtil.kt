package com.liquid.porostwo.utils

import android.os.Parcelable
import com.tencent.mmkv.MMKV

/**
 * 本地数据缓存类
 * save data use encode
 * get data use decode
 */
object MMKVUtil {
    var mmkv: MMKV? = null

    init {
        mmkv = MMKV.defaultMMKV()
    }

    fun encode(key: String, value: Any?) {
        when (value) {
            is String -> mmkv?.encode(key, value)
            is Float -> mmkv?.encode(key, value)
            is Boolean -> mmkv?.encode(key, value)
            is Int -> mmkv?.encode(key, value)
            is Long -> mmkv?.encode(key, value)
            is Double -> mmkv?.encode(key, value)
            is ByteArray -> mmkv?.encode(key, value)
            is Nothing -> return
        }
    }

    fun <T : Parcelable> encode(key: String, t: T?) {
        if (t == null) {
            return
        }
        mmkv?.encode(key, t)
    }

    fun encode(key: String, sets: Set<String>?) {
        if (sets == null) {
            return
        }
        mmkv?.encode(key, sets)
    }

    fun decode(key: String, defaultValue: Int = 0): Int = try {
        mmkv?.decodeInt(key, defaultValue)!!
    } catch (e: Exception) {
        defaultValue
    }

    fun decode(key: String, defaultValue: Double = 0.00): Double = try {
        mmkv?.decodeDouble(key, defaultValue)!!
    } catch (e: Exception) {
        defaultValue
    }

    fun decode(key: String, defaultValue: Long = 0): Long = try {
        mmkv?.decodeLong(key, defaultValue)!!
    } catch (e: Exception) {
        defaultValue
    }

    fun decode(key: String, defaultValue: Boolean = false): Boolean = try {
        mmkv?.decodeBool(key, defaultValue)!!
    } catch (e: Exception) {
        defaultValue
    }

    fun decode(key: String, defaultValue: Float = 0F): Float = try {
        mmkv?.decodeFloat(key, defaultValue)!!
    } catch (e: Exception) {
        defaultValue
    }

    fun decode(key: String, defaultValue: String = ""): String = try {
        mmkv?.decodeString(key, defaultValue)!!
    } catch (e: Exception) {
        defaultValue
    }

    @JvmStatic
    fun <T : Parcelable> decode(key: String, tClass: Class<T>): T? {
        return mmkv?.decodeParcelable(key, tClass)
    }

    fun decode(key: String): Set<String>? {
        return mmkv?.decodeStringSet(key, setOf())
    }

    fun removeKey(key: String) {
        mmkv?.removeValueForKey(key)
    }

    fun clearAll() {
        mmkv?.clearAll()
    }
}

