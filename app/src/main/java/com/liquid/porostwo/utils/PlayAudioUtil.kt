package com.liquid.porostwo.utils

import android.media.AudioManager
import android.media.MediaPlayer
import com.liquid.poros.PorosApplication
import com.liquid.poros.R

object PlayAudioUtil {
    var player: MediaPlayer? = null

    fun playAudioNotification(resId:Int = R.raw.mic_failed_tip,looping :Boolean = false) {
        try {
            if (player == null) {
                player = MediaPlayer.create(PorosApplication.context, resId)
                player?.isLooping = looping
                player?.setAudioStreamType(AudioManager.STREAM_MUSIC)
                player?.setOnCompletionListener {
                    player?.release()
                    player = null
                }
                player?.start()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}