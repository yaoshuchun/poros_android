package com.liquid.porostwo

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.kunminx.architecture.ui.callback.UnPeekLiveData
import com.liquid.poros.entity.PlayloadInfo

/**
 * Application级别ViewModel 用于Activity之间传递消息
 * @author guomenglong
 * @date 2020/9/29 5:25 PM
 */
class ApplicationViewModel(application: Application) : AndroidViewModel(application) {
    //微信登录Code
    val loginWebChatCode: UnPeekLiveData<String> by lazy { UnPeekLiveData<String>() }
    //网络状态改变LiveData,true 可连接 false 链接断开
    val netWorkIsConnected : UnPeekLiveData<Boolean> by lazy { UnPeekLiveData() }
    //用户信息变更，用于云信用户信息变更发送事件，List<String> 存储的account list
    val userInfoChange : UnPeekLiveData<List<String>> by lazy { UnPeekLiveData() }
    //用户发来的消息管理
    val messageNotification:UnPeekLiveData<PlayloadInfo> by lazy { UnPeekLiveData() }
}

