package com.liquid.porostwo.base

import android.content.res.Resources
import android.graphics.Color
import android.os.Bundle
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.viewbinding.ViewBinding
import com.alibaba.android.arouter.launcher.ARouter
import com.blankj.utilcode.util.BarUtils
import com.liquid.poros.PorosApplication
import com.liquid.poros.R
import com.liquid.poros.base.ThemeChangeObserver
import com.liquid.poros.constant.Constants
import com.liquid.poros.utils.SharePreferenceUtil
import com.liquid.poros.utils.StatusBarUtil
import com.liquid.porostwo.base.view.LoadingWindow
import com.liquid.porostwo.utils.getBindingClassByGeneric

/**
 * 所有Activity基类
 */
abstract class BaseActivity<T : ViewBinding> : AppCompatActivity(), ThemeChangeObserver {

    abstract fun getPageId(): String
    private lateinit var loadingWindow: LoadingWindow
    private val baseBusinessLifecycle:BaseBusinessLifecycle = BaseBusinessLifecycle()
    private var pageId: String = ""
    protected lateinit var mBinding: T
    private var mActivityProvider: ViewModelProvider? = null

    /**
     * ThemeTag 1 白天 -1 黑夜  默认1
     */
    val themeTag: Int
        get() = SharePreferenceUtil.getInt(
            SharePreferenceUtil.FILE_USER_ACCOUNT_DATA,
            Constants.KEY_CACHE_THEME_TAG,
            1
        )

    /**
     * 是否设置Theme
     */
    protected open fun needSetTheme(): Boolean = true

    open fun getParams(): Map<String, String> = mapOf()

    /**
     * 是否注入ARouter，默认注入
     */
    open fun isInjectARouter(): Boolean = true

    /**
     * 是否沉浸栏
     */
    open fun needTransparent(): Boolean = false


    override fun onCreate(savedInstanceState: Bundle?) {
        setupActivityBeforeCreate()
        super.onCreate(savedInstanceState)
        if (isInjectARouter()) {
            ARouter.getInstance().inject(this)
        }
        mBinding = getBindingClassByGeneric(javaClass, layoutInflater)
        setContentView(mBinding.root)
        pageId = getPageId()
        baseBusinessLifecycle.registerLifecycle(this,getParams(),pageId)
    }

    fun setupActivityBeforeCreate() {
        (application as PorosApplication).registerObserver(this)
        loadingCurrentTheme()
    }

    override fun onDestroy() {
        super.onDestroy()
        baseBusinessLifecycle.unregisterLifecycle()
    }

    override fun loadingCurrentTheme() {
        when (themeTag) {
            1 -> if (needSetTheme()) {
                setTheme(R.style.JinquanTheme_Day)
                StatusBarUtil.setDarkMode(this)
            } else {
                setTheme(R.style.Welcome_Theme)
            }
            -1 -> if (needSetTheme()) {
                setTheme(R.style.JinquanTheme_Night)
                StatusBarUtil.setLightMode(this)
            } else {
                setTheme(R.style.Welcome_Theme_night)
            }
        }
    }

    override fun notifyByThemeChanged() {}

    override fun notifyNetworkChanged(isConnected: Boolean) {}

    fun showLoading(@StringRes message: Int = 0, outSideDismiss: Boolean = true) {
        if (!this::loadingWindow.isInitialized) {
            loadingWindow = LoadingWindow(this, message, outSideDismiss)
        }
        if (!loadingWindow.isShowing) {
            loadingWindow.showPopupWindow()
        }
    }

    fun hideLoading() {
        if (this::loadingWindow.isInitialized && loadingWindow.isShowing) {
            loadingWindow.dismiss()
        }
    }

    open fun <V : ViewModel?> getActivityViewModel(modelClass: Class<V>): V {
        if (mActivityProvider == null) {
            mActivityProvider = ViewModelProvider(this)
        }
        return mActivityProvider!!.get(modelClass)
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        if (needTransparent()) {
            BarUtils.setStatusBarColor(this.window, Color.TRANSPARENT)
        } else {
            BarUtils.setStatusBarColor(
                this.window,
                if (themeTag == 1) Color.WHITE else resources.getColor(R.color.custom_color_app_bg_night)
            )
        }
    }

    override fun getResources(): Resources {
        val res = super.getResources()
        val config = res.configuration
        config.fontScale = 1f
        res.updateConfiguration(config, res.displayMetrics)
        return res
    }

}