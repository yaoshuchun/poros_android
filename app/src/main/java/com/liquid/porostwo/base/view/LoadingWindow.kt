package com.liquid.porostwo.base.view

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.Gravity
import android.view.View
import androidx.annotation.StringRes
import com.liquid.poros.R
import com.liquid.poros.databinding.DialogLoadingViewBinding
import com.liquid.porostwo.base.BaseDialog

/**
 * LoadingWindow
 */
class LoadingWindow(context: Context) : BaseDialog(context) {
    private var msgResId :Int = 0
    private var outSideDismiss:Boolean = true

    private lateinit var mBinding:DialogLoadingViewBinding

    constructor(context: Context,@StringRes msgResId: Int = 0,outSideDismiss:Boolean = true):this(context){
        this.msgResId = msgResId
        this.outSideDismiss = outSideDismiss
    }

    override fun onCreateContentView(): View {
        setOutSideDismiss(outSideDismiss)
        setBackground(ColorDrawable(Color.TRANSPARENT))
        popupGravity = Gravity.CENTER
        return createPopupById(R.layout.dialog_loading_view)
    }

    override fun onViewCreated(contentView: View) {
        mBinding = DialogLoadingViewBinding.bind(contentView)
        mBinding.tip.text = if (msgResId == 0) "" else context.getString(msgResId)
        mBinding.tip.visibility = if (msgResId == 0) View.GONE else View.VISIBLE
    }
}