package com.liquid.porostwo.base.glide

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import com.bumptech.glide.load.DecodeFormat
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.liquid.poros.R


/**
 * 扩展加载url函数，解决url末尾有空格加载失败问题
 * @receiver GlideRequests
 * @param roundingRadius Int
 * @return GlideRequest<Bitmap>
 */
fun GlideRequests.loadUrl(url: String?): GlideRequest<Drawable> {
    return this.load(url?.trim() ?: "")
}

fun GlideRequest<Drawable>.withDefaultAvatar(): GlideRequest<Drawable> {
    return this.placeholder(R.mipmap.img_default_avatar)
}

/**
 * 添加圆角扩展函数
 * @receiver GlideRequests
 * @param roundingRadius Int
 * @return GlideRequest<Bitmap>
 */
fun GlideRequests.asRoundCorner(roundingRadius: Int): GlideRequest<Bitmap> {
    return this.asBitmap().transform(RoundedCorners(roundingRadius))
}

fun GlideRequest<Drawable>.asRoundCorner(roundingRadius: Int): GlideRequest<Drawable> {
    return this.transform(RoundedCorners(roundingRadius))
}

/**
 * 设置当前图片解析格式保留Alpha通道
 * 当Glide memoryCache占满时可能出现有alpha通道的图片解析错误的问题
 * @receiver GlideRequests
 * @return GlideRequests
 */
fun GlideRequests.withAlpha(): GlideRequests {
    return this.setDefaultRequestOptions(
        RequestOptions().format(DecodeFormat.PREFER_ARGB_8888).disallowHardwareConfig()
    )
}