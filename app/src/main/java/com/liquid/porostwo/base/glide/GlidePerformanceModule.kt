package com.liquid.porostwo.base.glide

import android.app.ActivityManager
import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.util.Log
import com.blankj.utilcode.util.LogUtils
import com.bumptech.glide.GlideBuilder
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.load.engine.bitmap_recycle.LruBitmapPool
import com.bumptech.glide.load.engine.cache.LruResourceCache
import com.bumptech.glide.load.resource.bitmap.BitmapTransitionOptions
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.module.AppGlideModule
import com.bumptech.glide.request.RequestOptions
import com.liquid.poros.BuildConfig

/**
 * Glide图片加载优化
 * 如果低内存则加载RGB565否者加载ARGB8888
 */
@GlideModule
class GlidePerformanceModule : AppGlideModule() {

    override fun isManifestParsingEnabled(): Boolean {
        return false
    }

    override fun applyOptions(context: Context, builder: GlideBuilder) {
        super.applyOptions(context, builder)
        if (BuildConfig.DEBUG) {
            LogUtils.i("Glide Module Register")
            builder.setLogLevel(Log.DEBUG)
        }
        val activityManager = context.getSystemService(Context.ACTIVITY_SERVICE)
        val memoryInfo = ActivityManager.MemoryInfo()
        if (activityManager != null && activityManager is ActivityManager) {
            activityManager.getMemoryInfo(memoryInfo)
            LogUtils.i(
                "系统剩余内存=>${memoryInfo.availMem shr 20}m\n" +
                        "系统是否处于低内存运行=>${memoryInfo.lowMemory}\n" +
                        "当系统剩余内存低于${memoryInfo.threshold shr 20}m 时就看成低内存运行"
            )
        }
        //图片渐显
        builder.setDefaultTransitionOptions(
            Bitmap::class.java,
            BitmapTransitionOptions.withCrossFade()
        )
        builder.setDefaultTransitionOptions(
            Drawable::class.java,
            DrawableTransitionOptions.withCrossFade()
        )
        var options = RequestOptions()
        //如果在打开APP时是低内存则跳过MemoryCache否则设置MemoryCache为1
        options = options.skipMemoryCache(memoryInfo.lowMemory)
        //设置Memory 缓存池大小
        val memoryCacheSizeBytes = 1024 * 1024 * 20 // 20mb
        builder.setMemoryCache(LruResourceCache(memoryCacheSizeBytes.toLong()))
        //设置Bitmap 缓存池大小
        val bitmapPoolSizeBytes = 1024 * 1024 * 30 // 30mb
        builder.setBitmapPool(LruBitmapPool(bitmapPoolSizeBytes.toLong()))
        builder.setDefaultRequestOptions(options)
    }
}