package com.liquid.porostwo.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.viewbinding.ViewBinding
import com.liquid.poros.PorosApplication
import com.liquid.poros.R
import com.liquid.poros.analytics.utils.MultiTracker
import com.liquid.poros.base.ThemeChangeObserver
import com.liquid.porostwo.base.view.LoadingWindow
import com.liquid.porostwo.utils.getBindingClassByGeneric

/**
 *fragment基类
 */
abstract class BaseFragment<T : ViewBinding> : Fragment(), ThemeChangeObserver {

    private var startTime: Long = 0
    private var duration: Long = 0
    private var pageId: String = ""
    private var _binding: T? = null
    protected val mBinding get() = _binding!!

    abstract fun getPageId(): String
    private lateinit var loadingWindow: LoadingWindow

    private var mFragmentProvider: ViewModelProvider? = null
    private var mActivityProvider: ViewModelProvider? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupActivityBeforeCreate()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        pageId = getPageId()
        _binding = getBindingClassByGeneric(javaClass, inflater, container)
        return _binding?.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


    private  fun setupActivityBeforeCreate() {
        (requireActivity().application as PorosApplication).registerObserver(this)
        loadingCurrentTheme()
    }

    override  fun loadingCurrentTheme() {
        when ((requireActivity() as BaseActivity<*>).themeTag ) {
            1 -> requireActivity().setTheme(R.style.JinquanTheme_Day)
            -1 -> requireActivity().setTheme(R.style.JinquanTheme_Night)
        }
    }

    override fun notifyByThemeChanged() {}

    override fun notifyNetworkChanged(isConnected: Boolean) {}

    protected open fun <V : ViewModel?> getFragmentViewModel(modelClass: Class<V>): V {
        if (mFragmentProvider == null) {
            mFragmentProvider = ViewModelProvider(this)
        }
        return mFragmentProvider!!.get(modelClass)
    }

    protected open fun <V : ViewModel?> getActivityViewModel(modelClass: Class<V>): V {
        if (mActivityProvider == null) {
            mActivityProvider = ViewModelProvider(requireActivity())
        }
        return mActivityProvider!!.get(modelClass)
    }

    fun showLoading(@StringRes message: Int = 0, outSideDismiss: Boolean = true) {
        try {
            if (!this::loadingWindow.isInitialized) {
                loadingWindow = LoadingWindow(requireActivity(), message, outSideDismiss)
            }
            if (!loadingWindow.isShowing) {
                loadingWindow.showPopupWindow()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun hideLoading() {
        if (this::loadingWindow.isInitialized && loadingWindow.isShowing) {
            loadingWindow.dismiss()
        }
    }

    private fun enterPage() {
        if (pageId.isEmpty()) {
            return
        }
        startTime = System.currentTimeMillis()
        val map = HashMap<String, String>()
        MultiTracker.onPageResume(pageId, map)
    }

    private fun leavePage() {
        if (pageId.isEmpty()) {
            return
        }
        duration = System.currentTimeMillis() - startTime
        val map = HashMap<String, String>()
        map["duration"] = duration.toString()
        MultiTracker.onPagePause(pageId, map)
    }

    override fun onResume() {
        super.onResume()
        enterPage()
    }

    override fun onPause() {
        super.onPause()
        leavePage()
    }

    override fun onDestroy() {
        super.onDestroy()
        (requireActivity().application as PorosApplication).unregisterObserver(this)
    }
}