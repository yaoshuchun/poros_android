package com.liquid.porostwo.base

import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import com.liquid.poros.analytics.utils.MultiTracker
import com.liquid.poros.ui.activity.audiomatch.CPMatchingGlobal
import com.liquid.poros.ui.floatball.GiftPackageFloatManager
import com.liquid.poros.utils.AccountUtil
import java.lang.ref.WeakReference
import java.util.HashMap

/**
 * BaseActivity生命周期相关的业务
 */
class BaseBusinessLifecycle{
    private var startTime: Long = 0
    private var duration: Long = 0
    private var pageId: String = ""
    private var params: Map<String, String> = mapOf()
    private var activity: WeakReference<AppCompatActivity>? = null
    private val handler:Handler = Handler(Looper.getMainLooper())

    private val lifecycleEventObserver: LifecycleEventObserver =
        LifecycleEventObserver { source, event ->
            when(event){
                Lifecycle.Event.ON_CREATE ->{

                }
                Lifecycle.Event.ON_START ->{

                }
                Lifecycle.Event.ON_RESUME ->{
                    enterPage()
                    handler.postDelayed({ CPMatchingGlobal.getInstance(activity?.get()!!).showMatchingBall()},100)
                    handler.postDelayed({ GiftPackageFloatManager.getInstance().showFloat(activity?.get()!!)},100)
                }
                Lifecycle.Event.ON_PAUSE ->{
                    leavePage()
                }
                Lifecycle.Event.ON_STOP ->{

                }
                Lifecycle.Event.ON_DESTROY ->{

                }
            }
        }
    fun registerLifecycle(activity: AppCompatActivity?,params: Map<String, String>,pageId:String){
        this.activity = WeakReference(activity)
        this.params = params
        this.pageId = pageId
        this.activity?.get()?.lifecycle?.addObserver(lifecycleEventObserver)
    }

    fun unregisterLifecycle(){
        activity?.get()?.lifecycle?.removeObserver(lifecycleEventObserver)
    }

    private fun enterPage() {
        if (pageId.isEmpty()) {
            return
        }
        AccountUtil.getInstance().pageId = pageId
        startTime = System.currentTimeMillis()
        val map = HashMap<String, String>()
        map.putAll(params)
        MultiTracker.onPageResume(pageId, map)
    }

    private fun leavePage() {
        if (pageId.isEmpty()) {
            return
        }
        duration = System.currentTimeMillis() - startTime
        val map = HashMap<String, String>()
        map["duration"] = duration.toString()
        map.putAll(params)
        MultiTracker.onPagePause(pageId, map)
    }
}