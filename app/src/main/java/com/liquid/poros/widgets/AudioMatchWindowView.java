package com.liquid.poros.widgets;

import android.content.Context;

import androidx.annotation.NonNull;

import com.liquid.poros.R;
import com.liquid.poros.utils.DensityUtil;
import com.liquid.stat.boxtracker.util.DeviceUtil;

public class AudioMatchWindowView extends BaseWindowView {
    private RippleView ballBG;

    public AudioMatchWindowView(@NonNull Context context) {
        super(context);
        initView();
    }

    public AudioMatchWindowView(@NonNull Context context, String saveLocalDataKey) {
        super(context, saveLocalDataKey);
        initView();
    }

    @Override
    public int contentXmlResourceId() {
        return R.layout.window_view_audio_match;
    }

    @Override
    protected int initialPosiX() {
        //62 自身宽度
        return DeviceUtil.getScreenWidth(mContext) - DensityUtil.dp2px(62) - DensityUtil.dp2px(6);
    }

    @Override
    protected int initialPosiY() {
        return DensityUtil.dp2px(558);
    }

    private void initView() {
        ballBG = findViewById(R.id.matching_ball_bg);
    }

    public void showBallAnimation() {
        ballBG.setVisibility(VISIBLE);
        ballBG.startWaveAnimation();
    }

    public void hideBallAnimation() {
        ballBG.setVisibility(GONE);
        ballBG.stopWaveAnimation();
    }

    @Override
    protected void onViewClick() {
        super.onViewClick();

    }
}
