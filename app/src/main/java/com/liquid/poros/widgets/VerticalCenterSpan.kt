package com.liquid.poros.widgets

import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.text.TextPaint
import android.text.style.ReplacementSpan
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import com.liquid.base.guideview.DimenUtil
import com.liquid.base.utils.ContextUtils

/**
 * 使TextView中不同大小字体垂直居中
 */
class VerticalCenterSpan(//单位:sp
        private var fontSizeSp: Int,private var color: Int) : ReplacementSpan() {

    override fun getSize(@NonNull paint: Paint, text: CharSequence?, start: Int, end: Int, @Nullable fm: Paint.FontMetricsInt?): Int {
        val newPaint: Paint = getCustomTextPaint(paint)
        return newPaint.measureText(text, start, end).toInt()
    }

    override fun draw(@NonNull canvas: Canvas, text: CharSequence, start: Int, end: Int, x: Float, top: Int, y: Int, bottom: Int, @NonNull paint: Paint) {
        val newPaint: Paint = getCustomTextPaint(paint)
        val fontMetricsInt = newPaint.fontMetricsInt
        val offsetY = (y + fontMetricsInt.ascent + y + fontMetricsInt.descent) / 2 - (top + bottom) / 2
        canvas.drawText(text, start, end, x, y - offsetY.toFloat(), newPaint)
    }

    private fun getCustomTextPaint(srcPaint: Paint): TextPaint {
        val textPaint = TextPaint(srcPaint)
        textPaint.color = color
        textPaint.textSize = DimenUtil.sp2px(ContextUtils.getApplicationContext(), fontSizeSp.toFloat()).toFloat() //設定字型大小, sp轉換為px
        return textPaint
    }
}

