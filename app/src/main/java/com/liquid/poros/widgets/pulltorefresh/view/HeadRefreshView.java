package com.liquid.poros.widgets.pulltorefresh.view;

import android.content.Context;
import android.graphics.drawable.Animatable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.liquid.poros.R;


/**
 * Created Time:　12/11/2018
 * Created by  :  jcsun
 * Desc        :
 */
public class HeadRefreshView extends FrameLayout implements HeadView {

    private ImageView arrow;

    public HeadRefreshView(Context context) {
        this(context, null);
    }

    public HeadRefreshView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public HeadRefreshView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_header, null);
        addView(view);
        arrow = view.findViewById(R.id.header_arrow);

    }

    @Override
    public void begin() {
        ((Animatable) arrow.getDrawable()).start();
    }

    @Override
    public void progress(float progress, float all) {
        if (!((Animatable) arrow.getDrawable()).isRunning()) {
            ((Animatable) arrow.getDrawable()).start();
        }
    }

    @Override
    public void finishing(float progress, float all) {
        ((Animatable) arrow.getDrawable()).stop();
    }

    @Override
    public void loading() {

    }

    @Override
    public void normal() {
        ((Animatable) arrow.getDrawable()).stop();
    }

    @Override
    public View getView() {
        return this;
    }
}
