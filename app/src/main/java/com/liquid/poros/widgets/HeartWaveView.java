package com.liquid.poros.widgets;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PaintFlagsDrawFilter;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

public class HeartWaveView extends View {
    private PaintFlagsDrawFilter mDrawFilter;
    private Paint mWavePaint;
    private float mOffset1 = 0.0f;
    private float mOffset2 = 0.0f;
    private float mSpeed1 = 0.05f;
    private float mSpeed2 = 0.07f;
    private int paddingLeft;
    private int paddingRight;
    private int waveColor = 0xFFFF598C;

    private int paddingTop;
    private int paddingBottom;
    private int realWidth;
    private int realHeight;


    public HeartWaveView(Context context) {
        super(context);
        init();
    }

    public HeartWaveView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public HeartWaveView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void init() {
        // 初始绘制波纹的画笔
        mWavePaint = new Paint();
        // 去除画笔锯齿
        mWavePaint.setAntiAlias(true);
        // 设置风格为实线
        mWavePaint.setStyle(Paint.Style.FILL);
        // 设置画笔颜色
        mWavePaint.setColor(waveColor);
        mDrawFilter = new PaintFlagsDrawFilter(0, Paint.ANTI_ALIAS_FLAG | Paint.FILTER_BITMAP_FLAG);

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int usedHeight = getRealHeight(heightMeasureSpec);
        int usedWidth = getRealWidth(widthMeasureSpec);
        setMeasuredDimension(usedWidth, usedHeight);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        realWidth = w;
        realHeight = h;
    }

    public int getRealWidth(int widthMeasureSpec) {
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthVal = MeasureSpec.getSize(widthMeasureSpec);
        //取得父View或ViewGroup分配的默认为子View的大小和模式
        paddingLeft = getPaddingLeft();
        paddingRight = getPaddingRight();
        //注意处理一下Padding，把Padding值也赋给想要设置的子View的大小
        if (widthMode == MeasureSpec.EXACTLY) {
            return paddingLeft + paddingRight + widthVal;
            //精确模式下返回具体值
        } else if (widthMode == MeasureSpec.UNSPECIFIED) {
            return Math.abs(+paddingLeft + paddingRight);
            //未确定模式下返回尽可能容纳View的尺寸
        } else {
            return Math.min(paddingLeft + paddingRight, widthVal);
            //AT_MOST下返回父View限制值和View尽可能大尺寸之间的最小值
        }
    }

    public int getRealHeight(int heightMeasureSpec) {
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightVal = MeasureSpec.getSize(heightMeasureSpec);
        //取得父View或ViewGroup分配的默认为子View的大小和模式
        paddingTop = getPaddingTop();
        paddingBottom = getPaddingBottom();
        //注意处理一下Padding，把Padding值也赋给想要设置的子View的大小
        if (heightMode == MeasureSpec.EXACTLY) {
            return paddingTop + paddingBottom + heightVal;
            //精确模式下返回具体值
        } else if (heightMode == MeasureSpec.UNSPECIFIED) {
            return Math.abs(+paddingTop + paddingBottom);
            //未确定模式下返回尽可能容纳View的尺寸
        } else {
            return Math.min(paddingTop + paddingBottom, heightVal);
            //AT_MOST下返回父View限制值和View尽可能大尺寸之间的最小值
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        // 从canvas层面去除绘制时锯齿
        canvas.setDrawFilter(mDrawFilter);
        for (int i = 0; i < realWidth; i++) {

            // y = A * sin( wx + b) + h ; A： 浪高； w：周期；b：初相；
            float endY = (float) (5 * Math.sin(2 * Math.PI / realWidth * i + mOffset1) + realHeight * progress);
            //画第一条波浪
            canvas.drawLine(i, realHeight, i, endY, mWavePaint);

            //画第二条波浪
            float endY2 = (float) (5 * Math.sin(2 * Math.PI / realWidth * i + mOffset2) + realHeight * progress);
            canvas.drawLine(i, realHeight, i, endY2, mWavePaint);
        }

        if (mOffset1 > Float.MAX_VALUE - 1) {//防止数值超过浮点型的最大值
            mOffset1 = 0;
        }
        mOffset1 += mSpeed1;

        if (mOffset2 > Float.MAX_VALUE - 1) {//防止数值超过浮点型的最大值
            mOffset2 = 0;
        }
        mOffset2 += mSpeed2;
        //刷新
        if (progress > 0) {
            postInvalidateDelayed(30);
        }

    }

    private float progress = 1;

    public void progress(float progress) {
        this.progress = progress;
        if (progress < 0.2) {
            this.progress = 0.2f;
        }
        if (progress > 0.8) {
            this.progress = 0.8f;
        }
        postInvalidate();
    }
}
