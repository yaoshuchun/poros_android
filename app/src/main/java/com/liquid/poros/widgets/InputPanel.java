package com.liquid.poros.widgets;

import android.app.Activity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.liquid.base.utils.ToastUtil;
import com.liquid.im.kit.custom.StickerAttachment;
import com.liquid.im.kit.emoji.EmojiManager;
import com.liquid.im.kit.emoji.EmoticonPickerView;
import com.liquid.im.kit.emoji.IEmoticonSelectedListener;
import com.liquid.im.kit.permission.MPermission;
import com.liquid.poros.R;
import com.liquid.poros.adapter.QuickMessageAdapter;
import com.liquid.poros.helper.AudioPlayerHelper;
import com.liquid.poros.utils.CountDownTimerUtil;
import com.liquid.poros.utils.KeyboardHelper;
import com.liquid.poros.utils.MoonUtil;
import com.liquid.poros.utils.ScreenUtil;
import com.liquid.poros.utils.StringUtil;
import com.netease.lava.nertc.sdk.NERtcEx;
import com.netease.nimlib.sdk.media.record.AudioRecorder;
import com.netease.nimlib.sdk.media.record.IAudioRecordCallback;
import com.netease.nimlib.sdk.media.record.RecordType;
import com.netease.nimlib.sdk.msg.MessageBuilder;
import com.netease.nimlib.sdk.msg.attachment.MsgAttachment;
import com.netease.nimlib.sdk.msg.constant.SessionTypeEnum;
import com.netease.nimlib.sdk.msg.model.IMMessage;
import java.io.File;
import java.util.List;

public class InputPanel implements View.OnClickListener, IEmoticonSelectedListener, QuickMessageAdapter.OnListener {
    private View container;
    public static final int NORMAL_CHAT = 1;
    public static final int GIRL_CHAT = 2;
    private View tv_send;
    private View iv_choose_picture;
    private View tv_take_picture;
    private EditText et_content;
    private EmoticonPickerView emoticonPickerView;
    private ImageView iv_choose_audio;
    private ImageView iv_send_gift;
    private LinearLayout ll_audio;
    private FrameLayout fl_audio;
    private FrameLayout fl_audio_play;
    private ImageView iv_switch;
    private ImageView iv_audio_record_anim;
    private ImageView iv_audio_record;
    private TextView tv_audio_timer;
    private TextView tv_del_audio;
    private TextView tv_send_audio;
    private TextView tv_audio_status;
    private LinearLayout ll_play_audio;
    private LinearLayout ll_del_audio;
    private ImageView iv_play_audio;
    private ImageView iv_del_audio;
    private ImageView iv_play_status;

    private View ll_quick_container;
    private RecyclerView rv_quick_message;
    private float downX;
    private float downY;
    private float moveX;
    private float moveY;
    private float upX;
    private float upY;
    private boolean touched = false; // 是否按着
    private long downTime;//按下时间
    private long upTime;  //抬起时间
    private boolean isEndRecord = false;
    private OnInputCallback mCallback;
    private String sessionId;
    private Activity mContext;
    protected AudioRecorder audioMessageHelper;
    private File mAudioFile;//语音文件
    private long mAudioLength;
    private int maxRecorder = 60;//录制最大时长
    private List<String> mQuickMsgList;
    private int currentMode = 0;
    private int audioLength = 0;
    //private EmojiPopup emojiPopup;
    private int p2pChatType;
    private CountDownTimerUtil countDownTimerUtil = new CountDownTimerUtil();

    public InputPanel(Activity context, View container, String sessionId, OnInputCallback callback,int p2pChatType) {
        this.mContext = context;
        this.container = container;
        this.mCallback = callback;
        this.sessionId = sessionId;
        EmojiManager.init(mContext);
        this.p2pChatType = p2pChatType;
        initView();
    }

    private void initView() {
        tv_send = container.findViewById(R.id.tv_send);
        tv_send.setOnClickListener(this);
        iv_choose_picture = container.findViewById(R.id.iv_choose_picture);
        iv_choose_picture.setOnClickListener(this);
        tv_take_picture = container.findViewById(R.id.tv_take_picture);
        tv_take_picture.setOnClickListener(this);
        et_content = container.findViewById(R.id.et_content);
        emoticonPickerView = container.findViewById(R.id.emoticon_picker_view);
        iv_choose_audio = container.findViewById(R.id.iv_choose_audio);
        iv_choose_audio.setOnClickListener(this);
        iv_send_gift = container.findViewById(R.id.iv_send_gift);
        iv_send_gift.setOnClickListener(this);
        // 语音
        ll_audio = container.findViewById(R.id.ll_audio);
        //录音按钮
        fl_audio = container.findViewById(R.id.fl_audio);
        //播放控制
        fl_audio_play = container.findViewById(R.id.fl_audio_play);
        fl_audio_play.setOnClickListener(this);
        iv_play_status = container.findViewById(R.id.iv_play_status);
        iv_audio_record_anim = container.findViewById(R.id.iv_audio_record_anim);
        iv_audio_record = container.findViewById(R.id.iv_audio_record);
        tv_audio_timer = container.findViewById(R.id.tv_audio_timer);
        tv_audio_status = container.findViewById(R.id.tv_audio_status);
        ll_play_audio = container.findViewById(R.id.ll_play_audio);
        ll_del_audio = container.findViewById(R.id.ll_del_audio);
        iv_play_audio = container.findViewById(R.id.iv_play_audio);
        iv_del_audio = container.findViewById(R.id.iv_del_audio);
        //删除
        tv_del_audio = container.findViewById(R.id.tv_del_audio);
        tv_del_audio.setOnClickListener(this);
        //发送
        tv_send_audio = container.findViewById(R.id.tv_send_audio);

        iv_switch = container.findViewById(R.id.iv_switch);
        iv_switch.setOnClickListener(this);
        ll_quick_container = container.findViewById(R.id.ll_quick_container);
        rv_quick_message = container.findViewById(R.id.rv_quick_message);
        rv_quick_message.setLayoutManager(new LinearLayoutManager(mContext, RecyclerView.HORIZONTAL, false));

        et_content.setFocusable(true);
        et_content.setFocusableInTouchMode(true);
        et_content.requestFocus();
        //emojiPopup = EmojiPopup.Builder.fromRootView(container).build(et_content);
        tv_send_audio.setOnClickListener(this);
        et_content.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    switchToTextLayout();
                }
                return false;
            }
        });
        et_content.addTextChangedListener(new TextWatcher() {
            private int start;
            private int count;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                this.start = start;
                this.count = count;
            }

            @Override
            public void afterTextChanged(Editable s) {
                MoonUtil.replaceEmoticons(mContext, s, start, count);
                tv_send.setVisibility(s.length() > 0 ? View.VISIBLE : View.GONE);
            }
        });

        fl_audio.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        downX = event.getX();
                        downY = event.getY();
                        touched = true;
                        downTime = System.currentTimeMillis();
                        initAudioRecord();
                        break;
                    case MotionEvent.ACTION_UP:
                        upX = event.getX();
                        upY = event.getY();
                        upTime = System.currentTimeMillis();
                        if (downX - upX > 100) {
                            isEndRecord = false;
                            onEndAudioRecord(false);
                        } else {
                            touched = false;
                            isEndRecord = true;
                            if (upTime - downTime > 1000){
                                onEndAudioRecord(isCancelled(v, event));
                            }else {
                                onEndAudioRecord(true);
                                ToastUtil.show(R.string.session_audio_short_duration_tip);
                            }
                        }
                        break;
                    case MotionEvent.ACTION_MOVE:
                        moveX = event.getX();
                        moveY = event.getY();
                        if (downX - moveX > 50) {
                            iv_play_audio.setBackgroundResource(R.mipmap.icon_play_audio_select);
                            iv_del_audio.setBackgroundResource(R.mipmap.icon_delete_audio_normal);
                            isEndRecord = false;
                        } else if (moveX - downX > 50) {
                            iv_play_audio.setBackgroundResource(R.mipmap.icon_play_audio_normal);
                            iv_del_audio.setBackgroundResource(R.mipmap.icon_delete_audio_select);
                            isEndRecord = true;
                        }
                        break;
                }
                return true;
            }
        });
        fl_audio.setLongClickable(true);

    }

    private IAudioRecordCallback iAudioRecordCallback = new IAudioRecordCallback() {
        @Override
        public void onRecordReady() {
        }

        @Override
        public void onRecordStart(File audioFile, RecordType recordType) {
            if (!touched) {
                return;
            }
            playAudioRecordAnim();
        }

        @Override
        public void onRecordSuccess(File audioFile, long audioLength, RecordType recordType) {
            mAudioFile = audioFile;
            mAudioLength = audioLength;
            if (isEndRecord) {
                if (mAudioFile == null) {
                    return;
                }
                IMMessage audioMessage = MessageBuilder.createAudioMessage(sessionId, SessionTypeEnum.P2P, mAudioFile, mAudioLength);
                if (mCallback != null) {
                    mCallback.onSendMsg(audioMessage);
                }
            }
        }

        @Override
        public void onRecordFail() {
        }

        @Override
        public void onRecordCancel() {

        }

        @Override
        public void onRecordReachedMaxTime(final int maxTime) {
            stopAudioRecordAnim();
        }
    };

    // 上滑取消录音判断
    private static boolean isCancelled(View view, MotionEvent event) {
        int[] location = new int[2];
        view.getLocationOnScreen(location);

        if (event.getRawX() < location[0] || event.getRawX() > location[0] + view.getWidth()
                || event.getRawY() < location[1] - 40) {
            return true;
        }

        return false;
    }

    private void initAudioRecord() {
        checkAudioPermission();
        audioMessageHelper = new AudioRecorder(mContext, RecordType.AAC, maxRecorder, iAudioRecordCallback);
        if (audioMessageHelper != null) {
            audioMessageHelper.startRecord();
        }
    }


    private void playAudioRecordAnim() {
        ll_play_audio.setVisibility(View.VISIBLE);
        ll_del_audio.setVisibility(View.VISIBLE);
        iv_play_audio.setBackgroundResource(R.mipmap.icon_play_audio_normal);
        iv_del_audio.setBackgroundResource(R.mipmap.icon_delete_audio_normal);
        rotateAnim();
        tv_audio_timer.setVisibility(View.VISIBLE);
        ll_play_audio.setVisibility(View.VISIBLE);
        ll_del_audio.setVisibility(View.VISIBLE);
        tv_audio_status.setVisibility(View.GONE);
        countDownTimerUtil.startTime();
        countDownTimerUtil.setAudioListener(new CountDownTimerUtil.AudioPlayListener() {
            @Override
            public void startTime(int time) {
                audioLength = time;
                tv_audio_timer.setText(time + "s");
            }

            @Override
            public void finishTime() {

            }
        });

    }

    public void rotateAnim() {
        iv_audio_record_anim.setVisibility(View.VISIBLE);
        Animation anim = new RotateAnimation(0f, 360f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        anim.setDuration(1000);
        anim.setInterpolator(new LinearInterpolator());
        anim.setRepeatCount(Animation.INFINITE);
        anim.setRepeatMode(Animation.RESTART);
        iv_audio_record_anim.startAnimation(anim);
    }

    /**
     * 重置录音状态
     */
    private void restAudio() {
        tv_audio_timer.setVisibility(View.GONE);
        tv_audio_status.setVisibility(View.VISIBLE);
        fl_audio.setVisibility(View.VISIBLE);
        fl_audio_play.setVisibility(View.GONE);
        tv_del_audio.setVisibility(View.GONE);
        tv_send_audio.setVisibility(View.GONE);
        countDownTimerUtil.releaseTimer();
        endAnim();
    }

    /**
     * 隐藏预览播放 删除
     */
    public void endAnim() {
        iv_audio_record_anim.setVisibility(View.GONE);
        iv_audio_record_anim.clearAnimation();
        ll_play_audio.setVisibility(View.GONE);
        ll_del_audio.setVisibility(View.GONE);
    }

    /**
     * 结束语音录制
     *
     * @param cancel
     */
    private void onEndAudioRecord(boolean cancel) {
        if (audioMessageHelper != null) {
            audioMessageHelper.completeRecord(cancel);
        }
        stopAudioRecordAnim();
    }

    /**
     * 结束语音录制动画
     */
    private void stopAudioRecordAnim() {
        if (isEndRecord) {
            tv_audio_timer.setVisibility(View.GONE);
            tv_audio_status.setVisibility(View.VISIBLE);
            fl_audio.setVisibility(View.VISIBLE);
            fl_audio_play.setVisibility(View.GONE);
            tv_del_audio.setVisibility(View.GONE);
            tv_send_audio.setVisibility(View.GONE);
        } else {
            tv_audio_timer.setVisibility(View.VISIBLE);
            tv_audio_status.setVisibility(View.GONE);
            fl_audio.setVisibility(View.GONE);
            fl_audio_play.setVisibility(View.VISIBLE);
            iv_play_status.setBackgroundResource(R.mipmap.icon_stop_audio);
            tv_del_audio.setVisibility(View.VISIBLE);
            tv_send_audio.setVisibility(View.VISIBLE);
        }
        countDownTimerUtil.releaseTimer();
        endAnim();
    }

    public void setSendGiftVisible(boolean isVisible) {
        if (isVisible) {
            iv_send_gift.setVisibility(View.VISIBLE);
        }else {
            iv_send_gift.setVisibility(View.GONE);
        }
    }

    public void setQuickList(List<String> quickMsgList) {
        if (mQuickMsgList != null) {
            return;
        }
        this.mQuickMsgList = quickMsgList;
        ll_quick_container.setVisibility(mQuickMsgList == null || mQuickMsgList.size() == 0 ? View.GONE : View.VISIBLE);
        QuickMessageAdapter adapter = new QuickMessageAdapter(mContext, mQuickMsgList);
        adapter.setOnItemListener(this);
        rv_quick_message.setAdapter(adapter);
        rv_quick_message.addItemDecoration(new HorizontalItemDecoration(ScreenUtil.dip2px(8)));
    }

    private AudioPlayerHelper audioPlayerHelper = new AudioPlayerHelper();

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_switch:
                if (currentMode == 0) {
                    if (p2pChatType == GIRL_CHAT) {
                        //emojiPopup.dismiss();
                    }else{
                        emoticonPickerView.setVisibility(View.GONE);
                    }
                    KeyboardHelper.showInputMethod(mContext);
                    ll_audio.setVisibility(View.GONE);
                    iv_switch.setImageResource(R.mipmap.icon_input_emoji);
                    currentMode = 1;
                } else {
                    et_content.setFocusable(true);
                    et_content.requestFocus();
                    iv_switch.setImageResource(R.mipmap.icon_input_text);
                    KeyboardHelper.hintKeyBoard(mContext);
                    if (p2pChatType == GIRL_CHAT) {
                        //emojiPopup.show();
                    }else {
                        emoticonPickerView.setVisibility(View.VISIBLE);
                        emoticonPickerView.show(InputPanel.this);
                    }
                    ll_audio.setVisibility(View.GONE);
                    currentMode = 0;
                }
                if (mCallback != null) {
                    mCallback.onInputPanelExpand();
                }
                break;
            case R.id.tv_send:
                String content = et_content.getText().toString();
                if (StringUtil.isEmpty(content)) {
                    return;
                }
                IMMessage message = MessageBuilder.createTextMessage(sessionId, SessionTypeEnum.P2P, content);
                if (mCallback != null) {
                    mCallback.onSendMsgAndSys(message);
                }
                et_content.setText("");
                break;
            case R.id.iv_choose_audio:
                switchAudioLayout();
                break;
            case R.id.tv_del_audio:
                restAudio();
                break;
            case R.id.tv_send_audio:
                if (mAudioFile == null) {
                    return;
                }
                IMMessage audioMessage = MessageBuilder.createAudioMessage(sessionId, SessionTypeEnum.P2P, mAudioFile, mAudioLength);
                if (mCallback != null) {
                    mCallback.onSendMsg(audioMessage);
                }
                restAudio();
                break;
            case R.id.fl_audio_play:
                if (mAudioFile == null) {
                    return;
                }
                audioPlayerHelper.startPlay(iv_play_status,mAudioFile.getPath(), false, false);
                audioPlayerHelper.startTimer(audioLength);
                audioPlayerHelper.setAudioListener(new AudioPlayerHelper.AudioPlayListener() {
                    @Override
                    public void startTime(int time) {
                        tv_audio_timer.setText((time == 0 ? audioLength : time) + "s");
                    }

                    @Override
                    public void finishTime() {

                    }
                });
                break;
            case R.id.iv_choose_picture:
                if (mCallback != null) {
                    mCallback.onChoosePicture();
                }
                break;
            case R.id.tv_take_picture:
                if (mCallback != null) {
                    mCallback.onTakePicture();
                }
                break;
            case R.id.iv_send_gift:
                if (mCallback != null) {
                    mCallback.sendGift();
                }
                break;
        }
    }

    //录音布局切换
    private void switchAudioLayout() {
        KeyboardHelper.hintKeyBoard(mContext);
        if (p2pChatType == GIRL_CHAT) {
            /*if (emojiPopup != null) {
                emojiPopup.dismiss();
            }*/
        }else {
            if (emoticonPickerView != null) {
                emoticonPickerView.setVisibility(View.GONE);
            }
        }
        ll_audio.setVisibility(ll_audio.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
        iv_switch.setImageResource(R.mipmap.icon_input_emoji);
        currentMode = 1;
        if (mCallback != null) {
            mCallback.onInputPanelExpand();
        }
    }

    private void switchToTextLayout() {
        if (p2pChatType == GIRL_CHAT) {
            //emojiPopup.dismiss();
        }else {
            emoticonPickerView.setVisibility(View.GONE);
        }
        ll_audio.setVisibility(View.GONE);
        iv_switch.setImageResource(R.mipmap.icon_input_emoji);
        currentMode = 1;
        if (mCallback != null) {
            mCallback.onInputPanelExpand();
        }
    }

    public void setCallback(OnInputCallback mCallback) {
        this.mCallback = mCallback;
    }

    public void hideInput() {
        KeyboardHelper.hintKeyBoard(mContext);
        if (p2pChatType == GIRL_CHAT) {
            //emojiPopup.dismiss();
        }else {
            emoticonPickerView.setVisibility(View.GONE);
        }
        ll_audio.setVisibility(View.GONE);
    }

    @Override
    public void onEmojiSelected(String key) {
        Editable mEditable = et_content.getText();
        if (key.equals("/DEL")) {
            et_content.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_DEL));
        } else {
            int start = et_content.getSelectionStart();
            int end = et_content.getSelectionEnd();
            start = (start < 0 ? 0 : start);
            end = (start < 0 ? 0 : end);
            mEditable.replace(start, end, key);
        }
    }

    @Override
    public void onStickerSelected(String category, String item) {
        Log.i("InputPanel", "onStickerSelected, category =" + category + ", sticker =" + item);
        MsgAttachment attachment = new StickerAttachment(category, item);
        IMMessage stickerMessage = MessageBuilder.createCustomMessage(sessionId, SessionTypeEnum.P2P, "贴图消息", attachment);
        if (mCallback != null) {
            mCallback.onSendMsg(stickerMessage);
        }
    }

    public void onDestroy() {
        this.mCallback = null;
        if (audioMessageHelper != null) {
            iAudioRecordCallback = null;
            audioMessageHelper.destroyAudioRecorder();
        }
        if (audioPlayerHelper != null) {
            audioPlayerHelper.release();
        }
        if (emoticonPickerView != null) {
            emoticonPickerView.setListener(null);
        }
    }

    private void checkAudioPermission() {
        List<String> lackPermissions = NERtcEx.getInstance().checkPermission(mContext);
        if (!lackPermissions.isEmpty()) {
            String[] permissions = new String[lackPermissions.size()];
            for (int i = 0; i < lackPermissions.size(); i++) {
                permissions[i] = lackPermissions.get(i);
            }
            MPermission.with(mContext).permissions(permissions).request();
        }
    }

    @Override
    public void onItemClick(int pos) {
        String msg = mQuickMsgList.get(pos);
        if (mCallback != null) {
            IMMessage message = MessageBuilder.createTextMessage(sessionId, SessionTypeEnum.P2P, msg);
            mCallback.onSendMsg(message);
        }
    }

    public interface OnInputCallback {

        void onInputPanelExpand();

        void onSendMsg(IMMessage message);

        void onSendMsgAndSys(IMMessage message);

        void onChoosePicture();

        void onTakePicture();

        void sendGift();
    }
}
