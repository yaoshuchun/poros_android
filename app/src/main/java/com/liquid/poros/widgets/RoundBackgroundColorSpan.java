package com.liquid.poros.widgets;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.text.style.ReplacementSpan;

import com.liquid.base.utils.BasicUtils;
import com.liquid.poros.R;
import com.liquid.base.utils.LogOut;
import com.liquid.poros.utils.ScreenUtil;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class RoundBackgroundColorSpan extends ReplacementSpan {

    private int bgColor;
    private int textColor;

    private Context context;

    public RoundBackgroundColorSpan(Context context, int bgColor, int textColor) {
        super();
        this.bgColor = bgColor;
        this.textColor = textColor;
        this.context = context;
    }

    @Override
    public int getSize(@NonNull Paint paint, CharSequence text, int start, int end, @Nullable Paint.FontMetricsInt fm) {
        return ((int)paint.measureText(text,start,end) + ScreenUtil.dip2px(26));
    }

    @Override
    public void draw(@NonNull Canvas canvas, CharSequence text, int start, int end, float x, int top, int y, int bottom, @NonNull Paint paint) {
        int color1 = paint.getColor();
        //背景
        paint.setColor(this.bgColor);
        canvas.drawRoundRect(new RectF(x , top + ScreenUtil.dip2px(3), x + ((int)paint.measureText(text,start,end) + ScreenUtil.dip2px(20))  , ScreenUtil.dip2px(19)) , ScreenUtil.dip2px(9),ScreenUtil.dip2px(9),paint);

//      //左边的图
        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_dengji);
        canvas.drawBitmap(getNewBitmap(bitmap, ScreenUtil.dip2px(10), ScreenUtil.dip2px(10)), ScreenUtil.dip2px(2) ,ScreenUtil.dip2px((float) 6) ,paint);
        //文字
        paint.setColor(this.textColor);
        canvas.drawText(text, start, end, x + ScreenUtil.dip2px(14), y, paint);

        paint.setColor(color1);
    }

    public Bitmap getNewBitmap(Bitmap bitmap, int newWidth ,int newHeight){
        // 获得图片的宽高.
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        // 计算缩放比例.
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // 取得想要缩放的matrix参数.
        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);
        // 得到新的图片.
        return Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
    }
}
