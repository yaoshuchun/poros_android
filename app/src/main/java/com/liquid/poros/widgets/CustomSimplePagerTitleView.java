package com.liquid.poros.widgets;

import android.content.Context;
import android.util.TypedValue;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.SimplePagerTitleView;

public class CustomSimplePagerTitleView extends SimplePagerTitleView {
    private Context mContext;
    public CustomSimplePagerTitleView(Context context) {
        super(context);
        mContext = context;
    }

    @Override
    public void onSelected(int index, int totalCount) {
        super.onSelected(index, totalCount);
        setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
    }

    @Override
    public void onDeselected(int index, int totalCount) {
        setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
        super.onDeselected(index, totalCount);
    }
}
