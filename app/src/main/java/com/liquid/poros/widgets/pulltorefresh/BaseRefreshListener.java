package com.liquid.poros.widgets.pulltorefresh;

/**
 * Created Time:　12/11/2018
 * Created by  :  jcsun
 * Desc        :
 */
public interface BaseRefreshListener {


    /**
     * 刷新
     */
    void refresh();

    /**
     * 加载更多
     */
    void loadMore();
}
