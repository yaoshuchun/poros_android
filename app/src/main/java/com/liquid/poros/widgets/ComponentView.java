package com.liquid.poros.widgets;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.liquid.base.guideview.Component;

/**
 * 引导蒙层view
 */
public class ComponentView implements Component {
    private int mResId;
    private int mAnchor;
    private int mFitPos;
    private int mXOffset;
    private int mYOffset;
    private int mHeight;
    private int mWidth;

    public ComponentView(int resid, int anchor, int fitPos, int xOffset, int yOffset) {
        this.mResId = resid;
        this.mAnchor = anchor;
        this.mFitPos = fitPos;
        this.mXOffset = xOffset;
        this.mYOffset = yOffset;
    }

    public void setmHeight(int mHeight) {
        this.mHeight = mHeight;
    }

    public void setmWidth(int mWidth) {
        this.mWidth = mWidth;
    }

    @Override
    public View getView(LayoutInflater inflater) {
        ImageView imageView = new ImageView(inflater.getContext());
        imageView.setBackgroundResource(mResId);
        return imageView;
    }

    @Override
    public int getAnchor() {
        return mAnchor;
    }

    @Override
    public int getFitPosition() {
        return mFitPos;
    }

    @Override
    public int getXOffset() {
        return mXOffset;
    }

    @Override
    public int getYOffset() {
        return mYOffset;
    }

    @Override
    public int getHeight() {
        return mHeight;
    }

    @Override
    public int getWidth() {
        return mWidth;
    }
}
