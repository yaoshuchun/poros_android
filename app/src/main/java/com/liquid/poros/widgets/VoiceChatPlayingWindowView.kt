package com.liquid.poros.widgets

import android.content.Context
import com.blankj.utilcode.util.ConvertUtils
import com.liquid.poros.R
import com.liquid.poros.databinding.WindowViewVoiceChatPlayingBinding
import com.liquid.stat.boxtracker.util.DeviceUtil

class VoiceChatPlayingWindowView(context:Context) :BaseWindowView(context) {
    var binding: WindowViewVoiceChatPlayingBinding = WindowViewVoiceChatPlayingBinding.bind(contentView!!)
    override fun contentXmlResourceId(): Int  = R.layout.window_view_voice_chat_playing

    /**
     * 初始位置X
     * @return
     */
    override fun initialPosiX(): Int {
        return DeviceUtil.getScreenWidth(mContext)
    }

    /**
     * 初始位置Y
     * @return
     */
    override fun initialPosiY(): Int {
        return DeviceUtil.getScreenHeight(mContext) - ConvertUtils.dp2px(260f)
    }
}