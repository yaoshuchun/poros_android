package com.liquid.poros.widgets;

import android.content.Context;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import com.liquid.poros.R;
import com.liquid.poros.adapter.GiftAdapter;
import com.liquid.poros.entity.MsgUserModel;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class GiftPagerAdapter extends PagerAdapter {

    private List<MsgUserModel.GiftBean> giftBeans;
    private LinkedList<View> mViews;
    private Context mContext;
    private GiftAdapter giftAdapter;
    private List<GiftAdapter> adapters;
    private OnGiftSelectedListener listener;
    private int selectPos;
    private int lastStartIndex = 0;

    public GiftPagerAdapter(List<MsgUserModel.GiftBean> giftBeans, Context context,int selectPos) {
        this.giftBeans = giftBeans;
        mViews = new LinkedList<>();
        adapters = new ArrayList<>();
        mContext = context;
        this.selectPos = selectPos;
    }

    public void setListener(OnGiftSelectedListener listener) {
        this.listener = listener;
    }

    @Override
    public int getCount() {
        return (int)Math.ceil(((float)(giftBeans.size()))/8f);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View convertView;
        ViewHolder holder;
        if (mViews.size() == 0) {
            holder = new ViewHolder();
            convertView = View.inflate(mContext, R.layout.nim_gift_gridview,null);
            holder.gridView = convertView.findViewById(R.id.gv_view);
            convertView.setTag(holder);
        }else {
            convertView = mViews.removeFirst();
            holder = (ViewHolder) convertView.getTag();
        }
        giftAdapter = new GiftAdapter(mContext,giftBeans,position * 8,selectPos);
        giftAdapter.setListener(new GiftAdapter.OnGiftSelectListener() {
            @Override
            public void select(int pos, int startIndex) {
                selectPos = pos;
                if (listener != null) {
                    listener.select(pos);
                }
                if (lastStartIndex != startIndex) {
                    lastStartIndex = startIndex;
                    container.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (adapters != null && adapters.size() > 0) {
                                for (GiftAdapter adapter : adapters) {
                                    adapter.setSelectedPos(selectPos);
                                }
                            }
                        }
                    },400);
                }
            }
        });
        adapters.add(giftAdapter);
        holder.gridView.setAdapter(giftAdapter);
        holder.gridView.setNumColumns(4);
        holder.gridView.setHorizontalSpacing(29);
        holder.gridView.setVerticalSpacing(15);
        holder.gridView.setGravity(Gravity.CENTER);
        container.addView(convertView);
        return convertView;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        View convertView = (View) object;
        container.removeView(convertView);
        mViews.add(convertView);
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }

    public class ViewHolder {
        public GridView gridView = null;
    }

    public interface OnGiftSelectedListener{
        void select(int pos);
    }

    public void notifyBlindBox(int position) {
        this.selectPos = position;
        giftAdapter.notifyDataSetChanged();
    }
}
