package com.liquid.poros.widgets;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.EditText;

import com.liquid.poros.R;

public class ClickEditTextView extends EditText {
    /**
     * EditText右侧的图标
     */
    protected Drawable mRightDrawable;

    public ClickEditTextView(Context context) {
        this(context, null);
    }

    public ClickEditTextView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ClickEditTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        mRightDrawable = getCompoundDrawables()[2];

        if (mRightDrawable == null) {
            //这里当没有设置右侧图标时您可以给它设置个默认的右侧图标，当然根据您的项目需求来
            return;
        }

        //这里当设置了右侧图标时，我们对图标做了一些自定义设置，您也可以做其他设置
        mRightDrawable.setBounds(0, 0, mRightDrawable.getIntrinsicWidth(), mRightDrawable.getIntrinsicHeight());
    }

    public void setRightPicOnclickListener(RightPicOnclickListener rightPicOnclickListener) {
        this.onRightPicClickListener = rightPicOnclickListener;
    }

    public interface RightPicOnclickListener {
        void rightPicClick();
    }

    private RightPicOnclickListener onRightPicClickListener;

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_UP) {
            if (mRightDrawable != null) {
                boolean touchable = event.getX() > (getWidth() - getTotalPaddingRight())
                        && (event.getX() < ((getWidth() - getPaddingRight())));
                if (touchable) {
                    //设置点击EditText右侧图标EditText失去焦点，
                    // 防止点击EditText右侧图标EditText获得焦点，软键盘弹出
                    setFocusableInTouchMode(false);
                    setFocusable(false);
                    //点击EditText右侧图标事件接口回调
                    if (onRightPicClickListener != null) {
                        onRightPicClickListener.rightPicClick();
                    }
                } else {
                    setFocusableInTouchMode(true);
                    setFocusable(true);
                }
            }
        }
        return super.onTouchEvent(event);
    }

    private int currentMode = 0; //0 默认是文字输入 1 表情

    public int getCurrentMode() {
        return currentMode;
    }

    public void setCurrentMode(int currentMode) {
        this.currentMode = currentMode;
        if (currentMode == 0) {
            Drawable nav_up = getResources().getDrawable(R.mipmap.icon_input_emoji);
            nav_up.setBounds(0, 0, nav_up.getMinimumWidth(), nav_up.getMinimumHeight());
            setCompoundDrawables(null, null, nav_up, null);
        } else {
            Drawable nav_up = getResources().getDrawable(R.mipmap.icon_input_text);
            nav_up.setBounds(0, 0, nav_up.getMinimumWidth(), nav_up.getMinimumHeight());
            setCompoundDrawables(null, null, nav_up, null);
        }
    }
}
