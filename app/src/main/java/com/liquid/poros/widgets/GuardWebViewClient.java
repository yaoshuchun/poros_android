package com.liquid.poros.widgets;

import android.text.TextUtils;

import com.liquid.base.utils.LogOut;
import com.liquid.poros.utils.StringUtil;
import com.tencent.smtt.export.external.interfaces.WebResourceRequest;
import com.tencent.smtt.export.external.interfaces.WebResourceResponse;
import com.tencent.smtt.sdk.WebView;
import com.tencent.smtt.sdk.WebViewClient;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;


/**
 * 微信二跳反劫持
 */
public class GuardWebViewClient extends WebViewClient {

    //这里遇到一种情况是：
    //http://i64.xxx.com/9 会 302到 http://i64.xxx.com/9/
    //http://i64.xxx.com/9/ 里有一行 <script>location.href='index.html'</script>
    //如果是webview直接请求，会更新当前网址，最终请求 http://i64.xxx.com/9/index.html
    //而拦截请求后，会导致请求 http://i64.xxx.com/index.html

    public static boolean shouldIntercept = true;
    private static final String TAG = "GuardWebViewClient";
    private static String lastCharset;
    private boolean jumpLocked;

    /**
     * 检查所有HTTP和HTTPS的GET请求
     */
    public WebResourceResponse shouldInterceptRequest2(WebView webView, WebResourceRequest webResourceRequest) {
        String scheme = webResourceRequest.getUrl().getScheme().trim();
        String method = webResourceRequest.getMethod();
        HttpURLConnection httpURLConnection;
        WebResourceResponse injectIntercept;
        Map hashMap;

        if ((scheme.equalsIgnoreCase("http") ||
                scheme.equalsIgnoreCase("https")) &&
                method.equalsIgnoreCase("get")) {
            try {
                URL url2 = new URL(webResourceRequest.getUrl().toString());
                URLConnection openConnection2 = url2.openConnection();
                if (openConnection2 == null) {
                    return super.shouldInterceptRequest(webView, webResourceRequest);
                }
                for (Map.Entry entry2 : webResourceRequest.getRequestHeaders().entrySet()) {
                    openConnection2.setRequestProperty((String) entry2.getKey(), (String) entry2.getValue());
                }
                //TO DO
                //openConnection2.setRequestProperty("x-requested-with", package_name);
                String contentType = openConnection2.getContentType();
                String mime = getMime(contentType, url2.toString());
                String charset2 = getCharset(mime, contentType);

                httpURLConnection = (HttpURLConnection) openConnection2;
                int responseCode2 = httpURLConnection.getResponseCode();
                String responseMessage2 = httpURLConnection.getResponseMessage();

                Set<String> keySet2 = httpURLConnection.getHeaderFields().keySet();
                if (!(TextUtils.isEmpty(mime) || TextUtils.isEmpty(charset2) || isBinaryRes(mime))) {
                    injectIntercept = injectIntercept(url2.toString(), mime, charset2, new WebResourceResponse(mime, charset2, httpURLConnection.getInputStream()));
                    injectIntercept.setStatusCodeAndReasonPhrase(responseCode2, responseMessage2);
                    hashMap = new HashMap();
                    for (String trim22 : keySet2) {
                        hashMap.put(trim22, httpURLConnection.getHeaderField(trim22));
                    }
                    injectIntercept.setResponseHeaders(hashMap);
                    return injectIntercept;
                } else {
                    return super.shouldInterceptRequest(webView, webResourceRequest);
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return super.shouldInterceptRequest(webView, webResourceRequest);
    }

    private String getCharset(String mime, String contentType) {
        if (contentType == null) {
            return null;
        }
        String[] split = contentType.split(";");
        if (split.length <= 1) {
            return mime != null ? ("application/javascript".equals(mime) || "application/x-javascript".equals(mime) || mime.startsWith("text")) ? lastCharset : null : null;
        } else {
            String charset = split[1];
            if (!charset.contains("=")) {
                return null;
            }
            String value = charset.substring(charset.indexOf("=") + 1);
            lastCharset = value;
            return value;
        }
    }

    private String getMime(String contentType, String url) {
        if (StringUtil.isNotEmpty(contentType)) {
            return contentType.split(";")[0];
        } else {
            if (url.contains(".js?") || url.endsWith(".js")) {
                return "application/javascript";
            } else if (url.contains(".css?") || url.endsWith(".css")) {
                return "text/css";
            } else if (url.contains(".html?") || url.endsWith(".html") ||
                    url.contains(".htm?") || url.endsWith(".htm")) {
                return "text/html";
            } else {
                return "";
            }
        }
    }

    private WebResourceResponse injectIntercept(String url, String mime, String charset, WebResourceResponse webResourceResponse) {
        InputStream byteArrayInputStream;
        InputStream data = webResourceResponse.getData();
        try {
            byte[] bArr;
            byte[] consumeInputStream = consumeInputStream(data);
            if ("application/javascript".equals(mime) || "application/x-javascript".equals(mime) || "text/javascript".equals(mime)) {
                byte[] bArr2 = consumeInputStream;
                String content = new String(bArr2, StandardCharsets.UTF_8);
                if (content.contains("weixin:")) {
                    setJumpLocked(true);
                    LogOut.debug(TAG, "## jumpLocked: " + url);
                }
                bArr = bArr2;
            } else {
                bArr = consumeInputStream;
            }
            byteArrayInputStream = new ByteArrayInputStream(bArr);
        } catch (Exception e) {
            byteArrayInputStream = data;
        }
        return new WebResourceResponse(mime, charset, byteArrayInputStream);
    }

    private boolean isBinaryRes(String str) {
        return !str.startsWith("application/javascript") && !str.startsWith("application/x-javascript") && !str.startsWith("application/json") && !str.startsWith("text");
    }

    private byte[] consumeInputStream(InputStream inputStream) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] bArr = new byte[1024];
        while (true) {
            int read = 0;
            try {
                read = inputStream.read(bArr);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (read == -1) {
                return byteArrayOutputStream.toByteArray();
            }
            byteArrayOutputStream.write(bArr, 0, read);
        }
    }

    public boolean isJumpLocked() {
        return jumpLocked;
    }

    public void setJumpLocked(boolean jumpLocked) {
        this.jumpLocked = jumpLocked;
    }

}
