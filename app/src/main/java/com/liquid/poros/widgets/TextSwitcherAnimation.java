package com.liquid.poros.widgets;

import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.widget.TextSwitcher;

public class TextSwitcherAnimation {
    private static final int DURATION = 1000;

    private TextSwitcher textSwitcher;
    private String[] texts;
    private int marker;
    private AnimationSet InAnimationSet;
    private AnimationSet OutAnimationSet;

    private int delayTime = 3000;
    private boolean mIsHtml = false;//是否是html
    private Handler handler = new Handler();
    private Runnable task = new Runnable() {
        @Override
        public void run() {
            if (mIsHtml) {
                nextHtmlView();
            } else {
                nextView();
            }
            handler.postDelayed(task, delayTime * 2);
        }
    };

    public TextSwitcherAnimation(TextSwitcher textSwitcher) {
        this.textSwitcher = textSwitcher;
    }

    public TextSwitcherAnimation(TextSwitcher textSwitcher, String[] texts) {
        this.textSwitcher = textSwitcher;
        this.texts = texts;
    }

    public void start() {
        stop();
        if (handler == null) {
            handler = new Handler();
        }
        handler.postDelayed(task, delayTime);
    }

    public void stop() {
        handler.removeCallbacks(task);
        handler.removeCallbacksAndMessages(null);
        handler = null;

    }

    public int getMarker() {
        return marker;
    }

    public TextSwitcherAnimation setTexts(String[] texts) {
        this.texts = texts;
        return this;
    }

    public TextSwitcherAnimation setHtmlTexts(boolean isHtml) {
        this.mIsHtml = isHtml;
        return this;
    }

    public void setDelayTime(int delayTime) {
        this.delayTime = delayTime;
    }

    public TextSwitcherAnimation setTextSwitcher(TextSwitcher textSwitcher) {
        this.textSwitcher = textSwitcher;
        return this;
    }

    public void create() {
        marker = 0;
        if (texts == null) {
            Log.w("TextSwitcherAnimation", "texts is null");
            return;
        }
        if (textSwitcher == null) {
            Log.w("TextSwitcherAnimation", "textSwitcher is null");
            return;
        }
        if (mIsHtml) {
            textSwitcher.setText(Html.fromHtml(texts[0]));
        } else {
            textSwitcher.setText(texts[0]);
        }
        createAnimation();
        textSwitcher.setInAnimation(InAnimationSet);
        textSwitcher.setOutAnimation(OutAnimationSet);
        start();
    }

    private void createAnimation() {
        AlphaAnimation alphaAnimation;
        TranslateAnimation translateAnimation;

        int h = textSwitcher.getHeight();
        if (h <= 0) {
            textSwitcher.measure(0, 0);
            h = textSwitcher.getMeasuredHeight();
        }

        InAnimationSet = new AnimationSet(true);
        OutAnimationSet = new AnimationSet(true);

        alphaAnimation = new AlphaAnimation(0, 1);
        translateAnimation = new TranslateAnimation(Animation.ABSOLUTE, 0, Animation.ABSOLUTE, 0,
                Animation.ABSOLUTE, h, Animation.ABSOLUTE, 0);
        InAnimationSet.addAnimation(alphaAnimation);
        InAnimationSet.addAnimation(translateAnimation);
        InAnimationSet.setDuration(DURATION);

        alphaAnimation = new AlphaAnimation(1, 0);
        translateAnimation = new TranslateAnimation(Animation.ABSOLUTE, 0, Animation.ABSOLUTE, 0,
                Animation.ABSOLUTE, 0, Animation.ABSOLUTE, -h);
        OutAnimationSet.addAnimation(alphaAnimation);
        OutAnimationSet.addAnimation(translateAnimation);
        OutAnimationSet.setDuration(DURATION);
    }

    private void nextView() {
        marker = ++marker % texts.length;
        textSwitcher.setText(texts[marker]);
    }

    private void nextHtmlView() {
        marker = ++marker % texts.length;
        textSwitcher.setText(Html.fromHtml(texts[marker]));
        textSwitcher.invalidate();
    }
}
