package com.liquid.poros.widgets;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;

import com.anbetter.danmuku.model.utils.DimensionUtil;
import com.liquid.poros.R;

import androidx.appcompat.widget.AppCompatTextView;

/**
 * Created by hiwhitley on 17-2-20.
 */

public class CPLevelProgressBar extends AppCompatTextView {

    public static final int STYLE_SINGLE_COLOR = 1;
    public static final int STYLE_TWO_COLOR = 2;
    public static final int STYLE_THREE_COLOR = 3;

    //背景画笔
    private Paint mBackgroundPaint;
    //按钮文字画笔
    private volatile Paint mTextPaint;
    private Paint paint;
    //背景颜色
    private int mBackgroundColor;
    //下载中后半部分后面背景颜色
    private int mBackgroundSecondColor;
    //文字颜色
    private int mTextColor;
    //文字大小
    private float mTextSize;
    //覆盖后颜色
    private int mTextCoverColor;

    private int mStartColor;
    private int mCenterColor;
    private int mEndColor;

    private int mProgressStyle = STYLE_SINGLE_COLOR;

    private float mButtonRadius;
    //边框宽度
    private float mBorderWidth;

    private float mProgress = -1;
    private float mToProgress;
    private float mMaxProgress;
    private float mMinProgress;
    private float mProgressPercent;

    //是否显示边框，默认是true
    private boolean showBorder;
    private RectF mBackgroundBounds;
    private RectF mProgressBounds;
    private LinearGradient mProgressTextGradient;

    //下载平滑动画
    private ValueAnimator mProgressAnimation;

    //记录当前文字
    private CharSequence mCurrentText;

    public CPLevelProgressBar(Context context) {
        this(context, null);
    }

    public CPLevelProgressBar(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CPLevelProgressBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (!isInEditMode()) {
            initAttrs(context, attrs);
            init();
            setupAnimations();
        }
    }

    private void initAttrs(Context context, AttributeSet attrs) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CPLevelProgressBar);
        try {
            mBackgroundColor = a.getColor(R.styleable.CPLevelProgressBar_progress_background_color, Color.parseColor("#3385FF"));
            mBackgroundSecondColor = a.getColor(R.styleable.CPLevelProgressBar_progress_background_second_color, Color.parseColor("#E8E8E8"));
            mButtonRadius = a.getDimension(R.styleable.CPLevelProgressBar_progress_radius, 0);
            mTextColor = a.getColor(R.styleable.CPLevelProgressBar_progress_text_color, mBackgroundColor);
            mTextSize = a.getDimension(R.styleable.CPLevelProgressBar_progress_text_size, DimensionUtil.spToPx(context, 8));
            mTextCoverColor = a.getColor(R.styleable.CPLevelProgressBar_progress_text_cover_color, Color.WHITE);
            mBorderWidth = a.getDimension(R.styleable.CPLevelProgressBar_progress_border_width, dp2px(2));
            mProgressStyle = a.getInt(R.styleable.CPLevelProgressBar_progress_style,STYLE_SINGLE_COLOR );
            mStartColor = a.getColor(R.styleable.CPLevelProgressBar_progress_start_color,Color.parseColor("#3385FF"));
            mCenterColor = a.getInt(R.styleable.CPLevelProgressBar_progress_center_color,Color.parseColor("#3385FF"));
            mEndColor = a.getInt(R.styleable.CPLevelProgressBar_progress_end_color,Color.parseColor("#3385FF"));
            showBorder = a.getBoolean(R.styleable.CPLevelProgressBar_progress_show_border,false);
        } finally {
            a.recycle();
        }
    }

    private void init() {

        mMaxProgress = 100;
        mMinProgress = 0;
        mProgress = 0;

        //设置背景画笔
        mBackgroundPaint = new Paint();
        mBackgroundPaint.setAntiAlias(true);
        mBackgroundPaint.setStyle(Paint.Style.FILL);

        paint = new Paint();
        Typeface font = Typeface.create(Typeface.DEFAULT_BOLD, Typeface.BOLD);
        paint.setTypeface(font);
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.FILL);

        //设置文字画笔
        mTextPaint = new Paint();
        mTextPaint.setAntiAlias(true);
        mTextPaint.setTextSize(mTextSize);
        //解决文字有时候画不出问题
        setLayerType(LAYER_TYPE_SOFTWARE, mTextPaint);

        invalidate();
    }

    private void setupAnimations() {
        //ProgressBar的动画
        mProgressAnimation = ValueAnimator.ofFloat(0, 1).setDuration(500);
        mProgressAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float timePercent = (float) animation.getAnimatedValue();
                mProgress = ((mToProgress - mProgress) * timePercent + mProgress);
                invalidate();
            }
        });
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!isInEditMode()) {
            drawing(canvas);
        }
    }

    private void drawing(Canvas canvas) {
        drawBackground(canvas);
        drawTextAbove(canvas);
    }

    private void drawBackground(Canvas canvas) {
        mBackgroundBounds = new RectF();
        //根据Border宽度得到Button的显示区域
        mBackgroundBounds.left = showBorder ? mBorderWidth : 0;
        mBackgroundBounds.top = showBorder ? mBorderWidth : 0;
        mBackgroundBounds.right = getMeasuredWidth() - (showBorder ? mBorderWidth : 0);
        mBackgroundBounds.bottom = getMeasuredHeight() - (showBorder ? mBorderWidth : 0);

        if (showBorder) {
            mBackgroundPaint.setStyle(Paint.Style.STROKE);
            mBackgroundPaint.setColor(mBackgroundColor);
            mBackgroundPaint.setStrokeWidth(mBorderWidth);
            canvas.drawRoundRect(mBackgroundBounds, mButtonRadius, mButtonRadius, mBackgroundPaint);
        }
        mBackgroundPaint.setStyle(Paint.Style.FILL);
        //color

        //计算当前的进度
        mProgressPercent = mProgress / (mMaxProgress + 0f);
        mBackgroundPaint.setColor(mBackgroundSecondColor);
        canvas.save();
        //画出dst图层
        canvas.drawRoundRect(mBackgroundBounds, mButtonRadius, mButtonRadius, mBackgroundPaint);

        mProgressBounds = new RectF();
        //计算 src 矩形的右边界
        float right = mBackgroundBounds.right * mProgressPercent;
        if (mProgressStyle == STYLE_SINGLE_COLOR){
            paint.setColor(mStartColor);
        }else if (mProgressStyle == STYLE_TWO_COLOR){
            LinearGradient linearGradient = new LinearGradient(0, 0, right, 0, new int[]{mStartColor,mEndColor},null, Shader.TileMode.CLAMP);
            paint.setShader(linearGradient);
        }else if (mProgressStyle == STYLE_THREE_COLOR){
            LinearGradient linearGradient = new LinearGradient(0, 0, right, 0, new int[]{mStartColor,mCenterColor,mEndColor},null, Shader.TileMode.CLAMP);            paint.setShader(linearGradient);
        }

        mProgressBounds.left = mBackgroundBounds.left;
        mProgressBounds.top = mBackgroundBounds.top;
        mProgressBounds.bottom = mBackgroundBounds.bottom;
        mProgressBounds.left = right;

        canvas.drawRoundRect(mProgressBounds, mButtonRadius, mButtonRadius, paint);
        canvas.restore();
    }

    private void drawTextAbove(Canvas canvas) {
        //计算Baseline绘制的Y坐标
        final float y = canvas.getHeight() / 2 - (mTextPaint.descent() / 2 + mTextPaint.ascent() / 2);
        if (mCurrentText == null) {
            mCurrentText = "";
        }
        final float textWidth = mTextPaint.measureText(mCurrentText.toString());

        //进度条压过距离
        float coverLength = getMeasuredWidth() * mProgressPercent;
        //开始渐变指示器
        float indicator1 = getMeasuredWidth() / 2 - textWidth / 2;
        //结束渐变指示器
        float indicator2 = getMeasuredWidth() / 2 + textWidth / 2;
        //文字变色部分的距离
        float coverTextLength = textWidth / 2 - getMeasuredWidth() / 2 + coverLength;
        float textProgress = coverTextLength / textWidth;
        if (coverLength <= indicator1) {
            mTextPaint.setShader(null);
            mTextPaint.setColor(mTextColor);
        } else if (indicator1 < coverLength && coverLength <= indicator2) {
            //设置变色效果
            mProgressTextGradient = new LinearGradient((getMeasuredWidth() - textWidth) / 2, 0, (getMeasuredWidth() + textWidth) / 2, 0,
                    new int[]{mTextCoverColor, mTextColor},
                    new float[]{textProgress, textProgress + 0.001f},
                    Shader.TileMode.CLAMP);
            mTextPaint.setColor(mTextColor);
            mTextPaint.setShader(mProgressTextGradient);
        } else {
            mTextPaint.setShader(null);
            mTextPaint.setColor(mTextCoverColor);
        }
        canvas.drawText(mCurrentText.toString(), (getMeasuredWidth() - textWidth) / 2, y, mTextPaint);

    }

    /**
     * 设置当前按钮文字
     */
    public void setCurrentText(CharSequence charSequence) {
        mCurrentText = charSequence;
        invalidate();
    }

    public boolean isShowBorder() {
        return showBorder;
    }

    public void setShowBorder(boolean showBorder) {
        this.showBorder = showBorder;
    }

    public float getBorderWidth() {
        return mBorderWidth;
    }

    public void setBorderWidth(int width) {
        this.mBorderWidth = dp2px(width);
    }

    public float getProgress() {
        return mProgress;
    }

    public void setProgress(float progress) {
        if (progress == 0){
            this.mProgress = 0;
        }else if (progress < mMaxProgress/10f){
            this.mProgress = mMaxProgress / 10f;
        }else {
            this.mProgress = progress;
        }
    }

    public float getButtonRadius() {
        return mButtonRadius;
    }

    public void setButtonRadius(float buttonRadius) {
        mButtonRadius = buttonRadius;
    }

    public int getTextColor() {
        return mTextColor;
    }

    public void setTextColor(int textColor) {
        mTextColor = textColor;
    }

    public int getTextCoverColor() {
        return mTextCoverColor;
    }

    public void setTextCoverColor(int textCoverColor) {
        mTextCoverColor = textCoverColor;
    }

    public float getMinProgress() {
        return mMinProgress;
    }

    public void setMinProgress(float minProgress) {
        mMinProgress = minProgress;
    }

    public float getMaxProgress() {
        return mMaxProgress;
    }

    public void setMaxProgress(float maxProgress) {
        mMaxProgress = maxProgress;
    }

    private int dp2px(int dp) {
        float density = getContext().getResources().getDisplayMetrics().density;
        return (int) (dp * density);
    }
}
