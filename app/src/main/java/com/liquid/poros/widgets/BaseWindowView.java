package com.liquid.poros.widgets;

import android.content.ComponentCallbacks;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Point;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;

import com.liquid.base.utils.ContextUtils;
import com.liquid.base.utils.SharePreferenceUtil;
import com.liquid.poros.PorosApplication;
import com.liquid.poros.utils.DensityUtil;
import com.liquid.stat.boxtracker.util.DeviceUtil;

public abstract class BaseWindowView extends FrameLayout {

    protected final String TAG = getClass().getSimpleName();
    /**
     * 记录手指按下时在小悬浮窗的View上的横坐标的值
     */
    protected float xInView;
    /**
     * 记录手指按下时在小悬浮窗的View上的纵坐标的值
     */
    protected float yInView;
    /**
     * 记录当前手指位置在屏幕上的横坐标值
     */
    protected float xInScreen;
    /**
     * 记录当前手指位置在屏幕上的纵坐标值
     */
    protected float yInScreen;
    /**
     * 记录手指按下时在屏幕上的横坐标的值
     */
    protected float xDownInScreen;
    /**
     * 记录手指按下时在屏幕上的纵坐标的值
     */
    protected float yDownInScreen;

    protected View contentView = null;
    protected boolean isAnchoring = false;
    protected WindowManager windowManager = null;
    protected WindowManager.LayoutParams mParams = null;
    protected Context mContext;
    protected int screenHeight = 0;
    protected int screenWidth;
    protected boolean LeftOrRight = true;
    protected final String POSTION =  "_postion";
    public static final int CIRCLE_TOP = DeviceUtil.getScreenHeight(ContextUtils.getApplicationContext()) - DensityUtil.dp2px(212);//自身高度
    public int x_pos;
    public int y_pos;
    protected OnViewClickListener onViewClickListener;
    private float interceptDownInScreenX;
    private float interceptDownInScreenY;


    protected  int getPosY(){
        if (DeviceUtil.getScreenHeight(ContextUtils.getApplicationContext()) == DeviceUtil.getScreenHeight2(ContextUtils.getApplicationContext())){
            return CIRCLE_TOP + DeviceUtil.getStatusBarHeight(ContextUtils.getApplicationContext())/2;
        }else {
            return CIRCLE_TOP + DeviceUtil.getStatusBarHeight(ContextUtils.getApplicationContext());
        }
    }

    public BaseWindowView(@NonNull Context context) {
        this(context,null);
    }

    /**
     * 初始位置X
     * @return
     */
    protected int initialPosiX(){
        return 0;
    }

    /**
     * 初始位置Y
     * @return
     */
    protected int initialPosiY(){
        return DeviceUtil.getStatusBarHeight(ContextUtils.getApplicationContext());
    }

    public BaseWindowView(@NonNull Context context, String saveLocalDataKey) {
        super(context);
        if (null == context)
            return;
        if (!TextUtils.isEmpty(saveLocalDataKey)){
            this.saveLocalDataKey = saveLocalDataKey;
        }
        mContext = context;
        init();
        markPosition(initialPosiX(),initialPosiY());
    }


    public int getPositionX(){
        return  x_pos;
    }

    public int getPositionY(){
        return  y_pos;
    }

    protected String saveLocalDataKey = getClass().getSimpleName();

    public String getSaveLocalDataKey() {
        return saveLocalDataKey;
    }

    public void setSaveLocalDataKey(String saveLocalDataKey) {
        this.saveLocalDataKey = saveLocalDataKey;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (MotionEvent.ACTION_DOWN == ev.getAction()){
            interceptDownInScreenX = ev.getRawX();
            interceptDownInScreenY = ev.getRawY();
        }

        if (MotionEvent.ACTION_MOVE == ev.getAction()){
            if (Math.abs(ev.getRawX() - interceptDownInScreenX) > ViewConfiguration.get(getContext()).getScaledTouchSlop()
                    || Math.abs(ev.getRawY() - interceptDownInScreenY) > ViewConfiguration.get(getContext()).getScaledTouchSlop()){
                return true;
            }
        }

        return false;
    }

    public void markPosition(int defaultX, int defaultY){
        String position = SharePreferenceUtil.getString(SharePreferenceUtil.FILE_USER_DATA, saveLocalDataKey +POSTION,"-1@-1");
        //兼容原来的版本。
        if (position.equals("-1@-1")){
            position = SharePreferenceUtil.getString(SharePreferenceUtil.FILE_USER_DATA, TAG +POSTION,"-1@-1");
        }
        String[] temps = position.split("@");
        int x = Integer.parseInt(temps[0]);
        int y = Integer.parseInt(temps[1]);
        if(defaultX > 0 && defaultY > 0){
            x_pos = defaultX;
            y_pos = defaultY;
        }else{
            x_pos = x;
            y_pos = y;
        }
    }
    
    public abstract int contentXmlResourceId();

    protected void init() {
        try {
            windowManager = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
            screenHeight = DeviceUtil.getScreenHeight(ContextUtils.getApplicationContext());
            screenWidth = DeviceUtil.getScreenWidth(ContextUtils.getApplicationContext());
            LayoutInflater inflater = LayoutInflater.from(mContext);
            contentView = inflater.inflate(mContext.getResources().getLayout(contentXmlResourceId()), null);
            addView(contentView);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        }

        myComponentCallbacks =  new MyComponentCallbacks();
        PorosApplication.context.registerComponentCallbacks(myComponentCallbacks);
    }

    public void setParams(WindowManager.LayoutParams params) {
        mParams = params;
    }

    public void postInvalidate( ){
        try {
            windowManager.updateViewLayout(BaseWindowView.this,mParams);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    boolean isFirstTouch;
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (isAnchoring || null == mParams) {
            return true;
        }
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                xInView = event.getX();
                yInView = event.getY();
                xDownInScreen = event.getRawX();
                yDownInScreen = event.getRawY();
                xInScreen = event.getRawX();
                yInScreen = event.getRawY();
                break;
            case MotionEvent.ACTION_MOVE:
                if (!isFirstTouch) {
                    xInView = event.getX();
                    yInView = event.getY();
                    xDownInScreen = event.getRawX();
                    yDownInScreen = event.getRawY();
                    xInScreen = event.getRawX();
                    yInScreen = event.getRawY();
                    isFirstTouch = true;
                }else {
                    xInScreen = event.getRawX();
                    yInScreen = event.getRawY();
                    // 手指移动的时候更新小悬浮窗的位置
                    updateViewPosition();
                }
                break;
            case MotionEvent.ACTION_UP:
                isFirstTouch = false;
                if (Math.abs(xDownInScreen - xInScreen) <= ViewConfiguration.get(getContext()).getScaledTouchSlop()
                        && Math.abs(yDownInScreen - yInScreen) <= ViewConfiguration.get(getContext()).getScaledTouchSlop()) {
                    onViewClick();
                } else {
                    //吸附效果
                    anchorToSide();
                }
                break;
            default:
                break;
        }
        return true;
    }

    protected void onViewClick(){
        if (null != onViewClickListener){
            onViewClickListener.OnViewClick(this);
        }
    }

    public void anchorToSide() {
        isAnchoring = true;
        int middleX;
        int animTime = 0;
        int xDistance = 0;
        int yDistance = 0;
        if (PorosApplication.context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            middleX = mParams.x + getHeight() / 2;

            if (middleX <= getWidth() / 2) {
                LeftOrRight = true;
                xDistance = -mParams.x;
            } else if (middleX <= screenHeight / 2) {
                LeftOrRight = true;
                xDistance = -mParams.x;
            } else if (middleX >= screenHeight - getWidth() / 2) {
                LeftOrRight = false;
                xDistance = screenHeight - mParams.x - getWidth();
            } else {
                LeftOrRight = false;
                xDistance = screenHeight - mParams.x - getWidth();
            }

            if (mParams.y < 0) {
                yDistance = -mParams.y;
            } else if (mParams.y + getHeight() >= screenWidth) {
                yDistance = screenWidth - mParams.y - getHeight();
            }
            animTime = Math.abs(xDistance) > Math.abs(yDistance) ? (int) (((float) xDistance / (float) screenHeight) * 600f)
                    : (int) (((float) yDistance / (float) screenWidth) * 900f);
            this.post(new AnchorAnimRunnable(Math.abs(animTime), xDistance, yDistance, System.currentTimeMillis()));
        } else {
            middleX = mParams.x + getWidth() / 2;

            if (middleX <= getWidth() / 2) {
                LeftOrRight = true;
                xDistance = -mParams.x;
            } else if (middleX <= screenWidth / 2) {
                LeftOrRight = true;
                xDistance = -mParams.x;
            } else if (middleX >= screenWidth - getWidth() / 2) {
                LeftOrRight = false;
                xDistance = screenWidth - mParams.x - getWidth();
            } else {
                LeftOrRight = false;
                xDistance = screenWidth - mParams.x - getWidth();
            }

            if (mParams.y < 0) {
                yDistance = -mParams.y;
            } else if (mParams.y + getHeight() >= screenHeight) {
                yDistance = screenHeight - mParams.y - getHeight();
            }
            animTime = Math.abs(xDistance) > Math.abs(yDistance) ? (int) (((float) xDistance / (float) screenWidth) * 600f)
                    : (int) (((float) yDistance / (float) screenHeight) * 900f);
            this.post(new AnchorAnimRunnable(Math.abs(animTime), xDistance, yDistance, System.currentTimeMillis()));
        }
    }

    protected class AnchorAnimRunnable implements Runnable {

        protected int animTime;
        protected long currentStartTime;
        protected Interpolator interpolator;
        protected int xDistance;
        protected int yDistance;
        protected int startX;
        protected int startY;

        public AnchorAnimRunnable(int animTime, int xDistance, int yDistance, long currentStartTime) {
            this.animTime = animTime;
            this.currentStartTime = currentStartTime;
            interpolator = new AccelerateDecelerateInterpolator();
            this.xDistance = xDistance;
            this.yDistance = yDistance;
            startX = mParams.x;
            startY = mParams.y;
        }

        @Override
        public void run() {
            if (System.currentTimeMillis() >= currentStartTime + animTime) {
                isAnchoring = false;
                if(x_pos < (screenWidth / 2)){
                    LeftOrRight = true;
                }else {
                    LeftOrRight = false;
                }
                SharePreferenceUtil.saveString(SharePreferenceUtil.FILE_USER_DATA, saveLocalDataKey + POSTION, x_pos+"@"+y_pos);
                return;
            }
            if(x_pos < (screenWidth / 2)){
                LeftOrRight = true;
            }else {
                LeftOrRight = false;
            }
            float delta = interpolator.getInterpolation((System.currentTimeMillis() - currentStartTime) / (float) animTime);
            int xMoveDistance = (int) (xDistance * delta);
            int yMoveDistance = (int) (yDistance * delta);
            mParams.x = startX + xMoveDistance;
            mParams.y = startY + yMoveDistance;
            x_pos = mParams.x;
            y_pos = mParams.y;
            windowManager.updateViewLayout(BaseWindowView.this, mParams);
            BaseWindowView.this.postDelayed(this, 16);
        }
    }

    protected void updateViewPosition() {
        //增加移动误差
        mParams.x = (int) (xInScreen - xInView);
        mParams.y = (int) (yInScreen - yInView);
        x_pos = mParams.x;
        y_pos = mParams.y;
        windowManager.updateViewLayout(this, mParams);
    }

    protected int dp2px(Context context, float dp) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dp * scale + 0.5f);
    }


    public void setOnViewClickListener(OnViewClickListener onViewClickListener) {
        this.onViewClickListener = onViewClickListener;
    }


    public interface OnViewClickListener{
        void OnViewClick(BaseWindowView liveCircleWindowView);
    }

    private MyComponentCallbacks myComponentCallbacks;
    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        PorosApplication.context.unregisterComponentCallbacks(myComponentCallbacks);
    }

    class MyComponentCallbacks implements ComponentCallbacks {
        @Override
        public void onConfigurationChanged(Configuration newConfig) {

            if (windowManager == null  || mParams == null){
                return;
            }

            Point point = new Point();
            windowManager.getDefaultDisplay().getSize(point);
            if (point.x > point.y){
                screenWidth = point.y;
                screenHeight = point.x;
            }else {
                screenWidth = point.x;
                screenHeight = point.y;
            }

            int nowHeight;
            if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
                nowHeight = (y_pos * screenWidth) / screenHeight;
                if (LeftOrRight){
                    mParams.x = 0;
                }else {
                    mParams.x = screenHeight - getWidth();
                }

                mParams.y = nowHeight;

                if (mParams.y >= screenWidth - getHeight()) {
                    mParams.y = screenWidth - getHeight();
                }

            } else {
                nowHeight = (y_pos * screenHeight) / screenWidth;

                if (LeftOrRight){
                    mParams.x = 0;
                }else {
                    mParams.x = screenWidth - getWidth();
                }

                mParams.y = nowHeight;

                if (mParams.y >= screenHeight - getHeight()) {
                    mParams.y = screenHeight - getHeight();
                }
            }

            x_pos = mParams.x;
            y_pos = mParams.y;

            try {
                windowManager.updateViewLayout(BaseWindowView.this, mParams);
            }catch (Exception | Error e){
                e.printStackTrace();
            }

        }

        @Override
        public void onLowMemory() {

        }
    }
}
