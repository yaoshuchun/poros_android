package com.liquid.poros.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;

@Deprecated
public class MessageLinearLayout extends RelativeLayout {
    public boolean canChange = true;
    public boolean canDown = false;
    private float y = 0;
    private int distance = 400;

    public void setOnAction(OnAction onAction) {
        this.onAction = onAction;
    }

    private OnAction onAction;


    public MessageLinearLayout(Context context) {
        super(context);
    }

    public MessageLinearLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public MessageLinearLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {

        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
            y = ev.getY();
        }

        if (ev.getAction() == MotionEvent.ACTION_MOVE) {
            if (y - ev.getY() > 0 && canChange) {
                return true;
            }

            if (ev.getY() - y > 0 && canDown) {
                return true;
            }
        }
        return super.onInterceptTouchEvent(ev);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                break;
            case MotionEvent.ACTION_MOVE:
                if (y - event.getY() > 0 && canChange) {

                    float v1 = distance + event.getY() - y;
                    if (v1 < 0) {
                        v1 = 0.0f;
                    }
                    int i = (int) (y - event.getY() + 0.5f);
                    if (onAction != null) {
                        onAction.onMoveToTopAction(v1 / distance, i);
                    }
                }

                if (event.getY() - y > 0 && canDown) {

                    float v1 = distance + y - event.getY();
                    if (v1 < 0) {
                        v1 = 0.0f;
                    }
                    int i = (int) (event.getY() - y + 0.5f);
                    if (onAction != null) {
                        onAction.onMoveToBottomAction(1 - (v1 / distance), -i);
                    }
                }
                break;
            case MotionEvent.ACTION_UP:
                if (y - event.getY() > 0 && canChange) {
                    if (y - event.getY() > distance) {
                        if (onAction != null) {
                            canDown = true;
                            canChange = false;
                            onAction.onToTopUpCompleteAction();
                        }
                    } else {
                        if (onAction != null) {
                            onAction.onToTopUpAction();
                        }
                    }
                }

                if (event.getY() - y > 0 && canDown) {
                    if (event.getY() - y > distance) {
                        if (onAction != null) {
                            canDown = false;
                            canChange = true;
                            onAction.onToBottomUpCompleteAction();
                        }
                    } else {
                        if (onAction != null) {
                            onAction.onToBottomUpAction();
                        }
                    }
                }
                break;
        }
        return super.onTouchEvent(event);
    }

    public interface OnAction {
        void onMoveToTopAction(float alpha, int distance);

        void onToTopUpCompleteAction();

        void onToTopUpAction();

        void onMoveToBottomAction(float alpha, int distance);

        void onToBottomUpCompleteAction();

        void onToBottomUpAction();
    }

}
