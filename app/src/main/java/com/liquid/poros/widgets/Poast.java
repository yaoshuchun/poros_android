package com.liquid.poros.widgets;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.IntDef;
import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.request.RequestOptions;
import com.liquid.poros.R;
import com.liquid.poros.analytics.utils.MultiTracker;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.entity.PlayloadInfo;
import com.liquid.poros.ui.activity.user.SessionActivity;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.poros.utils.ScreenUtil;
import com.liquid.poros.utils.ViewUtils;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.HashMap;

public class Poast {
    static final String TAG = "Poast";

    public static final int LENGTH_SHORT = 0;
    public static final int LENGTH_LONG = 1;
    final Activity mContext;
    int mDuration;
    View mNextView;
    final TN mTN;

    @IntDef({LENGTH_SHORT, LENGTH_LONG})
    @Retention(RetentionPolicy.SOURCE)
    public @interface Duration {
    }

    public Poast(Activity context) {
        mContext = context;
        mTN = new TN();
    }

    public void setDuration(int duration) {
        if (mTN != null) {
            mTN.mDuration = duration;
        }
    }


    public static Poast makeText(@NonNull Activity context,
                                 @NonNull PlayloadInfo info, @Duration int duration) {

        Poast result = new Poast(context);
        LayoutInflater inflate = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflate.inflate(R.layout.view_push_toast, null);
        LinearLayout llPushContent = view.findViewById(R.id.ll_push_content);
        LinearLayout notifyMsgContent = view.findViewById(R.id.notify_msg_content);
        TextView tvTitle = view.findViewById(R.id.tv_title);
        TextView tvContent = view.findViewById(R.id.tv_content);
        ImageView iv_head = view.findViewById(R.id.iv_head);
        ImageView mic_tag = view.findViewById(R.id.mic_tag);
        RequestOptions options = new RequestOptions()
                .placeholder(R.mipmap.img_default_avatar)
                .apply(RequestOptions.bitmapTransform(new CircleCrop()))
                .override(ScreenUtil.dip2px(50), ScreenUtil.dip2px(50))
                .error(R.mipmap.img_default_avatar);
        Glide.with(context)
                .load(info.getAvatar_url())
                .apply(options)
                .into(iv_head);

        tvTitle.setText(info.getMsg_title());
        tvContent.setText(Html.fromHtml(info.getMsg_content()));

        if (info.getType() == 21) {
            tvTitle.setTextColor(Color.parseColor("#9EA4B0"));
            tvTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, 10);

            tvContent.setTextColor(Color.parseColor("#ff1d1e20"));
            tvContent.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);

        } else {
            tvContent.setTextColor(Color.parseColor("#9EA4B0"));
            tvContent.setTextSize(TypedValue.COMPLEX_UNIT_SP, 10);

            tvTitle.setTextColor(Color.parseColor("#ff1d1e20"));
            tvTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
        }
        if (info.isTeam_notice()) {
            tvContent.setTextColor(context.getResources().getColor(R.color.color_ffffff));
            tvTitle.setTextColor(context.getResources().getColor(R.color.color_ffffff));
            notifyMsgContent.setBackgroundResource(R.mipmap.bg_mic);
        }else {
            tvContent.setTextColor(context.getResources().getColor(R.color.color_A4A9B3));
            tvTitle.setTextColor(context.getResources().getColor(R.color.color_1D1C20));
            notifyMsgContent.setBackgroundResource(R.mipmap.ic_normal);
            mic_tag.setVisibility(View.GONE);
        }
        llPushContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ViewUtils.isFastClick()) {
                    return;
                }
                if (info.getType() == 1) {
                    if (info.isTeam_notice()) {
                        HashMap<String, String> map = new HashMap<>();
                        map.put(Constants.MALE_USER_ID, AccountUtil.getInstance().getUserId());
                        map.put(Constants.FEMALE_GUEST_ID,info.getAccount());
                        MultiTracker.onEvent(TrackConstants.B_CLICK_INVITE_MIC_MSG, map);
                    }
                    Bundle bundle = new Bundle();
                    bundle.putBoolean(Constants.SHOW_RECHARGE,info.isShow_recharge());
                    bundle.putBoolean(Constants.SHOW_BUY_DIALOG,info.show_buy_dialog);
                    bundle.putBoolean(Constants.IS_INVITE_MIC,info.isTeam_notice());
                    bundle.putString(Constants.SESSION_ID, info.getAccount());
                    bundle.putString(Constants.FROM, Constants.INNER_NOTICE);
                    if (info.show_buy_dialog){
                        bundle.putString(Constants.BUY_POSITION, Constants.buy_card_outline_app);
                    }
                    context.startActivity(new Intent(context, SessionActivity.class).putExtras(bundle));
                }
                result.hide();
            }
        });
        if (info != null && (info.show_recharge || info.show_buy_dialog)) {
            HashMap<String,String> params = new HashMap<>();
            params.put(Constants.POSITION,TrackConstants.POSI_APPLICATION);
            params.put(Constants.ACT_PAGE,AccountUtil.getInstance().getPageId());
            if (info.show_recharge) {
                MultiTracker.onEvent(TrackConstants.B_REMIND_TEAM_JOIN_CARD_TIME_3MIN_EXPOSURE,params);
            }else {
                MultiTracker.onEvent(TrackConstants.B_REMIND_TEAM_JOIN_CARD_TIME_END_EXPOSURE,params);
            }
        }
        result.mNextView = view;
        result.mDuration = duration;
        result.setDuration(duration);
        return result;
    }

    public void show() {
        if (mNextView == null) {
            throw new RuntimeException("setView must have been called");
        }
        TN tn = mTN;
        tn.mNextView = mNextView;
        try {
            tn.mRootView = mContext.getWindow().getDecorView().findViewById(android.R.id.content);
            tn.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void hide() {
        if (mTN != null) {
            mTN.hide();
        }
    }

    private static class TN {
        private final FrameLayout.LayoutParams mParams = new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.WRAP_CONTENT);

        private static final int SHOW = 0;
        private static final int HIDE = 1;
        private static final int CANCEL = 2;
        int mGravity = Gravity.TOP;
        FrameLayout mRootView;
        View mView;
        View mNextView;
        int mDuration;


        static final long SHORT_DURATION_TIMEOUT = 3000;
        static final long LONG_DURATION_TIMEOUT = 10000;
        Handler mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case SHOW: {
                        handleShow();
                        break;
                    }
                    case HIDE: {
                        handleHide();
                        mNextView = null;
                        break;
                    }
                    case CANCEL: {
                        handleHide();
                        mNextView = null;
                        break;
                    }
                }

            }
        };


        public void show() {
            Log.v(TAG, "SHOW: " + this);
            mHandler.obtainMessage(SHOW).sendToTarget();
        }

        public void hide() {
            Log.v(TAG, "HIDE: " + this);
            mHandler.obtainMessage(HIDE).sendToTarget();
        }


        public void cancel() {
            Log.v(TAG, "CANCEL: " + this);
            mHandler.obtainMessage(CANCEL).sendToTarget();
        }


        public void handleShow() {
            Log.v(TAG, "HANDLE SHOW: " + this + " mView=" + mView
                    + " mNextView=" + mNextView);

            if (mHandler.hasMessages(CANCEL) || mHandler.hasMessages(HIDE)) {
                return;
            }
            if (mView != mNextView) {
                handleHide();
                mView = mNextView;
                final Configuration config = mView.getContext().getResources().getConfiguration();
                final int gravity = Gravity.getAbsoluteGravity(mGravity, config.getLayoutDirection());
                mParams.gravity = gravity;

                if (mView.getParent() != null) {
                    Log.v(TAG, "REMOVE! " + mView + " in " + this);
                    mRootView.removeView(mView);
                }
                Log.v(TAG, "ADD! " + mView + " in " + this);
                try {
                    mRootView.addView(mView, mParams);
                    long duration = mDuration == Poast.LENGTH_LONG ? LONG_DURATION_TIMEOUT : SHORT_DURATION_TIMEOUT;
                    mHandler.sendEmptyMessageDelayed(HIDE, duration);
                } catch (WindowManager.BadTokenException e) {
                    /* ignore */
                }
            }
        }

        public void handleHide() {
            Log.v(TAG, "HANDLE HIDE: " + this + " mView=" + mView);
            mHandler.removeCallbacksAndMessages(HIDE);
            if (mView != null) {
                if (mView.getParent() != null) {
                    mRootView.removeView(mView);
                }
                mView = null;
            }
        }

    }

}
