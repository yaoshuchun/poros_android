package com.liquid.poros.widgets;

import android.content.Context;
import android.graphics.drawable.Animatable;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.liquid.poros.R;
import com.liquid.poros.utils.DensityUtil;
import com.liquid.stat.boxtracker.util.DeviceUtil;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;

/**
 * 小额礼包悬浮窗
 */
public class SmallGiftPackageWindowView extends BaseWindowView {
    private TextView time;
    private ConstraintLayout layoutGift;
    private ScaleAnimation scaleAnimation;

    public SmallGiftPackageWindowView(@NonNull Context context) {
        super(context);
        initView();
    }

    public SmallGiftPackageWindowView(@NonNull Context context, String saveLocalDataKey) {
        super(context, saveLocalDataKey);
        initView();
    }

    @Override
    public int contentXmlResourceId() {
        return R.layout.window_view_small_gift_package;
    }

    @Override
    protected int initialPosiX() {
        //62 自身宽度
        return DeviceUtil.getScreenWidth(mContext) - DensityUtil.dp2px(62) - DensityUtil.dp2px(6);
    }

    @Override
    protected int initialPosiY() {
        return DensityUtil.dp2px(370);
    }


    protected void initView() {
        time = findViewById(R.id.time);
        layoutGift = findViewById(R.id.layout_gift);


        scaleAnimation = new ScaleAnimation(1f, 0.95f, 1f, 0.95f);
        scaleAnimation.setDuration(730);
        scaleAnimation.setFillAfter(true);
        scaleAnimation.setFillBefore(true);
        scaleAnimation.setRepeatMode(ScaleAnimation.REVERSE);
        scaleAnimation.setRepeatCount(ScaleAnimation.INFINITE);
    }

    public void startAnimation() {
        if (layoutGift != null && scaleAnimation != null) {
            layoutGift.startAnimation(scaleAnimation);

        }
    }

    /**
     * 释放动画
     */
    public void releaseAnimation() {
        if (layoutGift != null && scaleAnimation != null) {
            layoutGift.clearAnimation();
            scaleAnimation.cancel();
        }
    }


    public void setTime(String timeTv) {
        if (time != null) {
            time.setText(String.format("(%s)", timeTv));
        }
    }

    @Override
    protected void onViewClick() {
        super.onViewClick();
    }
}
