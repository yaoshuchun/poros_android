package com.liquid.poros.widgets;

import android.graphics.Rect;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class HorizontalItemDecoration extends RecyclerView.ItemDecoration {
    private int space;

    public HorizontalItemDecoration(int space) {
        this.space = space;
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        int position = parent.getChildAdapterPosition(view);
        int totalCount = parent.getAdapter().getItemCount();
        if (position == 0) {
            outRect.left = space / 2;
            outRect.right = 0;
        } else if (position == totalCount - 1) {
            outRect.left = space / 2;
            outRect.right = space / 2;
        } else {
            outRect.left = space / 2;
            outRect.right = 0;
        }
    }

}