package com.liquid.poros.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.liquid.poros.R;
import com.liquid.poros.adapter.GiftNumSelectAdapter;
import com.liquid.poros.entity.MsgUserModel;
import java.util.List;

public class SelectGiftNumView extends LinearLayout {
    private Context context;
    private OnGiftNumItemSelectListener listener;
    private GiftNumSelectAdapter giftNumSelectAdapter;

    private RecyclerView rvAllGift;

    public SelectGiftNumView(Context context) {
        super(context);
        init(context);
    }

    public SelectGiftNumView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public SelectGiftNumView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public void setListener(OnGiftNumItemSelectListener listener) {
        this.listener = listener;
    }

    private void init(Context context) {
        this.context = context;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.gift_count_select_view,this);
        initView();
    }

    public void setData(List<MsgUserModel.GiftNums> giftNums) {
        giftNumSelectAdapter.setDatas(giftNums);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
    }

    private void initView() {
        rvAllGift = findViewById(R.id.rv_all_gift);
        giftNumSelectAdapter = new GiftNumSelectAdapter(context);
        rvAllGift.setLayoutManager(new LinearLayoutManager(context));
        rvAllGift.setAdapter(giftNumSelectAdapter);
        giftNumSelectAdapter.setListener(new GiftNumSelectAdapter.OnGiftNumSelectListener() {
            @Override
            public void giftNumSelect(int num) {
                setVisibility(GONE);
                if (listener != null) {
                    listener.numSelect(num);
                }
            }
        });
    }

    public interface OnGiftNumItemSelectListener{
        void numSelect(int num);
    }
}
