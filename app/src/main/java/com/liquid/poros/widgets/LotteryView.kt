package com.liquid.poros.widgets

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ValueAnimator
import android.content.Context
import android.util.AttributeSet
import android.view.animation.AccelerateDecelerateInterpolator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.liquid.base.event.EventCenter
import com.liquid.poros.adapter.LotteryAdapter
import com.liquid.poros.analytics.utils.MultiTracker
import com.liquid.poros.constant.Constants
import com.liquid.poros.constant.TrackConstants
import com.liquid.poros.entity.BoostLotteryListBean
import de.greenrobot.event.EventBus

/**
 * 助力奖品转盘
 */
class LotteryView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : RecyclerView(context, attrs, defStyleAttr) {
    private val animator = ValueAnimator()
    private var onDrawClick: DrawClickListener
    private val adapter: LotteryAdapter
    private var mRoomId: String? = null //房间id
    private var mBoostLotteryType: String? = "boost_cm" //抽奖类型
    private var mGuestId: String? = ""//女嘉宾id
    private var mAnchorId: String? = ""//主持人id
    private var mPos1Id: String? = ""//一号麦id

    private var lotteryList = ArrayList<BoostLotteryListBean>()

    init {
        animator.duration = 2000
        onDrawClick = object : DrawClickListener {
            override fun onClickDraw() {
                EventBus.getDefault().post(EventCenter<Any?>(Constants.EVENT_CODE_OPEN_LOTTERY))
                val params: MutableMap<String, String> = java.util.HashMap<String, String>()
                params["anchor_id"] = mAnchorId!!
                params["guest_id"] = mGuestId!!
                params["talk_user_id"] = mPos1Id!!
                if ("boost_cm" == mBoostLotteryType) {
                    //初级宝箱抽奖
                    MultiTracker.onEvent(TrackConstants.B_PRIMARY_TREASURE_BOX_START_CLICK, params)
                } else {
                    //高级宝箱抽奖
                    MultiTracker.onEvent(TrackConstants.B_HIGH_TREASURE_BOX_START_CLICK, params)
                }
            }
        }
        adapter = LotteryAdapter(context, onDrawClick)
        layoutManager = object : GridLayoutManager(context, 3) {
            override fun canScrollVertically(): Boolean {
                return false
            }
        }
    }

    fun setLotteryData(roomId: String, boostLotteryType: String, boostLotteryListBean: List<BoostLotteryListBean>, guestId: String?, anchorId: String?, pos1MicId: String?) {
        mRoomId = roomId
        mBoostLotteryType = boostLotteryType
        mGuestId = guestId
        mAnchorId = anchorId
        mPos1Id = pos1MicId
        if (!boostLotteryListBean.isNullOrEmpty()) {
            lotteryList.clear()
            lotteryList.addAll(boostLotteryListBean)
            adapter.update(lotteryList)
            setAdapter(adapter)
        }
    }

    /**
     * 动画结束,更新中奖位置
     */
    fun updateCurrentPosition(position: Int) {
        animator.setIntValues(0, 2 * 8 + position)
        animator.interpolator = AccelerateDecelerateInterpolator()
        animator.addUpdateListener {
            val position = it.animatedValue as Int
            setCurrentPosition(position % 8)
        }
        animator.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator?) {
                super.onAnimationEnd(animation)
                setCurrentPosition(position)
                //重置中奖停止位置
                adapter?.updateSelectionPosition(-1)
                //展示中奖奖品弹窗
                mOnLotteryAnimationEndListener?.OnAnimationEnd()
            }
        })
        animator.start()
    }

    private fun setCurrentPosition(position: Int) {
        //刷新当前所在位置
        adapter.setSelectionPosition(position)
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        animator.cancel()
    }

    interface DrawClickListener {
        fun onClickDraw()
    }

    interface OnLotteryAnimationEndListener {
        fun OnAnimationEnd()
    }

    private var mOnLotteryAnimationEndListener: OnLotteryAnimationEndListener? = null

    fun setOnLotteryAnimationEndListener(onLotteryAnimationEndListener: OnLotteryAnimationEndListener?) {
        mOnLotteryAnimationEndListener = onLotteryAnimationEndListener
    }
}