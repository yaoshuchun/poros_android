package com.liquid.poros.widgets;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.liquid.poros.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CommonInputView extends RelativeLayout {
    private Context context;
    private TextView tv_year_first;
    private TextView tv_year_second;
    private TextView tv_year_third;
    private TextView tv_year_four;
    private EditText et_submit_year;
    private List<String> codes = new ArrayList<>();

    public CommonInputView(Context context) {
        super(context);
        this.context = context;
        loadView();
    }

    public CommonInputView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        loadView();
    }

    private void loadView() {
        View view = LayoutInflater.from(context).inflate(R.layout.include_submit_year, this);
        initView(view);
        initEvent();
    }

    private void initView(View view) {
        tv_year_first = (TextView) view.findViewById(R.id.tv_year_first);
        tv_year_second = (TextView) view.findViewById(R.id.tv_year_second);
        tv_year_third = (TextView) view.findViewById(R.id.tv_year_third);
        tv_year_four = (TextView) view.findViewById(R.id.tv_year_four);
        et_submit_year = (EditText) view.findViewById(R.id.et_submit_year);
        et_submit_year.requestFocus();
    }

    private void initEvent() {
        et_submit_year.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable != null && editable.length() > 0) {
                    et_submit_year.setText("");
                    if (codes.size() < 4) {
                        String data = editable.toString().trim();
                        if (data.length() >= 4) {
                            //将string转换成List
                            List<String> list = Arrays.asList(data.split(""));
                            //Arrays.asList没有实现add和remove方法，继承的AbstractList，需要将list放进java.util.ArrayList中
                            codes = new ArrayList<>(list);
                            if (codes != null && codes.size() > 4) {
                                //使用data.split("")方法会将""放进第一下标里需要去掉
                                codes.remove(0);
                            }
                        } else {
                            codes.add(data);
                        }
                        showCode();
                    }
                }
            }
        });
        // 监听验证码删除按键
        et_submit_year.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
                if (codes != null) {
                    if (keyCode == KeyEvent.KEYCODE_DEL && keyEvent.getAction() == KeyEvent.ACTION_DOWN && codes.size() > 0) {
                        codes.remove(codes.size() - 1);
                        showCode();
                        return true;
                    }
                }
                return false;
            }
        });
    }

    private void showCode() {
        String code1 = "";
        String code2 = "";
        String code3 = "";
        String code4 = "";
        if (codes.size() >= 1) {
            code1 = codes.get(0);
        }
        if (codes.size() >= 2) {
            code2 = codes.get(1);
        }
        if (codes.size() >= 3) {
            code3 = codes.get(2);
        }
        if (codes.size() >= 4) {
            code4 = codes.get(3);
        }
        tv_year_first.setText(code1);
        tv_year_second.setText(code2);
        tv_year_third.setText(code3);
        tv_year_four.setText(code4);
        callBack();
    }

    public void showEmptyCode() {
        tv_year_first.setText("");
        tv_year_second.setText("");
        tv_year_third.setText("");
        tv_year_four.setText("");
        codes.clear();
        et_submit_year.setText("");
    }

    private void callBack() {
        if (onInputListener == null) {
            return;
        }
        if (codes.size() == 4) {
            onInputListener.onSuccess(getPhoneCode());
        } else {
            onInputListener.onInput(getPhoneCode());
        }
    }

    public interface OnInputListener {
        void onSuccess(String codes);

        void onInput(String codes);
    }

    private OnInputListener onInputListener;

    public void setOnInputListener(OnInputListener onInputListener) {
        this.onInputListener = onInputListener;
    }

    public String getPhoneCode() {
        StringBuilder sb = new StringBuilder();
        for (String codes : codes) {
            sb.append(codes);
        }
        return sb.toString();
    }

}
