package com.liquid.poros.widgets;
import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.liquid.poros.R;
import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class CustomHeader extends LinearLayout {
    @BindView(R.id.tv_title)
    public TextView tv_title;

    @BindView(R.id.iv_left)
    public ImageView iv_left;

    @BindView(R.id.tv_right)
    public TextView tv_right;

    @BindView(R.id.view_title_line)
    public View view_title_line;

    public CustomHeader(Context context) {
        this(context, null);
    }

    public CustomHeader(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CustomHeader(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.CustomHeader);
        String title = typedArray.getString(R.styleable.CustomHeader_title);
        //标题横线  true 显示,false 隐藏
        boolean titleLine = typedArray.getBoolean(R.styleable.CustomHeader_title_line,true);
        typedArray.recycle();
        View view = LayoutInflater.from(context).inflate(R.layout.common_header, this);//使用this，相当于使用null，在addView
        ButterKnife.bind(view);//这里使用了butterKnife注解，比较常见的一个库
        tv_title.setText(title);
        view_title_line.setVisibility(titleLine ? View.VISIBLE : View.GONE);
        tv_right.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mOnEndTvClicker != null){
                    mOnEndTvClicker.endTvClick();
                }
            }
        });
    }

    public void setTitile(String title) {
        tv_title.setText(title);
    }

    @OnClick(R.id.iv_left)
    public void onClickLeftTV(View v) {
        if (getContext() instanceof Activity) {
            Activity activity = (Activity) getContext();
            activity.finish();
        }
    }

    private OnEndTvClicker mOnEndTvClicker;
    public interface OnEndTvClicker {
        void endTvClick();
    }

    public void setOnEndTvClicker(OnEndTvClicker onEndTvClicker){
        this.mOnEndTvClicker = onEndTvClicker;
    }

    public void showEndTv(String text){
        if(tv_right != null){
            tv_right.setText(text);
            tv_right.setVisibility(VISIBLE);
        }
    }
}
