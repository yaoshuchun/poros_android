package com.dankegongyu.customer.basic.analytics.model

import androidx.annotation.IntDef
import java.io.Serializable

/**
 * 公共属性
 */
class Properties : Serializable {

    var app_name: String = ""
    var application_id: String = ""
    var app_version: String = "" //应用版本
    var dkid: String = "" //用户的唯一标识
    var device_name: String = "" //设备名
    var device_brand: String = "" //硬件品牌
    var platform: String = "" //应用平台
    var os_version: String = "" //系统版本号
    var room_location: String = "" //用户使用app所选城市
    var user_location: String = "" //用户当前所处地理位置
    var longitude: String = "" //位置信息（经度)
    var latitude: String = "" //位置信息（维度)
    var screen_width: String = "1080" //屏幕尺寸
    var screen_height: String = "1920" //屏幕尺寸
    var operator: String = "中国电信" //运营商
    var net_type: String = "WIFI" //网络类型
    var track_type: Int = TrackType.MANUAL //采集类型 自动采集还是手动买点?  1 自动埋点 2 手动埋点
    var track_id: String = "" //uuid
    var user_id: String = "" //用户注册id
    var ip: String = "" //用户IP
    var visit_session: String = "" //访问ID
}

/**
 * 埋点类型，手动还是自动埋点
 */
@kotlin.annotation.Retention(AnnotationRetention.SOURCE)
@IntDef(TrackType.AUTO, TrackType.MANUAL)
annotation class TrackType {
    companion object {
        /**
         * 自动埋点
         */
        const val AUTO = 1
        /**
         * 手动埋点
         */
        const val MANUAL = 2
    }
}