package com.liquid.poros.analytics

import java.util.*

/**
 * 埋点事件key统一入口
 * 已经配置的信息
 * Created by yejiurui on 2020-12-18
 */
class LogConstants {

    companion object {
        const val SLS_PONIT_URL = "cn-beijing.log.aliyuncs.com"
        const val ACCESS_KEY_ID = "LTAI4GBM4QAvTJJay58Cn4U6"
        const val ACCESS_KEY_SECRET = "xyDttCel3oOEexw2CusZlZtP0HtIvZ"
        //旧的账号
//        const val PROJECT = "app-jinquan"
        //   const val PROJECT = if (Constants.isDebug) "ussd-applog-test" else "ussd-applog-prod"
//        const val LOG_STOTRE = "log-jinquan-android"

        //新的手滑账号日志仓库
        const val PROJECT = "k8s-log-jinquan"
        const val LOG_STOTRE = "jinquan-android"
    }

    //页面的code
    object Page {
        const val PAGE_START = "page_start" //进入页面
        const val PAGE_END = "page_end" //退出页面
        const val APP_MAIN = "app_main" //app主Activity
        const val APP_HOME_PAGE = "app_home_page" //首頁
        const val ROOM_LIST_PAGE = "room_list_page" //list页面
    }

    //事件的code
    object Event {
        const val EVENT_TIME = "event_time"
        const val EVENT_USER_ID = "user_id"
        const val LIST_SEARCH_NO_RESULT_CLICK = "list_search_noresult_click" //   list页搜索无结果线索提交
    }

    object EventProperty {
        const val PHONE = "phone_number"
        const val VISIBLE_ITEMS_START = "visible_items_start"
        const val VISIBLE_ITEMS_END = "visible_items_end"
        const val PAGE_SESSION = "page_session"
        const val PAGE_BEHAVIOR = "page_behavior"
        const val PAGE_FROM = "page_from"
        const val PAGE_FROM_SESSION = "page_from_session"
        const val MODULE_SESSION = "module_session"
        const val ROOM_ID = "room_id"
        const val ITEM_IDX = "item_idx"
        const val SID = "sid"
    }

    object WebPage {
        val URL_PAGE_CODE = HashMap<String, String>()
    }

}