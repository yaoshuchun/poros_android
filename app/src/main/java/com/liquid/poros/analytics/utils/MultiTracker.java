package com.liquid.poros.analytics.utils;

import com.liquid.poros.PorosApplication;
import com.liquid.poros.analytics.LogConstants;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.stat.boxtracker.core.BoxTracker;
import com.tendcloud.tenddata.TCAgent;

import java.util.HashMap;
import java.util.Map;

/**
 * 埋点上报入口类
 */
public class MultiTracker {
    private static HashMap<String, String> localClickFromMap = new HashMap<>();

    private static Object mapLock = new Object();

    public static void addLocalClickFromMap(String key, String value) {
        synchronized (mapLock) {
            localClickFromMap.put(key, value);
        }
    }

    public static String getLocalClickFromValue(String key) {
        synchronized (mapLock) {
            if (localClickFromMap.containsKey(key)) {
                return localClickFromMap.get(key);
            }
            return "undefined";
        }
    }

    public static void onEvent(String event, Map map) {
        HashMap<String,Object> tcParams = new HashMap<>();
        if (map != null) {
            tcParams.put(LogConstants.Event.EVENT_USER_ID, AccountUtil.getInstance().getUserId());
            tcParams.put(LogConstants.Event.EVENT_TIME, System.currentTimeMillis());
            tcParams.putAll(map);
        }
        TCAgent.onEvent(PorosApplication.context, event, null, tcParams);
        BoxTracker.onEvent(event, map);
    }

    public static void onEvent(String event) {
        HashMap<String,Object> tcParams = new HashMap<>();
        tcParams.put(LogConstants.Event.EVENT_USER_ID, AccountUtil.getInstance().getUserId());
        tcParams.put(LogConstants.Event.EVENT_TIME, System.currentTimeMillis());
        TCAgent.onEvent(PorosApplication.context, event, null, tcParams);

        HashMap<String,String> boxParams = new HashMap<>();
        BoxTracker.onEvent(event,boxParams);
    }

    public static void onPageResume(String pageId, Map<String, String> parameters) {
        BoxTracker.recordPageResume(pageId, parameters);
        TCAgent.onPageStart(PorosApplication.context, pageId);

    }

    public static void onPagePause(String pageId, Map<String, String> parameters) {
        BoxTracker.recordPagePause(parameters);
        TCAgent.onPageEnd(PorosApplication.context, pageId);
    }
}
