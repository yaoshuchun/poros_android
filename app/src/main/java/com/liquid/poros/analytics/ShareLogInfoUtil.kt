package com.liquid.poros.analytics

import android.text.TextUtils

/**
 * 共享信息Util
 * 用于处理所有事件一些共享数据信息
 */
object ShareLogInfoUtil {
    @JvmStatic
    var prePageCode: String? = null  //上一个页面的名称
    @JvmStatic
    var prePageSession: String? = null  //上一个页面的PageSession
    @JvmStatic
//    当前页面的PageCode,用于View中传递Log使用
    var currentPageCode: String = ""
        set(value) {
            if (!TextUtils.isEmpty(currentPageCode)) {
                prePageCode = currentPageCode
            }
            field = value
        }
    @JvmStatic
    //当前页面的PageSession,用于View中传递Log使用
    var currentPageSession: String = ""
        set(value) {
            if (!TextUtils.isEmpty(currentPageSession)) {
                prePageSession = currentPageSession
            }
            field = value
        }

//    @JvmStatic
//    //记录点击的位置
//    var clickPoint: Point? = null
//
//    @JvmStatic
//    fun initLog(): AnalyticsLog {
//        val analyticsLog = AnalyticsLog()
//        //App信息
//        analyticsLog.common_properties.app_name = GlobalContext.getAppContext().getString(R.string.app_name)
//        analyticsLog.common_properties.application_id = GlobalContext.getApplication().packageName
//        analyticsLog.common_properties.app_version = GlobalContext.getAppVersionName()
//        //设备信息
//        analyticsLog.common_properties.os_version = android.os.Build.VERSION.RELEASE
//        analyticsLog.common_properties.platform = "Android"
//        analyticsLog.common_properties.dkid = DKUtil.getDkid() //生成的设备唯一标识
//        analyticsLog.common_properties.device_name = DeviceUtils.getModel()
//        analyticsLog.common_properties.device_brand = DeviceUtils.getBrand()
//        //网络类型
//        analyticsLog.common_properties.net_type = NetworkUtil.getNetworkType()
//        //运营商
//        analyticsLog.common_properties.operator = DKUtil.getOperatorName()
//        //位置信息
//        analyticsLog.common_properties.user_location = SPLocation.getLocCity()
//        analyticsLog.common_properties.room_location = SPCity.getCachedCityName()
//        analyticsLog.common_properties.latitude = SPLocation.getLocLatitude().toString()
//        analyticsLog.common_properties.longitude = SPLocation.getLocLongitude().toString()
//
//        //屏幕信息
//        analyticsLog.common_properties.screen_width = ScreenUtil.getScreenWidth().toString()
//        analyticsLog.common_properties.screen_height = ScreenUtil.getScreenHeight().toString()
//        analyticsLog.common_properties.track_type = TrackType.MANUAL
//        analyticsLog.common_properties.track_id = UUID.randomUUID().toString()
//        //用户Id
//        analyticsLog.common_properties.user_id = SPUser.getUserId()
//        //Log时间
//        analyticsLog.time = System.currentTimeMillis()
//        return analyticsLog
//    }

}