package com.dankegongyu.customer.basic.analytics.model

import androidx.annotation.IntDef
import com.google.gson.JsonObject
import java.io.Serializable

/**
 * 预留日志格式
 * @author yejiurui
 */
class Log : Serializable {
    var common_properties: Properties = Properties()
    var time: Long = -1
    var event_type: Int = -1 //点击事件
    var page_code: String? = null //页面埋点标识
    var event_code: String? = null //事件埋点标识
    var event_properties: JsonObject? = null
}

/**
 * 事件类型
 */
@kotlin.annotation.Retention(AnnotationRetention.SOURCE)
@IntDef(EventType.VIEW, EventType.CLICK, EventType.EXPOSURE)
annotation class EventType {
    companion object {
        /**
         * 浏览
         */
        const val VIEW = 1
        /**
         * 点击
         */
        const val CLICK = 2
        /**
         * 曝光
         */
        const val EXPOSURE = 3
        /**
         * app启动
         */
        const val START = 4
        /**
         * 页面模块浏览，比如地图找房页点小区Marker弹出的BottomSheet页属于子页面，统计为模块浏览
         */
        const val VIEW_MODULE = 5
    }
}