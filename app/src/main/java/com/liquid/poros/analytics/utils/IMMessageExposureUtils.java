package com.liquid.poros.analytics.utils;

import android.util.Log;
import android.util.SparseArray;
import android.view.View;

import com.bumptech.glide.Glide;
import com.liquid.im.kit.custom.AskPTAGiftAttachment;
import com.liquid.im.kit.custom.AudioMatchAttachment;
import com.liquid.poros.adapter.SessionMessageAdapter;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.entity.CoupleRoomListInfo;
import com.liquid.poros.entity.CpListInfo;
import com.liquid.poros.entity.IMMessageImpl;
import com.liquid.poros.entity.SingleCommentBean;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.poros.utils.StringUtil;
import com.liulishuo.filedownloader.i.IFileDownloadIPCCallback;
import com.netease.nimlib.sdk.msg.attachment.AudioAttachment;
import com.netease.nimlib.sdk.msg.attachment.ImageAttachment;
import com.netease.nimlib.sdk.msg.attachment.MsgAttachment;
import com.netease.nimlib.sdk.msg.model.IMMessage;
import com.tencent.open.im.IM;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.collection.ArrayMap;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import static com.liquid.poros.constant.Constants.IS_VIP_MSG;

/**
 * 消息曝光埋点上报
 *
 * @param <T>
 */
public class IMMessageExposureUtils<T extends RecyclerView.Adapter,V> implements RecyclerView.OnChildAttachStateChangeListener {

    private RecyclerView mRecyclerView;
    private T mBaseRecyclerViewAdapter;
    private boolean needExposure = false;
    private final static long time = 600;
    private List<V> mDataList;
    private SparseArray<InnerData> timeMap = new SparseArray<>();
    private String mFeedFrom;
    private String femaleGuestId;

    public void setFemaleGuestId(String femaleGuestId) {
        this.femaleGuestId = femaleGuestId;
    }

    public IMMessageExposureUtils(@NonNull T baseRecyclerViewAdapter1, List<V> dataList) {
        this.mBaseRecyclerViewAdapter = baseRecyclerViewAdapter1;
        this.mDataList = dataList;
    }

    public void setmRecyclerView(RecyclerView recyclerView) {
        this.mRecyclerView = recyclerView;
    }

    public void setFeedFrom(String feedFrom) {
        this.mFeedFrom = feedFrom;
    }

    public void startFeedExposureStatistics() {
        if (mRecyclerView != null && !needExposure) {
            mRecyclerView.addOnChildAttachStateChangeListener(this);
            needExposure = true;
        }
    }

    public void stopFeedExposureStatistics() {
        if (mRecyclerView != null && needExposure) {
            mRecyclerView.removeOnChildAttachStateChangeListener(this);
            needExposure = false;
        }
    }

    @Override
    public void onChildViewAttachedToWindow(View view) {
        RecyclerView.ViewHolder childViewHolder = this.mRecyclerView.getChildViewHolder(view);
        if (childViewHolder != null) {
            int adapterPosition = childViewHolder.getAdapterPosition();
            if (adapterPosition >= 0 && adapterPosition < mBaseRecyclerViewAdapter.getItemCount()) {
                Object item = mDataList.get(adapterPosition);
                if (item instanceof IMMessage){
                    MsgAttachment attachment = ((IMMessage) item).getAttachment();
                    if (attachment instanceof AskPTAGiftAttachment){
                        if (timeMap.get(item.hashCode()) == null){
                            InnerData innerData = new InnerData();
                            innerData.explosureTime = System.currentTimeMillis();
                            timeMap.put(item.hashCode(), innerData);
                        }
                    }else if (attachment instanceof AudioMatchAttachment){
                        if (timeMap.get(item.hashCode()) == null){
                            InnerData innerData = new InnerData();
                            innerData.explosureTime = System.currentTimeMillis();
                            timeMap.put(item.hashCode(), innerData);
                        }
                    }else if (attachment instanceof AudioAttachment){
                        if (timeMap.get(item.hashCode()) == null){
                            InnerData innerData = new InnerData();
                            innerData.explosureTime = System.currentTimeMillis();
                            timeMap.put(item.hashCode(), innerData);
                        }
                    }else if (attachment instanceof ImageAttachment){
                        if (timeMap.get(item.hashCode()) == null){
                            InnerData innerData = new InnerData();
                            innerData.explosureTime = System.currentTimeMillis();
                            timeMap.put(item.hashCode(), innerData);
                        }
                    }
                }

            }
        }
    }

    @Override
    public void onChildViewDetachedFromWindow(View view) {
        RecyclerView.ViewHolder childViewHolder = this.mRecyclerView.getChildViewHolder(view);
        if (childViewHolder != null) {
            exposureImp(childViewHolder.getAdapterPosition());
        }
    }

    public void exposureImp(int i) {
        int size = mBaseRecyclerViewAdapter.getItemCount();
        if (i >= 0 && i < size) {
            Object userItem = mDataList.get(i);
            if (userItem == null) return;
            InnerData innerData = timeMap.get(userItem.hashCode());
            if (innerData == null) return;

            if (userItem instanceof IMMessage){
                MsgAttachment attachment = ((IMMessage) userItem).getAttachment();
                if (attachment instanceof AskPTAGiftAttachment){
                    long currentTimeMillis = System.currentTimeMillis() - innerData.explosureTime;
                    if (needExposure && !innerData.isExplosured && currentTimeMillis >= time) {
                        innerData.isExplosured = true;
                        HashMap<String, String> map = new HashMap<>();
                        map.put("female_guest_id", femaleGuestId);
                        map.put("dress_id", ((AskPTAGiftAttachment) attachment).getSuit_id());
                        MultiTracker.onEvent(TrackConstants.B_ASK_FOR_CARD_EXPOSURE, map);
                    }
                }else if(attachment instanceof AudioMatchAttachment){
                    HashMap<String, String> params = new HashMap<>();
                    params.put("event_time", String.valueOf(System.currentTimeMillis()));
                    params.put("message_id",((IMMessage) userItem).getUuid());
                    params.put("reward_type",((AudioMatchAttachment) attachment).getReward_type());
                    MultiTracker.onEvent(TrackConstants.B_MESSAGE_QUICK_MATCH_EXPOSURE, params);

                    Map<String, Object> remoteExt = ((IMMessage) userItem).getRemoteExtension();
                    if (remoteExt != null) {
                        if (remoteExt.containsKey(IS_VIP_MSG)) {
                            boolean is_vip_msg = (boolean) remoteExt.get(IS_VIP_MSG);
                            if (is_vip_msg) {
                                HashMap<String, String> audioParams = new HashMap<>();
                                audioParams.put("user_id",AccountUtil.getInstance().getUserId());
                                audioParams.put("female_guest_id",((IMMessage) userItem).getFromAccount());
                                audioParams.put("event_time", String.valueOf(System.currentTimeMillis()));
                                audioParams.put("msg_type","voice");
                                MultiTracker.onEvent(TrackConstants.B_VIP_MESSAGE_EXPOSURE, audioParams);
                            }
                        }
                    }
                }else if (attachment instanceof AudioAttachment){
                    Map<String, Object> remoteExt = ((IMMessage) userItem).getRemoteExtension();
                    if (remoteExt != null) {
                        if (remoteExt.containsKey(IS_VIP_MSG)) {
                            boolean is_vip_msg = (boolean) remoteExt.get(IS_VIP_MSG);
                            if (is_vip_msg) {
                                HashMap<String, String> audioParams = new HashMap<>();
                                audioParams.put("user_id",AccountUtil.getInstance().getUserId());
                                audioParams.put("female_guest_id",((IMMessage) userItem).getFromAccount());
                                audioParams.put("event_time", String.valueOf(System.currentTimeMillis()));
                                audioParams.put("msg_type","voice");
                                MultiTracker.onEvent(TrackConstants.B_VIP_MESSAGE_EXPOSURE, audioParams);
                            }
                        }
                    }
                } else if(attachment instanceof ImageAttachment){
                    Map<String, Object> remoteExt = ((IMMessage) userItem).getRemoteExtension();
                    if (remoteExt != null) {
                        if (remoteExt.containsKey(IS_VIP_MSG)) {
                            boolean is_vip_msg = (boolean) remoteExt.get(IS_VIP_MSG);
                            if (is_vip_msg) {
                                if(!Constants.is_sounds_vip || !SessionMessageAdapter.judge30min((IMMessage) userItem)){
                                    HashMap<String, String> params = new HashMap<>();
                                    params.put("user_id",AccountUtil.getInstance().getUserId());
                                    params.put("female_guest_id",((IMMessage) userItem).getFromAccount());
                                    params.put("event_time", String.valueOf(System.currentTimeMillis()));
                                    params.put("msg_type","image");
                                    MultiTracker.onEvent(TrackConstants.B_VIP_MESSAGE_EXPOSURE, params);
                                }
                            }
                        }
                    }

                }
            }
        }
    }

    public void calExplosure() {
        if (this.mRecyclerView != null) {
            RecyclerView.LayoutManager layoutManager = this.mRecyclerView.getLayoutManager();
            if (layoutManager instanceof LinearLayoutManager) {
                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) layoutManager;
                int findLastVisibleItemPosition = linearLayoutManager.findLastVisibleItemPosition();
                int findFirstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition();
                while (findFirstVisibleItemPosition <= findLastVisibleItemPosition) {
                    exposureImp(findFirstVisibleItemPosition);
                    findFirstVisibleItemPosition++;
                }
            }
        }
    }

    public int findFirstPos(int[] pos) {
        if (pos.length > 0) {
            return pos[0];
        }
        return -1;
    }

    public int findLastPos(int[] pos) {
        if (pos.length == 2) {
            return pos[1];
        } else if (pos.length == 1) {
            return pos[0];
        } else {
            return -1;
        }
    }

    static class InnerData {
        public long explosureTime;
        public boolean isExplosured;
        public int position;
    }
}
