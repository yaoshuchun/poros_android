package com.liquid.poros.analytics.utils;

import android.view.View;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.liquid.poros.constant.Constants;
import com.liquid.poros.entity.CoupleRoomListInfo;
import com.liquid.poros.entity.CpListInfo;
import com.liquid.poros.entity.SingleCommentBean;
import com.liquid.poros.ui.activity.live.RoomLiveActivityV2;
import com.liquid.poros.utils.AccountUtil;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * 曝光埋点上报
 * @param <T>
 * @param <V>
 */
public class ExposureUtils<T extends RecyclerView.Adapter,V extends ExposeData> implements RecyclerView.OnChildAttachStateChangeListener {

        private RecyclerView mRecyclerView;
        private T mBaseRecyclerViewAdapter;
        private boolean needExposure = false;
        private final static long time = 600;
        private ArrayList mDataList;
        private String mFeedFrom;
        public ExposureUtils(@NonNull T baseRecyclerViewAdapter1, ArrayList dataList) {
            this.mBaseRecyclerViewAdapter = baseRecyclerViewAdapter1;
            this.mDataList = dataList;
        }

        public void setmRecyclerView(RecyclerView recyclerView){
            this.mRecyclerView = recyclerView;
        }
        public void setFeedFrom(String feedFrom){
            this.mFeedFrom =feedFrom;
        }
        public void startFeedExposureStatistics() {
            if (mRecyclerView != null && !needExposure) {
                mRecyclerView.addOnChildAttachStateChangeListener(this);
                needExposure = true;
            }
        }

        public void stopFeedExposureStatistics() {
            if (mRecyclerView != null && needExposure) {
                mRecyclerView.removeOnChildAttachStateChangeListener(this);
                needExposure = false;
            }
        }

        @Override
        public void onChildViewAttachedToWindow(View view) {
            RecyclerView.ViewHolder childViewHolder = this.mRecyclerView.getChildViewHolder(view);
            if (childViewHolder != null) {
                int adapterPosition = childViewHolder.getAdapterPosition();
                if (adapterPosition >= 0 && adapterPosition < mBaseRecyclerViewAdapter.getItemCount()) {
                    Object item = mDataList.get(adapterPosition);
                    if (item != null) {
                        if (item instanceof ExposeData) {
                            V feedDataItem = (V) item;
                            feedDataItem.explosureTime = System.currentTimeMillis();
                        }
                    }
                }
            }
        }

        @Override
        public void onChildViewDetachedFromWindow(View view) {
            RecyclerView.ViewHolder childViewHolder = this.mRecyclerView.getChildViewHolder(view);
            if (childViewHolder != null) {
                exposureImp(childViewHolder.getAdapterPosition());
            }
        }

        public void exposureImp(int i) {
            int size = mBaseRecyclerViewAdapter.getItemCount();
            if (i >= 0 && i < size) {
                Object tAdapterItem = mDataList.get(i);
                if (tAdapterItem != null) {
                    if (tAdapterItem instanceof CpListInfo.UserItem) {
                        CpListInfo.UserItem userItem = (CpListInfo.UserItem) tAdapterItem;
                        long currentTimeMillis = System.currentTimeMillis() - userItem.explosureTime;
                        if (needExposure && !userItem.isExplosured && currentTimeMillis >= time) {
                            userItem.isExplosured = true;
                            if(userItem != null){
                                HashMap<String, String> map = new HashMap<>();
                                map.put("user_id", AccountUtil.getInstance().getUserId());
                                map.put("female_guest_id",userItem.getUser_id());
                                map.put("exposure_time",String.valueOf(currentTimeMillis));
                                MultiTracker.onEvent("b_square_guest_list_exposure", map);
                            }
                        }
                    }else if(tAdapterItem instanceof SingleCommentBean) {
                        SingleCommentBean commentBean = (SingleCommentBean) tAdapterItem;
                        long currentTimeMillis = System.currentTimeMillis() - commentBean.explosureTime;
                        if (needExposure && !commentBean.isExplosured && currentTimeMillis >= time) {
                            commentBean.isExplosured = true;
                            if(commentBean != null){
                                HashMap<String, String> map = new HashMap<>();
                                map.put(Constants.USER_ID,AccountUtil.getInstance().getUserId());
                                map.put(Constants.FEMALE_GUEST_ID,commentBean.user_id);
                                map.put(Constants.MOMENT_ID,commentBean.feed_id);
                                map.put("exposure_time",String.valueOf(currentTimeMillis));
                                map.put("from",mFeedFrom);
                                MultiTracker.onEvent("b_exposure_moment_on_guest_home", map);
                            }
                        }
                    }else if(tAdapterItem instanceof CoupleRoomListInfo.CoupleRoomInfo) {
                        CoupleRoomListInfo.CoupleRoomInfo roomInfo = (CoupleRoomListInfo.CoupleRoomInfo) tAdapterItem;
                        long currentTimeMillis = System.currentTimeMillis() - roomInfo.explosureTime;
                        if (needExposure && !roomInfo.isExplosured && currentTimeMillis >= time) {
                            roomInfo.isExplosured = true;
                            if(roomInfo != null){
                                HashMap<String, String> map = new HashMap<>();
                                map.put("user_id",AccountUtil.getInstance().getUserId());
                                map.put("live_room_id",roomInfo.getRoom_id());
                                if (roomInfo.getAnchor() != null) {
                                    map.put("anchor_id",roomInfo.getAnchor().getUser_id());
                                }
                                if (roomInfo.getMic_user() != null){
                                    map.put("female_guest_id",roomInfo.getMic_user().getUser_id());
                                }
                                map.put("exposure_time",String.valueOf(currentTimeMillis));
                                if (roomInfo.getRoom_type() == RoomLiveActivityV2.ROOM_TYPE_PK){
                                    map.put("room_type","pk_room");
                                }
                                MultiTracker.onEvent("b_couple_room_list_exposure", map);
                            }
                        }
                    }
                }
            }
        }

        public void calExplosure() {
            if (this.mRecyclerView != null) {
                RecyclerView.LayoutManager layoutManager = this.mRecyclerView.getLayoutManager();
                if (layoutManager instanceof LinearLayoutManager) {
                    LinearLayoutManager linearLayoutManager = (LinearLayoutManager) layoutManager;
                    int findLastVisibleItemPosition = linearLayoutManager.findLastVisibleItemPosition();
                    int findFirstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition();
                    while (findFirstVisibleItemPosition <= findLastVisibleItemPosition) {
                        exposureImp(findFirstVisibleItemPosition);
                        findFirstVisibleItemPosition++;
                    }
                }
            }
        }

    public void calDiscoverExplosure() {
        if (this.mRecyclerView != null) {
            RecyclerView.LayoutManager layoutManager = this.mRecyclerView.getLayoutManager();
            if (layoutManager instanceof GridLayoutManager) {
                GridLayoutManager gridLayoutManager = (GridLayoutManager) layoutManager;
                int findLastVisibleItemPosition = gridLayoutManager.findLastCompletelyVisibleItemPosition();
                int findFirstVisibleItemPosition = gridLayoutManager.findFirstCompletelyVisibleItemPosition();
                while (findFirstVisibleItemPosition <= findLastVisibleItemPosition && findFirstVisibleItemPosition != -1) {
                    exposureImp(findFirstVisibleItemPosition);
                    findFirstVisibleItemPosition++;
                }
            }
        }
    }

    public int findFirstPos(int[] pos) {
            if (pos.length > 0) {
                return pos[0];
            }
            return -1;
    }

    public int findLastPos(int[] pos) {
            if (pos.length == 2) {
                return pos[1];
            }else if(pos.length == 1) {
                return pos[0];
            }else {
                return -1;
            }
    }
}
