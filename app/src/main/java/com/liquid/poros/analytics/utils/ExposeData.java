package com.liquid.poros.analytics.utils;

public class ExposeData {

    public long explosureTime;
    public boolean isExplosured;
    public int position;

    public long getExplosureTime() {
        return explosureTime;
    }

    public void setExplosureTime(long explosureTime) {
        this.explosureTime = explosureTime;
    }

    public boolean isExplosured() {
        return isExplosured;
    }

    public void setExplosured(boolean explosured) {
        isExplosured = explosured;
    }
}
