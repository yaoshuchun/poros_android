package com.liquid.poros.analytics.helper

import com.alibaba.fastjson.JSONObject
import com.aliyun.sls.android.sdk.LogException
import com.aliyun.sls.android.sdk.core.callback.CompletedCallback
import com.aliyun.sls.android.sdk.model.Log
import com.aliyun.sls.android.sdk.model.LogGroup
import com.aliyun.sls.android.sdk.request.PostLogRequest
import com.aliyun.sls.android.sdk.result.PostLogResult
import com.liquid.base.utils.ContextUtils
import com.liquid.base.utils.LogOut
import com.liquid.poros.analytics.LogAnalyticsManager.logClient
import com.liquid.poros.analytics.LogConstants
import com.liquid.poros.analytics.utils.MultiTracker
import com.liquid.poros.constant.TrackConstants
import com.liquid.poros.utils.DeviceInfoUtils
import com.liquid.stat.boxtracker.constants.StaticsConfig
import com.liquid.stat.boxtracker.core.BoxTracker
import java.util.*

/**
 * box埋点初始化
 * Created by yejiurui on 2020/12/18.
 * Mail:yejiurui@liquidnetwork.com
 */
class BoxTrackerUtil {
    //初始化数据上报模块
    fun init() {
        BoxTracker.initialize(ContextUtils.getApplicationContext(), { data ->
            initGlobalParams()
            reportData(data)
        }, true)
        BoxTracker.setUploadPolicy(BoxTracker.UploadPolicy.UPLOAD_POLICY_INTERVAL)
        val map = DeviceInfoUtils.getGlobalParams()
        BoxTracker.onGlobalEvent(map)
        val bdParams = HashMap<String?, String?>()
        bdParams[TrackConstants.TO_PAGE] = TrackConstants.P_SPLASH
        MultiTracker.onEvent(TrackConstants.B_LAUNCH_APP, bdParams)
        BoxTracker.report()
    }

    private fun initGlobalParams() {
        BoxTracker.onGlobalEvent(DeviceInfoUtils.getGlobalParams())
    }

    private fun reportData(data: JSONObject) {
        LogOut.debug("box report data:$data")
        val logGroup = LogGroup("sls test", "")
        val array = data.getJSONArray(StaticsConfig.TrackerEventHardCodeParams.EVENTS_ARRAY_KEY)
        for (i in array.indices) {
            val jsonObject = array.getJSONObject(i)
            val it: Iterator<String> = jsonObject.keys.iterator()
            val log = Log()
            while (it.hasNext()) {
                val key = it.next()
                val value = jsonObject.getString(key)
                log.PutContent(key, value)
            }

            //批量解析map
            val globalParams = DeviceInfoUtils.getGlobalParams()
            val iter: Iterator<String> = globalParams.keys.iterator()
            while (iter.hasNext()) {
                val key = iter.next()
                val value = globalParams[key]
                log.PutContent(key, value)
            }
            logGroup.PutLog(log)
        }

        //log-jinquan-android
        try {
            val request = PostLogRequest(LogConstants.PROJECT, LogConstants.LOG_STOTRE, logGroup)
            logClient.asyncPostLog(request, object : CompletedCallback<PostLogRequest, PostLogResult?> {
                override fun onSuccess(request: PostLogRequest, result: PostLogResult?) {
                    LogOut.debug(request.toString())
                }

                override fun onFailure(request: PostLogRequest, exception: LogException) {
                    LogOut.debug(request.toString())
                }
            })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    companion object {
        var instance: BoxTrackerUtil? = null
            get() {
                if (field == null) {
                    field = BoxTrackerUtil()
                }
                return field
            }
            private set
    }
}