package com.liquid.poros.analytics

import android.annotation.SuppressLint
import android.content.Context
import com.aliyun.sls.android.sdk.ClientConfiguration
import com.aliyun.sls.android.sdk.LOGClient
import com.aliyun.sls.android.sdk.SLSLog
import com.aliyun.sls.android.sdk.core.auth.PlainTextAKSKCredentialProvider
import com.aliyun.sls.android.sdk.utils.IPService
import com.liquid.base.utils.ContextUtils
import com.liquid.base.utils.GlobalConfig
import com.liquid.poros.BuildConfig
import com.liquid.poros.analytics.helper.BoxTrackerUtil
import com.liquid.poros.constant.Constants
import com.liquid.poros.utils.DeviceInfoUtils
import com.tendcloud.tenddata.TCAgent
import com.umeng.commonsdk.UMConfigure
import com.umeng.commonsdk.statistics.common.DeviceConfig


/**
 * 日志分析统一工具类
 * @author yejiurui
 */
object LogAnalyticsManager {
    @SuppressLint("StaticFieldLeak")
    lateinit var logClient: LOGClient

    @JvmField
    var source_ip = "undefined"
    var isAsyncGetIp = true //是否同步获取本机IP

    /**
     * 日志相关初始化
     */
    @JvmStatic
    fun init() {
        //1.sls日志及架构初始化
        SLSClientInit()

        //2.box埋点初始化
        BoxTrackerUtil.instance?.init()

        //3.TcAgent埋点相关
        TcAgentInit()

        //4.设备信息的初始化
        DeviceInfoUtils.uploadDeviceInfo()

        //5.友盟初始化
        UMConfigure.init(ContextUtils.getApplicationContext(), if (BuildConfig.DEBUG) Constants.UMENG_TEST else Constants.UMENG_ONLINE, GlobalConfig.instance().channelName, UMConfigure.DEVICE_TYPE_PHONE, "");
        UMConfigure.setLogEnabled(BuildConfig.DEBUG);

    }

    /**
     * sls的初始化
     */
    fun SLSClientInit() {
        val credentialProvider = PlainTextAKSKCredentialProvider(LogConstants.ACCESS_KEY_ID, LogConstants.ACCESS_KEY_SECRET)
        val conf = ClientConfiguration()
        conf.connectionTimeout = 15 * 1000 // 连接超时，默认15秒
        conf.socketTimeout = 15 * 1000 // socket超时，默认15秒
        conf.maxConcurrentRequest = 5 // 最大并发请求书，默认5个
        conf.maxErrorRetry = 3 // 失败后最大重试次数，默认2次
        conf.cachable = false
        conf.connectType = ClientConfiguration.NetworkPolicy.WWAN_OR_WIFI
        SLSLog.enableLog() // log打印在控制台
        logClient = LOGClient(ContextUtils.getApplicationContext(), LogConstants.SLS_PONIT_URL, credentialProvider, conf)

        // 这里是同步获取的，init建议异步初始化
        try {
            source_ip = IPService.getInstance().syncGetIp(IPService.DEFAULT_URL)
        } catch (e: Throwable) {
        }
    }

    /**
     * TcAgent埋点初始化
     */
    fun TcAgentInit() {
        TCAgent.LOG_ON = false
        TCAgent.init(ContextUtils.getApplicationContext(), Constants.TD_ID, GlobalConfig.instance().channelName)
        //捕获异常
        TCAgent.setReportUncaughtExceptions(true);
    }

//    /**
//     * 推荐使用的方式，直接调用异步接口，通过callback 获取回调信息
//     */
//    private fun asyncUploadLog(jsonContent: String = "") {
//
//        /* 创建logGroup */
//        val logGroup = LogGroup("", source_ip)
//
//        /* 存入一条log */
//        val log = Log()
//        log.PutContent("content", jsonContent)
//
//        logGroup.PutLog(log)
//
//        try {
//            val request = PostLogRequest(PROJECT, LOG_STOTRE, logGroup)
//            logClient.asyncPostLog(request, object : CompletedCallback<PostLogRequest, PostLogResult> {
//                override fun onSuccess(request: PostLogRequest, result: PostLogResult) {
////                    if (Constants.isDebug) {
////                        Logger.json(GsonUtil.toJson(result))
////                    }
//                }
//
//                override fun onFailure(request: PostLogRequest, exception: LogException) {
////                    if (Constants.isDebug) {
////                        Logger.json(GsonUtil.toJson(exception))
////                    }
//                }
//            })
//        } catch (e: LogException) {
//            e.printStackTrace()
//        }
//    }


//
//
//    @JvmStatic
//    fun postEventLog(eventCode: String, @EventType type: Int) {
//        postEventLog(eventCode, type, ShareLogInfoUtil.currentPageCode, null)
//    }
//
//    @JvmStatic
//    fun postEventLog(eventCode: String, @EventType type: Int, properties: JsonObject? = null) {
//        postEventLog(eventCode, type, ShareLogInfoUtil.currentPageCode, properties, BasicUtils.getUUID())
//    }
//
//    @JvmStatic
//    fun postEventLog(eventCode: String, @EventType type: Int, pageCode: String) {
//        postEventLog(eventCode, type, pageCode, null)
//    }
//
//    @JvmStatic
//    fun postEventLog(eventCode: String, @EventType type: Int, pageCode: String, properties: JsonObject? = null) {
//        postEventLog(eventCode, type, pageCode, properties, BasicUtils.getUUID())
//    }
//
//    /**
//     * 发送事件log
//     * @param id eventId 事件唯一id
//     * @param eventCode 事件埋点标识
//     * @param type 事件类型  点击、浏览、曝光
//     * @param properties 事件属性自定义Json
//     * @param pageCode 页面埋点标识
//     */
//    @JvmStatic
//    fun postEventLog(eventCode: String, @EventType type: Int, pageCode: String, properties: JsonObject? = null, id: String = BasicUtils.getUUID()) {
//        try {
//            val analyticsLog = ShareLogInfoUtil.initLog()
//            analyticsLog.common_properties.visit_session = visitSession
//            analyticsLog.common_properties.ip = source_ip
//            analyticsLog.common_properties.track_id = id
//            analyticsLog.event_code = eventCode
//            analyticsLog.event_type = type
//            analyticsLog.page_code = pageCode
//            //所有事件增加page_session
//            val clickProperty = properties ?: JsonObject()
//            if (!clickProperty.has(LogConstants.EventProperty.PAGE_SESSION)) {
//                clickProperty.addProperty(LogConstants.EventProperty.PAGE_SESSION, ShareLogInfoUtil.currentPageSession)
//            }
//            if (ShareLogInfoUtil.clickPoint != null) {
//                clickProperty.addProperty("click_x", ShareLogInfoUtil.clickPoint!!.x)
//                clickProperty.addProperty("click_y", ShareLogInfoUtil.clickPoint!!.y)
//                ShareLogInfoUtil.clickPoint = null
//            }
//            analyticsLog.event_properties = clickProperty
//            postEventLog(analyticsLog)
//        } catch (e: Throwable) {
//            e.printStackTrace()
//        }
//    }
//
//
//    @JvmStatic
//    fun postEventLog(log: com.dankegongyu.customer.basic.analytics.model.Log) {
////        if (Constants.isDebug) {
////            DKLog.json(GsonUtil.toJson(log))
////        }
//        asyncUploadLog(GsonUtil.toJson(log))
//    }
//
//    @JvmStatic
//    fun getViewClickPoint(view: View?) {
//        if (view == null) {
//            return
//        }
//        val location = IntArray(2)
//        view.getLocationOnScreen(location)
//        ShareLogInfoUtil.clickPoint = Point(location[0] + view.width / 2, location[1] + view.height / 2)
//    }
//
//    /**
//     * 创建默认的PAGE_SESSION
//     */
//    @JvmStatic
//    fun getDefaultPageSession(): String {
//        return UUID.randomUUID().toString()
//    }
//
//    /**
//     * 页面浏览开始
//     */
//    @JvmStatic
//    fun pageStart(pageCode: String, pageSession: String) {
//        pageStart(pageCode, pageSession, "")
//    }
//
//    /**
//     * 页面浏览开始
//     */
//    @JvmStatic
//    fun pageStart(pageCode: String, pageSession: String, pageFrom: String? = "") {
//        pageStart(pageCode, pageSession, pageFrom, null)
//    }
//
//    /**
//     * 页面浏览开始
//     */
//    @JvmStatic
//    fun pageStart(pageCode: String, pageSession: String, properties: MutableMap<String, Any>? = null) {
//        pageStart(pageCode, pageSession, "", properties)
//    }
//
//    /**
//     * 页面浏览开始
//     */
//    @JvmStatic
//    fun pageStart(pageCode: String, pageSession: String, pageFrom: String? = "", properties: MutableMap<String, Any>? = null) {
//        var jsonObject = GsonUtil.fromJson(GsonUtil.toJson(properties), JsonObject::class.java)
//        if (jsonObject == null) {
//            jsonObject = JsonObject()
//        }
//        jsonObject.addProperty(LogConstants.EventProperty.PAGE_BEHAVIOR, LogConstants.Page.PAGE_START)
//        jsonObject.addProperty(LogConstants.EventProperty.PAGE_SESSION, pageSession)
//        jsonObject.addProperty(LogConstants.EventProperty.PAGE_FROM, pageFrom)
//        postEventLog(pageCode, EventType.VIEW, pageCode, jsonObject)
//    }
//
//    /**
//     * 页面浏览结束
//     */
//    @JvmStatic
//    fun pageEnd(pageCode: String, pageSession: String) {
//        pageEnd(pageCode, pageSession, "")
//    }
//
//    /**
//     * 页面浏览结束
//     */
//    @JvmStatic
//    fun pageEnd(pageCode: String, pageSession: String, pageFrom: String? = "") {
//        pageEnd(pageCode, pageSession, pageFrom, null)
//    }
//
//    /**
//     * 页面浏览结束
//     */
//    @JvmStatic
//    fun pageEnd(pageCode: String, pageSession: String, properties: MutableMap<String, Any>? = null) {
//        pageEnd(pageCode, pageSession, "", properties)
//    }
//
//    /**
//     * 页面浏览结束
//     */
//    @JvmStatic
//    fun pageEnd(pageCode: String, pageSession: String, pageFrom: String? = "", properties: MutableMap<String, Any>? = null) {
//        var jsonObject = GsonUtil.fromJson(GsonUtil.toJson(properties), JsonObject::class.java)
//        if (jsonObject == null) {
//            jsonObject = JsonObject()
//        }
//        jsonObject.addProperty(LogConstants.EventProperty.PAGE_BEHAVIOR, LogConstants.Page.PAGE_END)
//        jsonObject.addProperty(LogConstants.EventProperty.PAGE_SESSION, pageSession)
//        jsonObject.addProperty(LogConstants.EventProperty.PAGE_FROM, pageFrom)
//        postEventLog(pageCode, EventType.VIEW, pageCode, jsonObject)
//    }
//
//    /**
//     * 页面模块浏览事件，比如地图找房页的房源列表页或筛选页统计为模块浏览
//     */
//    @JvmStatic
//    fun moduleStart(eventCode: String, moduleSession: String, properties: MutableMap<String, Any>? = null) {
//        var jsonObject = GsonUtil.fromJson(GsonUtil.toJson(properties), JsonObject::class.java)
//        if (jsonObject == null) {
//            jsonObject = JsonObject()
//        }
//        jsonObject.addProperty(LogConstants.EventProperty.PAGE_BEHAVIOR, LogConstants.Page.PAGE_START)
//        jsonObject.addProperty(LogConstants.EventProperty.PAGE_SESSION, ShareLogInfoUtil.currentPageSession)
//        jsonObject.addProperty(LogConstants.EventProperty.MODULE_SESSION, moduleSession)
//        postEventLog(eventCode, EventType.VIEW_MODULE, jsonObject)
//    }
//
//    /**
//     * 页面模块浏览事件，比如地图找房页的房源列表页或筛选页统计为模块浏览
//     */
//    @JvmStatic
//    fun moduleEnd(eventCode: String, moduleSession: String, properties: MutableMap<String, Any>? = null) {
//        var jsonObject = GsonUtil.fromJson(GsonUtil.toJson(properties), JsonObject::class.java)
//        if (jsonObject == null) {
//            jsonObject = JsonObject()
//        }
//        jsonObject.addProperty(LogConstants.EventProperty.PAGE_BEHAVIOR, LogConstants.Page.PAGE_END)
//        jsonObject.addProperty(LogConstants.EventProperty.PAGE_SESSION, ShareLogInfoUtil.currentPageSession)
//        jsonObject.addProperty(LogConstants.EventProperty.MODULE_SESSION, moduleSession)
//        postEventLog(eventCode, EventType.VIEW_MODULE, jsonObject)
//    }
//
//    /**
//     * 点击事件
//     *
//     * @param properties 属性/参数
//     */
//    @JvmStatic
//    @JvmOverloads
//    fun click(view: View? = null, eventCode: String, properties: MutableMap<String, Any>? = null) {
//        getViewClickPoint(view)
//        val jsonObject = GsonUtil.fromJson(GsonUtil.toJson(properties), JsonObject::class.java)
//        postEventLog(eventCode, EventType.CLICK, jsonObject)
//    }
//
//    /**
//     * 点击事件
//     *
//     * @param properties 属性/参数
//     */
//    @JvmStatic
//    fun click(view: View? = null, eventCode: String, pageCode: String, properties: MutableMap<String, Any>? = null) {
//        getViewClickPoint(view)
//        val jsonObject = GsonUtil.fromJson(GsonUtil.toJson(properties), JsonObject::class.java)
//        postEventLog(eventCode, EventType.CLICK, pageCode, jsonObject)
//    }
//
//    /**
//     * 曝光事件
//     */
//    @JvmStatic
//    fun exposeRoomList(eventCode: String, recyclerView: RecyclerView?, data: List<Any>?, properties: MutableMap<String, Any>? = null) {
//        if (recyclerView != null && data != null) {
//            try {
//                val visibleItemPositions = getRecyclerViewFirstAndLastVisibleItemPosition(recyclerView)
//                val firstVisibleItemPosition = visibleItemPositions[0]
//                var lastVisibleItemPosition = visibleItemPositions[1]
//                if (lastVisibleItemPosition >= data.size) {
//                    lastVisibleItemPosition = data.size - 1
//                }
//                var firstPositionId = ""
//                var lastPositionId = firstPositionId
//                var sid = ""
//                val roomIds = StringBuilder()
//                for (i in firstVisibleItemPosition..lastVisibleItemPosition) {
//                    val item = data[i]
//                    if (item is ILogListRoom) {
//                        if (StringUtil.isEmpty(firstPositionId)) {
//                            firstPositionId = item.getItemPositionId() ?: ""
//                        }
//                        lastPositionId = item.getItemPositionId() ?: ""
//                        if (StringUtil.isEmpty(sid)) {
//                            sid = item.getItemSid() ?: ""
//                        }
//                        if (roomIds.isNotEmpty()) {
//                            roomIds.append(',')
//                        }
//                        roomIds.append(item.getItemRoomId())
//                    }
//                }
//                var jsonObject = GsonUtil.fromJson(GsonUtil.toJson(properties), JsonObject::class.java)
//                if (jsonObject == null) {
//                    jsonObject = JsonObject()
//                }
//                jsonObject.addProperty(LogConstants.EventProperty.VISIBLE_ITEMS_START, firstPositionId)
//                jsonObject.addProperty(LogConstants.EventProperty.VISIBLE_ITEMS_END, lastPositionId)
//                jsonObject.addProperty(LogConstants.EventProperty.SID, sid)
//                jsonObject.addProperty(LogConstants.EventProperty.ROOM_ID, roomIds.toString())
//                postEventLog(eventCode, EventType.EXPOSURE, jsonObject)
//            } catch (e: Throwable) {
//                e.printStackTrace()
//            }
//        }
//    }
//
//    /**
//     * 获取RecyclerView首尾可见item的position
//     */
//    private fun getRecyclerViewFirstAndLastVisibleItemPosition(recyclerView: RecyclerView?): IntArray {
//        var firstVisibleItemPosition = 0
//        var lastVisibleItemPosition = 0
//        try {
//            if (recyclerView != null) {
//                val layoutManager = recyclerView.layoutManager
//                when (layoutManager) {
//                    is StaggeredGridLayoutManager -> {
//                        val firstPositions = layoutManager.findFirstVisibleItemPositions(null)
//                        if (firstPositions != null && firstPositions.isNotEmpty()) {
//                            firstVisibleItemPosition = firstPositions[0]
//                        }
//                        val lastPositions = layoutManager.findLastVisibleItemPositions(null)
//                        if (lastPositions != null && lastPositions.isNotEmpty()) {
//                            lastVisibleItemPosition = lastPositions[0]
//                        }
//                    }
//                    is GridLayoutManager -> {
//                        firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition()
//                        lastVisibleItemPosition = layoutManager.findLastVisibleItemPosition()
//                    }
//                    is LinearLayoutManager -> {
//                        firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition()
//                        lastVisibleItemPosition = layoutManager.findLastVisibleItemPosition()
//                    }
//                }
//            }
//        } catch (e: Throwable) {
//        }
//        return intArrayOf(firstVisibleItemPosition, lastVisibleItemPosition)
//    }
//
//    @JvmStatic
//    fun parseUrlToMap(url: String?): JsonObject {
//        if (StringUtil.isEmpty(url) || !url!!.contains("?")) {
//            return JsonObject()
//        }
//        val params = JsonObject()
//        val substring = url.substring(url.indexOf("?") + 1)
//        substring.split("&").forEach {
//            params.addProperty(it.substring(0, it.indexOf("=")), it.substring(it.indexOf("=") + 1))
//        }
//        return params
//    }
}