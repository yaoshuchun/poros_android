package com.liquid.poros.sdk;

import com.liquid.base.utils.ContextUtils;
import com.netease.neliveplayer.playerkit.sdk.PlayerManager;
import com.netease.neliveplayer.playerkit.sdk.model.SDKOptions;
import com.netease.neliveplayer.proxy.config.NEPlayerConfig;

/**
 * 网易云播放器初始化
 * Created by yejiurui on 2020/12/28.
 * Mail:yejiurui@liquidnetwork.com
 */
public class NimPlayUtils {


    /**
     * 播放器 sdk初始化
     */
    public static void init() {
        SDKOptions config = new SDKOptions();
//        config.dataUploadListener = mOnDataUploadListener;
//        config.supportDecodeListener = supportDecodeListener;
        config.privateConfig = new NEPlayerConfig();
        PlayerManager.init(ContextUtils.getApplicationContext(), config);
    }


//    private NELivePlayer.OnSupportDecodeListener supportDecodeListener = new NELivePlayer.OnSupportDecodeListener() {
//
//        @Override
//        public void onSupportDecode(boolean isSupport) {
//        }
//    };
//
//
//    private NELivePlayer.OnDataUploadListener mOnDataUploadListener = new NELivePlayer.OnDataUploadListener() {
//
//        @Override
//        public boolean onDataUpload(String url, String data) {
//            return true;
//        }
//
//        @Override
//        public boolean onDocumentUpload(String url, Map<String, String> params, Map<String, String> filepaths) {
//
//            return false;
//        }
//    };

}
