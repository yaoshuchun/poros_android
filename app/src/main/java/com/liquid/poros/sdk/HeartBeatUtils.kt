package com.liquid.poros.sdk

import android.text.TextUtils
import com.blankj.utilcode.util.AppUtils
import com.liquid.base.utils.ContextUtils
import com.liquid.base.utils.GsonUtil
import com.liquid.library.retrofitx.RetrofitX
import com.liquid.poros.PorosApplication
import com.liquid.poros.analytics.utils.MultiTracker
import com.liquid.poros.http.ApiUrl
import com.liquid.poros.http.utils.postPoros
import com.liquid.poros.utils.AccountUtil
import com.liquid.poros.utils.AppConfigUtil
import com.liquid.poros.utils.RoomLiveMonitorHelper
import com.liquid.poros.utils.SystemUtils
import org.json.JSONObject
import java.util.*
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit


/**
 * 心跳上报服务
 * Created by yejiurui on 2020/12/28.
 * Mail:yejiurui@liquidnetwork.com
 */
object HeartBeatUtils {
    private var scheduledExecutorService: ScheduledExecutorService? =
        Executors.newScheduledThreadPool(3)
    private const val firstDelay: Long = 5 //默认第一次执行延迟时间
    private var period = 10L //两次间隔
    private val extensionParams = mutableMapOf<String, Any>()
    private var executing: Boolean = false //是否正在执行，如果正在执行则不再start

    //开始心跳
    @JvmStatic
    fun start() {
        if (executing) {
            return
        }
        executing = true
        if (scheduledExecutorService == null) {
            scheduledExecutorService = Executors.newScheduledThreadPool(3)
        }
        period = AppConfigUtil.getLong(AppConfigUtil.HEARTBEAT_INTERVAL, 10L)
        scheduledExecutorService?.scheduleAtFixedRate(
            { userActiveHeartbeat() },
            firstDelay,
            period,
            TimeUnit.SECONDS
        )
    }

    /**
     * 停止心跳
     */
    @JvmStatic
    fun shutdown() {
        scheduledExecutorService?.shutdownNow()
        scheduledExecutorService = null
        executing = false
    }

    /**
     * 增加心跳参数
     */
    fun addHeartBeatParams(params: Map<String, Any>) {
        extensionParams.putAll(params)
    }

    /**
     * 移除心跳参数
     */
    fun removeHeartBeatParams(key: String) {
        extensionParams.remove(key)
    }


    private fun userActiveHeartbeat() {
        if (TextUtils.isEmpty(AccountUtil.getInstance().account_token)) {
            return
        }
        trackActiveHeatBeat()
        RetrofitX.url(ApiUrl.HEART_BEAT)
            //如果APP是前台则 true 后台则 false，is_background "命名有歧义"
            .param("is_background", AppUtils.isAppForeground())
            .also {
                RoomLiveMonitorHelper.instance().faceInfo?.let { faceInfo ->
                    it.param("face_info", JSONObject(GsonUtil.toJson(faceInfo)))
                }
                if (extensionParams.isNotEmpty()) {
                    it.paramMap(extensionParams.toMap())
                }
            }.postPoros()
            .subscribe()
    }

    /**
     * 心跳埋点
     */
    private fun trackActiveHeatBeat() {
        val params: HashMap<String, String?> = HashMap<String, String?>()
        params["dbm"] = SystemUtils.getInstance().dbm.toString() + ""
        params["wifi_state"] =
            SystemUtils.getInstance().checkWifiState(ContextUtils.getApplicationContext())
        params["max_memory"] = SystemUtils.getInstance().maxMemory.toString()
        params["total_memory"] = SystemUtils.getInstance().totalMemory.toString()
        params["free_memory"] =
            SystemUtils.getInstance().freeMemory.toString()
        params["used_memory"] =
            SystemUtils.getInstance().usedMemory.toString()
        params["trace_memory"] =
            SystemUtils.getInstance().traceMemory.toString()
        params["battery"] = SystemUtils.getInstance()
            .getBattery(ContextUtils.getApplicationContext()).toString()
        params["user_id"] = AccountUtil.getInstance().userId
        params["event_time"] = System.currentTimeMillis().toString()
        MultiTracker.onEvent("active_heat_beat", params)
    }
}