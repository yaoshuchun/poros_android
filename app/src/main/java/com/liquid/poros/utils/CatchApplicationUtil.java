package com.liquid.poros.utils;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import java.util.List;

public class CatchApplicationUtil {
    /**
     * 获取APP应用列表（包含系统自带应用）
     *
     * @param context
     */
    public static String getAppList(Context context) {
        StringBuffer stringBuffer = new StringBuffer();
        PackageManager packageManager = context.getPackageManager();
        List<PackageInfo> packages = packageManager.getInstalledPackages(0);
        for (int i = 0; i < packages.size(); i++) {
            PackageInfo packageInfo = packages.get(i);
            stringBuffer.append(packageInfo.packageName + " , ");
        }
        return stringBuffer.toString();
    }

    /**
     * 获取APP应用列表（不包含系统自带应用）
     *
     * @param context
     */
    public static String getSimpleAppList(Context context) {
        StringBuffer stringBuffer = new StringBuffer();
        PackageManager packageManager = context.getPackageManager();
        List<PackageInfo> packages = packageManager.getInstalledPackages(0);
        for (int i = 0; i < packages.size(); i++) {
            PackageInfo packageInfo = packages.get(i);
            //过滤系统应用
            if ((packageInfo.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) == 0) {
                stringBuffer.append(packageInfo.packageName + " , ");
            }
        }
        return stringBuffer.toString();
    }
}
