package com.liquid.poros.utils.network;

import com.liquid.base.retrofit.monitor.HttpMonitor;
import com.liquid.base.utils.LogOut;
import com.liquid.base.utils.BaseConstants;
import com.liquid.base.utils.GlobalConfig;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.utils.AccountUtil;

import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import okio.BufferedSink;
import okio.GzipSink;
import okio.Okio;
import retrofit2.Retrofit;

/**
 * 静态内部类的方式实现单利模式，保证线程安全
 */

public class RetrofitHttpManager {
    public static final int TIMEOUT = 7000;
    public static final int READ_TIMEOUT = 7000;
    private OkHttpClient okHttpClient;
    public HttpInterface httpInterface;

    private static class RetrofitHolder {
        public static RetrofitHttpManager instance = new RetrofitHttpManager();
    }

    private RetrofitHttpManager() {
        initOkHttpClient();
        initRetrofit();
    }

    /**
     * 取消所有请求请求
     */
    public void cancelAll() {
        if (okHttpClient == null) {
            return;
        }
        for (Call call : okHttpClient.dispatcher().queuedCalls()) {
            call.cancel();
        }
        for (Call call : okHttpClient.dispatcher().runningCalls()) {
            call.cancel();
        }
    }


    public static RetrofitHttpManager getInstance() {
        return RetrofitHolder.instance;
    }

    public void initOkHttpClient() {
        if (okHttpClient != null) return;
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .connectTimeout(TIMEOUT, TimeUnit.MILLISECONDS)
                .readTimeout(READ_TIMEOUT, TimeUnit.MILLISECONDS)
                .addInterceptor(new GzipRequestInterceptor())
                .addInterceptor(new HttpHeaderInterceptor())
                .addInterceptor(new UrlCommonParamsInterceptor())
                .retryOnConnectionFailure(true);
        if (GlobalConfig.LOG_ENABLE) {
            builder.addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY));
        }
        try {
            builder.addInterceptor(new HttpMonitor());
        } catch (Exception e) {
            e.printStackTrace();
        }
        okHttpClient = builder.build();
    }

    public void forceInitOkHttpClient() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .connectTimeout(TIMEOUT, TimeUnit.MILLISECONDS)
                .readTimeout(READ_TIMEOUT, TimeUnit.MILLISECONDS)
                .addInterceptor(new GzipRequestInterceptor())
                .addInterceptor(new HttpHeaderInterceptor())
                .addInterceptor(new UrlCommonParamsInterceptor())
                .retryOnConnectionFailure(true);
        if (GlobalConfig.LOG_ENABLE) {
            builder.addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY));
        }
        try {
            builder.addInterceptor(new HttpMonitor());
        } catch (Exception e) {
            e.printStackTrace();
        }
        okHttpClient = builder.build();
    }


    private void initRetrofit() {
        httpInterface = new Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(Constants.BASE_URL)
                .build()
                .create(HttpInterface.class);
    }

    /**
     * This interceptor compresses the HTTP request body. Many webservers can't handle this!
     */
    final class GzipRequestInterceptor implements Interceptor {
        @Override
        public Response intercept(Chain chain) throws IOException {
            Request originalRequest = chain.request();
            if (originalRequest.body() == null || originalRequest.header("Content-Encoding") != null) {
                return chain.proceed(originalRequest);
            }

            Request compressedRequest = originalRequest.newBuilder()
                    .header("Content-Encoding", "gzip")
                    .method(originalRequest.method(), originalRequest.body())
                    .build();
            return chain.proceed(compressedRequest);
        }

        private RequestBody gzip(final RequestBody body) {
            return new RequestBody() {
                @Override
                public MediaType contentType() {
                    return body.contentType();
                }

                @Override
                public long contentLength() {
                    return -1; // We don't know the compressed length in advance!
                }

                @Override
                public void writeTo(BufferedSink sink) throws IOException {
                    BufferedSink gzipSink = Okio.buffer(new GzipSink(sink));
                    body.writeTo(gzipSink);
                    gzipSink.close();
                }
            };
        }
    }

    class HttpHeaderInterceptor implements Interceptor {
        @Override
        public Response intercept(Chain chain) throws IOException {
            Request request = chain.request()
                    .newBuilder()
//                    .addHeader("User-Agent", AppConfig.instance().userAgent)
                    .addHeader("Accept-Charset", "UTF-8")
//                    .addHeader("Accept-Encoding", "gzip")//官方解释：添加自己的 Accept-Encoding 头信息时, OkHttp会认为您要自己处理解压缩步骤. 删除这个头信息后, OkHttp 会自动处理加头信息和解压缩的步骤.
                    .addHeader("Content-Type", "application/json;charset=utf-8")
                    .addHeader("Connection", "keep-alive")
                    .addHeader("Accept", "*/*")
//                    .addHeader("Cookie", AppBoxCookie.getCookie())
                    .build();
            return chain.proceed(request);
        }
    }

    class UrlCommonParamsInterceptor implements Interceptor {

        @Override
        public Response intercept(Chain chain) throws IOException {
            Request oldRequest = chain.request();
            String path = oldRequest.url().url().getPath();
            String expiredTime = "" + System.currentTimeMillis() / 1000 + 300;
            // 添加新的参数
            HttpUrl oldUrl = oldRequest.url();
            HttpUrl.Builder authorizedUrlBuilder = oldUrl
                    .newBuilder()
                    .scheme(oldUrl.scheme())
                    .host(oldUrl.host());

            //TO url verifycation
            String ran = UUID.randomUUID().toString();
            if (oldUrl.url().toString().contains("liquidnetwork.com") ||
                    oldUrl.url().toString().contains("diffusenetwork.com")) {
                authorizedUrlBuilder
                        .addQueryParameter(BaseConstants.REMOTE_KEY.VERSION_NAME, GlobalConfig.instance().getClientVersion())
                        .addQueryParameter(BaseConstants.REMOTE_KEY.DEVICE_ID, GlobalConfig.instance().getDeviceID())
                        .addQueryParameter(BaseConstants.REMOTE_KEY.OAID, GlobalConfig.instance().getOaid())
                        .addQueryParameter(BaseConstants.REMOTE_KEY.CHANNEL_NAME, GlobalConfig.instance().getChannelName())
                        .addQueryParameter(BaseConstants.REMOTE_KEY.EXPIRED_TIME, expiredTime)
                        .addQueryParameter(BaseConstants.REMOTE_KEY.DEVICE_SERIAL, GlobalConfig.instance().getSerial())
                        .addQueryParameter(BaseConstants.REMOTE_KEY.NONCE, ran)
                        .addQueryParameter(BaseConstants.REMOTE_KEY.IMEI, GlobalConfig.instance().getIMEI())
                        .addQueryParameter(BaseConstants.REMOTE_KEY.MAC, GlobalConfig.instance().getMAC())
                        .addQueryParameter(BaseConstants.REMOTE_KEY.USER_ID, AccountUtil.getInstance().getUserId())
                        .addQueryParameter(BaseConstants.REMOTE_KEY.ANDROID_ID, GlobalConfig.instance().getANDROIDID())
                        .addQueryParameter(BaseConstants.REMOTE_KEY.BOX_PKG_NAME, GlobalConfig.instance().getPackageName())
//                        .addQueryParameter(BaseConstants.REMOTE_KEY.YID, UserAccount.getInstance().getUserYid())
                        .addQueryParameter(BaseConstants.REMOTE_KEY.PLATFORM_NAME, "Android");
                //所有的服务器请求接口统一增加yid和user_id参数
                try {
                    authorizedUrlBuilder.addQueryParameter(BaseConstants.REMOTE_KEY.LATITUDE, GlobalConfig.instance().mLatitude)
                            .addQueryParameter(BaseConstants.REMOTE_KEY.LONGITUDE, GlobalConfig.instance().mLongitude);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
//             新的请求
            Request newRequest = oldRequest.newBuilder()
                    .method(oldRequest.method(), oldRequest.body())
                    .url(authorizedUrlBuilder.build())
                    .build();
            LogOut.debug("newHomeDebug", authorizedUrlBuilder.build().url() + "");
            return chain.proceed(newRequest);
        }
    }
}
