package com.liquid.poros.utils.bundle

interface DownloadListener {
    fun onProgress(int: Int)

    fun onComplete()
}