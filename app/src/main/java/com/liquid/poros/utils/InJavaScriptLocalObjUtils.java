package com.liquid.poros.utils;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.webkit.JavascriptInterface;

import com.liquid.base.event.EventCenter;
import com.liquid.base.utils.GlobalConfig;
import com.liquid.base.utils.LogOut;
import com.liquid.base.utils.ToastOldUtil;
import com.liquid.base.utils.ToastUtil;
import com.liquid.poros.analytics.utils.MultiTracker;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.ui.activity.blindbox.BlindBoxRecordActivity;
import com.tencent.smtt.sdk.ValueCallback;
import com.tencent.smtt.sdk.WebView;

import java.lang.ref.WeakReference;

import androidx.annotation.NonNull;
import de.greenrobot.event.EventBus;

public class InJavaScriptLocalObjUtils {
    private static final String TAG = "InJavaScriptLocalObj";
    private WeakReference<Activity> normalActivity;


    private WebView mWebview;

    public InJavaScriptLocalObjUtils(@NonNull Activity activity, WebView webView) {
        this.normalActivity = new WeakReference<>(activity);
        this.mWebview = webView;
    }

    //获取手机device_id
    @JavascriptInterface
    public String getDevicesId() {
        return GlobalConfig.instance().getDeviceID();
    }

    /**
     * 盲盒介绍 - 去看看
     * 0 - 初级盲盒
     * 1 - 中级盲盒
     * 2 - 高级盲盒
     */
    @JavascriptInterface
    public void entryBlindBox(int blindBoxType) {
        EventBus.getDefault().post(new EventCenter(Constants.EVENT_CODE_ENTRY_BLIND_BOX, blindBoxType));
    }

    //盲盒介绍 - 中奖记录
    @JavascriptInterface
    public void entryBlindBoxRecord() {
        if (normalActivity == null || normalActivity.get() == null) {
            return;
        }
        Activity activity = normalActivity.get();
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                activity.startActivity(new Intent(activity, BlindBoxRecordActivity.class));
                MultiTracker.onEvent(TrackConstants.B_WIN_RECORD_ENTRANCE_CLICK);
            }
        });
    }

    @Deprecated
    public void webViewLoadJs(final String js) {
        LogOut.debug(TAG, "webviewLoadJs js=" + js);
        if (normalActivity == null) {
            return;
        }
        final Activity activity = normalActivity.get();
        if (activity != null) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (mWebview != null) {
                        String tryJs = buildTryCatchJS(js);
                        LogOut.debug(TAG, "webviewLoadJs tryJs js=" + js);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {//sdk>19才有用
                            mWebview.evaluateJavascript(tryJs, new ValueCallback<String>() {
                                @Override
                                public void onReceiveValue(String responseJson) {
                                    LogOut.debug(TAG, "webviewLoadJs responseJson =" + responseJson);
                                }
                            });
                        } else {//sdk<19后,通过prompt来获取
                            mWebview.loadUrl(tryJs);
                        }
                    }
                }
            });
        }

    }

    @Deprecated
    //构建一个try catch的js脚本
    private String buildTryCatchJS(String js) {
        StringBuilder sb = new StringBuilder();
        sb.append("javascript:try{");
        sb.append(js);
        sb.append("}catch(e){console.warn(e)}");
        LogOut.debug(TAG, "buildTryCatchInjectJS js=" + js);
        return sb.toString();
    }


}
