package com.liquid.poros.utils

import com.liquid.base.utils.ContextUtils
import com.liquid.poros.constant.FileDownloadConstant
import com.liquid.poros.utils.LottieAnimationUtils
import com.tonyodev.fetch2.*
import com.tonyodev.fetch2core.Downloader
import com.tonyodev.fetch2core.FetchLogger
import com.tonyodev.fetch2okhttp.OkHttpDownloader
import java.io.File
import kotlin.random.Random

/**
 * 文件下载工具类
 */
object FileDownloadUtil {
    val parentPath = FileDownloadConstant.rootPathAnimation
    private val fetch = Fetch.getInstance(FetchConfiguration.Builder(ContextUtils.getApplicationContext())
            .setLogger(FetchLogger(true,"动画下载"))
            .enableLogging(true)
            .setDownloadConcurrentLimit(5)
            .setAutoRetryMaxAttempts(3)
            .setHttpDownloader(OkHttpDownloader(Downloader.FileDownloaderType.PARALLEL))
            .setProgressReportingInterval(1000)
            .build())

    /**
     * 下载监听
     */
    class DownloadListener(private val groupId : Int,private val num:Int,private val callback :((Boolean) -> Unit) ?) : AbstractFetchListener(){
        private var downloaded: Int = 0 //已下载的数量
        override fun onCompleted(download: Download) {
            if (download.group != groupId) {
                return
            }
            LottieAnimationUtils.unZip(download.file, parentPath)
            downloadCount(true)
        }

        override fun onError(download: Download, error: Error, throwable: Throwable?) {
            if (download.group != groupId) {
                return
            }
            downloadCount(false)
        }

        private val downloadCount: (Boolean) -> Unit = {
            downloaded++
            if (downloaded == num) {
                callback?.invoke(it)
                fetch.removeListener(this)
            }
        }
    }

    fun downloadList(pathList: List<String>, priority: Priority = Priority.NORMAL, callback: ((Boolean) -> Unit)? = null) {
        val groupId = Random.nextInt()
        pathList.filter {
            //本地文件不存在的才下载
            !File(parentPath + File.separator + it).exists()
        }.flatMap { path ->
            arrayListOf(Request("${FileDownloadConstant.BaseConfig.GIFT_PATH}/$path" + ".zip", parentPath + "/" + path + ".zip").also {
                it.groupId = groupId
                it.priority = priority
            })
        }.apply {
            if (size == 0) {
                callback?.invoke(true)
                return@apply
            }
            fetch.addListener(DownloadListener(groupId, size, callback)).enqueue(this)
        }
    }

    /**
     * 下载单个文件
     */
    fun download(path: String, callback: ((Boolean) -> Unit)? = null) {
        val groupId = Random.nextInt()
        fetch.addListener(DownloadListener(groupId, 1, callback)).enqueue(Request("${FileDownloadConstant.BaseConfig.GIFT_PATH}/$path" + ".zip", "$parentPath/$path.zip").also { it.groupId = groupId })
    }
}