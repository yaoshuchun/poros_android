package com.liquid.poros.utils.network;

import com.liquid.poros.utils.StringUtil;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public abstract class HttpCallback implements Callback<ResponseBody> {
    @Override
    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
        String result = null;
        try {
            result = response.body().string();
        } catch (Exception e) {
            e.printStackTrace();
            OnFailed(-1, "请求失败，请稍后再试");
        }
        if (StringUtil.isEmpty(result) && !response.isSuccessful()) {
            OnFailed(-1, "请求失败，请稍后再试");
        } else {
            OnSucceed(result);
        }
    }

    @Override
    public void onFailure(Call<ResponseBody> call, Throwable t) {
        t.printStackTrace();
        String errMsg = "请求失败，请稍后再试";
        if (t instanceof ConnectException || t instanceof UnknownHostException
        || t instanceof SocketTimeoutException){
            errMsg = "网络异常";
        }
        OnFailed(-2,errMsg);
    }

    public abstract void OnSucceed(String result);

    public abstract void OnFailed(int ret, String result);
}
