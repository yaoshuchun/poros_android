package com.liquid.poros.utils;

import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.NumberPicker;

import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created Time:　14/05/2018
 * Created by  :  jcsun
 * Desc        :  时间工具类
 */
public class DateUtil {
    private static DecimalFormat decimalFormat = new DecimalFormat("00");

    /**
     * @return currentTimeMillis
     */
    public static long getCurrentTime() {
        long time = System.currentTimeMillis();
        return time;
    }

    /**
     * getDate
     * yyyy-MM-dd HH:mm:ss
     *
     * @param
     * @param format 如yyyy-MM-dd HH:mm:ss
     * @return
     */
    public static String getDateString(long milliseconds, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(new Date(milliseconds));
    }

    /**
     * getDate
     * yyyy-MM-dd HH:mm:ss
     *
     * @param
     * @param format 如yyyy-MM-dd HH:mm:ss
     * @return
     */
    public static long getDateMilliseconds(String date, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        try {
            return sdf.parse(date).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }
    }


    //根据秒数转化为时分秒   00:00:00
    public static String getTime(int second) {
        if (second < 10) {
            return "00:0" + second;
        }
        if (second < 60) {
            return "00:" + second;
        }
        if (second < 3600) {
            int minute = second / 60;
            second = second - minute * 60;
            if (minute < 10) {
                if (second < 10) {
                    return "0" + minute + ":0" + second;
                }
                return "0" + minute + ":" + second;
            }
            if (second < 10) {
                return minute + ":0" + second;
            }
            return minute + ":" + second;
        }
        int hour = second / 3600;
        int minute = (second - hour * 3600) / 60;
        second = second - hour * 3600 - minute * 60;
        if (hour < 10) {
            if (minute < 10) {
                if (second < 10) {
                    return "0" + hour + ":0" + minute + ":0" + second;
                }
                return "0" + hour + ":0" + minute + ":" + second;
            }
            if (second < 10) {
                return "0" + hour + minute + ":0" + second;
            }
            return "0" + hour + minute + ":" + second;
        }
        if (minute < 10) {
            if (second < 10) {
                return hour + ":0" + minute + ":0" + second;
            }
            return hour + ":0" + minute + ":" + second;
        }
        if (second < 10) {
            return hour + minute + ":0" + second;
        }
        return hour + minute + ":" + second;
    }



    /**
     * 返回日时分秒
     * @param second
     * @return
     */
    public static String secondToTime(long second) {
        StringBuffer stringBuffer = new StringBuffer();
        long days = second / 86400;//转换天数
        second = second % 86400;//剩余秒数
        long hours = second / 3600;//转换小时数
        second = second % 3600;//剩余秒数
        long minutes = second / 60;//转换分钟
        second = second % 60;//剩余秒数
        if (0 < days){
            stringBuffer.append(days).append("天 ");
        }
        if (hours<10){
            stringBuffer.append("0");
        }
        stringBuffer.append(hours);
        stringBuffer.append(":");
        if (minutes<10){
            stringBuffer.append("0");
        }
        stringBuffer.append(minutes);
        stringBuffer.append(":");
        if (second<10){
            stringBuffer.append("0");
        }
        stringBuffer.append(second);
        return stringBuffer.toString();
    }
    //获取当前日期
    public static String getCurrDate(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");// HH:mm:ss
        Date date = new Date(System.currentTimeMillis());
        return  simpleDateFormat.format(date);
    }

    //获取当前日期
    public static String getCurrDate2(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");// HH:mm:ss
        Date date = new Date(System.currentTimeMillis());
        return  simpleDateFormat.format(date);
    }


    /**
     * 返回日时分，以数组的形式，3个元素
     * @param second
     * @return
     */
    public static long[] secondToDHM(long second) {
        long[] arr = new long[3];
        StringBuffer stringBuffer = new StringBuffer();
        long days = second / 86400;//转换天数
        second = second % 86400;//剩余秒数
        long hours = second / 3600;//转换小时数
        second = second % 3600;//剩余秒数
        long minutes = second / 60;//转换分钟
        arr[0] = days;
        arr[1] = hours;
        arr[2] = minutes;
        return arr;
    }


    /**
     * 返回日时分秒，以天 时 分 分割的方式返回字符串
     * @param second
     * @return
     */
    public static String secondToDHMStr(long second) {
        long[] arr = new long[4];

        StringBuffer stringBuffer = new StringBuffer();

        long days = second / 86400;//转换天数
        second = second % 86400;//剩余秒数
        long hours = second / 3600;//转换小时数
        second = second % 3600;//剩余秒数
        long minutes = second / 60;//转换分钟
        arr[0] = days;
        arr[1] = hours;
        arr[2] = minutes;



        if (arr[0] > 0){
            stringBuffer.append(arr[0]).append("天");
        }
        if (arr[1] >= 10){
            stringBuffer.append(arr[1]).append("时");
        }else if (arr[1] > 0){
            stringBuffer.append("0").append(arr[1]).append("时");
        }

        if (arr[2] >= 10){
            stringBuffer.append(arr[2]).append("分");
        }else {
            stringBuffer.append("0").append(arr[2]).append("分");
        }



        return stringBuffer.toString();
    }

    /**
     * 返回日时分秒，以数组的形式，4个元素
     * @param second
     * @return
     */
    public static long[] secondToDHMS(long second) {
        long[] arr = new long[4];
        StringBuffer stringBuffer = new StringBuffer();
        long days = second / 86400;//转换天数
        second = second % 86400;//剩余秒数
        long hours = second / 3600;//转换小时数
        second = second % 3600;//剩余秒数
        long minutes = second / 60;//转换分钟
        second = second % 60;
        arr[0] = days;
        arr[1] = hours;
        arr[2] = minutes;
        arr[3] = second;
        return arr;
    }

    /**
     * 返回日时分秒，以:分割的方式返回字符串
     * @param second
     * @return
     */
    public static String secondToDHMSStr(long second) {
        long[] arr = new long[4];

        StringBuffer stringBuffer = new StringBuffer();

        long days = second / 86400;//转换天数
        second = second % 86400;//剩余秒数
        long hours = second / 3600;//转换小时数
        second = second % 3600;//剩余秒数
        long minutes = second / 60;//转换分钟
        second = second % 60;
        arr[0] = days;
        arr[1] = hours;
        arr[2] = minutes;
        arr[3] = second;


        if (arr[0] > 0){
            stringBuffer.append(arr[0]).append(":");
        }
        if (arr[1] >= 10){
            stringBuffer.append(arr[1]).append(":");
        }else if (arr[1] > 0){
            stringBuffer.append("0").append(arr[1]).append(":");
        }

        if (arr[2] >= 10){
            stringBuffer.append(arr[2]).append(":");
        }else {
            stringBuffer.append("0").append(arr[2]).append(":");
        }

        if (arr[3] >= 10){
            stringBuffer.append(arr[3]);
        }else {
            stringBuffer.append("0").append(arr[3]);
        }

        return stringBuffer.toString();
    }

    /**
     * 获得指定日期的前一天
     * @param specifiedDay
     * @return
     * @throws Exception
     */
    public static String getBeforeDay(String specifiedDay){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        Date date=null;
        try {
            date = simpleDateFormat.parse(specifiedDay);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        c.setTime(date);
        int day=c.get(Calendar.DATE);
        c.set(Calendar.DATE,day-1);

        return simpleDateFormat.format(c.getTime());
    }
    /**
     * 获得指定日期的后一天
     * @param specifiedDay
     * @return
     */
    public static String getAfterDay(String specifiedDay){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        Date date=null;
        try {
            date = simpleDateFormat.parse(specifiedDay);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        c.setTime(date);
        int day=c.get(Calendar.DATE);
        c.set(Calendar.DATE,day+1);

        return simpleDateFormat.format(c.getTime());
    }

    /**
     * 获得指定日期的前一天
     * @param specifiedDay
     * @return
     * @throws Exception
     */
    public static int[] getBeforeDayArr(String specifiedDay){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        Date date=null;
        try {
            date = simpleDateFormat.parse(specifiedDay);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        c.setTime(date);
        int day=c.get(Calendar.DATE);
        c.set(Calendar.DATE,day-1);

        return new int[]{c.get(Calendar.YEAR),c.get(Calendar.MONTH),c.get(Calendar.DAY_OF_MONTH)};
    }
    /**
     * 获得指定日期的后一天
     * @param specifiedDay
     * @return
     */
    public static int[] getAfterDayArr(String specifiedDay){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        Date date=null;
        try {
            date = simpleDateFormat.parse(specifiedDay);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        c.setTime(date);
        int day=c.get(Calendar.DATE);
        c.set(Calendar.DATE,day+1);

        return new int[]{c.get(Calendar.YEAR),c.get(Calendar.MONTH),c.get(Calendar.DAY_OF_MONTH)};
    }

    /**
     * 设置时间选择器的分割线颜色
     *
     * @param datePicker
     */
    public static void setDatePickerDividerColor(DatePicker datePicker,int colorResourceId) {
        try {
            // Divider changing:

            // 获取 mSpinners
            LinearLayout llFirst = (LinearLayout) datePicker.getChildAt(0);

            // 获取 NumberPicker
            LinearLayout mSpinners = (LinearLayout) llFirst.getChildAt(0);
            for (int i = 0; i < mSpinners.getChildCount(); i++) {
                NumberPicker picker = (NumberPicker) mSpinners.getChildAt(i);

                Field[] pickerFields = NumberPicker.class.getDeclaredFields();
                for (Field pf : pickerFields) {
                    if (pf.getName().equals("mSelectionDivider")) {
                        pf.setAccessible(true);
                        try {
                            pf.set(picker, new ColorDrawable(colorResourceId));//设置分割线颜色
                        } catch (IllegalArgumentException e) {
                            e.printStackTrace();
                        } catch (Resources.NotFoundException e) {
                            e.printStackTrace();
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        }
                        break;
                    }
                }
            }
        }catch (Throwable th){

        }

    }
}
