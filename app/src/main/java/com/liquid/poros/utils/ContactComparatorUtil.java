package com.liquid.poros.utils;

import android.text.TextUtils;

import java.util.Comparator;

public class ContactComparatorUtil implements Comparator<String> {

    @Override
    public int compare(String o1, String o2) {
        if (TextUtils.isEmpty(o1) || TextUtils.isEmpty(o2)) {
            return -1;
        }
        if (TextUtils.isEmpty(o1) || TextUtils.isEmpty(o2)) {
            return -1;
        }
        int a = (o1.charAt(0) + "").toUpperCase().hashCode();
        int b = (o2.charAt(0) + "").toUpperCase().hashCode();
        return a == b ? 0 : (a > b ? 1 : -1);
    }

}
