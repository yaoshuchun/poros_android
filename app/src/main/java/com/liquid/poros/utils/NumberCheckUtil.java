package com.liquid.poros.utils;

import android.text.TextUtils;
/**
 * 手机号,身份证正则
 */
public class NumberCheckUtil {
    /**
     * 验证手机号是否合法
     *
     * @param phoneNums
     * @return false 不合法,true 合法
     */
    public static boolean isMobileNO(String phoneNums) {
        String telRegex = "^[1](([3][0-9])|([4][5-9])|([5][0-3,5-9])|([6][5,6])|([7][0-8])|([8][0-9])|([9][1,8,9]))[0-9]{8}$";
        if (TextUtils.isEmpty(phoneNums))
            return false;
        else
            return phoneNums.matches(telRegex);
    }

    public static String getStarMobile(String mobile) {
        if (!TextUtils.isEmpty(mobile)) {
            if (mobile.length() >= 11)
                return mobile.substring(0, 3) + "****" + mobile.substring(7, mobile.length());
        } else {
            return "";
        }
        return mobile;
    }

}