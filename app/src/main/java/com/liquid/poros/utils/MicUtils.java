package com.liquid.poros.utils;


public class MicUtils {
    private static MicUtils instance;
    private boolean isOnMic;
    private MicUtils() {}

    public static MicUtils getInstance() {
        if (instance == null) {
            synchronized (MicUtils.class) {
                if (instance == null) {
                    instance = new MicUtils();
                }
            }
        }
        return instance;
    }

    public boolean isOnMic() {
        return isOnMic;
    }

    public void setOnMic(boolean onMic) {
        isOnMic = onMic;
    }
}
