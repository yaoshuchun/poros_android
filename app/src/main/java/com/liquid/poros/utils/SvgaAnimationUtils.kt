package com.liquid.poros.utils

import android.content.Context
import android.view.View
import com.opensource.svgaplayer.*
import java.io.File
import java.io.FileInputStream

/**
 * SVGA动画支持类
 */
object SvgaAnimationUtils {
    fun loadSvga(context: Context, svgaImageView: SVGAImageView, dataPath: String, giftName: String){
        val svgaFile = File(dataPath)
        var fis: FileInputStream? = null
        if (svgaFile.exists()) {
            fis = FileInputStream(svgaFile)
            var svgaParser = SVGAParser(context)
            SVGAParser.shareParser().init(context)
            svgaParser.decodeFromInputStream(fis,"", object : SVGAParser.ParseCompletion {
                override fun onComplete(videoItem: SVGAVideoEntity) {
                    if (svgaImageView != null) {
                        svgaImageView.visibility = View.VISIBLE
                        var drawable = SVGADrawable(videoItem)
                        drawable.dynamicItem
                        svgaImageView.setImageDrawable(drawable)
                        svgaImageView.startAnimation()
                    }
                }

                override fun onError() {

                }
            })
        } else {
            FileDownloadUtil.download(giftName)
        }
    }
}