package com.liquid.poros.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.BatteryManager;
import android.os.Build;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;

import java.lang.reflect.Method;

import androidx.annotation.RequiresApi;

public class SystemUtils {
    private static  SystemUtils instance;
    private TelephonyManager telephonyManager;
    private PhoneStatListener phoneStatListener;
    private int M = 1024*1024;
    private int dbm;
    private int level;

    private SystemUtils() {}

    public static SystemUtils getInstance() {
        if (instance == null) {
            synchronized (SystemUtils.class) {
                if (instance == null) {
                    instance = new SystemUtils();
                }
            }
        }
        return instance;
    }

    public void listenerSignal(Context context) {
        if (telephonyManager == null) {
            //获取一个电话管理类
            telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            if (phoneStatListener == null) {
                phoneStatListener = new PhoneStatListener();
            }
            //通过 listen 方法来侦听电话信息的改变，这里用来侦听网络信号的强度变化，具体还能侦听什么需要看 PhoneStateListener 类的源码
            telephonyManager.listen(phoneStatListener, PhoneStatListener.LISTEN_SIGNAL_STRENGTHS);
        }
    }

    private class PhoneStatListener extends PhoneStateListener {
        @RequiresApi(api = Build.VERSION_CODES.M)
        @Override
        public void onSignalStrengthsChanged(SignalStrength signalStrength) {
            Method method1 = null;
            try {
                method1 = signalStrength.getClass().getMethod("getDbm");
                dbm = (int) method1.invoke(signalStrength);
                Method method2 = signalStrength.getClass().getMethod("getLteLevel");
                level = (int) method2.invoke(signalStrength);
           } catch (Throwable e) {
                e.printStackTrace();
            }
        }
    }

    public boolean isWifiConnect(Context context) {
        ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifiInfo = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        return mWifiInfo.isConnected();
    }

    public String checkWifiState(Context context) {
        if (isWifiConnect(context)) {
            WifiManager mWifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            WifiInfo mWifiInfo = mWifiManager.getConnectionInfo();
            int wifi = mWifiInfo.getRssi();//获取wifi信号强度
            if (wifi > -50 && wifi < 0) {//最强
                return "最强";
            } else if (wifi > -70 && wifi < -50) {//较强
                return "最强";
            } else if (wifi > -80 && wifi < -70) {//较弱
                return "较弱";
            } else if (wifi > -100 && wifi < -80) {//微弱
                return "微弱";
            }
        } else {
            return "无连接";
        }
        return "无连接";
    }

    //最大可用内存
    public int getMaxMemory() {
        int maxMomery = (int) (Runtime.getRuntime().maxMemory() / M);
        return maxMomery;
    }

    //当前可用内存
    public int getTotalMemory() {
        int totalMomery = (int) (Runtime.getRuntime().totalMemory() / M);
        return totalMomery;
    }

    //空闲内存
    public int getFreeMemory() {
        int freeMomery = (int) (Runtime.getRuntime().freeMemory() / M);
        return freeMomery;
    }

    //当前已使用内存
    public int getUsedMemory() {
        Runtime r = Runtime.getRuntime();
        int usedMemory = (int) ((r.totalMemory() - r.freeMemory()) / M);
        return usedMemory;
    }

    //剩余可用内存
    public int getTraceMemory() {
        Runtime r = Runtime.getRuntime();
        int traceMemory = (int) ((r.maxMemory() - r.totalMemory() + r.freeMemory()) / M);
        return traceMemory;
    }

    //获取电量大小
    public int getBattery(Context context) {
        BatteryManager batteryManager = (BatteryManager)context.getSystemService(Context.BATTERY_SERVICE);
        int battery = batteryManager.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
        return battery;
    }

    public int getDbm() {
        return dbm;
    }

    public int getLevel() {
        return level;
    }
}
