package com.liquid.poros.utils;

import java.math.BigDecimal;

/**
 * 金额转换工具类
 */
public class NumberFormatUtil {
    //余额转换  1元 = 100000
    public static long balanceFormat(long balance) {
        BigDecimal bigDecimal = new BigDecimal(balance / 100000f);
        long currentMoney = bigDecimal.setScale(0, BigDecimal.ROUND_DOWN).longValue();
        return currentMoney;
    }
    //余额转换  1元 = 100000 保留两位小数
    public static float formatTwoDecimal(long balance) {
        BigDecimal bigDecimal = new BigDecimal(balance / 100000f);
        float currentMoney = bigDecimal.setScale(2, BigDecimal.ROUND_DOWN).floatValue();
        return currentMoney;
    }
    //余额转换  1元 = 100分 保留两位小数
    public static float formatCandyToCash(long balance) {
        BigDecimal bigDecimal = new BigDecimal(balance);
        BigDecimal bigDecimal1 = new BigDecimal( 100);
        float currentMoney =bigDecimal.divide(bigDecimal1,2,BigDecimal.ROUND_HALF_UP).floatValue();
        return currentMoney;
    }
}
