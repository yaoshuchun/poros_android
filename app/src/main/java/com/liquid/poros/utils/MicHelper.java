package com.liquid.poros.utils;

import com.liquid.im.kit.custom.JoinRoomAttachment;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.chatroom.ChatRoomMessageBuilder;
import com.netease.nimlib.sdk.chatroom.ChatRoomService;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;

/**
 * Created by sundroid on 13/05/2019.
 */

public class MicHelper {
    /**
     * ************************************ 单例 ***************************************
     */
    static class InstanceHolder {
        final static MicHelper instance = new MicHelper();
    }

    private static final String TAG = "TeamPlayHelper";

    public static MicHelper getInstance() {
        return InstanceHolder.instance;
    }



    public void sendJoinRoomMsg(String roomId, String account, String name) {
        JoinRoomAttachment attachment = new JoinRoomAttachment(account, name);
        ChatRoomMessage message = ChatRoomMessageBuilder.createChatRoomCustomMessage(roomId, attachment);
        NIMClient.getService(ChatRoomService.class).sendMessage(message, false);
    }


}
