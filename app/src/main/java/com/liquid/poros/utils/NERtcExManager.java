package com.liquid.poros.utils;

import android.util.Log;
import com.liquid.base.utils.ContextUtils;
import com.netease.lava.nertc.sdk.NERtc;
import com.netease.lava.nertc.sdk.NERtcCallback;

public class NERtcExManager implements NERtcCallback {
    private static  volatile NERtcExManager mInstance;

    private NERtcExManager() {
        try {
            NERtc.getInstance().init(ContextUtils.getApplicationContext(),"e2973c4516dcfc977db1a9f650d227e5", this,null);
        } catch (Exception e) {
        }
    }

    public static NERtcExManager getInstance() {
        if (mInstance == null) {
            synchronized (NERtcExManager.class) {
                if (mInstance == null) {
                    mInstance = new NERtcExManager();
                }
            }
        }
        return mInstance;
    }

    @Override
    public void onJoinChannel(int i, long l, long l1) {
        Log.e("shixinhua","onJoinChannel " + i + "  " + l + "  " + l1);
    }

    @Override
    public void onLeaveChannel(int i) {
        Log.e("shixinhua","onLeaveChannel " + i);
    }

    @Override
    public void onUserJoined(long l) {
        Log.e("shixinhua","onUserJoined " + l);
    }

    @Override
    public void onUserLeave(long l, int i) {
        Log.e("shixinhua","onUserLeave " + i + "  " + l);
    }

    @Override
    public void onUserAudioStart(long l) {
        Log.e("shixinhua","onUserAudioStart " + l);
    }

    @Override
    public void onUserAudioStop(long l) {
        Log.e("shixinhua","onUserAudioStop " + l);
    }

    @Override
    public void onUserVideoStart(long l, int i) {
        Log.e("shixinhua","onUserVideoStart " + i + "  " + l);
    }

    @Override
    public void onUserVideoStop(long l) {
        Log.e("shixinhua","onUserVideoStop " + l);
    }

    @Override
    public void onDisconnect(int i) {
        Log.e("shixinhua","onDisconnect " + i);
    }
}
