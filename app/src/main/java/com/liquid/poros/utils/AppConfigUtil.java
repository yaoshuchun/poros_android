package com.liquid.poros.utils;

import com.liquid.poros.constant.Constants;
import com.liquid.poros.entity.AppBaseConfigBean;
import com.liquid.poros.entity.RechargeWayInfo;
import com.liquid.poros.ui.floatball.GiftPackageFloatManager;

import java.util.List;


/**
 * APP全局配置工具类
 * 全局配置，以后取全局配置，请使用该类
 * Created on 2021/1/7.
 */
public class AppConfigUtil {
    public static final String AD_CSJ_APPID = "ad_csj_appid"; //穿山甲广告id
    public static final String AD_CSJ_CODEID = "ad_csj_codeid";//穿山甲codeid
    public static final String BIND_MASTER_URL = "bind_master_url";
    public static final String CP_RIGHT_CONTENT = "cp_right_content";
    public static final String CP_RIGHT_TITLE = "cp_right_title";
    public static final String CP_VOICE_CHAT_PRICE = "cp_voice_chat_price";
    public static final String FEEDBACK_GROUP = "feedback_group";
    public static final String HIDE_MAKEUP = "hide_makeup";
    public static final String IS_FORBID_USER = "is_forbid_user";
    public static final String SERVER_TIMESTAMP = "server_timestamp";
    public static final String SHARE_GROUP_URL = "share_group_url";
    public static final String SHOW_AGE_LEVEL_CONTINUE = "show_age_level_continue";
    public static final String SHOW_CARTOON = "show_cartoon";
    public static final String SHOW_FEED = "show_feed";
    public static final String SHOW_SQUARE_FEED = "show_square_feed";
    public static final String STATUS = "status";
    public static final String USER_CLIENT_URL = "user_client_url";
    public static final String HEARTBEAT_INTERVAL = "heartbeat_interval";
    public static final String USING_NEW_LIVE_ROOM = "using_new_live_room"; //判断使用新旧视频房
    public List<String> cp_room_anchor_tips;
    public List<String> cp_room_mic_user_tips;
    public List<String> cp_room_watch_user_tips;
    public List<String> games;
    public List<String> live_tips;
    public List<String> private_room_tips;
    public List<RechargeWayInfo> recharge_way;
    public List<String> team_leader_tips;
    public List<String> team_user_tips;
    public List<String> together_room_anchor_tips;
    public List<String> together_room_other_tips;

    /**
     * 统一保存基础配置信息
     */
    public static void saveBaseConfig(AppBaseConfigBean appBaseConfig) {
        if (appBaseConfig == null) {
            return;
        }
        SharePreferenceUtil.getSharedPreferences().edit()
                .putString(AD_CSJ_APPID, appBaseConfig.ad_csj_appid)
                .putString(AD_CSJ_CODEID, appBaseConfig.ad_csj_codeid)
                .putString(BIND_MASTER_URL, appBaseConfig.bind_master_url)

                .putString(CP_RIGHT_TITLE, appBaseConfig.cp_right_title)
                .putString(CP_RIGHT_CONTENT, appBaseConfig.cp_right_content)
                .putInt(CP_VOICE_CHAT_PRICE, appBaseConfig.cp_voice_chat_price)
                .putString(FEEDBACK_GROUP, appBaseConfig.feedback_group)
                .putBoolean(HIDE_MAKEUP, appBaseConfig.hide_makeup)
                .putBoolean(IS_FORBID_USER, appBaseConfig.is_forbid_user)
                .putLong(SERVER_TIMESTAMP, System.currentTimeMillis() - appBaseConfig.server_timestamp)
                .putString(SHARE_GROUP_URL, appBaseConfig.share_group_url + "%s")
                .putBoolean(SHOW_AGE_LEVEL_CONTINUE, appBaseConfig.show_age_level_continue)
                .putBoolean(SHOW_CARTOON, appBaseConfig.show_cartoon)
                .putInt(SHOW_FEED, appBaseConfig.show_feed)
                .putInt(SHOW_SQUARE_FEED, appBaseConfig.show_square_feed)
                .putInt(STATUS, appBaseConfig.status)
                .putString(USER_CLIENT_URL, appBaseConfig.user_client_url)
                .putLong(HEARTBEAT_INTERVAL,appBaseConfig.heartbeat_interval)
                .putBoolean(USING_NEW_LIVE_ROOM,appBaseConfig.using_new_live_room)
                .apply();
    }

    /**
     * 退出登陆的时候，删除基础配置信息
     */
    public static void removeBaseConfig() {
        SharePreferenceUtil.clearFile(SharePreferenceUtil.FILE_APP_CONFIG);
    }

    public static long getLong(String key,long defaultValue){
        return SharePreferenceUtil.getSharedPreferences().getLong(key,defaultValue);
    }

    public static String getAd_csj_appid() {
        return SharePreferenceUtil.getSharedPreferences().getString(AD_CSJ_APPID, "");
    }

    public static String getAd_csj_codeid() {
        return SharePreferenceUtil.getSharedPreferences().getString(AD_CSJ_CODEID, "");
    }

    public static void setAd_csj_appid(String appid) {
        SharePreferenceUtil.getSharedPreferences().edit().putString(AD_CSJ_APPID, appid).apply();
    }

    public static void setAd_csj_codeid(String codeid) {
        SharePreferenceUtil.getSharedPreferences().edit().putString(AD_CSJ_CODEID, codeid).apply();
    }

    public static  String getBind_master_url() {
        return SharePreferenceUtil.getSharedPreferences().getString(BIND_MASTER_URL, "");
    }

    public static  String getCp_right_content() {
        return SharePreferenceUtil.getSharedPreferences().getString(CP_RIGHT_CONTENT, "");
    }

    public static  String getCp_right_title() {
        return SharePreferenceUtil.getSharedPreferences().getString(CP_RIGHT_TITLE, "");
    }

    public static  Integer getCp_voice_chat_price() {
        return SharePreferenceUtil.getSharedPreferences().getInt(CP_VOICE_CHAT_PRICE, 0);
    }

    public static  String getFeedback_group() {
        return SharePreferenceUtil.getSharedPreferences().getString(FEEDBACK_GROUP, "618853382");
    }

    public static  Boolean getHide_makeup() {
        return SharePreferenceUtil.getSharedPreferences().getBoolean(HIDE_MAKEUP, false);
    }

    public static  Boolean getIs_forbid_user() {
        return SharePreferenceUtil.getSharedPreferences().getBoolean(IS_FORBID_USER, false);
    }

    public static  long getServer_timestamp() {
        return SharePreferenceUtil.getSharedPreferences().getLong(SERVER_TIMESTAMP, 0);
    }

    public static  String getShare_group_url() {
        return SharePreferenceUtil.getSharedPreferences().getString(SHARE_GROUP_URL, "https://liquid-h5.oss-cn-beijing.aliyuncs.com/box_shop/favour_index_test.html?group_id=%s#");
    }

    public  static Boolean getShow_age_level_continue() {
        return SharePreferenceUtil.getSharedPreferences().getBoolean(SHOW_AGE_LEVEL_CONTINUE, false);
    }

    public  static Boolean getShow_cartoon() {
        return SharePreferenceUtil.getSharedPreferences().getBoolean(SHOW_CARTOON, false);
    }

    public  static Integer getShow_feed() {
        return SharePreferenceUtil.getSharedPreferences().getInt(SHOW_FEED, 0);
    }

    public  static Integer getShow_square_feed() {
        return SharePreferenceUtil.getSharedPreferences().getInt(SHOW_SQUARE_FEED, 1);
    }

    public  static Integer getStatus() {
        return SharePreferenceUtil.getSharedPreferences().getInt(STATUS, 0);
    }

    public  static String getUser_client_url() {
        return SharePreferenceUtil.getSharedPreferences().getString(USER_CLIENT_URL, "");
    }
    //永远跳转新页面，改方法废弃
    @Deprecated
    public  static boolean getUsing_new_live_room() {
        return SharePreferenceUtil.getSharedPreferences().getBoolean(USING_NEW_LIVE_ROOM, true);
    }

    //TODO 存储以下变量
    public List<String> getCp_room_anchor_tips() {
        return cp_room_anchor_tips;
    }

    public void setCp_room_anchor_tips(List<String> cp_room_anchor_tips) {
        this.cp_room_anchor_tips = cp_room_anchor_tips;
    }

    public List<String> getCp_room_mic_user_tips() {
        return cp_room_mic_user_tips;
    }

    public void setCp_room_mic_user_tips(List<String> cp_room_mic_user_tips) {
        this.cp_room_mic_user_tips = cp_room_mic_user_tips;
    }

    public List<String> getCp_room_watch_user_tips() {
        return cp_room_watch_user_tips;
    }

    public void setCp_room_watch_user_tips(List<String> cp_room_watch_user_tips) {
        this.cp_room_watch_user_tips = cp_room_watch_user_tips;
    }

    public List<String> getGames() {
        return games;
    }

    public void setGames(List<String> games) {
        this.games = games;
    }

    public List<String> getLive_tips() {
        return live_tips;
    }

    public void setLive_tips(List<String> live_tips) {
        this.live_tips = live_tips;
    }

    public List<String> getPrivate_room_tips() {
        return private_room_tips;
    }

    public void setPrivate_room_tips(List<String> private_room_tips) {
        this.private_room_tips = private_room_tips;
    }

    public List<RechargeWayInfo> getRecharge_way() {
        return recharge_way;
    }

    public void setRecharge_way(List<RechargeWayInfo> recharge_way) {
        this.recharge_way = recharge_way;
    }

    public List<String> getTeam_leader_tips() {
        return team_leader_tips;
    }

    public void setTeam_leader_tips(List<String> team_leader_tips) {
        this.team_leader_tips = team_leader_tips;
    }

    public List<String> getTeam_user_tips() {
        return team_user_tips;
    }

    public void setTeam_user_tips(List<String> team_user_tips) {
        this.team_user_tips = team_user_tips;
    }

    public List<String> getTogether_room_anchor_tips() {
        return together_room_anchor_tips;
    }

    public void setTogether_room_anchor_tips(List<String> together_room_anchor_tips) {
        this.together_room_anchor_tips = together_room_anchor_tips;
    }

    public List<String> getTogether_room_other_tips() {
        return together_room_other_tips;
    }

    public void setTogether_room_other_tips(List<String> together_room_other_tips) {
        this.together_room_other_tips = together_room_other_tips;
    }
}
