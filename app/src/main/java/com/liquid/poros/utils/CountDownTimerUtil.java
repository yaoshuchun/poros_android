package com.liquid.poros.utils;

import android.os.Handler;
import android.os.Message;

import java.util.Timer;
import java.util.TimerTask;

public class CountDownTimerUtil {
    private int mAudioTimer = 0;
    private int audioMaxTime = 60;
    private Timer mTimer2;
    private TimerTask mTask2;

    public void startTime() {
        if (mTimer2 == null && mTask2 == null) {
            mTimer2 = new Timer();
            mTask2 = new TimerTask() {
                @Override
                public void run() {
                    mHandler.sendMessage(mHandler.obtainMessage(1));
                }
            };
            mTimer2.schedule(mTask2, 0, 1000);
        }
    }

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            synchronized (CountDownTimerUtil.this) {
                mAudioTimer++;
                if (mAudioTimer <= audioMaxTime) {
                    if (listener != null) {
                        listener.startTime(mAudioTimer);
                    }
                } else {
                    releaseTimer();
                }
            }
        }
    };

    public void releaseTimer() {
        mAudioTimer = 0;
        if (mTimer2 != null) {
            mTimer2.cancel();
            mTimer2 = null;
        }
        if (mTask2 != null) {
            mTask2.cancel();
            mTask2 = null;
        }
    }

    private CountDownTimerUtil.AudioPlayListener listener;

    public CountDownTimerUtil setAudioListener(CountDownTimerUtil.AudioPlayListener listener) {
        this.listener = listener;
        return this;
    }

    public interface AudioPlayListener {
        //开始计时
        void startTime(int time);

        //结束计时
        void finishTime();

    }
}
