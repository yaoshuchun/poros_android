package com.liquid.poros.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.liquid.poros.PorosApplication;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SharePreferenceUtil {

    public static final String FILE_APP_CONFIG = "app_config";//app config的 文件存储
    public static final String FILE_API_CONFIG = "app_api";//app api的 文件存储
    public static final String FILE_USER_ACCOUNT_DATA = "file_user_account_data";


    public static final String KEY_USER_ACCOUNT_TOKEN = "key_user_account_token";
    public static final String KEY_USER_ACCOUNT_INFO = "key_user_account_info";
    public static final String KEY_API_URL = "key_api_url";


    @Deprecated
    public static final String KEY_CORE_SERVICE_STARTED = "key_core_service_started";


    public static final String KEY_NEW_REG_USER = "key_new_reg_user";
    public static final String KEY_GIFT_EVER_CLICK = "key_gift_ever_click";
    public final static String KEY_INFO_GAMES = "key_info_game";
    public final static String FILE_INFO_GAMES_DATA = "file_info_game_data";
    public final static String KEY_DRESS_UP_TIP_FIRST = "key_dress_up_tip_first";
    public final static String KEY_VIRTUAL_DOWNLOAD_COUNT = "key_virtual_download_count";
    public final static String KEY_SHOULD_SHOW_KEY_TIP = "key_should_show_key_tip";
    public static final String KEY_GUIDE_HELP_BOX = "key_guide_help_box";//是否引导过助力宝箱

    public final static String KEY_FIRST_USED_APPID = "first_used_appid";//穿山甲广告首次使用的appid
    //音视频通话，互动直播相关joinChannel 需要 的token更新 时间
    public static final String KEY_USER_NERTC_TOKEN_UPDATE_TIME = "key_user_nertc_token_update_time";
    //音视频通话，互动直播相关joinChannel 需要 的token
    public static final String KEY_USER_NERTC_TOKEN = "key_user_nertc_token";


    /**
     * app 的基础配置
     * @return
     */
    public static SharedPreferences getSharedPreferences() {
        try {
            SharedPreferences preferences = PorosApplication.context.getSharedPreferences(FILE_APP_CONFIG, Context.MODE_PRIVATE);
            return preferences;
        } catch (Exception e) {
        }
        return null;
    }

    public static void removeKey(String filename, String key) {
        try {
            SharedPreferences preferences = PorosApplication.context.getSharedPreferences(filename, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.remove(key);
            editor.apply();
        } catch (Exception e) {
        }
    }

    public static void saveString(String filename, String key, String value) {
        try {
            SharedPreferences preferences = PorosApplication.context.getSharedPreferences(filename, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(key, value);
            editor.apply();
        } catch (Exception e) {
        }
    }

    public static String getString(String filename, String key, String defaultValue) {
        try {
            SharedPreferences preferences = PorosApplication.context.getSharedPreferences(filename, Context.MODE_PRIVATE);
            return preferences.getString(key, defaultValue);
        } catch (Exception e) {
        }
        return defaultValue;
    }

    public static void saveInt(String filename, String key, int value) {
        try {
            SharedPreferences preferences = PorosApplication.context.getSharedPreferences(filename, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putInt(key, value);
            editor.apply();
        } catch (Exception e) {
        }
    }

    public static void saveLong(String filename, String key, long value) {
        try {
            SharedPreferences preferences = PorosApplication.context.getSharedPreferences(filename, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putLong(key, value);
            editor.apply();
        } catch (Exception e) {
        }
    }


    public static long getLong(String filename, String key, long defaultValue) {
        try {
            SharedPreferences preferences = PorosApplication.context.getSharedPreferences(filename, Context.MODE_PRIVATE);
            return preferences.getLong(key, defaultValue);
        } catch (Exception e) {
        }
        return defaultValue;
    }

    public static int getInt(String filename, String key, int defaultValue) {
        try {
            SharedPreferences preferences = PorosApplication.context.getSharedPreferences(filename, Context.MODE_PRIVATE);
            return preferences.getInt(key, defaultValue);
        } catch (Exception e) {
        }
        return defaultValue;
    }

    public static void saveBoolean(String filename, String key, boolean value) {
        try {
            SharedPreferences preferences = PorosApplication.context.getSharedPreferences(filename, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean(key, value);
            editor.apply();
        } catch (Exception e) {
        }
    }

    public static boolean getBoolean(String filename, String key, boolean defaultValue) {
        try {
            SharedPreferences preferences = PorosApplication.context.getSharedPreferences(filename, Context.MODE_PRIVATE);
            return preferences.getBoolean(key, defaultValue);
        } catch (Exception e) {
        }
        return defaultValue;
    }

    // 保存搜索记录
    public static void saveSearchHistory(String fileName, String key, String inputText) {
        SharedPreferences sp = PorosApplication.context.getSharedPreferences(fileName, Context.MODE_PRIVATE);
        if (TextUtils.isEmpty(inputText)) {
            return;
        }
        String longHistory = sp.getString(key, "");
        String[] tmpHistory = longHistory.split(",");
        List<String> historyList = new ArrayList<String>(Arrays.asList(tmpHistory));
        SharedPreferences.Editor editor = sp.edit();
        if (historyList.size() > 0) {
            //1.移除之前重复添加的元素
            for (int i = 0; i < historyList.size(); i++) {
                if (inputText.equals(historyList.get(i))) {
                    historyList.remove(i);
                    break;
                }
            }
            historyList.add(0, inputText); //倒序插入
            if (historyList.size() > 15) {
                //最多保存15条
                historyList.remove(historyList.size() - 1);
            }
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < historyList.size(); i++) {
                sb.append(historyList.get(i) + ",");
            }
            editor.putString(key, sb.toString());
            editor.commit();
        } else {
            //之前未添加过
            editor.putString(key, inputText + ",");
            editor.commit();
        }
    }

    //获取搜索记录
    public static List<String> getSearchHistory(String fileName, String key) {
        SharedPreferences sp = PorosApplication.context.getSharedPreferences(fileName, Context.MODE_PRIVATE);
        String longHistory = sp.getString(key, "");
        String[] tmpHistory = longHistory.split(",");
        List<String> historyList = new ArrayList<String>(Arrays.asList(tmpHistory));
        if (historyList.size() == 1 && historyList.get(0).equals("")) {
            //如果没有搜索记录，split之后第0位是个空串的情况下
            historyList.clear();  //清空集合
        }
        return historyList;
    }

    //清除
    public static void clearFile(String fileName) {
        SharedPreferences sp = PorosApplication.context.getSharedPreferences(fileName, Context.MODE_PRIVATE);
        if (sp != null) {
            sp.edit().clear().commit();
        }
    }

    public static void saveStringArray(String fileName, String key, String[] stringArray) {
        SharedPreferences prefs = PorosApplication.context.getSharedPreferences(fileName, Context.MODE_PRIVATE);
        StringBuilder sb = new StringBuilder();
        if (stringArray.length != 0) {
            for (int i = 0; i < stringArray.length; i++) {
                sb.append(stringArray[i] + ",");
            }
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString(key, sb.toString());
            editor.commit();
        }
    }

    public static String[] getStringArray(String fileName, String key) {
        SharedPreferences sp = PorosApplication.context.getSharedPreferences(fileName, Context.MODE_PRIVATE);
        String temp = ",";
        String[] resArray = null;
        String values;
        values = sp.getString(key, "");
        resArray = values.split(temp);
        return resArray;
    }
}
