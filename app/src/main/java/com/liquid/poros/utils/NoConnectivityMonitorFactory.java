package com.liquid.poros.utils;

import android.content.Context;

import androidx.annotation.NonNull;

import com.bumptech.glide.manager.ConnectivityMonitor;
import com.bumptech.glide.manager.ConnectivityMonitorFactory;

public class NoConnectivityMonitorFactory implements ConnectivityMonitorFactory {

    @NonNull
    @Override
    public ConnectivityMonitor build(@NonNull Context context, @NonNull ConnectivityMonitor.ConnectivityListener listener) {
        return new ConnectivityMonitor() {

            @Override
            public void onStart() {


            }

            @Override
            public void onStop() {


            }

            @Override
            public void onDestroy() {


            }
        };
    }
}
