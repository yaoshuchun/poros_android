package com.liquid.poros.utils;

import android.app.Activity;

import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.AppUtils;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class MyActivityManager {
    private static MyActivityManager activityManager;
    private List<String> activityNameList;
    private List<Activity> activityList;

    private MyActivityManager() {
        activityNameList = new ArrayList<>();
        activityList = new ArrayList<>();
    }

    public static MyActivityManager getInstance(){
        if (null == activityManager){
            synchronized (MyActivityManager.class){
                if (null == activityManager){
                    activityManager = new MyActivityManager();
                }
            }
        }

        return activityManager;
    }


    public Activity getCurrentActivity() {
        return ActivityUtils.getTopActivity();
    }

    public void add(String activityName) {
        synchronized (MyActivityManager.class) {
            if (activityNameList != null) {
                activityNameList.add(activityName);
            }
        }
    }

    public void remove(String activityName) {
        synchronized (MyActivityManager.class) {
            if (activityNameList != null && activityNameList.contains(activityName)) {
                activityNameList.remove(activityName);
            }
        }
    }

    public boolean isActivityExit(String activityName) {
        synchronized (MyActivityManager.class) {
            if (activityNameList != null) {
                return activityNameList.contains(activityName);
            }
            return false;
        }
    }

    public void add(Activity activity) {
        synchronized (MyActivityManager.class) {
            if (activityList != null) {
                activityList.add(activity);
            }
        }
    }

    public void remove(Activity activity) {
        synchronized (MyActivityManager.class) {
            if (activityList != null && activityList.contains(activity)) {
                activityList.remove(activity);
            }
        }
    }

    public List<Activity> getActivityList() {
        return activityList;
    }
}
