package com.liquid.poros.utils;

import android.text.TextUtils;

import com.liquid.base.tools.GT;
import com.liquid.base.utils.LogOut;
import com.liquid.poros.entity.BaseModel;
import com.liquid.poros.entity.NERtcTokenInfo;
import com.liquid.poros.entity.PorosUserInfo;
import com.liquid.poros.utils.bundle.VirtualBundleDownloadUtil;
import com.liquid.poros.utils.network.HttpCallback;
import com.liquid.poros.utils.network.RetrofitHttpManager;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;

public class AccountUtil {

    private static final String TAG = AccountUtil.class.getSimpleName();
    private static AccountUtil mInstance = null;
    private PorosUserInfo userInfo = null;
    private String ageLevel;
    private int currentOrderStatus = 0;
    private String coin;
    private boolean needSetBasicInfo;
    private boolean matchControl;
    private boolean open;
    private int pay_amount;
    private int countdown;
    private String pageId;

    private AccountUtil() {

    }

    public String getPageId() {
        return pageId;
    }

    public void setPageId(String pageId) {
        this.pageId = pageId;
    }

    public boolean isOpen() {
        return open;
    }

    public void setOpen(boolean open) {
        this.open = open;
    }

    public int getPay_amount() {
        return pay_amount;
    }

    public void setPay_amount(int pay_amount) {
        this.pay_amount = pay_amount;
    }

    public int getCountdown() {
        return countdown;
    }

    public void setCountdown(int countdown) {
        this.countdown = countdown;
    }

    public String getAgeLevel() {
        return ageLevel;
    }

    public void setAgeLevel(String ageLevel) {
        this.ageLevel = ageLevel;
    }


    public boolean isNeedSetBasicInfo() {
        return needSetBasicInfo;
    }

    public void setNeedSetBasicInfo(boolean needSetBasicInfo) {
        this.needSetBasicInfo = needSetBasicInfo;
    }

    public String getCoin() {
        return coin;
    }

    public void setCoin(String coin) {
        this.coin = coin;
    }

    public int getCurrentOrderStatus() {
        return currentOrderStatus;
    }

    public void setCurrentOrderStatus(int currentOrderStatus) {
        this.currentOrderStatus = currentOrderStatus;
    }

    public boolean isMatchControl() {
        return matchControl;
    }

    public void setMatchControl(boolean matchControl) {
        this.matchControl = matchControl;
    }

    public int getVoiceOrderStatus() {
        if (getUserInfo() != null) {
            return getUserInfo().getVoice_order_status();
        } else {
            return 0;
        }
    }

    public PorosUserInfo getUserInfo() {
        if (userInfo == null) {
            String userStr = SharePreferenceUtil.getString(SharePreferenceUtil.FILE_USER_ACCOUNT_DATA, SharePreferenceUtil.KEY_USER_ACCOUNT_INFO, "");
            if (!TextUtils.isEmpty(userStr)) {
                try {
                    JSONObject jsonObject = new JSONObject(userStr);
                    userInfo = PorosUserInfo.generate(jsonObject);
                } catch (Throwable e) {
                    e.printStackTrace();
                }
            }
        }
        return userInfo;
    }

    public String getAccount_token() {
        if (getUserInfo() != null) {
            return getUserInfo().getAccount_token();
        } else {
            return "";
        }
    }


    public void setUserInfo(PorosUserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public static AccountUtil getInstance() {
        return mInstance == null ? mInstance = new AccountUtil() : mInstance;
    }


    public static boolean handleUserInfo(String result) {
        JSONObject ret;
        boolean isHandle = false;
        try {
            ret = new JSONObject(result);
            if (ret.getInt("code") == 0) {
                JSONObject userJson = ret.getJSONObject("data").getJSONObject("user_info");
                PorosUserInfo userInfo = PorosUserInfo.generate(userJson);
                SharePreferenceUtil.saveString(SharePreferenceUtil.FILE_USER_ACCOUNT_DATA, SharePreferenceUtil.KEY_USER_ACCOUNT_INFO, userJson.toString());
                SharePreferenceUtil.saveBoolean(SharePreferenceUtil.FILE_USER_ACCOUNT_DATA, SharePreferenceUtil.KEY_NEW_REG_USER, userJson.optBoolean("new_reg_user"));
                AccountUtil.getInstance().setUserInfo(userInfo);
                isHandle = true;
                JSONObject config = ret.getJSONObject("data").getJSONObject("newbie_small_gift_config");
                if (config != null){
                    AccountUtil.getInstance().setOpen(config.optBoolean("open"));
                    AccountUtil.getInstance().setPay_amount(config.optInt("pay_amount"));
                    AccountUtil.getInstance().setCountdown(config.optInt("countdown"));
                }
//                VirtualBundleDownloadUtil.INSTANCE.getData();
                getNERtcToken(null);
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return isHandle;
    }

    public static boolean handleUserInfo1(String result) {
        JSONObject ret;
        boolean isHandle = false;
        try {
            ret = new JSONObject(result);
            if (ret.getInt("code") == 0) {
                JSONObject userJson = ret.getJSONObject("data");
                PorosUserInfo userInfo = PorosUserInfo.generate(userJson);
                SharePreferenceUtil.saveString(SharePreferenceUtil.FILE_USER_ACCOUNT_DATA, SharePreferenceUtil.KEY_USER_ACCOUNT_INFO, userJson.toString());
                AccountUtil.getInstance().setUserInfo(userInfo);
                isHandle = true;
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return isHandle;
    }

    public String getUserId() {
        if (getUserInfo() != null) {
            return getUserInfo().getUser_id();
        }
        return "";
    }

    public String getGoldMasterId() {
        if (getUserInfo() != null) {
            return getUserInfo().getSelf_gold_master_id();
        }
        return "";
    }

    public interface WechatLoginListener {
        void onLoginSucceed();

        void onLoginFailed(String msg);
    }
    public static void getNERtcToken(NERtcTokenGeter neRtcTokenGeter) {
        long lastUpdateTime = SharePreferenceUtil.getLong(SharePreferenceUtil.FILE_USER_ACCOUNT_DATA, SharePreferenceUtil.KEY_USER_NERTC_TOKEN_UPDATE_TIME, 0);
        String rtcToken = SharePreferenceUtil.getString(SharePreferenceUtil.FILE_USER_ACCOUNT_DATA, SharePreferenceUtil.KEY_USER_NERTC_TOKEN, "");
        if (System.currentTimeMillis() - lastUpdateTime > 10 * 60 * 60 * 1000 || lastUpdateTime == 0 || TextUtils.isEmpty(rtcToken) || null == neRtcTokenGeter){
            RetrofitHttpManager.getInstance().httpInterface.getNERtcToken(AccountUtil.getInstance().getUserId(),AccountUtil.getInstance().getAccount_token()).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    try {
                        NERtcTokenInfo tokenInfo = GT.fromJson(new JSONObject(result).optJSONObject("data").toString(), NERtcTokenInfo.class);
                        SharePreferenceUtil.saveString(SharePreferenceUtil.FILE_USER_ACCOUNT_DATA, SharePreferenceUtil.KEY_USER_NERTC_TOKEN, tokenInfo.getIm_video_token());
                        SharePreferenceUtil.saveLong(SharePreferenceUtil.FILE_USER_ACCOUNT_DATA, SharePreferenceUtil.KEY_USER_NERTC_TOKEN_UPDATE_TIME, System.currentTimeMillis());
                        if (null != neRtcTokenGeter){
                            neRtcTokenGeter.tokenGeted(tokenInfo.getIm_video_token());
                        }
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void OnFailed(int ret, String result) {

                }
            });
        }else {
            if (null != neRtcTokenGeter){
                neRtcTokenGeter.tokenGeted(rtcToken);
            }
        }
    }
    public void wechatLogin(String code, final WechatLoginListener wechatLoginListener) {
        try {
            JSONObject object = new JSONObject();
            object.put("code", code);
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json;charset=utf-8"), object.toString());

            RetrofitHttpManager.getInstance().httpInterface.wechat_login(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    if (handleUserInfo(result)) {
                        wechatLoginListener.onLoginSucceed();
                    } else {
                        BaseModel baseModel = GT.fromJson(result, BaseModel.class);
                        if (baseModel != null && !TextUtils.isEmpty(baseModel.getMsg())) {
                            wechatLoginListener.onLoginFailed(baseModel.getMsg());
                        }
                    }
                    LogOut.debug(TAG, "login OnSucceed:" + result);
                }

                @Override
                public void OnFailed(int ret, String result) {
                    LogOut.debug(TAG, "login OnFailed:" + result);
                    wechatLoginListener.onLoginFailed("登录失败");
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            wechatLoginListener.onLoginFailed("登录失败");
        }
    }

    public void qqLogin(String openid, String access_token, final WechatLoginListener wechatLoginListener) {
        try {
            JSONObject object = new JSONObject();
            object.put("openid", openid);
            object.put("access_token", access_token);

            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json;charset=utf-8"), object.toString());

            RetrofitHttpManager.getInstance().httpInterface.qq_login(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    if (handleUserInfo(result)) {
                        wechatLoginListener.onLoginSucceed();
                    } else {
                        BaseModel baseModel = GT.fromJson(result, BaseModel.class);
                        if (baseModel != null && !TextUtils.isEmpty(baseModel.getMsg())) {
                            wechatLoginListener.onLoginFailed(baseModel.getMsg());
                        }
                    }
                }

                @Override
                public void OnFailed(int ret, String result) {
                    LogOut.debug(TAG, "login OnFailed:" + result);
                    wechatLoginListener.onLoginFailed("登录失败");
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            wechatLoginListener.onLoginFailed("登录失败");
        }
    }


    public interface AutoLoginListener {
        void onSucceed();

        void onFailed();
    }

    public void auto_login(String account_token, final AutoLoginListener autoLoginListener) {
        try {
            if (TextUtils.isEmpty(account_token)) {
                account_token = SharePreferenceUtil.getString(SharePreferenceUtil.FILE_USER_ACCOUNT_DATA, SharePreferenceUtil.KEY_USER_ACCOUNT_TOKEN, "");
            }
            if (TextUtils.isEmpty(account_token)) {
                return;
            }
            JSONObject object = new JSONObject();
            object.put("token", account_token);
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json;charset=utf-8"), object.toString());

            RetrofitHttpManager.getInstance().httpInterface.user_info(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    if (handleUserInfo(result)) {
                        if (null != autoLoginListener){
                            autoLoginListener.onSucceed();
                        }
                    } else {
                        if (null != autoLoginListener){
                            autoLoginListener.onFailed();
                        }
                    }
                    LogOut.debug(TAG, "auto_login OnSucceed:" + result);
                }

                @Override
                public void OnFailed(int ret, String result) {
                    if (null != autoLoginListener){
                        autoLoginListener.onFailed();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            if (null != autoLoginListener){
                autoLoginListener.onFailed();
            }
        }
    }
    public interface NERtcTokenGeter{
        void tokenGeted(String rtcToken);
    }
    /**
     * 更新用户信息接口
     */
    public void updateUserInfo() {
        try {
            JSONObject object = new JSONObject();
            object.put("token", getAccount_token());
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json;charset=utf-8"), object.toString());

            RetrofitHttpManager.getInstance().httpInterface.user_info(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    handleUserInfo(result);
                }

                @Override
                public void OnFailed(int ret, String result) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
