package com.liquid.poros.utils.bundle

import com.liquid.base.tools.GT
import com.liquid.base.utils.ContextUtils
import com.liquid.poros.PorosApplication
import com.liquid.poros.entity.BundleBean
import com.liquid.poros.entity.BundleListBean
import com.liquid.poros.utils.AccountUtil
import com.liquid.poros.utils.SharePreferenceUtil
import com.liquid.poros.utils.network.HttpCallback
import com.liquid.poros.utils.network.RetrofitHttpManager
import com.tonyodev.fetch2.*
import com.tonyodev.fetch2.Fetch.Impl.getInstance
import kotlinx.coroutines.*
import java.io.File
import kotlin.coroutines.resume

object VirtualBundleDownloadUtil {

    private val parentPathPro = ContextUtils.getApplicationContext().cacheDir.path + File.separator + "virtual"
    val parentPath = parentPathPro + File.separator
    private const val temNameEnd = ".temp"
    val fetch = getInstance(FetchConfiguration.Builder(PorosApplication.instance!!)
            .setDownloadConcurrentLimit(1)
            .setProgressReportingInterval(1000)
            .build())

    var launch  = CoroutineScope(Dispatchers.Main)

    var downloaded = false

    var count = 0

    var hasGet = false

    val list = ArrayList<BundleBean>()

    fun getData() {
        if (hasGet) {
            return
        }

        hasGet = true

        var isAll = if (!File(parentPathPro).exists()) {
            1
        } else if (File(parentPathPro).exists() && SharePreferenceUtil.getInt(SharePreferenceUtil.FILE_USER_ACCOUNT_DATA, SharePreferenceUtil.KEY_VIRTUAL_DOWNLOAD_COUNT, 0) > 0) {
            1
        } else {
            0
        }

        RetrofitHttpManager.getInstance().httpInterface.download_list(isAll, AccountUtil.getInstance().account_token).enqueue(object : HttpCallback() {
            override fun OnFailed(ret: Int, result: String) {

            }

            override fun OnSucceed(result: String) {
                val bundleListBean = GT.fromJson(result, BundleListBean::class.java)
                if (bundleListBean != null && !bundleListBean.data.isNullOrEmpty()) {
                    list.clear()
                    list.addAll(bundleListBean.data)
                    downloadAll()
                }
            }
        })
    }

    @JvmOverloads
    fun downloadAll() {
        cancelJob()

        count = list.size
        SharePreferenceUtil.saveInt(SharePreferenceUtil.FILE_USER_ACCOUNT_DATA, SharePreferenceUtil.KEY_VIRTUAL_DOWNLOAD_COUNT, count)

        launch = CoroutineScope(Dispatchers.Main)
        launch.launch {
            list.forEachIndexed { index, bundleBean ->
                if (!bundleBean.bundle_url.isNullOrEmpty()) {
                    if (!File(parentPath + bundleBean.bundle).exists()) {
                        download(bundleBean, null)
                    }
                }
                count--
                SharePreferenceUtil.saveInt(SharePreferenceUtil.FILE_USER_ACCOUNT_DATA, SharePreferenceUtil.KEY_VIRTUAL_DOWNLOAD_COUNT, count)
            }
            downloaded = true
        }
    }

    @JvmOverloads
    fun download(list: List<BundleBean>, downloadListener: DownloadListener?) {
        cancelJob()

        launch = CoroutineScope(Dispatchers.Main)

        launch.launch {
            list.forEachIndexed { index, bundleBean ->
                if (!bundleBean.bundle_url.isNullOrEmpty()) {
                    if (!File(parentPath + bundleBean.bundle).exists()) {
                        download(bundleBean, downloadListener)
                    }
                }
            }
            downloadListener?.onComplete()

            if (!downloaded) {
                downloadAll()
            }
        }

    }

    @JvmOverloads
    fun cancelJob() {
        fetch.cancelAll()
        if ( launch.isActive) {
            launch.cancel()
        }
    }

    @JvmOverloads
    suspend fun download(bundleBean: BundleBean, downloadListener: DownloadListener?): Int {
        return suspendCancellableCoroutine {
            val file = File(parentPath + if (bundleBean.bundle.contains(".")) bundleBean.bundle.substring(0, bundleBean.bundle.indexOf(".") + 1) else bundleBean.bundle + temNameEnd)
            if (file.exists()) {
                file.delete()
            }

            fetch.addListener(object : AbstractFetchListener() {
                override fun onProgress(download: Download, etaInMilliSeconds: Long, downloadedBytesPerSecond: Long) {
                    super.onProgress(download, etaInMilliSeconds, downloadedBytesPerSecond)

                    if (download.url == bundleBean.bundle_url) {
                        downloadListener?.onProgress(download.progress)
                    }
                }

                override fun onCompleted(download: Download) {
                    file.renameTo(File(parentPath + File.separator + bundleBean.bundle))
                    if (it.isActive){
                        it.resume(0)
                    }

                    fetch.removeListener(this)
                }

                override fun onError(download: Download, error: Error, throwable: Throwable?) {
                    if (it.isActive){
                        it.resume(0)
                    }
                    fetch.removeListener(this)
                }

                override fun onCancelled(download: Download) {
                    super.onCancelled(download)
                    if (it.isActive){
                        it.resume(0)
                    }
                    fetch.removeListener(this)
                }
            })
                    .enqueue(Request(bundleBean.bundle_url, file.absolutePath))

        }
    }
}