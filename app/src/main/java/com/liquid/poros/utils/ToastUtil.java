package com.liquid.poros.utils;

import android.widget.Toast;

import com.liquid.poros.PorosApplication;

public class ToastUtil {
    public static void show(String text){
        Toast.makeText(PorosApplication.context,text,Toast.LENGTH_SHORT).show();
    }
    public static void showL(String text){
        Toast.makeText(PorosApplication.context,text,Toast.LENGTH_LONG).show();
    }
}
