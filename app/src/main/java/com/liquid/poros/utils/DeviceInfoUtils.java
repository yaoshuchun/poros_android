package com.liquid.poros.utils;

import com.liquid.base.utils.ContextUtils;
import com.liquid.base.utils.GlobalConfig;
import com.liquid.base.utils.GsonUtil;
import com.liquid.poros.BuildConfig;
import com.liquid.poros.analytics.utils.MultiTracker;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.ui.activity.WelcomeActivity;
import com.liquid.poros.utils.network.HttpCallback;
import com.liquid.poros.utils.network.RetrofitHttpManager;
import com.liquid.stat.boxtracker.constants.StaticsConfig;
import com.liquid.stat.boxtracker.util.DeviceUtil;
import com.liquid.stat.boxtracker.util.LtNetworkUtil;

import java.util.HashMap;
import java.util.Map;

import okhttp3.RequestBody;

/**
 * Created by yejiurui on 2020/12/18.
 * Mail:yejiurui@liquidnetwork.com
 */
public class DeviceInfoUtils {

    public static int battery_rate;
    public static int temperature;

    /**
     * 全局参数获取
     *
     * @return
     */
    public static Map<String, String> getGlobalParams() {
        final Map<String, String> map = new HashMap<>();
        map.put(StaticsConfig.TrackerEventHardCodeParams.REPORT_ID, DeviceUtil.getUUID());
        map.put(StaticsConfig.TrackerEventHardCodeParams.MAC_ADDR, GlobalConfig.instance().getMAC());
        map.put(StaticsConfig.TrackerEventHardCodeParams.IMEI, GlobalConfig.instance().getIMEI());
        map.put(StaticsConfig.TrackerEventHardCodeParams.ANDROID_ID, GlobalConfig.instance().getANDROIDID());
        map.put(StaticsConfig.TrackerEventHardCodeParams.DEVICE_ID, GlobalConfig.instance().getDeviceID());
        map.put(StaticsConfig.TrackerEventHardCodeParams.OAID, GlobalConfig.instance().getOaid());
        map.put(StaticsConfig.TrackerEventHardCodeParams.VERSION_NAME, GlobalConfig.instance().getClientVersion());
        map.put(StaticsConfig.TrackerEventHardCodeParams.CHANNEL_NAME, GlobalConfig.instance().getChannelName());
        map.put(StaticsConfig.TrackerEventHardCodeParams.PHONE_MODEL, GlobalConfig.instance().getSystemModel());
        map.put(StaticsConfig.TrackerEventHardCodeParams.PHONE_BRAND, GlobalConfig.instance().getDeviceBrand());
        map.put(StaticsConfig.TrackerEventHardCodeParams.PHONE_MANUFACTURER, GlobalConfig.instance().getDeviceManufacturer());
        map.put(StaticsConfig.TrackerEventHardCodeParams.SYSTEM_VERSION, GlobalConfig.instance().getSystemVersion());
        map.put(StaticsConfig.TrackerEventHardCodeParams.CPU_INFO, GlobalConfig.instance().getCpuInfo());
        map.put(StaticsConfig.TrackerEventAlteringParams.USER_ID, AccountUtil.getInstance().getUserId());
        map.put(StaticsConfig.TrackerEventHardCodeParams.CPU_CORE_NUM, GlobalConfig.instance().getCpuCoreNum());
        map.put(StaticsConfig.TrackerEventHardCodeParams.TOTAL_RAM, GlobalConfig.instance().getTotalRam());
        map.put(StaticsConfig.TrackerEventHardCodeParams.DISPLAY_METRICS, GlobalConfig.instance().getDisplayMetrics());
        map.put(StaticsConfig.TrackerEventHardCodeParams.WIFI_MAC_ADDR, GlobalConfig.instance().getLocalMac());
        map.put(StaticsConfig.TrackerEventHardCodeParams.DEVICE_SERIAL, GlobalConfig.instance().getSerial());
        map.put(StaticsConfig.TrackerEventHardCodeParams.OPERATOR, GlobalConfig.instance().getOperator());
        map.put(StaticsConfig.TrackerEventHardCodeParams.NETWORK, LtNetworkUtil.getNetWorkType(ContextUtils.getApplicationContext()));
        map.put(StaticsConfig.TrackerEventHardCodeParams.REMAIN_CAPACITY, GlobalConfig.getTotalAvailableMemorySize(ContextUtils.getApplicationContext()));
        map.put(StaticsConfig.TrackerEventHardCodeParams.TOTAL_CAPACITY, GlobalConfig.getTotalMemorySize(ContextUtils.getApplicationContext()));
        map.put(StaticsConfig.TrackerEventHardCodeParams.SYSTEM_TIME, String.valueOf(System.currentTimeMillis()));
        map.put(StaticsConfig.TrackerEventHardCodeParams.EVENT_TIME, String.valueOf(System.currentTimeMillis()));
        map.put(StaticsConfig.TrackerEventHardCodeParams.PLATFORM_TYPE, BuildConfig.DEBUG ? "debug" : "release");
        map.put(StaticsConfig.TrackerEventHardCodeParams.TEMPERATURE,  String.valueOf(temperature));
        map.put(StaticsConfig.TrackerEventHardCodeParams.BATTERY_RATE, String.valueOf(battery_rate));
        return map;
    }


    /**
     * 上报设备信息
     */
    public static void uploadDeviceInfo() {
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json;charset=utf-8"),  GsonUtil.toJson(getGlobalParams()));
        RetrofitHttpManager.getInstance().httpInterface.device_info(body).enqueue(new HttpCallback() {
            @Override
            public void OnSucceed(String result) {
            }

            @Override
            public void OnFailed(int ret, String result) {

            }
        });
    }

    /**
     * 上报App安装列表信息
     */
    public static void uploadAppListInfo() {
        HashMap<String, String> params = new HashMap<>();
        params.put("app_name_list", CatchApplicationUtil.getSimpleAppList(ContextUtils.getApplicationContext()));
        MultiTracker.onEvent(TrackConstants.APPLICATION_INFO_LIST, params);
    }

}
