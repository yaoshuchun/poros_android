package com.liquid.poros.utils

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import com.airbnb.lottie.LottieAnimationView
import com.airbnb.lottie.LottieComposition
import com.blankj.utilcode.util.ZipUtils
import com.liquid.poros.constant.FileDownloadConstant
import com.liquid.poros.utils.FileDownloadUtil
import com.liquid.base.utils.ToastUtil
import java.io.File
import java.io.FileInputStream
import java.io.IOException

object LottieAnimationUtils {

    fun loadLottie(lottieAnimationView: LottieAnimationView,imagePath:String,dataPath : String,giftName:String):FileInputStream?{
        lottieAnimationView.setCacheComposition(false)
        val imageFiles = File(imagePath)
        val jsonFile = File(dataPath)
        var fis:FileInputStream? = null
        if (!imageFiles.exists()) {
            if (jsonFile.exists()) {
                fis = FileInputStream(jsonFile)
                try {
                    lottieAnimationView.setImageAssetDelegate(null)
                }catch (e : Exception) {

                }
                LottieComposition.Factory.fromInputStream(fis) {
                    it?.let {
                        it1 -> lottieAnimationView.setComposition(it1)
                    }
                    lottieAnimationView.playAnimation()
                }
            }else {
                FileDownloadUtil.download(giftName)
            }
            return fis
        }
        try {
            fis = FileInputStream(jsonFile)
            val absolutePath = imageFiles.absolutePath
            lottieAnimationView.setImageAssetDelegate() {
                val opts = BitmapFactory.Options()
                opts.inScaled = true
                var bitmap: Bitmap? = null
                bitmap = BitmapFactory.decodeFile(absolutePath + File.separator + it.fileName, opts)
                bitmap
            }
            LottieComposition.Factory.fromInputStream(fis) {
                it?.let { it1 -> lottieAnimationView.setComposition(it1) }
                lottieAnimationView.playAnimation()
            }
            return fis
        } catch (e: Exception) {

        }
        return null
    }

    fun unZip(imagePath: String,destPath:String) {
        try {
            val file = File(imagePath)
            val destFile = File(destPath)
            if (!destFile.exists()) {
                destFile.mkdir()
            }
            if (!file.exists()) return
            ZipUtils.unzipFile(file,destFile)
        } catch (e: Throwable) {
            e.printStackTrace()
        }
    }

    fun inputStreamClose(fis:FileInputStream?) {
        if (fis != null) {
            try {
                fis.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    fun getGiftFileName(giftId:Int):String {
        if (giftId > FileDownloadConstant.giftList.size){
            ToastUtil.show("请下载最新版本送此礼物")
            return ""
        }
        return FileDownloadConstant.giftList[giftId?.minus(1)]
    }
}