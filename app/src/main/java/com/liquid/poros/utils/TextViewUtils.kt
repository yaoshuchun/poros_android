package com.liquid.poros.utils

class TextViewUtils {
    companion object {
        fun getColoredSpanned(text: String, color: String): String {
            return "<font color=$color>$text</font>"
        }
    }
}