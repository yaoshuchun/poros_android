package com.liquid.poros.utils;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.List;

public class GenerateJsonUtil {
    /**
     * 生成json array
     *
     * @param arrs
     * @return
     * @throws JSONException
     */
    public static JSONArray jsonArray(List arrs) throws JSONException {
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < arrs.size(); i++) {
            jsonArray.put(i, arrs.get(i));
        }
        return jsonArray;
    }
}
