package com.liquid.poros.utils.network;


import com.liquid.poros.constant.Constants;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface HttpInterface {
    /*
     *              Retrofit 注解使用方式及规范
     * 1.请求方式注解
     *  @GET  get请求  @POST post请求  @PUT put请求  @DELETE delete请求
     *  其后添加（"xxx"）用于接收一个字符串表示接口path，与baseUrl组成完整的Url；也可以不添加（"xxx"），结合@Url注解使用
     *  url中可以使用变量，如（"blog/{id}"），并使用@Path("id")注解为前面例子中的id赋值
     *  @HTTP 可以用来替代上述几种请求方式，及其他扩展方法 如：@HTTP(method = "get", path = "blog/{id}", hasBody = false)
     *
     * 2.标记类注解
     *   @FormUrlEncoded 表单请求，表示请求体是From表单
     *   @Multipar 表示请求体是支持文件上传的From表单
     *   @Streaming 标记，表示响应体的数据是用流的形式返回 未使用该注解，默认会把数据全部载入内存，之后通过流获取数据也是读取内存中数据，所以返回数据较大时，需要使用该注解。
     *
     *  3.参数类注解
     *  @Headers 作用于该方法，添加请求头 （添加静态头部）使用 @Headers 注解设置固定的请求头 ，所有的请求头不会相互覆盖，即使名字相同，也全都会包含在请求中。
     *  @Header 作用于方法参数(形参), 添加不固定的Header （添加动态头部）使用 @Header 注解动态更新请求头，匹配的参数必须提供给 @Header ，若参数值为 null ，这个头会被省略，否则，会使用参数值的 toString 方法的返回值。
     *
     *  @Body 非表单请求体  （如果所传数据是json类型的话，需要配合相应的实体类使用该注解）如：
     *       @POST("users/new")
     *       Call<RequestBody> createUser(@Body User user);
     *
     *  @Field 表单字段，与FieldMap、FormUrlEncoded配合使用
     *  @FieldMap 表单字段，与 Field、FormUrlEncoded 配合；接受 Map<String, String> 类型，非 String 类型会调用 toString() 方法
     *  @Part	表单字段，与 PartMap 配合，适合文件上传情况
     *  @PartMap	表单字段，与 Part 配合，适合文件上传情况；默认接受 Map<String, RequestBody> 类型，非 RequestBody 会通过 Converter 转换
     *  @Path @Query @QueryMap @Url 都是用于URL，其中@Path用于url中的变量，@Url用于直接使用URL  剩下用于的类似于get请求的键值对
     *
     *  注意：
     *  1.Map 用来组合复杂的参数。
     *  2.Query、QueryMap 与 Field、FieldMap 功能一样，生成的数据形式一样；不同的是，Query、QueryMap 中的数据体现在 Url 上，Field、FieldMap 的数据是请求体；
     *  3.{占位符}和 PATH 尽量只用在URL的 path 部分，url 中的参数使用 Query、QueryMap 代替，保证接口的简洁；
     *  4：Query、Field、Part 支持数组和实现了 Iterable 接口的类型， 如 List、Set等，方便向后台传递数组 如：
     *      Call<ResponseBody> foo(@Query("ids[]") List<Integer> ids);
     *
     *
     * */

    /**
     * 微信登录
     * 参数名称                    是否必传
     * code                         Y
     *
     * @param body
     * @return
     */
    @POST("user/wechat_login")
    Call<ResponseBody> wechat_login(@Body RequestBody body);

    /**
     * qq 登录
     *
     * @param body
     * @return
     */
    @POST("user/qq_login")
    Call<ResponseBody> qq_login(@Body RequestBody body);


    /**
     * 用户信息查询
     * 参数名称                    是否必传
     * token                         Y
     *
     * @param body
     * @return
     */
    @POST("user/info")
    Call<ResponseBody> user_info(@Body RequestBody body);


    /**
     * 创建战队
     * 参数名称                    是否必传
     * token                         Y
     * group_name                    Y  //战队名字
     * group_type                    Y  //战队类型  0 公开  1私密
     * group_desc                    Y  //战队描述
     * group_icon                    Y  //战队图标
     *
     * @param body
     * @return
     */
    @POST("group/create")
    Call<ResponseBody> group_create(@Body RequestBody body);

    /**
     * 获取已加入的战队列表
     * 参数名称                    是否必传
     * token                         Y
     *
     * @param body
     * @return
     */
    @POST("user/user_group_list/v2")
    Call<ResponseBody> user_group_list(@Body RequestBody body);

    /**
     * APP更新版本
     *
     * @param app_name 项目名
     * @return
     */
    @GET("http://conf.liquidnetwork.com/appversion/app_update")
    Call<ResponseBody> checkAppUpdate(@Query("app_name") String app_name);

    /**
     * 消息历史记录
     * 参数名称                    是否必传
     * token                         Y
     * msgidClient                   N   //消息id
     * teamId                        Y   //战队id
     * direction                     N   //方向,forward：向前查询，backward：向后查询
     *
     * @param body
     * @return
     */
    @POST("chat_room/get_history_messages")
    Call<ResponseBody> chat_room_get_history_messages(@Body RequestBody body);

    /**
     * 用户离开聊天室已读消息位置上报
     * 参数名称                    是否必传
     * token                         Y
     * im_room_id                    Y  //聊天室id
     * msgidClient                   Y  //消息id
     *
     * @param body
     * @return
     */
    @POST("user/leave_room")
    Call<ResponseBody> leave_room(@Body RequestBody body);

    /**
     * 上传图片
     * 图片在MultipartBody 中 “image_file”
     *
     * @param body
     * @return
     */
    @POST("user/upload_image")
    Call<ResponseBody> user_upload_image(@Body MultipartBody body);

    /**
     * 领队招募上传图片
     *
     * @param body
     * @return
     */
    @POST("admin/upload_anchor_level")
    Call<ResponseBody> upload_anchor_level(@Body MultipartBody body);

    /**
     * 上传和平精英二维码组队图片
     *
     * @param body
     * @return
     */
    @POST("live_logic/upload_game_team_conf")
    Call<ResponseBody> upload_game_team_conf(@Body MultipartBody body);

    @POST("team_room/set_team_deep_link")
    Call<ResponseBody> set_team_deep_link(@Body MultipartBody body);

    @POST("live_logic/set_team_deep_link")
    Call<ResponseBody> hand_team_deep_link(@Body RequestBody body);

    /**
     * 上报错误
     * 参数名称                    是否必传
     * filepath                      Y  //错误日志
     * user_id                       Y  //上报用户id
     *
     * @param body
     * @return
     */
    @POST("live_monitor/upload_log")
    Call<ResponseBody> upload_log(@Body MultipartBody body);

    @POST("live_monitor/upload_trace_log")
    Call<ResponseBody> upload_trace_log(@Body RequestBody body);

    /**
     * 设置游戏信息
     * 参数名称                    是否必传
     * token                         Y
     * hpjy_game_nick                Y  //和平精英游戏昵称
     * hpjy_game_area                Y  //游戏分区
     *
     * @param body
     * @return
     */
    @POST("user/set_hpjy_account_info")
    Call<ResponseBody> set_hpjy_account_info(@Body RequestBody body);


    @POST("chat_room/check_free_mic_room")
    Call<ResponseBody> check_free_mic_room(@Body RequestBody body);

    /**
     * 查询用户是否在某聊天室被禁言
     * 参数名称                    是否必传
     * token                         Y
     * room_id                       Y  //房间id
     * target_user_id                Y  //被禁言用户id
     *
     * @param body
     * @return
     */
    @POST("chat_room/check_user_banned")
    Call<ResponseBody> check_user_banned(@Body RequestBody body);

    @POST("chat_room/refresh_live_room_seats")
    Call<ResponseBody> refresh_live_room_seats(@Body RequestBody body);

    @POST("chat_room/get_live_room_on_mic_and_wait")
    Call<ResponseBody> get_live_room_seats(@Body RequestBody body);


    @POST("chat_room/set_invite_newbie")
    Call<ResponseBody> set_invite_newbie(@Body RequestBody body);

    /**
     * 房间举报
     * 参数名称       是否必传
     * token            Y
     * room_id          Y  //身份id
     * reason           Y  //举报原因
     *
     * @param body
     * @return
     */
    @POST("chat_room/report_chat_room")
    Call<ResponseBody> report_chat_room(@Body RequestBody body);


    @POST("user/report_user")
    Call<ResponseBody> report_user(@Body RequestBody body);


    @POST("chat_room/check_live_banned")
    Call<ResponseBody> check_live_banned(@Body RequestBody body);

    @POST("user/set_gt_cid")
    Call<ResponseBody> set_gt_cid(@Body RequestBody body);

    @POST("chat_room/get_room_info_by_user")
    Call<ResponseBody> get_room_info_by_user(@Body RequestBody body);

    @POST("live_logic/set_mic_channel_conf")
    Call<ResponseBody> set_mic_channel_conf(@Body RequestBody body);

    @POST("live_logic/get_group_info_by_room_id")
    Call<ResponseBody> get_group_info_by_room_id(@Body RequestBody body);


    @POST("chat_room/check_user_online")
    Call<ResponseBody> check_user_online(@Body RequestBody body);


    @POST("chat_room/edit_live_room_admins")
    Call<ResponseBody> edit_live_room_admins(@Body RequestBody body);


    @POST("live_monitor/upload")
    Call<ResponseBody> uploadAnchorData(@Body RequestBody body);

    /**
     * 获取战队内身份
     * 参数名称       是否必传
     * token            Y
     * room_id          Y  //身份id
     *
     * @param body
     * @return
     */
    @POST("chat_room/get_live_room_roles")
    Call<ResponseBody> get_live_room_roles(@Body RequestBody body);

    /**
     * 修改用户信息
     * 参数名称       是否必传
     * token            Y
     * nick_name        N
     * avatar_url       N
     * gender           N
     * game_info        N
     *
     * @param body
     * @return
     */
    @POST("views/user/update_info")
    Call<ResponseBody> user_update_info(@Body RequestBody body);

    /**
     * 设置粉丝消息推送开关
     * 参数名称       是否必传
     * token            Y
     * push             Y 1 开,0 关
     *
     * @param body
     * @return
     */
    @POST("user/set_push_fans_msg")
    Call<ResponseBody> set_push_fans_msg(@Body RequestBody body);

    /**
     * 提现实名认证
     * 参数名称       是否必传
     * token            Y
     * real_name        Y  //身份证姓名
     * id_card          Y  //身份证号
     *
     * @param body
     * @return
     */
    @POST("user/real_name_id_card")
    Call<ResponseBody> real_name_id_card(@Body RequestBody body);

    /**
     * 修改战队内频道信息
     * 参数名称       是否必传
     * token            Y
     * room_id          Y  //频道id
     * group_id         Y  //战队id
     * room_name        Y  //修改后的频道名称
     *
     * @param body
     * @return
     */
    @POST("chat_room/update_info")
    Call<ResponseBody> chat_room_update_info(@Body RequestBody body);

    /**
     * 修改战队内主持人
     * 参数名称       是否必传
     * token            Y
     * room_id          Y  //频道id
     * target_user_id   Y  //目标id
     *
     * @param body
     * @return
     */
    @POST("chat_room/set_live_room_anchor")
    Call<ResponseBody> set_live_room_anchor(@Body RequestBody body);

    /**
     * 获取战队信息
     * 参数名称       是否必传
     * token            Y
     * group_id         Y  //战队id
     *
     * @param body
     * @return
     */
    @POST("group/info")
    Call<ResponseBody> group_info(@Body RequestBody body);

    /**
     * 任务列表
     * 参数名称       是否必传
     * token            Y
     * family_id        Y  //家族id
     *
     * @param body
     * @return
     */
    @POST("family/task_list")
    Call<ResponseBody> task_list(@Body RequestBody body);


    /**
     * 成员每日任务收益
     * 参数名称       是否必传
     * token            Y
     * family_id        Y  //家族id
     * time_stamp       Y  //时间戳(毫秒)
     *
     * @param body
     * @return
     */
    @POST("family/member_task_balance")
    Call<ResponseBody> member_task_balance(@Body RequestBody body);


    /**
     * 发放工资
     * 参数名称       是否必传
     * token            Y
     * family_id        Y  //家族id
     * percentage       Y  //群主自提奖励
     *
     * @param body
     * @return
     */
    @POST("family/paid_salary")
    Call<ResponseBody> paid_salary(@Body RequestBody body);

    /**
     * 检测当前是否在派单
     * team_id
     *
     * @param body
     * @return
     */
    @POST("team_room/newbie/check_if_dispatching")
    Call<ResponseBody> check_if_dispatching(@Body RequestBody body);


    /**
     * 族长自提工资信息
     * 参数名称       是否必传
     * token            Y
     * family_id        Y  //家族id
     *
     * @param body
     * @return
     */
    @POST("family/get_salary_info")
    Call<ResponseBody> get_salary_info(@Body RequestBody body);

    /**
     * 完成任务
     * 参数名称       是否必传
     * token            Y
     * task_id          Y  //任务id
     *
     * @param body
     * @return
     */
    @POST("family/finish_task")
    Call<ResponseBody> finish_task(@Body RequestBody body);

    /**
     * 用户今日总收益
     * 参数名称       是否必传
     * token            Y
     *
     * @param body
     * @return
     */
    @POST("user/get_account_balance")
    Call<ResponseBody> get_account_balance(@Body RequestBody body);

    @POST("user/get_config")
    Call<ResponseBody> get_config(@Body RequestBody body);

    /**
     * 微信支付
     * 参数名称       是否必传
     * token            Y
     * amount           Y  //充值金额
     * recharge_type    Y  //充值类型
     *
     * @param body
     * @return
     */
    @POST("recharge/wechat_order")
    Call<ResponseBody> wechat_order(@Body RequestBody body);

    /**
     * 支付宝支付
     * 参数名称       是否必传
     * token            Y
     * amount           Y  //充值金额
     * recharge_type    Y  //充值类型
     *
     * @param body
     * @return
     */
    @POST("recharge/alipay_order")
    Call<ResponseBody> alipay_order(@Body RequestBody body);

    /**
     * 获取和平精英海报
     * 参数名称       是否必传
     * token            Y
     *
     * @param body
     * @return
     */
    @POST("user/banner_list")
    Call<ResponseBody> banner_list(@Body RequestBody body);


    @POST("user/user_withdraw")
    Call<ResponseBody> user_withdraw(@Body RequestBody body);

    @POST("views/feed/like")
    Call<ResponseBody> feed_like(@Body RequestBody body);

    @GET("views/feed/moment_list")
    Call<ResponseBody> get_moment_list(@Query("token") String token,
                                       @Query("feed_user_id") String feed_user_id,
                                       @Query("last_id") String last_id);

    @GET("views/feed/comment_list")
    Call<ResponseBody> get_comment_list(@Query("token") String token,
                                        @Query("feed_id") String feed_id,
                                        @Query("need_feed_info") int need_feed_info, @Query("source") String source);

    /**
     * 家族的期总收益和天总收益
     * 参数名称       是否必传
     * token            Y
     * family_id        Y  //家族id
     *
     * @param body
     * @return
     */
    @POST("family/get_family_balance_info")
    Call<ResponseBody> get_family_balance_info(@Body RequestBody body);

    /**
     * 获取战队数据
     * 参数名称       是否必传
     * token            Y
     * group_id         Y  //战队id
     *
     * @param body
     * @return
     */
    @POST("user/user_group_info")
    Call<ResponseBody> user_group_info(@Body RequestBody body);

    /**
     * 家族收支记录
     * 参数名称       是否必传
     * token            Y
     * family_id        Y  //家族id
     * page_size        Y  //条数
     * page             Y  //页码
     *
     * @param body
     * @return
     */
    @POST("family/get_family_balance_record")
    Call<ResponseBody> get_family_balance_record(@Body RequestBody body);

    /**
     * 个人收支记录
     * 参数名称       是否必传
     * token            Y
     * page_size        Y  //条数
     * page             Y  //页码
     *
     * @param body
     * @return
     */
    @POST("user/get_user_balance_record")
    Call<ResponseBody> get_user_balance_record(@Body RequestBody body);

    /**
     * 家族的期排行和天排行
     * 参数名称       是否必传
     * token            Y
     * family_id        Y  //家族id
     * dimension        Y  //daily 天排行，phase 期排行
     *
     * @param body
     * @return
     */
    @POST("family/get_family_balance_rank")
    Call<ResponseBody> get_family_balance_rank(@Body RequestBody body);

    /**
     * 获取用户配置信息
     * 参数名称       是否必传
     * token            Y
     *
     * @param body
     * @return
     */
    @POST("user/get_global_config")
    Call<ResponseBody> get_global_config(@Body RequestBody body);


    /**
     * 战队组内删除频道（聊天室）
     * 参数名称       是否必传
     * token            Y
     * group_id         Y   //战队id
     * channel_id       Y   //频道id
     *
     * @param body
     * @return
     */
    @POST("group/delete_channel")
    Call<ResponseBody> group_delete_channel(@Body RequestBody body);

    /**
     * 加入战队组
     * 参数名称       是否必传
     * token            Y
     * group_id         Y  //战队id
     *
     * @param body
     * @return
     */
    @POST("group/join")
    Call<ResponseBody> group_join(@Body RequestBody body);


    /**
     * 创建频道
     * 参数名称       是否必传
     * token            Y
     * group_id         Y  //战队id
     * channel_name     Y  //标题
     * room_cover       Y  //图片
     * anchor           Y  //用户id
     * channel_type     Y  0:普通频道，1：语音频道，2：视频频道
     *
     * @param body
     * @return
     */
    @POST("group/create_channel")
    Call<ResponseBody> group_create_channel(@Body RequestBody body);


    /**
     * 战队成员查询
     * 参数名称       是否必传
     * token            Y
     * group_id         Y
     *
     * @param body
     * @return
     */
    @POST("group/group_member_list")
    Call<ResponseBody> group_group_member_list(@Body RequestBody body);

    /**
     * 发放奖励
     * 参数名称        是否必传
     * token            Y
     * family_id        Y  //家族id
     * target_user_ids  Y  //目标id列表
     * desc             Y  //奖励备注
     * reward_coin      Y  //奖励金额
     *
     * @param body
     * @return
     */
    @POST("family/give_out_reward")
    Call<ResponseBody> give_out_reward(@Body RequestBody body);


    /**
     * 获取战队通知列表
     * 参数名称        是否必传
     * token            Y
     * group_id         Y  //战队id
     *
     * @param body
     * @return
     */
    @POST("notifications/get_group_notification_list")
    Call<ResponseBody> get_group_notification_list(@Body RequestBody body);

    /**
     * 获取通知信息
     * 参数名称          是否必传
     * token              Y
     * notifications_id   Y  //通知表id
     *
     * @param body
     * @return
     */
    @POST("notifications/notifications_info")
    Call<ResponseBody> notifications_info(@Body RequestBody body);

    /**
     * 通知读后标记
     * 参数名称          是否必传
     * token              Y
     * notifications_id   Y  //通知表id
     *
     * @param body
     * @return
     */
    @POST("notifications/mark_read")
    Call<ResponseBody> mark_read(@Body RequestBody body);

    /**
     * 发送通知
     * 参数名称          是否必传
     * token              Y
     * group_id           Y  //战队id
     * title              Y  //消息标题
     * attach             Y  //消息内容
     * target_user_ids    N  //目标user_id
     * target_role_ids    N  //目标role_id
     *
     * @param body
     * @return
     */
    @POST("notifications/send_notifications")
    Call<ResponseBody> send_notifications(@Body RequestBody body);

    /**
     * 删除通知
     * 参数名称          是否必传
     * token              Y
     * notification_id    Y  //群公告id
     *
     * @param body
     * @return
     */
    @POST("notifications/del_notification")
    Call<ResponseBody> del_notification(@Body RequestBody body);

    /**
     * 获取最新群未读通知
     * 参数名称          是否必传
     * token              Y
     * group_id           Y  //群id
     *
     * @param body
     * @return
     */
    @POST("notifications/get_group_newest_notification")
    Call<ResponseBody> get_group_newest_notification(@Body RequestBody body);

    /**
     * 获取热门战队列表
     * 参数名称       是否必传
     * token            Y
     * page             Y  //页码
     *
     * @param body
     * @return
     */
    @POST("group/hot_list_v2")
    Call<ResponseBody> group_hot_list(@Body RequestBody body);

    /**
     * 获取战队训练列表
     * 参数名称       是否必传
     * token            Y
     *
     * @param body
     * @return
     */
    @POST("live_logic/get_channel_list_v2")
    Call<ResponseBody> get_channel_list(@Body RequestBody body);

    /**
     * 获取直播间提示语配置信息(未使用)
     * 参数名称       是否必传
     * token            Y
     *
     * @param body
     * @return
     */
    @POST("live_logic/get_live_config")
    Call<ResponseBody> get_live_config(@Body RequestBody body);

    /**
     * 获取是否有需要评价的直播
     * 参数名称       是否必传
     * token            Y
     * room_id          Y  //房间id
     *
     * @param body
     * @return
     */
    @POST("user/need_score_anchor")
    Call<ResponseBody> need_score_anchor(@Body RequestBody body);

    /**
     * 获取开黑房间信息
     * 参数名称       是否必传
     * token            Y
     * team_id          Y  //开黑房间id
     *
     * @param body
     * @return
     */
    @POST("team_room/get_team_info")
    Call<ResponseBody> get_team_info(@Body RequestBody body);

    /**
     * 加入开黑房间
     * 参数名称       是否必传
     * token            Y
     * team_id          Y  //开黑房间id
     * is_invite        Y  //是否邀请 1是 0否
     *
     * @param body
     * @return
     */
    @POST("team_room/join_team")
    Call<ResponseBody> join_team(@Body RequestBody body);


    /**
     * 获取领队列表 视频
     *
     * @param body
     * @return
     */
    @POST("user/get_leader_list")
    Call<ResponseBody> get_leader_list(@Body RequestBody body);

    /**
     * 踢出开黑房间
     * 参数名称       是否必传
     * token            Y
     * team_id          Y  //开黑房间id
     * target_user_id   Y  //目标用户id
     *
     * @param body
     * @return
     */
    @POST("team_room/kick_out_team_user")
    Call<ResponseBody> kick_out_team_user(@Body RequestBody body);

    /**
     * 踢出开黑房间（未使用）
     * 参数名称       是否必传
     * token            Y
     * team_id          Y  //开黑房间id
     * target_user_id   Y  //目标用户id
     *
     * @param body
     * @return
     */
    @POST("team_room/ban_team_user")
    Call<ResponseBody> ban_team_user(@Body RequestBody body);

    /**
     * 关闭开黑房间
     * 参数名称       是否必传
     * token            Y
     * team_id          Y  //开黑房间id
     *
     * @param body
     * @return
     */
    @POST("team_room/close_team")
    Call<ResponseBody> close_team(@Body RequestBody body);


    /**
     * 退出开黑房间
     * 参数名称       是否必传
     * token            Y
     * team_id          Y  //开黑房间id
     *
     * @param body
     * @return
     */
    @POST("team_room/quit_team")
    Call<ResponseBody> quit_team(@Body RequestBody body);


    /**
     * 战队消息撤回
     * 参数名称       是否必传
     * token            Y
     * msgidClient      Y  //消息id
     * group_id         Y  //群组id
     * room_id          Y  //频道id
     *
     * @param body
     * @return
     */
    @POST("chat_room/recall_message")
    Call<ResponseBody> recall_message(@Body RequestBody body);

    /**
     * 战队名模糊查询
     * 参数名称              是否必传
     * token                 Y
     * group_name            Y  //查询关键字
     *
     * @param body
     * @return
     */
    @POST("group/search_group")
    Call<ResponseBody> group_search_group(@Body RequestBody body);

    /**
     * 创建战队身份
     * 参数名称                 是否必传
     * token                     Y
     * group_id                  Y  //战队id
     * role_id                   Y  //身份id
     * role_name                 Y  //身份名称
     * role_color                Y  //身份颜色
     * group_permissions         Y  //群权限
     * enter_channel             Y  //进入频道权限作用域
     * speak_channel             Y  //频道发言权限作用域
     * ban_user                  Y  //禁言权限作用域
     * ban_msg                   Y  //删除消息作用域
     *
     * @param body
     * @return
     */
    @POST("group_role/create_role")
    Call<ResponseBody> create_role(@Body RequestBody body);

    /**
     * 更新战队身份
     * 参数名称                 是否必传
     * token                     Y
     * group_id                  Y  //战队id
     * role_id                   Y  //身份id
     * role_name                 Y  //身份名称
     * role_color                Y  //身份颜色
     * group_permissions         Y  //群权限
     * enter_channel             Y  //进入频道权限作用域
     * speak_channel             Y  //频道发言权限作用域
     * ban_user                  Y  //禁言权限作用域
     * ban_msg                   Y  //删除消息作用域
     *
     * @param body
     * @return
     */
    @POST("group_role/update_role")
    Call<ResponseBody> update_role(@Body RequestBody body);

    /**
     * 获取自己在某个战队里的角色信息
     * 参数名称                 是否必传
     * token                     Y
     * group_id                  Y  //战队id
     *
     * @param body
     * @return
     */
    @POST("group_role/get_user_role_in_group")
    Call<ResponseBody> get_user_role_in_group(@Body RequestBody body);

    @POST("chat_room/online_heart_beat")
    Call<ResponseBody> online_heart_beat(@Body RequestBody body);

    /**
     * 直播间用户心跳
     * 参数名称                 是否必传
     * token                     Y
     * room_id                   Y  //直播间房间id
     * action                    Y  //权限action   详见VoiceAction类
     *
     * @param body
     * @return
     */
    @POST("chat_room/user_heart_beat")
    Call<ResponseBody> user_heart_beat(@Body RequestBody body);

    /**
     * 校验直播间在线
     * 参数名称                 是否必传
     * token                     Y
     * room_id                   Y  //直播间房间id
     *
     * @param body
     * @return
     */
    @POST("chat_room/check_room_online")
    Call<ResponseBody> check_room_online(@Body RequestBody body);

    /**
     * 家族
     * 参数名称                 是否必传
     * token                     Y
     * family_id                 Y  //家族id
     *
     * @param body
     * @return
     */
    @POST("family/enter_family")
    Call<ResponseBody> enter_family(@Body RequestBody body);

    /**
     * 获取战队内所有角色列表
     * 参数名称                 是否必传
     * token                     Y
     * group_id                  Y  //战队id
     *
     * @param body
     * @return
     */
    @POST("group_role/get_group_roles")
    Call<ResponseBody> get_group_roles(@Body RequestBody body);

    /**
     * 删除战队身份
     * 参数名称                 是否必传
     * token                     Y
     * role_id                   Y  //身份id
     * group_id                  Y  //战队id
     *
     * @param body
     * @return
     */
    @POST("group_role/delete_role")
    Call<ResponseBody> delete_role(@Body RequestBody body);

    /**
     * 按身份查找成员列表（支持分页）
     * 参数名称                 是否必传
     * token                     Y
     * role_id                   Y  //身份id
     * index                     Y  //页码
     *
     * @param body
     * @return
     */
    @POST("group_role/get_users_by_role_id")
    Call<ResponseBody> get_users_by_role_id(@Body RequestBody body);

    /**
     * 战队成员昵称模糊查询
     * 参数名称                 是否必传
     * token                     Y
     * group_id                  Y  //战队id
     * nick_name                 Y  //群成员昵称
     *
     * @param body
     * @return
     */
    @POST("group/search_user_in_group")
    Call<ResponseBody> search_user_in_group(@Body RequestBody body);

    /**
     * 分配战队身份
     * 参数名称                 是否必传
     * token                     Y
     * group_id                  Y  //战队id
     * role_id                   Y  //身份id
     * target_users              N  //目标用户id
     * target_user_role_ids      N  //目标用户身份
     *
     * @param body
     * @return
     */
    @POST("group_role/assign_role")
    Call<ResponseBody> assign_role(@Body RequestBody body);


    /**
     * 转让战队
     * 参数名称       是否必传
     * token            Y
     * group_id         Y  //战队id
     * target_user_id   Y  //目标用户id
     *
     * @param body
     * @return
     */
    @POST("group/transfer_group_master")
    Call<ResponseBody> transfer_group_master(@Body RequestBody body);


    /**
     * 新进战队默认身份
     * 参数名称       是否必传
     * token            Y
     * group_id         Y  //战队id
     * role_id          Y  //身份id
     *
     * @param body
     * @return 修改群默认身份后，先进群的人还是原来分配的身份，只有新进入的人会被分配新的默认身份(注:身份包含 管理员和普通成员)
     */
    @POST("group_role/edit_group_default_role")
    Call<ResponseBody> edit_group_default_role(@Body RequestBody body);


    @POST("group_role/reset_default_role")
    Call<ResponseBody> reset_default_role(@Body RequestBody body);

    /**
     * 查询其他人在战队组中身份
     * 参数名称       是否必传
     * token            Y
     * group_id         Y  //战队id
     * user_id          Y  //目标用户id
     * team_id          Y  //房间id
     *
     * @param body
     * @return
     */
    @POST("group_role/query_user_role")
    Call<ResponseBody> query_user_role(@Body RequestBody body);

    /**
     * 修改战队信息
     * 参数名称       是否必传
     * token            Y
     * group_id         Y  //战队id
     * group_name       N  //新战队名称
     * group_notice     N  //新战队公告
     * group_desc       N  //新战队描述
     * group_icon       N  //新战队图标
     *
     * @param body
     * @return
     */
    @POST("group/update_info")
    Call<ResponseBody> group_update_info(@Body RequestBody body);

    /**
     * 解散战队
     * 参数名称       是否必传
     * token            Y
     * group_id         Y  //战队id
     *
     * @param body
     * @return
     */
    @POST("group/delete")
    Call<ResponseBody> group_delete(@Body RequestBody body);

    /**
     * 获取所加入战队未读信息条数和最新消息时间需要配合一下接口使用
     * 参数名称       是否必传
     * token            Y
     *
     * @param body
     * @return
     */
    @POST("user/unread_messages")
    Call<ResponseBody> user_unread_messages(@Body RequestBody body);

    /**
     * 获取战队内未读消息
     * 参数名称       是否必传
     * token            Y
     * group_id         Y //战队id
     *
     * @param body
     * @return
     */
    @POST("user/group_unread_messages")
    Call<ResponseBody> group_unread_messages(@Body RequestBody body);


    /**
     * 踢人出战队
     * 参数名称       是否必传
     * token            Y
     * group_id         Y //战队id
     * kick_user_id     Y //被踢用户id
     *
     * @param body
     * @return
     */
    @POST("group/kick_group_member")
    Call<ResponseBody> group_kick_group_member(@Body RequestBody body);


    /**
     * 禁言用户
     * 参数名称       是否必传
     * token            Y
     * group_id         Y  //战队id
     * target_user_id   Y  //被禁言用户id
     * room_id          Y  //房间id
     * duration         Y  //禁言时长
     *
     * @param body
     * @return
     */
    @POST("chat_room/ban_user")
    Call<ResponseBody> chat_room_ban_user(@Body RequestBody body);


    /**
     * 主动退出战队
     * 参数名称       是否必传
     * token            Y
     * group_id         Y  //战队id
     *
     * @param body
     * @return
     */
    @POST("group/leave_group")
    Call<ResponseBody> group_leave_group(@Body RequestBody body);


    /**
     * 创建评价信息
     * 参数名称       是否必传
     * token            Y
     * room_id          Y // 房间id
     * anchor           Y // 直播id
     * score            Y // 评分 1-5分
     * tags             Y // 标签
     * comment          Y //评价描述 100字以内
     *
     * @param body
     * @return
     */
    @POST("user/score_anchor")
    Call<ResponseBody> score_anchor(@Body RequestBody body);

    /**
     * 获取评价列表
     * 参数名称       是否必传
     * token            Y
     * page             Y  // 页码 从0开始Y
     * page_size        Y  // 页面条数 默认20
     *
     * @param body
     * @return
     */
    @POST("user/get_anchor_score_list")
    Call<ResponseBody> get_anchor_score_list(@Body RequestBody body);

    /**
     * 开黑房间列表
     * 参数名称       是否必传
     * token            Y
     * group_id         N  //战队组id
     * page             Y  // 页码 从0开始Y
     * page_size        Y  // 页面条数 默认20
     *
     * @param body
     * @return
     */
    @POST("team_room/get_team_list")
    Call<ResponseBody> get_team_list(@Body RequestBody body);

    /**
     * 创建开黑房间
     * 参数名称       是否必传
     * token            Y
     * group_id         Y //战队组id
     * team_name        Y  // 房间名称
     * game_nick_name   Y  // 游戏昵称
     * game_zone        Y  // 游戏分区
     * members_total    Y  // 组队人数
     * is_public        Y  // 是否公开
     * rcv_order        Y  // 是否同步接单
     *
     * @param body
     * @return
     */
    @POST("team_room/create_team")
    Call<ResponseBody> create_team(@Body RequestBody body);

    /**
     * 更新房间信息
     * 参数名称       是否必传
     * token            Y
     * group_id         Y //战队组id
     * team_name        Y  // 房间名称
     * game_nick_name   Y  // 游戏昵称
     * game_zone        Y  // 游戏分区
     * members_total    Y  // 组队人数
     * is_public        Y  // 是否公开
     *
     * @param body
     * @return
     */
    @POST("team_room/update_team_info")
    Call<ResponseBody> update_team_info(@Body RequestBody body);

    /**
     * 设置邀请开黑
     *
     * @param body
     * @return
     */
    @POST("team_room/set_p2p_invite_rule")
    Call<ResponseBody> set_p2p_invite_rule(@Body RequestBody body);

    /**
     * 获取邀请开黑
     *
     * @param body
     * @return
     */
    @POST("team_room/get_p2p_invite_rule")
    Call<ResponseBody> get_p2p_invite_rule(@Body RequestBody body);

    /**
     * 添加好友
     * 参数名称       是否必传
     * token            Y
     * target_user_id   Y  //目标用户id
     *
     * @param body
     * @return
     */
    @POST("user/add_friend")
    Call<ResponseBody> add_friend(@Body RequestBody body);


    @POST("chat_room/couple_room_ban_user_speak")
    Call<ResponseBody> couple_room_ban_user_speak(@Body RequestBody body);

    /**
     * 删除好友
     * 参数名称       是否必传
     * token            Y
     * target_user_id   Y  //目标用户id
     *
     * @param body
     * @return
     */
    @POST("user/remove_friend")
    Call<ResponseBody> remove_friend(@Body RequestBody body);

    /**
     * 获取好友列表
     * 参数名称       是否必传
     * token            Y
     *
     * @param body
     * @return
     */
    @POST("user/get_friend_list")
    Call<ResponseBody> get_friend_list(@Body RequestBody body);

    /**
     * 获取好友消息列表
     * 参数名称       是否必传
     * token            Y
     * need_friend      Y  是否好友  1 好友消息  0 粉丝消息
     *
     * @param body
     * @return
     */
    @POST("p2p_message/get_msg_session_list")
    Call<ResponseBody> get_friend_msg_list(@Body RequestBody body);

    /**
     * 获取好友&粉丝消息列表
     * 参数名称       是否必传
     * token            Y
     *
     * @param body
     * @return
     */
    @POST("p2p_message/get_msg_session_list_v3")
    Call<ResponseBody> get_msg_session_list_v3(@Body RequestBody body);

    /**
     * 获取好友消息列表（好友+粉丝消息）
     * 参数名称       是否必传
     * token            Y
     *
     * @param body
     * @return
     */
    @POST("p2p_message/get_msg_session_list_v2")
    Call<ResponseBody> get_friend_msg_list_v2(@Body RequestBody body);

    /**
     * 上报已读消息
     * 参数名称       是否必传
     * token            Y
     * msg_id           Y  //已读最新消息id
     * target_user_id   Y  //目标用户id
     *
     * @param body
     * @return
     */
    @POST("p2p_message/read_msg")
    Call<ResponseBody> read_msg(@Body RequestBody body);

    /**
     * 获取私聊历史消息列表
     * 参数名称       是否必传
     * token            Y
     * msg_id           N  //取比该消息id更早的消息
     * limit            N  //获取更早的20条消息
     * target_user_id   Y  //目标用户id
     *
     * @param body
     * @return
     */
    @POST("p2p_message/get_msg_list")
    Call<ResponseBody> get_msg_list(@Body RequestBody body);

    /**
     * 获取目标用户信息
     * 参数名称       是否必传
     * token            Y
     * target_user_id   Y  //目标用户id
     *
     * @param body
     * @return
     */
    @POST("user/get_msg_user_info")
    Call<ResponseBody> get_msg_user_info(@Body RequestBody body);

    @POST("views/cp_score/hint")
    Call<ResponseBody> cp_score_hint(@Body RequestBody body);

    @POST("user/send_gift")
    Call<ResponseBody> send_gift(@Body RequestBody body);

    /**
     * 获取首页数据
     * 参数名称       是否必传
     * token            Y
     * group_id         Y //战队id
     *
     * @param body
     * @return
     */
    @POST("user/user_group_info/v2")
    Call<ResponseBody> index_user_group_info(@Body RequestBody body);

    /**
     * 删除消息会话
     * 参数名称       是否必传
     * token            Y
     * target_user_id   Y //目标用户id
     *
     * @param body
     * @return
     */
    @POST("p2p_message/delete_msg_session")
    Call<ResponseBody> delete_msg(@Body RequestBody body);

    /**
     * 置顶消息
     * 参数名称       是否必传
     * token            Y
     * target_user_id   Y //目标用户id
     *
     * @param body
     * @return
     */
    @POST("p2p_message/pin_msg")
    Call<ResponseBody> top_msg(@Body RequestBody body);

    /**
     * 取消置顶消息
     * 参数名称       是否必传
     * token            Y
     * target_user_id   Y //目标用户id
     *
     * @param body
     * @return
     */
    @POST("p2p_message/unpin_msg")
    Call<ResponseBody> untop_msg(@Body RequestBody body);

    /**
     * 获取大厅在线用户列表
     * 参数名称       是否必传
     * token            Y
     *
     * @param body
     * @return
     */
    @POST("user/online_user_list")
    Call<ResponseBody> online_newbie_list(@Body RequestBody body);

    /**
     * 用户心跳
     * 参数名称       是否必传
     * token            Y
     *
     * @param body
     * @return
     */
    @POST("user/user_active_heartbeat")
    Call<ResponseBody> user_active_heartbeat(@Body RequestBody body);

    /**
     * 新用户进入聊天室发送欢迎消息和开黑邀请
     * 参数名称       是否必传
     * token            Y
     *
     * @param body
     * @return
     */
    @POST("user/welcome_newbie")
    Call<ResponseBody> welcome_newbie(@Body RequestBody body);

    /**
     * 询问是否有空闲萌新领队
     * 参数名称       是否必传
     * token            Y
     *
     * @param body
     * @return
     */
    @POST("team_room/newbie/has_free_leader")
    Call<ResponseBody> has_free_leader(@Body RequestBody body);

    /**
     * 萌新领队开始接单
     * 参数名称       是否必传
     * token            Y
     * team_name        Y  //萌新领队开黑房间名
     * game_nick_name   Y  //萌新领队开黑和平精英游戏昵称
     * game_zone        Y  //萌新领队开黑游戏分区
     *
     * @param body
     * @return
     */
    @POST("team_room/newbie_leader/start_rcv_order")
    Call<ResponseBody> start_rcv_order(@Body RequestBody body);

    /**
     * 萌新领队抢单
     * 参数名称       是否必传
     * token            Y
     * team_id          Y  //萌新开黑房间id
     *
     * @param body
     * @return
     */
    @POST("team_room/newbie_leader/grab_order")
    Call<ResponseBody> grab_order(@Body RequestBody body);

    /**
     * 萌新领队继续接单
     * 参数名称       是否必传
     * token            Y
     * team_id          N  //同步接单房间id
     *
     * @param body
     * @return
     */
    @POST("team_room/newbie_leader/open_rcv_order")
    Call<ResponseBody> open_rcv_order(@Body RequestBody body);

    /**
     * 萌新拒绝开黑邀请
     * 参数名称       是否必传
     * token            Y
     *
     * @param body
     * @return
     */
    @POST("team_room/newbie/refuse_team_invite")
    Call<ResponseBody> refuse_team_invite(@Body RequestBody body);

    @POST("team_room/refuse_p2p_team")
    Call<ResponseBody> refuse_p2p_team(@Body RequestBody body);

    /**
     * 萌新发起开黑订单
     * 参数名称       是否必传
     * token            Y
     * team_id          N //开黑房间id ,接单超时，继续接单时传
     *
     * @param body
     * @return
     */
    @POST("team_room/newbie/start_team_order")
    Call<ResponseBody> start_team_order(@Body RequestBody body);

    /**
     * 萌新领队停止接单
     * 参数名称       是否必传
     * token            Y
     *
     * @param body
     * @return
     */
    @POST("team_room/newbie_leader/stop_rcv_order")
    Call<ResponseBody> stop_rcv_order(@Body RequestBody body);

    /**
     * 萌新领队获取接单信息
     * 参数名称       是否必传
     * token            Y
     *
     * @param body
     * @return
     */
    @POST("team_room/newbie_leader/get_rcv_order_info")
    Call<ResponseBody> get_rcv_order_info(@Body RequestBody body);

    /**
     * 私聊撤回消息
     * 参数名称       是否必传
     * token            Y
     * target_user_id   Y  //目标用户id
     * msg_id           Y  //消息id
     *
     * @param body
     * @return
     */
    @POST("p2p_message/recall_msg")
    Call<ResponseBody> p2p_recall_msg(@Body RequestBody body);

    /**
     * 设置萌新领队自动发送消息
     * 参数名称       是否必传
     * token            Y
     * preset_msg       Y  //预设消息
     * send_msg         Y  //是否开启自送发送消息给萌新  true 是  false 否
     *
     * @param body
     * @return
     */
    @POST("team_room/newbie_leader/set_leader_auto_send_msg")
    Call<ResponseBody> set_leader_auto_send_msg(@Body RequestBody body);

    /**
     * 获取萌新领队设置自动发送消息
     * 参数名称       是否必传
     * token            Y
     *
     * @param body
     * @return
     */
    @POST("team_room/newbie_leader/get_leader_auto_send_msg")
    Call<ResponseBody> get_leader_auto_send_msg(@Body RequestBody body);

    /**
     * // 0关闭 1开启 2派单确认中
     *
     * @param body
     * @return
     */
    @POST("team_room/get_leader_wait_status")
    Call<ResponseBody> get_leader_wait_status(@Body RequestBody body);

    /**
     * 获取领队信息
     * 参数名称              是否必传
     * token                  Y
     * target_leader_id       Y  //领队id
     *
     * @param body
     * @return
     */
    @POST("user/other_leader/info")
    Call<ResponseBody> get_leader_info(@Body RequestBody body);

    /**
     * 获取用户资料信息
     * 参数名称              是否必传
     * token                  Y
     * target_leader_id       Y  //目标id
     *
     * @param body
     * @return
     */
    @POST("user/other_info")
    Call<ResponseBody> get_other_info(@Body RequestBody body);

    /**
     * 领取每日奖励点赞卡
     * 参数名称              是否必传
     * token                  Y
     *
     * @param body
     * @return
     */
    @POST("user/get_daily_gift_card")
    Call<ResponseBody> get_daily_gift_card(@Body RequestBody body);

    @POST("user/get_daily_prize_coin")
    Call<ResponseBody> get_daily_prize_coin(@Body RequestBody body);

    /**
     * 点赞卡变动记录
     * 参数名称              是否必传
     * token                  Y
     * page                   Y  //第几页0
     * page_size              Y  //每页20
     *
     * @param body
     * @return
     */
    @POST("user/gift_card_change_record")
    Call<ResponseBody> gift_card_change_record(@Body RequestBody body);

    /**
     * 设置年龄段
     * 参数名称              是否必传
     * token                  Y
     * age_level              Y   // 1：小于10岁, 2：10-16岁，3：17-25岁, 4：大于26岁
     *
     * @param body
     * @return
     */
    @POST("user/set_age_level")
    Call<ResponseBody> set_age_level(@Body RequestBody body);

    @POST("user/logout")
    Call<ResponseBody> logout(@Body RequestBody body);

    /**
     * 一起玩房间列表
     * 参数名称              是否必传
     * token                  Y
     * page                   Y  //第几页0
     * page_size              Y  //每页20
     *
     * @param body
     * @return
     */
    @POST("together_room/list")
    Call<ResponseBody> together_room_list(@Body RequestBody body);


    /**
     * 开启一起玩房间
     * 参数名称              是否必传
     * token                  Y
     * room_id                Y  //房间id
     * room_title             Y  //房间名字
     * room_cover             Y //房间封面
     *
     * @param body
     * @return
     */
    @POST("together_room/open")
    Call<ResponseBody> together_room_open(@Body RequestBody body);

    @POST("together_room/open_grab_mic")
    Call<ResponseBody> open_grab_mic(@Body RequestBody body);

    @POST("together_room/close_grab_mic")
    Call<ResponseBody> close_grab_mic(@Body RequestBody body);


    @POST("together_room/grab_mic")
    Call<ResponseBody> grab_mic(@Body RequestBody body);

    @POST("together_room/confirm_on_mic")
    Call<ResponseBody> confirm_on_mic(@Body RequestBody body);


    @POST("together_room/agree_on_mic")
    Call<ResponseBody> agree_on_mic(@Body RequestBody body);


    @POST("together_room/kickout_mic")
    Call<ResponseBody> kickout_mic(@Body RequestBody body);


    @POST("together_room/lock_mic")
    Call<ResponseBody> lock_mic(@Body RequestBody body);

    @POST("together_room/cancel_lock_mic")
    Call<ResponseBody> cancel_lock_mic(@Body RequestBody body);


    @POST(" together_room/quit_on_mic")
    Call<ResponseBody> quit_on_mic(@Body RequestBody body);


    @POST("together_room/close")
    Call<ResponseBody> close_together_room(@Body RequestBody body);

    @POST("user/assistant_monitor_record")
    Call<ResponseBody> assistant_monitor_record(@Body RequestBody body);

    @POST("together_room/quit_room")
    Call<ResponseBody> upload_room_duration(@Body RequestBody body);


    @POST("together_room/info")
    Call<ResponseBody> get_together_room(@Body RequestBody body);

    @POST("together_room/cancel_wait_mic")
    Call<ResponseBody> cancel_wait_mic(@Body RequestBody body);

    @POST("together_room/set_channel_id")
    Call<ResponseBody> set_channel_id(@Body RequestBody body);


    @POST("together_room/wait_mic")
    Call<ResponseBody> wait_mic(@Body RequestBody body);


    @POST("together_room/switch_mic")
    Call<ResponseBody> switch_mic(@Body RequestBody body);

    /**
     * 上报同盾检测结果
     * 参数名称              是否必传
     * token                  Y
     * action                 Y  //检测事件 activate激活  login登录
     * k1                     Y  //跟随服务器返回 动态参数
     *
     * @param body
     * @return
     */
    @POST("user/decision")
    Call<ResponseBody> decision(@Body RequestBody body);


    @POST("together_room/refuse_team_invite")
    Call<ResponseBody> refuse_together_invite(@Body RequestBody body);

    /**
     * 加入黑名单
     * 参数名称              是否必传
     * token                  Y
     * target_user_id         Y  //目标用户id
     *
     * @param body
     * @return
     */
    @POST("user/block_user")
    Call<ResponseBody> block_user(@Body RequestBody body);

    /**
     * 移出黑名单
     * 参数名称              是否必传
     * token                  Y
     * target_user_id         Y  //目标用户id
     *
     * @param body
     * @return
     */
    @POST("user/unblock_user")
    Call<ResponseBody> unblock_user(@Body RequestBody body);

    /**
     * 文字频道(房间)消息免打扰
     * 参数名称              是否必传
     * token                  Y
     * room_id                Y  //频道id
     * is_ignore              Y  //是否屏蔽
     *
     * @param body
     * @return
     */
    @POST("chat_room/set_push_msg")
    Call<ResponseBody> set_push_msg(@Body RequestBody body);

    /**
     * 开启组CP直播
     * 参数名称              是否必传
     * token                  Y
     * room_id                Y  //房间id
     * room_title             Y  //房间介绍
     * room_cover             Y  //房间封面
     *
     * @param body
     * @return
     */
    @POST("couple/open_room")
    Call<ResponseBody> couple_open_room(@Body RequestBody body);

    /**
     * 获取充值页信息
     * 参数名称              是否必传
     * token                  Y
     *
     * @param body
     * @return
     */
    @POST("recharge/info")
    Call<ResponseBody> recharge_info(@Body RequestBody body);

    /**
     * 账单明细
     * 参数名称              是否必传
     * token                  Y
     *
     * @param body
     * @return
     */
    @POST("user/coin_change_record")
    Call<ResponseBody> coin_change_record(@Body RequestBody body);

    /**
     * 组cp房间列表
     * 参数名称              是否必传
     * token                  Y
     *
     * @param body
     * @return
     */
    @POST("couple/room_list/v2")
    Call<ResponseBody> couple_room_list(@Body RequestBody body);

    /**
     * 关闭组CP直播
     * 参数名称              是否必传
     * token                  Y
     *
     * @param body
     * @return
     */
    @POST("couple/close_room")
    Call<ResponseBody> couple_close_room(@Body RequestBody body);

    @POST("couple/room/is_on_mic")
    Call<ResponseBody> is_on_mic(@Body RequestBody body);

    /**
     * 购买CP卡/邀请组CP/同意组CP
     * 参数名称              是否必传
     * token                  Y
     * card_type              Y  //cp卡类型
     * card_price             Y  //cp卡价格
     * room_id                N  //房间ID
     * room_anchor            N  //房间主持人ID
     * target_user_id         Y  //目标用户ID
     * cp_order_id            N  //同意组cp传递 邀请id
     *
     * @param body
     * @return
     */
    @POST("couple/buy_cp_card")
    Call<ResponseBody> buy_couple_card(@Body RequestBody body);

    /**
     * 拒绝组CP
     * 参数名称              是否必传
     * token                  Y
     * cp_order_id            Y  //邀请id
     *
     * @param body
     * @return
     */
    @POST("couple/couple_invite/refuse")
    Call<ResponseBody> refuse_couple_invite(@Body RequestBody body);

    /**
     * 组CP房间详情
     * token                  Y
     * room_id                Y  //房间ID
     *
     * @param body
     * @return
     */
    @POST("couple/room_info")
    Call<ResponseBody> couple_room_info(@Body RequestBody body);

    /**
     * 获取可购买CP卡列表
     * token                  Y
     * target_user_id         Y  //目标用户ID
     * cp_order_id            N  //同意组cp传递 邀请id
     *
     * @param body
     * @return
     */
    @POST("couple/cp_card_list")
    Call<ResponseBody> couple_card_list(@Body RequestBody body);

    /**
     * 组队房间设置通道id
     *
     * @param body
     * @return
     */
    @POST("couple/room_set_channel_id")
    Call<ResponseBody> room_set_channel_id(@Body RequestBody body);

    /**
     * 上麦接口
     *
     * @param body
     * @return
     */
    @POST("couple/room_on_mic/v2")
    Call<ResponseBody> room_on_mic(@Body RequestBody body);


    @POST("couple/room_quit_on_mic")
    Call<ResponseBody> room_quit_on_mic(@Body RequestBody body);

    /**
     * 继续上麦接口
     * token                  Y
     * room_id                Y  //房间id
     * mic_card_price         Y  //上麦卡价格
     *
     * @param body
     * @return
     */
    @POST("couple/room_continue_on_mic")
    Call<ResponseBody> room_continue_on_mic(@Body RequestBody body);

    /**
     * 开关摄像头接口
     *
     * @param body
     * @return
     */
    @POST("couple/room_switch_camera")
    Call<ResponseBody> room_switch_camera(@Body RequestBody body);

    /**
     * 充值结果查询
     * token                  Y
     * order_id               Y  //订单ID
     *
     * @param body
     * @return
     */
    @POST("recharge/result")
    Call<ResponseBody> recharge_result(@Body RequestBody body);

    /**
     * 获取开播房间信息
     * token                  Y
     * room_id                Y  //房间id
     *
     * @param body
     * @return
     */
    @POST("couple/room_open_info")
    Call<ResponseBody> room_open_info(@Body RequestBody body);

    /**
     * 获取个人金币余额
     * token                  Y
     *
     * @param body
     * @return
     */
    @POST("user/rest_coin")
    Call<ResponseBody> user_rest_coin(@Body RequestBody body);

    /**
     * 踢下麦
     *
     * @param body
     * @return
     */
    @POST("couple/room_kickout_mic")
    Call<ResponseBody> room_kickout_mic(@Body RequestBody body);

    /**
     * 禁止上麦接口
     *
     * @param body
     * @return
     */
    @POST("couple/room_ban_user")
    Call<ResponseBody> room_ban_user(@Body RequestBody body);

    /**
     * 用户行为上报
     * token                  Y
     * room_id                N  //房间id
     * target_user_id         N  //3号麦用户id、私聊对方id
     * record_type            Y  //行为类型
     * anchor                 N  //主播id
     *
     * @param body
     * @return
     */
    @POST("user/upload_behavior")
    Call<ResponseBody> upload_behavior(@Body RequestBody body);

    /**
     * 主持人邀请加入专属开黑房间
     *
     * @param body
     * @return
     */
    @POST("couple/room/team/invite/v2")
    Call<ResponseBody> inviteCoupleTeam(@Body RequestBody body);


    @POST("couple/room/team/invite/agree")
    Call<ResponseBody> agreeCoupleTeam(@Body RequestBody body);

    @POST("couple/room/team/invite/refuse")
    Call<ResponseBody> refuseCoupleTeam(@Body RequestBody body);

    @POST("user/bind_wechat")
    Call<ResponseBody> bind_wechat(@Body RequestBody body);

    /**
     * 设置基础信息
     * token                  Y
     * age_level              Y  //年龄
     * gender                 Y  //性别
     * games                  Y  //游戏
     *
     * @param body
     * @return
     */
    @POST("user/set_basic_info")
    Call<ResponseBody> set_basic_info(@Body RequestBody body);

    @POST("post/list")
    Call<ResponseBody> getPostList(@Body RequestBody body);

    /**
     * 发布动态
     * token                  Y
     * content                Y  //文字描述
     * image_urls             Y  //图片地址
     * video_url              Y  //视频地址
     *
     * @param body
     * @return
     */
    @POST("post/send")
    Call<ResponseBody> post_send(@Body RequestBody body);

    /**
     * 广场列表
     *
     * @param body
     * @return
     */
    @POST("couple/user/list")
    Call<ResponseBody> getCpUserList(@Body RequestBody body);

    /**
     * 广场弹窗邀请
     * token                  Y
     *
     * @param body
     * @return
     */
    @POST("couple/cproom/invite")
    Call<ResponseBody> index_cp_invite(@Body RequestBody body);

    @POST("user/upload_image_oss")
    Call<ResponseBody> upload_image_oss(@Body MultipartBody body);

    /**
     * 个人中心信息
     * token                  Y
     * target_user_id         Y  //目标用户id
     *
     * @param body
     * @return
     */
    @POST("views/user/info/other")
    Call<ResponseBody> user_info_other(@Body RequestBody body);

    /**
     * 获取个人相册列表
     * token                  Y
     * page                   Y  //页
     * page_size              Y  //默认20
     *
     * @param body
     * @return
     */
    @POST("user/photos")
    Call<ResponseBody> user_photos(@Body RequestBody body);

    /**
     * 删除相片
     * token                  Y
     * photo_id               Y  //相片id
     *
     * @param body
     * @return
     */
    @POST("user/photo/delete")
    Call<ResponseBody> user_photo_delete(@Body RequestBody body);

    /**
     * 购买专属语音体验
     *
     * @param body
     * @return
     */
    @POST("couple/voice_chat/buy")
    Call<ResponseBody> voice_chat_buy(@Body RequestBody body);


    @POST("team_room/join_cp_team")
    Call<ResponseBody> join_cp_room(@Body RequestBody body);

    /**
     * 获取可购买CP卡列表
     * token                  Y
     * target_user_id         Y  //目标用户ID
     * cp_order_id            N  //同意组cp传递 邀请id
     *
     * @param body
     * @return
     */
    @POST("couple/cp_card_list/v2")
    Call<ResponseBody> couple_cp_card_list_v2(@Body RequestBody body);

    /**
     * 获取cp首次私聊开黑自动消息
     * token                  Y
     * target_user_id         Y  //目标用户ID
     *
     * @param body
     * @return
     */
    @POST("p2p_message/cp_first_msg")
    Call<ResponseBody> cp_first_msg(@Body RequestBody body);

    /**
     * 获取可购买CP卡列表
     * token                  Y
     * target_user_id         Y  //目标用户ID
     * card_category          Y  //卡类型  1.TEAM 开黑卡 2.MIC 上麦卡 3.CP仅CP卡 4.TEAM+CP 开黑和CP卡
     * is_room                Y  //是否直播间内
     *
     * @param body
     * @return
     */
    @POST("couple/sale/card/list")
    Call<ResponseBody> couple_sale_card_list(@Body RequestBody body);

    /**
     * 使用/购买开黑卡
     * token                  Y
     * target_user_id         Y  //目标用户ID
     * room_id                N  //CP直播间id
     * only_use_card          Y  //true是仅使用开黑卡 false为购买并使用开黑卡
     * price                  N  //购买开黑卡花费金币
     *
     * @param body
     * @return
     */
    @POST("couple/team_card/use")
    Call<ResponseBody> couple_team_card_user(@Body RequestBody body);

    /**
     * 使用/购买上麦卡
     * token                  Y
     * room_id                N  //CP直播间id
     * only_use_card          Y  //true是仅使用开黑卡 false为购买并使用开黑卡
     * mic_card_price         N  //购买上麦卡花费金币
     *
     * @param body
     * @return
     */
    @POST("couple/room_on_mic/v2")
    Call<ResponseBody> couple_room_on_mic_v2(@Body RequestBody body);

    /**
     * token                  Y
     * target_user_id         Y  //true是仅使用开黑卡 false为购买并使用开黑卡
     * clicked                Y  //是否点击邀请组CP
     * clicked_team           Y  //是否点击上麦开黑
     *
     * @param body
     * @return
     */
    @POST("couple/expt_cp/available")
    Call<ResponseBody> expt_cp_available(@Body RequestBody body);

    /**
     * token                  Y
     * target_user_id         Y  //true是仅使用开黑卡 false为购买并使用开黑卡
     * clicked                Y  //是否点击邀请组CP
     * clicked_team           Y  //是否点击上麦开黑
     *
     * @param body
     * @return
     */
    @POST("p2p_message/audio_msg_read")
    Call<ResponseBody> p2p_audio_msg_read(@Body RequestBody body);

    /**
     * 取消充值订单
     * token                  Y
     * out_trade_no           Y  //充值订单ID
     *
     * @param body
     * @return
     */
    @POST("recharge/close_order")
    Call<ResponseBody> recharge_close_order(@Body RequestBody body);

    /**
     * 获取游戏静态信息
     * token                  Y
     *
     * @param body
     * @return
     */
    @POST("views/user/game/static")
    Call<ResponseBody> user_game(@Body RequestBody body);

    /**
     * 足迹触发
     * token                  Y
     * dispatch_type          Y
     * 触发场景：
     * 1 # 新手女嘉宾派单
     * 2 # 用户上线派单
     * 3 # 爆灯派单
     * 4 # 广场详情
     * 5 # 退出语音房
     * 6 # 发送消息
     *
     * @param body
     * @return
     */
    @POST("views/dispatch/by_type")
    Call<ResponseBody> user_dispatch_female(@Body RequestBody body);

    /**
     * 设备信息
     *
     * @param body
     * @return
     */
    @POST("views/user/device_info")
    Call<ResponseBody> device_info(@Body RequestBody body);

    /**
     * 申请上麦
     * token                  Y
     * room_id	              Y  房间ID
     *
     * @param body
     * @return
     */
    @POST("couple/waitinglist")
    Call<ResponseBody> couple_waiting_list(@Body RequestBody body);

    /**
     * 离开视频房,取消上麦申请
     * token                  Y
     * room_id	              Y  房间ID
     *
     * @param body
     * @return
     */
    @POST("couple/video_room/leave")
    Call<ResponseBody> couple_mic_leave(@Body RequestBody body);


    /**
     * CP等级信息
     *
     * @param body
     * @return
     */
    @POST("views/cp_score/detail")
    Call<ResponseBody> cp_score_detail(@Body RequestBody body);

    /**
     * 礼物排行榜
     *
     * @param body
     * @return
     */
    @POST("views/gift_rank/rank_list")
    Call<ResponseBody> get_rank_list(@Body RequestBody body);

    /**
     * 视频房女嘉宾信息
     *
     * @param body
     * @return
     */
    @POST("views/video_room/user_info/other")
    Call<ResponseBody> video_room_user_info(@Body RequestBody body);

    /**
     * cp等级弹框显示回调
     *
     * @param body
     * @return
     */
    @POST("views/cp_score/check_rank_raise")
    Call<ResponseBody> check_rank_raise(@Body RequestBody body);

    /**
     * cp等级大礼包弹框显示回调
     *
     * @param body
     * @return
     */
    @POST("views/cp_score/raise_group_back")
    Call<ResponseBody> raise_group_back(@Body RequestBody body);

    /**
     * 申请进入专属房间
     * token                  Y
     * room_id	              Y  房间ID
     *
     * @param body
     * @return
     */
    @POST("couple/private_room/request")
    Call<ResponseBody> couple_private_room(@Body RequestBody body);

    /**
     * 专属房间心跳
     * token                  Y
     * room_id	              Y  房间ID
     *
     * @param body
     * @return
     */
    @POST("views/video_room/private_room/heart_beat")
    Call<ResponseBody> couple_private_room_heart_beat(@Body RequestBody body);

    /**
     * 专属房间上麦
     * token                  Y
     * room_id	              Y  房间ID
     *
     * @param body
     * @return
     */
    @POST("couple/private_room/on_mic")
    Call<ResponseBody> couple_private_room_on_mic(@Body RequestBody body);

    /**
     * 发布评论
     *
     * @param body
     * @return
     */
    @POST("views/feed/comment")
    Call<ResponseBody> upload_comment(@Body RequestBody body);

    /**
     * 专属房间列表
     * token                  Y
     *
     * @param body
     * @return
     */
    @POST("views/video_room/private_room/list")
    Call<ResponseBody> couple_private_room_list(@Body RequestBody body);

    /**
     * 获取房间类型
     * token                  Y
     * room_id	              Y  房间ID
     *
     * @param body
     * @return
     */
    @POST("views/video_room/details")
    Call<ResponseBody> couple_video_room_details(@Body RequestBody body);

    /**
     * 查看晋级详情
     * token                  Y
     * room_id	              Y  房间ID
     *
     * @param body
     * @return
     */
    @POST("users/cp_level/level_detail")
    Call<ResponseBody> checkCpLevelDetail(@Body RequestBody body);


    /**
     * 随机宣言内容
     * token                  Y
     * room_id	              Y  房间ID
     *
     * @param body
     * @return
     */
    @POST("users/cp_level/random_swear")
    Call<ResponseBody> getRandomSwear(@Body RequestBody body);

    /**
     * 随机宣言内容
     * token                  Y
     * room_id	              Y  房间ID
     *
     * @param body
     * @return
     */
    @POST("users/cp_level/send_advance")
    Call<ResponseBody> sendAdvance(@Body RequestBody body);


    /**
     * 取消速配请求/退出匹配房间
     *
     * @param body
     * @return
     */
    @POST("views/match_room/quit")
    Call<ResponseBody> cancelMatchRoom(@Body RequestBody body);

    /**
     * 设置速配channelId
     *
     * @param body
     * @return
     */
    @POST("views/match_room/room_set_channel_id")
    Call<ResponseBody> setChannelId(@Body RequestBody body);

    /**
     * 速配房间信息
     * @param body
     * @return
     */
    @POST("views/match_room/room_info")
    Call<ResponseBody> matchRoomInfo(@Body RequestBody body);

    /**
     * 速配信息设置
     *
     * @param body
     * @return
     */
    @POST("views/match_room/match_info")
    Call<ResponseBody> fetchAudioMatchInfo(@Body RequestBody body);

    /**
     * 开始速配
     *
     * @param body
     * @return
     */
    @POST("views/match_room/request")
    Call<ResponseBody> startAudioMatch(@Body RequestBody body);

    /**
     * 拒绝开黑邀请
     *
     * @param body
     * @return
     */
    @POST("views/match_room/refuse")
    Call<ResponseBody> matchRoomRefuse(@Body RequestBody body);

    /**
     * 检测是否有开黑时长
     *
     * @param body
     * @return
     */
    @POST("couple/has_team_card")
    Call<ResponseBody> hasTeamCard(@Body RequestBody body);
    /**
     * 动态广场
     *
     * @param
     * @return
     */
    @GET("views/feed/square_moment_list")
    Call<ResponseBody> feedSquareList(@Query("token") String token,@Query("page") int page);
    /**
     * 动态操作上报
     * token                  Y
     * feed_id	              Y  动态ID
     * action	              Y  操作类型 10 -播放语音,11 - 查看图片
     * source	              Y  动态来源
     * @return
     */
    @POST("views/feed/action")
    Call<ResponseBody> feedActionReport(@Body RequestBody body);


    /**
     * 小额礼包支付下单
     * 参数名称       是否必传
     * token            Y
     * pay_amount           Y  //充值金额
     * pay_way    Y  //充值类型 1 微信  2支付宝
     *
     * @param body
     * @return
     */
    @POST("views/reward/small_packet/pay")
    Call<ResponseBody> small_packet_pay(@Body RequestBody body);
    /**
     * 拉去 连麦列表
     * token                  Y
     * room_id                Y  //房间ID
     *
     * @param body
     * @return
     */
    @POST("couple/waitinglist/list")
    Call<ResponseBody> pull_mic_list(@Body RequestBody body);
    /**
     * 拉取专属房间申请列表
     *
     * @param body
     * @return
     */
    @POST( "couple/private_room/request_list")
    Call<ResponseBody> private_room_request_list(@Body RequestBody body);

    /**
     * 获取当前聊天页的套装
     *
     * @param body
     * @return
     */
    @GET( "views/cartoon/suit")
    Call<ResponseBody> cartoon_suit(@Query("other_user_id") String is_all,@Query("token") String token);

    /**
     * 卡通形象下载列表
     *
     * @param is_all 0=增量请1=全量请求
     * @return
     */
    @GET( "views/cartoon/download_list")
    Call<ResponseBody> download_list(@Query("is_all") int is_all,@Query("token") String token);

    @POST("views/reward/red_cent/pay")
    Call<ResponseBody> red_cent_pay (@Body RequestBody body);
    /**
     * 获取用户等级
     * @param body
     * @return
     */
    @POST("views/user/get_user_level")
    Call<ResponseBody> getUserLevel (@Body RequestBody body);

    @GET( "views/cp_room_diversion/b_user_delete_info")
    Call<ResponseBody> leaveRoom(@Query("token") String token);

    //上传颜值图片
    @Multipart
    @POST("live_monitor/beauty_detect")
    Call<ResponseBody> uploadFaceImg(@Part MultipartBody.Part photo, @Part("token") RequestBody username, @Part("room_id") RequestBody room_id);
    //获取云信音视频通话，互动直播 具体方法：joinChannel() token
    @GET("user/im_video_token")
    Call<ResponseBody> getNERtcToken(@Query("user_id") String user_id,@Query("token") String token);
    //盲盒 - 入口点击记录嘉宾
    @POST("gift/blind_box_intro_click")
    Call<ResponseBody> blindBoxIntroClick (@Body RequestBody body);
}




