package com.liquid.poros.utils;

import android.graphics.Bitmap;
import android.os.CountDownTimer;
import com.faceunity.wrapper.faceunity;
import com.liquid.base.utils.LogOut;
import com.liquid.im.kit.custom.RoomActionAttachment;
import com.liquid.poros.PorosApplication;
import com.liquid.poros.constant.CoupleVoiceAction;
import com.liquid.poros.entity.FaceInfo;
import com.liquid.poros.utils.network.HttpCallback;
import com.liquid.poros.utils.network.RetrofitHttpManager;
import com.netease.lava.nertc.sdk.video.NERtcVideoView;
import com.netease.lava.webrtc.EglRenderer;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.chatroom.ChatRoomMessageBuilder;
import com.netease.nimlib.sdk.chatroom.ChatRoomService;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * 视频房开口&露脸采集
 */
public class RoomLiveMonitorHelper {
	private volatile static RoomLiveMonitorHelper instance;

	private NERtcVideoView mNeRtcVideoView;
	private VideoViewProvider videoViewProvider;
	private String mRoomId;
	private long lastGetFaceDataTime;//上一次获取正脸及开口数据的时间
	private final long getFacedataInterval = 1000;//每隔多长时间取脸部一次
	//正脸
	private final List<Integer> faceFrameList= new ArrayList<>(35);//每一帧是否是正脸，每秒可能会回调很多帧，比如30次
	private final List<Integer> faceSecondList= new ArrayList<>(20);//每秒内 n多帧是否有正脸的,每15秒清空一次
	private final List<Integer[]> faceResultArr = new ArrayList<>(2);//2个length为3的构成
	private int faceYesCountPer15 = 0;//每15秒钟的正脸数量

	private final List<Integer> faceFiveSecondList= new ArrayList<>(20);//每秒内 n多帧是否有正脸的,每5秒清空一次
	private int faceYesCountPer5 = 0;//每5秒钟的正脸数量

	//========================================
	//开口
	private final List<Integer> mouseFrameList= new ArrayList<>(35);//每一帧是否是开口，每秒可能会回调很多帧，比如30次
	private final List<Integer> mouseSecondList= new ArrayList<>(20);//每秒内 n多帧是否有开口的
	private final List<Integer[]> mouseResultArr = new ArrayList<>(2);//2个length为3的构成
	private int mouseYesCountPer15 = 0;//每15秒钟的开口数量

	private final List<Integer> mouseFiveSecondList= new ArrayList<>(10);//每秒内 n多帧是否有开口的
	private int mouseYesCountPer5 = 0;//每5秒钟的开口数量

	private final int TYPE_NO = 0;//非正脸 非开口
	private final int TYPE_YES = 1;//正脸，开口
	private final int COUNT_5 = 5;//5秒
	private final int COUNT_15 = 15;//15秒
	private final int COUNT_30 = 30;//30秒
	private final int COUNT_60 = 60;//60秒


	private int tickCounter = -1;////第一次回调不处理相关逻辑，保证是15秒每次，从0开始则是14

	private final float[] mouseFloats = new float[75 * 3];
	private final float[] faceFloats = new float[3];

	private final float centerFaceThreshold = 0.3f;//-1 -  0 - 1，90 - 0 - 90
	private final double openMouseThreshold = 6d;//测试不张嘴默认为4.xxx，不露脸默认为1.xxx
	private final int volumeThreshold = 50;//0 - 100

	//
	private int roomLiveLocalAverageVolume;//音量平均值
	private int volumeUpdateCount;//音量更新次数
	private boolean uploadedImg;//
	private CountDownTimer countDownTimer;
	private String micPosition;
	private RoomLiveMonitorHelper() {

	}

	public static RoomLiveMonitorHelper instance(){
		if (null == instance){
			synchronized (RoomLiveMonitorHelper.class){
				if (null == instance){
					instance = new RoomLiveMonitorHelper();
				}
			}
		}
		return instance;
	}

	public synchronized FaceInfo getFaceInfo() {
		//数据都为空 则返回null，心跳作用接口 就不再添加 face_info信息
		//理论上 faceResultArr 和 mouseResultArr 要么同时有数据，要么同时size为0
		if (faceResultArr.size() < 1 && mouseResultArr.size() < 1){
			return null;
		}

		FaceInfo faceInfo = new FaceInfo();

		List<Integer[]> arrayList = new ArrayList<>(2);
		arrayList.addAll(faceResultArr);
		faceInfo.face = arrayList;

		arrayList = new ArrayList<>(2);
		arrayList.addAll(mouseResultArr);
		faceInfo.mouth = arrayList;

		faceInfo.room_id = mRoomId;
		faceInfo.user_id = AccountUtil.getInstance().getUserId();
		faceInfo.mic_position = micPosition;

		faceResultArr.clear();
		// 清空list
		mouseResultArr.clear();
		return faceInfo;
	}
	public void updateRoomLiveLocalAudioVolume(int volume) {
		this.roomLiveLocalAverageVolume  = (roomLiveLocalAverageVolume * volumeUpdateCount + volume) / ( ++ volumeUpdateCount);
	}

	private void resetVolume(){
		roomLiveLocalAverageVolume = 0;
		volumeUpdateCount = 0;
	}


	public void start(NERtcVideoView neRtcVideoView, String roomId,String micPosition) {
		destroy();
		this.mNeRtcVideoView = neRtcVideoView;
		this.mRoomId = roomId;
		this.micPosition = micPosition;
		startTimer();
	}

	public void start(VideoViewProvider viewProvider, String roomId,String micPosition) {
		destroy();
		this.videoViewProvider = viewProvider;
		this.mRoomId = roomId;
		this.micPosition = micPosition;
		startTimer();
	}

	private void startTimer(){
		stopTimer();
		countDownTimer = new CountDownTimer(Integer.MAX_VALUE, 1_000){

			@Override
			public void onTick(long millisUntilFinished) {
				long l = System.currentTimeMillis();
				tickCounter++;
				//第一次回调不处理相关逻辑
				if (tickCounter <= 0)
					return;

				//理论上这两个数组size是一样的
				int size = Math.min(faceFrameList.size(),mouseFrameList.size());
				int face = TYPE_NO;//非正脸
				int mouse = TYPE_NO;//非开口
				for (int i = 0; i < size; i++) {

					if (face == TYPE_NO){
						face = faceFrameList.get(i);
					}

					if (mouse == TYPE_NO){
						mouse = mouseFrameList.get(i);
					}

					if (face != TYPE_NO && mouse != TYPE_NO){
						break;
					}

				}

				//正脸
				faceSecondList.add(face);
				faceFiveSecondList.add(face);
				if (face == TYPE_YES){
					faceYesCountPer15++;
					faceYesCountPer5++;
				}
				//是否正脸帧数据清空
				faceFrameList.clear();
				//=======================================
				//开口
				mouseSecondList.add(face);
				mouseFiveSecondList.add(face);
				//并且音量大于阈值
				LogOut.debug("mouseResultArr", "roomLiveLocalAverageVolume: "+ roomLiveLocalAverageVolume);
				if (mouse == TYPE_YES && roomLiveLocalAverageVolume > volumeThreshold){
					mouseYesCountPer15++;
					mouseYesCountPer5++;
				}
				//是否开口帧数据清空
				mouseFrameList.clear();
				resetVolume();


				//每15秒钟
				if (tickCounter % COUNT_15 == 0){

					//正脸
					Integer[] arr = new Integer[3];
					arr[0] = faceSecondList.size();
					arr[1] = faceYesCountPer15;
					arr[2] = arr[0] - arr[1];

					faceResultArr.add(arr);

					faceYesCountPer15 = 0; //正脸计数清0
					faceSecondList.clear();//上15秒数据清空

					LogOut.debug("faceResultArr", "COUNT_15: "+ Arrays.toString(faceResultArr.get(0)));


					//===========================================================================
					//开口
					Integer[] mouseArr = new Integer[3];
					mouseArr[0] = mouseSecondList.size();
					mouseArr[1] = mouseYesCountPer15;
					mouseArr[2] = mouseArr[0] - mouseArr[1];

					mouseResultArr.add(mouseArr);

					mouseYesCountPer15 = 0; //正脸计数清0
					mouseSecondList.clear();//上15秒数据清空

					LogOut.debug("mouseResultArr", "COUNT_15: "+ Arrays.toString(mouseResultArr.get(0)));

				}

				//每5秒
				if (tickCounter % COUNT_5 == 0) {
					//正脸数据
					faceFiveSecondList.clear();//上5秒数据清空
					faceYesCountPer5 = 0;//正脸计数清0

					//发送开口采集数据
					sendMouseDataMessage();
					mouseFiveSecondList.clear();//上5秒数据清空
					mouseYesCountPer5 = 0;//正脸计数清0
				}
				//每30秒钟
				/*if (tickCounter % COUNT_30 == 0){

					//发送数据，

					//正脸
					//发送 faceResultArr

					LogOut.debug("faceResultArr", "COUNT_30: "+ Arrays.toString(faceResultArr.get(0)));
					LogOut.debug("faceResultArr", "COUNT_30: "+ Arrays.toString(faceResultArr.get(1)));

					// 清空list
					faceResultArr.clear();

					//=======================================
					//开口
					//发送 mouseResultArr

					LogOut.debug("mouseResultArr", "COUNT_30: "+ Arrays.toString(mouseResultArr.get(0)));
					LogOut.debug("mouseResultArr", "COUNT_30: "+ Arrays.toString(mouseResultArr.get(1)));

					// 清空list
					mouseResultArr.clear();
				}*/


				//打印 1，或 0，时间很短
				LogOut.debug("onTick", "time: "+ (System.currentTimeMillis() - l));


			}

			@Override
			public void onFinish() {

			}
		};
		countDownTimer.start();
	}

	private void stopTimer(){
		if (null != countDownTimer){
			countDownTimer.cancel();
			countDownTimer = null;
		}
	}

	public void destroy() {
		stopTimer();
		//正脸
		faceFrameList.clear();
		faceSecondList.clear();//每秒内 n多帧是否有正脸的
		faceResultArr.clear();//2个length为3的构成
		faceYesCountPer15 = 0;//每15秒钟的正脸数量

		faceFiveSecondList.clear();//每秒内 n多帧是否有正脸的
		faceYesCountPer5 = 0;//每5秒钟的正脸数量
		//========================================
		//开口
		mouseFrameList.clear();//每一帧是否是开口，每秒可能会回调很多帧，比如30次
		mouseSecondList.clear();//每秒内 n多帧是否有开口的
		mouseResultArr.clear();//2个length为3的构成
		mouseYesCountPer15 = 0;//每15秒钟的开口数量

		mouseFiveSecondList.clear();//每秒内 n多帧是否有开口的
		mouseYesCountPer5 = 0;//每5秒钟的开口数量

		tickCounter = -1;////第一次回调不处理相关逻辑，保证是15秒每次，从0开始则是14

		//
		resetVolume();

		uploadedImg = false;
		mNeRtcVideoView = null;
		mRoomId = null;
		micPosition = null;
		lastGetFaceDataTime = 0;
	}



	/**
	 * 在FURenderer.onDrawFrame系列方法之后调用
	 */
	public void getFaceData(){
		if (System.currentTimeMillis() - lastGetFaceDataTime < getFacedataInterval){
			return;
		}
		faceunity.fuGetFaceInfo(0, "landmarks_ar", mouseFloats);
		faceunity.fuGetFaceInfo(0, "rotation_euler", faceFloats);

		double mouseDistance = point3dDistance(mouseFloats[62 * 3], mouseFloats[62 * 3 + 1], mouseFloats[62 * 3 + 2],
				mouseFloats[59 * 3], mouseFloats[59 * 3 + 1], mouseFloats[59 * 3 + 2]);

		LogOut.debug("onVideoCallback", "mouseFloatsTop: "+ mouseFloats[62*3]+"---"+ mouseFloats[62*3+1]+"---"+ mouseFloats[62*3+2]);
		LogOut.debug("onVideoCallback", "mouseFloatsBottom: "+ mouseFloats[59*3]+"---"+ mouseFloats[59*3+1]+"---"+ mouseFloats[59*3+2]);
		LogOut.debug("onVideoCallback", "mouseDistance: "+ mouseDistance);
		LogOut.debug("onVideoCallback", "脸部数量: "+ faceunity.fuIsTracking());


		LogOut.debug("onVideoCallback", "faceFloats: "+ faceFloats[0]+"---"+ faceFloats[1]+"---"+ faceFloats[2]);



		//正脸
		if (faceunity.fuIsTracking() > 0 && Math.abs(faceFloats[0]) < centerFaceThreshold && Math.abs(faceFloats[1]) < centerFaceThreshold){
			faceFrameList.add(TYPE_YES);
			if (!uploadedImg && null != getNeRtcVideoView() && tickCounter > 1){//大于1秒钟，等待美颜生效
				uploadedImg = true;
				uploadFaceImg();

			}
		} else {//非正脸
			faceFrameList.add(TYPE_NO);

		}

		//开口
		if (faceunity.fuIsTracking() > 0 && mouseDistance > openMouseThreshold){
			mouseFrameList.add(TYPE_YES);
		} else {//非开口
			mouseFrameList.add(TYPE_NO);

		}
		lastGetFaceDataTime = System.currentTimeMillis();
	}

	/**
	 * 上传颜值图片
	 */
	private void uploadFaceImg(){
		if (null == getNeRtcVideoView())
			return;
		//每add一次就回调一次
		getNeRtcVideoView().addFrameListener(new EglRenderer.FrameListener() {
			@Override
			public void onFrame(Bitmap bitmap) {
				ExecutorService executorService = Executors.newCachedThreadPool();
				executorService.execute(new Runnable() {
					@Override
					public void run() {
						LogOut.debug("onFrame", "testSnapshot onFrame: " + bitmap);
						try {
							File file = new File(PorosApplication.context.getCacheDir() + "/faceLevel.jpg");
							FileOutputStream out = new FileOutputStream(file);
							bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
							out.flush();
							out.close();

							String fileName = file.getName();
							RequestBody photoRequestBody = RequestBody.create(MediaType.parse("image/*"), file);
							MultipartBody.Part photo = MultipartBody.Part.createFormData("image_file", fileName, photoRequestBody);

							RequestBody token = RequestBody.create(null, AccountUtil.getInstance().getAccount_token());
							RequestBody roomId = RequestBody.create(null, mRoomId+"");

							RetrofitHttpManager.getInstance().httpInterface.uploadFaceImg(photo,token,roomId).enqueue(new HttpCallback() {
								@Override
								public void OnSucceed(String result) {
									file.delete();

								}

								@Override
								public void OnFailed(int ret, String result) {
									file.delete();
								}
							});

						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
				executorService.shutdown();

			}

		},1.0f);
	}

	/**
	 * 获取三维 两个点的距离
	 * @param x1
	 * @param y1
	 * @param z1
	 * @param x2
	 * @param y2
	 * @param z2
	 * @return
	 */
	public static double point3dDistance(float x1, float y1, float z1, float x2, float y2, float z2) {
		return Math.sqrt(Math.pow(Math.abs(x1 - x2), 2) + Math.pow(Math.abs(y1 - y2), 2) + Math.pow(Math.abs(z1 - z2), 2));
	}



	public NERtcVideoView getNeRtcVideoView() {
		if (null == mNeRtcVideoView && null != videoViewProvider){
			mNeRtcVideoView = videoViewProvider.provide();
		}
		return mNeRtcVideoView;
	}

	public interface VideoViewProvider{
		NERtcVideoView provide();
	}

	/**
	 * 发送5s开口采集数据
	 */
	private void sendMouseDataMessage(){
		RoomActionAttachment roomActionAttachment = new RoomActionAttachment();
		roomActionAttachment.setAccount(AccountUtil.getInstance().getUserId());
		roomActionAttachment.setAction(CoupleVoiceAction.LIVE_MOUSE_MONITOR.getCode());
		roomActionAttachment.setMouse5sList(mouseFiveSecondList);
		//设置男用户麦位
		roomActionAttachment.setPos(1);
		ChatRoomMessage message = ChatRoomMessageBuilder.createChatRoomCustomMessage(mRoomId, roomActionAttachment);
		NIMClient.getService(ChatRoomService.class).sendMessage(message, false);
	}
}
