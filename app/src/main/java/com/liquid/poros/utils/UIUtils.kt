package com.liquid.poros.utils

import android.animation.Animator
import android.view.View
import com.airbnb.lottie.LottieAnimationView

object UIUtils {
    @JvmStatic
    fun startLottieAnimation(targetView:View,lottieView:LottieAnimationView,path:String,animListener:AnimListener?) {
        try {//trycatch资源不存在时的错误
            lottieView.visibility = View.VISIBLE
            targetView.visibility = View.INVISIBLE
            lottieView.setAnimation("$path/data.json")
            lottieView.imageAssetsFolder = "$path/images"
            lottieView.addAnimatorListener(object : Animator.AnimatorListener {
                override fun onAnimationStart(animation: Animator) {}
                override fun onAnimationEnd(animation: Animator) {
                    try {
                        lottieView.removeAnimatorListener(this)
                        lottieView.visibility = View.INVISIBLE
                        targetView.visibility = View.VISIBLE
                        animListener?.onAnimEnd()
                    }catch (e:Exception){
                    }
                }

                override fun onAnimationCancel(animation: Animator) {
                    try {
                        lottieView.removeAnimatorListener(this)
                        lottieView.visibility = View.INVISIBLE
                        targetView.visibility = View.VISIBLE
                    }catch (e:Exception){
                    }

                }

                override fun onAnimationRepeat(animation: Animator) {}
            })
            lottieView.playAnimation()
        }catch (e:Exception){
            lottieView.visibility = View.INVISIBLE
            targetView.visibility = View.VISIBLE
            e.printStackTrace()
        }

    }

    @JvmStatic
    fun startLottieAnimation(lottieView:LottieAnimationView,path:String,animListener:AnimListener?) {
        try {//
            lottieView.visibility = View.VISIBLE
            lottieView.setAnimation("$path/data.json")
            lottieView.imageAssetsFolder = "$path/images"
            lottieView.addAnimatorListener(object : Animator.AnimatorListener {
                override fun onAnimationStart(animation: Animator) {}
                override fun onAnimationEnd(animation: Animator) {
                    try {
                        lottieView.removeAnimatorListener(this)
                        lottieView.visibility = View.INVISIBLE
                        animListener?.onAnimEnd()
                    }catch (e:Exception){
                    }
                }

                override fun onAnimationCancel(animation: Animator) {
                    try {
                        lottieView.removeAnimatorListener(this)
                        lottieView.visibility = View.INVISIBLE
                    }catch (e:Exception){
                    }
                }

                override fun onAnimationRepeat(animation: Animator) {}
            })
            lottieView.playAnimation()
        }catch (e:Exception){
            lottieView.visibility = View.INVISIBLE
            e.printStackTrace()
        }
    }
}


interface AnimListener{
    fun onAnimEnd()
}

