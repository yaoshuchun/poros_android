package com.liquid.poros.utils.push

import android.app.*
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.core.app.NotificationCompat
import com.liquid.base.utils.ContextUtils
import com.liquid.poros.R
import com.liquid.poros.analytics.utils.MultiTracker
import com.liquid.poros.constant.Constants
import com.liquid.poros.constant.TrackConstants
import com.liquid.poros.entity.PlayloadInfo
import com.liquid.poros.ui.activity.user.SessionActivity
import com.liquid.poros.utils.AccountUtil
import com.liquid.poros.utils.StringUtil
import java.util.*
import java.util.concurrent.atomic.AtomicInteger

/**
 * 通知栏的本地消息推送
 * Created by yejiurui on 2021/1/5.
 * Mail:yejiurui@liquidnetwork.com
 */
class PushNotifyUtils private constructor() {

    private val mNum = AtomicInteger(1)
    private var pendingActivity: Class<*>? = null
    fun sentGeTuiNotify(info: PlayloadInfo?) {
        if (info == null) {
            return
        }
        val notifyTitle = info.msg_notify_title
        val msgTitle = info.msg_title
        val title = if (StringUtil.isNotEmpty(notifyTitle)) notifyTitle else msgTitle
        val content = info.msg_content
        val groupId = "Default"
        val groupName = "Default"
        val channelId = "Default"
        val channelName = "Default"
        val bundle = Bundle()
        bundle.putString(Constants.SESSION_ID, info.account)
        bundle.putBoolean(Constants.SHOW_RECHARGE, info.show_recharge)
        bundle.putBoolean(Constants.SHOW_BUY_DIALOG, info.show_buy_dialog)
        bundle.putString(Constants.FROM, Constants.OUTER_NOTICE)
        if (info.show_buy_dialog) {
            bundle.putString(Constants.BUY_POSITION, Constants.buy_card_outline_notice)
        }
        when (info.type) {
            PlayloadInfo.P2P -> pendingActivity = SessionActivity::class.java
        }

        // android O 兼容
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val manager = ContextUtils.getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            val group = NotificationChannelGroup(groupId, groupName)
            manager.createNotificationChannelGroup(group)
            val channel = NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_HIGH)
            channel.group = groupId
            manager.createNotificationChannel(channel)
        }
        if (info.show_recharge || info.show_buy_dialog) {
            val params = HashMap<String?, String?>()
            params[Constants.POSITION] = TrackConstants.POSI_NOTICE
            if (info.show_recharge) {
                MultiTracker.onEvent(TrackConstants.B_REMIND_TEAM_JOIN_CARD_TIME_3MIN_EXPOSURE, params)
            } else {
                MultiTracker.onEvent(TrackConstants.B_REMIND_TEAM_JOIN_CARD_TIME_END_EXPOSURE, params)
            }
        }
        val builder = NotificationCompat.Builder(ContextUtils.getApplicationContext(), channelId)
        builder.setSmallIcon(R.mipmap.ic_jinquan)
        builder.setContentTitle(title)
        builder.setContentText(content)
        builder.setTicker("进圈$title")
        builder.setWhen(System.currentTimeMillis())
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            builder.priority = NotificationManager.IMPORTANCE_DEFAULT
        }
        builder.setDefaults(Notification.DEFAULT_ALL)

        builder.setAutoCancel(true)
        val intent = Intent(ContextUtils.getApplicationContext(), pendingActivity)
        intent.action = "com.liquid.poros.pushs"
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.putExtras(bundle)
        val pi = PendingIntent.getActivity(ContextUtils.getApplicationContext(), mNum.get(), intent, 0)
        builder.setContentIntent(pi)
        val notification = builder.build()
        val notificationManager = ContextUtils.getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.notify(mNum.getAndAdd(1), notification)
    }

    companion object {
        private var mInstance: PushNotifyUtils? = null

        @JvmStatic
        val instance: PushNotifyUtils
            get() = if (mInstance == null) PushNotifyUtils().also { mInstance = it } else mInstance!!
    }
}