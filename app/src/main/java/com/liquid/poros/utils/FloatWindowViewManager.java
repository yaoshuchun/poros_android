package com.liquid.poros.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.PixelFormat;
import android.os.Build;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import com.liquid.base.utils.ContextUtils;
import com.liquid.poros.R;
import com.liquid.poros.widgets.BaseWindowView;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class FloatWindowViewManager {
    private static volatile FloatWindowViewManager instance;
    private WindowManager windowManager = null;
    private View mToAddToWindowView;
    private OnFloatWindowViewStateListener onFloatWindowViewStateListener;
    private WindowManager.LayoutParams layoutParams;
    private int hashCode = -1;
    private List<View> windowViews = new ArrayList<>();

    private FloatWindowViewManager() {

    }

    public static FloatWindowViewManager getInstance() {
        if (instance == null) {
            synchronized (FloatWindowViewManager.class) {
                if (instance == null) {
                    instance = new FloatWindowViewManager();
                }
            }
        }
        return instance;
    }

    public WindowManager.LayoutParams getLayoutParams() {
        return layoutParams;
    }

    public void setOnFloatWindowViewStateListener(OnFloatWindowViewStateListener onFloatWindowViewStateListener) {
        this.onFloatWindowViewStateListener = onFloatWindowViewStateListener;
    }

    public synchronized boolean showView(Activity activity, View toAddToWindowView, int positionX, int positionY,boolean global) {
        if (ViewUtils.isActityDestroyed(activity))
            return false;
        if (activity == null || toAddToWindowView == null){
            return false;
        }
        if (null == activity.getWindow().getDecorView().getWindowToken()){
            return false;
        }
        if (!canShow(toAddToWindowView)){
            return false;
        }

        try {
            if (windowManager == null) {
                windowManager = (WindowManager) activity.getSystemService(Context.WINDOW_SERVICE);
            }

            if (windowViews.contains(toAddToWindowView)){
                removeView(toAddToWindowView);
            }
            windowViews.add(toAddToWindowView);

            layoutParams = createParams(activity, positionX, positionY,global);
            return addView(toAddToWindowView,layoutParams);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
        }
    }
    public synchronized <T> boolean showSingleView(Activity activity, Class<T> tClass, int positionX, int positionY,boolean global) {
        if (null == tClass)
            return false;
        View toAddToWindowView = null;
        try {
            Iterator<View> iterator = windowViews.iterator();
            while(iterator.hasNext()){
                View  windowView = iterator.next();
                if(tClass.getSimpleName().equals(windowView.getClass().getSimpleName())){
                    iterator.remove();
                    removeView(windowView);
                    toAddToWindowView = windowView;
                }
            }

            if (null != toAddToWindowView){
                return showSingleView(activity,toAddToWindowView,positionX,positionY,global);
            }else {
                return false;
            }

        }catch (Exception e){
            return false;
        }
    }
    public synchronized boolean showSingleView(Activity activity, View toAddToWindowView, int positionX, int positionY,boolean global) {
        if (null == toAddToWindowView){
            return false;
        }
        if (ViewUtils.isActityDestroyed(activity))
            return false;
        if (null == activity.getWindow().getDecorView().getWindowToken()){
            return false;
        }

        Iterator<View> iterator = windowViews.iterator();
        while(iterator.hasNext()){
            View  windowView = iterator.next();
            if(toAddToWindowView.getClass().getSimpleName().equals(windowView.getClass().getSimpleName())){
                iterator.remove();
                removeView(windowView);
            }
        }

        windowViews.add(toAddToWindowView);

        try {
            if (windowManager == null) {
                windowManager = (WindowManager) activity.getSystemService(Context.WINDOW_SERVICE);
            }

            if (toAddToWindowView instanceof BaseWindowView){
                BaseWindowView baseWindowView = (BaseWindowView) toAddToWindowView;
                positionX = baseWindowView.getPositionX();
                positionY = baseWindowView.getPositionY();
            }
            layoutParams = createParams(activity, positionX, positionY,global);
            return addView(toAddToWindowView,layoutParams);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
        }
    }

    public View getReadTimeView() {
        if (null != mToAddToWindowView){
            return mToAddToWindowView.findViewById(R.id.content);
        }
        return null;
    }

    public View getFloatView(){
        return mToAddToWindowView;
    }

    private boolean canShow(View toAddToWindowView){
        if (toAddToWindowView == null){
            return false;
        }
        return true;
    }



    public void dismissView(View view) {
        removeView(view);
    }


    public WindowManager.LayoutParams createParams(Activity activity,int positionX,int positionY,boolean onGlobal) {
        if (ViewUtils.isActityDestroyed(activity))
            return null;
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.packageName = activity.getPackageName();
        layoutParams.width = WindowManager.LayoutParams.WRAP_CONTENT;
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
        layoutParams.flags = WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_LAYOUT_INSET_DECOR
                | WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM;
        if (onGlobal){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                layoutParams.type = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
            } else {
                layoutParams.type = WindowManager.LayoutParams.TYPE_PHONE;
            }
        }else {
            layoutParams.type = WindowManager.LayoutParams.TYPE_APPLICATION_SUB_PANEL;
        }
        layoutParams.format = PixelFormat.RGBA_8888;
        layoutParams.gravity = Gravity.LEFT | Gravity.TOP;
        layoutParams.x = positionX;
        layoutParams.y = positionY;
        if (!onGlobal){
            layoutParams.token = activity.getWindow().getDecorView().getWindowToken();
        }
        return layoutParams;
    }

    public WindowManager.LayoutParams createParams(Context context,int positionX,int positionY,boolean onGlobal) {
        if (null == context)
            return null;
        if (!onGlobal && !(context instanceof Activity)){
            return null;
        }
        if (context instanceof Activity){
            if (ViewUtils.isActityDestroyed((Activity)context))
                return null;
        }
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.packageName = context.getPackageName();
        layoutParams.width = WindowManager.LayoutParams.WRAP_CONTENT;
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
        layoutParams.flags = WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_LAYOUT_INSET_DECOR
                | WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM;
        if (onGlobal){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                layoutParams.type = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
            } else {
                layoutParams.type = WindowManager.LayoutParams.TYPE_PHONE;
            }
        }else {
            layoutParams.type = WindowManager.LayoutParams.TYPE_APPLICATION_SUB_PANEL;
        }
        layoutParams.format = PixelFormat.RGBA_8888;
        layoutParams.gravity = Gravity.LEFT | Gravity.TOP;
        layoutParams.x = positionX;
        layoutParams.y = positionY;
        if (!onGlobal){
            layoutParams.token = ((Activity)context).getWindow().getDecorView().getWindowToken();
        }
        return layoutParams;
    }

    public synchronized void removeView(View view) {
        try {
            if (windowManager != null && view != null) {
                windowManager.removeViewImmediate(view);
                if (-1 != windowViews.indexOf(view)){
                    windowViews.remove(view);
                }
                if (null != onFloatWindowViewStateListener){
                    onFloatWindowViewStateListener.onremove(mToAddToWindowView);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean addView(View view,WindowManager.LayoutParams params) {
        try {
            if (windowManager != null && view != null && null != params) {
                windowManager.addView(view,params);
                if (null != onFloatWindowViewStateListener){
                    onFloatWindowViewStateListener.onAdd(mToAddToWindowView);
                }

                if (view instanceof BaseWindowView){
                    BaseWindowView baseWindowView = (BaseWindowView) view;
                    baseWindowView.setParams(layoutParams);
                }
                return true;
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public interface OnFloatWindowViewStateListener{
        void onAdd(View floatWindowView);
        void onremove(View floatWindowView);
    }


    public synchronized boolean showViewWithPermi(View toAddToWindowView, int positionX, int positionY) {
        if (toAddToWindowView == null){
            return false;
        }
        if (!canShow(toAddToWindowView)){
            return false;
        }

        try {
            if (windowManager == null) {
                windowManager = (WindowManager) ContextUtils.getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
            }

            boolean addToList = true;
            Iterator<View> iterator = windowViews.iterator();
            while(iterator.hasNext()){
                View  windowView = iterator.next();

                if(toAddToWindowView.getClass().getSimpleName().equals(windowView.getClass().getSimpleName())){
                    if (windowView != toAddToWindowView){
                        iterator.remove();
                        removeView(windowView);
                    }else {
                        addToList = false;
                    }
                }
            }
            if (addToList){
                windowViews.add(toAddToWindowView);
            }
            layoutParams = createParams(ContextUtils.getApplicationContext(), positionX, positionY,true);
            return addView(toAddToWindowView,layoutParams);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
        }
    }

    public synchronized <T> void removePermiView(Class<T> t) {
        try {
            if (null != windowManager && null != t) {
                Iterator<View> iterator = windowViews.iterator();
                while(iterator.hasNext()){
                    View windowView = iterator.next();
                    if(null != windowView && t.getSimpleName().equals(windowView.getClass().getSimpleName())){
                        iterator.remove();
                        windowManager.removeViewImmediate(windowView);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized <T> void dismissSingleView(Class<T> t) {
        try {
            if (null != windowManager  && null != t) {
                Iterator<View> iterator = windowViews.iterator();
                while(iterator.hasNext()){
                    View windowView = iterator.next();
                    if(null != windowView && t.getSimpleName().equals(windowView.getClass().getSimpleName())){
                        windowManager.removeViewImmediate(windowView);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized void saveSingleView(View view) {
        try {
            if (null == view)
                return;
            Iterator<View> iterator = windowViews.iterator();
            while(iterator.hasNext()){
                View windowView = iterator.next();
                if(null != windowView && view.getClass().getSimpleName().equals(windowView.getClass().getSimpleName())){
                    iterator.remove();
                    removeView(windowView);
                }
            }
            windowViews.add(view);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized <T> T getSingleView(Class<T> t) {
        try {
            if (null == t){
                return null;
            }
            Iterator<View> iterator = windowViews.iterator();
            while(iterator.hasNext()){
                View windowView = iterator.next();
                if(t.getSimpleName().equals(windowView.getClass().getSimpleName())){
                    return (T) windowView;
                }
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public synchronized <T> void removeSingleView(Class<T> t) {
        removePermiView(t);
    }
}
