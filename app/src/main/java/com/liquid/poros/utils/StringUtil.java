package com.liquid.poros.utils;

import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;

import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 内部字符串比较统一使用此工具类。
 * @author 滕达
 */
public class StringUtil {
    public static boolean isNotEmpty(String string) {
        return !isEmpty(string);
    }

    public static int length(String string){
        if (null == string || 0 == string.trim().length())
            return 0;
        else
            return string.trim().length();
    }
    public static boolean isEmpty(String string) {
        if (null == string || 0 == string.trim().length())
            return true;
        else
            return false;
    }

    public static String ensureNoNull(String string) {
        return null == string ? "" : string;
    }

    public static boolean notEquals(String string1, String string2) {
        if (null == string1) {
            return string2 != null;
        } else {
            return !string1.equals(string2);
        }
    }
    public static boolean isEquals(String string1, String string2) {
        return !notEquals(string1,string2);
    }
    public static double isSimilar(String strA, String strB) {
        String newStrA = removeSign(strA);
        String newStrB = removeSign(strB);
        if(newStrA.length()<newStrB.length()){
            String temp = newStrA;
            newStrA = newStrB;
            newStrB = temp;
        }
        return getLCSlen(newStrA, newStrB) * 1.0 / newStrA.length();
    }
    private static String removeSign(String str) {
        StringBuffer sb = new StringBuffer();
        for (char item : str.toCharArray()) {
            if (charReg(item)) {
                sb.append(item);
            }
        }
        return sb.toString();
    }
    private static boolean charReg(char charValue) {
        return (charValue >= 0x4E00 && charValue <= 0x9FA5) || (charValue >= 'a' && charValue <= 'z') || (charValue >= 'A' && charValue <= 'Z');
    }
    private static int getLCSlen(String strA, String strB){
        String[] strAElement = new String[strA.length()];
        for (int i = 0; i < strA.length(); i++) {
            strAElement[i] = strA.substring(i, i + 1);
        }
        String[] strBElement = new String[strB.length()];
        for (int i = 0; i < strB.length(); i++) {
            strBElement[i] = strB.substring(i, i + 1);
        }
        int m = strAElement.length;
        int n = strBElement.length;
        int[][] matrix = new int[m + 1][n + 1];
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                if (strAElement[i - 1].equals(strBElement[j - 1])) {
                    matrix[i][j] = matrix[i - 1][j - 1] + 1;
                } else {
                    matrix[i][j] = Math.max(Math.max(matrix[i][j - 1], matrix[i - 1][j]), matrix[i - 1][j - 1]);
                }
            }
        }
        return matrix[m][n];
    }

    /**
     * 过滤空格 回车 换行符
     *
     * @param str
     * @return
     */
    public static String replaceBlank(String str) {
        String dest = "";
        if (str != null) {
            Pattern p = Pattern.compile("\\s*|\t|\r|\n");
            Matcher m = p.matcher(str);
            dest = m.replaceAll("");
        }
        return dest;
    }
    /**
     * 拼配关键字变色
     *
     * @param isSizeSpan 字体大小
     * @param color      色值
     * @param text       文字
     * @param keyword    匹配关键字
     * @return
     */
    public static CharSequence matcherSearchText(boolean isSizeSpan, int color, String text, String keyword) {
        if (!TextUtils.isEmpty(text) && !TextUtils.isEmpty(keyword)) {
            SpannableString ss = new SpannableString(text);
            String temp = Pattern.quote(keyword);
            Pattern pattern = Pattern.compile(temp);
            Matcher matcher = pattern.matcher(ss);
            while (matcher.find()) {
                int start = matcher.start();
                int end = matcher.end();
                ss.setSpan(new ForegroundColorSpan(color), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                if (isSizeSpan) {
                    ss.setSpan(new RelativeSizeSpan(0.6f), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                }
            }
            return ss;
        }
        return "";
    }
    /**
     * 拼配关键字变色
     *
     * @param isSizeSpan 字体大小
     * @param text       文字
     * @param keyword    匹配关键字
     * @return
     */
    public static CharSequence matcherSearchText(boolean isSizeSpan, String text, String keyword) {
        if (!TextUtils.isEmpty(text) && !TextUtils.isEmpty(keyword)) {
            SpannableString ss = new SpannableString(text);
            String temp = Pattern.quote(keyword);
            Pattern pattern = Pattern.compile(temp);
            Matcher matcher = pattern.matcher(ss);
            while (matcher.find()) {
                int start = matcher.start();
                int end = matcher.end();
                if (isSizeSpan) {
                    ss.setSpan(new RelativeSizeSpan(0.6f), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                }
            }
            return ss;
        }
        return "";
    }

}