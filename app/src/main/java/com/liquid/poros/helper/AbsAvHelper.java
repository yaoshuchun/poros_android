package com.liquid.poros.helper;

import android.content.Context;

import com.netease.lava.nertc.sdk.video.NERtcVideoView;

public abstract class AbsAvHelper {
    /**
     * 注册云信观察者
     *
     * @param isRegister true 注册   false 反注册
     */
    abstract void registerObservers(boolean isRegister);

    /**
     * 加入房间
     */
     abstract void joinRoom();

    /**
     * 创建房间
     */
    abstract void createRoom();


    /**
     * 开启或者关闭麦克风
     */
    public abstract void setMicrophoneMute(boolean mute);


    /**
     * 设置语音房间参数
     */
    abstract void setAudioParameters();

    /**
     * 释放连麦资源并离开房间
     */
    public abstract void releaseAvChat();

    /**
     * 设置视频房间参数
     */
    public abstract void setAvParameters();

    /**
     * 打开摄像头采集视频
     * @param render
     */
    public abstract void openCamera(NERtcVideoView render);

    /**
     * 关闭摄像头
     */
    public abstract void closeCamera();

    /**
     * 初始化美颜
     * @param context
     */
    abstract void initRender(Context context);

    /**
     * 释放美颜资源
     */
    abstract void releaseVideoEffect();

    /**
     * 执行视频连麦
     */
    public abstract void joinAvRoom(Context context);

    public abstract void joinAudioRoom();

}
