package com.liquid.poros.helper

import android.util.Log
import com.liquid.poros.analytics.utils.MultiTracker
import com.liquid.poros.constant.Constants
import com.netease.lava.nertc.sdk.NERtcConstants
import java.util.*

object AvExceptionUploadHelper {

    fun onError(code :Int,roomId:String){
        val params = HashMap<String?, String?>()
        params[Constants.ROOM_ID] = roomId
        params["error_info"] = when (code) {
            NERtcConstants.RuntimeError.ADM_NO_AUTHORIZE -> "没有音频权限"
            NERtcConstants.RuntimeError.ADM_PLAYOUT_INIT_ERROR ->  "音频播放设备初始化失败"
            NERtcConstants.RuntimeError.ADM_PLAYOUT_START_ERROR -> "音频播放设备打开失败"
            NERtcConstants.RuntimeError.ADM_PLAYOUT_UNKNOWN_ERROR -> "音频播放设备运行错误"
            NERtcConstants.RuntimeError.ADM_RECORD_INIT_ERROR -> "麦克风初始化失败"
            NERtcConstants.RuntimeError.ADM_RECORD_START_ERROR -> "麦克风打开失败"
            NERtcConstants.RuntimeError.ADM_RECORD_UNKNOWN_ERROR -> "麦克风运行错误"
            NERtcConstants.RuntimeError.VDM_CAMERA_DISCONNECT_ERROR -> "相机被其他应用抢占"
            NERtcConstants.RuntimeError.VDM_CAMERA_FREEZED_ERROR -> "相机已冻结"
            NERtcConstants.RuntimeError.VDM_CAMERA_UNKNOWN_ERROR -> "相机运行错误"
            NERtcConstants.RuntimeError.VDM_NO_AUTHORIZE -> "没有视频权限"
            else -> ""
        }
        MultiTracker.onEvent("open_camera_error", params)

    }

    fun onWarning(code :Int,roomId:String){
        val params = HashMap<String?, String?>()
        params[Constants.ROOM_ID] = roomId
        var warningInfo = when(code){

            else -> ""
        }
        params["warning_info"] = warningInfo
        MultiTracker.onEvent("open_camera_warning", params)
    }

    fun initException(msg :String,roomId:String){
        val params = HashMap<String?, String?>()
        params[Constants.ROOM_ID] = roomId
        params["msg"] = msg
        MultiTracker.onEvent("sdk_init_exception", params)
    }

    fun onReJoinChannel(code:Int,channelId :Long ,roomId:String){
        val params = HashMap<String?, String?>()
        params[Constants.ROOM_ID] = roomId
        params["error_info"] = when (code) {
            NERtcConstants.ErrorCode.ENGINE_ERROR_ADD_TRACK_FAIL -> "添加Track失败"
            NERtcConstants.ErrorCode.ENGINE_ERROR_CONNECT_FAIL -> "连接失败"
            NERtcConstants.ErrorCode.ENGINE_ERROR_CONNECTION_NOT_FOUND -> "连接未找到"
            NERtcConstants.ErrorCode.ENGINE_ERROR_CREATE_DEVICE_SOURCE_FAIL -> "创建设备失败"
            NERtcConstants.ErrorCode.ENGINE_ERROR_CREATE_DUMP_FILE_FAIL -> "创建dump 失败"
            NERtcConstants.ErrorCode.ENGINE_ERROR_DEVICE_NOT_FOUND -> "设备未找到"
            NERtcConstants.ErrorCode.ENGINE_ERROR_DEVICE_PREVIEW_ALREADY_STARTED -> "预览已打开"
            NERtcConstants.ErrorCode.ENGINE_ERROR_FATAL -> "内部错误"
            NERtcConstants.ErrorCode.ENGINE_ERROR_INVALID_DEVICE_SOURCEID -> "设备ID非法"
            NERtcConstants.ErrorCode.ENGINE_ERROR_INVALID_INDEX -> "序号非法"
            NERtcConstants.ErrorCode.ENGINE_ERROR_INVALID_PARAM -> "参数错误"
            NERtcConstants.ErrorCode.ENGINE_ERROR_INVALID_RENDER -> "画布非法"
            NERtcConstants.ErrorCode.ENGINE_ERROR_INVALID_STATE -> "状态错误"
            NERtcConstants.ErrorCode.ENGINE_ERROR_INVALID_USERID -> "非法用户"
            NERtcConstants.ErrorCode.ENGINE_ERROR_INVALID_VIDEO_PROFILE -> "视频能力非法"
            NERtcConstants.ErrorCode.ENGINE_ERROR_LACK_OF_RESOURCE -> "缺乏资源"
            NERtcConstants.ErrorCode.ENGINE_ERROR_MEDIA_CONNECTION_DISCONNECTED -> "媒体连接已断开"
            NERtcConstants.ErrorCode.ENGINE_ERROR_MEDIA_NOT_STARTED -> "媒体会话未建立"
            NERtcConstants.ErrorCode.ENGINE_ERROR_NOT_SUPPORTED -> "不支持"
            NERtcConstants.ErrorCode.ENGINE_ERROR_OUT_OF_MEMORY -> "内存溢出"
            NERtcConstants.ErrorCode.ENGINE_ERROR_REQUEST_JOIN_ROOM_FAIL -> "请求加入房间失败"
            NERtcConstants.ErrorCode.ENGINE_ERROR_ROOM_ALREADY_JOINED -> "房间已加入"
            NERtcConstants.ErrorCode.ENGINE_ERROR_ROOM_CLOSED -> "房间被关闭"
            NERtcConstants.ErrorCode.ENGINE_ERROR_ROOM_NOT_JOINED -> "房间未加入"
            NERtcConstants.ErrorCode.ENGINE_ERROR_ROOM_REPLEATEDLY_LEAVE -> "重复离开房间"
            NERtcConstants.ErrorCode.ENGINE_ERROR_SERVER_KICKED -> "被管理员踢出房间"
            NERtcConstants.ErrorCode.ENGINE_ERROR_SESSION_NOT_FOUND -> "会话未找到"
            NERtcConstants.ErrorCode.ENGINE_ERROR_SIGNAL_DISCONNECTED -> "信令断开"
            NERtcConstants.ErrorCode.ENGINE_ERROR_SOURCE_NOT_FOUND -> "媒体源未找到"
            NERtcConstants.ErrorCode.ENGINE_ERROR_START_DUMP_FAIL -> "开始dump失败"
            NERtcConstants.ErrorCode.ENGINE_ERROR_STREAM_NOT_FOUND -> "媒体流未找到"
            NERtcConstants.ErrorCode.ENGINE_ERROR_TRACK_NOT_FOUND -> "Track未找到"
            NERtcConstants.ErrorCode.ENGINE_ERROR_TRANSMIT_PENDDING -> "挂起"
            NERtcConstants.ErrorCode.ENGINE_ERROR_USER_NOT_FOUND -> "用户未找到"
            NERtcConstants.ErrorCode.ERR_INVALID_OPERATION -> "操作不支持"
            NERtcConstants.ErrorCode.ERR_UNINITIALIZED -> "未初始化"
            NERtcConstants.ErrorCode.ILLEGAL_ARGUMENT -> "参数错误 ILLEGAL_ARGUMENT"
            NERtcConstants.ErrorCode.ILLEGAL_STATUS -> "状态错误"
            NERtcConstants.ErrorCode.OK -> "成功"
            NERtcConstants.ErrorCode.RESERVE_ERROR_INVALID_PARAMETER -> "参数错误 INVALID_PARAMETER"
            NERtcConstants.ErrorCode.RESERVE_ERROR_MORE_THAN_TWO_USER -> "只支持两个用户, 有第三个人试图使用相同的频道名分配频道"
            NERtcConstants.ErrorCode.RESERVE_ERROR_NO_PERMISSION -> "没有权限，包括没有开通音视频功能、没有开通非安全但是请求了非安全等"
            NERtcConstants.ErrorCode.RESERVE_ERROR_RESERVE_SERVER_FAIL -> "分配频道服务器出错"
            NERtcConstants.ErrorCode.RESERVE_ERROR_TIME_OUT -> "请求超时"
            else -> ""
        }
        MultiTracker.onEvent("rejoin_channel", params)
    }

}