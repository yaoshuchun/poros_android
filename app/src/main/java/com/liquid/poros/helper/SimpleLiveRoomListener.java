package com.liquid.poros.helper;

public class SimpleLiveRoomListener implements LiveRoomListener{
    @Override
    public void onUserLeave(String account) {

    }

    @Override
    public void onUserJoined(String account) {

    }

    @Override
    public void onJoinRoomSuccess(long channelId) {

    }

    @Override
    public void onJoinRoomFailed(int resultCode) {

    }

    @Override
    public void onVideoLeave() {

    }

    @Override
    public void onMatchLeave() {

    }

    @Override
    public void onUserVideoStart(long l, int i) {

    }

    @Override
    public void onDisconnect() {

    }
}
