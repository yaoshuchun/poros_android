package com.liquid.poros.helper.provider;

import com.liquid.poros.helper.SimpleCallback;
import com.liquid.poros.helper.cache.NimUserInfoCache;
import com.liquid.poros.helper.user.IUserInfoProvider;
import com.netease.nimlib.sdk.uinfo.model.NimUserInfo;

import java.util.List;

public class DefaultUserInfoProvider implements IUserInfoProvider<NimUserInfo> {
    @Override
    public NimUserInfo getUserInfo(String account) {
        NimUserInfo user = NimUserInfoCache.getInstance().getUserInfo(account);
        if (user == null) {
            NimUserInfoCache.getInstance().getUserInfoFromRemote(account, null);
        }
        return user;
    }

    @Override
    public List<NimUserInfo> getUserInfo(List<String> accounts) {
        return null;
    }

    @Override
    public void getUserInfoAsync(String account, SimpleCallback<NimUserInfo> callback) {

    }

    @Override
    public void getUserInfoAsync(List<String> accounts, SimpleCallback<List<NimUserInfo>> callback) {

    }
}
