package com.liquid.poros.helper

import android.content.Context
import android.graphics.Rect
import android.hardware.Camera
import android.media.AudioManager
import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.core.content.ContextCompat.getSystemService
import com.faceunity.nama.NamaRenderer
import com.faceunity.nama.fromapp.entity.BeautyParameterModel
import com.faceunity.nama.fromapp.utils.CameraUtils
import com.liquid.base.event.EventCenter
import com.liquid.base.utils.ContextUtils
import com.liquid.library.retrofitx.utils.LogUtils
import com.liquid.poros.BuildConfig
import com.liquid.poros.PorosApplication
import com.liquid.poros.analytics.utils.MultiTracker
import com.liquid.poros.constant.Constants
import com.liquid.poros.entity.FaceInfo
import com.liquid.poros.helper.AvExceptionUploadHelper.initException
import com.liquid.poros.helper.AvExceptionUploadHelper.onError
import com.liquid.poros.helper.AvExceptionUploadHelper.onReJoinChannel
import com.liquid.poros.helper.AvExceptionUploadHelper.onWarning
import com.liquid.poros.ui.activity.live.RoomLiveActivityV2
import com.liquid.poros.ui.activity.live.VideoMatchChattingActivity
import com.liquid.poros.ui.floatball.FloatManager
import com.liquid.poros.utils.AccountUtil
import com.liquid.poros.utils.RoomLiveMonitorHelper
import com.netease.lava.api.model.RTCVideoCropMode
import com.netease.lava.nertc.sdk.NERtcCallbackEx
import com.netease.lava.nertc.sdk.NERtcConstants
import com.netease.lava.nertc.sdk.NERtcEx
import com.netease.lava.nertc.sdk.NERtcParameters
import com.netease.lava.nertc.sdk.stats.NERtcAudioVolumeInfo
import com.netease.lava.nertc.sdk.video.NERtcVideoCallback
import com.netease.lava.nertc.sdk.video.NERtcVideoConfig
import com.netease.lava.nertc.sdk.video.NERtcVideoFrame
import com.netease.lava.nertc.sdk.video.NERtcVideoView
import de.greenrobot.event.EventBus
import java.util.*


class AvHelper {
    private var mRoomId: String = ""
    private var imVideoToken: String

    //是否是音频，true 是音频 false 视频
    private var isAudio: Boolean

    //美颜相关
    var fURenderer: NamaRenderer? = null
    private var mVideoEffectHandler: Handler? = null
    private var mIsFirstFrame = true
    private var mRoomListener: LiveRoomListener? = null
    private var currentAudioMode: Int = 0

    private var context:Context? = null

    /**
     * 语音房间
     *
     * @param roomId
     * @param type
     */
    constructor(roomId: String, imVideoToken: String, type: Boolean) {
        mRoomId = roomId
        this.imVideoToken = imVideoToken
        isAudio = type
    }

    /**
     * 视频房间
     *
     * @param mRoomId
     * @param type
     */
    constructor(mRoomId: String, imVideoToken: String, type: Boolean, liveRoomListener: LiveRoomListener?) {
        this.mRoomId = mRoomId
        mRoomListener = liveRoomListener
        this.imVideoToken = imVideoToken
        isAudio = type
    }

    init {
        val am = getSystemService(PorosApplication.context!!, AudioManager::class.java)
        currentAudioMode = am?.mode ?: 0
    }

    /**
     * 设置麦克风是否静音
     *
     * @param mute
     */
    fun setMicrophoneMute(mute: Boolean) {
        NERtcEx.getInstance().enableLocalAudio(!mute)
    }

    /**
     * 视频房开黑
     *
     * @param context
     */
    @JvmOverloads
    fun joinAvRoom(context: Context, videoView: NERtcVideoView? = null,enableVido: Boolean = true) {
        this.context = context
        initFaceUnityRender(context)
        initNERtcEngine(videoView,enableVido)
        NERtcEx.getInstance().joinChannel(imVideoToken, mRoomId, AccountUtil.getInstance().userId.toLong())
        NERtcEx.getInstance().setVideoCallback(videoCallbackToFaceUnity, true)
    }

    /**
     * 下麦释放音视频资源
     */
    fun releaseAvRoom() {
        val params = HashMap<String?, String?>()
        params[Constants.MALE_USER_ID] = AccountUtil.getInstance().userId
        params[Constants.ROOM_ID] = mRoomId
        params[Constants.TAG] = "connect_mic_ques"
        MultiTracker.onEvent("release_av_chat", params)
        //音视频需要释放美颜等资源
        if (!isAudio) {
            releaseFaceUnityRender()
            NERtcEx.getInstance().enableLocalAudio(false)
            NERtcEx.getInstance().enableLocalVideo(false)
        }
        NERtcEx.getInstance().leaveChannel()
        NERtcEx.getInstance().release()
        val am = getSystemService(PorosApplication.context!!, AudioManager::class.java)
        am?.mode = currentAudioMode
        RoomLiveMonitorHelper.instance().destroy()
    }

    /**
     * 私聊、速配开黑
     */
    fun joinAudioRoom() {
        setAudioParameters()
        NERtcEx.getInstance().joinChannel(imVideoToken, mRoomId, AccountUtil.getInstance().userId.toLong())
    }

    /**
     * 初始化FaceUnity美颜
     *
     * @param context
     */
    private fun initFaceUnityRender(context: Context) {
        NamaRenderer.initFURenderer(context)
        if (fURenderer == null) {
            fURenderer = NamaRenderer.Builder(context)
                    .maxFaces(4)
                    .inputTextureType(NamaRenderer.FU_ADM_FLAG_EXTERNAL_OES_TEXTURE)
                    .inputImageOrientation(CameraUtils.getCameraOrientation(Camera.CameraInfo.CAMERA_FACING_FRONT))
                    .setLoadAiHumanProcessor(false)
                    .maxHumans(1)
                    .setNeedFaceBeauty(true)
                    .build()
            BeautyParameterModel.useSavedParams(fURenderer)
        }
    }

    /**
     * 释放FaceUnity美颜相关资源
     */
    private fun releaseFaceUnityRender() {
        if (mVideoEffectHandler != null) {
            mVideoEffectHandler!!.post {
                if (fURenderer != null) {
                    fURenderer!!.onSurfaceDestroyed()
                    fURenderer = null
                    mIsFirstFrame = true
                    mVideoEffectHandler = null
                }
            }
        }
    }

    /**
     * 初始化音视频通话
     */
    private fun initNERtcEngine(videoView: NERtcVideoView?,enableVido: Boolean = true) {
        val config = NERtcVideoConfig()
        config.videoProfile = NERtcConstants.VideoProfile.STANDARD
        config.videoCropMode = RTCVideoCropMode.kRTCVideoCropModeDefault
        config.frameRate = NERtcVideoConfig.NERtcVideoFrameRate.FRAME_RATE_FPS_15
        NERtcEx.getInstance().setLocalVideoConfig(config)
        NERtcEx.getInstance().setChannelProfile(NERtcConstants.RTCChannelProfile.LIVE_BROADCASTING)
        val parameters = NERtcParameters()
        parameters.set(NERtcParameters.KEY_PUBLISH_SELF_STREAM, true)
        parameters.set(NERtcParameters.KEY_VIDEO_ENCODE_MODE, NERtcConstants.MediaCodecMode.MEDIA_CODEC_SOFTWARE) //视频编码模式，优先使用软件编解码器
        parameters.set(NERtcParameters.KEY_VIDEO_DECODE_MODE, NERtcConstants.MediaCodecMode.MEDIA_CODEC_SOFTWARE) //视频解码模式，优先使用软件编解码器
        NERtcEx.getInstance().setParameters(parameters) //先设置参数，后初始化
        NERtcEx.getInstance().enableLocalVideo(enableVido)
        NERtcEx.getInstance().setAudioProfile(NERtcConstants.AudioProfile.DEFAULT, NERtcConstants.AudioScenario.SPEECH) //初始化前设置音频质量与场景

        try {
            NERtcEx.getInstance().init(ContextUtils.getApplicationContext(), BuildConfig.APP_KEY, InitCallBack(), null)
        } catch (e: Exception) {
            initException("setAvParameters " + e.message, mRoomId)
        }
        if (enableVido){
            videoView?.let {
                //加入房间前预览，以免黑屏时前过长
                videoView.visibility = View.VISIBLE
                NERtcEx.getInstance().startVideoPreview()
                NERtcEx.getInstance().setupLocalVideoCanvas(it)
            }
        }
    }

    /**
     * 音频通话设置参数
     */
    private fun setAudioParameters() {
        val parameters = NERtcParameters()
        NERtcEx.getInstance().setParameters(parameters) //先设置参数，后初始化
        try {
            NERtcEx.getInstance().init(ContextUtils.getApplicationContext(), BuildConfig.APP_KEY, InitCallBack(), null)
        } catch (e: Exception) {
            initException("setAudioParameters " + e.message, mRoomId)
        }
        val i = NERtcEx.getInstance().enableLocalAudio(true)
        if (i != 0) {
            initException("enableLocalAudio return code:$i", mRoomId)
        }
    }

    /**
     * 初始化回调
     */
    private inner class InitCallBack : NERtcCallbackEx {
        override fun onJoinChannel(resultCode: Int, channelId: Long, l1: Long) {
            NERtcEx.getInstance().enableAudioVolumeIndication(true, 200)
            if (resultCode == 0) {
                //关闭耳返
                EventBus.getDefault().post(EventCenter<Any?>(Constants.ACTION_P2P_JOIN_ROOM))
                NERtcEx.getInstance().enableEarback(false, 0)
                mRoomListener?.onJoinRoomSuccess(channelId)
                if (isAudio) {
                    EventBus.getDefault().post(EventCenter<Any?>(Constants.ACTION_MATCH_JOIN_ROOM_SUCCESS))
                }
            } else {
                LogUtils.d("&&&&&&&&&&&&&&&&i="+resultCode+ " channelId="+channelId +" l1 = "+l1)
                mRoomListener?.onJoinRoomFailed(resultCode)
            }
        }

        override fun onLeaveChannel(i: Int) {
            mRoomListener?.onVideoLeave()
        }

        override fun onUserJoined(l: Long) {
            mRoomListener?.onUserJoined(l.toString())
        }

        override fun onUserLeave(l: Long, i: Int) {
            mRoomListener?.onUserLeave(l.toString())
        }

        override fun onUserAudioStart(l: Long) {}
        override fun onUserAudioStop(l: Long) {}
        override fun onUserVideoStart(l: Long, i: Int) {
            mRoomListener?.onUserVideoStart(l, i)
        }

        override fun onUserVideoStop(l: Long) {}
        override fun onDisconnect(i: Int) {
            mRoomListener?.onDisconnect()
        }

        override fun onUserAudioMute(l: Long, b: Boolean) {}
        override fun onUserVideoMute(l: Long, b: Boolean) {}
        override fun onFirstAudioDataReceived(l: Long) {}
        override fun onFirstVideoDataReceived(l: Long) {}
        override fun onFirstAudioFrameDecoded(l: Long) {}
        override fun onFirstVideoFrameDecoded(l: Long, i: Int, i1: Int) {}
        override fun onUserVideoProfileUpdate(l: Long, i: Int) {}
        override fun onAudioDeviceChanged(i: Int) {}
        override fun onAudioDeviceStateChange(i: Int, i1: Int) {}
        override fun onVideoDeviceStageChange(i: Int) {}
        override fun onConnectionTypeChanged(newConnectionType: Int) {}
        override fun onReconnectingStart() {}
        override fun onReJoinChannel(i: Int, l: Long) {
            onReJoinChannel(i, l, mRoomId)
        }

        override fun onAudioMixingStateChanged(i: Int) {}
        override fun onAudioMixingTimestampUpdate(l: Long) {}
        override fun onAudioEffectFinished(i: Int) {}
        override fun onLocalAudioVolumeIndication(volume: Int) {
            val speakers: MutableMap<String, Int> = HashMap()
            speakers[AccountUtil.getInstance().userId] = volume
            EventBus.getDefault().post(EventCenter<Any?>(Constants.EVENT_CODE_REPORT_SPEAKER, speakers))
            EventBus.getDefault().post(EventCenter<Any?>(Constants.EVENT_CODE_REPORT_LOCALE_SPEAKER, volume))
            RoomLiveMonitorHelper.instance().updateRoomLiveLocalAudioVolume(volume)
        }

        override fun onRemoteAudioVolumeIndication(volumeArray: Array<NERtcAudioVolumeInfo>, totalVolume: Int) {
            val speakers: MutableMap<String, Int> = HashMap()
            for (neRtcAudioVolumeInfo in volumeArray) {
                speakers[neRtcAudioVolumeInfo.uid.toString() + ""] = neRtcAudioVolumeInfo.volume
            }
            EventBus.getDefault().post(EventCenter<Any?>(Constants.EVENT_CODE_REPORT_SPEAKER, speakers))
            FloatManager.getInstance().onAudioVolume(speakers)
        }

        override fun onLiveStreamState(s: String, s1: String, i: Int) {}
        override fun onConnectionStateChanged(i: Int, i1: Int) {}
        override fun onCameraFocusChanged(rect: Rect) {}
        override fun onCameraExposureChanged(rect: Rect) {}
        override fun onError(i: Int) {
            onError(i, mRoomId)
        }

        override fun onWarning(i: Int) {
            onWarning(i, mRoomId)
        }
    }

    //用于FaceUnity的回调
    var videoCallbackToFaceUnity: NERtcVideoCallback = object : NERtcVideoCallback {
        override fun onVideoCallback(neRtcVideoFrame: NERtcVideoFrame): Boolean {
            if (null == fURenderer) return false
            if (mIsFirstFrame) {
                mVideoEffectHandler = Handler(Looper.myLooper())
                fURenderer!!.onSurfaceCreated()
                mIsFirstFrame = false
                var neRtcVideoView:NERtcVideoView? = null
                var micPositon = FaceInfo.LIVE_ROOM_1
                if (this@AvHelper.context is RoomLiveActivityV2){
                    micPositon = FaceInfo.LIVE_ROOM_1
                    neRtcVideoView = (this@AvHelper.context as RoomLiveActivityV2).getBoyNeRtcVideoView();
                }
                if (this@AvHelper.context is VideoMatchChattingActivity){
                    micPositon = FaceInfo.VIDEO_MATCH
                    neRtcVideoView = (this@AvHelper.context as VideoMatchChattingActivity).getMySelfRtcVideoView();
                }
                RoomLiveMonitorHelper.instance().start(neRtcVideoView, this@AvHelper.mRoomId,micPositon)
                return false
            }
            val textId = fURenderer!!.onDrawFrameSelf(neRtcVideoFrame.data, neRtcVideoFrame.textureId,
                    neRtcVideoFrame.width, neRtcVideoFrame.height)
            neRtcVideoFrame.textureId = textId
            if (neRtcVideoFrame.format == NERtcVideoFrame.Format.TEXTURE_OES) {
                neRtcVideoFrame.format = NERtcVideoFrame.Format.TEXTURE_RGB
            }
            RoomLiveMonitorHelper.instance().getFaceData()
            return true
        }
    }

    /**
     * 打开摄像头
     *
     * @param render
     */
    @Deprecated("音视频默认开启摄像头，此方法废弃")
    fun openCamera(render: NERtcVideoView?) {
        NERtcEx.getInstance().startVideoPreview()
        NERtcEx.getInstance().setupLocalVideoCanvas(null)
        NERtcEx.getInstance().setupLocalVideoCanvas(render)
    }

    /**
     * 关闭摄像头
     */
    @Deprecated("音视频默认开启摄像头，此方法废弃")
    fun closeCamera() {
        NERtcEx.getInstance().stopVideoPreview()
    }

}