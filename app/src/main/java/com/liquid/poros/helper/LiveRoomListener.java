package com.liquid.poros.helper;

public interface LiveRoomListener {

    void onUserLeave(String account);

    void onUserJoined(String account);

    void onJoinRoomSuccess(long channelId);

    void onJoinRoomFailed(int resultCode);

    void onVideoLeave();

    void onMatchLeave();

    void onUserVideoStart(long l,int i);

    void onDisconnect();
}
