package com.liquid.poros.helper;

import android.graphics.drawable.AnimationDrawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.audiofx.LoudnessEnhancer;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.liquid.poros.R;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

public class AudioPlayerHelper {
    private ImageView animationView;
    private MediaPlayer mediaPlayer;
    private boolean mIsMessage = false;
    private Timer mTimer2;
    private TimerTask mTask2;
    private int mAudioTimer;
    private static final int MSG = 1;
    private boolean isReceiveMsg = false;
    private LoudnessEnhancer loudnessEnhancer;

    public void startPlay(ImageView animationView, String audioPath, boolean isMessage, boolean isReceiveMsg) {
        if (this.animationView != animationView && this.animationView != null) {
            stopAnim();
        }
        this.animationView = animationView;
        this.mIsMessage = isMessage;
        this.isReceiveMsg = isReceiveMsg;
        startAnim();
        resetUrl(audioPath);
    }


    public AudioPlayerHelper() {
        initMediaPlayer();
    }

    public void initMediaPlayer() {
        try {
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mp, int what, int extra) {
                    mediaPlayer.reset();
                    return false;
                }
            });
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    stopAnim();
                }
            });
        } catch (Throwable e) {
        }
    }

    public void resetUrl(String url) {
        if (mediaPlayer.isPlaying() || TextUtils.isEmpty(url)) {
            return;
        }
        try {
            mediaPlayer.stop();
            mediaPlayer.reset();
            mediaPlayer.setDataSource(url);
            mediaPlayer.prepare();
            mediaPlayer.start();
            loudnessEnhancer = new LoudnessEnhancer(mediaPlayer.getAudioSessionId());
            loudnessEnhancer.setTargetGain(1500);
            loudnessEnhancer.setEnabled(true);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public void startAnim() {
        if (mIsMessage) {
            animationView.setBackgroundResource(isReceiveMsg ? R.drawable.audio_animation_list_left : R.drawable.audio_animation_list_right);
        } else {
            animationView.setBackgroundResource(R.drawable.anim_user_audio_list);
        }
        if (animationView.getBackground() instanceof AnimationDrawable) {
            AnimationDrawable animation = (AnimationDrawable) animationView.getBackground();
            animation.start();
        }
    }

    public void stopAnim() {
        if (animationView != null && animationView.getBackground() instanceof AnimationDrawable) {
            AnimationDrawable animation = (AnimationDrawable) animationView.getBackground();
            animation.stop();
            if (mIsMessage) {
                animationView.setBackgroundResource(isReceiveMsg ? R.mipmap.audio_animation_list_left_00000 : R.mipmap.audio_animation_list_right_00000);
            } else {
                animationView.setBackgroundResource(R.mipmap.icon_stop_audio);
            }
            animationView.clearAnimation();
            releaseTimer();
        }
    }

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            synchronized (AudioPlayerHelper.this) {
                mAudioTimer--;
                if (mAudioTimer >= 0) {
                    if (listener != null) {
                        listener.startTime(mAudioTimer);
                    }
                } else {
                    if (listener != null) {
                        listener.finishTime();
                    }
                }
            }
        }
    };

    public void startTimer(int time) {
        mAudioTimer = time;
        if (mTimer2 == null && mTask2 == null) {
            mTimer2 = new Timer();
            mTask2 = new TimerTask() {
                @Override
                public void run() {
                    mHandler.sendMessage(mHandler.obtainMessage(MSG));
                }
            };
            mTimer2.schedule(mTask2, 0, 1000);
        }
    }

    public void release() {
        if (mediaPlayer != null) {
            mediaPlayer.release();
            mediaPlayer = null;
        }
        if (loudnessEnhancer != null) {
            loudnessEnhancer.release();
        }
        releaseTimer();
    }

    private void releaseTimer() {
        if (mTask2 != null) {
            mTask2.cancel();
            mTask2 = null;
        }
        if (mTimer2 != null) {
            mTimer2.cancel();
            mTimer2 = null;
        }
    }

    private AudioPlayerHelper.AudioPlayListener listener;

    public AudioPlayerHelper setAudioListener(AudioPlayerHelper.AudioPlayListener listener) {
        this.listener = listener;
        return this;
    }

    public interface AudioPlayListener {
        //开始计时
        void startTime(int time);

        //结束计时
        void finishTime();

    }
}
