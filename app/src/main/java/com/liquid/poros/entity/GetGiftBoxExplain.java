package com.liquid.poros.entity;

import java.io.Serializable;

public class GetGiftBoxExplain implements Serializable {
    public String title;
    public String imgUrl;
    public String btnText;

    public GetGiftBoxExplain(String title, String imgUrl, String btnText) {
        this.title = title;
        this.imgUrl = imgUrl;
        this.btnText = btnText;
    }
}
