package com.liquid.poros.entity;

import java.util.List;

public class IndexCpInviteInfo extends BaseModel {
    private Data data;
    

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }


    public static class Data {
        private String desc;
        private boolean show;
        private long end_timestamp;
        private String room_id;
        private UserInfo user_info;

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public boolean isShow() {
            return show;
        }

        public void setShow(boolean show) {
            this.show = show;
        }

        public long getEnd_timestamp() {
            return end_timestamp;
        }

        public void setEnd_timestamp(long end_timestamp) {
            this.end_timestamp = end_timestamp;
        }

        public String getRoom_id() {
            return room_id;
        }

        public void setRoom_id(String room_id) {
            this.room_id = room_id;
        }

        public UserInfo getUser_info() {
            return user_info;
        }

        public void setUser_info(UserInfo user_info) {
            this.user_info = user_info;
        }

        public class UserInfo {
            private String nick_name;
            private String avatar_url;
            private int gender;
            private String age;
            private String user_id;
            private List<String> games;
            private List<RefItem> photo_list;

            public String getNick_name() {
                return nick_name;
            }

            public void setNick_name(String nick_name) {
                this.nick_name = nick_name;
            }

            public String getAvatar_url() {
                return avatar_url;
            }

            public void setAvatar_url(String avatar_url) {
                this.avatar_url = avatar_url;
            }

            public int getGender() {
                return gender;
            }

            public void setGender(int gender) {
                this.gender = gender;
            }

            public String getAge() {
                return age;
            }

            public void setAge(String age) {
                this.age = age;
            }

            public List<String> getGames() {
                return games;
            }

            public void setGames(List<String> games) {
                this.games = games;
            }

            public List<RefItem> getPhoto_list() {
                return photo_list;
            }

            public void setPhoto_list(List<RefItem> photo_list) {
                this.photo_list = photo_list;
            }

            public String getUser_id() {
                return user_id;
            }

            public void setUser_id(String user_id) {
                this.user_id = user_id;
            }

            public class RefItem {
                private int type;
                private String path;

                public int getType() {
                    return type;
                }

                public void setType(int type) {
                    this.type = type;
                }

                public String getPath() {
                    return path;
                }

                public void setPath(String path) {
                    this.path = path;
                }
            }
        }
    }
}
