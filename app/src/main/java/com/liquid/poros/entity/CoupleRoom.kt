package com.liquid.poros.entity

import com.liquid.im.kit.bean.CoupleGiftBoostBean

/**
 * CP房间信息
 */
data class CoupleRoom(
        var anchor_info: MicUserInfo? = null,
        var pos_1_info: MicUserInfo? = null,
        var pos_2_info: MicUserInfo? = null,
        var im_room_id: String,
        var im_video_token: String,
        var cid: String,
        var http_pull_url: String,
        var hls_pull_url: String,
        var rtmp_pull_url: String,
        var push_url: String,
        var gender: Int,
        var room_desc: String,
        var mic_card: MicCard,
        var is_online: Boolean,
        var is_couple: Boolean,
        var couple_score: String,
        var couple_end_time: String,
        var coin: Long,
        var is_mic_free: Boolean,
        var default_camera: Boolean,
        var is_mic_minute_cost: Boolean,
        var mic_price: String,
        var request_onmic: Boolean,
        var has_mic_card: Boolean,
        var show_gift_icon: Boolean,
        var gift_list: List<MsgUserModel.GiftBean>,
        var gift_num_list: List<MsgUserModel.GiftNums>,
        var room_type: Int,
        var only_voice_room:Boolean,
        var private_room_coin_consume_str: String,
        var pk_status: Int,
        var pk_info: PkInfo? = null,
        var cpgroup_info: CpGroupInfo? = null,
        var pos_0_rank_top_avatars: List<String>? = null,
        var pos_2_rank_top_avatars: List<String>? = null,
        var gift_boost_status: Any,//是否是助力,为空,对照组;false 未设置助力，true，助力已设置
        var gift_boost_brief_info: GiftBoostInfo,
        var lottery_times: String, //助力宝箱总个数
) : BaseBean() {
    var voice_chatting: Int? = 0
        get() = field ?:0
    class PkInfo {
        var pos_1_pk_score: Long = 0
        var pos_2_pk_score: Long = 0
        var pos_1_rank_top_avatars: List<String>? = null
        var pos_2_rank_top_avatars: List<String>? = null
    }

    class MicCard {
        var desc: String? = null
        var continue_desc: String? = null
        var real_price = 0
        var ori_price = 0
    }
    class  CpGroupInfo{
        var user_in_group: Boolean? = null
        var members_count  = 0
    }
    class GiftBoostInfo{
        var title: String? = null
        var h5: String? = null
        var gift_boost_info_list: List<CoupleGiftBoostBean>? = null
    }
}

