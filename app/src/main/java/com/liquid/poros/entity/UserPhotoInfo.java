package com.liquid.poros.entity;

import java.util.List;

/**
 * 个人相册
 * 使用V2版本
 */
@Deprecated
public class UserPhotoInfo {
    private int code;
    private String msg;
    private Data data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public static class Data {
        private List<Photos> photos;

        public List<Photos> getPhotos() {
            return photos;
        }

        public void setPhotos(List<Photos> photos) {
            this.photos = photos;
        }

        public static class Photos {
            private String photo_id;
            private int type;
            private String path;

            public String getPhoto_id() {
                return photo_id;
            }

            public void setPhoto_id(String photo_id) {
                this.photo_id = photo_id;
            }

            public int getType() {
                return type;
            }

            public void setType(int type) {
                this.type = type;
            }

            public String getPath() {
                return path;
            }

            public void setPath(String path) {
                this.path = path;
            }
        }

    }
}
