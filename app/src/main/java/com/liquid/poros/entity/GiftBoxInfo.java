package com.liquid.poros.entity;

import java.io.Serializable;
import java.util.List;

public class GiftBoxInfo extends BaseBean implements Serializable {

    public int gift_box_count;// 礼盒数
    public int gift_box_key_count;// 钥匙数
    public int open_box_need_key;// 开1个礼盒需要几个钥匙
    public String prize_list_image;//点击跳转奖池




    public GetedGiftOfBoxRecordInfo reward_info;

    public static class GetedGiftOfBoxRecordInfo implements Serializable{
        public List<GetedGiftOfBoxRecord> record_list;
        public String recordListStr;
        public int show_time;//每条滚动时长：秒
    }

    public static class GetedGiftOfBoxRecord implements Serializable{
        public String record_id;//
        public String nick_name;//
        public String gift_name;//
        public String gift_image;//
        public String desc;// 整段描述
    }


    //请求拆礼盒需要的参数
    public String from_user_id;



}
