package com.liquid.poros.entity;

public class AlipayInfo {
    private int code;
    private Data data;
    private String msg;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public static class Data {
        private RechargeData recharge_data;
        private String order_id;

        public RechargeData getRecharge_data() {
            return recharge_data;
        }

        public void setRecharge_data(RechargeData recharge_data) {
            this.recharge_data = recharge_data;
        }

        public String getOrder_id() {
            return order_id;
        }

        public void setOrder_id(String order_id) {
            this.order_id = order_id;
        }

        public static class RechargeData {
            private String order_string;

            public String getOrder_string() {
                return order_string;
            }

            public void setOrder_string(String order_string) {
                this.order_string = order_string;
            }
        }
    }

}
