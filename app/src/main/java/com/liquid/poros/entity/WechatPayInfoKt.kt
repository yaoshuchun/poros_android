package com.liquid.poros.entity

data class WechatPayInfoKt(val data: WeChatPayInfo.Data) : BaseBean()