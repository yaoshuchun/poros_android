package com.liquid.poros.entity;
import java.util.List;
public class PtaSuitListInfo extends BaseBean {
    private List<SuitListInfo> suit_list;

    public List<SuitListInfo> getSuit_list() {
        return suit_list;
    }

    public void setSuit_list(List<SuitListInfo> suit_list) {
        this.suit_list = suit_list;
    }

    public class SuitListInfo {
        private String _id;
        private String icon_url;
        private String name;
        private List<PriceListInfo> price_coins_list;
        private boolean isSuitSelect =false;
        public String get_id() {
            return _id;
        }

        public void set_id(String _id) {
            this._id = _id;
        }

        public String getIcon_url() {
            return icon_url;
        }

        public void setIcon_url(String icon_url) {
            this.icon_url = icon_url;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<PriceListInfo> getPrice_coins_list() {
            return price_coins_list;
        }

        public void setPrice_coins_list(List<PriceListInfo> price_coins_list) {
            this.price_coins_list = price_coins_list;
        }

        public boolean isSuitSelect() {
            return isSuitSelect;
        }

        public void setSuitSelect(boolean suitSelect) {
            isSuitSelect = suitSelect;
        }

        public class PriceListInfo {
            private String coin;
            private String days;
            private boolean isPriceSelect =false;
            public String getCoin() {
                return coin;
            }

            public void setCoin(String coin) {
                this.coin = coin;
            }

            public String getDays() {
                return days;
            }

            public void setDays(String days) {
                this.days = days;
            }

            public boolean isPriceSelect() {
                return isPriceSelect;
            }

            public void setPriceSelect(boolean priceSelect) {
                isPriceSelect = priceSelect;
            }
        }
    }
}
