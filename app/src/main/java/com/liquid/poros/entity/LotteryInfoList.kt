package com.liquid.poros.entity

class LotteryInfoList : BaseBean() {

    var record_list : ArrayList<LotterryInfo> ?= null
    var last_id : String? = null

    class LotterryInfo : BaseBean() {
        var gift_image: String? = null
        var gift_name: String? = null
        var create_time: String? = null
    }

}