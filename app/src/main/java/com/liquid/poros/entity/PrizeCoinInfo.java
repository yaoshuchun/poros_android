package com.liquid.poros.entity;

import java.util.List;

public class PrizeCoinInfo extends BaseModel {
    private PrizeCoin data;

    public PrizeCoin getData() {
        return data;
    }

    public void setData(PrizeCoin data) {
        this.data = data;
    }

    public class PrizeCoin {
        private boolean show_page;
        private String title;
        private String content;
        private String tips;
        private List<PrizeItem> prize_items;
        private String reward_type;
        private List<String> today_reward;

        public String getReward_type() {
            return reward_type;
        }

        public void setReward_type(String reward_type) {
            this.reward_type = reward_type;
        }

        public List<String> getToday_reward() {
            return today_reward;
        }

        public void setToday_reward(List<String> today_reward) {
            this.today_reward = today_reward;
        }

        public String getTodayReward() {
            StringBuilder sb = new StringBuilder();
            int pos = 0;
            for (String s : today_reward) {
                sb.append(s);
                if (pos < today_reward.size() - 1) {
                    sb.append(" | ");
                }
                ++pos;
            }
            return sb.toString();
        }


        public boolean isShow_page() {
            return show_page;
        }

        public void setShow_page(boolean show_page) {
            this.show_page = show_page;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getTips() {
            return tips;
        }

        public void setTips(String tips) {
            this.tips = tips;
        }

        public List<PrizeItem> getPrize_items() {
            return prize_items;
        }

        public void setPrize_items(List<PrizeItem> prize_items) {
            this.prize_items = prize_items;
        }
    }

    public class PrizeItem {
        private String day_str;
        private String coin_str;
        private boolean has_rcv;
        private boolean is_current;
        private String mic_str;
        private String team_str;
        private String image_url;
        private boolean future;
        private String desc;
        private String card_str;

        public boolean isFuture() {
            return future;
        }

        public void setFuture(boolean future) {
            this.future = future;
        }

        public String getImage_url() {
            return image_url;
        }

        public void setImage_url(String image_url) {
            this.image_url = image_url;
        }

        public String getMic_str() {
            return mic_str;
        }

        public void setMic_str(String mic_str) {
            this.mic_str = mic_str;
        }

        public String getTeam_str() {
            return team_str;
        }

        public void setTeam_str(String team_str) {
            this.team_str = team_str;
        }

        public String getDay_str() {
            return day_str;
        }

        public void setDay_str(String day_str) {
            this.day_str = day_str;
        }

        public String getCoin_str() {
            return coin_str;
        }

        public void setCoin_str(String coin_str) {
            this.coin_str = coin_str;
        }

        public boolean isHas_rcv() {
            return has_rcv;
        }

        public void setHas_rcv(boolean has_rcv) {
            this.has_rcv = has_rcv;
        }

        public boolean isIs_current() {
            return is_current;
        }

        public void setIs_current(boolean is_current) {
            this.is_current = is_current;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public String getCard_str() {
            return card_str;
        }

        public void setCard_str(String card_str) {
            this.card_str = card_str;
        }
    }
}
