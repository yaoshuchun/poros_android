package com.liquid.poros.entity;

import java.util.List;

public class GameRoomInfo extends BaseModel {

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        private LeaderInfoBean leader_info;
        private TeamInfoBean team_info;
        private List<MemberGroupBean> member_group;
        private String tips;
        private int tips_style;
        private String im_video_token;

        public String getIm_video_token() {
            return im_video_token;
        }

        public void setIm_video_token(String im_video_token) {
            this.im_video_token = im_video_token;
        }

        public String getTips() {
            return tips;
        }

        public void setTips(String tips) {
            this.tips = tips;
        }

        public int getTips_style() {
            return tips_style;
        }

        public void setTips_style(int tips_style) {
            this.tips_style = tips_style;
        }

        public LeaderInfoBean getLeader_info() {
            return leader_info;
        }

        public void setLeader_info(LeaderInfoBean leader_info) {
            this.leader_info = leader_info;
        }

        public TeamInfoBean getTeam_info() {
            return team_info;
        }

        public void setTeam_info(TeamInfoBean team_info) {
            this.team_info = team_info;
        }

        public List<MemberGroupBean> getMember_group() {
            return member_group;
        }

        public void setMember_group(List<MemberGroupBean> member_group) {
            this.member_group = member_group;
        }

        public static class LeaderInfoBean {
            private String user_id;
            private String nick_name;
            private String avatar_url;
            private String game_nick_name;
            private String game_zone;

            public String getUser_id() {
                return user_id;
            }

            public void setUser_id(String user_id) {
                this.user_id = user_id;
            }

            public String getNick_name() {
                return nick_name;
            }

            public void setNick_name(String nick_name) {
                this.nick_name = nick_name;
            }

            public String getAvatar_url() {
                return avatar_url;
            }

            public void setAvatar_url(String avatar_url) {
                this.avatar_url = avatar_url;
            }

            public String getGame_nick_name() {
                return game_nick_name;
            }

            public void setGame_nick_name(String game_nick_name) {
                this.game_nick_name = game_nick_name;
            }

            public String getGame_zone() {
                return game_zone;
            }

            public void setGame_zone(String game_zone) {
                this.game_zone = game_zone;
            }
        }

        public static class TeamInfoBean {
            private String _id;
            private String team_im_id;
            private String group_id;
            private String leader_id;
            private String team_name;
            private boolean is_matching;
            private int members_total;
            private boolean is_public;
            private String team_deep_link;
            private String create_time;

            public String get_id() {
                return _id;
            }

            public void set_id(String _id) {
                this._id = _id;
            }

            public String getTeam_im_id() {
                return team_im_id;
            }

            public void setTeam_im_id(String team_im_id) {
                this.team_im_id = team_im_id;
            }

            public String getGroup_id() {
                return group_id;
            }

            public void setGroup_id(String group_id) {
                this.group_id = group_id;
            }

            public String getLeader_id() {
                return leader_id;
            }

            public void setLeader_id(String leader_id) {
                this.leader_id = leader_id;
            }

            public String getTeam_name() {
                return team_name;
            }

            public void setTeam_name(String team_name) {
                this.team_name = team_name;
            }

            public boolean isIs_matching() {
                return is_matching;
            }

            public void setIs_matching(boolean is_matching) {
                this.is_matching = is_matching;
            }

            public int getMembers_total() {
                return members_total;
            }

            public void setMembers_total(int members_total) {
                this.members_total = members_total;
            }

            public boolean isIs_public() {
                return is_public;
            }

            public void setIs_public(boolean is_public) {
                this.is_public = is_public;
            }

            public String getTeam_deep_link() {
                return team_deep_link;
            }

            public void setTeam_deep_link(String team_deep_link) {
                this.team_deep_link = team_deep_link;
            }

            public String getCreate_time() {
                return create_time;
            }

            public void setCreate_time(String create_time) {
                this.create_time = create_time;
            }
        }

        public static class MemberGroupBean {
            private String user_id;
            private int join_time;
            private String nick_name;
            private String avatar_url;
            private String game_nick_name;
            private String game_zone;

            public String getUser_id() {
                return user_id;
            }

            public void setUser_id(String user_id) {
                this.user_id = user_id;
            }

            public int getJoin_time() {
                return join_time;
            }

            public void setJoin_time(int join_time) {
                this.join_time = join_time;
            }

            public String getNick_name() {
                return nick_name;
            }

            public void setNick_name(String nick_name) {
                this.nick_name = nick_name;
            }

            public String getAvatar_url() {
                return avatar_url;
            }

            public void setAvatar_url(String avatar_url) {
                this.avatar_url = avatar_url;
            }

            public String getGame_nick_name() {
                return game_nick_name;
            }

            public void setGame_nick_name(String game_nick_name) {
                this.game_nick_name = game_nick_name;
            }

            public String getGame_zone() {
                return game_zone;
            }

            public void setGame_zone(String game_zone) {
                this.game_zone = game_zone;
            }
        }
    }
}
