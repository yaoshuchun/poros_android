package com.liquid.poros.entity;

import java.util.List;

/**
 * 点赞卡
 */

public class LikeCardInfo {
    private int code;
    private Data data;
    private String msg;

    public void setCode(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public class Data {
        private int gift_card_cnt;
        private List<Record> record_list;

        public class Record {
            private String desc;
            private int change_cnt;
            private String create_time;

            public String getDesc() {
                return desc;
            }

            public void setDesc(String desc) {
                this.desc = desc;
            }

            public int getChange_cnt() {
                return change_cnt;
            }

            public void setChange_cnt(int change_cnt) {
                this.change_cnt = change_cnt;
            }

            public String getCreate_time() {
                return create_time;
            }

            public void setCreate_time(String create_time) {
                this.create_time = create_time;
            }
        }

        public int getGift_card_cnt() {
            return gift_card_cnt;
        }

        public void setGift_card_cnt(int gift_card_cnt) {
            this.gift_card_cnt = gift_card_cnt;
        }

        public List<Record> getRecord_list() {
            return record_list;
        }

        public void setRecord_list(List<Record> record_list) {
            this.record_list = record_list;
        }
    }
}
