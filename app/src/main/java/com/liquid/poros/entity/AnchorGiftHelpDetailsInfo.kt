package com.liquid.poros.entity

/**
 * 主持人助力详情
 */
data class AnchorGiftHelpDetailsInfo(var title: String? = null, var tip: String? = null, var gift_boost_info_list: List<GiftBoostBean>? = null) : BaseBean()

data class GiftBoostBean(var gift_name: String? = null,
                         var gift_icon: String? = null,
                         var gift_id: String? = null,
                         var total_count: Int = 0,
                         var boost_count: Int = 0,
                         var boost_male_guest_info_list: List<GiftBoostRankBean>? = null)


data class GiftBoostRankBean(var avatar_url: String ? = null)