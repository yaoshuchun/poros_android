package com.liquid.poros.entity;

public class CommentInfo {
    public String comment_id;
    public String user_id;
    public String nick_name;
    public String avatar_url;
    public String text;
    public String create_time;
}
