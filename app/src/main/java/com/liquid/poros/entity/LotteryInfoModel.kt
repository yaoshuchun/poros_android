package com.liquid.poros.entity

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.liquid.library.retrofitx.RetrofitX
import com.liquid.poros.http.ApiUrl
import com.liquid.poros.http.observer.ObjectObserver
import com.liquid.poros.http.utils.getPoros
import com.liquid.poros.http.utils.postPoros

class LotteryInfoModel: ViewModel() {
    val success: MutableLiveData<LotteryInfoList> by lazy {
        MutableLiveData<LotteryInfoList>()
    }

    val fail : MutableLiveData<LotteryInfoList> by lazy {
        MutableLiveData<LotteryInfoList>()
    }

    /**
     * 获取中奖列表信息
     */
    fun getLotteryList() {
        RetrofitX.url(ApiUrl.LOTTERY_INFO_LIST)
                .getPoros()
                .subscribe(object : ObjectObserver<LotteryInfoList>() {
                    override fun success(result: LotteryInfoList) {
                        success.postValue(result)
                    }

                    override fun failed(result: LotteryInfoList) {
                        fail.postValue(result)
                    }
                })
    }

}