package com.liquid.poros.entity;

/**
 * 购买CP卡/邀请组CP/同意组CP
 */

public class BuyCoupleCardInfo {
    private int code;
    private Data data;
    private String msg;

    public void setCode(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public class Data extends BaseBean{
        private String cp_order_id;
        private String cp_card_desc;
        private String cp_card_title1;
        private String cp_card_title2;
        private String cp_card_title3;
        private String cp_card_title4;
        private String cp_card_title5;
        private String cp_card_title6;
        private int get_gift_box;

        public int getGet_gift_box() {
            return get_gift_box;
        }

        public void setGet_gift_box(int get_gift_box) {
            this.get_gift_box = get_gift_box;
        }

        public String getCp_order_id() {
            return cp_order_id;
        }

        public void setCp_order_id(String cp_order_id) {
            this.cp_order_id = cp_order_id;
        }

        public String getCp_card_desc() {
            return cp_card_desc;
        }

        public void setCp_card_desc(String cp_card_desc) {
            this.cp_card_desc = cp_card_desc;
        }

        public String getCp_card_title1() {
            return cp_card_title1;
        }

        public void setCp_card_title1(String cp_card_title1) {
            this.cp_card_title1 = cp_card_title1;
        }

        public String getCp_card_title2() {
            return cp_card_title2;
        }

        public void setCp_card_title2(String cp_card_title2) {
            this.cp_card_title2 = cp_card_title2;
        }

        public String getCp_card_title3() {
            return cp_card_title3;
        }

        public void setCp_card_title3(String cp_card_title3) {
            this.cp_card_title3 = cp_card_title3;
        }

        public String getCp_card_title4() {
            return cp_card_title4;
        }

        public void setCp_card_title4(String cp_card_title4) {
            this.cp_card_title4 = cp_card_title4;
        }

        public String getCp_card_title5() {
            return cp_card_title5;
        }

        public void setCp_card_title5(String cp_card_title5) {
            this.cp_card_title5 = cp_card_title5;
        }

        public String getCp_card_title6() {
            return cp_card_title6;
        }

        public void setCp_card_title6(String cp_card_title6) {
            this.cp_card_title6 = cp_card_title6;
        }
    }
}
