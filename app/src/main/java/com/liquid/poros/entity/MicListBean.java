package com.liquid.poros.entity;

import java.util.List;

public class MicListBean {

    /**
     * code : 0
     * data : {"items":[{"age":18,"avatar_url":"http://poros-static.liquidnetwork.com/avatar/43450421_1604316753.png","coins":0,"gender":1,"hpjy_game_area":"微信","hpjy_game_nick":null,"im_account":"43450421","nick_name":"更丰富发的","user_id":"43450421"}]}
     * msg : 成功
     */

    private int code;
    private DataBean data;
    private String msg;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public static class DataBean {
        /**
         * age : 18
         * avatar_url : http://poros-static.liquidnetwork.com/avatar/43450421_1604316753.png
         * coins : 0
         * gender : 1
         * hpjy_game_area : 微信
         * hpjy_game_nick : null
         * im_account : 43450421
         * nick_name : 更丰富发的
         * user_id : 43450421
         */

        private List<ItemsBean> items;

        public List<ItemsBean> getItems() {
            return items;
        }

        public void setItems(List<ItemsBean> items) {
            this.items = items;
        }

        public static class ItemsBean {
            private int age;
            private String avatar_url;
            private int coins;
            private int gender;
            private String hpjy_game_area;
            private Object hpjy_game_nick;
            private String im_account;
            private String nick_name;
            private String user_id;
            private boolean could_invite;

            public int getAge() {
                return age;
            }

            public void setAge(int age) {
                this.age = age;
            }

            public String getAvatar_url() {
                return avatar_url;
            }

            public void setAvatar_url(String avatar_url) {
                this.avatar_url = avatar_url;
            }

            public int getCoins() {
                return coins;
            }

            public void setCoins(int coins) {
                this.coins = coins;
            }

            public int getGender() {
                return gender;
            }

            public void setGender(int gender) {
                this.gender = gender;
            }

            public String getHpjy_game_area() {
                return hpjy_game_area;
            }

            public void setHpjy_game_area(String hpjy_game_area) {
                this.hpjy_game_area = hpjy_game_area;
            }

            public Object getHpjy_game_nick() {
                return hpjy_game_nick;
            }

            public void setHpjy_game_nick(Object hpjy_game_nick) {
                this.hpjy_game_nick = hpjy_game_nick;
            }

            public String getIm_account() {
                return im_account;
            }

            public void setIm_account(String im_account) {
                this.im_account = im_account;
            }

            public String getNick_name() {
                return nick_name;
            }

            public void setNick_name(String nick_name) {
                this.nick_name = nick_name;
            }

            public String getUser_id() {
                return user_id;
            }

            public void setUser_id(String user_id) {
                this.user_id = user_id;
            }

            public boolean isCould_invite() {
                return could_invite;
            }

            public void setCould_invite(boolean could_invite) {
                this.could_invite = could_invite;
            }
        }
    }
}
