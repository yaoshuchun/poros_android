package com.liquid.poros.entity;

public class IsOnMic extends BaseModel {
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data{
        public boolean isIs_on_mic() {
            return is_on_mic;
        }

        public void setIs_on_mic(boolean is_on_mic) {
            this.is_on_mic = is_on_mic;
        }

        private boolean is_on_mic;
    }

}
