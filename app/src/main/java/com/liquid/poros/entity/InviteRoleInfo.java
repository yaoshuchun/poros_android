package com.liquid.poros.entity;

/**
 * Created by sundroid on 24/07/2019.
 */

public class InviteRoleInfo {
    private int code;
    private Data data;
    private String msg;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public class Data {
        private int p2p_invite_rule;

        public int getP2p_invite_rule() {
            return p2p_invite_rule;
        }

        public void setP2p_invite_rule(int p2p_invite_rule) {
            this.p2p_invite_rule = p2p_invite_rule;
        }
    }
}
