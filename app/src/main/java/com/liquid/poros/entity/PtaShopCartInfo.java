package com.liquid.poros.entity;

import java.util.ArrayList;
import java.util.List;

public class PtaShopCartInfo {
    private int code;
    private Data data;
    private String msg;

    public void setCode(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public static class Data {
        private List<PtaShopItem> pta_shop_list;

        public List<PtaShopItem> getPta_shop_list() {
            return pta_shop_list;
        }

        public void setPta_shop_list(List<PtaShopItem> pta_shop_list) {
            this.pta_shop_list = pta_shop_list;
        }

        public static class PtaShopItem {
            public PtaShopItem() {
            }
            private String pta_title;
            private String pta_image;
            private String pta_coin;
            private String pta_id;
            private List<PtaDurationItem> ptaDuration;
            private boolean isSelect = true;

            public String getPta_title() {
                return pta_title;
            }

            public void setPta_title(String pta_title) {
                this.pta_title = pta_title;
            }

            public String getPta_image() {
                return pta_image;
            }

            public void setPta_image(String pta_image) {
                this.pta_image = pta_image;
            }

            public String getPta_coin() {
                return pta_coin;
            }

            public void setPta_coin(String pta_coin) {
                this.pta_coin = pta_coin;
            }

            public String getPta_id() {
                return pta_id;
            }

            public void setPta_id(String pta_id) {
                this.pta_id = pta_id;
            }

            public List<PtaDurationItem> getPtaDuration() {
                return ptaDuration;
            }

            public void setPtaDuration(List<PtaDurationItem> ptaDuration) {
                this.ptaDuration = ptaDuration;
            }

            public boolean isSelect() {
                return isSelect;
            }

            public void setSelect(boolean select) {
                isSelect = select;
            }

            public static class PtaDurationItem {
                private int ptaDay;
                private int ptaMoney;
                private boolean isPtaDurationSelect = false;
                public PtaDurationItem(int ptaDay, int ptaMoney,boolean isPtaDurationSelect) {
                    this.ptaDay = ptaDay;
                    this.ptaMoney = ptaMoney;
                    this.isPtaDurationSelect = isPtaDurationSelect;
                }
                public int getPtaDay() {
                    return ptaDay;
                }

                public void setPtaDay(int ptaDay) {
                    this.ptaDay = ptaDay;
                }

                public int getPtaMoney() {
                    return ptaMoney;
                }

                public void setPtaMoney(int ptaMoney) {
                    this.ptaMoney = ptaMoney;
                }

                public boolean isPtaDurationSelect() {
                    return isPtaDurationSelect;
                }

                public void setPtaDurationSelect(boolean ptaDurationSelect) {
                    isPtaDurationSelect = ptaDurationSelect;
                }
            }
        }
    }
}
