package com.liquid.poros.entity;

import java.io.Serializable;

public class Contacts implements Serializable {
    private String mName;
    private int mType;

    private int age;
    private String avatar_url;
    private int gender;
    private String hpjy_game_area;
    private String hpjy_game_nick;
    private String im_account;
    private String nick_name;
    private String user_id;
    private String first_letter;
    private String game_zone;

    public String getFirst_letter() {
        return first_letter;
    }

    public void setFirst_letter(String first_letter) {
        this.first_letter = first_letter;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getAvatar_url() {
        return avatar_url;
    }

    public void setAvatar_url(String avatar_url) {
        this.avatar_url = avatar_url;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getHpjy_game_area() {
        return hpjy_game_area;
    }

    public void setHpjy_game_area(String hpjy_game_area) {
        this.hpjy_game_area = hpjy_game_area;
    }

    public String getHpjy_game_nick() {
        return hpjy_game_nick;
    }

    public void setHpjy_game_nick(String hpjy_game_nick) {
        this.hpjy_game_nick = hpjy_game_nick;
    }

    public String getIm_account() {
        return im_account;
    }

    public void setIm_account(String im_account) {
        this.im_account = im_account;
    }

    public String getNick_name() {
        return nick_name;
    }

    public void setNick_name(String nick_name) {
        this.nick_name = nick_name;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public Contacts(String name, int type) {
        mName = name;
        mType = type;
    }

    public void setmType(int mType) {
        this.mType = mType;
    }

    public String getmName() {
        return mName;
    }

    public int getmType() {
        return mType;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getGame_zone() {
        return game_zone;
    }

    public void setGame_zone(String game_zone) {
        this.game_zone = game_zone;
    }
}
