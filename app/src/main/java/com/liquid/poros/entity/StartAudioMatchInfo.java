package com.liquid.poros.entity;

/**
 * 发起语音速配
 */

public class StartAudioMatchInfo {
    private int code;
    private Data data;
    private String msg;

    public void setCode(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public class Data {
        private boolean request_success;
        private boolean show_recharge_hint;
        private String text;
        private long seconds;
        public boolean isRequest_success() {
            return request_success;
        }

        public void setRequest_success(boolean request_success) {
            this.request_success = request_success;
        }

        public boolean isShow_recharge_hint() {
            return show_recharge_hint;
        }

        public void setShow_recharge_hint(boolean show_recharge_hint) {
            this.show_recharge_hint = show_recharge_hint;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public long getSeconds() {
            return seconds;
        }

        public void setSeconds(long seconds) {
            this.seconds = seconds;
        }
    }
}
