package com.liquid.poros.entity

import java.io.Serializable

data class Bag(var gift_list: ArrayList<BagBean>) : BaseBean()

data class BagBean(
        var info: CandyInfo?,
        var effect: Effect?,
        var coins: String = "0"
        , var record_id: Long = 0
        , var name: String = ""
        , var image: String = ""
        , var use_description: String = ""
        , var num: Int = 0
        , var type: Int = 0
        , var select: Boolean = false
        , var gift_id: Int = 0)

data class CandyInfo(var cash: String = "0", var num: String = "0")

data class Effect(var json: String = "", var audio: String = "") : Serializable