package com.liquid.poros.entity;

import java.util.List;

public class GuardRankerBean extends BaseModel{
    private GuardUserInfo guard_info;
    private String guard_rule;
    private boolean has_guard;
    private List<GuardRankerInfo> rank_list;
    private SelfRankInfo self_rank_info;

    public GuardUserInfo getGuard_info() {
        return guard_info;
    }

    public void setGuard_info(GuardUserInfo guard_info) {
        this.guard_info = guard_info;
    }

    public boolean isHas_guard() {
        return has_guard;
    }

    public void setHas_guard(boolean has_guard) {
        this.has_guard = has_guard;
    }

    public List<GuardRankerInfo> getRank_list() {
        return rank_list;
    }

    public void setRank_list(List<GuardRankerInfo> rank_list) {
        this.rank_list = rank_list;
    }

    public SelfRankInfo getSelf_rank_info() {
        return self_rank_info;
    }

    public void setSelf_rank_info(SelfRankInfo self_rank_info) {
        this.self_rank_info = self_rank_info;
    }

    public String getGuard_rule() {
        return guard_rule;
    }

    public void setGuard_rule(String guard_rule) {
        this.guard_rule = guard_rule;
    }
}
