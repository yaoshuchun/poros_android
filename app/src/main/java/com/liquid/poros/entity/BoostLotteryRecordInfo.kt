package com.liquid.poros.entity

/**
 * 助力中奖记录
 */
data class BoostLotteryRecordInfo(var last_id: String? = null, var record_list: List<BoostRecordBean>? = null) : BaseBean() {
    class BoostRecordBean {
        var create_time: String? = null
        var gift_image: String? = null
        var gift_name: String? = null
        var gift_num = 0
        var record_id: String? = null
    }
}