package com.liquid.poros.entity;


/**
 * Created by sundroid on 23/04/2019.
 */

public class TipItem {
    private String title;
    private int textColor;

    private MessageAction action;

    public MessageAction getAction() {
        return action;
    }

    public void setAction(MessageAction action) {
        this.action = action;
    }

    public TipItem(String title, int textColor, MessageAction action) {
        this.title = title;
        this.textColor = textColor;
        this.action = action;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getTextColor() {
        return textColor;
    }

    public void setTextColor(int textColor) {
        this.textColor = textColor;
    }
}