package com.liquid.poros.entity;

import java.util.List;

/**
 * Created by sundroid on 24/07/2019.
 */

public class MatchVoiceCallInfo extends BaseModel {
    public Data data;
    public class Data {
        public String room_id;
        public String im_room_id;
        public String im_video_token;
        public String textline;
        public String girl_team_text;
        public String boy_team_text;
        public Info girl_info;
        public Info boy_info;
        public Account boy_account;
    }

    public class Info{
        public String user_id;
        public String nick_name;
        public String avatar_url;
        public String age;
        public String gender;
    }

    public class Account{
        public String coins;
        public String free_minutes;
    }
}
