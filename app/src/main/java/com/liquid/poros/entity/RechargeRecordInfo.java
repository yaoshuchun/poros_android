package com.liquid.poros.entity;

import java.util.List;

/**
 * Created by sundroid on 26/04/2019.
 */

public class RechargeRecordInfo {
    private int code;
    private Data data;
    private String msg;

    public void setCode(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public class Data {
        private List<Records> records;

        public class Records {
            private String desc;
            private int coin;
            private String create_time;
            private String change_str;
            private String type_str;
            public String getDesc() {
                return desc;
            }

            public void setDesc(String desc) {
                this.desc = desc;
            }

            public int getCoin() {
                return coin;
            }

            public void setCoin(int coin) {
                this.coin = coin;
            }

            public String getCreate_time() {
                return create_time;
            }

            public void setCreate_time(String create_time) {
                this.create_time = create_time;
            }

            public String getChange_str() {
                return change_str;
            }

            public void setChange_str(String change_str) {
                this.change_str = change_str;
            }

            public String getType_str() {
                return type_str;
            }

            public void setType_str(String type_str) {
                this.type_str = type_str;
            }
        }

        public List<Records> getRecords() {
            return records;
        }

        public void setRecords(List<Records> records) {
            this.records = records;
        }
    }
}
