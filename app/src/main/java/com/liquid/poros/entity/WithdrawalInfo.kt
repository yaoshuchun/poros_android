package com.liquid.poros.entity

/**
 * 提现详情
 */
class WithdrawalInfo : BaseBean() {
    //余额相关
    var cash_info: CashInfoBean? = null

    //糖果相关
    var candy_info: CandyInfoBean? = null
    var certification = false
    var explanation: String? = null
    var is_bind_wechat = false
        private set
    var withdraw_channels: List<Int>? = null
    var login_from: String? = null
    class CashInfoBean {
        var cash_balance = 0
        var cash_balance_yuan = 0.0
        var withdraw_denomination_info_list: List<WithdrawBalanceBean>? = null

        class WithdrawBalanceBean {
            var cash = 0
            var cash_str: String? = null
            var cash_to_coin = 0

        }
    }

    class CandyInfoBean {
        var candy_count: String? = null
        var candy_to_cash: String? = null
        var candy_to_coin: String? = null

    }
}