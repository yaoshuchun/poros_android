package com.liquid.poros.entity;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.liquid.im.kit.custom.ConnectedAttachment;
import com.liquid.im.kit.custom.CustomAttachParser;
import com.liquid.im.kit.custom.RoomActionAttachment;
import com.liquid.poros.utils.AccountUtil;

import com.netease.nimlib.sdk.msg.attachment.AudioAttachment;
import com.netease.nimlib.sdk.msg.attachment.ImageAttachment;
import com.netease.nimlib.sdk.msg.attachment.MsgAttachment;
import com.netease.nimlib.sdk.msg.attachment.VideoAttachment;
import com.netease.nimlib.sdk.msg.constant.AttachStatusEnum;
import com.netease.nimlib.sdk.msg.constant.MsgDirectionEnum;
import com.netease.nimlib.sdk.msg.constant.MsgStatusEnum;
import com.netease.nimlib.sdk.msg.constant.MsgTypeEnum;
import com.netease.nimlib.sdk.msg.constant.SessionTypeEnum;
import com.netease.nimlib.sdk.msg.model.CustomMessageConfig;
import com.netease.nimlib.sdk.msg.model.IMMessage;
import com.netease.nimlib.sdk.msg.model.MemberPushOption;
import com.netease.nimlib.sdk.msg.model.MsgThreadOption;
import com.netease.nimlib.sdk.msg.model.NIMAntiSpamOption;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class IMMessageImpl implements IMMessage {
    private String fromAccount;
    private String uuid;
    private String body;
    private MsgAttachment attachment;
    private MsgTypeEnum msgTypeEnum;
    private String fromNick;
    private String msgType;
    private MsgDirectionEnum msgDirectionEnum;
    private long msgTimestamp;
    private String sessionId;
    private boolean is_recall;
    private boolean gift_unread; //true 标识未读
    private MsgStatusEnum msgStatusEnum = MsgStatusEnum.success;

    private Map<String, Object> map;

    public boolean parseJson(JSONObject msg) {
        this.fromAccount = msg.optString("fromAccount");
        this.uuid = msg.optString("_id");
        this.body = msg.optString("body");
        this.fromNick = msg.optString("fromNick");
        this.msgType = msg.optString("msgType");
        this.msgTimestamp = msg.optLong("msgTimestamp");
        this.sessionId = msg.optString("to");
        this.is_recall = msg.optBoolean("is_recall");
        this.gift_unread = msg.optBoolean("gift_unread");
        if (fromAccount.equals(AccountUtil.getInstance().getUserInfo().getIm_account())) {
            msgDirectionEnum = MsgDirectionEnum.Out;
        } else {
            msgDirectionEnum = MsgDirectionEnum.In;
        }
        final String attach = msg.optString("attach");
        final JSONObject ext_json = msg.optJSONObject("ext_json");
        if (ext_json != null) {
            msgStatusEnum = ext_json.optBoolean("send_fail", false) ? MsgStatusEnum.draft : MsgStatusEnum.success;

            try {
                Map<String, Object> map = toMap(ext_json);
                setRemoteExtension(map);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (TextUtils.isEmpty(attach)) {
            msgType = "TEXT";
        }
        switch (msgType) {
            case "AUDIO":
                this.msgTypeEnum = MsgTypeEnum.audio;
                this.attachment = new AudioAttachment(attach);
                break;
            case "TEXT":
                msgTypeEnum = MsgTypeEnum.text;
                this.attachment = new MsgAttachment() {
                    @Override
                    public String toJson(boolean b) {
                        return attach;
                    }
                };
                break;
            case "VIDEO":
                msgTypeEnum = MsgTypeEnum.video;
                this.attachment = new VideoAttachment(attach);
                break;
            case "IMAGE":
            case "PICTURE":
                msgTypeEnum = MsgTypeEnum.image;
                this.attachment = new ImageAttachment(attach);
                break;
            case "CUSTOM":
                msgTypeEnum = MsgTypeEnum.custom;
                CustomAttachParser parser = new CustomAttachParser();
                this.attachment = parser.parse(attach);
                if (attachment instanceof ConnectedAttachment || attachment instanceof RoomActionAttachment) {
                    return false;
                }
                break;
            default:
                msgTypeEnum = MsgTypeEnum.undef;
                break;
        }


        return true;
    }

    public static Map<String, Object> toMap(JSONObject jsonobj)  throws JSONException {
        Map<String, Object> map = new HashMap<String, Object>();
        Iterator<String> keys = jsonobj.keys();
        while(keys.hasNext()) {
            String key = keys.next();
            Object value = jsonobj.get(key);
            if (value instanceof JSONArray) {
                value = toList((JSONArray) value);
            } else if (value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            map.put(key, value);
        }   return map;
    }

    public static List<Object> toList(JSONArray array) throws JSONException {
        List<Object> list = new ArrayList<Object>();
        for(int i = 0; i < array.length(); i++) {
            Object value = array.get(i);
            if (value instanceof JSONArray) {
                value = toList((JSONArray) value);
            }
            else if (value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            list.add(value);
        }   return list;
    }

    @Override
    public String getUuid() {
        return uuid;
    }

    @Override
    public boolean isTheSame(IMMessage imMessage) {
        return false;
    }

    @Override
    public String getSessionId() {
        return sessionId;
    }

    public boolean isGift_unread() {
        return gift_unread;
    }

    public void setGift_unread(boolean gift_unread) {
        this.gift_unread = gift_unread;
    }

    @Override
    public SessionTypeEnum getSessionType() {
        return null;
    }

    @Override
    public String getFromNick() {
        return fromNick;
    }

    @Override
    public MsgTypeEnum getMsgType() {
        return msgTypeEnum;
    }

    @Override
    public int getSubtype() {
        return 0;
    }

    @Override
    public void setSubtype(int i) {

    }

    @Override
    public MsgStatusEnum getStatus() {
        return msgStatusEnum;
    }

    @Override
    public void setStatus(MsgStatusEnum msgStatusEnum) {

    }

    @Override
    public void setDirect(MsgDirectionEnum msgDirectionEnum) {

    }

    @Override
    public MsgDirectionEnum getDirect() {
        return msgDirectionEnum;
    }

    @Override
    public void setContent(String s) {

    }

    @Override
    public String getContent() {
        return body;
    }

    @Override
    public long getTime() {
        return msgTimestamp;
    }

    @Override
    public void setFromAccount(String s) {

    }

    @Override
    public String getFromAccount() {
        return fromAccount;
    }

    @Override
    public void setAttachment(MsgAttachment msgAttachment) {

    }

    @Override
    public MsgAttachment getAttachment() {
        return attachment;
    }

    @Override
    public String getAttachStr() {
        return null;
    }

    @Override
    public AttachStatusEnum getAttachStatus() {
        return null;
    }

    @Override
    public void setAttachStatus(AttachStatusEnum attachStatusEnum) {

    }

    @Override
    public CustomMessageConfig getConfig() {
        return null;
    }

    @Override
    public void setConfig(CustomMessageConfig customMessageConfig) {

    }

    @Override
    public Map<String, Object> getRemoteExtension() {
        return map;
    }

    @Override
    public void setRemoteExtension(Map<String, Object> map) {
        this.map = map;
    }

    @Override
    public Map<String, Object> getLocalExtension() {
        return null;
    }

    @Override
    public void setLocalExtension(Map<String, Object> map) {

    }

    @Override
    public String getCallbackExtension() {
        return null;
    }

    @Override
    public String getPushContent() {
        return null;
    }

    @Override
    public void setPushContent(String s) {

    }

    @Override
    public Map<String, Object> getPushPayload() {
        return null;
    }

    @Override
    public void setPushPayload(Map<String, Object> map) {

    }

    @Override
    public MemberPushOption getMemberPushOption() {
        return null;
    }

    @Override
    public void setMemberPushOption(MemberPushOption memberPushOption) {

    }

    @Override
    public boolean isRemoteRead() {
        return false;
    }

    @Override
    public boolean needMsgAck() {
        return false;
    }

    @Override
    public void setMsgAck() {

    }

    @Override
    public boolean hasSendAck() {
        return false;
    }

    @Override
    public int getTeamMsgAckCount() {
        return 0;
    }

    @Override
    public int getTeamMsgUnAckCount() {
        return 0;
    }

    @Override
    public int getFromClientType() {
        return 0;
    }

    @Override
    public NIMAntiSpamOption getNIMAntiSpamOption() {
        return null;
    }

    @Override
    public void setNIMAntiSpamOption(NIMAntiSpamOption nimAntiSpamOption) {

    }

    @Override
    public void setClientAntiSpam(boolean b) {

    }

    @Override
    public void setForceUploadFile(boolean b) {

    }

    @Override
    public boolean isInBlackList() {
        return false;
    }

    @Override
    public long getServerId() {
        return 0;
    }

    @Override
    public void setChecked(Boolean aBoolean) {

    }

    @Override
    public Boolean isChecked() {
        return null;
    }

    @Override
    public boolean isSessionUpdate() {
        return false;
    }

    @Override
    public void setSessionUpdate(boolean b) {

    }

    @Override
    public MsgThreadOption getThreadOption() {
        return null;
    }

    @Override
    public void setThreadOption(IMMessage imMessage) {

    }

    @Override
    public boolean isThread() {
        return false;
    }

    @Override
    public long getQuickCommentUpdateTime() {
        return 0;
    }

    @Override
    public boolean isDeleted() {
        return false;
    }

    @Override
    public String getYidunAntiCheating() {
        return null;
    }

    @Override
    public void setYidunAntiCheating(String s) {

    }

    @Override
    public String getEnv() {
        return null;
    }

    @Override
    public void setEnv(String s) {

    }

    public boolean isIs_recall() {
        return is_recall;
    }

    public void setIs_recall(boolean is_recall) {
        this.is_recall = is_recall;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IMMessageImpl imMessage = (IMMessageImpl) o;
        return Objects.equals(uuid, imMessage.uuid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uuid);
    }
}
