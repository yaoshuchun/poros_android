package com.liquid.poros.entity

import java.io.Serializable

/**
 * api返回数据基类
 */
open class BaseBean(
        var code: Int = SUCCESS,
        var msg: String? = null
) : Serializable {
    companion object {
        private const val serialVersionUID = 1577077472943461887L
        const val SUCCESS = 0
    }
}