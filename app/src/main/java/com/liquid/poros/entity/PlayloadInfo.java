package com.liquid.poros.entity;

/**
 * Created by sundroid on 24/07/2019.
 */

public class PlayloadInfo {

    //type 消息类型
    public final static int P2P = 1;                            //*P2P = 1                       私聊
    public final static int ORDER = 2;                          //  *ORDER = 2                     派单到领队
    public final static int P2P_TEAM = 3;                       //*P2P_TEAM = 3                  一键开黑邀请
    public final static int PRIZE = 4;                          //*PRIZE = 4     实时奖励
    public final static int OFFLINE_LEADER_NOTIFY = 5;          //  *OFFLINE_LEADER_NOTIFY = 5     离线领队通知
    public final static int GUILD_NEW_MEMBER = 6;               //  *GUILD_NEW_MEMBER = 6          公会新成员通知
    public final static int CHAT_ROOM_AT = 7;                   //  *CHAT_ROOM_AT = 7              聊天室@提醒
    public final static int P2P_NO_OFFLINE = 8;                 //    *P2P_NO_OFFLINE = 8            私聊不推送粉丝消息提示
    public final static int MASTER_APPRENTICE_TEAM = 9;         //  *MASTER_APPRENTICE_TEAM = 9    师徒组队接单房间
    public final static int CP_TEAM_INVITE = 10;                //  *CP_TEAM_INVITE = 10           CP开黑邀请
    public final static int NEW_USER_INVITE = 11;               // NEW_USER_INVITE = 11         （接单）叫单提醒消息
    public final static int CHAT_ROOM_MSG = 21;                 // *CHAT_ROOM_MSG = 21            文字频道消息
    public final static int CP_MATCHING_ROOM = 106;             //    *CP_MATCHING_ROOM = 106        速配成功
    public final static int VOICE_MATCH_INVITE_TEAM = 107;      //   VOICE_MATCH_INVITE_TEAM = 107      速配通话中收到邀请开黑
    public final static int VOICE_MATCH_INVITE_REFUSE = 108;    //    VOICE_MATCH_INVITE_REFUSE = 108  语音速配房间拒绝开黑邀请
    public final static int VOICE_MATCH_INVITE_AGREE = 109;     //   VOICE_MATCH_INVITE_AGREE = 109 语音速配房间接受开黑邀请
    public final static int VOICE_MATCH_INVITE_TIMEDOUT = 110;  //    VOICE_MATCH_INVITE_TIMEDOUT = 110  语音速配房间开黑邀请，对方超时拒绝

    private int type;
    private String team_id;
    private String msg_id;
    private String account;
    private long end_timestamp;
    private int remain_cnt;
    private boolean is_friend;
    private String nick_name;
    private String avatar_url;
    private String to_user_id;
    private String msg_content;
    private String msg_title;
    private String group_id;
    private String room_id;
    private String source;
    private String age;
    private String invite_id;


    private String msg_notify_title;//真正的标题，如果为空则使用msg_title字段
    private String voice_url;
    private String keep_float_window;
    private String c_toast;

    private boolean team_notice;
    public Details details;
    public String txt;
    public String coin;
    public String game;
    public String anchor_nick_name;
    public String anchor_avatar_url;
    public String desc;

    public boolean show_recharge;
    public boolean show_buy_dialog;

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getInvite_id() {
        return invite_id;
    }

    public void setInvite_id(String invite_id) {
        this.invite_id = invite_id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getAnchor_nick_name() {
        return anchor_nick_name;
    }

    public void setAnchor_nick_name(String anchor_nick_name) {
        this.anchor_nick_name = anchor_nick_name;
    }

    public String getAnchor_avatar_url() {
        return anchor_avatar_url;
    }

    public void setAnchor_avatar_url(String anchor_avatar_url) {
        this.anchor_avatar_url = anchor_avatar_url;
    }

    public String getGame() {
        return game;
    }

    public void setGame(String game) {
        this.game = game;
    }

    public boolean isShow_buy_dialog() {
        return show_buy_dialog;
    }

    public void setShow_buy_dialog(boolean show_buy_dialog) {
        this.show_buy_dialog = show_buy_dialog;
    }

    public String getMsg_notify_title() {
        return msg_notify_title;
    }

    public void setMsg_notify_title(String msg_notify_title) {
        this.msg_notify_title = msg_notify_title;
    }

    public String getVoice_url() {
        return voice_url;
    }

    public void setVoice_url(String voice_url) {
        this.voice_url = voice_url;
    }

    public String getKeep_float_window() {
        return keep_float_window;
    }

    public void setKeep_float_window(String keep_float_window) {
        this.keep_float_window = keep_float_window;
    }

    public String getC_toast() {
        return c_toast;
    }

    public void setC_toast(String c_toast) {
        this.c_toast = c_toast;
    }

    public boolean isTeam_notice() {
        return team_notice;
    }

    public void setTeam_notice(boolean team_notice) {
        this.team_notice = team_notice;
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public String getRoom_id() {
        return room_id;
    }

    public void setRoom_id(String room_id) {
        this.room_id = room_id;
    }

    public String getMsg_title() {
        return msg_title;
    }

    public void setMsg_title(String msg_title) {
        this.msg_title = msg_title;
    }

    public String getMsg_content() {
        return msg_content;
    }

    public void setMsg_content(String msg_content) {
        this.msg_content = msg_content;
    }

    public String getTo_user_id() {
        return to_user_id;
    }

    public void setTo_user_id(String to_user_id) {
        this.to_user_id = to_user_id;
    }

    public String getNick_name() {
        return nick_name;
    }

    public void setNick_name(String nick_name) {
        this.nick_name = nick_name;
    }

    public String getAvatar_url() {
        return avatar_url;
    }

    public void setAvatar_url(String avatar_url) {
        this.avatar_url = avatar_url;
    }

    public String getMsg_id() {
        return msg_id;
    }

    public void setMsg_id(String msg_id) {
        this.msg_id = msg_id;
    }

    public boolean isIs_friend() {
        return is_friend;
    }

    public void setIs_friend(boolean is_friend) {
        this.is_friend = is_friend;
    }

    public int getRemain_cnt() {
        return remain_cnt;
    }

    public void setRemain_cnt(int remain_cnt) {
        this.remain_cnt = remain_cnt;
    }

    public long getEnd_timestamp() {
        return end_timestamp * 1000;
    }

    public void setEnd_timestamp(long end_timestamp) {
        this.end_timestamp = end_timestamp;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getTeam_id() {
        return team_id;
    }

    public void setTeam_id(String team_id) {
        this.team_id = team_id;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public boolean isShow_recharge() {
        return show_recharge;
    }

    public void setShow_recharge(boolean show_recharge) {
        this.show_recharge = show_recharge;
    }

    public class Details{
        public String title;
        public String price_ori;
        public String price;
        public String text;
    }
}
