package com.liquid.poros.entity;

import java.io.Serializable;

/**
 * 速配请求Model
 */
public class MatchingRoomModel extends BaseBean implements Serializable {
    private static final long serialVersionUID = 4281103507390263935L;
    public boolean request_success;  //":false,
    public boolean show_recharge_hint;  //":true,
    public long seconds;
    public String text;  //":"你的金币不足，请充值"
}
