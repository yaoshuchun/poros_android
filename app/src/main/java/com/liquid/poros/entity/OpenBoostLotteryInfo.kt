package com.liquid.poros.entity

/**
 * cp房助力抽奖
 */
class OpenBoostLotteryInfo(var prize: OpenBoostLotteryInfoBean) : BaseBean()

data class OpenBoostLotteryInfoBean(var boost_lottery_num: Int? = 0,
                                    var coin_value: Int? = 0,
                                    var image: String? = null,
                                    var prize_name: String? = null,
                                    var gift_id: String? = null,
                                    var prize_position: Int? = 0,
                                    var common_count: Int? = 0,
                                    var advance_count: Int? = 0)


