package com.liquid.poros.entity

class GiftInfo : BaseBean() {
    var coins = 0
    var spend_coins = 0
    var gift_id: Int = 0
    var giftName: String = ""
    var giftNum : Int = 0
    var gift_boost_success: Boolean = false //是否助力成功
}