package com.liquid.poros.entity;

public class CoupleCardInfo {
    private int card_type;
    private String name;
    private int card_price;
    private boolean is_default;
    private int cp_time;
    private String desc;
    private String img;
    private String ori_price;
    private String tip;
    private String title;
    private int discounts;
    private int gift_box_key;
    private int gift_box_keys;
    private boolean show_cp_right;
    private String card_category;
    private String card_cnt;
    private String expire_time_str;
    private int bonus_price;
    private int cheap_price;

    public int getCheap_price() {
        return cheap_price;
    }

    public void setCheap_price(int cheap_price) {
        this.cheap_price = cheap_price;
    }

    public int getBonus_price() {
        return bonus_price;
    }

    public void setBonus_price(int bonus_price) {
        this.bonus_price = bonus_price;
    }

    public int getDiscounts() {
        return discounts;
    }

    public void setDiscounts(int discounts) {
        this.discounts = discounts;
    }

    public int getGift_box_key() {
        return gift_box_key;
    }

    public int getGift_box_keys() {
        return gift_box_keys;
    }

    public void setGift_box_keys(int gift_box_keys) {
        this.gift_box_keys = gift_box_keys;
    }

    public void setGift_box_key(int gift_box_key) {
        this.gift_box_key = gift_box_key;
    }

    public int getCard_type() {
        return card_type;
    }

    public void setCard_type(int card_type) {
        this.card_type = card_type;
    }

    public int getCard_price() {
        return card_price;
    }

    public void setCard_price(int card_price) {
        this.card_price = card_price;
    }

    public boolean isIs_default() {
        return is_default;
    }

    public void setIs_default(boolean is_default) {
        this.is_default = is_default;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCp_time() {
        return cp_time;
    }

    public void setCp_time(int cp_time) {
        this.cp_time = cp_time;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getOri_price() {
        return ori_price;
    }

    public void setOri_price(String ori_price) {
        this.ori_price = ori_price;
    }

    public String getTip() {
        return tip;
    }

    public void setTip(String tip) {
        this.tip = tip;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isShow_cp_right() {
        return show_cp_right;
    }

    public void setShow_cp_right(boolean show_cp_right) {
        this.show_cp_right = show_cp_right;
    }

    public String getCard_category() {
        return card_category;
    }

    public void setCard_category(String card_category) {
        this.card_category = card_category;
    }

    public String getCard_cnt() {
        return card_cnt;
    }

    public void setCard_cnt(String card_cnt) {
        this.card_cnt = card_cnt;
    }

    public String getExpire_time_str() {
        return expire_time_str;
    }

    public void setExpire_time_str(String expire_time_str) {
        this.expire_time_str = expire_time_str;
    }
}
