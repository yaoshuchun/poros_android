package com.liquid.poros.entity;
import java.util.List;

public class RechargeInfo {
    private int code;
    private Data data;
    private String msg;

    public void setCode(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public class Data {

        private String rest_coin;
        private List<RechargeListInfo> recharge_list;
        private List<RechargeWayInfo> recharge_way;
        private String tips;
        private String unit;
        private long cach_balance;//提现余额

        public long getCach_balance() {
            return cach_balance;
        }

        public void setCach_balance(long cach_balance) {
            this.cach_balance = cach_balance;
        }

        public String getRest_coin() {
            return rest_coin;
        }

        public void setRest_coin(String rest_coin) {
            this.rest_coin = rest_coin;
        }

        public List<RechargeListInfo> getRecharge_list() {
            return recharge_list;
        }

        public void setRecharge_list(List<RechargeListInfo> recharge_list) {
            this.recharge_list = recharge_list;
        }

        public List<RechargeWayInfo> getRecharge_way() {
            return recharge_way;
        }

        public void setRecharge_way(List<RechargeWayInfo> recharge_way) {
            this.recharge_way = recharge_way;
        }

        public String getTips() {
            return tips;
        }

        public void setTips(String tips) {
            this.tips = tips;
        }

        public String getUnit() {
            return unit;
        }

        public void setUnit(String unit) {
            this.unit = unit;
        }
    }
}
