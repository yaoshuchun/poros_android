package com.liquid.poros.entity;

import com.liquid.poros.analytics.utils.ExposeData;
import com.liquid.poros.constant.SquareStatusEnum;

import java.util.List;

public class CpListInfo extends BaseModel {
    private UserList data;

    public UserList getData() {
        return data;
    }

    public void setData(UserList data) {
        this.data = data;
    }

    public class UserList {
        private boolean show_match_window;
        private boolean video_match_show;
        private List<String> video_match_open_time;
        private String video_match_open_time_desc;
        private boolean match_room_newbie_hint;
        private List<UserItem> user_list;

        public boolean isVideo_match_show() {
            return video_match_show;
        }

        public void setVideo_match_show(boolean video_match_show) {
            this.video_match_show = video_match_show;
        }

        public boolean isShow_match_window() {
            return show_match_window;
        }

        public void setShow_match_window(boolean show_match_window) {
            this.show_match_window = show_match_window;
        }

        public String getVideo_match_open_time_desc() {
            return video_match_open_time_desc;
        }

        public void setVideo_match_open_time_desc(String video_match_open_time_desc) {
            this.video_match_open_time_desc = video_match_open_time_desc;
        }

        public List<String> getVideo_match_open_time() {
            return video_match_open_time;
        }

        public void setVideo_match_open_time(List<String> video_match_open_time) {
            this.video_match_open_time = video_match_open_time;
        }

        public boolean isMatch_room_newbie_hint() {
            return match_room_newbie_hint;
        }

        public void setMatch_room_newbie_hint(boolean match_room_newbie_hint) {
            this.match_room_newbie_hint = match_room_newbie_hint;
        }

        public List<UserItem> getUser_list() {
            return user_list;
        }

        public void setUser_list(List<UserItem> user_list) {
            this.user_list = user_list;
        }
    }

    public class UserItem extends ExposeData {
        private String user_id;
        private String nick_name;
        private String avatar_url;
        private String age;
        private int gender;
        private List<String> games;
        private List<String> tags;
        private int new_feed;

        @Override
        public String toString() {
            return "UserItem{" +
                    "user_id='" + user_id + '\'' +
                    ", nick_name='" + nick_name + '\'' +
                    ", avatar_url='" + avatar_url + '\'' +
                    ", age='" + age + '\'' +
                    ", gender=" + gender +
                    ", games=" + games +
                    ", tags=" + tags +
                    ", couple_desc='" + couple_desc + '\'' +
                    ", status_str='" + status_str + '\'' +
                    ", status=" + status +
                    ", cp_room_id='" + cp_room_id + '\'' +
                    ", online_str='" + online_str + '\'' +
                    ", photo_list=" + photo_list +
                    '}';
        }

        private String couple_desc;

        public List<String> getTags() {
            return tags;
        }

        public void setTags(List<String> tags) {
            this.tags = tags;
        }

        private String status_str;
        private int status;

        public SquareStatusEnum getStatus() {
            return SquareStatusEnum.typeOfValue(status);
        }

        public void setStatus(int status) {
            this.status = status;
        }

        private String cp_room_id;
        private String online_str;
        private String im_room_id;

        public String getIm_room_id() {
            return im_room_id;
        }

        public void setIm_room_id(String im_room_id) {
            this.im_room_id = im_room_id;
        }

        private List<RefItem> photo_list;

        public List<RefItem> getPhoto_list() {
            return photo_list;
        }

        public void setPhoto_list(List<RefItem> photo_list) {
            this.photo_list = photo_list;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getNick_name() {
            return nick_name;
        }

        public void setNick_name(String nick_name) {
            this.nick_name = nick_name;
        }

        public String getAvatar_url() {
            return avatar_url;
        }

        public void setAvatar_url(String avatar_url) {
            this.avatar_url = avatar_url;
        }

        public String getAge() {
            return age;
        }

        public void setAge(String age) {
            this.age = age;
        }

        public int getGender() {
            return gender;
        }

        public void setGender(int gender) {
            this.gender = gender;
        }

        public List<String> getGames() {
            return games;
        }

        public void setGames(List<String> games) {
            this.games = games;
        }

        public String getCouple_desc() {
            return couple_desc;
        }

        public void setCouple_desc(String couple_desc) {
            this.couple_desc = couple_desc;
        }

        public String getStatus_str() {
            return status_str;
        }

        public void setStatus_str(String status_str) {
            this.status_str = status_str;
        }

        public String getCp_room_id() {
            return cp_room_id;
        }

        public void setCp_room_id(String cp_room_id) {
            this.cp_room_id = cp_room_id;
        }

        public String getOnline_str() {
            return online_str;
        }

        public void setOnline_str(String online_str) {
            this.online_str = online_str;
        }

        public int getNew_feed() {
            return new_feed;
        }

        public void setNew_feed(int new_feed) {
            this.new_feed = new_feed;
        }
    }

    public class RefItem {
        private int type;
        private String path;

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }
    }
}
