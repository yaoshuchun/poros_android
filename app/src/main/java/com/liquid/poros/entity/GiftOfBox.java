package com.liquid.poros.entity;

import java.io.Serializable;

public class GiftOfBox extends BaseBean implements Serializable {
//    public static final int CODE_KEY_NOT_ENOUGH = 11;//钥匙不足
//    public static final int CODE_BOX_NOT_ENOUGH = 12;//礼盒不足

    public String target_user_id;
    public static final int UNKNOW = 0;//	未知
    public static final int MONEY = 1;//	现金	放入钱包
    public static final int CANDY = 2;//	糖果	可提现
    public static final int GIFT_FRAGMENT = 3;//礼物碎片可合成
    public static final int ORDINARY_GIFT = 4;//	普通礼物	可一键送出
    public static final int EXCLUSIVE_GIFT = 5;//	专属礼物	可一键送出

    public int gift_id;//liwuid
    public String title;//标题
    public String gift_image;// 奖品图片
    public String bg_image;// 奖品图片背景图片
    public String gift_name;// 奖品名称
    public String value;// 奖品价值
    public int box_gift_type;// 奖品类型，类型见：礼盒相关接口，用来区分一键送礼|收下
    public String desc;// 最下面文案
    public int gift_box_key_count;// 钥匙数
    public int gift_box_count;// 礼盒数
    public int num;
    public int bag_detail_id;
    public Effect effect;
}
