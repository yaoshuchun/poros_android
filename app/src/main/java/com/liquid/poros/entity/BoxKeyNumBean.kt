package com.liquid.poros.entity

class BoxKeyNumBean(
        var data : Data
):BaseBean(){
    inner class Data(
            var gift_box_key_popup:Boolean,
        var gift_key_count:Int,
        var gift_box_key_count_everyday:Int
    ):BaseBean()
}