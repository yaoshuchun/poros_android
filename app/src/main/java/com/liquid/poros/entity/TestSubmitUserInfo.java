package com.liquid.poros.entity;

import java.util.List;

public class TestSubmitUserInfo {
    private int code;
    private String msg;
    private UserCenterInfo.Data data;
    private UserCenterInfo.WZRYDATA wzrydata;
    private UserCenterInfo.HPJYDATA hpjydata;
    private SettingEditGameBean staticGameBean;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public UserCenterInfo.Data getData() {
        return data;
    }

    public void setData(UserCenterInfo.Data data) {
        this.data = data;
    }

    public UserCenterInfo.WZRYDATA getWzrydata() {
        return wzrydata;
    }

    public void setWzrydata(UserCenterInfo.WZRYDATA wzrydata) {
        this.wzrydata = wzrydata;
    }

    public UserCenterInfo.HPJYDATA getHpjydata() {
        return hpjydata;
    }

    public void setHpjydata(UserCenterInfo.HPJYDATA hpjydata) {
        this.hpjydata = hpjydata;
    }

    public SettingEditGameBean getStaticGameBean() {
        return staticGameBean;
    }

    public void setStaticGameBean(SettingEditGameBean staticGameBean) {
        this.staticGameBean = staticGameBean;
    }

    public static class Data {
        private SettingEditGameBean staticGameBean;
        private UserCenterInfo.Data.UserInfo.GameInfoBean.HpjyBean hpjy;
        private UserCenterInfo.Data.UserInfo.GameInfoBean.WzryBean wzry;
        private List<NormalGame> other;
        public SettingEditGameBean getStaticGameBean() {
            return staticGameBean;
        }

        public void setStaticGameBean(SettingEditGameBean staticGameBean) {
            this.staticGameBean = staticGameBean;
        }

        public UserCenterInfo.Data.UserInfo.GameInfoBean.HpjyBean getHpjy() {
            return hpjy;
        }

        public void setHpjy(UserCenterInfo.Data.UserInfo.GameInfoBean.HpjyBean hpjy) {
            this.hpjy = hpjy;
        }

        public UserCenterInfo.Data.UserInfo.GameInfoBean.WzryBean getWzry() {
            return wzry;
        }

        public void setWzry(UserCenterInfo.Data.UserInfo.GameInfoBean.WzryBean wzry) {
            this.wzry = wzry;
        }

        public List<NormalGame> getOther() {
            return other;
        }

        public void setOther(List<NormalGame> other) {
            this.other = other;
        }
    }
}
