package com.liquid.poros.entity;

import java.util.List;

public class GuardRankerInfo {
    private String avatar_url;
    private float coins;
    private List<String> gift_list;
    private String nick_name;

    public String getAvatar_url() {
        return avatar_url;
    }

    public void setAvatar_url(String avatar_url) {
        this.avatar_url = avatar_url;
    }

    public float getCoins() {
        return coins;
    }

    public void setCoins(float coins) {
        this.coins = coins;
    }

    public List<String> getGift_list() {
        return gift_list;
    }

    public void setGift_list(List<String> gift_list) {
        this.gift_list = gift_list;
    }

    public String getNick_name() {
        return nick_name;
    }

    public void setNick_name(String nick_name) {
        this.nick_name = nick_name;
    }
}
