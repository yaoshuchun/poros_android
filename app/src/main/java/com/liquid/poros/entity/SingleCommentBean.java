package com.liquid.poros.entity;

import com.liquid.poros.analytics.utils.ExposeData;

import java.util.List;

public class SingleCommentBean extends ExposeData {
    public String feed_id;
    public String user_id;
    public String nick_name;
    public String avatar_url;
    public String text;
    public List<String> images;
    public String voice;
    public int voice_time;
    public String create_time;
    public int comment_count;
    public int like_count;
    public boolean liked;
    public int mAudioTimer;
}
