package com.liquid.poros.entity;

import java.io.Serializable;

public class RoomLiveInfo extends BaseBean {
    private String anchor_id;
    private String boy_id;
    private String girl_id;
    private String hint;
    private boolean is_online;
    private String room_id;
    private int room_type;
    private String im_room_id;

    public String getAnchor_id() {
        return anchor_id;
    }

    public void setAnchor_id(String anchor_id) {
        this.anchor_id = anchor_id;
    }

    public String getBoy_id() {
        return boy_id;
    }

    public void setBoy_id(String boy_id) {
        this.boy_id = boy_id;
    }

    public String getGirl_id() {
        return girl_id;
    }

    public void setGirl_id(String girl_id) {
        this.girl_id = girl_id;
    }

    public String getHint() {
        return hint;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }

    public boolean isIs_online() {
        return is_online;
    }

    public void setIs_online(boolean is_online) {
        this.is_online = is_online;
    }

    public String getRoom_id() {
        return room_id;
    }

    public void setRoom_id(String room_id) {
        this.room_id = room_id;
    }

    public int getRoom_type() {
        return room_type;
    }

    public void setRoom_type(int room_type) {
        this.room_type = room_type;
    }

    public String getIm_room_id() {
        return im_room_id;
    }

    public void setIm_room_id(String im_room_id) {
        this.im_room_id = im_room_id;
    }
}
