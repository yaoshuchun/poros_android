package com.liquid.poros.entity

class PrivateRoomHeartBeatInfo : BaseBean() {
    var coin: String? = null
    var popup: PopupData? = null

    class PopupData {
        var content: String? = null
        var desc: String? = null
        var countdown = 0
    }
}