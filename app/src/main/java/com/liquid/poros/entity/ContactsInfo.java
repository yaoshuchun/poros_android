package com.liquid.poros.entity;

import java.util.List;

/**
 * Created by sundroid on 24/07/2019.
 */

public class ContactsInfo {
    private int code;
    private List<Contacts> data;
    private String msg;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public List<Contacts> getData() {
        return data;
    }

    public void setData(List<Contacts> data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }


}
