package com.liquid.poros.entity;

/**
 * 送礼成功 - 礼物条目
 */
public class GiftItemInfo extends BaseModel {
    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        private int coins;
        private boolean gift_boost_success;
        private GiftItemBean gift_item;

        public int getCoins() {
            return coins;
        }

        public void setCoins(int coins) {
            this.coins = coins;
        }

        public boolean isGift_boost_success() {
            return gift_boost_success;
        }

        public void setGift_boost_success(boolean gift_boost_success) {
            this.gift_boost_success = gift_boost_success;
        }

        public GiftItemBean getGift_item() {
            return gift_item;
        }

        public void setGift_item(GiftItemBean gift_item) {
            this.gift_item = gift_item;
        }

        public static class GiftItemBean {
            private int add_score;
            private EffectBean effect;
            private int effect_type;
            private String icon;
            private int id;
            private String name;
            private int num;
            private int price;
            private int type;

            public int getAdd_score() {
                return add_score;
            }

            public void setAdd_score(int add_score) {
                this.add_score = add_score;
            }

            public EffectBean getEffect() {
                return effect;
            }

            public void setEffect(EffectBean effect) {
                this.effect = effect;
            }

            public int getEffect_type() {
                return effect_type;
            }

            public void setEffect_type(int effect_type) {
                this.effect_type = effect_type;
            }

            public String getIcon() {
                return icon;
            }

            public void setIcon(String icon) {
                this.icon = icon;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public int getNum() {
                return num;
            }

            public void setNum(int num) {
                this.num = num;
            }

            public int getPrice() {
                return price;
            }

            public void setPrice(int price) {
                this.price = price;
            }

            public int getType() {
                return type;
            }

            public void setType(int type) {
                this.type = type;
            }

            public static class EffectBean {
                private String audio;

                public String getAudio() {
                    return audio;
                }

                public void setAudio(String audio) {
                    this.audio = audio;
                }
            }
        }
    }
}
