package com.liquid.poros.entity

/**
 * cp房助力抽奖详情
 */
class BoostLotteryInfo(var lotterys: BoostLotteryInfoBean) : BaseBean()

data class BoostLotteryInfoBean(var common_count: Int? = 0,
                                var advance_count: Int? = 0,
                                var advance: List<BoostLotteryListBean>? = null, var common: List<BoostLotteryListBean>? = null,var boost_reward_msg_list:List<String>)

data class BoostLotteryListBean(var boost_lottery_num: Int? = 0,
                                var coin_value: Int? = 0,
                                var image: String? = null,
                                var prize_name: String? = null)

