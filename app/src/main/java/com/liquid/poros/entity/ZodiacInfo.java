package com.liquid.poros.entity;

public class ZodiacInfo {
    private String zodiacName;
    private int zodiacPic;
    private int type;
    private boolean select = false;

    public String getZodiacName() {
        return zodiacName;
    }

    public void setZodiacName(String zodiacName) {
        this.zodiacName = zodiacName;
    }

    public int getZodiacPic() {
        return zodiacPic;
    }

    public void setZodiacPic(int zodiacPic) {
        this.zodiacPic = zodiacPic;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public boolean isSelect() {
        return select;
    }

    public void setSelect(boolean select) {
        this.select = select;
    }
}
