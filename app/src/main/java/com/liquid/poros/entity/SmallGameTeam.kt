package com.liquid.poros.entity

import com.alibaba.android.arouter.facade.annotation.Autowired
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class SmallGameTeam : BaseBean(),Serializable {
    val im_room_id: String? = "273200899"
    val other_avatar_url: String? = null//对方头像
    val other_nick_name: String? = null//对方昵称
    val self_avatar_url: String? = null//自己头像
    val self_nick_name: String? = null//自己昵称
    val game_desc: String? = null//游戏描述
    val game_icon_url: String? = null//游戏图标
    val game_prize_pool_url: String? = null//游戏奖池h5
    val watting_time: Long = 10
    val ready_time: Long = 10
    val refuse_time: Long = 20
    
    companion object{
        const val ROLE_LAUNCHER = 0 //游戏发起人
        const val ROLE_RECEIVER = 1 //游戏接收者
    }

   
    var role = ROLE_LAUNCHER
}