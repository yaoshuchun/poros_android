package com.liquid.poros.entity
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.liquid.library.retrofitx.RetrofitX
import com.liquid.library.retrofitx.utils.LogUtils
import com.liquid.poros.http.ApiUrl
import com.liquid.poros.http.observer.ObjectObserver
import com.liquid.poros.http.utils.getPoros
import com.liquid.poros.http.utils.postPoros
import com.liquid.poros.utils.AccountUtil

/**
 * 1v1视频列表页面model
 */
class SolotVideoModel: ViewModel() {

    val success: MutableLiveData<SoloVideoInfoList> by lazy {
        MutableLiveData<SoloVideoInfoList>()
    }

    val fail : MutableLiveData<SoloVideoInfoList> by lazy {
        MutableLiveData<SoloVideoInfoList>()
    }

    val soloCallSucess: MutableLiveData<SoloCallInfo> by lazy {
        MutableLiveData<SoloCallInfo>()
    }

    val soloCallFail : MutableLiveData<SoloCallInfo> by lazy {
        MutableLiveData<SoloCallInfo>()
    }
    val requestVideoMatchMicLiveDataSuccess: MutableLiveData<VideoMatchInfo> by lazy { MutableLiveData<VideoMatchInfo>() } //男用户上麦成功
    val requestVideoMatchMicLiveDataFail: MutableLiveData<VideoMatchInfo> by lazy { MutableLiveData<VideoMatchInfo>() } //男用户上麦失败
    fun getRoomList() {
        RetrofitX.url(ApiUrl.SOLO_VIDEO_LIST_REQUIRE).
            param("token",AccountUtil.getInstance().account_token).
            getPoros().
            subscribe(object : ObjectObserver<SoloVideoInfoList>() {
                override fun success(result: SoloVideoInfoList) {
                    success.postValue(result)
                }

                override fun failed(result: SoloVideoInfoList) {
                    fail.postValue(result)
                }

        })
    }

    /**
     * 1v1视频call
     */
    fun getSoloCall (){
        RetrofitX.url(ApiUrl.SOLO_VIDEO_CALL).
                param("token",AccountUtil.getInstance().account_token).
                postJSON().
                subscribe(object : ObjectObserver<SoloCallInfo>() {
                    override fun success(result: SoloCallInfo) {
                        soloCallSucess.postValue(result)
                    }

                    override fun failed(result: SoloCallInfo) {
                        soloCallFail.postValue(result)
                    }

                })
    }

    /**
     * 请求上麦
     */
    fun requestVideoMatchMic(user_id: String?,nick_name: String?, avatar_url:String?,image_url: String?) {
        LogUtils.d("请求时间----"+System.currentTimeMillis())
        RetrofitX.url(ApiUrl.REQUEST_VIDEO_MATCH_MIC)
                .param("user_id", user_id)
                .postPoros().subscribe(object : ObjectObserver<VideoMatchInfo>() {
                    override fun success(result: VideoMatchInfo) {
                        var liveInfo = result.live_info
                        liveInfo?.image_url = image_url
                        liveInfo?.image_url = image_url
                        result.live_info = liveInfo
                        requestVideoMatchMicLiveDataSuccess.postValue(result)
                    }

                    override fun failed(result: VideoMatchInfo) {
                        if(result.code == 19001 || result.code == 19017){
                            var liveInfo = VideoMatchInfo.LiveFemaleGuestInfo(user_id,avatar_url,nick_name,0,0, emptyList(),image_url)
                            result.live_info = liveInfo
                        }
                        requestVideoMatchMicLiveDataFail.postValue(result)
                    }

                })
    }

}