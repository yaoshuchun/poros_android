package com.liquid.poros.entity;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 语音速配信息设置
 */

public class AudioMatchInfo {
    private int code;
    private Data data;
    private String msg;
    private GAMEDATA gameData;
    private AudioMatchEditGameBean staticGameBean;

    public GAMEDATA getGameData() {
        return gameData;
    }

    public void setGameData(GAMEDATA gameData) {
        this.gameData = gameData;
    }

    public AudioMatchEditGameBean getStaticGameBean() {
        return staticGameBean;
    }

    public void setStaticGameBean(AudioMatchEditGameBean staticGameBean) {
        this.staticGameBean = staticGameBean;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public static class Data {
        private String button;
        private String desc;
        private boolean free_tag;
        private String price_hint;
        private String text;
        private String title;
        private UserGameInfo user_game_info;
        private boolean match_room_newbie_hint;

        public String getButton() {
            return button;
        }

        public void setButton(String button) {
            this.button = button;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public boolean isFree_tag() {
            return free_tag;
        }

        public void setFree_tag(boolean free_tag) {
            this.free_tag = free_tag;
        }

        public String getPrice_hint() {
            return price_hint;
        }

        public void setPrice_hint(String price_hint) {
            this.price_hint = price_hint;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public UserGameInfo getUser_game_info() {
            return user_game_info;
        }

        public void setUser_game_info(UserGameInfo user_game_info) {
            this.user_game_info = user_game_info;
        }

        public boolean isMatch_room_newbie_hint() {
            return match_room_newbie_hint;
        }

        public void setMatch_room_newbie_hint(boolean match_room_newbie_hint) {
            this.match_room_newbie_hint = match_room_newbie_hint;
        }

        public static class GameInfo {
            private RankBean rank_choice;
            private ZoneBean zone_choice;

            public RankBean getRank_choice() {
                return rank_choice;
            }

            public void setRank_choice(RankBean rank_choice) {
                this.rank_choice = rank_choice;
            }

            public ZoneBean getZone_choice() {
                return zone_choice;
            }

            public void setZone_choice(ZoneBean zone_choice) {
                this.zone_choice = zone_choice;
            }

            public static class RankBean {
                /**
                 * 0 : 不限
                 * 1 : 倔强青铜
                 * 2 : 秩序白银
                 * 3 : 荣耀黄金
                 * 4 : 尊贵铂金
                 * 5 : 永恒钻石
                 * 6 : 至尊星耀
                 * 7 : 最强王者
                 * 8 : 荣耀王者
                 */

                @SerializedName("0")
                private String _$0;
                @SerializedName("1")
                private String _$1;
                @SerializedName("2")
                private String _$2;
                @SerializedName("3")
                private String _$3;
                @SerializedName("4")
                private String _$4;
                @SerializedName("5")
                private String _$5;
                @SerializedName("6")
                private String _$6;
                @SerializedName("7")
                private String _$7;
                @SerializedName("8")
                private String _$8;

                public String get_$0() {
                    return _$0;
                }

                public void set_$0(String _$0) {
                    this._$0 = _$0;
                }

                public String get_$1() {
                    return _$1;
                }

                public void set_$1(String _$1) {
                    this._$1 = _$1;
                }

                public String get_$2() {
                    return _$2;
                }

                public void set_$2(String _$2) {
                    this._$2 = _$2;
                }

                public String get_$3() {
                    return _$3;
                }

                public void set_$3(String _$3) {
                    this._$3 = _$3;
                }

                public String get_$4() {
                    return _$4;
                }

                public void set_$4(String _$4) {
                    this._$4 = _$4;
                }

                public String get_$5() {
                    return _$5;
                }

                public void set_$5(String _$5) {
                    this._$5 = _$5;
                }

                public String get_$6() {
                    return _$6;
                }

                public void set_$6(String _$6) {
                    this._$6 = _$6;
                }

                public String get_$7() {
                    return _$7;
                }

                public void set_$7(String _$7) {
                    this._$7 = _$7;
                }

                public String get_$8() {
                    return _$8;
                }

                public void set_$8(String _$8) {
                    this._$8 = _$8;
                }
            }

            public static class ZoneBean {
                /**
                 * 1 : QQ
                 * 2 : 微信
                 */

                @SerializedName("1")
                private String _$1;
                @SerializedName("2")
                private String _$2;

                public String get_$1() {
                    return _$1;
                }

                public void set_$1(String _$1) {
                    this._$1 = _$1;
                }

                public String get_$2() {
                    return _$2;
                }

                public void set_$2(String _$2) {
                    this._$2 = _$2;
                }
            }


        }

        public class UserGameInfo {
            private int rank;
            private int zone;

            public int getRank() {
                return rank;
            }

            public void setRank(int rank) {
                this.rank = rank;
            }

            public int getZone() {
                return zone;
            }

            public void setZone(int zone) {
                this.zone = zone;
            }
        }
    }

    public static class GAMEDATA {
        private ArrayList<Map<String, String>> platform_list;
        private ArrayList<Map<String, String>> rank_list;
        private ArrayList<Map<String, String>> area_list;
        private String icon;

        public ArrayList<Map<String, String>> getPlatform_list() {
            return platform_list;
        }

        public void setPlatform_list(ArrayList<Map<String, String>> platform_list) {
            this.platform_list = platform_list;
        }

        public ArrayList<Map<String, String>> getRank_list() {
            return rank_list;
        }

        public void setRank_list(ArrayList<Map<String, String>> rank_list) {
            this.rank_list = rank_list;
        }

        public ArrayList<Map<String, String>> getArea_list() {
            return area_list;
        }

        public void setArea_list(ArrayList<Map<String, String>> area_list) {
            this.area_list = area_list;
        }

        public String getIcon() {
            return icon;
        }

        public void setIcon(String icon) {
            this.icon = icon;
        }
    }
}
