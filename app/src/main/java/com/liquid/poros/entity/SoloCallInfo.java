package com.liquid.poros.entity;

/**
 * 1v1视频列表页面: bean类
 */
public class SoloCallInfo extends BaseBean{


    /**
     * avatar_url : http://static.diffusenetwork.com/avatar/69652151_1616654661.png
     * image_1v1_url :
     * image_url :
     * live_id : 1657
     * mic_id : 60b851b215703a05d13730f9
     * nick_name : 哈哈
     * ttl : 15
     * user_id : 69652151
     * video_url : https://liquid-poros.oss-cn-beijing.aliyuncs.com/videos/04.mp4
     */

    private String avatar_url;
    private String image_1v1_url;
    private String image_url;
    private String live_id;
    private String mic_id;
    private String nick_name;
    private int ttl;
    private String user_id;
    private String video_url;

    public String getAvatar_url() {
        return avatar_url;
    }

    public void setAvatar_url(String avatar_url) {
        this.avatar_url = avatar_url;
    }

    public String getImage_1v1_url() {
        return image_1v1_url;
    }

    public void setImage_1v1_url(String image_1v1_url) {
        this.image_1v1_url = image_1v1_url;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getLive_id() {
        return live_id;
    }

    public void setLive_id(String live_id) {
        this.live_id = live_id;
    }

    public String getMic_id() {
        return mic_id;
    }

    public void setMic_id(String mic_id) {
        this.mic_id = mic_id;
    }

    public String getNick_name() {
        return nick_name;
    }

    public void setNick_name(String nick_name) {
        this.nick_name = nick_name;
    }

    public int getTtl() {
        return ttl;
    }

    public void setTtl(int ttl) {
        this.ttl = ttl;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getVideo_url() {
        return video_url;
    }

    public void setVideo_url(String video_url) {
        this.video_url = video_url;
    }
}
