package com.liquid.poros.entity;

import java.util.List;

/**
 * APP基础配置
 * 2020.01.07
 */
public class AppBaseConfigBean extends BaseBean {

    public String ad_csj_appid;
    public String ad_csj_codeid;
    public String bind_master_url;
    public String cp_right_content;
    public String cp_right_title;
    public int cp_voice_chat_price;
    public String feedback_group;
    public Boolean hide_makeup;
    public Boolean is_forbid_user;
    public int server_timestamp;
    public String share_group_url;
    public Boolean show_age_level_continue;
    public Boolean show_cartoon;
    public int show_feed;
    public int show_square_feed;
    public int status;
    public String user_client_url;
    public String[] cp_room_anchor_tips;
    public String[] cp_room_mic_user_tips;
    public String[] cp_room_watch_user_tips;
    public String[] games;
    public String[] live_tips;
    public String[] private_room_tips;
    public List<RechargeWayInfo> recharge_way;
    public String[] team_leader_tips;
    public String[] team_user_tips;
    public String[] together_room_anchor_tips;
    public String[] together_room_other_tips;
    public boolean is_sounds_vip;
    public int buy_vip_money;
    public String buy_vip_desc;
    public int buy_vip_time_limit;
    public long heartbeat_interval;
    public boolean using_new_live_room;
    public int get_gift_box;
    public int buy_vip_get_coin;

    public int getBuy_vip_get_coin() {
        return buy_vip_get_coin;
    }

    public void setBuy_vip_get_coin(int buy_vip_get_coin) {
        this.buy_vip_get_coin = buy_vip_get_coin;
    }

    public int getGet_gift_box() {
        return get_gift_box;
    }

    public void setGet_gift_box(int get_gift_box) {
        this.get_gift_box = get_gift_box;
    }

    public boolean isUsing_new_live_room() {
        return using_new_live_room;
    }

    public int getBuy_vip_time_limit() {
        return buy_vip_time_limit;
    }

    public void setBuy_vip_time_limit(int buy_vip_time_limit) {
        this.buy_vip_time_limit = buy_vip_time_limit;
    }

    public String getBuy_vip_desc() {
        return buy_vip_desc;
    }

    public void setBuy_vip_desc(String buy_vip_desc) {
        this.buy_vip_desc = buy_vip_desc;
    }

    public int getBuy_vip_money() {
        return buy_vip_money;
    }

    public void setBuy_vip_money(int buy_vip_money) {
        this.buy_vip_money = buy_vip_money;
    }

    public boolean isIs_sounds_vip() {
        return is_sounds_vip;
    }

    public void setIs_sounds_vip(boolean is_sounds_vip) {
        this.is_sounds_vip = is_sounds_vip;
    }

    public String getAd_csj_appid() {
        return ad_csj_appid;
    }

    public void setAd_csj_appid(String ad_csj_appid) {
        this.ad_csj_appid = ad_csj_appid;
    }

    public String getAd_csj_codeid() {
        return ad_csj_codeid;
    }

    public void setAd_csj_codeid(String ad_csj_codeid) {
        this.ad_csj_codeid = ad_csj_codeid;
    }

    public String getBind_master_url() {
        return bind_master_url;
    }

    public void setBind_master_url(String bind_master_url) {
        this.bind_master_url = bind_master_url;
    }

    public String getCp_right_content() {
        return cp_right_content;
    }

    public void setCp_right_content(String cp_right_content) {
        this.cp_right_content = cp_right_content;
    }

    public String getCp_right_title() {
        return cp_right_title;
    }

    public void setCp_right_title(String cp_right_title) {
        this.cp_right_title = cp_right_title;
    }

    public int getCp_voice_chat_price() {
        return cp_voice_chat_price;
    }

    public void setCp_voice_chat_price(int cp_voice_chat_price) {
        this.cp_voice_chat_price = cp_voice_chat_price;
    }

    public String getFeedback_group() {
        return feedback_group;
    }

    public void setFeedback_group(String feedback_group) {
        this.feedback_group = feedback_group;
    }

    public Boolean getHide_makeup() {
        return hide_makeup;
    }

    public void setHide_makeup(Boolean hide_makeup) {
        this.hide_makeup = hide_makeup;
    }

    public Boolean getIs_forbid_user() {
        return is_forbid_user;
    }

    public void setIs_forbid_user(Boolean is_forbid_user) {
        this.is_forbid_user = is_forbid_user;
    }

    public int getServer_timestamp() {
        return server_timestamp;
    }

    public void setServer_timestamp(int server_timestamp) {
        this.server_timestamp = server_timestamp;
    }

    public String getShare_group_url() {
        return share_group_url;
    }

    public void setShare_group_url(String share_group_url) {
        this.share_group_url = share_group_url;
    }

    public Boolean getShow_age_level_continue() {
        return show_age_level_continue;
    }

    public void setShow_age_level_continue(Boolean show_age_level_continue) {
        this.show_age_level_continue = show_age_level_continue;
    }

    public Boolean getShow_cartoon() {
        return show_cartoon;
    }

    public void setShow_cartoon(Boolean show_cartoon) {
        this.show_cartoon = show_cartoon;
    }

    public int getShow_feed() {
        return show_feed;
    }

    public void setShow_feed(int show_feed) {
        this.show_feed = show_feed;
    }

    public int getShow_square_feed() {
        return show_square_feed;
    }

    public void setShow_square_feed(int show_square_feed) {
        this.show_square_feed = show_square_feed;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getUser_client_url() {
        return user_client_url;
    }

    public void setUser_client_url(String user_client_url) {
        this.user_client_url = user_client_url;
    }

    public String[] getCp_room_anchor_tips() {
        return cp_room_anchor_tips;
    }

    public void setCp_room_anchor_tips(String[] cp_room_anchor_tips) {
        this.cp_room_anchor_tips = cp_room_anchor_tips;
    }

    public String[] getCp_room_mic_user_tips() {
        return cp_room_mic_user_tips;
    }

    public void setCp_room_mic_user_tips(String[] cp_room_mic_user_tips) {
        this.cp_room_mic_user_tips = cp_room_mic_user_tips;
    }

    public String[] getCp_room_watch_user_tips() {
        return cp_room_watch_user_tips;
    }

    public void setCp_room_watch_user_tips(String[] cp_room_watch_user_tips) {
        this.cp_room_watch_user_tips = cp_room_watch_user_tips;
    }

    public String[] getGames() {
        return games;
    }

    public void setGames(String[] games) {
        this.games = games;
    }

    public String[] getLive_tips() {
        return live_tips;
    }

    public void setLive_tips(String[] live_tips) {
        this.live_tips = live_tips;
    }

    public String[] getPrivate_room_tips() {
        return private_room_tips;
    }

    public void setPrivate_room_tips(String[] private_room_tips) {
        this.private_room_tips = private_room_tips;
    }

    public List<RechargeWayInfo> getRecharge_way() {
        return recharge_way;
    }

    public void setRecharge_way(List<RechargeWayInfo> recharge_way) {
        this.recharge_way = recharge_way;
    }

    public String[] getTeam_leader_tips() {
        return team_leader_tips;
    }

    public void setTeam_leader_tips(String[] team_leader_tips) {
        this.team_leader_tips = team_leader_tips;
    }

    public String[] getTeam_user_tips() {
        return team_user_tips;
    }

    public void setTeam_user_tips(String[] team_user_tips) {
        this.team_user_tips = team_user_tips;
    }

    public String[] getTogether_room_anchor_tips() {
        return together_room_anchor_tips;
    }

    public void setTogether_room_anchor_tips(String[] together_room_anchor_tips) {
        this.together_room_anchor_tips = together_room_anchor_tips;
    }

    public String[] getTogether_room_other_tips() {
        return together_room_other_tips;
    }

    public void setTogether_room_other_tips(String[] together_room_other_tips) {
        this.together_room_other_tips = together_room_other_tips;
    }
}
