package com.liquid.poros.entity;

import java.util.List;

public class AppConfig extends BaseModel {


    private Data data;


    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {
        private String share_group_url;
        private String feedback_group;
        private String[] team_leader_tips;
        private String[] team_user_tips;
        private String[] live_tips;
        private String[] together_room_other_tips;
        private long server_timestamp;
        private int cp_voice_chat_price = 1;

        public int getCp_voice_chat_price() {
            return cp_voice_chat_price;
        }

        public void setCp_voice_chat_price(int cp_voice_chat_price) {
            this.cp_voice_chat_price = cp_voice_chat_price;
        }

        public long getServer_timestamp() {
            return server_timestamp * 1000;
        }

        public void setServer_timestamp(long server_timestamp) {
            this.server_timestamp = server_timestamp;
        }

        private String[] cp_room_anchor_tips;
        private String[] cp_room_watch_user_tips;
        private boolean hide_makeup = false;
        private boolean is_forbid_user = false;
        private String[] games;
        private String cp_right_content;
        private String cp_right_title;
        private String[] private_room_tips;
        private int show_feed;
        private int show_square_feed;
        private String ad_csj_appid;
        private String ad_csj_codeid;
        private List<RechargeWayInfo> recharge_way;
        private boolean show_cartoon;
        private boolean is_sounds_vip;

        public boolean isIs_sounds_vip() {
            return is_sounds_vip;
        }

        public void setIs_sounds_vip(boolean is_sounds_vip) {
            this.is_sounds_vip = is_sounds_vip;
        }

        public boolean isShow_cartoon() {
            return show_cartoon;
        }

        public void setShow_cartoon(boolean show_cartoon) {
            this.show_cartoon = show_cartoon;
        }

        public List<RechargeWayInfo> getRecharge_way() {
            return recharge_way;
        }

        public void setRecharge_way(List<RechargeWayInfo> recharge_way) {
            this.recharge_way = recharge_way;
        }

        public int getShow_square_feed() {
            return show_square_feed;
        }

        public void setShow_square_feed(int show_square_feed) {
            this.show_square_feed = show_square_feed;
        }

        public String getAd_csj_appid() {
            return ad_csj_appid;
        }

        public void setAd_csj_appid(String ad_csj_appid) {
            this.ad_csj_appid = ad_csj_appid;
        }

        public String getAd_csj_codeid() {
            return ad_csj_codeid;
        }

        public void setAd_csj_codeid(String ad_csj_codeid) {
            this.ad_csj_codeid = ad_csj_codeid;
        }

        public String getCp_right_content() {
            return cp_right_content;
        }

        public void setCp_right_content(String cp_right_content) {
            this.cp_right_content = cp_right_content;
        }

        public boolean isIs_forbid_user() {
            return is_forbid_user;
        }

        public void setIs_forbid_user(boolean is_forbid_user) {
            this.is_forbid_user = is_forbid_user;
        }

        public String getCp_right_title() {
            return cp_right_title;
        }

        public void setCp_right_title(String cp_right_title) {
            this.cp_right_title = cp_right_title;
        }

        public boolean isHide_makeup() {
            return hide_makeup;
        }

        public void setHide_makeup(boolean hide_makeup) {
            this.hide_makeup = hide_makeup;
        }

        public String[] getCp_room_anchor_tips() {
            return cp_room_anchor_tips;
        }

        public void setCp_room_anchor_tips(String[] cp_room_anchor_tips) {
            this.cp_room_anchor_tips = cp_room_anchor_tips;
        }

        public String[] getCp_room_watch_user_tips() {
            return cp_room_watch_user_tips;
        }

        public void setCp_room_watch_user_tips(String[] cp_room_watch_user_tips) {
            this.cp_room_watch_user_tips = cp_room_watch_user_tips;
        }

        public String[] getCp_room_mic_user_tips() {
            return cp_room_mic_user_tips;
        }

        public void setCp_room_mic_user_tips(String[] cp_room_mic_user_tips) {
            this.cp_room_mic_user_tips = cp_room_mic_user_tips;
        }

        private String[] cp_room_mic_user_tips;
        private boolean show_age_level_continue;//是否显示跳过  true 显示

        public String[] getTogether_room_other_tips() {
            return together_room_other_tips;
        }

        public void setTogether_room_other_tips(String[] together_room_other_tips) {
            this.together_room_other_tips = together_room_other_tips;
        }

        public String[] getTogether_room_anchor_tips() {
            return together_room_anchor_tips;
        }

        public void setTogether_room_anchor_tips(String[] together_room_anchor_tips) {
            this.together_room_anchor_tips = together_room_anchor_tips;
        }

        private String[] together_room_anchor_tips;


        public String[] getLive_tips() {
            return live_tips;
        }

        public void setLive_tips(String[] live_tips) {
            this.live_tips = live_tips;
        }

        public String[] getTeam_leader_tips() {
            return team_leader_tips;
        }

        public void setTeam_leader_tips(String[] team_leader_tips) {
            this.team_leader_tips = team_leader_tips;
        }

        public String[] getTeam_user_tips() {
            return team_user_tips;
        }

        public void setTeam_user_tips(String[] team_user_tips) {
            this.team_user_tips = team_user_tips;
        }

        public String getFeedback_group() {
            return feedback_group;
        }

        public void setFeedback_group(String feedback_group) {
            this.feedback_group = feedback_group;
        }

        public String getShare_group_url() {
            return share_group_url;
        }

        public void setShare_group_url(String share_group_url) {
            this.share_group_url = share_group_url;
        }

        public boolean isShow_age_level_continue() {
            return show_age_level_continue;
        }

        public void setShow_age_level_continue(boolean show_age_level_continue) {
            this.show_age_level_continue = show_age_level_continue;
        }

        public String[] getGames() {
            return games;
        }

        public void setGames(String[] games) {
            this.games = games;
        }

        public String[] getPrivate_room_tips() {
            return private_room_tips;
        }

        public void setPrivate_room_tips(String[] private_room_tips) {
            this.private_room_tips = private_room_tips;
        }

        public int getShow_feed() {
            return show_feed;
        }

        public void setShow_feed(int show_feed) {
            this.show_feed = show_feed;
        }
    }

}
