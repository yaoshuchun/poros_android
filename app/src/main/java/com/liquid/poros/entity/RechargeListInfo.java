package com.liquid.poros.entity;

public class RechargeListInfo {
    private long coin;
    private long money;
    private String type;
    private boolean is_default;
    private long ori_money;
    private String hint;
    private String tag;
    public long getCoin() {
        return coin;
    }

    public void setCoin(long coin) {
        this.coin = coin;
    }

    public long getMoney() {
        return money;
    }

    public void setMoney(long money) {
        this.money = money;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isIs_default() {
        return is_default;
    }

    public void setIs_default(boolean is_default) {
        this.is_default = is_default;
    }

    public long getOri_money() {
        return ori_money;
    }

    public void setOri_money(long ori_money) {
        this.ori_money = ori_money;
    }

    public String getHint() {
        return hint;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
}
