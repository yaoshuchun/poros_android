package com.liquid.poros.entity;

import com.liquid.poros.constant.GiftTypeEnum;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sundroid on 26/03/2019.
 */

public class MsgUserModel extends BaseModel{

    private UserInfo data;

    public UserInfo getData() {
        return data;
    }

    public void setData(UserInfo data) {
        this.data = data;
    }

    public class UserInfo extends BaseBean{
        private String avatar_url;
        private boolean is_friend;
        private boolean is_leader;//是否是领队身份
        private String nick_name;
        private String avatar_border;
        private boolean is_block_by_other;
        private boolean is_block_other;
        private boolean show_couple;
        private String couple_score;
        private String couple_desc;
        private boolean is_cp_end;
        private int cp_status;
        private long voice_end_time;
        private TeamInfo team_info;
        private List<String> msg_templates;
        private boolean is_online;
        private String status_str;
        private boolean show_team_time_lack;
        private String team_time_lack_content;
        private ArrayList<GiftBean> gifts;
        private ArrayList<GiftNums> gift_nums;
        private boolean is_mic_minute_cost;
        private String mic_price;
        private boolean highlight_mic_btn;
        private int cp_score_limit;
        private String cp_limit_desc;
        private boolean has_click_cp_hint;
        private boolean has_click_team_hint;
        private String cp_right_title;
        private String cp_right_content;
        private int user_coins;
        private int recharge_tips_coin;
        private String common_game_hint;
        private String common_games_str;
        public RankInfo rank_info;
        public String im_video_token;
        public String couple_room_id;
        public String couple_im_room_id;
        private boolean is_in_private_room;
        public P2PRankUpgradeAwardInfo raise_group_info;
        private CartoonGuide cartoon_guide_popup;
        private boolean show_cartoon;
        private long server_timestamp;
        private boolean is_in_pk_room;
        private int gift_box_key_count;
        private boolean gift_box_key_test; // 男嘉宾进入私聊页面 是否是实验组，礼盒分组
        private GiftBox gift_box;//礼盒相关 不为空显示礼盒入口
        private boolean send_cupid_system_msg;
        private int status;
        //盲盒h5介绍
        private String blind_box_h5;
        //盲盒分桶控制,true是实验组
        private boolean show_blind_box;
        private TempFriendInfo temp_friend_info;

        public TempFriendInfo getTemp_friend_info() {
            return temp_friend_info;
        }

        public void setTemp_friend_info(TempFriendInfo temp_friend_info) {
            this.temp_friend_info = temp_friend_info;
        }

        public String getBlind_box_h5() {
            return blind_box_h5;
        }

        public void setBlind_box_h5(String blind_box_h5) {
            this.blind_box_h5 = blind_box_h5;
        }

        public boolean isShow_blind_box() {
            return show_blind_box;
        }

        public void setShow_blind_box(boolean show_blind_box) {
            this.show_blind_box = show_blind_box;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public boolean isSend_cupid_system_msg() {
            return send_cupid_system_msg;
        }

        public void setSend_cupid_system_msg(boolean send_cupid_system_msg) {
            this.send_cupid_system_msg = send_cupid_system_msg;
        }

        public GiftBox getGift_box() {
            return gift_box;
        }

        public void setGift_box(GiftBox gift_box) {
            this.gift_box = gift_box;
        }

        public boolean isIs_in_pk_room() {
            return is_in_pk_room;
        }

        public void setIs_in_pk_room(boolean is_in_pk_room) {
            this.is_in_pk_room = is_in_pk_room;
        }

        public boolean isShow_cartoon() {
            return show_cartoon;
        }

        public void setShow_cartoon(boolean show_cartoon) {
            this.show_cartoon = show_cartoon;
        }

        public CartoonGuide getCartoon_guide_popup() {
            return cartoon_guide_popup;
        }

        public void setCartoon_guide_popup(CartoonGuide cartoon_guide_popup) {
            this.cartoon_guide_popup = cartoon_guide_popup;
        }

        public String getCommon_game_hint() {
            return common_game_hint;
        }

        public void setCommon_game_hint(String common_game_hint) {
            this.common_game_hint = common_game_hint;
        }

        public String getCommon_games_str() {
            return common_games_str;
        }

        public void setCommon_games_str(String common_games_str) {
            this.common_games_str = common_games_str;
        }

        public int getRecharge_tips_coin() {
            return recharge_tips_coin;
        }

        public void setRecharge_tips_coin(int recharge_tips_coin) {
            this.recharge_tips_coin = recharge_tips_coin;
        }

        public ArrayList<GiftBean> getGifts() {
            return gifts;
        }

        public void setGifts(ArrayList<GiftBean> gifts) {
            this.gifts = gifts;
        }

        public String getCp_right_title() {
            return cp_right_title;
        }

        public void setCp_right_title(String cp_right_title) {
            this.cp_right_title = cp_right_title;
        }

        public String getCp_right_content() {
            return cp_right_content;
        }

        public void setCp_right_content(String cp_right_content) {
            this.cp_right_content = cp_right_content;
        }

        public boolean isHas_click_cp_hint() {
            return has_click_cp_hint;
        }

        public void setHas_click_cp_hint(boolean has_click_cp_hint) {
            this.has_click_cp_hint = has_click_cp_hint;
        }

        public boolean isHas_click_team_hint() {
            return has_click_team_hint;
        }

        public ArrayList<GiftNums> getGift_nums() {
            return gift_nums;
        }

        public void setGift_nums(ArrayList<GiftNums> gift_nums) {
            this.gift_nums = gift_nums;
        }

        public void setHas_click_team_hint(boolean has_click_team_hint) {
            this.has_click_team_hint = has_click_team_hint;
        }

        public int getCp_score_limit() {
            return cp_score_limit;
        }

        public void setCp_score_limit(int cp_score_limit) {
            this.cp_score_limit = cp_score_limit;
        }

        public String getCp_limit_desc() {
            return cp_limit_desc;
        }

        public void setCp_limit_desc(String cp_limit_desc) {
            this.cp_limit_desc = cp_limit_desc;
        }

        public boolean isIs_mic_minute_cost() {
            return is_mic_minute_cost;
        }

        public void setIs_mic_minute_cost(boolean is_mic_minute_cost) {
            this.is_mic_minute_cost = is_mic_minute_cost;
        }

        public String getMic_price() {
            return mic_price;
        }

        public void setMic_price(String mic_price) {
            this.mic_price = mic_price;
        }

        public boolean isHighlight_mic_btn() {
            return highlight_mic_btn;
        }

        public void setHighlight_mic_btn(boolean highlight_mic_btn) {
            this.highlight_mic_btn = highlight_mic_btn;
        }

        public String getTeam_time_lack_time() {
            return team_time_lack_time;
        }

        public void setTeam_time_lack_time(String team_time_lack_time) {
            this.team_time_lack_time = team_time_lack_time;
        }

        private String team_time_lack_time;

        public boolean isShow_team_time_lack() {
            return show_team_time_lack;
        }

        public void setShow_team_time_lack(boolean show_team_time_lack) {
            this.show_team_time_lack = show_team_time_lack;
        }

        public String getTeam_time_lack_content() {
            return team_time_lack_content;
        }

        public void setTeam_time_lack_content(String team_time_lack_content) {
            this.team_time_lack_content = team_time_lack_content;
        }

        public String getStatus_str() {
            return status_str;
        }

        public void setStatus_str(String status_str) {
            this.status_str = status_str;
        }

        public TeamInfo getTeam_info() {
            return team_info;
        }

        public void setTeam_info(TeamInfo team_info) {
            this.team_info = team_info;
        }

        public int getCp_status() {
            return cp_status;
        }

        public void setCp_status(int cp_status) {
            this.cp_status = cp_status;
        }

        public long getVoice_end_time() {
            return voice_end_time;
        }

        public void setVoice_end_time(long voice_end_time) {
            this.voice_end_time = voice_end_time;
        }

        public String getAvatar_url() {
            return avatar_url;
        }

        public String getAvatar_border() {
            return avatar_border;
        }

        public void setAvatar_border(String avatar_border) {
            this.avatar_border = avatar_border;
        }

        public void setAvatar_url(String avatar_url) {
            this.avatar_url = avatar_url;
        }

        public boolean isIs_friend() {
            return is_friend;
        }

        public void setIs_friend(boolean is_friend) {
            this.is_friend = is_friend;
        }

        public boolean isIs_leader() {
            return is_leader;
        }

        public void setIs_leader(boolean is_leader) {
            this.is_leader = is_leader;
        }

        public String getNick_name() {
            return nick_name;
        }

        public void setNick_name(String nick_name) {
            this.nick_name = nick_name;
        }

        public boolean isIs_block_by_other() {
            return is_block_by_other;
        }

        public void setIs_block_by_other(boolean is_block_by_other) {
            this.is_block_by_other = is_block_by_other;
        }

        public boolean isIs_block_other() {
            return is_block_other;
        }

        public void setIs_block_other(boolean is_block_other) {
            this.is_block_other = is_block_other;
        }

        public int getUser_coins() {
            return user_coins;
        }

        public void setUser_coins(int user_coins) {
            this.user_coins = user_coins;
        }

        public boolean isShow_couple() {
            return show_couple;
        }

        public void setShow_couple(boolean show_couple) {
            this.show_couple = show_couple;
        }

        public String getCouple_score() {
            return couple_score;
        }

        public void setCouple_score(String couple_score) {
            this.couple_score = couple_score;
        }

        public String getCouple_desc() {
            return couple_desc;
        }

        public void setCouple_desc(String couple_desc) {
            this.couple_desc = couple_desc;
        }

        public boolean isIs_cp_end() {
            return is_cp_end;
        }

        public void setIs_cp_end(boolean is_cp_end) {
            this.is_cp_end = is_cp_end;
        }

        public List<String> getMsg_templates() {
            return msg_templates;
        }

        public void setMsg_templates(List<String> msg_templates) {
            this.msg_templates = msg_templates;
        }

        public String getIm_video_token() {
            return im_video_token;
        }

        public void setIm_video_token(String im_video_token) {
            this.im_video_token = im_video_token;
        }

        public boolean isIs_online() {
            return is_online;
        }

        public void setIs_online(boolean is_online) {
            this.is_online = is_online;
        }

        public String getCouple_room_id() {
            return couple_room_id;
        }

        public void setCouple_room_id(String couple_room_id) {
            this.couple_room_id = couple_room_id;
        }

        public String getCouple_im_room_id() {
            return couple_im_room_id;
        }

        public void setCouple_im_room_id(String couple_im_room_id) {
            this.couple_im_room_id = couple_im_room_id;
        }

        public boolean isIs_in_private_room() {
            return is_in_private_room;
        }

        public void setIs_in_private_room(boolean is_in_private_room) {
            this.is_in_private_room = is_in_private_room;
        }

        public long getServer_timestamp() {
            return server_timestamp;
        }

        public void setServer_timestamp(long server_timestamp) {
            this.server_timestamp = server_timestamp;
        }

        public int getGift_box_key_count() {
            return gift_box_key_count;
        }

        public void setGift_box_key_count(int gift_box_key_count) {
            this.gift_box_key_count = gift_box_key_count;
        }

        public boolean isGift_box_key_test() {
            return gift_box_key_test;
        }

        public void setGift_box_key_test(boolean gift_box_key_test) {
            this.gift_box_key_test = gift_box_key_test;
        }
    }
    public class CartoonGuide{
        private String img;

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }
    }


    public static class TempFriendInfo{
        private boolean expire_friend;
        private int expire_seconds;
        private boolean temp_friend;
        private int temp_friend_coin;

        public boolean isExpire_friend() {
            return expire_friend;
        }

        public void setExpire_friend(boolean expire_friend) {
            this.expire_friend = expire_friend;
        }

        public int getExpire_seconds() {
            return expire_seconds;
        }

        public void setExpire_seconds(int expire_seconds) {
            this.expire_seconds = expire_seconds;
        }

        public boolean isTemp_friend() {
            return temp_friend;
        }

        public void setTemp_friend(boolean temp_friend) {
            this.temp_friend = temp_friend;
        }

        public int getTemp_friend_coin() {
            return temp_friend_coin;
        }

        public void setTemp_friend_coin(int temp_friend_coin) {
            this.temp_friend_coin = temp_friend_coin;
        }
    }

    public class TeamInfo {
        private String leader_id;
        private List<Member> member_group;
        private String team_im_id;

        private String team_id;

        public String getTeam_im_id() {
            return team_im_id;
        }

        public void setTeam_im_id(String team_im_id) {
            this.team_im_id = team_im_id;
        }

        public String getTeam_id() {
            return team_id;
        }

        public void setTeam_id(String team_id) {
            this.team_id = team_id;
        }

        public String getLeader_id() {
            return leader_id;
        }

        public void setLeader_id(String leader_id) {
            this.leader_id = leader_id;
        }

        public List<Member> getMember_group() {
            return member_group;
        }

        public void setMember_group(List<Member> member_group) {
            this.member_group = member_group;
        }
    }

    public class Member {
        private String user_id;
        private long join_time;

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public long getJoin_time() {
            return join_time;
        }

        public void setJoin_time(long join_time) {
            this.join_time = join_time;
        }
    }

    public static class GiftBean implements Serializable {
        public int add_score;
        public EffectBean effect;
        public String icon;
        public int id;
        public String name;
        public int price;
        private int type;

        public GiftTypeEnum getGiftType() {
            //获取礼物类型
            return GiftTypeEnum.typeOfValue(type);
        }
    }

    public static class GiftNums implements Serializable{
        public int count;
        public String name;
    }

    public static class EffectBean implements Serializable{
        public String json;
        public String audio;
    }

    public class RankInfo {
        public int group_id;
        public boolean has_rank_rise;
        public String name;
        public String need_score;
        public String point_award;
        public String rank;
        public String rank_score_base;
        public String rank_icon;
        public String cp_score;
        public boolean level_pack_test;
        public boolean need_advance;
        public String frame_url;
        public String next_rank_name;
        public boolean cp_hint;

    }
    public static class P2PRankUpgradeAwardInfo {
        public ArrayList<Info> award_list;
        public String group_name ;

        public class Info{
            public String icon;
            public String title;
            public String num;
        }
    }

    public static class GiftBox{
        public int count;
        public boolean show_guide ;
    }

}
