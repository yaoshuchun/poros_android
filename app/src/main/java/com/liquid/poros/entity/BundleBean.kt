package com.liquid.poros.entity

import com.google.gson.JsonObject

data class BundleBean(val _id:String
                      ,val bundle_url :String = ""
                      ,val bundle:String
                      ,val cate_1:Int
                      ,val cate_2:Int
                      ,val ext:JsonObject
                      ,val icon_url:String
                      ,val bean: BundleExtBean )

data class BundleExtBean(val bundle:String,val gender:Int,val icon :String)


data class BundleListBean(val data: List<BundleBean>, val msg:String, val code:Int)