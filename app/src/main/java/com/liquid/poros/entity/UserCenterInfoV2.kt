package com.liquid.poros.entity

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * 个人中心
 */
class UserCenterInfoV2 :BaseBean(){
    var user_info: UserInfo? = null
    var wzrydata: WZRYDATA? = null
    var hpjydata: HPJYDATA? = null
    var staticGameBean: SettingEditGameBean? = null


    class UserInfo {
        var user_id: String? = null
        var nick_name: String? = null
        var avatar_url: String? = null
        var gender: String? = null
        var age: String? = null
        var cover_img: String? = null
        var couple_desc: String? = null
        var games: List<String>? = null
        var voice_intro: String? = null
        var isIs_block = false
            private set
        var isIs_on_mic = false
            private set
        var room_id: String? = null
        var im_room_id: String? = null
        var isMatch_control //区分对照组&实验组  true 对照组
                = false
        var game_info: GameInfoBean? = null
        var game_info_static: GameInfoStaticBean? = null
        var rank_info: RankInfoBean? = null
        var isNew_relation_free_chat = false
        var user_level = 0
        var isShow_block_btn = false
        fun setIs_block(is_block: Boolean) {
            isIs_block = is_block
        }

        fun setIs_on_mic(is_on_mic: Boolean) {
            isIs_on_mic = is_on_mic
        }

        class RankInfoBean {
            var avatar_url: String? = null
            var coins = 0
            var gift_list: List<String>? = null
            var is_guard = false
            var nick_name: String? = null
        }

        class GameInfoBean {
            /**
             * hpjy : {"platform":[1],"rank":[1,2,3,4,5,6,7,8],"zone":[1,2]}
             * other : []
             * wzry : {"platform":[1],"rank":[1,2,3,4,5,6,7,8],"zone":[1,2]}
             */
            var hpjy: HpjyBean? = null
            var wzry: WzryBean? = null
            var other: List<NormalGame>? = null

            class HpjyBean {
                var platform: List<Int>? = null
                var rank: List<Int>? = null
                var zone: List<Int>? = null
            }

            class WzryBean {
                var platform: List<Int>? = null
                var rank: List<Int>? = null
                var zone: List<Int>? = null
            }
        }

        class GameInfoStaticBean {
            /**
             * hpjy : {"platform":{"1":"安卓","2":"IOS"},"rank":{"0":"不限","1":"热血青铜","2":"不屈白银","3":"英勇黄金","4":"坚韧铂金","5":"不朽星钻","6":"荣耀皇冠","7":"超级王牌","8":"无敌战神"},"zone":{"1":"QQ","2":"微信"}}
             * other : {"1":"穿越火线 手游","2":"QQ飞车 手游","3":"欢乐斗地主","4":"迷你世界","5":"我的世界手游","6":"第五人格","7":"跑跑卡丁车"}
             * wzry : {"platform":{"1":"安卓","2":"IOS"},"rank":{"0":"不限","1":"倔强青铜","2":"秩序白银","3":"荣耀黄金","4":"尊贵铂金","5":"永恒钻石","6":"至尊星耀","7":"最强王者","8":"荣耀王者"},"zone":{"1":"QQ","2":"微信"}}
             */
            var hpjy: HpjyBeanX? = null
            var other: List<NormalGame>? = null
            var wzry: WzryBeanX? = null

            class HpjyBeanX {
                /**
                 * platform : {"1":"安卓","2":"IOS"}
                 * rank : {"0":"不限","1":"热血青铜","2":"不屈白银","3":"英勇黄金","4":"坚韧铂金","5":"不朽星钻","6":"荣耀皇冠","7":"超级王牌","8":"无敌战神"}
                 * zone : {"1":"QQ","2":"微信"}
                 */
                var platform: PlatformBean? = null
                var rank: RankBean? = null
                var zone: ZoneBean? = null

                class PlatformBean {
                    /**
                     * 1 : 安卓
                     * 2 : IOS
                     */
                    @SerializedName("1")
                    private var `_$1`: String? = null

                    @SerializedName("2")
                    private var `_$2`: String? = null
                    fun `get_$1`(): String? {
                        return `_$1`
                    }

                    fun `set_$1`(`_$1`: String?) {
                        this.`_$1` = `_$1`
                    }

                    fun `get_$2`(): String? {
                        return `_$2`
                    }

                    fun `set_$2`(`_$2`: String?) {
                        this.`_$2` = `_$2`
                    }
                }

                class RankBean {
                    /**
                     * 0 : 不限
                     * 1 : 热血青铜
                     * 2 : 不屈白银
                     * 3 : 英勇黄金
                     * 4 : 坚韧铂金
                     * 5 : 不朽星钻
                     * 6 : 荣耀皇冠
                     * 7 : 超级王牌
                     * 8 : 无敌战神
                     */
                    @SerializedName("0")
                    private var `_$0`: String? = null

                    @SerializedName("1")
                    private var `_$1`: String? = null

                    @SerializedName("2")
                    private var `_$2`: String? = null

                    @SerializedName("3")
                    private var `_$3`: String? = null

                    @SerializedName("4")
                    private var `_$4`: String? = null

                    @SerializedName("5")
                    private var `_$5`: String? = null

                    @SerializedName("6")
                    private var `_$6`: String? = null

                    @SerializedName("7")
                    private var `_$7`: String? = null

                    @SerializedName("8")
                    private var `_$8`: String? = null
                    fun `get_$0`(): String? {
                        return `_$0`
                    }

                    fun `set_$0`(`_$0`: String?) {
                        this.`_$0` = `_$0`
                    }

                    fun `get_$1`(): String? {
                        return `_$1`
                    }

                    fun `set_$1`(`_$1`: String?) {
                        this.`_$1` = `_$1`
                    }

                    fun `get_$2`(): String? {
                        return `_$2`
                    }

                    fun `set_$2`(`_$2`: String?) {
                        this.`_$2` = `_$2`
                    }

                    fun `get_$3`(): String? {
                        return `_$3`
                    }

                    fun `set_$3`(`_$3`: String?) {
                        this.`_$3` = `_$3`
                    }

                    fun `get_$4`(): String? {
                        return `_$4`
                    }

                    fun `set_$4`(`_$4`: String?) {
                        this.`_$4` = `_$4`
                    }

                    fun `get_$5`(): String? {
                        return `_$5`
                    }

                    fun `set_$5`(`_$5`: String?) {
                        this.`_$5` = `_$5`
                    }

                    fun `get_$6`(): String? {
                        return `_$6`
                    }

                    fun `set_$6`(`_$6`: String?) {
                        this.`_$6` = `_$6`
                    }

                    fun `get_$7`(): String? {
                        return `_$7`
                    }

                    fun `set_$7`(`_$7`: String?) {
                        this.`_$7` = `_$7`
                    }

                    fun `get_$8`(): String? {
                        return `_$8`
                    }

                    fun `set_$8`(`_$8`: String?) {
                        this.`_$8` = `_$8`
                    }
                }

                class ZoneBean {
                    /**
                     * 1 : QQ
                     * 2 : 微信
                     */
                    @SerializedName("1")
                    private var `_$1`: String? = null

                    @SerializedName("2")
                    private var `_$2`: String? = null
                    fun `get_$1`(): String? {
                        return `_$1`
                    }

                    fun `set_$1`(`_$1`: String?) {
                        this.`_$1` = `_$1`
                    }

                    fun `get_$2`(): String? {
                        return `_$2`
                    }

                    fun `set_$2`(`_$2`: String?) {
                        this.`_$2` = `_$2`
                    }
                }
            }

            class WzryBeanX {
                /**
                 * platform : {"1":"安卓","2":"IOS"}
                 * rank : {"0":"不限","1":"倔强青铜","2":"秩序白银","3":"荣耀黄金","4":"尊贵铂金","5":"永恒钻石","6":"至尊星耀","7":"最强王者","8":"荣耀王者"}
                 * zone : {"1":"QQ","2":"微信"}
                 */
                var platform: PlatformBeanX? = null
                var rank: RankBeanX? = null
                var zone: ZoneBeanX? = null

                class PlatformBeanX {
                    /**
                     * 1 : 安卓
                     * 2 : IOS
                     */
                    @SerializedName("1")
                    private var `_$1`: String? = null

                    @SerializedName("2")
                    private var `_$2`: String? = null
                    fun `get_$1`(): String? {
                        return `_$1`
                    }

                    fun `set_$1`(`_$1`: String?) {
                        this.`_$1` = `_$1`
                    }

                    fun `get_$2`(): String? {
                        return `_$2`
                    }

                    fun `set_$2`(`_$2`: String?) {
                        this.`_$2` = `_$2`
                    }
                }

                class RankBeanX {
                    /**
                     * 0 : 不限
                     * 1 : 倔强青铜
                     * 2 : 秩序白银
                     * 3 : 荣耀黄金
                     * 4 : 尊贵铂金
                     * 5 : 永恒钻石
                     * 6 : 至尊星耀
                     * 7 : 最强王者
                     * 8 : 荣耀王者
                     */
                    @SerializedName("0")
                    private var `_$0`: String? = null

                    @SerializedName("1")
                    private var `_$1`: String? = null

                    @SerializedName("2")
                    private var `_$2`: String? = null

                    @SerializedName("3")
                    private var `_$3`: String? = null

                    @SerializedName("4")
                    private var `_$4`: String? = null

                    @SerializedName("5")
                    private var `_$5`: String? = null

                    @SerializedName("6")
                    private var `_$6`: String? = null

                    @SerializedName("7")
                    private var `_$7`: String? = null

                    @SerializedName("8")
                    private var `_$8`: String? = null
                    fun `get_$0`(): String? {
                        return `_$0`
                    }

                    fun `set_$0`(`_$0`: String?) {
                        this.`_$0` = `_$0`
                    }

                    fun `get_$1`(): String? {
                        return `_$1`
                    }

                    fun `set_$1`(`_$1`: String?) {
                        this.`_$1` = `_$1`
                    }

                    fun `get_$2`(): String? {
                        return `_$2`
                    }

                    fun `set_$2`(`_$2`: String?) {
                        this.`_$2` = `_$2`
                    }

                    fun `get_$3`(): String? {
                        return `_$3`
                    }

                    fun `set_$3`(`_$3`: String?) {
                        this.`_$3` = `_$3`
                    }

                    fun `get_$4`(): String? {
                        return `_$4`
                    }

                    fun `set_$4`(`_$4`: String?) {
                        this.`_$4` = `_$4`
                    }

                    fun `get_$5`(): String? {
                        return `_$5`
                    }

                    fun `set_$5`(`_$5`: String?) {
                        this.`_$5` = `_$5`
                    }

                    fun `get_$6`(): String? {
                        return `_$6`
                    }

                    fun `set_$6`(`_$6`: String?) {
                        this.`_$6` = `_$6`
                    }

                    fun `get_$7`(): String? {
                        return `_$7`
                    }

                    fun `set_$7`(`_$7`: String?) {
                        this.`_$7` = `_$7`
                    }

                    fun `get_$8`(): String? {
                        return `_$8`
                    }

                    fun `set_$8`(`_$8`: String?) {
                        this.`_$8` = `_$8`
                    }
                }

                class ZoneBeanX {
                    /**
                     * 1 : QQ
                     * 2 : 微信
                     */
                    @SerializedName("1")
                    private var `_$1`: String? = null

                    @SerializedName("2")
                    private var `_$2`: String? = null
                    fun `get_$1`(): String? {
                        return `_$1`
                    }

                    fun `set_$1`(`_$1`: String?) {
                        this.`_$1` = `_$1`
                    }

                    fun `get_$2`(): String? {
                        return `_$2`
                    }

                    fun `set_$2`(`_$2`: String?) {
                        this.`_$2` = `_$2`
                    }
                }
            }
        }
    }

    class WZRYDATA {
        var wzry_platform_list: ArrayList<Map<String, String>>? = null
        var wzry_rank_list: ArrayList<Map<String, String>>? = null
        var wzry_area_list: ArrayList<Map<String, String>>? = null
        var icon: String? = null
    }

    class HPJYDATA {
        var hpjy_platform_list: ArrayList<Map<String, String>>? = null
        var hpjy_rank_list: ArrayList<Map<String, String>>? = null
        var hpjy_area_list: ArrayList<Map<String, String>>? = null
        var icon: String? = null
    }
}