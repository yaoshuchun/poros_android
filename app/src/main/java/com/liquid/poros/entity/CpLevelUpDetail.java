package com.liquid.poros.entity;

import java.util.List;

public class CpLevelUpDetail {


    /**
     * code : 0
     * data : {"advance_detail":{"_id":4,"gift":{"gift_name":"温馨粉钻戒指","gift_pic_url":"https://liquid-poros.oss-cn-beijing.aliyuncs.com/cp_rank/%E7%B2%89%E9%92%BB2.png","gift_price":14.9},"reward":{"frame_url":"","free_msg":20,"free_team_duration":1200,"reward_day":3}},"group":{"_id":4,"cp_tag":"https://liquid-poros.oss-cn-beijing.aliyuncs.com/cp_rank/cp_tag/%E7%99%BD%E9%93%B6%E6%A0%87%E7%AD%BE.png","icon_tuple":["https://liquid-poros.oss-cn-beijing.aliyuncs.com/cp_rank/%E7%99%BD%E9%93%B6_%E9%AB%98%E4%BA%AE.png","https://liquid-poros.oss-cn-beijing.aliyuncs.com/cp_rank/%E7%99%BD%E9%93%B6-%E7%BD%AE%E7%81%B0.png"],"name":"白银CP","point_addition":1.2,"point_award":30,"point_limit":160,"rank_num":5},"level_swear":false,"swear_words":"你像一股春风，漾起了我心海里爱的波澜；你像沾满露珠的花瓣，给我带来了一室芳香；你像划过蓝天的哨鸽，给我带来心灵的宁静。今生，我只爱你！","target_level_swear":false}
     * msg : 成功
     */

    private int code;
    /**
     * advance_detail : {"_id":4,"gift":{"gift_name":"温馨粉钻戒指","gift_pic_url":"https://liquid-poros.oss-cn-beijing.aliyuncs.com/cp_rank/%E7%B2%89%E9%92%BB2.png","gift_price":14.9},"reward":{"frame_url":"","free_msg":20,"free_team_duration":1200,"reward_day":3}}
     * group : {"_id":4,"cp_tag":"https://liquid-poros.oss-cn-beijing.aliyuncs.com/cp_rank/cp_tag/%E7%99%BD%E9%93%B6%E6%A0%87%E7%AD%BE.png","icon_tuple":["https://liquid-poros.oss-cn-beijing.aliyuncs.com/cp_rank/%E7%99%BD%E9%93%B6_%E9%AB%98%E4%BA%AE.png","https://liquid-poros.oss-cn-beijing.aliyuncs.com/cp_rank/%E7%99%BD%E9%93%B6-%E7%BD%AE%E7%81%B0.png"],"name":"白银CP","point_addition":1.2,"point_award":30,"point_limit":160,"rank_num":5}
     * level_swear : false
     * swear_words : 你像一股春风，漾起了我心海里爱的波澜；你像沾满露珠的花瓣，给我带来了一室芳香；你像划过蓝天的哨鸽，给我带来心灵的宁静。今生，我只爱你！
     * target_level_swear : false
     */

    private DataBean data;
    private String msg;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public static class DataBean {
        /**
         * _id : 4
         * gift : {"gift_name":"温馨粉钻戒指","gift_pic_url":"https://liquid-poros.oss-cn-beijing.aliyuncs.com/cp_rank/%E7%B2%89%E9%92%BB2.png","gift_price":14.9}
         * reward : {"frame_url":"","free_msg":20,"free_team_duration":1200,"reward_day":3}
         */

        private AdvanceDetailBean advance_detail;
        /**
         * _id : 4
         * cp_tag : https://liquid-poros.oss-cn-beijing.aliyuncs.com/cp_rank/cp_tag/%E7%99%BD%E9%93%B6%E6%A0%87%E7%AD%BE.png
         * icon_tuple : ["https://liquid-poros.oss-cn-beijing.aliyuncs.com/cp_rank/%E7%99%BD%E9%93%B6_%E9%AB%98%E4%BA%AE.png","https://liquid-poros.oss-cn-beijing.aliyuncs.com/cp_rank/%E7%99%BD%E9%93%B6-%E7%BD%AE%E7%81%B0.png"]
         * name : 白银CP
         * point_addition : 1.2
         * point_award : 30
         * point_limit : 160
         * rank_num : 5
         */

        private GroupBean group;
        private boolean level_swear;
        private String swear_words;
        private boolean target_level_swear;
        private String target_swear_words;

        public String getTarget_swear_words() {
            return target_swear_words;
        }

        public void setTarget_swear_words(String target_swear_words) {
            this.target_swear_words = target_swear_words;
        }

        public AdvanceDetailBean getAdvance_detail() {
            return advance_detail;
        }

        public void setAdvance_detail(AdvanceDetailBean advance_detail) {
            this.advance_detail = advance_detail;
        }

        public GroupBean getGroup() {
            return group;
        }

        public void setGroup(GroupBean group) {
            this.group = group;
        }

        public boolean isLevel_swear() {
            return level_swear;
        }

        public void setLevel_swear(boolean level_swear) {
            this.level_swear = level_swear;
        }

        public String getSwear_words() {
            return swear_words;
        }

        public void setSwear_words(String swear_words) {
            this.swear_words = swear_words;
        }

        public boolean isTarget_level_swear() {
            return target_level_swear;
        }

        public void setTarget_level_swear(boolean target_level_swear) {
            this.target_level_swear = target_level_swear;
        }

        public static class AdvanceDetailBean {
            private int _id;
            /**
             * gift_name : 温馨粉钻戒指
             * gift_pic_url : https://liquid-poros.oss-cn-beijing.aliyuncs.com/cp_rank/%E7%B2%89%E9%92%BB2.png
             * gift_price : 14.9
             */

            private GiftBean gift;
            /**
             * frame_url :
             * free_msg : 20
             * free_team_duration : 1200
             * reward_day : 3
             */

            private RewardBean reward;

            public int get_id() {
                return _id;
            }

            public void set_id(int _id) {
                this._id = _id;
            }

            public GiftBean getGift() {
                return gift;
            }

            public void setGift(GiftBean gift) {
                this.gift = gift;
            }

            public RewardBean getReward() {
                return reward;
            }

            public void setReward(RewardBean reward) {
                this.reward = reward;
            }

            public static class GiftBean {
                private String gift_name;
                private String gift_pic_url;
                private double gift_price;

                public String getGift_name() {
                    return gift_name;
                }

                public void setGift_name(String gift_name) {
                    this.gift_name = gift_name;
                }

                public String getGift_pic_url() {
                    return gift_pic_url;
                }

                public void setGift_pic_url(String gift_pic_url) {
                    this.gift_pic_url = gift_pic_url;
                }

                public double getGift_price() {
                    return gift_price;
                }

                public void setGift_price(double gift_price) {
                    this.gift_price = gift_price;
                }
            }

            public static class RewardBean {
                private String frame_url;
                private int free_msg;
                private int free_team_duration;
                private int reward_day;

                public String getFrame_url() {
                    return frame_url;
                }

                public void setFrame_url(String frame_url) {
                    this.frame_url = frame_url;
                }

                public int getFree_msg() {
                    return free_msg;
                }

                public void setFree_msg(int free_msg) {
                    this.free_msg = free_msg;
                }

                public int getFree_team_duration() {
                    return free_team_duration;
                }

                public void setFree_team_duration(int free_team_duration) {
                    this.free_team_duration = free_team_duration;
                }

                public int getReward_day() {
                    return reward_day;
                }

                public void setReward_day(int reward_day) {
                    this.reward_day = reward_day;
                }
            }
        }

        public static class GroupBean {
            private int _id;
            private String cp_tag;
            private String name;
            private double point_addition;
            private int point_award;
            private int point_limit;
            private int rank_num;
            private List<String> icon_tuple;

            public int get_id() {
                return _id;
            }

            public void set_id(int _id) {
                this._id = _id;
            }

            public String getCp_tag() {
                return cp_tag;
            }

            public void setCp_tag(String cp_tag) {
                this.cp_tag = cp_tag;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public double getPoint_addition() {
                return point_addition;
            }

            public void setPoint_addition(double point_addition) {
                this.point_addition = point_addition;
            }

            public int getPoint_award() {
                return point_award;
            }

            public void setPoint_award(int point_award) {
                this.point_award = point_award;
            }

            public int getPoint_limit() {
                return point_limit;
            }

            public void setPoint_limit(int point_limit) {
                this.point_limit = point_limit;
            }

            public int getRank_num() {
                return rank_num;
            }

            public void setRank_num(int rank_num) {
                this.rank_num = rank_num;
            }

            public List<String> getIcon_tuple() {
                return icon_tuple;
            }

            public void setIcon_tuple(List<String> icon_tuple) {
                this.icon_tuple = icon_tuple;
            }
        }
    }
}
