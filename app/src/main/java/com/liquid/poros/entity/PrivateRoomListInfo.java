package com.liquid.poros.entity;

import com.liquid.poros.analytics.utils.ExposeData;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public class PrivateRoomListInfo {
    private int code;
    private String msg;
    private Data data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {
        private List<CoupleRoomInfo> room_list;
        private String top_img;

        public List<CoupleRoomInfo> getRoom_list() {
            return room_list;
        }

        public void setRoom_list(List<CoupleRoomInfo> room_list) {
            this.room_list = room_list;
        }

        public String getTop_img() {
            return top_img;
        }

        public void setTop_img(String top_img) {
            this.top_img = top_img;
        }
    }

    public static class CoupleRoomInfo extends ExposeData implements Serializable {
        private String room_id;
        private String nick_name;
        private String room_desc;
        private String room_cover;
        private AnchorInfo anchor;
        private MicUserInfo mic_user;
        private String border_color;
        private int online_cnt;
        private String im_room_id;
        private boolean private_room;

        public CoupleRoomInfo() {
        }
        public String getIm_room_id() {
            return im_room_id;
        }

        public void setIm_room_id(String im_room_id) {
            this.im_room_id = im_room_id;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            CoupleRoomInfo that = (CoupleRoomInfo) o;
            return Objects.equals(room_id, that.room_id);
        }

        @Override
        public int hashCode() {
            return Objects.hash(room_id);
        }

        public String getBorder_color() {
            return border_color;
        }

        public void setBorder_color(String border_color) {
            this.border_color = border_color;
        }

        public String getRoom_id() {
            return room_id;
        }

        public void setRoom_id(String room_id) {
            this.room_id = room_id;
        }

        public String getNick_name() {
            return nick_name;
        }

        public void setNick_name(String nick_name) {
            this.nick_name = nick_name;
        }

        public String getRoom_desc() {
            return room_desc;
        }

        public void setRoom_desc(String room_desc) {
            this.room_desc = room_desc;
        }

        public String getRoom_cover() {
            return room_cover;
        }

        public void setRoom_cover(String room_cover) {
            this.room_cover = room_cover;
        }

        public AnchorInfo getAnchor() {
            return anchor;
        }

        public void setAnchor(AnchorInfo anchor) {
            this.anchor = anchor;
        }

        public MicUserInfo getMic_user() {
            return mic_user;
        }

        public void setMic_user(MicUserInfo mic_user) {
            this.mic_user = mic_user;
        }

        public int getOnline_cnt() {
            return online_cnt;
        }

        public void setOnline_cnt(int online_cnt) {
            this.online_cnt = online_cnt;
        }

        public boolean isPrivate_room() {
            return private_room;
        }

        public void setPrivate_room(boolean private_room) {
            this.private_room = private_room;
        }
    }

    public static class AnchorInfo implements Serializable {
        private String user_id;
        private String avatar_url;
        private String nick_name;

        public AnchorInfo() {
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getAvatar_url() {
            return avatar_url;
        }

        public void setAvatar_url(String avatar_url) {
            this.avatar_url = avatar_url;
        }

        public String getNick_name() {
            return nick_name;
        }

        public void setNick_name(String nick_name) {
            this.nick_name = nick_name;
        }
    }

}
