package com.liquid.poros.entity;

import java.util.List;

public class FeedDetailBean{
    public SingleCommentBean feed_info;
    public List<CommentInfo> comment_list;
}
