package com.liquid.poros.entity

/**
 * 连麦列表List
 */
data class MicListInfo(
        var items: List<ItemsBean>? = null
):BaseBean(){
    class ItemsBean {
        var age = 0
        var avatar_url: String? = null
        var coins = 0
        var gender = 0
        var hpjy_game_area: String? = null
        var hpjy_game_nick: Any? = null
        var im_account: String? = null
        var nick_name: String? = null
        var user_id: String? = null
        var isCould_invite = false
    }
}
