package com.liquid.poros.entity

class AppraiseInfo : BaseBean() {
    /**
     * appraise : true
     * appraise_data : {"boy_satisfaction":["服务态度好","帮我说话","推荐的嘉宾很合适","介绍CP很积极"],"boy_un_satisfaction":["要礼物频繁","被粗暴放下麦","主持能力差","联合嘉宾骗礼物","工作不认真","匹配嘉宾不合适","嘉宾态度差","女嘉宾头像与真人不符"],"girl_satisfaction":["服务态度好","帮我说话","推荐的嘉宾很合适","介绍CP很积极"],"girl_un_satisfaction":["要礼物频繁","被粗暴放下麦","主持能力差","联合嘉宾骗礼物","工作不认真","匹配嘉宾不合适","嘉宾态度差","女嘉宾头像与真人不符"]}
     */
    var appraise = false
    var appraise_data: AppraiseDataBean? = null

    class AppraiseDataBean {
        var boy_satisfaction: List<String>? = null
        var boy_un_satisfaction: List<String>? = null
        var girl_satisfaction: List<String>? = null
        var girl_un_satisfaction: List<String>? = null
    }
}