package com.liquid.poros.entity;

import java.io.Serializable;
import java.util.List;

public class CoupleCardListInfo extends BaseBean{
    public Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data  extends BaseBean implements Serializable{
        private String title;
        private String tip;
        private String desc;
        private int coin;
        private String cp_right_title;
        private String cp_right_content;
        private List<CoupleCardInfo> card_list;
        private boolean buy_style;
        private int cp_card_time;

        public int getCp_card_time() {
            return cp_card_time;
        }

        public void setCp_card_time(int cp_card_time) {
            this.cp_card_time = cp_card_time;
        }

        public boolean isBuy_style() {
            return buy_style;
        }

        public void setBuy_style(boolean buy_style) {
            this.buy_style = buy_style;
        }

        public String getTip() {
            return tip;
        }

        public void setTip(String tip) {
            this.tip = tip;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public int getCoin() {
            return coin;
        }

        public void setCoin(int coin) {
            this.coin = coin;
        }

        public List<CoupleCardInfo> getCard_list() {
            return card_list;
        }

        public void setCard_list(List<CoupleCardInfo> card_list) {
            this.card_list = card_list;
        }

        public String getCp_right_title() {
            return cp_right_title;
        }

        public void setCp_right_title(String cp_right_title) {
            this.cp_right_title = cp_right_title;
        }

        public String getCp_right_content() {
            return cp_right_content;
        }

        public void setCp_right_content(String cp_right_content) {
            this.cp_right_content = cp_right_content;
        }
    }
}
