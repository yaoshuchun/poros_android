package com.liquid.poros.entity;

import com.liquid.im.kit.custom.CpLevelUpdateTestAttachment;
import com.liquid.im.kit.custom.LevelChangeAttachment;
import com.liquid.im.kit.custom.ReceiveGiftPackageAttachment;
import com.liquid.im.kit.custom.ShowGiftPackageAttachment;
import com.liquid.im.kit.custom.UpdateDressAttachment;
import com.liquid.im.kit.custom.VideoAdAttachment;
import com.netease.nimlib.sdk.msg.model.IMMessage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sundroid on 18/03/2019.
 */

public class IMRoomMessageParser {

    public static List<IMMessage> parseJson(String json) {
        List<IMMessage> messages = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(json);
            if (jsonObject.optInt("code") == 0) {
                JSONArray messagess = jsonObject.optJSONArray("data");
                for (int i = 0; i < messagess.length(); i++) {
                    JSONObject msg = messagess.optJSONObject(i);
                    IMMessageImpl imMessage = new IMMessageImpl();
                    if (imMessage.parseJson(msg)) {
                        if (imMessage.getAttachment() instanceof LevelChangeAttachment) {
                            continue;
                        }

                        if (imMessage.getAttachment() instanceof CpLevelUpdateTestAttachment) {
                            continue;
                        }

                        if (imMessage.getAttachment() instanceof ShowGiftPackageAttachment) {
                            continue;
                        }

                        if (imMessage.getAttachment() instanceof ReceiveGiftPackageAttachment) {
                            continue;
                        }

                        if (imMessage.getAttachment() instanceof VideoAdAttachment) {
                            continue;
                        }

                        if (imMessage.getAttachment() instanceof UpdateDressAttachment) {
                            continue;
                        }

                        messages.add(imMessage);
                    }
                }
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return messages;
    }
}
