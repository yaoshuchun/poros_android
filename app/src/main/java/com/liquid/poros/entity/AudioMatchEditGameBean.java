package com.liquid.poros.entity;

import java.util.ArrayList;

/**
 * Created by sundroid on 28/04/2019.
 */

public class AudioMatchEditGameBean {
    public SettingGameBean peace_elite;
    public SettingGameBean honor_of_kings;
    public SettingOtherGameBean other;

    public static class SettingGameBean{
        public String icon;
        public AudioMatchGameBean platform;
        public AudioMatchGameBean rank;
        public AudioMatchGameBean zone;
    }

    public static class SettingOtherGameBean{
        public String icon;
        public ArrayList<AudioMatchGameBean.Item> list = new ArrayList<>();
    }
}
