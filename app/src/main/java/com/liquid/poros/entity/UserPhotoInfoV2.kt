package com.liquid.poros.entity

/**
 * 个人相册 V2
 */
class UserPhotoInfoV2 : BaseBean() {
    var photos: List<Photos>? = null
    class Photos {
        var photo_id: String? = null
        var type = 0
        var path: String? = null
    }
}