package com.liquid.poros.entity

/**
 * 盲盒中奖记录
 */
data class BlindBoxRecordInfo(var record_list: List<BlindBoxBean>? = null) : BaseBean() {
    class BlindBoxBean {
        var icon: String? = null
        var name: String? = null
        var nick_name: String? = null
        var create_time: String? = null
    }
}