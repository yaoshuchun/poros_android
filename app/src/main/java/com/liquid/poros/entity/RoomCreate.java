package com.liquid.poros.entity;

public class RoomCreate extends BaseModel {
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data{

        private String cp_user_id;
        private String team_id;
        private String leader_id;

        public String getCp_user_id() {
            return cp_user_id;
        }

        public void setCp_user_id(String cp_user_id) {
            this.cp_user_id = cp_user_id;
        }

        public String getTeam_id() {
            return team_id;
        }

        public void setTeam_id(String team_id) {
            this.team_id = team_id;
        }

        public String getLeader_id() {
            return leader_id;
        }

        public void setLeader_id(String leader_id) {
            this.leader_id = leader_id;
        }
    }

}
