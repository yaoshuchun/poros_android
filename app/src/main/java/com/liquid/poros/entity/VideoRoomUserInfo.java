package com.liquid.poros.entity;

import java.util.List;

/**
 * 视频房女嘉宾详情
 */
public class VideoRoomUserInfo {
    private int code;
    private String msg;
    private Data data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data {
        private UserInfo user_info;
        private String button_content;
        private RankInfoBean rank_info;

        public RankInfoBean getRank_info() {
            return rank_info;
        }

        public void setRank_info(RankInfoBean rank_info) {
            this.rank_info = rank_info;
        }

        public static class RankInfoBean{
            public String avatar_url;
            public int coins;
            public List<String> gift_list;
            public boolean is_guard;
            public String nick_name;
        }

        public UserInfo getUser_info() {
            return user_info;
        }

        public void setUser_info(UserInfo user_info) {
            this.user_info = user_info;
        }

        public String getButton_content() {
            return button_content;
        }

        public void setButton_content(String button_content) {
            this.button_content = button_content;
        }

        public static class UserInfo {
            private String user_id;
            private String nick_name;
            private String avatar_url;
            private boolean is_friend;
            private String couple_desc;
            private int user_charm;

            public int getUser_charm() {
                return user_charm;
            }

            public void setUser_charm(int user_charm) {
                this.user_charm = user_charm;
            }

            public String getUser_id() {
                return user_id;
            }

            public void setUser_id(String user_id) {
                this.user_id = user_id;
            }

            public String getNick_name() {
                return nick_name;
            }

            public void setNick_name(String nick_name) {
                this.nick_name = nick_name;
            }

            public String getAvatar_url() {
                return avatar_url;
            }

            public void setAvatar_url(String avatar_url) {
                this.avatar_url = avatar_url;
            }

            public boolean isIs_friend() {
                return is_friend;
            }

            public void setIs_friend(boolean is_friend) {
                this.is_friend = is_friend;
            }

            public String getCouple_desc() {
                return couple_desc;
            }

            public void setCouple_desc(String couple_desc) {
                this.couple_desc = couple_desc;
            }
        }
    }
}
