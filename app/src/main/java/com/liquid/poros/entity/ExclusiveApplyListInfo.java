package com.liquid.poros.entity;

import java.util.ArrayList;

/**
 * Created by sundroid on 28/04/2019.
 */

public class ExclusiveApplyListInfo {
    private int code;
    private Data data;
    private String msg;

    public void setCode(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Data getData() {
        return data;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public class Data {

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }

        public ArrayList<Item> getItems() {
            return items;
        }

        public void setItems(ArrayList<Item> items) {
            this.items = items;
        }

        private int count;
        private ArrayList<Item> items;
    }

    public class Item {
        public String user_id;
        public String nick_name;
        public String avatar_url;
        public boolean   could_invite;
        public String coins;
        public String age;
        public String im_account;
    }
}
