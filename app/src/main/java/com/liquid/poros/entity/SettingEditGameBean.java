package com.liquid.poros.entity;

import java.util.ArrayList;

/**
 * Created by sundroid on 28/04/2019.
 */

public class SettingEditGameBean {
    public SettingGameBean peace_elite;
    public SettingGameBean honor_of_kings;
    public SettingOtherGameBean other;

    public static class SettingGameBean{
        public String icon;
        public EditGameBean platform;
        public EditGameBean rank;
        public EditGameBean zone;
    }

    public static class SettingOtherGameBean{
        public String icon;
        public ArrayList<EditGameBean.Item> list = new ArrayList<>();
    }
}
