package com.liquid.poros.entity;

import java.util.ArrayList;

/**
 * Created by sundroid on 28/04/2019.
 */

public class AudioMatchGameBean {
    public ArrayList<Item> items = new ArrayList<Item>();
    public String des;

    public static class Item {
        public String id;
        public String name;
        public boolean select = false;
        public String image_url = "";
    }
}
