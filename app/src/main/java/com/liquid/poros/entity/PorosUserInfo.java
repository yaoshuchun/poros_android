package com.liquid.poros.entity;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.constant.UserRoleEnum;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.poros.utils.SharePreferenceUtil;

import org.json.JSONArray;
import org.json.JSONObject;

public class PorosUserInfo {
    private String account_token;
    private String im_account;
    private String im_token;
    private String avatar_url;
    private int balance;
    private String nick_name;
    private String user_id;
    private int gender;
    private String own_family;
    private String in_family;
    private boolean new_user;
    private boolean is_admin;
    private boolean is_assistant;
    private int gift_card_cnt;
    private String coin;
    private String couple_room_id;
    private boolean show_income;
    private boolean need_set_basic_info;
    private boolean show_voice_order;
    private int voice_order_status;
    private boolean match_control;//是否是对照组
    private boolean is_vip;
    private long register_time;
    private int user_level;
    private int user_charm;
    private boolean is_super_admin;//是否是官方账号
    private String self_gold_master_id;


    public boolean isIs_super_admin() {
        return is_super_admin;
    }

    public void setIs_super_admin(boolean is_super_admin) {
        this.is_super_admin = is_super_admin;
    }

    public boolean isIs_vip() {
        return is_vip;
    }

    public void setIs_vip(boolean is_vip) {
        this.is_vip = is_vip;
    }

    public boolean isMatch_control() {
        return match_control;
    }

    public void setMatch_control(boolean match_control) {
        this.match_control = match_control;
    }

    public boolean isShow_voice_order() {
        return show_voice_order;
    }

    public void setShow_voice_order(boolean show_voice_order) {
        this.show_voice_order = show_voice_order;
    }

    public String getSelf_gold_master_id() {
        return self_gold_master_id;
    }

    public void setSelf_gold_master_id(String self_gold_master_id) {
        this.self_gold_master_id = self_gold_master_id;
    }

    public int getUser_charm() {
        return user_charm;
    }

    public void setUser_charm(int user_charm) {
        this.user_charm = user_charm;
    }

    public int getUser_level() {
        return user_level;
    }

    public void setUser_level(int user_level) {
        this.user_level = user_level;
    }

    public long getRegister_time() {
        return register_time;
    }

    public void setRegister_time(long register_time) {
        this.register_time = register_time;
    }

    public int getVoice_order_status() {
        return voice_order_status;
    }

    public void setVoice_order_status(int voice_order_status) {
        this.voice_order_status = voice_order_status;
    }

    public boolean isNeed_set_basic_info() {
        return need_set_basic_info;
    }

    public void setNeed_set_basic_info(boolean need_set_basic_info) {
        this.need_set_basic_info = need_set_basic_info;
    }

    public String getCouple_room_id() {
        return couple_room_id;
    }

    public void setCouple_room_id(String couple_room_id) {
        this.couple_room_id = couple_room_id;
    }

    public boolean isShow_income() {
        return show_income;
    }

    public void setShow_income(boolean show_income) {
        this.show_income = show_income;
    }

    public int getGift_card_cnt() {
        return gift_card_cnt;
    }

    public void setGift_card_cnt(int gift_card_cnt) {
        this.gift_card_cnt = gift_card_cnt;
    }

    public String getCoin() {
        return coin;
    }

    public void setCoin(String coin) {
        this.coin = coin;
    }

    public boolean isIs_admin() {
        return is_admin;
    }

    public boolean isIs_assistant() {
        return is_assistant;
    }

    private String avatar_border;
    private String hpjy_game_area;
    private String hpjy_game_nick;
    private String game_level;
    private boolean new_reg_user;

    private boolean push_fans_msg;
    private int user_role;//用户身份
    private String rcv_order_status;//接单状态 0关闭接单 1开启接单
    private String rcv_order_team_name;//开黑房间名称
    private String age_level;//年龄
    private String age;
    private boolean is_live_unactive_user; //是否是新用户
    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getAge_level() {
        return age_level;
    }

    public void setAge_level(String age_level) {
        this.age_level = age_level;
    }

    public String getHpjy_game_area() {
        return hpjy_game_area;
    }

    public String getHpjy_game_nick() {
        if ("null".equals(hpjy_game_nick)) {
            return "";
        }
        return hpjy_game_nick;
    }

    public boolean isNew_user() {
        return new_user;
    }

    public boolean isNew_reg_user() {
        return SharePreferenceUtil.getBoolean(SharePreferenceUtil.FILE_USER_ACCOUNT_DATA, SharePreferenceUtil.KEY_NEW_REG_USER, false);
    }

    public void setNew_reg_user(boolean new_reg_user) {
        this.new_reg_user = new_reg_user;
    }

    private boolean certification;
    private boolean enable_create_family;
    private boolean show_beauty;

    public boolean isShow_beauty() {
        return show_beauty;
    }

    public void setShow_beauty(boolean show_beauty) {
        this.show_beauty = show_beauty;
    }

    /**
     * 评价分
     */
    private String score;
    private String score_count;


    public PorosUserInfo(String account_token) {
        this.account_token = account_token;
    }

    public PorosUserInfo() {
    }

    public boolean isEnable_create_family() {
        return enable_create_family;
    }


    public String getAvatar_border() {
        return avatar_border;
    }


    public String getGame_level() {
        return game_level;
    }


    public boolean isPush_fans_msg() {
        return push_fans_msg;
    }

    public static PorosUserInfo generate(JSONObject json) {
        try {
            PorosUserInfo userInfo = new PorosUserInfo();
            userInfo.account_token = json.optString("token");
            userInfo.is_super_admin = json.optBoolean("is_super_admin");
            userInfo.is_live_unactive_user = json.optBoolean("is_live_unactive_user");
            userInfo.im_account = json.optString("im_account");
            userInfo.im_token = json.optString("im_token");
            userInfo.avatar_url = json.optString("avatar_url");
            userInfo.balance = json.optInt("balance");
            userInfo.nick_name = json.optString("nick_name");
            userInfo.user_id = json.optString("user_id");
            userInfo.self_gold_master_id = json.optString("self_gold_master_id");
            userInfo.gender = json.optInt("gender");
            userInfo.avatar_border = json.optString("avatar_border");
            userInfo.new_user = json.optBoolean("new_user", false);
            userInfo.hpjy_game_area = json.optString("hpjy_game_area");
            userInfo.hpjy_game_nick = json.optString("hpjy_game_nick");
            userInfo.certification = json.optBoolean("certification");
            userInfo.game_level = json.optString("game_level");
            userInfo.enable_create_family = json.optBoolean("enable_create_family");
            userInfo.score = json.optString("score");
            if (json.has("is_forbid_user")) {
                Constants.hide_peking_user = json.optBoolean("is_forbid_user");
            }
            userInfo.score_count = json.optString("score_count");
            userInfo.couple_room_id = json.optString("couple_room_id");
            userInfo.show_income = json.optBoolean("show_income");
            userInfo.new_reg_user = json.optBoolean("new_reg_user", false);
            userInfo.is_assistant = json.optBoolean("is_assistant", false);
            userInfo.is_admin = json.optBoolean("is_admin", false);
            userInfo.show_beauty = json.optBoolean("show_beauty", false);
            userInfo.show_voice_order = json.optBoolean("show_voice_order", false);
            userInfo.voice_order_status = json.optInt("voice_order_status", 0);
            userInfo.need_set_basic_info = json.optBoolean("need_set_basic_info", false);
            AccountUtil.getInstance().setNeedSetBasicInfo(userInfo.need_set_basic_info);
            userInfo.match_control = json.optBoolean("match_control", false);
            AccountUtil.getInstance().setMatchControl(userInfo.match_control);
            userInfo.gift_card_cnt = json.optInt("gift_card_cnt");
            userInfo.coin = json.optString("coin");
            AccountUtil.getInstance().setCoin(userInfo.coin);
            userInfo.push_fans_msg = json.optBoolean("push_fans_msg", false);
            userInfo.age_level = json.optString("age_level");
            userInfo.age = json.optString("age");
            AccountUtil.getInstance().setAgeLevel(userInfo.age_level);
            JSONArray own_family = json.optJSONArray("own_group");
            if (own_family.length() > 0) {
                userInfo.own_family = own_family.optString(0);
            }
            JSONArray in_family = json.optJSONArray("in_group");
            if (in_family.length() > 0) {
                userInfo.in_family = in_family.optString(0);
            }
            userInfo.user_role = json.optInt("user_role");
            userInfo.rcv_order_status = json.optString("rcv_order_status");
            userInfo.rcv_order_team_name = json.optString("rcv_order_team_name");
            userInfo.is_vip = json.optBoolean("is_vip",false);
            userInfo.register_time = json.optLong("register_time",0);
            userInfo.user_level = json.optInt("user_level",0);
            userInfo.user_charm = json.optInt("user_charm",0);
            SharePreferenceUtil.saveString(SharePreferenceUtil.FILE_USER_ACCOUNT_DATA, SharePreferenceUtil.KEY_USER_ACCOUNT_TOKEN, userInfo.getAccount_token());
            return userInfo;
        } catch (Exception e) {
            return null;
        }
    }

    public boolean isCertification() {
        return certification;
    }

    public String getAccount_token() {
        return account_token;
    }


    public boolean isIs_live_unactive_user() {
        return is_live_unactive_user;
    }

    public void setIs_live_unactive_user(boolean is_live_unactive_user) {
        this.is_live_unactive_user = is_live_unactive_user;
    }
    public void setAccount_token(String account_token) {
        this.account_token = account_token;
    }

    public String getIm_account() {
        return im_account;
    }

    public void setIm_account(String im_account) {
        this.im_account = im_account;
    }

    public String getIm_token() {
        return im_token;
    }

    public void setIm_token(String im_token) {
        this.im_token = im_token;
    }

    public String getAvatar_url() {
        return avatar_url;
    }

    public void setAvatar_url(String avatar_url) {
        this.avatar_url = avatar_url;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public String getNick_name() {
        return nick_name;
    }

    public void setNick_name(String nick_name) {
        this.nick_name = nick_name;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getOwn_family() {
        return own_family;
    }

    public void setOwn_family(String own_family) {
        this.own_family = own_family;
    }

    public String getIn_family() {
        return in_family;
    }

    public void setIn_family(String in_family) {
        this.in_family = in_family;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getScore_count() {
        return score_count;
    }

    public void setScore_count(String score_count) {
        this.score_count = score_count;
    }

    public UserRoleEnum getUser_role() {
        return UserRoleEnum.typeOfValue(user_role);
    }

    public void setUser_role(int user_role) {
        this.user_role = user_role;
    }

    public String getRcv_order_status() {
        return rcv_order_status;
    }

    public void setRcv_order_status(String rcv_order_status) {
        this.rcv_order_status = rcv_order_status;
    }

    public String getRcv_order_team_name() {
        return rcv_order_team_name;
    }

    public void setRcv_order_team_name(String rcv_order_team_name) {
        this.rcv_order_team_name = rcv_order_team_name;
    }
}
