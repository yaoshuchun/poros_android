package com.liquid.poros.entity

/**
 * 提现记录
 */
data class WithdrawalHistoryInfo(var user_balance_record_list: List<withdrawalBean>? = null) : BaseBean() {
    class withdrawalBean {
        var desc: String? = null
        var count = 0
        var unit : String? = null
        var time_stamp : String? = null
        var withdraw_status =0
    }
}