package com.liquid.poros.entity;

/**
 * Created by sundroid on 08/04/2019.
 */

public enum MessageAction {
    COPY(1, "复制"),
    RECALL(2, "撤回"),
    BAN_MSG(3, "禁言"),
    INVITE_VOICE(5, "上麦"),
    KICK_MEMBER(4, "踢出"),
    DELETE_MSG(6, "删除"),
    CLOSE_MIC(7, "关麦"),
    BAN_USER(9, "禁麦"),
    OPEN_MIC(8, "开麦");

    private int action;
    private String desc;

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getAction() {
        return action;
    }

    public void setAction(int action) {
        this.action = action;
    }

    MessageAction(int action, String desc) {
        this.action = action;
        this.desc = desc;
    }
}