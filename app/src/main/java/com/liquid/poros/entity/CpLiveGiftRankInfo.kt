package com.liquid.poros.entity

/**
 * cp房礼物排行榜
 */
class CpLiveGiftRankInfo : BaseBean() {
    var items: List<RankInfo>? = null
    var total_coins: String? = null

    inner class RankInfo {
        var avatar_url: String? = null
        var coins : String? = null
        var nick_name: String? = null
        var user_id: String? = null
    }
}