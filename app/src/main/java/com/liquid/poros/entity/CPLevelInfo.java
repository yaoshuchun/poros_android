package com.liquid.poros.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sundroid on 28/04/2019.
 */

public class CPLevelInfo extends BaseBean {

    public String female_avatar_url;
    public String male_avatar_url;
    public boolean is_test_group;
    public ArrayList<Award> award_list;
    public RankInfo rank_info;

    public class Award {
        public String name;
        public String group_icon;
        public int type = 1;
        public boolean has_group = false;
        public int advance_level;

        private String female_swear;
        private boolean has_swear;
        private String male_swear;

        public String getFemale_swear() {
            return female_swear;
        }

        public void setFemale_swear(String female_swear) {
            this.female_swear = female_swear;
        }

        public boolean isHas_swear() {
            return has_swear;
        }

        public void setHas_swear(boolean has_swear) {
            this.has_swear = has_swear;
        }

        public String getMale_swear() {
            return male_swear;
        }

        public void setMale_swear(String male_swear) {
            this.male_swear = male_swear;
        }
    }

    public class Rank {
        public String award;
        public String name;
        public boolean has_award;
    }

    public class RankInfo {
        public int group_id;
        public String name;
        public String need_score;
        public String next_rank_icon;
        public String next_rank_name;
        public String point_addition;
        public String point_award;
        public String rank;
        public String rank_icon;
        public String rank_score_base;
        public String cp_score;
        public String next_gift_box_cnt;//升级下一等级
    }
}
