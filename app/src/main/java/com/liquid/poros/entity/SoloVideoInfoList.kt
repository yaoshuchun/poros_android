package com.liquid.poros.entity

class SoloVideoInfoList : BaseBean() {
    var items: MutableList<SoloVideoInfo>? = null
    val busy_hint : String? = null
    var total : Int? = null
}
