package com.liquid.poros.entity;

import java.util.List;

public class FriendMessageListInfo {
    private int code;
    private String msg;
    private FriendMessage data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public FriendMessage getData() {
        return data;
    }

    public void setData(FriendMessage data) {
        this.data = data;
    }

    public class FriendMessage {

        private List<FriendMessageInfo> session_list;

        private HighScoreEntryInfo high_score_entry_info;

        private int unread_msg_cnt;
        private boolean show_expire_list;
        private boolean expire_list_red_dot;
        public int getUnread_msg_cnt() {
            return unread_msg_cnt;
        }

        public HighScoreEntryInfo getHigh_score_entry_info() {
            return high_score_entry_info;
        }

        public void setHigh_score_entry_info(HighScoreEntryInfo high_score_entry_info) {
            this.high_score_entry_info = high_score_entry_info;
        }

        public boolean isShow_expire_list() {
            return show_expire_list;
        }

        public void setShow_expire_list(boolean show_expire_list) {
            this.show_expire_list = show_expire_list;
        }

        public boolean isExpire_list_red_dot() {
            return expire_list_red_dot;
        }

        public void setExpire_list_red_dot(boolean expire_list_red_dot) {
            this.expire_list_red_dot = expire_list_red_dot;
        }

        public void setUnread_msg_cnt(int unread_msg_cnt) {
            this.unread_msg_cnt = unread_msg_cnt;
        }

        public List<FriendMessageInfo> getSession_list() {
            return session_list;
        }

        public void setSession_list(List<FriendMessageInfo> session_list) {
            this.session_list = session_list;
        }

    }

    public class HighScoreEntryInfo {
        private String icon;
        private String sub_title;
        private String title;
        private int unread_msg_cnt;

        public int getUnread_msg_cnt() {
            return unread_msg_cnt;
        }

        public void setUnread_msg_cnt(int unread_msg_cnt) {
            this.unread_msg_cnt = unread_msg_cnt;
        }

        public String getIcon() {
            return icon;
        }

        public void setIcon(String icon) {
            this.icon = icon;
        }

        public String getSub_title() {
            return sub_title;
        }

        public void setSub_title(String sub_title) {
            this.sub_title = sub_title;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }

}
