package com.liquid.poros.entity

data class SessionDressBean(val female_suit_list: ArrayList<BundleBean> = ArrayList(), val male_suit_list: MutableList<BundleBean> = mutableListOf(), var cartoon_suit_id: String = "") : BaseBean()