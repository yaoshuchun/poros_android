package com.liquid.poros.entity;

public class NormalGame {

    /**
     * _id : 1
     * icon : https://liquid-poros.oss-cn-beijing.aliyuncs.com/b/game/%E7%A9%BF%E8%B6%8A%E7%81%AB%E7%BA%BF.png
     * name : 穿越火线
     */

    private int _id;
    private String icon;
    private String name;

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
