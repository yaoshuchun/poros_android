package com.liquid.poros.entity;

import java.util.List;

public class FaceInfo {
	public List<Integer[]> mouth;//# 多少秒， 开口数， 非开口口数， 无口数
	public List<Integer[]> face;//# 多少秒， 正脸数， 非正脸数， 无脸数
	public String user_id;
	public String room_id;
	public String mic_position;
	public static final String LIVE_ROOM_0 = "live_room_0";//主持人位
	public static final String LIVE_ROOM_1 = "live_room_1";//左侧位
	public static final String LIVE_ROOM_2 = "live_room_2";//右侧位
	public static final String VIDEO_MATCH = "video_match";//视频速配

}
