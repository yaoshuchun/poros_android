package com.liquid.poros.entity;

import java.io.Serializable;

public class FriendMessageInfo implements Serializable {
    private String earliest_msg_id;
    private String newest_msg_id;
    private String user_id;
    private int unread_msg_cnt;
    private String nick_name;
    private String newest_msg_body;
    private String avatar_url;
    private long newest_msg_time;
    private int pin_priority;// 置顶优先级 -1为非置顶消息
    private String role_tags;
    private String other_tags;
    private String game_zone;
    private boolean is_free;
    private String game;
    private boolean is_friend;
    private String guild_name;
    private String relation_tags;//我的师傅/徒弟
    private String cp_tag;//cp标签
    private boolean is_online;
    private boolean is_on_mic;
    private boolean is_on_video;
    private boolean guard_tag;
    private boolean is_in_private_room;
    private boolean is_in_pk_room;
    private int client;
    private long temp_remain_time;//临时好友剩余时间

    private long curr_temp_friend_remain_time;//当前临时好友剩余时间，实时变化

    public String getRelation_tags() {
        return relation_tags;
    }

    public void setRelation_tags(String relation_tags) {
        this.relation_tags = relation_tags;
    }

    public String getGuild_name() {
        return guild_name;
    }

    public void setGuild_name(String guild_name) {
        this.guild_name = guild_name;
    }

    public boolean isIs_friend() {
        return is_friend;
    }

    public void setIs_friend(boolean is_friend) {
        this.is_friend = is_friend;
    }

    public String getGame_zone() {
        return game_zone;
    }

    public void setGame_zone(String game_zone) {
        this.game_zone = game_zone;
    }

    public boolean isIs_free() {
        return is_free;
    }

    public void setIs_free(boolean is_free) {
        this.is_free = is_free;
    }

    public String getGame() {
        return game;
    }

    public void setGame(String game) {
        this.game = game;
    }

    public String getRole_tags() {
        return role_tags;
    }

    public void setRole_tags(String role_tags) {
        this.role_tags = role_tags;
    }

    public String getOther_tags() {
        return other_tags;
    }

    public void setOther_tags(String other_tags) {
        this.other_tags = other_tags;
    }

    public String getEarliest_msg_id() {
        return earliest_msg_id;
    }

    public void setEarliest_msg_id(String earliest_msg_id) {
        this.earliest_msg_id = earliest_msg_id;
    }

    public int getClient() {
        return client;
    }

    public void setClient(int client) {
        this.client = client;
    }

    public String getNewest_msg_id() {
        return newest_msg_id;
    }

    public void setNewest_msg_id(String newest_msg_id) {
        this.newest_msg_id = newest_msg_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public int getUnread_msg_cnt() {
        return unread_msg_cnt;
    }

    public void setUnread_msg_cnt(int unread_msg_cnt) {
        this.unread_msg_cnt = unread_msg_cnt;
    }

    public String getNick_name() {
        return nick_name;
    }

    public void setNick_name(String nick_name) {
        this.nick_name = nick_name;
    }

    public String getNewest_msg_body() {
        return newest_msg_body;
    }

    public void setNewest_msg_body(String newest_msg_body) {
        this.newest_msg_body = newest_msg_body;
    }

    public String getAvatar_url() {
        return avatar_url;
    }

    public void setAvatar_url(String avatar_url) {
        this.avatar_url = avatar_url;
    }

    public long getNewest_msg_time() {
        return newest_msg_time;
    }

    public void setNewest_msg_time(long newest_msg_time) {
        this.newest_msg_time = newest_msg_time;
    }

    public int getPin_priority() {
        return pin_priority;
    }

    public void setPin_priority(int pin_priority) {
        this.pin_priority = pin_priority;
    }

    public String getCp_tag() {
        return cp_tag;
    }

    public void setCp_tag(String cp_tag) {
        this.cp_tag = cp_tag;
    }

    public boolean isIs_online() {
        return is_online;
    }

    public void setIs_online(boolean is_online) {
        this.is_online = is_online;
    }

    public boolean isIs_on_mic() {
        return is_on_mic;
    }

    public void setIs_on_mic(boolean is_on_mic) {
        this.is_on_mic = is_on_mic;
    }

    public boolean isIs_on_video() {
        return is_on_video;
    }

    public void setIs_on_video(boolean is_on_video) {
        this.is_on_video = is_on_video;
    }

    public boolean isGuard_tag() {
        return guard_tag;
    }

    public void setGuard_tag(boolean guard_tag) {
        this.guard_tag = guard_tag;

    }

    public boolean isIs_in_private_room() {
        return is_in_private_room;
    }

    public void setIs_in_private_room(boolean is_in_private_room) {
        this.is_in_private_room = is_in_private_room;
    }

    public boolean isIs_in_pk_room() {
        return is_in_pk_room;
    }

    public void setIs_in_pk_room(boolean is_in_pk_room) {
        this.is_in_pk_room = is_in_pk_room;
    }
    public long getTemp_remain_time() {
        return temp_remain_time;
    }

    public void setTemp_remain_time(long temp_remain_time) {
        this.temp_remain_time = temp_remain_time;
    }

    public long getCurr_temp_friend_remain_time() {
        return curr_temp_friend_remain_time;
    }

    public void setCurr_temp_friend_remain_time(long curr_temp_friend_remain_time) {
        this.curr_temp_friend_remain_time = curr_temp_friend_remain_time;
    }
}
