package com.liquid.poros.entity;

import java.io.Serializable;

/**
 * 用户余额
 */
public class UserCoinInfo {
    private int code;
    private String msg;
    private Data data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data implements Serializable {
        private String coin;
        private String unit;//单位
        private CardData mic_cards;
        private CardData team_cards;

        public String getCoin() {
            return coin;
        }

        public void setCoin(String coin) {
            this.coin = coin;
        }

        public String getUnit() {
            return unit;
        }

        public void setUnit(String unit) {
            this.unit = unit;
        }

        public CardData getMic_cards() {
            return mic_cards;
        }

        public void setMic_cards(CardData mic_cards) {
            this.mic_cards = mic_cards;
        }

        public CardData getTeam_cards() {
            return team_cards;
        }

        public void setTeam_cards(CardData team_cards) {
            this.team_cards = team_cards;
        }

        public class CardData {
            private String count;
            private String expire_time;
            private String unit;//单位

            public String getCount() {
                return count;
            }

            public void setCount(String count) {
                this.count = count;
            }

            public String getExpire_time() {
                return expire_time;
            }

            public void setExpire_time(String expire_time) {
                this.expire_time = expire_time;
            }

            public String getUnit() {
                return unit;
            }

            public void setUnit(String unit) {
                this.unit = unit;
            }
        }
    }
}
