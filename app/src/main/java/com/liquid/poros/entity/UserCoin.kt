package com.liquid.poros.entity

data class UserCoin(
var  coin:Int,
var unit:String,
var  free_msgs:FreeMsgs,
var  mic_cards:MicCards,
var  team_cards:TeamCards,
var video_free_minutes:Int //速配分钟补贴 0 分钟用尽,调起充值弹窗，1 正常匹配
) :BaseBean(){
    class FreeMsgs(var count:Int,var unit:String){

    }
    class MicCards(var count:Int,var expire_time:String,var unit:String){

    }
    class TeamCards(var count:Int,var expire_time:String,var unit:String){

    }
}