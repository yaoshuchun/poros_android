package com.liquid.poros.entity;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 个人中心
 * 使用V2版本
 */
@Deprecated
public class UserCenterInfo {
    private int code;
    private String msg;
    private Data data;
    private WZRYDATA wzrydata;
    private HPJYDATA hpjydata;
    private SettingEditGameBean staticGameBean;

    public SettingEditGameBean getStaticGameBean() {
        return staticGameBean;
    }

    public void setStaticGameBean(SettingEditGameBean staticGameBean) {
        this.staticGameBean = staticGameBean;
    }

    public WZRYDATA getWzrydata() {
        return wzrydata;
    }

    public void setWzrydata(WZRYDATA wzrydata) {
        this.wzrydata = wzrydata;
    }

    public HPJYDATA getHpjydata() {
        return hpjydata;
    }

    public void setHpjydata(HPJYDATA hpjydata) {
        this.hpjydata = hpjydata;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public static class Data {
        private UserInfo user_info;

        public UserInfo getUser_info() {
            return user_info;
        }

        public void setUser_info(UserInfo user_info) {
            this.user_info = user_info;
        }

        public static class UserInfo {
            private String user_id;
            private String nick_name;
            private String avatar_url;
            private String gender;
            private String age;
            private int user_charm;
            private String cover_img;
            private String couple_desc;
            private List<String> games;
            private int client;
            private String voice_intro;
            private boolean is_block;
            private boolean is_on_mic;
            private String room_id;
            private String im_room_id;
            private boolean match_control;//区分对照组&实验组  true 对照组
            private GameInfoBean game_info;
            private GameInfoStaticBean game_info_static;
            private RankInfoBean rank_info;
            private boolean new_relation_free_chat;
            private int user_level;

            public int getUser_level() {
                return user_level;
            }

            public void setUser_level(int user_level) {
                this.user_level = user_level;
            }

            public boolean isNew_relation_free_chat() {
                return new_relation_free_chat;
            }

            public void setNew_relation_free_chat(boolean new_relation_free_chat) {
                this.new_relation_free_chat = new_relation_free_chat;
            }

            public int getClient() {
                return client;
            }

            public void setClient(int client) {
                this.client = client;
            }

            public RankInfoBean getRank_info() {
                return rank_info;
            }

            public void setRank_info(RankInfoBean rank_info) {
                this.rank_info = rank_info;
            }

            public String getIm_room_id() {
                return im_room_id;
            }

            public void setIm_room_id(String im_room_id) {
                this.im_room_id = im_room_id;
            }

            private boolean show_block_btn;

            public boolean isShow_block_btn() {
                return show_block_btn;
            }

            public void setShow_block_btn(boolean show_block_btn) {
                this.show_block_btn = show_block_btn;
            }

            public boolean isIs_block() {
                return is_block;
            }

            public void setIs_block(boolean is_block) {
                this.is_block = is_block;
            }


            public String getUser_id() {
                return user_id;
            }

            public void setUser_id(String user_id) {
                this.user_id = user_id;
            }

            public String getNick_name() {
                return nick_name;
            }

            public void setNick_name(String nick_name) {
                this.nick_name = nick_name;
            }

            public String getAvatar_url() {
                return avatar_url;
            }

            public void setAvatar_url(String avatar_url) {
                this.avatar_url = avatar_url;
            }

            public String getGender() {
                return gender;
            }

            public void setGender(String gender) {
                this.gender = gender;
            }

            public String getAge() {
                return age;
            }

            public void setAge(String age) {
                this.age = age;
            }

            public int getUser_charm() {
                return user_charm;
            }

            public void setUser_charm(int user_charm) {
                this.user_charm = user_charm;
            }

            public String getCover_img() {
                return cover_img;
            }

            public void setCover_img(String cover_img) {
                this.cover_img = cover_img;
            }

            public String getCouple_desc() {
                return couple_desc;
            }

            public void setCouple_desc(String couple_desc) {
                this.couple_desc = couple_desc;
            }

            public List<String> getGames() {
                return games;
            }

            public void setGames(List<String> games) {
                this.games = games;
            }

            public String getVoice_intro() {
                return voice_intro;
            }

            public void setVoice_intro(String voice_intro) {
                this.voice_intro = voice_intro;
            }

            public boolean isIs_on_mic() {
                return is_on_mic;
            }

            public void setIs_on_mic(boolean is_on_mic) {
                this.is_on_mic = is_on_mic;
            }

            public String getRoom_id() {
                return room_id;
            }

            public void setRoom_id(String room_id) {
                this.room_id = room_id;
            }

            public boolean isMatch_control() {
                return match_control;
            }

            public void setMatch_control(boolean match_control) {
                this.match_control = match_control;
            }

            public GameInfoBean getGame_info() {
                return game_info;
            }

            public void setGame_info(GameInfoBean game_info) {
                this.game_info = game_info;
            }

            public GameInfoStaticBean getGame_info_static() {
                return game_info_static;
            }

            public void setGame_info_static(GameInfoStaticBean game_info_static) {
                this.game_info_static = game_info_static;
            }

            public static class RankInfoBean{
                public String avatar_url;
                public int coins;
                public List<String> gift_list;
                public boolean is_guard;
                public String nick_name;
            }

            public static class GameInfoBean {
                /**
                 * hpjy : {"platform":[1],"rank":[1,2,3,4,5,6,7,8],"zone":[1,2]}
                 * other : []
                 * wzry : {"platform":[1],"rank":[1,2,3,4,5,6,7,8],"zone":[1,2]}
                 */

                private HpjyBean hpjy;
                private WzryBean wzry;
                private List<NormalGame> other;

                public HpjyBean getHpjy() {
                    return hpjy;
                }

                public void setHpjy(HpjyBean hpjy) {
                    this.hpjy = hpjy;
                }

                public WzryBean getWzry() {
                    return wzry;
                }

                public void setWzry(WzryBean wzry) {
                    this.wzry = wzry;
                }

                public List<NormalGame> getOther() {
                    return other;
                }

                public void setOther(List<NormalGame> other) {
                    this.other = other;
                }

                public static class HpjyBean {
                    private List<Integer> platform;
                    private List<Integer> rank;
                    private List<Integer> zone;

                    public List<Integer> getPlatform() {
                        return platform;
                    }

                    public void setPlatform(List<Integer> platform) {
                        this.platform = platform;
                    }

                    public List<Integer> getRank() {
                        return rank;
                    }

                    public void setRank(List<Integer> rank) {
                        this.rank = rank;
                    }

                    public List<Integer> getZone() {
                        return zone;
                    }

                    public void setZone(List<Integer> zone) {
                        this.zone = zone;
                    }
                }

                public static class WzryBean {
                    private List<Integer> platform;
                    private List<Integer> rank;
                    private List<Integer> zone;

                    public List<Integer> getPlatform() {
                        return platform;
                    }

                    public void setPlatform(List<Integer> platform) {
                        this.platform = platform;
                    }

                    public List<Integer> getRank() {
                        return rank;
                    }

                    public void setRank(List<Integer> rank) {
                        this.rank = rank;
                    }

                    public List<Integer> getZone() {
                        return zone;
                    }

                    public void setZone(List<Integer> zone) {
                        this.zone = zone;
                    }
                }
            }

            public static class GameInfoStaticBean {
                /**
                 * hpjy : {"platform":{"1":"安卓","2":"IOS"},"rank":{"0":"不限","1":"热血青铜","2":"不屈白银","3":"英勇黄金","4":"坚韧铂金","5":"不朽星钻","6":"荣耀皇冠","7":"超级王牌","8":"无敌战神"},"zone":{"1":"QQ","2":"微信"}}
                 * other : {"1":"穿越火线 手游","2":"QQ飞车 手游","3":"欢乐斗地主","4":"迷你世界","5":"我的世界手游","6":"第五人格","7":"跑跑卡丁车"}
                 * wzry : {"platform":{"1":"安卓","2":"IOS"},"rank":{"0":"不限","1":"倔强青铜","2":"秩序白银","3":"荣耀黄金","4":"尊贵铂金","5":"永恒钻石","6":"至尊星耀","7":"最强王者","8":"荣耀王者"},"zone":{"1":"QQ","2":"微信"}}
                 */

                private HpjyBeanX hpjy;
                private List<NormalGame> other;
                private WzryBeanX wzry;

                public HpjyBeanX getHpjy() {
                    return hpjy;
                }

                public void setHpjy(HpjyBeanX hpjy) {
                    this.hpjy = hpjy;
                }

                public List<NormalGame> getOther() {
                    return other;
                }

                public void setOther(List<NormalGame> other) {
                    this.other = other;
                }

                public WzryBeanX getWzry() {
                    return wzry;
                }

                public void setWzry(WzryBeanX wzry) {
                    this.wzry = wzry;
                }

                public static class HpjyBeanX {
                    /**
                     * platform : {"1":"安卓","2":"IOS"}
                     * rank : {"0":"不限","1":"热血青铜","2":"不屈白银","3":"英勇黄金","4":"坚韧铂金","5":"不朽星钻","6":"荣耀皇冠","7":"超级王牌","8":"无敌战神"}
                     * zone : {"1":"QQ","2":"微信"}
                     */

                    private PlatformBean platform;
                    private RankBean rank;
                    private ZoneBean zone;

                    public PlatformBean getPlatform() {
                        return platform;
                    }

                    public void setPlatform(PlatformBean platform) {
                        this.platform = platform;
                    }

                    public RankBean getRank() {
                        return rank;
                    }

                    public void setRank(RankBean rank) {
                        this.rank = rank;
                    }

                    public ZoneBean getZone() {
                        return zone;
                    }

                    public void setZone(ZoneBean zone) {
                        this.zone = zone;
                    }

                    public static class PlatformBean {
                        /**
                         * 1 : 安卓
                         * 2 : IOS
                         */

                        @SerializedName("1")
                        private String _$1;
                        @SerializedName("2")
                        private String _$2;

                        public String get_$1() {
                            return _$1;
                        }

                        public void set_$1(String _$1) {
                            this._$1 = _$1;
                        }

                        public String get_$2() {
                            return _$2;
                        }

                        public void set_$2(String _$2) {
                            this._$2 = _$2;
                        }
                    }

                    public static class RankBean {
                        /**
                         * 0 : 不限
                         * 1 : 热血青铜
                         * 2 : 不屈白银
                         * 3 : 英勇黄金
                         * 4 : 坚韧铂金
                         * 5 : 不朽星钻
                         * 6 : 荣耀皇冠
                         * 7 : 超级王牌
                         * 8 : 无敌战神
                         */

                        @SerializedName("0")
                        private String _$0;
                        @SerializedName("1")
                        private String _$1;
                        @SerializedName("2")
                        private String _$2;
                        @SerializedName("3")
                        private String _$3;
                        @SerializedName("4")
                        private String _$4;
                        @SerializedName("5")
                        private String _$5;
                        @SerializedName("6")
                        private String _$6;
                        @SerializedName("7")
                        private String _$7;
                        @SerializedName("8")
                        private String _$8;

                        public String get_$0() {
                            return _$0;
                        }

                        public void set_$0(String _$0) {
                            this._$0 = _$0;
                        }

                        public String get_$1() {
                            return _$1;
                        }

                        public void set_$1(String _$1) {
                            this._$1 = _$1;
                        }

                        public String get_$2() {
                            return _$2;
                        }

                        public void set_$2(String _$2) {
                            this._$2 = _$2;
                        }

                        public String get_$3() {
                            return _$3;
                        }

                        public void set_$3(String _$3) {
                            this._$3 = _$3;
                        }

                        public String get_$4() {
                            return _$4;
                        }

                        public void set_$4(String _$4) {
                            this._$4 = _$4;
                        }

                        public String get_$5() {
                            return _$5;
                        }

                        public void set_$5(String _$5) {
                            this._$5 = _$5;
                        }

                        public String get_$6() {
                            return _$6;
                        }

                        public void set_$6(String _$6) {
                            this._$6 = _$6;
                        }

                        public String get_$7() {
                            return _$7;
                        }

                        public void set_$7(String _$7) {
                            this._$7 = _$7;
                        }

                        public String get_$8() {
                            return _$8;
                        }

                        public void set_$8(String _$8) {
                            this._$8 = _$8;
                        }
                    }

                    public static class ZoneBean {
                        /**
                         * 1 : QQ
                         * 2 : 微信
                         */

                        @SerializedName("1")
                        private String _$1;
                        @SerializedName("2")
                        private String _$2;

                        public String get_$1() {
                            return _$1;
                        }

                        public void set_$1(String _$1) {
                            this._$1 = _$1;
                        }

                        public String get_$2() {
                            return _$2;
                        }

                        public void set_$2(String _$2) {
                            this._$2 = _$2;
                        }
                    }
                }

                public static class WzryBeanX {
                    /**
                     * platform : {"1":"安卓","2":"IOS"}
                     * rank : {"0":"不限","1":"倔强青铜","2":"秩序白银","3":"荣耀黄金","4":"尊贵铂金","5":"永恒钻石","6":"至尊星耀","7":"最强王者","8":"荣耀王者"}
                     * zone : {"1":"QQ","2":"微信"}
                     */

                    private PlatformBeanX platform;
                    private RankBeanX rank;
                    private ZoneBeanX zone;

                    public PlatformBeanX getPlatform() {
                        return platform;
                    }

                    public void setPlatform(PlatformBeanX platform) {
                        this.platform = platform;
                    }

                    public RankBeanX getRank() {
                        return rank;
                    }

                    public void setRank(RankBeanX rank) {
                        this.rank = rank;
                    }

                    public ZoneBeanX getZone() {
                        return zone;
                    }

                    public void setZone(ZoneBeanX zone) {
                        this.zone = zone;
                    }

                    public static class PlatformBeanX {
                        /**
                         * 1 : 安卓
                         * 2 : IOS
                         */

                        @SerializedName("1")
                        private String _$1;
                        @SerializedName("2")
                        private String _$2;

                        public String get_$1() {
                            return _$1;
                        }

                        public void set_$1(String _$1) {
                            this._$1 = _$1;
                        }

                        public String get_$2() {
                            return _$2;
                        }

                        public void set_$2(String _$2) {
                            this._$2 = _$2;
                        }
                    }

                    public static class RankBeanX {
                        /**
                         * 0 : 不限
                         * 1 : 倔强青铜
                         * 2 : 秩序白银
                         * 3 : 荣耀黄金
                         * 4 : 尊贵铂金
                         * 5 : 永恒钻石
                         * 6 : 至尊星耀
                         * 7 : 最强王者
                         * 8 : 荣耀王者
                         */

                        @SerializedName("0")
                        private String _$0;
                        @SerializedName("1")
                        private String _$1;
                        @SerializedName("2")
                        private String _$2;
                        @SerializedName("3")
                        private String _$3;
                        @SerializedName("4")
                        private String _$4;
                        @SerializedName("5")
                        private String _$5;
                        @SerializedName("6")
                        private String _$6;
                        @SerializedName("7")
                        private String _$7;
                        @SerializedName("8")
                        private String _$8;

                        public String get_$0() {
                            return _$0;
                        }

                        public void set_$0(String _$0) {
                            this._$0 = _$0;
                        }

                        public String get_$1() {
                            return _$1;
                        }

                        public void set_$1(String _$1) {
                            this._$1 = _$1;
                        }

                        public String get_$2() {
                            return _$2;
                        }

                        public void set_$2(String _$2) {
                            this._$2 = _$2;
                        }

                        public String get_$3() {
                            return _$3;
                        }

                        public void set_$3(String _$3) {
                            this._$3 = _$3;
                        }

                        public String get_$4() {
                            return _$4;
                        }

                        public void set_$4(String _$4) {
                            this._$4 = _$4;
                        }

                        public String get_$5() {
                            return _$5;
                        }

                        public void set_$5(String _$5) {
                            this._$5 = _$5;
                        }

                        public String get_$6() {
                            return _$6;
                        }

                        public void set_$6(String _$6) {
                            this._$6 = _$6;
                        }

                        public String get_$7() {
                            return _$7;
                        }

                        public void set_$7(String _$7) {
                            this._$7 = _$7;
                        }

                        public String get_$8() {
                            return _$8;
                        }

                        public void set_$8(String _$8) {
                            this._$8 = _$8;
                        }
                    }

                    public static class ZoneBeanX {
                        /**
                         * 1 : QQ
                         * 2 : 微信
                         */

                        @SerializedName("1")
                        private String _$1;
                        @SerializedName("2")
                        private String _$2;

                        public String get_$1() {
                            return _$1;
                        }

                        public void set_$1(String _$1) {
                            this._$1 = _$1;
                        }

                        public String get_$2() {
                            return _$2;
                        }

                        public void set_$2(String _$2) {
                            this._$2 = _$2;
                        }
                    }
                }
            }

        }


    }

    public static class WZRYDATA {
        private ArrayList<Map<String, String>> wzry_platform_list;
        private ArrayList<Map<String, String>> wzry_rank_list;
        private ArrayList<Map<String, String>> wzry_area_list;
        private String icon;

        public ArrayList<Map<String, String>> getWzry_platform_list() {
            return wzry_platform_list;
        }

        public void setWzry_platform_list(ArrayList<Map<String, String>> wzry_platform_list) {
            this.wzry_platform_list = wzry_platform_list;
        }

        public ArrayList<Map<String, String>> getWzry_rank_list() {
            return wzry_rank_list;
        }

        public void setWzry_rank_list(ArrayList<Map<String, String>> wzry_rank_list) {
            this.wzry_rank_list = wzry_rank_list;
        }

        public ArrayList<Map<String, String>> getWzry_area_list() {
            return wzry_area_list;
        }

        public void setWzry_area_list(ArrayList<Map<String, String>> wzry_area_list) {
            this.wzry_area_list = wzry_area_list;
        }

        public String getIcon() {
            return icon;
        }

        public void setIcon(String icon) {
            this.icon = icon;
        }
    }

    public static class HPJYDATA {
        private ArrayList<Map<String, String>> hpjy_platform_list;
        private ArrayList<Map<String, String>> hpjy_rank_list;
        private ArrayList<Map<String, String>> hpjy_area_list;
        private String icon;

        public ArrayList<Map<String, String>> getHpjy_platform_list() {
            return hpjy_platform_list;
        }

        public void setHpjy_platform_list(ArrayList<Map<String, String>> hpjy_platform_list) {
            this.hpjy_platform_list = hpjy_platform_list;
        }

        public ArrayList<Map<String, String>> getHpjy_rank_list() {
            return hpjy_rank_list;
        }

        public void setHpjy_rank_list(ArrayList<Map<String, String>> hpjy_rank_list) {
            this.hpjy_rank_list = hpjy_rank_list;
        }

        public ArrayList<Map<String, String>> getHpjy_area_list() {
            return hpjy_area_list;
        }

        public void setHpjy_area_list(ArrayList<Map<String, String>> hpjy_area_list) {
            this.hpjy_area_list = hpjy_area_list;
        }

        public String getIcon() {
            return icon;
        }

        public void setIcon(String icon) {
            this.icon = icon;
        }
    }
}
