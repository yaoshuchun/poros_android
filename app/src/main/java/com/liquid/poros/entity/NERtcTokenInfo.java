package com.liquid.poros.entity;

public class NERtcTokenInfo {
    private String im_video_token;// im_video_token,
    private long expire;// 86400 秒

    public String getIm_video_token() {
        return im_video_token;
    }

    public void setIm_video_token(String im_video_token) {
        this.im_video_token = im_video_token;
    }

    public long getExpire() {
        return expire;
    }

    public void setExpire(long expire) {
        this.expire = expire;
    }
}
