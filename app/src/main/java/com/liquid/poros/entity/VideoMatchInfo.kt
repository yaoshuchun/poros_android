package com.liquid.poros.entity

import java.io.Serializable

data class VideoMatchInfo(
        var room_id: String?,
        var hint: String?,
        var im_video_token: String?,
        var live_id: String?,
        var video_room_id: String?,
        var video_match_price: Int,
        var  live_info:LiveFemaleGuestInfo?,
        var  video_match_success_list:List<MatchedInfo>
) :BaseBean(){
    class LiveFemaleGuestInfo(var anchor_id:String?,var avatar_url:String?,var nick_name:String?,var gender:Int?,var age:Int?,var games:List<String>,var image_url:String?): Serializable {

    }
    class MatchedInfo(var male_name:String?,var female_name:String?): Serializable{

    }

}