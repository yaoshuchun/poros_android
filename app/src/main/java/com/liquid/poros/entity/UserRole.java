package com.liquid.poros.entity;

/**
 * Created by sundroid on 10/04/2019.
 */

public class UserRole extends BaseModel{

    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }


    public class Data {
        private String role_color;
        private String role_name;
        private boolean new_user;
        private String avatar_border;

        public String getAvatar_border() {
            return avatar_border;
        }

        public void setAvatar_border(String avatar_border) {
            this.avatar_border = avatar_border;
        }

        public boolean isNew_user() {
            return new_user;
        }

        public void setNew_user(boolean new_user) {
            this.new_user = new_user;
        }

        public String getRole_name() {
            return role_name;
        }

        public void setRole_name(String role_name) {
            this.role_name = role_name;
        }

        public String getRole_color() {
            return role_color;
        }

        public void setRole_color(String role_color) {
            this.role_color = role_color;
        }
    }

}
