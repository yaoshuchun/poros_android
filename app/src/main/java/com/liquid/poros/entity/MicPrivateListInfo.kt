package com.liquid.poros.entity

import java.util.*

/**
 * 专属房申请列表Bean
 */
data class MicPrivateListInfo(
        var count: Int = 0,
        val items: List<Item>? = null
) : BaseBean() {
    class Item {
        var user_id: String? = null
        var nick_name: String? = null
        var avatar_url: String? = null
        var could_invite = false
        var coins: String? = null
        var age: String? = null
        var im_account: String? = null
    }
}
