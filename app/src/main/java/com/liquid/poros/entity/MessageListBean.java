package com.liquid.poros.entity;

import java.util.List;

public class MessageListBean extends BaseBean {

    private List<FriendMessageInfo> session_list;

    private int unread_msg_cnt;

    public int getUnread_msg_cnt() {
        return unread_msg_cnt;
    }

    public void setUnread_msg_cnt(int unread_msg_cnt) {
        this.unread_msg_cnt = unread_msg_cnt;
    }

    public List<FriendMessageInfo> getSession_list() {
        return session_list;
    }

    public void setSession_list(List<FriendMessageInfo> session_list) {
        this.session_list = session_list;
    }
}
