package com.liquid.poros.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MicUserInfo implements Serializable {
    private String user_id;
    private String avatar_url;
    private String nick_name;
    private long start_time;
    private boolean is_friend;
    private boolean use_team_card;
    private String age;
    private List<String> tags;
    private List<String> games;
    private String couple_desc;
    private int gender;//性别 1男 2女
    private boolean camera_close; //是否开启摄像头
    private boolean show_gift_icon;
    private boolean show_add_friend_button;
    private String room_role;
    private boolean is_live_unactive_user;//是否是新用户

    public boolean isIs_live_unactive_user() {
        return is_live_unactive_user;
    }

    public void setIs_live_unactive_user(boolean is_live_unactive_user) {
        this.is_live_unactive_user = is_live_unactive_user;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public List<String> getGames() {
        return games;
    }

    public void setGames(List<String> games) {
        this.games = games;
    }

    public String getCouple_desc() {
        return couple_desc;
    }

    public void setCouple_desc(String couple_desc) {
        this.couple_desc = couple_desc;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public boolean isUse_team_card() {
        return use_team_card;
    }

    public void setUse_team_card(boolean use_team_card) {
        this.use_team_card = use_team_card;
    }

    public boolean isCamera_close() {
        return camera_close;
    }

    public void setCamera_close(boolean camera_close) {
        this.camera_close = camera_close;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getAvatar_url() {
        return avatar_url;
    }

    public void setAvatar_url(String avatar_url) {
        this.avatar_url = avatar_url;
    }

    public String getNick_name() {
        return nick_name;
    }

    public void setNick_name(String nick_name) {
        this.nick_name = nick_name;
    }

    public long getStart_time() {
        return start_time;
    }

    public void setStart_time(long start_time) {
        this.start_time = start_time;
    }

    public boolean isIs_friend() {
        return is_friend;
    }

    public void setIs_friend(boolean is_friend) {
        this.is_friend = is_friend;
    }

    public boolean isShow_gift_icon() {
        return show_gift_icon;
    }

    public void setShow_gift_icon(boolean show_gift_icon) {
        this.show_gift_icon = show_gift_icon;
    }

    public boolean isShow_add_friend_button() {
        return show_add_friend_button;
    }

    public void setShow_add_friend_button(boolean show_add_friend_button) {
        this.show_add_friend_button = show_add_friend_button;
    }

    public String getRoom_role() {
        return room_role;
    }

    public void setRoom_role(String room_role) {
        this.room_role = room_role;
    }

    @Override
    public String toString() {
        return "MicUserInfo{" +
                "user_id='" + user_id + '\'' +
                ", avatar_url='" + avatar_url + '\'' +
                ", nick_name='" + nick_name + '\'' +
                ", start_time=" + start_time +
                ", is_friend=" + is_friend +
                ", use_team_card=" + use_team_card +
                ", age='" + age + '\'' +
                ", tags=" + tags +
                ", games=" + games +
                ", couple_desc='" + couple_desc + '\'' +
                ", gender=" + gender +
                ", camera_close=" + camera_close +
                '}';
    }
}
