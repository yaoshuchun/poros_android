package com.liquid.poros.entity;

import java.util.List;

public class LiveGiftRankInfo extends BaseBean {
    private List<RankInfo> rank_list;
    private SelfRankInfo self_rank_info;

    public List<RankInfo> getRank_list() {
        return rank_list;
    }

    public void setRank_list(List<RankInfo> rank_list) {
        this.rank_list = rank_list;
    }

    public SelfRankInfo getSelf_rank_info() {
        return self_rank_info;
    }

    public void setSelf_rank_info(SelfRankInfo self_rank_info) {
        this.self_rank_info = self_rank_info;
    }

    public class RankInfo {
        private String avatar_url;
        private float coins;
        private List<String> gift_list;
        private String nick_name;

        public String getAvatar_url() {
            return avatar_url;
        }

        public void setAvatar_url(String avatar_url) {
            this.avatar_url = avatar_url;
        }

        public float getCoins() {
            return coins;
        }

        public void setCoins(float coins) {
            this.coins = coins;
        }

        public List<String> getGift_list() {
            return gift_list;
        }

        public void setGift_list(List<String> gift_list) {
            this.gift_list = gift_list;
        }

        public String getNick_name() {
            return nick_name;
        }

        public void setNick_name(String nick_name) {
            this.nick_name = nick_name;
        }
    }

    public class SelfRankInfo {
        private String avatar_url;
        private float coins;
        private String nick_name;
        private int rank;

        public String getAvatar_url() {
            return avatar_url;
        }

        public void setAvatar_url(String avatar_url) {
            this.avatar_url = avatar_url;
        }

        public float getCoins() {
            return coins;
        }

        public void setCoins(float coins) {
            this.coins = coins;
        }

        public String getNick_name() {
            return nick_name;
        }

        public void setNick_name(String nick_name) {
            this.nick_name = nick_name;
        }

        public int getRank() {
            return rank;
        }

        public void setRank(int rank) {
            this.rank = rank;
        }
    }
}
