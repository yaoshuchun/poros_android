package com.liquid.poros.entity

class SoloVideoInfo : BaseBean() {
    /**
     * age : 0
     * available : true
     * user_id : 423847231
     * image_1v1_url :
     * image_url :
     * live_id : null
     * nick_name : 七二三四五六七八九十
     * room_id : 2
     * video_url :
     */
    var age = 0
    var available = false
    var user_id: String? = null
    var image_1v1_url: String? = null
    var image_url: String? = null
    var avatar_url: String? = null
    var live_id: String? = null
    var nick_name: String? = null
    var room_id = 0
    var video_url: String? = null

    var view_type: Int = -1
}