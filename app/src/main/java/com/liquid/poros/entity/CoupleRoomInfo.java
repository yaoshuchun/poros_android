package com.liquid.poros.entity;

import java.util.List;

public class CoupleRoomInfo extends BaseModel {

    private CoupleRoom data;


    public CoupleRoom getData() {
        return data;
    }

    public void setData(CoupleRoom data) {
        this.data = data;
    }

    public class CoupleRoom {
        private MicUserInfo anchor_info;
        private MicUserInfo pos_1_info;
        private MicUserInfo pos_2_info;
        private String im_room_id;
        private String im_video_token;
        private String cid;
        private String http_pull_url;
        private String hls_pull_url;
        private String rtmp_pull_url;
        private String push_url;
        private int gender;
        private String room_desc;
        private MicCard mic_card;
        private boolean is_online;
        private boolean is_couple;
        private String couple_score;
        private String couple_end_time;
        private long coin;
        private boolean is_mic_free;
        private boolean default_camera;
        private boolean is_mic_minute_cost;
        private String mic_price;
        private boolean request_onmic;
        private boolean has_mic_card;
        private boolean show_gift_icon;
        private List<MsgUserModel.GiftBean> gift_list;
        private List<MsgUserModel.GiftNums> gift_num_list;
        private int room_type;
        private String private_room_coin_consume_str;
        private int pk_status;
        private PkInfo pk_info;

        public PkInfo getPk_info() {
            return pk_info;
        }

        public void setPk_info(PkInfo pk_info) {
            this.pk_info = pk_info;
        }

        public int getPk_status() {
            return pk_status;
        }

        public void setPk_status(int pk_status) {
            this.pk_status = pk_status;
        }

        public String getPrivate_room_coin_consume_str() {
            return private_room_coin_consume_str;
        }

        public void setPrivate_room_coin_consume_str(String private_room_coin_consume_str) {
            this.private_room_coin_consume_str = private_room_coin_consume_str;
        }

        public boolean isIs_mic_minute_cost() {
            return is_mic_minute_cost;
        }

        public void setIs_mic_minute_cost(boolean is_mic_minute_cost) {
            this.is_mic_minute_cost = is_mic_minute_cost;
        }

        public String getMic_price() {
            return mic_price;
        }

        public void setMic_price(String mic_price) {
            this.mic_price = mic_price;
        }

        public boolean isDefault_camera() {
            return default_camera;
        }

        public void setDefault_camera(boolean default_camera) {
            this.default_camera = default_camera;
        }

        public String getIm_video_token() {
            return im_video_token;
        }

        public void setIm_video_token(String im_video_token) {
            this.im_video_token = im_video_token;
        }

        public boolean isIs_online() {
            return is_online;
        }

        public void setIs_online(boolean is_online) {
            this.is_online = is_online;
        }

        public String getRoom_desc() {
            return room_desc;
        }

        public void setRoom_desc(String room_desc) {
            this.room_desc = room_desc;
        }

        public MicUserInfo getAnchor_info() {
            return anchor_info;
        }

        public void setAnchor_info(MicUserInfo anchor_info) {
            this.anchor_info = anchor_info;
        }

        public MicUserInfo getPos_1_info() {
            return pos_1_info;
        }

        public void setPos_1_info(MicUserInfo pos_1_info) {
            this.pos_1_info = pos_1_info;
        }

        public MicUserInfo getPos_2_info() {
            return pos_2_info;
        }

        public void setPos_2_info(MicUserInfo pos_2_info) {
            this.pos_2_info = pos_2_info;
        }

        public String getIm_room_id() {
            return im_room_id;
        }

        public void setIm_room_id(String im_room_id) {
            this.im_room_id = im_room_id;
        }

        public String getCid() {
            return cid;
        }

        public void setCid(String cid) {
            this.cid = cid;
        }

        public String getHttp_pull_url() {
            return http_pull_url;
        }

        public void setHttp_pull_url(String http_pull_url) {
            this.http_pull_url = http_pull_url;
        }

        public String getHls_pull_url() {
            return hls_pull_url;
        }

        public void setHls_pull_url(String hls_pull_url) {
            this.hls_pull_url = hls_pull_url;
        }

        public String getRtmp_pull_url() {
            return rtmp_pull_url;
        }

        public void setRtmp_pull_url(String rtmp_pull_url) {
            this.rtmp_pull_url = rtmp_pull_url;
        }

        public String getPush_url() {
            return push_url;
        }

        public void setPush_url(String push_url) {
            this.push_url = push_url;
        }

        public int getGender() {
            return gender;
        }

        public void setGender(int gender) {
            this.gender = gender;
        }

        public MicCard getMic_card() {
            return mic_card;
        }

        public void setMic_card(MicCard mic_card) {
            this.mic_card = mic_card;
        }

        public boolean isIs_couple() {
            return is_couple;
        }

        public void setIs_couple(boolean is_couple) {
            this.is_couple = is_couple;
        }

        public String getCouple_score() {
            return couple_score;
        }

        public void setCouple_score(String couple_score) {
            this.couple_score = couple_score;
        }

        public String getCouple_end_time() {
            return couple_end_time;
        }

        public void setCouple_end_time(String couple_end_time) {
            this.couple_end_time = couple_end_time;
        }

        public long getCoin() {
            return coin;
        }

        public void setCoin(long coin) {
            this.coin = coin;
        }

        public boolean isIs_mic_free() {
            return is_mic_free;
        }

        public void setIs_mic_free(boolean is_mic_free) {
            this.is_mic_free = is_mic_free;
        }

        public boolean isRequest_onmic() {
            return request_onmic;
        }

        public void setRequest_onmic(boolean request_onmic) {
            this.request_onmic = request_onmic;
        }

        public boolean isHas_mic_card() {
            return has_mic_card;
        }

        public void setHas_mic_card(boolean has_mic_card) {
            this.has_mic_card = has_mic_card;
        }

        public boolean isShow_gift_icon() {
            return show_gift_icon;
        }

        public void setShow_gift_icon(boolean show_gift_icon) {
            this.show_gift_icon = show_gift_icon;
        }

        public List<MsgUserModel.GiftBean> getGift_list() {
            return gift_list;
        }

        public void setGift_list(List<MsgUserModel.GiftBean> gift_list) {
            this.gift_list = gift_list;
        }

        public List<MsgUserModel.GiftNums> getGift_num_list() {
            return gift_num_list;
        }

        public void setGift_num_list(List<MsgUserModel.GiftNums> gift_num_list) {
            this.gift_num_list = gift_num_list;
        }

        public int getRoom_type() {
            return room_type;
        }

        public void setRoom_type(int room_type) {
            this.room_type = room_type;
        }
    }

    public class MicCard {
        private String desc;
        private String continue_desc;
        private int real_price;
        private int ori_price;

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public String getContinue_desc() {
            return continue_desc;
        }

        public void setContinue_desc(String continue_desc) {
            this.continue_desc = continue_desc;
        }

        public int getReal_price() {
            return real_price;
        }

        public void setReal_price(int real_price) {
            this.real_price = real_price;
        }

        public int getOri_price() {
            return ori_price;
        }

        public void setOri_price(int ori_price) {
            this.ori_price = ori_price;
        }
    }

    public static class PkInfo {
        private long pos_1_pk_score;
        private long pos_2_pk_score;
        private List<String> pos_1_rank_top_avatars;
        private List<String> pos_2_rank_top_avatars;

        public long getPos_1_pk_score() {
            return pos_1_pk_score;
        }

        public void setPos_1_pk_score(long pos_1_pk_score) {
            this.pos_1_pk_score = pos_1_pk_score;
        }

        public long getPos_2_pk_score() {
            return pos_2_pk_score;
        }

        public void setPos_2_pk_score(long pos_2_pk_score) {
            this.pos_2_pk_score = pos_2_pk_score;
        }

        public List<String> getPos_1_rank_top_avatars() {
            return pos_1_rank_top_avatars;
        }

        public void setPos_1_rank_top_avatars(List<String> pos_1_rank_top_avatars) {
            this.pos_1_rank_top_avatars = pos_1_rank_top_avatars;
        }

        public List<String> getPos_2_rank_top_avatars() {
            return pos_2_rank_top_avatars;
        }

        public void setPos_2_rank_top_avatars(List<String> pos_2_rank_top_avatars) {
            this.pos_2_rank_top_avatars = pos_2_rank_top_avatars;
        }
    }

}
