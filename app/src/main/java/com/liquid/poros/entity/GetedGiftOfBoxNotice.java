package com.liquid.poros.entity;

/**
 * 中奖通告
 */
public class GetedGiftOfBoxNotice {
    public String record_id;// 记录id
    public String gift_image;// 奖品图片
    public String gift_name;// 奖品名称
    public String nick_name;// 昵称
    public String desc;// 整段描述
}
