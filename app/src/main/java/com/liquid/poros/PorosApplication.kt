package com.liquid.poros

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Application
import android.content.Context
import android.os.Process
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStore
import androidx.lifecycle.ViewModelStoreOwner
import com.alibaba.android.arouter.launcher.ARouter
import com.faceunity.nama.fromapp.entity.BeautyParameterModel
import com.faceunity.nama.utils.PreferenceUtil
import com.igexin.sdk.PushManager
import com.liquid.base.event.EventCenter
import com.liquid.base.utils.ContextUtils
import com.liquid.base.utils.GlobalConfig
import com.liquid.base.utils.LogOut
import com.liquid.im.kit.custom.CustomAttachParser
import com.liquid.poros.ad.TTAdManagerHolder
import com.liquid.poros.analytics.LogAnalyticsManager
import com.liquid.poros.analytics.utils.MultiTracker
import com.liquid.poros.base.ThemeChangeObserver
import com.liquid.poros.constant.Constants
import com.liquid.poros.helper.AppFrontBackHelper
import com.liquid.poros.helper.AppFrontBackHelper.OnAppStatusListener
import com.liquid.poros.helper.provider.DefaultUserInfoProvider
import com.liquid.poros.helper.user.IUserInfoProvider
import com.liquid.poros.http.RetrofitLoader.initRetrofitX
import com.liquid.poros.sdk.NimPlayUtils
import com.liquid.poros.service.JQIntentService
import com.liquid.poros.service.JQPushService
import com.liquid.poros.utils.AccountUtil
import com.liquid.poros.utils.IMUtil
import com.liquid.porostwo.ability.ConnectionStateMonitor
import com.liulishuo.filedownloader.FileDownloader
import com.netease.nimlib.sdk.NIMClient
import com.netease.nimlib.sdk.msg.MsgService
import com.netease.nimlib.sdk.util.NIMUtil
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.Logger
import de.greenrobot.event.EventBus
import java.util.*
import kotlin.concurrent.thread

class PorosApplication : Application(), ViewModelStoreOwner {
    private var mAppFrontBackHelper: AppFrontBackHelper? = null
    private var mThemeChangeObserverStack //  主题切换监听栈
            : MutableList<ThemeChangeObserver>? = null

    private var mAppViewModelStore: ViewModelStore? = null
    private var mViewModelProvider: ViewModelProvider? = null

    // 初始化用户信息提供者
    var userInfoProvider: IUserInfoProvider<*>? = null
        get() {
            if (field == null) {
                field = DefaultUserInfoProvider()
            }
            return field
        }
        private set

    override fun onCreate() {
        super.onCreate()
        context = this
        instance = this
        //创建全局ViewModelStore 用于跨Activity发送数据
        mAppViewModelStore = ViewModelStore()
        NIMClient.init(this, IMUtil.loginInfo(), IMUtil.getOptions(this@PorosApplication))
        if (NIMUtil.isMainProcess(this)) {
            //初始化网络状态指示器
            ConnectionStateMonitor().enable(this)
            if (BuildConfig.DEBUG) {  // These two lines must be written before init, otherwise these configurations will be invalid in the init process
                ARouter.openLog() // Print log
                ARouter.openDebug() // Turn on debugging mode (If you are running in InstantRun mode, you must turn on debug mode! Online version needs to be closed, otherwise there is a security risk)
            }
            ARouter.init(instance)
            initLogUtils()
            ContextUtils.init(this)
            GlobalConfig.instance().init(this)
            NimClientInit()
            // 个推初始化
            PushManager.getInstance().initialize(applicationContext, JQPushService::class.java)
            PushManager.getInstance().registerPushIntentService(
                applicationContext, JQIntentService::class.java
            )
            registerActivityLifecycle()
            initRetrofitX(instance)

            //穿山甲SDK初始化
            TTAdManagerHolder.init(context)
            initThirdService()
            try {
                PreferenceUtil.init(this)
                BeautyParameterModel.init()
            } catch (th: Throwable) {
            }
        }
    }

    /**
     * 初始化LogOut
     */
    private fun initLogUtils() {
        Logger.addLogAdapter(object : AndroidLogAdapter() {
            override fun isLoggable(priority: Int, tag: String?): Boolean {
                return BuildConfig.DEBUG
            }
        })
        LogOut.setDebug(BuildConfig.DEBUG)
    }

    /**
     * 网易云信IM初始化
     */
    private fun NimClientInit() {
        NIMClient.getService(MsgService::class.java)
            .registerCustomAttachmentParser(CustomAttachParser())
        NIMClient.toggleNotification(false)
        NimPlayUtils.init() //网易云信播放器初始化,耗时70ms
    }

    /**
     * 异步初始化部分服务
     */
    fun initThirdService() {
        //设置线程优先级，不与主线程抢占资源
        thread(priority = Process.THREAD_PRIORITY_BACKGROUND) {
            //埋点初始化,放在最前面执行
            //EmojiManager.install(new IosEmojiProvider());
            LogAnalyticsManager.init()
            FileDownloader.setupOnApplicationOnCreate(instance)
        }
    }

    private fun registerActivityLifecycle() {
        mAppFrontBackHelper = AppFrontBackHelper()
        mAppFrontBackHelper!!.register(this@PorosApplication, object : OnAppStatusListener {
            override fun onSwitchFront() {
                EventBus.getDefault()
                    .post(EventCenter<Any?>(Constants.EVENT_CODE_LEAVE_AVCHAT_ROOM))
            }

            override fun onSwitchBack() {}
            override fun onActivityResume(activity: Activity) {}

        })
    }

    /**
     * 获得observer堆栈
     */
    private fun obtainThemeChangeObserverStack(): MutableList<ThemeChangeObserver> {
        if (mThemeChangeObserverStack == null) mThemeChangeObserverStack = ArrayList()
        return mThemeChangeObserverStack!!
    }

    /**
     * 向堆栈中添加observer
     */
    fun registerObserver(observer: ThemeChangeObserver?) {
        if (observer == null || obtainThemeChangeObserverStack().contains(observer)) return
        obtainThemeChangeObserverStack().add(observer)
    }

    /**
     * 从堆栈中移除observer
     */
    fun unregisterObserver(observer: ThemeChangeObserver?) {
        if (observer == null || !obtainThemeChangeObserverStack().contains(observer)) return
        obtainThemeChangeObserverStack().remove(observer)
    }

    /**
     * 向堆栈中所有对象发送更新UI的指令
     */
    fun notifyByThemeChanged() {
        val observers: List<ThemeChangeObserver> = obtainThemeChangeObserverStack()
        for (observer in observers) {
            observer.loadingCurrentTheme() //
            observer.notifyByThemeChanged() //
        }
    }

    /**
     * 向堆栈中所有对象发送更新UI的指令
     */
    fun notifyNetworkChanged(isConnected: Boolean) {
        val observers: List<ThemeChangeObserver> = obtainThemeChangeObserverStack()
        for (observer in observers) {
            observer.notifyNetworkChanged(isConnected)
        }
    }

    override fun onLowMemory() {
        super.onLowMemory()
        val params = HashMap<String?, String?>()
        params["user_id"] = AccountUtil.getInstance().userId
        params["event_time"] = System.currentTimeMillis().toString()
        params["tag"] = "connect_mic_ques"
        MultiTracker.onEvent("on_low_memory", params)
    }

    companion object {
        @SuppressLint("StaticFieldLeak")
        lateinit var context: Context

        @SuppressLint("StaticFieldLeak")
        lateinit var instance: PorosApplication
    }

    override fun getViewModelStore(): ViewModelStore {
        return mAppViewModelStore!!
    }

    fun <T : ViewModel?> getViewModel(viewModel: Class<T>): T {
        if (mViewModelProvider == null) {
            mViewModelProvider =
                ViewModelProvider(this, ViewModelProvider.AndroidViewModelFactory.getInstance(this))
        }
        return mViewModelProvider!!.get(viewModel)
    }
}