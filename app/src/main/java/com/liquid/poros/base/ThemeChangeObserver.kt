package com.liquid.poros.base

interface ThemeChangeObserver {
    fun loadingCurrentTheme()
    fun notifyByThemeChanged()

    @Deprecated("重构时，改用ApplicationViewModel的netWorkIsConnected来代替")
    fun notifyNetworkChanged(isConnected: Boolean)
}