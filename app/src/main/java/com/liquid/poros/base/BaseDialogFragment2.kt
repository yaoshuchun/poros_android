package com.liquid.poros.base

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.Rect
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.TextUtils
import android.view.*

import androidx.annotation.Nullable
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.liquid.poros.R

abstract class BaseDialogFragment2 : DialogFragment(){

    private var mDialogFragmentProvider: ViewModelProvider? = null
    var mContext: Context? = null
    var mActivity: Activity? = null
    var rootView: View? = null
    private var onLifeCycleListener: OnLifeCycleListener? = null
    /*fun <T> setData(data: T) {
        val bundle = Bundle()
        bundle.putSerializable(IntentExtras.ARGUMENT_DATA, data as Serializable)
        arguments = bundle
    }

    fun <T> getData(t: Class<T>?): T? {
        return if (null != arguments && null != arguments!!.getSerializable(IntentExtras.ARGUMENT_DATA) &&
                arguments!!.getSerializable(IntentExtras.ARGUMENT_DATA).getClass().equals(t)) {
            arguments!!.getSerializable(IntentExtras.ARGUMENT_DATA) as T
        } else null
    }*/
    fun <V : ViewModel>  getViewModel(modelClass: Class<V>): V {
        if (null == mDialogFragmentProvider){
            mDialogFragmentProvider = ViewModelProvider (this)
        }
        return mDialogFragmentProvider!!.get(modelClass)
    }

    private var onDismissListener: DialogInterface.OnDismissListener? = null

    fun setOnDismissListener(listener: DialogInterface.OnDismissListener?) {
        onDismissListener = listener
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
        if (mContext is Activity) {
            mActivity = mContext as Activity
        }
    }



    @Nullable
    override fun onCreateView(inflater: LayoutInflater, @Nullable container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        rootView = if (null != getContentView()) {
            getContentView()
        } else {
            inflater.inflate(contentXmlId(), container, false)
        }
        initView()
        initListener()
        return rootView
    }

    override fun onStart() {
        super.onStart()
        try {
            if (dimEnable()) {
                if (getDimAmount() > 0f && getDimAmount() <= 1f){
                    val lp = dialog!!.window.attributes
                    lp.dimAmount = getDimAmount()
                    dialog!!.window.attributes = lp
                }
            } else {
                dialog!!.window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)
            }

            val rectangle = Rect()
            dialog!!.window.decorView.getWindowVisibleDisplayFrame(rectangle)
            dialog!!.window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, rectangle.bottom - rectangle.top)

            //dialog!!.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog!!.window.setGravity(getGravity())
        } catch (e: Exception) {
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //弹窗本身根View的背景 设置透明背景
        dialog!!.window.findViewById<View>(R.id.design_bottom_sheet)?.setBackgroundResource(android.R.color.transparent)
        if (null != onLifeCycleListener) {
            onLifeCycleListener!!.onActivityCreated()
        }
        initData()
    }

    open fun initData() {


    }

    protected abstract fun contentXmlId(): Int
    protected abstract fun initView()

    open fun initListener() {
        rootView!!.setOnClickListener { dismiss() }
    }

    open fun getContentView(): View? {
        return null
    }

    fun onClick(v: View?) {}


    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        if (onDismissListener != null) {
            onDismissListener!!.onDismiss(dialog)
        }
    }

    open fun getGravity(): Int {
        return Gravity.CENTER
    }

    open fun getDimAmount(): Float {
        return 0f
    }

    open fun dimEnable(): Boolean {
        return true
    }

    override fun show(manager: FragmentManager, tag: String?) {
        var tag = tag
        if (TextUtils.isEmpty(tag)) tag = this.javaClass.simpleName
        showAllowingStateLoss(manager, tag)
    }

    fun show(manager: FragmentManager) {
        try {
            show(manager,null)
        }catch (e :Exception){
            e.printStackTrace()
        }
    }


    private fun showAllowingStateLoss(manager: FragmentManager?, tag: String?) {
        if (null == manager) return
        try {
            //先remove
            manager.beginTransaction().remove(this).commitAllowingStateLoss()
        } catch (e: Exception) {
            //同一实例使用不同的tag会异常,这里捕获一下
            e.printStackTrace()
        }
        manager.beginTransaction().add(this, tag).commitAllowingStateLoss()
    }

    override fun dismissAllowingStateLoss() {
        if (isAdded) {
            super.dismissAllowingStateLoss()
        }
    }

    override fun dismiss() {
        dismissAllowingStateLoss()
    }


    fun setOnLifeCycleListener(onLifeCycleListener: OnLifeCycleListener?) {
        this.onLifeCycleListener = onLifeCycleListener
    }

    interface OnLifeCycleListener {
        fun onActivityCreated()
    }

}



