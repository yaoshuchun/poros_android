package com.liquid.poros.base;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.liquid.base.event.EventCenter;
import com.liquid.base.mvp.MvpActivity;
import com.liquid.base.tools.StringUtil;
import com.liquid.base.utils.LogOut;
import com.liquid.poros.PorosApplication;
import com.liquid.poros.R;
import com.liquid.poros.analytics.utils.MultiTracker;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.entity.PlayloadInfo;
import com.liquid.poros.ui.activity.LoginActivity;
import com.liquid.poros.ui.activity.WebViewActivity;
import com.liquid.poros.ui.activity.audiomatch.CPMatchingGlobal;
import com.liquid.poros.ui.floatball.FloatManager;
import com.liquid.poros.ui.floatball.GiftPackageFloatManager;
import com.liquid.poros.ui.fragment.dialog.common.CommonActionListener;
import com.liquid.poros.ui.fragment.dialog.common.CommonDialogBuilder;
import com.liquid.poros.ui.fragment.dialog.common.CommonDialogFragment;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.poros.utils.KeyboardHelper;
import com.liquid.poros.utils.SharePreferenceUtil;
import com.liquid.poros.utils.StatusBarUtil;
import com.liquid.poros.utils.network.HttpCallback;
import com.liquid.poros.utils.network.RetrofitHttpManager;
import com.liquid.poros.utils.push.PushNotifyUtils;
import com.liquid.poros.widgets.Poast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import okhttp3.RequestBody;

/**
 * Created by sundroid on 12/03/2019.
 */

public abstract class BaseActivity extends MvpActivity implements ThemeChangeObserver {
    protected static String TAG = "JINJ";
    private long startTime = 0;
    protected long duration;
    private HashMap<String, String> params = null;
    public String pageId = "";//页面唯一标识,可用做上报事件打点的event_id
    public static final int BASIC_PERMISSION_REQUEST_CODE = 0x100;
    private ViewModelProvider mActivityProvider;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setupActivityBeforeCreate();
        super.onCreate(savedInstanceState);
        if (needSetTransparent()) {
            StatusBarUtil.setTransparentForWindow(this);
            if (getThemeTag() == 1) {
                StatusBarUtil.setDarkMode(this);
            } else {
                StatusBarUtil.setLightMode(this);
            }
        } else {
            setStatusBar(getStatusBarColor());
        }
        pageId = getPageId();
        params = getParams();
    }

    protected boolean needSetTransparent() {
        return true;
    }

    //提现埋点
    protected String trackWithDrawType() {
        return "";
    }

    protected boolean needSetTheme() {
        return true;
    }

    protected abstract String getPageId();

    protected HashMap<String, String> getParams() {
        return null;
    }

    @Override
    protected void onEventComing(EventCenter eventCenter) {
        if (this instanceof WebViewActivity || this instanceof LoginActivity) {
            return;
        }
        switch (eventCenter.getEventCode()) {
            case Constants.EVENT_CODE_TOAST_MSG:
                if (isShowMsgTip()) {
                    PlayloadInfo info = (PlayloadInfo) eventCenter.getData();
                    if (!isCurrent(info)) {
                        if (info.isTeam_notice()) {
                            HashMap<String, String> map = new HashMap<>();
                            map.put("user_id", AccountUtil.getInstance().getUserId());
                            map.put("female_guest_id", info.getAccount());
                            MultiTracker.onEvent("b_invite_mic_msg", map);
                            Poast.makeText(this, info, Poast.LENGTH_LONG).show();
                        } else {
                            Poast.makeText(this, info, Poast.LENGTH_SHORT).show();
                        }
                    }
                    PushNotifyUtils.getInstance().sentGeTuiNotify(info);
                }
                break;
        }
    }

    protected boolean isCurrent(PlayloadInfo info) {
        return false;
    }

    @Override
    protected int getStatusBarColor() {
        return getThemeTag() == 1 ? Color.WHITE : getResources().getColor(R.color.custom_color_app_bg_night);
    }

    /**
     *
     */
    private void setupActivityBeforeCreate() {
        ((PorosApplication) getApplication()).registerObserver(this);
        loadingCurrentTheme();
    }

    protected boolean isShowMsgTip() {
        return true;
    }

    protected int getThemeTag() {
        return SharePreferenceUtil.getInt(SharePreferenceUtil.FILE_USER_ACCOUNT_DATA, Constants.KEY_CACHE_THEME_TAG, 1);
    }

    @Override
    public void loadingCurrentTheme() {
        switch (getThemeTag()) {
            case 1:
                if (needSetTheme()) {
                    setTheme(R.style.JinquanTheme_Day);
                    StatusBarUtil.setDarkMode(this);
                } else {
                    setTheme(R.style.Welcome_Theme);
                }
                break;
            case -1:
                if (needSetTheme()) {
                    setTheme(R.style.JinquanTheme_Night);
                    StatusBarUtil.setLightMode(this);
                } else {
                    setTheme(R.style.Welcome_Theme_night);
                }
                break;
        }
    }

    @Override
    public void notifyByThemeChanged() {

    }

    @Override
    protected Intent getGoIntent(Class<?> clazz) {
        return super.getGoIntent(clazz);
    }


    private void enterPage() {
        startTime = System.currentTimeMillis();
        LogOut.debug("AppBoxBaseActivity", "enterPage:");
        HashMap<String, String> map = new HashMap<>();
        if (StringUtil.isNotEmpty(trackWithDrawType())){
            map.put("page_type",trackWithDrawType());
        }
        if (params != null) {
            map.putAll(params);
        }
        MultiTracker.onPageResume(pageId, map);
        LogOut.debug("AppBoxBaseActivity enterPage params", map.toString());
    }

    private void leavePage() {
        duration = System.currentTimeMillis() - startTime;
        LogOut.debug("AppBoxBaseActivity", "leavePage:" + pageId + "===duration==" + duration);
        HashMap<String, String> map = new HashMap<>();
        map.put("duration", String.valueOf(duration));
        if (StringUtil.isNotEmpty(trackWithDrawType())){
            map.put("page_type",trackWithDrawType());
        }
        if (params != null) {
            map.putAll(params);
        }
        MultiTracker.onPagePause(pageId, map);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!TextUtils.isEmpty(pageId)) {
            AccountUtil.getInstance().setPageId(pageId);
            enterPage();
        }
        //每次页面重新展示时需要切换当前速配页面的Activity
        getHandler().postDelayed(() -> CPMatchingGlobal.getInstance(this).showMatchingBall(),100);
        getHandler().postDelayed(() -> GiftPackageFloatManager.getInstance().showFloat(this),100);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (!TextUtils.isEmpty(pageId)) {
            leavePage();
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        ((PorosApplication) getApplication()).unregisterObserver(this);
    }


    @Override
    public Resources getResources() {
        Resources res = super.getResources();
        Configuration config = res.getConfiguration();
        config.fontScale = 1;
        res.updateConfiguration(config, res.getDisplayMetrics());
        return res;
    }

    /**
     * 处理焦点在软键盘外事件
     *
     * @param ev
     * @return
     */
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if ((ev.getAction() == MotionEvent.ACTION_DOWN || ev.getAction() == KeyEvent.KEYCODE_DPAD_DOWN) && getCurrentFocus() != null &&
                getCurrentFocus().getWindowToken() != null) {
            // 获得当前得到焦点的View
            View v = getCurrentFocus();
            if (isShouldHideInput(v, ev)) {
                KeyboardHelper.hintKeyBoard(BaseActivity.this);
            }
            hideEmoji(ev, v);
        }
        return super.dispatchTouchEvent(ev);
    }

    public <V extends ViewModel> V getActivityViewModel(@NonNull Class<V> modelClass) {
        if (mActivityProvider == null) {
            mActivityProvider = new ViewModelProvider(this);
        }
        return mActivityProvider.get(modelClass);
    }

    /**
     * 根据EditText所在坐标和用户点击的坐标相对比，来判断是否隐藏键盘
     *
     * @param v
     * @param event
     * @return
     */
    private boolean isShouldHideInput(View v, MotionEvent event) {
        if (v != null && (v instanceof EditText || v instanceof TextView)) {
            int[] l = {0, 0};
            v.getLocationInWindow(l);
            int top = l[1];
            if (event.getY() > top) {
                return false;
            } else {
                return true;
            }
        }
        return false;
    }

    public void hideEmoji(MotionEvent event, View view) {

    }

    @Override
    public void notifyNetworkChanged(boolean isConnected) {

    }

}
