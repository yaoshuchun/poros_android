package com.liquid.poros.base;

import com.liquid.base.mvp.BaseContract;

import java.lang.ref.Reference;
import java.lang.ref.WeakReference;

public class BPresenter<V extends BaseContract.BaseView> implements BaseContract.BasePresenter<V> {
    protected Reference<V> mRefView;


    protected V getView() {
        return mRefView.get();
    }

    protected boolean isViewAttached() {
        return mRefView != null && mRefView.get() != null;
    }

    @Override
    public void attachView(V view) {
        mRefView = new WeakReference<>(view);
    }

    @Override
    public void detachView() {
        if (mRefView != null) {
            mRefView.clear();
            mRefView = null;
        }
    }
}
