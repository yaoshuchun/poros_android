package com.liquid.poros.base

import android.content.Context
import android.view.View
import androidx.lifecycle.*
import razerdp.basepopup.BasePopupWindow

abstract class BasePopupVMDialog(context: Context) : BasePopupWindow(context), LifecycleOwner,
    ViewModelStoreOwner {
    private var mViewModelStore: ViewModelStore? = null

    private var mLifecycleRegistry: LifecycleRegistry? = null
    override fun getLifecycle(): Lifecycle {
        if (mLifecycleRegistry == null) {
            mLifecycleRegistry = LifecycleRegistry(this@BasePopupVMDialog)
        }
        return mLifecycleRegistry!!
    }

    override fun onViewCreated(contentView: View) {
        mLifecycleRegistry?.currentState = Lifecycle.State.CREATED
        super.onViewCreated(contentView)
    }

    override fun getViewModelStore(): ViewModelStore {
        if (mViewModelStore == null) {
            mViewModelStore = ViewModelStore()
        }
        return mViewModelStore!!
    }

    override fun onDismiss() {
        super.onDismiss()
        mLifecycleRegistry?.currentState = Lifecycle.State.DESTROYED
    }

    fun <T : ViewModel?> getViewModel(viewModel: Class<T>): T {
        return ViewModelProvider.NewInstanceFactory().create(viewModel)
    }

    override fun onPreShow(): Boolean {
        mLifecycleRegistry?.currentState = Lifecycle.State.STARTED
        return super.onPreShow()
    }

    override fun onShowing() {
        mLifecycleRegistry?.currentState = Lifecycle.State.RESUMED
        super.onShowing()
    }

}