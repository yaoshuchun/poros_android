package com.liquid.poros.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.liquid.library.retrofitx.RetrofitX
import com.liquid.poros.entity.AppBaseConfigBean
import com.liquid.poros.entity.BaseBean
import com.liquid.poros.http.ApiUrl
import com.liquid.poros.http.observer.ObjectObserver
import com.liquid.poros.http.utils.postPoros

/**
 *
 * app的统一配置获取重构
 * Created by yejiurui on 2021/1/13.
 * Mail:yejiurui@liquidnetwork.com
 */
class AppConfigViewModel : ViewModel() {
    val appConfigDataSuccess: MutableLiveData<AppBaseConfigBean> by lazy { MutableLiveData<AppBaseConfigBean>() }
    val appConfigDataFail: MutableLiveData<AppBaseConfigBean> by lazy { MutableLiveData<AppBaseConfigBean>() }

    /**
     * 获取APP的全局变量信息
     */
    fun getAppConfig() {
        RetrofitX.url(ApiUrl.APP_COFIG_URL)
                .postPoros()
                .subscribe(object : ObjectObserver<AppBaseConfigBean>() {
                    override fun success(result: AppBaseConfigBean) {
                        appConfigDataSuccess.postValue(result)
                    }

                    override fun failed(result: AppBaseConfigBean) {
                        appConfigDataFail.postValue(result)
                    }
                })
    }
}