package com.liquid.poros.base;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.liquid.poros.analytics.utils.MultiTracker;
import com.liquid.base.utils.LogOut;

import java.util.HashMap;

public abstract class BaseDialogFragment extends DialogFragment {

    public String pageId = "";//页面唯一标识,可用做上报事件打点的event_id
    private long startTime = 0;
    private boolean isShowFragment = false;
    private ViewModelProvider mDialogFragmentProvider;
    public BaseDialogFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pageId = getPageId();
    }

    protected abstract String getPageId();

    protected HashMap<String, String> getParams() {
        return null;
    }

    protected <V extends ViewModel> V getDialogFragmentViewModel(@NonNull Class<V> modelClass) {
        if (mDialogFragmentProvider == null) {
            mDialogFragmentProvider = new ViewModelProvider(this);
        }
        return mDialogFragmentProvider.get(modelClass);
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        try {
            if (isShowFragment) {
                return;
            }
            isShowFragment = true;
            super.show(manager, tag);
        } catch (Exception e) {

        }
    }

    protected void hintKeyBoard(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm.isActive(view)) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), WindowManager.LayoutParams.SOFT_INPUT_STATE_UNSPECIFIED);
        }
    }

    /**
     * 注意,不要用super.dismiss(),bug 同上show()
     * super.onDismiss就没问题
     */
    public void dismissDialog() {
        if (getActivity() != null && !getActivity().isFinishing()) {
            super.dismissAllowingStateLoss();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getUserVisibleHint() && !TextUtils.isEmpty(pageId)) {
            enterPage();
        }
    }

    @Override
    public void dismiss() {
        super.dismiss();
        isShowFragment = false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        isShowFragment = false;
    }

    @Override
    public void onPause() {
        super.onPause();
        if (getUserVisibleHint() && !TextUtils.isEmpty(pageId)) {
            leavePage();
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisible) {
        super.setUserVisibleHint(isVisible);
        if (!TextUtils.isEmpty(pageId)) {
            if (isVisible) {
                enterPage();
            } else {
                leavePage();
            }
        }
    }

    private void enterPage() {
        startTime = System.currentTimeMillis();
        LogOut.debug("BaseFragment", "enterPage:" + pageId);
        HashMap<String, String> map = new HashMap<>();
        if (getParams() != null) {
            map.putAll(getParams());
        }
        MultiTracker.onPageResume(pageId, map);
    }

    private void leavePage() {
        long duration = System.currentTimeMillis() - startTime;
        LogOut.debug("BaseFragment", "leavePage:" + pageId + "===duration==" + duration);
        HashMap<String, String> map = new HashMap<>();
        if (getParams() != null) {
            map.putAll(getParams());
        }
        map.put("duration", String.valueOf(duration));
        MultiTracker.onPagePause(pageId, map);
    }

}
