package com.liquid.poros.base;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.liquid.base.event.EventCenter;
import com.liquid.base.mvp.MvpFragment;
import com.liquid.base.utils.LogOut;
import com.liquid.poros.PorosApplication;
import com.liquid.poros.R;
import com.liquid.poros.analytics.utils.MultiTracker;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.poros.utils.SharePreferenceUtil;

import java.util.HashMap;

/**
 * Created by sundroid on 12/03/2019.
 */

public abstract class BaseFragment extends MvpFragment implements ThemeChangeObserver {
    protected static String TAG = "BaseActivity";

    protected AppCompatActivity mActivity;
    private ViewModelProvider mFragmentProvider;
    private ViewModelProvider mActivityProvider;

    @Override
    protected void onEventComing(EventCenter eventCenter) {
    }

    @Override
    protected Intent getGoIntent(Class<?> clazz) {
        return super.getGoIntent(clazz);
    }

    public String pageId = "";//页面唯一标识,可用做上报事件打点的event_id

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupActivityBeforeCreate();
        pageId = getPageId();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mActivity = (AppCompatActivity) context;
    }

    /**
     *
     */
    private void setupActivityBeforeCreate() {
        ((PorosApplication) getActivity().getApplication()).registerObserver(this);
        loadingCurrentTheme();
    }

    @Override
    public void loadingCurrentTheme() {
        switch (getThemeTag()) {
            case 1:
                getActivity().setTheme(R.style.JinquanTheme_Day);
                break;
            case -1:
                getActivity().setTheme(R.style.JinquanTheme_Night);
                break;
        }
    }

    /**
     *
     */
    protected int getThemeTag() {
        return SharePreferenceUtil.getInt(SharePreferenceUtil.FILE_USER_ACCOUNT_DATA, Constants.KEY_CACHE_THEME_TAG, 1);
    }

    /**
     *
     */
    protected void setThemeTag(int tag) {
        SharePreferenceUtil.saveInt(SharePreferenceUtil.FILE_USER_ACCOUNT_DATA, Constants.KEY_CACHE_THEME_TAG, tag);
    }

    protected abstract String getPageId();

    protected HashMap<String, String> getParams() {
        return null;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getUserVisibleHint() && !TextUtils.isEmpty(pageId)) {
            AccountUtil.getInstance().setPageId(pageId);
            enterPage();
        }
    }


    @Override
    public void onPause() {
        super.onPause();
        if (getUserVisibleHint() && !TextUtils.isEmpty(pageId)) {
            leavePage();
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisible) {
        super.setUserVisibleHint(isVisible);
        if (!TextUtils.isEmpty(pageId)) {
            if (isVisible) {
                enterPage();
            } else {
                leavePage();
            }
        }
    }

    protected <V extends ViewModel> V getFragmentViewModel(@NonNull Class<V> modelClass) {
        if (mFragmentProvider == null) {
            mFragmentProvider = new ViewModelProvider(this);
        }
        return mFragmentProvider.get(modelClass);
    }

    protected <V extends ViewModel> V getActivityViewModel(@NonNull Class<V> modelClass) {
        if (mActivityProvider == null) {
            mActivityProvider = new ViewModelProvider(mActivity);
        }
        return mActivityProvider.get(modelClass);
    }

    private long startTime = 0;

    private void enterPage() {
        startTime = System.currentTimeMillis();
        LogOut.debug("BaseFragment", "enterPage:" + pageId);
        HashMap<String, String> map = new HashMap<>();
        if (getParams() != null) {
            map.putAll(getParams());
        }
        MultiTracker.onPageResume(pageId, map);
    }

    private void leavePage() {
        long duration = System.currentTimeMillis() - startTime;
        LogOut.debug("BaseFragment", "leavePage:" + pageId + "===duration==" + duration);
        HashMap<String, String> map = new HashMap<>();
        if (getParams() != null) {
            map.putAll(getParams());
        }
        map.put("duration", String.valueOf(duration));
        MultiTracker.onPagePause(pageId, map);
    }

    @Override
    public void onDestroy() {
        ((PorosApplication) getActivity().getApplication()).unregisterObserver(this);
        super.onDestroy();
    }
}
