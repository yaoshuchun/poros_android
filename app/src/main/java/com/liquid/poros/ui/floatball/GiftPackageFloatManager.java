package com.liquid.poros.ui.floatball;

import android.os.CountDownTimer;
import android.widget.Toast;

import com.liquid.base.event.EventCenter;
import com.liquid.base.mvp.MvpActivity;
import com.liquid.im.kit.custom.ReceiveGiftPackageAttachment;
import com.liquid.im.kit.custom.ShowGiftPackageAttachment;
import com.liquid.poros.BuildConfig;
import com.liquid.poros.PorosApplication;
import com.liquid.poros.analytics.utils.MultiTracker;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.entity.PlayloadInfo;
import com.liquid.poros.entity.RechargeWayInfo;
import com.liquid.poros.helper.TimeUtil;
import com.liquid.poros.ui.activity.LoginActivity;
import com.liquid.poros.ui.activity.WelcomeActivity;
import com.liquid.poros.ui.activity.user.SubmitUserInfoActivity;
import com.liquid.poros.ui.activity.user.TestSubmitUserInfoActivity;
import com.liquid.poros.ui.dialog.GiftPackageDialog;
import com.liquid.poros.ui.dialog.GiftPackagePayDialog;
import com.liquid.poros.ui.dialog.ReceiveGiftPackageDialog;
import com.liquid.poros.ui.dialog.RechargeCoinDialog;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.poros.utils.FloatWindowViewManager;
import com.liquid.poros.utils.TimeUtils;
import com.liquid.poros.widgets.SmallGiftPackageWindowView;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.Observer;
import com.netease.nimlib.sdk.msg.MsgServiceObserve;
import com.netease.nimlib.sdk.msg.model.IMMessage;

import java.util.HashMap;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import de.greenrobot.event.EventBus;
import de.greenrobot.event.Subscribe;
import de.greenrobot.event.ThreadMode;

/**
 * 礼包悬浮管理类
 */
public final class GiftPackageFloatManager {
    private SmallGiftPackageWindowView smallGiftPackageWindowView;
    private static GiftPackageFloatManager manager = new GiftPackageFloatManager();
    private AppCompatActivity mvpActivity;
    private long mCount = 10 * 60 * 1000;
    private CountDownTimer mCountDownTimer;
    private int pay_amount = 0;
    public List<RechargeWayInfo> recharge_way;
    private GiftPackagePayDialog payDialog;

    private GiftPackageFloatManager() {
        payDialog = new GiftPackagePayDialog();
        smallGiftPackageWindowView = new SmallGiftPackageWindowView(PorosApplication.context);
        smallGiftPackageWindowView.setOnViewClickListener(liveCircleWindowView -> {
            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put("event_time", String.valueOf(System.currentTimeMillis()));
            MultiTracker.onEvent(TrackConstants.B_SMALL_GIFT_BAG_ICON_CLICK, hashMap);
            showGiftBuyDialog(pay_amount == 0 ? AccountUtil.getInstance().getPay_amount() : pay_amount, 6);
        });
        EventBus.getDefault().register(this);

        NIMClient.getService(MsgServiceObserve.class).observeReceiveMessage(incomingMessageObserver, true);
    }

    Observer<List<IMMessage>> incomingMessageObserver = new Observer<List<IMMessage>>() {
        @Override
        public void onEvent(List<IMMessage> messages) {
            for (IMMessage message : messages) {
                if (message.getAttachment() instanceof ReceiveGiftPackageAttachment) {
                    GiftPackageFloatManager.getInstance().showReceiveGiftBuyDialog();
                    EventBus.getDefault().post(new EventCenter(Constants.EVENT_CODE_UPDATE_GIFT_PACKAGE_STATUS));
                    continue;
                }
            }
        }
    };

    @Subscribe(threadMode = ThreadMode.MainThread)
    public void onEventBus(EventCenter eventCenter) {
        if (null != eventCenter && eventCenter.getEventCode() == Constants.EVENT_CODE_BUY_GIFT_PACKAGE_SUCCESS) {
            endGiftPackage();
            release();
        }
    }

    public void showGiftBuyDialog(int pay_amount, int explosure_reason) {
        if (check()) {
            return;
        }

        if (mCount < 0) {
            return;
        }

        this.pay_amount = pay_amount;
        GiftPackageDialog.newInstance().setmCount(mCount).show(mvpActivity.getSupportFragmentManager(), GiftPackageDialog.class.getSimpleName());

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("event_time", String.valueOf(System.currentTimeMillis()));
        hashMap.put("explosure_reason", explosure_reason + "");
        MultiTracker.onEvent(TrackConstants.B_SMALL_GIFT_BAG_EXPLOSURE, hashMap);
    }

    public void showReceiveGiftBuyDialog() {
        if (check()) {
            return;
        }
        ReceiveGiftPackageDialog.newInstance().show(mvpActivity.getSupportFragmentManager(), ReceiveGiftPackageDialog.class.getSimpleName());
    }

    public void showPayDialog() {
        if (check()) {
            return;
        }
        payDialog.setRechargeWays(pay_amount, recharge_way);
        payDialog.show(mvpActivity.getSupportFragmentManager(), GiftPackagePayDialog.class.getSimpleName());
    }

    public static GiftPackageFloatManager getInstance() {
        if (manager == null) {
            manager = new GiftPackageFloatManager();
        }
        return manager;
    }


    public void showFloat(AppCompatActivity activity) {
        mvpActivity = activity;
        if (!AccountUtil.getInstance().isOpen()){
            return;
        }

        if (check()){
            return;
        }

        if (activity instanceof WelcomeActivity || activity instanceof LoginActivity || activity instanceof SubmitUserInfoActivity || activity instanceof TestSubmitUserInfoActivity) {
            return;
        }

        if (mCount < 0) {
            return;
        }
        mvpActivity = activity;
        FloatWindowViewManager.getInstance().saveSingleView(smallGiftPackageWindowView);
        FloatWindowViewManager.getInstance().showSingleView(activity, SmallGiftPackageWindowView.class, 0, 0,false);
        smallGiftPackageWindowView.startAnimation();
        startCountTime();
    }

    public void hideFloat() {
        smallGiftPackageWindowView.releaseAnimation();
        FloatWindowViewManager.getInstance().removeSingleView(SmallGiftPackageWindowView.class);
        endCountTime();
    }

    private void startCountTime() {
        mCount = AccountUtil.getInstance().getCountdown();
        if (mCountDownTimer == null && mCount > 0) {
            mCountDownTimer = new CountDownTimer(mCount * 1000, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    mCount = millisUntilFinished;
                    if (smallGiftPackageWindowView != null) {
                        smallGiftPackageWindowView.setTime(TimeUtils.getFormatTime(millisUntilFinished));
                    }
                }

                @Override
                public void onFinish() {
                    release();
                    endGiftPackage();
                }
            };
            mCountDownTimer.start();
        }
    }

    private void endCountTime() {
        if (mCountDownTimer != null) {
            mCountDownTimer.cancel();
            mCountDownTimer = null;
        }
    }

    public void release() {
        hideFloat();
        endCountTime();
    }

    public boolean checkGiftPackage(int type) {
        if (AccountUtil.getInstance().isOpen()) {
            showGiftBuyDialog(pay_amount == 0 ? AccountUtil.getInstance().getPay_amount() : pay_amount, type);
            return true;
        }
        return false;
    }

    public void endGiftPackage() {
        AccountUtil.getInstance().setOpen(false);
        EventBus.getDefault().post(new EventCenter(Constants.EVENT_CODE_UPDATE_GIFT_PACKAGE_STATUS));
    }

    public boolean check() {
        if (mvpActivity == null) {
            return true;
        }

        if (mvpActivity.isFinishing() || mvpActivity.isDestroyed()) {
            return true;
        }

        return false;
    }

}
