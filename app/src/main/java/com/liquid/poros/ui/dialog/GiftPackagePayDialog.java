package com.liquid.poros.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alipay.sdk.app.PayTask;
import com.bumptech.glide.Glide;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.liquid.base.adapter.CommonAdapter;
import com.liquid.base.adapter.CommonViewHolder;
import com.liquid.base.event.EventCenter;
import com.liquid.poros.R;
import com.liquid.poros.adapter.RechargeCoinAmountAdapter;
import com.liquid.poros.alipay.PayResult;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.contract.account.RechargeContract;
import com.liquid.poros.contract.account.RechargePresenter;
import com.liquid.poros.entity.AlipayInfo;
import com.liquid.poros.entity.RechargeInfo;
import com.liquid.poros.entity.RechargeListInfo;
import com.liquid.poros.entity.RechargeResultInfo;
import com.liquid.poros.entity.RechargeWayInfo;
import com.liquid.poros.entity.UserCoinInfo;
import com.liquid.poros.entity.WeChatPayInfo;
import com.liquid.poros.ui.fragment.dialog.account.PayWaysChooseDialogFragment;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.poros.utils.ScreenUtil;
import com.liquid.poros.utils.StringUtil;
import com.liquid.poros.utils.ViewUtils;
import com.liquid.poros.widgets.DrawableCenterTextView;
import com.liquid.poros.widgets.WrapContentLinearLayoutManager;
import com.liquid.poros.wxapi.WXUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import de.greenrobot.event.EventBus;
import de.greenrobot.event.Subscribe;
import de.greenrobot.event.ThreadMode;

public class GiftPackagePayDialog extends BottomSheetDialogFragment implements View.OnClickListener, RechargeContract.View{
    private Context context;
    private static final int SDK_PAY_FLAG = 1;
    private TextView tv_pay_money;
    private TextView tv_pay_coin;
    private RecyclerView rv_recharge_way;
    private TextView tv_recharge_pay;
    private List<RechargeWayInfo> mRechargeWaysList;
    private int mPayMoney;
    private int pay_way;
    private ArrayList<Integer> mIsSelectList = new ArrayList<>();
    private CommonAdapter<RechargeWayInfo> mCommonAdapter;
    private String mOrderId;
    private RechargePresenter mPresent;

    public GiftPackagePayDialog setRechargeWays(int payMoney, List<RechargeWayInfo> rechargeWaysList) {
        this.mPayMoney = payMoney;
        this.mRechargeWaysList = rechargeWaysList;
        return this;
    }

    private Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case SDK_PAY_FLAG:
                    if (TextUtils.isEmpty(mOrderId)) {
                        return;
                    }
                    PayResult payResult = new PayResult((Map<String, String>) msg.obj);
                    String resultStatus = payResult.getResultStatus();
                    if (!StringUtil.isEquals(resultStatus, "9000")) {
                        //订单取消/失败
                        mPresent.closeRechargeOrder(mOrderId);
                    }
                    mPresent.rechargeResult(mOrderId);
                    break;
            }
        }
    };

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresent = new RechargePresenter();
        mPresent.attachView(this);
        EventBus.getDefault().register(this);
    }

    @Subscribe(threadMode = ThreadMode.MainThread)
    public void onEventBus(EventCenter eventCenter) {
        if (null != eventCenter) {
            if (eventCenter.getEventCode() == Constants.EVENT_CODE_PAY_RECHARGE) {
                int baseRespCode = (int) eventCenter.getData();
                if (baseRespCode != 0) {//订单取消
                    if (!TextUtils.isEmpty(mOrderId)) {
                        mPresent.closeRechargeOrder(mOrderId);
                    }
                }
                if (!TextUtils.isEmpty(mOrderId)) {
                    mPresent.rechargeResult(mOrderId);
                }
            }
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
        View view = View.inflate(getContext(), R.layout.fragment_pay_ways_choose_dialog, null);
        dialog.setContentView(view);
        //设置透明背景
        dialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(R.drawable.bg_recharge_panel);
        View root = dialog.getDelegate().findViewById(R.id.design_bottom_sheet);
        BottomSheetBehavior behavior = BottomSheetBehavior.from(root);
        behavior.setHideable(false);
        initializeView(view);
        return dialog;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void initializeView(View view) {
        tv_pay_money = view.findViewById(R.id.tv_pay_money);
        tv_pay_coin = view.findViewById(R.id.tv_pay_coin);
        rv_recharge_way = view.findViewById(R.id.rv_recharge_way);
        tv_recharge_pay = view.findViewById(R.id.tv_recharge_pay);
        BigDecimal bigDecimal = new BigDecimal(mPayMoney);
        BigDecimal divide = bigDecimal.divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_UP);
        tv_pay_money.setText(StringUtil.matcherSearchText(true, Color.parseColor("#10D8C8"), divide.toString(), "￥"));
        tv_pay_coin.setText("订单详情:新手礼包");
        rv_recharge_way.setLayoutManager(new WrapContentLinearLayoutManager(getActivity()));
        mIsSelectList.add(0);
        pay_way = mRechargeWaysList.get(mIsSelectList.get(0)).getId();
        mCommonAdapter = new CommonAdapter<RechargeWayInfo>(getActivity(), R.layout.item_recharge_way) {
            @Override
            public void convert(CommonViewHolder holder, RechargeWayInfo rechargeWayInfo, int pos) {
                holder.setText(R.id.tv_recharge_way_title, rechargeWayInfo.getTitle());
                Glide.with(mContext)
                        .load(rechargeWayInfo.getIcon())
                        .override(ScreenUtil.dip2px(24), ScreenUtil.dip2px(24))
                        .into((ImageView) holder.getView(R.id.iv_recharge_way));
                if (mIsSelectList.contains(pos)) {
                    holder.getView(R.id.iv_recharge_way_check).setBackgroundResource(R.mipmap.icon_recharge_way_select);
                } else {
                    holder.getView(R.id.iv_recharge_way_check).setBackgroundResource(R.mipmap.icon_recharge_way_normal);
                }
                holder.getView(R.id.rl_recharge_way).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!mIsSelectList.contains(pos)) {
                            mIsSelectList.clear();
                            mIsSelectList.add(pos);
                        }
                        pay_way = mRechargeWaysList.get(mIsSelectList.get(0)).getId();
                        notifyDataSetChanged();
                    }
                });
            }
        };
        mCommonAdapter.update(mRechargeWaysList);
        rv_recharge_way.setAdapter(mCommonAdapter);
        tv_recharge_pay.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (ViewUtils.isFastClick()) {
            return;
        }
        if (!AccountUtil.getInstance().isOpen()){
            Toast.makeText(getContext(),"活动已结束",Toast.LENGTH_SHORT).show();
            dismissAllowingStateLoss();
            return;
        }

        mPresent.smallPacketPay(mPayMoney,pay_way);
    }

    @Override
    public void onRechargeInfoFetch(RechargeInfo rechargeInfo) {
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onWechatSuccessFetch(WeChatPayInfo weChatPayInfo) {
        if (weChatPayInfo == null || weChatPayInfo.getData() == null || weChatPayInfo.getData().getRecharge_data() == null) {
            return;
        }
        WeChatPayInfo.Data.RechargeData rechargeData = weChatPayInfo.getData().getRecharge_data();
        mOrderId = weChatPayInfo.getData().getOrder_id();
        WXUtils.getInstance(context)
                .recharge(rechargeData.getAppid()
                        , rechargeData.getPartnerid()
                        , rechargeData.getPrepayid()
                        , rechargeData.getNoncestr()
                        , rechargeData.getTimestamp()
                        , rechargeData.getPackageValue()
                        , rechargeData.getSign());
    }

    @Override
    public void onAlipaySuccessFetch(AlipayInfo alipayInfo) {
        if (alipayInfo == null || alipayInfo.getData() == null || alipayInfo.getData().getRecharge_data() == null) {
            return;
        }
        mOrderId = alipayInfo.getData().getOrder_id();
        String order_string = alipayInfo.getData().getRecharge_data().getOrder_string();
        Runnable payRunnable = new Runnable() {
            @Override
            public void run() {
                PayTask alipay = new PayTask(getActivity());
                Map<String, String> result = alipay.payV2(order_string, true);
                Message msg = new Message();
                msg.what = SDK_PAY_FLAG;
                msg.obj = result;
                mHandler.sendMessage(msg);
            }
        };
        // 必须异步调用
        Thread payThread = new Thread(payRunnable);
        payThread.start();
    }

    @Override
    public void onRechargeResultFetch(RechargeResultInfo rechargeResultInfo) {
        if (rechargeResultInfo == null) return;
        if (rechargeResultInfo.getCode() == 0) {
            EventBus.getDefault().post(new EventCenter(Constants.EVENT_CODE_BUY_GIFT_PACKAGE_SUCCESS));
            dismissAllowingStateLoss();
        }
    }

    @Override
    public void onUserCoinSuccessFetch(UserCoinInfo userCoinInfo) {

    }

    @Override
    public void onCloseRechargeOrderFetch() {

    }

    @Override
    public void showLoading() {

    }

    @Override
    public void showSuccess() {

    }

    @Override
    public void closeLoading() {

    }

    @Override
    public void onError() {

    }

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void onSuccess(Object data) {

    }

    public interface OnCoinUpdateListener{
        void update(String coin);
    }
}
