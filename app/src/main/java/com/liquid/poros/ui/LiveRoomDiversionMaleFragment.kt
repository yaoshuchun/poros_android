package com.liquid.poros.ui

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Html
import android.text.TextUtils
import android.util.DisplayMetrics
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.faceunity.nama.fromapp.utils.ToastUtil
import com.liquid.base.utils.ContextUtils
import com.liquid.im.kit.custom.DaoliuAttachment
import com.liquid.poros.R
import com.liquid.poros.analytics.utils.MultiTracker
import com.liquid.poros.base.BaseDialogFragment
import com.liquid.poros.constant.Constants
import com.liquid.poros.entity.RoomLiveInfo
import com.liquid.poros.ui.activity.live.RoomLiveActivityV2
import com.liquid.poros.utils.MyActivityManager
import com.liquid.poros.utils.ScreenUtil
import com.liquid.poros.utils.StringUtil
import com.liquid.poros.utils.TextViewUtils
import com.liquid.poros.viewmodel.LiveRoomDiversionViewModel
import com.luck.picture.lib.tools.ToastUtils
import kotlinx.android.synthetic.main.live_room_diversion_dialogfragment.*
import java.util.*

class LiveRoomDiversionMaleFragment : BaseDialogFragment() {
    private var liveRoomDiversionViewModel: LiveRoomDiversionViewModel? = null
    private var daoliuAttachment: DaoliuAttachment? = null
    var context: Activity? = null

    override fun getPageId(): String? {
        return null
    }

    companion object {
        fun newInstance(): LiveRoomDiversionMaleFragment {
            val fragment = LiveRoomDiversionMaleFragment()
            return fragment
        }
    }

    override fun onStart() {
        super.onStart()
        val win = dialog?.window
        win?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val dm = DisplayMetrics()
        context?.windowManager?.defaultDisplay?.getMetrics(dm)

        val params = win?.attributes
        params?.gravity = Gravity.CENTER
        params?.width = ScreenUtil.getDisplayWidth() * 4/ 5
        isCancelable = false
        try {
            win?.attributes = params
        }catch (exception:java.lang.Exception){
            exception.printStackTrace()
            return
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view = inflater.inflate(R.layout.live_room_diversion_dialogfragment, container)
         liveRoomDiversionViewModel =  getDialogFragmentViewModel(LiveRoomDiversionViewModel::class.java)
        liveRoomDiversionViewModel?.roomInfoDataSuccess?.observe(this,androidx.lifecycle.Observer{
            roomInfoDataSuccess(it)
        })
        liveRoomDiversionViewModel?.roomInfoDataFail?.observe(this, androidx.lifecycle.Observer { RoomLiveInfo: RoomLiveInfo -> ToastUtil.showToast(ContextUtils.getApplicationContext(), RoomLiveInfo.msg) })
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        iv_guest_avatar.visibility = View.GONE
        tv_guest_tags.visibility = View.GONE

        iv_guest_avatar.visibility = View.VISIBLE
        if(daoliuAttachment?.isIs_new == true) {
            iv_newer.visibility = View.VISIBLE
        }
        tv_guest_nickname.visibility = View.VISIBLE
        tv_guest_age.visibility = View.VISIBLE
        tv_guest_tags.visibility = View.VISIBLE
        daoliuAttachment?.avatar_url?.let {
            context?.let {
                Glide.with(it)
                        .load(daoliuAttachment?.avatar_url)
                        .circleCrop()
                        .into(iv_guest_avatar)
            }
        }

        tv_guest_nickname.text = daoliuAttachment?.nick_name ?: ""
        tv_guest_age.text = Html.fromHtml(TextViewUtils.getColoredSpanned("年龄：", "#10D8C8")
                + TextViewUtils.getColoredSpanned(daoliuAttachment?.age.toString()+"岁", "#FFFFFF"))
        tv_guest_tags.text = Html.fromHtml(TextViewUtils.getColoredSpanned("爱玩：", "#10D8C8")
                + TextViewUtils.getColoredSpanned(daoliuAttachment?.game ?: "", "#FFFFFF"))
        if (!TextUtils.isEmpty(daoliuAttachment?.anchor_avatar_url ?: "")) {
            context?.let {
                Glide.with(it)
                        .load(daoliuAttachment?.anchor_avatar_url)
                        .circleCrop()
                        .into(iv_master_avatar)
            }
        }


        if (TextUtils.isEmpty(daoliuAttachment?.nick_name)) {
            tv_master_slogan.text = Html.fromHtml(TextViewUtils.getColoredSpanned("主持：", "#10BCFF") +
                    TextViewUtils.getColoredSpanned("邀请你上麦组CP", "#FFFFFF"))
        } else {

            var nickName = if (daoliuAttachment?.nick_name?.length!! > 5) {
                daoliuAttachment?.nick_name?.substring(0, 4) + ".."
            } else {
                daoliuAttachment?.nick_name!!
            }
            tv_master_slogan.text = Html.fromHtml(TextViewUtils.getColoredSpanned("主持：", "#10BCFF") +
                    TextViewUtils.getColoredSpanned("女神【$nickName】邀请你上麦组团", "#FFFFFF"))
        }

        iv_close.setOnClickListener {
            liveRoomDiversionViewModel?.diversionDialogClose()
            dismissAllowingStateLoss()
            trackOnClick("clouse")
        }
        btn_not_to_go.setOnClickListener {
            liveRoomDiversionViewModel?.diversionDialogClose()
            dismissAllowingStateLoss()
            trackOnClick("not_go")
        }

        btn_free_onlooker.setOnClickListener {
            daoliuAttachment?.room_id?.let { it1 -> liveRoomDiversionViewModel?.getLiveRoomInfo(it1) }
            trackOnClick("look_on")
        }
        val msgParams = HashMap<String?, String?>()
        msgParams["anchor_id"] = daoliuAttachment?.anchor_id
        msgParams["guest_id"] = daoliuAttachment?.guest_id
        msgParams["diversion_popup_type"] = if (daoliuAttachment?.isIs_new == true) {
            "new_female_guest"
        } else {
            "female_guest"
        }
        MultiTracker.onEvent("b_diversion_popup_exposure", msgParams)

    }

    private fun trackOnClick(clickType: String) {
        val msgParams = HashMap<String?, String?>()
        msgParams["anchor_id"] = daoliuAttachment?.anchor_id
        msgParams["guest_id"] = daoliuAttachment?.guest_id
        msgParams["diversion_popup_click_type"] = clickType
        msgParams["diversion_popup_type"] = if (daoliuAttachment?.isIs_new == true) {
            "new_female_guest"
        } else {
            "female_guest"
        }
        MultiTracker.onEvent("b_diversion_popup_click", msgParams)
    }

    fun setAttachment(daoliuAttachment: DaoliuAttachment): LiveRoomDiversionMaleFragment {
        this.daoliuAttachment = daoliuAttachment
        return this
    }

    fun setActivity(activity: Activity): LiveRoomDiversionMaleFragment {
        this.context = activity
        return this
    }
    private fun roomInfoDataSuccess(roomInfo: RoomLiveInfo) {
        if (roomInfo != null && roomInfo.room_type == RoomLiveActivityV2.ROOM_TYPE_PRIVATE) {
            ToastUtils.s(ContextUtils.getApplicationContext(), "此房间是专属房，暂无法加入")
            dismissAllowingStateLoss()
        } else {
            goToLiveHome()
        }
    }

    private fun goToLiveHome() {
        if (context == null) return
        try {
            val activity = MyActivityManager.getInstance().currentActivity
            if (activity is RoomLiveActivityV2) {
                val mRoomId = activity.mRoomId
                if (StringUtil.isEquals(mRoomId, daoliuAttachment?.room_id)) {
                    return
                } else {
                    activity.finish()
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        daoliuAttachment?.run {
            val intent = Intent(context, RoomLiveActivityV2::class.java)
            val liveBundle = Bundle()
            liveBundle.putString(Constants.ROOM_ID, room_id)
            liveBundle.putString(Constants.IM_ROOM_ID, im_room_id)
            liveBundle.putString(Constants.IM_ROOM_SOURCE, source)
            liveBundle.putString(Constants.INVITE_ID, this?.invite_id.toString())
            isIs_new?.let { liveBundle.putBoolean(Constants.IS_NEW, it) }
//                liveBundle.putString(Constants.STYLE_DIVERSION,mRobMicStyle.toString())
//                if (mRobMicStyle == 1) {
//                    liveBundle.putString(Constants.AUDIENCE_FROM, "recive_into")
//                }else {
            liveBundle.putString(Constants.AUDIENCE_FROM, "system_call")
//                }
//                liveBundle.putBoolean(Constants.IS_LEAD_VIDEO,true)
            intent.putExtras(liveBundle)
            context?.startActivity(intent)
            dismissAllowingStateLoss()
        }
    }
}