package com.liquid.poros.ui.activity.blindbox

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentPagerAdapter
import com.liquid.poros.R
import com.liquid.poros.base.BaseActivity
import com.liquid.poros.constant.TrackConstants
import com.liquid.poros.ui.fragment.blindbox.BlindBoxRecordFragment
import com.liquid.poros.utils.DensityUtil
import kotlinx.android.synthetic.main.activity_blind_box_record.*
import net.lucode.hackware.magicindicator.ViewPagerHelper
import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView
import net.lucode.hackware.magicindicator.buildins.commonnavigator.indicators.LinePagerIndicator
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.ClipPagerTitleView
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.badge.BadgePagerTitleView
import java.util.ArrayList

/**
 * 盲盒中奖记录
 */
@Suppress("DEPRECATION")
class BlindBoxRecordActivity : BaseActivity() {

    private val viewPagerTitles = arrayListOf("初级盲盒", "中级盲盒", "高级盲盒")
    private val fragments: ArrayList<Fragment> = ArrayList()

    //初级盲盒
    private var TYPE_BLIND_COMMON: Int = 0

    //中级盲盒
    private var TYPE_BLIND_MIDDLE: Int = 1

    //高级宝箱
    private var TYPE_BLIND_HIGH: Int = 2

    override fun getPageId(): String {
        return TrackConstants.PAGE_BLIND_BOX_RECORD
    }

    override fun getContentViewLayoutID(): Int {
        return R.layout.activity_blind_box_record
    }

    override fun parseBundleExtras(extras: Bundle?) {
    }

    override fun needSetTransparent(): Boolean {
        return false
    }

    override fun initViewsAndEvents(savedInstanceState: Bundle?) {
        val commonRecordFragment = BlindBoxRecordFragment.newInstance(TYPE_BLIND_COMMON)
        commonRecordFragment?.let { fragments.add(it) }
        val middleRecordFragment = BlindBoxRecordFragment.newInstance(TYPE_BLIND_MIDDLE)
        middleRecordFragment?.let { fragments.add(it) }
        val highRecordFragment = BlindBoxRecordFragment.newInstance(TYPE_BLIND_HIGH)
        highRecordFragment?.let { fragments.add(it) }
        vp_blind_box_record.adapter = object : FragmentPagerAdapter(supportFragmentManager) {
            override fun getCount(): Int {
                return fragments.size
            }

            override fun getItem(position: Int): Fragment {
                return fragments[position]
            }
        }
        initMagicIndicator()
    }

    private fun initMagicIndicator() {
        val commonNavigator = CommonNavigator(this)
        commonNavigator.isAdjustMode = true
        commonNavigator.adapter = object : CommonNavigatorAdapter() {
            override fun getCount(): Int {
                return viewPagerTitles?.size ?: 0
            }

            override fun getTitleView(context: Context, index: Int): IPagerTitleView {
                val badgePagerTitleView = BadgePagerTitleView(context)
                val clipPagerTitleView = ClipPagerTitleView(context)
                clipPagerTitleView.text = viewPagerTitles[index]
                clipPagerTitleView.textColor = Color.parseColor("#A4A9B3")
                clipPagerTitleView.clipColor = Color.parseColor("#10D8C8")
                clipPagerTitleView.textSize = DensityUtil.dp2px(12F).toFloat()
                clipPagerTitleView.setOnClickListener {
                    vp_blind_box_record.currentItem = index
                }
                badgePagerTitleView.innerPagerTitleView = clipPagerTitleView
                return badgePagerTitleView
            }

            override fun getIndicator(context: Context): IPagerIndicator {
                val indicator = LinePagerIndicator(context)
                indicator.lineHeight = DensityUtil.dp2px(28F).toFloat()
                indicator.roundRadius = DensityUtil.dp2px(90F).toFloat()
                indicator.setColors(Color.parseColor("#ffffff"))
                return indicator
            }
        }
        magic_indicator_blind_box_record.navigator = commonNavigator
        ViewPagerHelper.bind(magic_indicator_blind_box_record, vp_blind_box_record)
    }

}