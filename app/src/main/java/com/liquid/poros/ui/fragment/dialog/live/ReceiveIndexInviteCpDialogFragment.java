package com.liquid.poros.ui.fragment.dialog.live;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.liquid.base.adapter.CommonAdapter;
import com.liquid.base.adapter.CommonViewHolder;
import com.liquid.base.adapter.MultiItemCommonAdapter;
import com.liquid.base.adapter.MultiItemTypeSupport;
import com.liquid.poros.R;
import com.liquid.poros.base.BaseDialogFragment;
import com.liquid.poros.analytics.utils.MultiTracker;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.entity.IndexCpInviteInfo;
import com.liquid.poros.ui.RouterUtils;
import com.liquid.poros.ui.activity.live.RoomLiveActivityV2;
import com.liquid.poros.utils.ScreenUtil;
import com.liquid.poros.utils.ViewUtils;
import com.liquid.poros.widgets.FlowLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


public class ReceiveIndexInviteCpDialogFragment extends BaseDialogFragment {
    private TimerTask mTask2;
    private Timer mTimer2;
    private IndexCpInviteInfo.Data indexCpInviteInfo;
    private long startTime;
    private int remainCount = 15;
    List<String> tagString = new ArrayList<>();

    public ReceiveIndexInviteCpDialogFragment setUserInfo(IndexCpInviteInfo.Data indexCpInviteInfo) {
        this.indexCpInviteInfo = indexCpInviteInfo;
        startTime = System.currentTimeMillis();
        return this;
    }

    public static ReceiveIndexInviteCpDialogFragment newInstance() {
        ReceiveIndexInviteCpDialogFragment fragment = new ReceiveIndexInviteCpDialogFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    protected String getPageId() {
        return TrackConstants.P_ROOM_INVITATION;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onStart() {
        super.onStart();
        Window win = getDialog().getWindow();
        // 一定要设置Background，如果不设置，window属性设置无效
        win.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams params = win.getAttributes();
        params.gravity = Gravity.CENTER;
        setCancelable(false);
        // 使用ViewGroup.LayoutParams，以便Dialog 宽度充满整个屏幕
        params.width = (int) (dm.widthPixels * 0.8);
        win.setAttributes(params);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_receive_invite_cp_game_dialog, null);
        ImageView iv_invite_cp_head = view.findViewById(R.id.iv_invite_cp_head);
        TextView tv_name = view.findViewById(R.id.tv_name);
        TextView tv_user_desc = view.findViewById(R.id.tv_user_desc);
        RequestOptions options = new RequestOptions()
                .placeholder(R.mipmap.img_default_avatar)
                .transform(new CircleCrop())
                .error(R.mipmap.img_default_avatar);
        Glide.with(getActivity())
                .load(indexCpInviteInfo.getUser_info().getAvatar_url())
                .apply(options)
                .into(iv_invite_cp_head);
        tv_user_desc.setText(indexCpInviteInfo.getDesc());
        tv_name.setText(indexCpInviteInfo.getUser_info().getNick_name());
        FlowLayout fl_user_game = view.findViewById(R.id.fl_user_game);
        RecyclerView fl_img = view.findViewById(R.id.fl_img);
        final TextView tv_cancel = view.findViewById(R.id.tv_cancel);
        tagString.clear();
        tagString.add(indexCpInviteInfo.getUser_info().getAge() + "岁");
        if (indexCpInviteInfo.getUser_info().getGames() != null && indexCpInviteInfo.getUser_info().getGames().size() > 0) {
            tagString.addAll(indexCpInviteInfo.getUser_info().getGames());
        }
        if (tagString != null && tagString.size() > 0) {
            fl_user_game.setVisibility(View.VISIBLE);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(0, 0, ScreenUtil.dip2px(8), ScreenUtil.dip2px(4));
            fl_user_game.removeAllViews();
            for (String item : tagString) {
                final TextView tv = new TextView(getContext());
                tv.setPadding(18, 8, 18, 8);
                tv.setText(item);
                tv.setMaxEms(10);
                tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 10);
                tv.setTextColor(Color.parseColor("#8896B3"));
                tv.setGravity(Gravity.CENTER);
                tv.setSingleLine();
                if (item.equals(indexCpInviteInfo.getUser_info().getAge() + "岁")) {
                    Drawable drawable = getActivity().getResources().getDrawable(R.mipmap.ic_nv);
                    drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
                    tv.setCompoundDrawables(drawable, null, null, null);
                    tv.setCompoundDrawablePadding(5);
                }
                tv.setBackground(getResources().getDrawable(R.drawable.bg_cp_item));
                tv.setLayoutParams(layoutParams);
                fl_user_game.addView(tv, layoutParams);
            }
        } else {
            fl_user_game.setVisibility(View.GONE);
        }

        if (indexCpInviteInfo.getUser_info() != null && indexCpInviteInfo.getUser_info().getPhoto_list() != null && indexCpInviteInfo.getUser_info().getPhoto_list().size() > 0) {
            List<IndexCpInviteInfo.Data.UserInfo.RefItem> refItemList = indexCpInviteInfo.getUser_info().getPhoto_list().subList(0, indexCpInviteInfo.getUser_info().getPhoto_list().size() >= 3 ? 3 : indexCpInviteInfo.getUser_info().getPhoto_list().size());
            CommonAdapter<IndexCpInviteInfo.Data.UserInfo.RefItem> refAdapter = new MultiItemCommonAdapter<IndexCpInviteInfo.Data.UserInfo.RefItem>(getActivity(), new MultiItemTypeSupport<IndexCpInviteInfo.Data.UserInfo.RefItem>() {
                @Override
                public int getLayoutId(int itemType) {
                    if (itemType == 1) {
                        return R.layout.item_cp_game_ref_img;
                    }
                    return R.layout.item_cp_game_ref_video;
                }

                @Override
                public int getItemViewType(int position, IndexCpInviteInfo.Data.UserInfo.RefItem refItem) {
                    return refItem.getType();
                }
            }) {
                @Override
                public void convert(CommonViewHolder holder, IndexCpInviteInfo.Data.UserInfo.RefItem refItem, int pos) {
                    ImageView iv_image = holder.getView(R.id.iv_image);
                    RequestOptions headOptions = new RequestOptions()
                            .transform(new RoundedCorners(ScreenUtil.dip2px(5)));
                    Glide.with(getActivity())
                            .load(refItem.getPath())
                            .apply(headOptions)
                            .into(iv_image);
                }
            };
            refAdapter.update(refItemList);
            fl_img.setLayoutManager(new GridLayoutManager(getActivity(), 3));
            fl_img.setAdapter(refAdapter);
            fl_img.setVisibility(View.VISIBLE);
        } else {
            fl_img.setVisibility(View.GONE);
        }
        view.findViewById(R.id.tv_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                releaseTimer();
                dismissDialog();
            }
        });
        view.findViewById(R.id.tv_confirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ViewUtils.isFastClick()) {
                    return;
                }
//                startActivity(new Intent(getActivity(), RoomLiveActivityV2.class).putExtra(Constants.ROOM_ID, indexCpInviteInfo.getRoom_id()));
                Bundle bundle = new Bundle();
                bundle.putString(Constants.ROOM_ID, indexCpInviteInfo.getRoom_id());
                bundle.putString(Constants.AUDIENCE_FROM, "invite_cp_dialog");
                RouterUtils.goToRoomLiveActivity(getContext(),bundle);
                releaseTimer();
                MultiTracker.onEvent(TrackConstants.B_AGREEL_ROOM);
                dismissDialog();
            }
        });

        builder.setView(view);
        int tempCount = (int) ((indexCpInviteInfo.getEnd_timestamp() - System.currentTimeMillis()) / 1000);
        if (tempCount < remainCount && tempCount > 0) {
            remainCount = tempCount;
        }
        if (mTimer2 == null && mTask2 == null) {
            mTimer2 = new Timer();
            mTask2 = new TimerTask() {
                @Override
                public void run() {
                    tv_cancel.post(new Runnable() {
                        @Override
                        public void run() {
                            long duration = System.currentTimeMillis() - startTime;
                            int time = remainCount - (int) (duration / 1000);
                            if (time <= 0) {
                                releaseTimer();
                                dismissDialog();
                            } else {
                                tv_cancel.setText(String.format("稍后再说 (%d)", time));
                            }
                        }
                    });

                }
            };
            mTimer2.schedule(mTask2, 0, 1000);
        }
        return builder.create();
    }

    private void releaseTimer() {
        if (mTimer2 != null) {
            mTimer2.cancel();
            mTimer2 = null;
            mTask2.cancel();
            mTask2 = null;
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        releaseTimer();
    }
}
