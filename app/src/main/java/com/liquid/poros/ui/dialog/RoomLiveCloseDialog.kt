package com.liquid.poros.ui.dialog

import android.content.Context
import android.view.Gravity
import android.view.View
import android.widget.TextView
import com.liquid.poros.R
import com.liquid.poros.ui.activity.live.RoomLiveActivityV2
import razerdp.basepopup.BasePopupWindow

/**
 * 直播房结束关闭对话框
 */
class RoomLiveCloseDialog(context: Context) : BasePopupWindow(context) {
    override fun onCreateContentView(): View {
        popupGravity = Gravity.CENTER
        return createPopupById(R.layout.dialog_room_live_close)
    }

    override fun onViewCreated(contentView: View) {
        contentView.findViewById<TextView>(R.id.tv_back).setOnClickListener {
            (context as RoomLiveActivityV2).finish()
        }
    }
}