package com.liquid.poros.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager.widget.ViewPager;
import com.liquid.poros.R;
import com.liquid.poros.adapter.GiftPagerAdapter;
import com.liquid.poros.analytics.utils.MultiTracker;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.constant.GiftTypeEnum;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.entity.MsgUserModel;
import com.liquid.poros.ui.activity.user.SessionActivity;
import com.liquid.poros.ui.fragment.BagFragment;
import com.liquid.poros.ui.fragment.BuyCpCardFragment;
import com.liquid.poros.ui.fragment.GiftSendFragment;
import com.liquid.poros.utils.StringUtil;
import com.liquid.poros.widgets.bottomdialog.ViewPagerBottomSheetBehavior;
import com.liquid.poros.widgets.bottomdialog.ViewPagerBottomSheetDialog;
import com.liquid.poros.widgets.bottomdialog.ViewPagerBottomSheetDialogFragment;
import com.netease.nimlib.sdk.msg.model.IMMessage;
import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.ViewPagerHelper;
import net.lucode.hackware.magicindicator.buildins.UIUtil;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.indicators.LinePagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.SimplePagerTitleView;
import org.jetbrains.annotations.NotNull;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MultiGiftSendDialog extends ViewPagerBottomSheetDialogFragment {
    public static final String GIFT_BEANS = "gift_beans";
    public static final String GIFT_NUMS = "gift_nums";
    public static final String GIFT_CHANNEL = "gift_channel";
    private Context context;
    private CustomViewPager viewpagerContainer;
    private MagicIndicator magicIndicatorTab;
    private TextView coinCount;
    private ImageView recharge;
    //盲盒相关
    private View cl_blind_box;
    //盲盒提示介绍
    private TextView tv_blind_box_tip;
    //盲盒点击查看礼物
    private TextView tv_entry_blind_box;

    private List<Fragment> fragments;
    private List<String> viewPagerTitle;
    private CommonNavigatorAdapter commonNavigatorAdapter;
    private CommonNavigator commonNavigator;
    private GiftSendFragment fragmentOne;
    private BuyCpCardFragment fragmentTwo;
    private BagFragment fragmentThree;
    private String cpOrderId;
    private boolean mIsSuperCp;
    private GiftPagerAdapter pagerAdapter;
    private RechargeCoinDialog rechargeCoinDialog;
    private String coin;
    private String femaleGuestId;
    private String giftChannel;
    private boolean mIsAddSuperCp;
    //礼物列表
    private ArrayList<MsgUserModel.GiftBean> mGiftBeans;
    //礼物数量列表
    private ArrayList<MsgUserModel.GiftNums> mGiftNums;
    //盲盒介绍h5
    private String blindBoxH5;
    public MultiGiftSendDialog(Context context) {
        this.context = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        ViewPagerBottomSheetDialog dialog = (ViewPagerBottomSheetDialog) super.onCreateDialog(savedInstanceState);
        View view = View.inflate(getContext(), R.layout.view_send_gift_multi, null);
        dialog.setContentView(view);
        //设置透明背景
        dialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);
        WindowManager.LayoutParams params = new WindowManager.LayoutParams();
        params.type =  1000;
        dialog.getWindow().setAttributes(params);
        View root = dialog.getDelegate().findViewById(R.id.design_bottom_sheet);
        ViewPagerBottomSheetBehavior behavior = ViewPagerBottomSheetBehavior.from(root);
        behavior.setHideable(false);
        initializeView(view);
        return dialog;
    }

    public void setCpOrderId(String cpOrderId) {
        if (fragmentTwo != null) {
            fragmentTwo.setCpOrderId(cpOrderId);
        }
    }

    private void initializeView(View view) {
        viewpagerContainer = view.findViewById(R.id.viewpager_container);
        magicIndicatorTab = view.findViewById(R.id.magic_indicator_tab);
        coinCount = view.findViewById(R.id.coin_count);
        recharge = view.findViewById(R.id.recharge);
        cl_blind_box = view.findViewById(R.id.cl_blind_box);
        tv_blind_box_tip = view.findViewById(R.id.tv_blind_box_tip);
        tv_entry_blind_box = view.findViewById(R.id.tv_entry_blind_box);
        viewpagerContainer.setScanScroll(true);
        coinCount.setText(coin);
        initData();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public MultiGiftSendDialog show(MultiGiftSendDialog fragment,FragmentActivity activity, String tag,
                                           ArrayList<MsgUserModel.GiftBean> giftBeans,
                                           ArrayList<MsgUserModel.GiftNums> giftNums,
                                           String sessionId,String cpOrderId,String gift_channel){
        if (activity.isDestroyed() || activity.isFinishing()) return null;
        Bundle bundle = new Bundle();
        bundle.putSerializable(GIFT_BEANS,giftBeans);
        bundle.putSerializable(GIFT_NUMS,giftNums);
        bundle.putString(Constants.FEMALE_GUEST_ID,sessionId);
        bundle.putString(Constants.CP_ORDER_ID,cpOrderId);
        bundle.putString(GIFT_CHANNEL,gift_channel);
        fragment.setArguments(bundle);
        fragment.show(activity.getSupportFragmentManager(),tag);
        return fragment;
    }

    public void sendGift(int giftId, @org.jetbrains.annotations.Nullable String icon, int num, @org.jetbrains.annotations.Nullable String audio, @NotNull String name,String json, int toPos) {
        if (context != null) {
            ((SessionActivity)context).sendGift(giftId,icon,num,audio,name,json,toPos);
        }
    }

    public void setIsCp(boolean isCp) {
        mIsSuperCp = isCp;
    }

    public void setCurrentItem(int pos) {
        if (viewpagerContainer != null) {
            viewpagerContainer.setCurrentItem(pos);
        }
    }

    public void updateCoinCount(String coin){
        this.coin = coin;
        if (coinCount != null) {
            coinCount.setText(coin);
        }
    }

    /**
     * 盲盒介绍h5
     * @param url
     */
    public void setBlindBoxH5(String url){
        this.blindBoxH5 = url;
    }

    private void initData() {
        Bundle arguments = getArguments();
        if (arguments != null) {
            mGiftBeans = (ArrayList<MsgUserModel.GiftBean>) arguments.get(GIFT_BEANS);
            mGiftNums = (ArrayList<MsgUserModel.GiftNums>) arguments.get(GIFT_NUMS);
            femaleGuestId = arguments.getString(Constants.FEMALE_GUEST_ID);
            giftChannel = arguments.getString(GIFT_CHANNEL);
            cpOrderId = arguments.getString(Constants.CP_ORDER_ID);
            fragments = new ArrayList<>();
            viewPagerTitle = new ArrayList<>();
            viewPagerTitle.add("礼物");
            if (mIsAddSuperCp) {
                viewPagerTitle.add("超级CP");
            }
            viewPagerTitle.add("背包");
            Bundle params = new Bundle();
            params.putSerializable(GIFT_NUMS,mGiftNums);
            params.putSerializable(GIFT_BEANS,mGiftBeans);
            params.putString(Constants.FEMALE_GUEST_ID,femaleGuestId);
            params.putString(GIFT_CHANNEL,giftChannel);
            Bundle paramsTwo = new Bundle();
            paramsTwo.putString(Constants.FEMALE_GUEST_ID,femaleGuestId);
            paramsTwo.putString(Constants.CP_ORDER_ID,cpOrderId);
            Bundle paramsThree = new Bundle();
            paramsThree.putSerializable(GIFT_NUMS,mGiftNums);
            paramsThree.putString(Constants.FEMALE_GUEST_ID,femaleGuestId);
            fragmentOne = GiftSendFragment.newInstance(params);
            fragmentTwo = BuyCpCardFragment.newInstance(paramsTwo);
            fragmentThree = BagFragment.Companion.newInstance(paramsThree);
            fragmentThree.setContentGpVisibility(true);
            fragmentOne.setGiftPresenterVisibly(true);
            fragments.add(fragmentOne);
            if (mIsAddSuperCp) {
                fragments.add(fragmentTwo);
            }
            fragments.add(fragmentThree);
            pagerAdapter = new GiftPagerAdapter(getChildFragmentManager(),fragments,viewPagerTitle);
            viewpagerContainer.setOffscreenPageLimit(fragments.size());
            viewpagerContainer.setAdapter(pagerAdapter);
            initMagicIndicator();
            initListener();
        }
    }

    public void updateSuperCpFragment(boolean isAddSuperCp) {
        mIsAddSuperCp = isAddSuperCp;
    }

    private void initListener() {
        recharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (rechargeCoinDialog == null) {
                    rechargeCoinDialog = new RechargeCoinDialog();
                }
                ((SessionActivity)context).getSupportFragmentManager().beginTransaction().remove(rechargeCoinDialog).commit();
                rechargeCoinDialog.setPositon(RechargeCoinDialog.POSITION_GIFT_ALERT);
                rechargeCoinDialog.show(((SessionActivity)context).getSupportFragmentManager(), RechargeCoinDialog.class.getName());
                rechargeCoinDialog.setWindowFrom("gift_jump_to_recharge");
            }
        });
        fragmentTwo.setListener(new BuyCpCardFragment.OnSendMessageListener() {
            @Override
            public void sendMsg(IMMessage inviteMessage, boolean resend) {
                SessionActivity activity = (SessionActivity)context;
                if (context != null && !activity.isDestroyed() && !activity.isFinishing()) {
                    activity.sendMessage(inviteMessage,false);
                }
                dismiss();
            }

            @Override
            public void fail() {
                dismiss();
            }

            @Override
            public void updateP2PUi() {
                SessionActivity activity = (SessionActivity)context;
                if (context != null && !activity.isDestroyed() && !activity.isFinishing()) {
                    activity.updateP2PUi();
                }
                dismiss();
            }
        });

        viewpagerContainer.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 1) {
                    SessionActivity activity = (SessionActivity)context;
                    if (context != null && !activity.isDestroyed() && !activity.isFinishing()) {
                        activity.hideActiveBoxKey();
                    }
                    trackUploadCpDialogExpourse();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        fragmentOne.setListener(new GiftSendFragment.OnTouchListener() {
            @Override
            public void onTouch() {

            }

            @Override
            public void giftSelect(int selectPos) {
                if (mGiftBeans != null && mGiftBeans.size() > 0) {
                    cl_blind_box.setVisibility(mGiftBeans.get(selectPos).getGiftType() == GiftTypeEnum.GIFT_BLIND ? View.VISIBLE : View.GONE);
                    tv_blind_box_tip.setText("赠送" + mGiftBeans.get(selectPos).name + "可随机开出不同价值礼物");
                }
            }
        });
        tv_entry_blind_box.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (StringUtil.isEmpty(blindBoxH5)){
                    return;
                }
                //盲盒 -查看礼物
                BlindBoxDetailsPW blindBoxDetailsPW = new BlindBoxDetailsPW(getActivity());
                blindBoxDetailsPW.setBlindBoxH5(blindBoxH5,false);
                blindBoxDetailsPW.showPopupWindow();
                MultiTracker.onEvent(TrackConstants.B_VIEW_BLIND_BOX_GIFT_CLICK);
            }
        });
    }

    private void trackUploadCpDialogExpourse() {
        HashMap<String,String> params = new HashMap<>();
        params.put(Constants.GUEST_ID,femaleGuestId);
        params.put(Constants.IS_SUPER_CP,String.valueOf(mIsSuperCp));
        MultiTracker.onEvent(TrackConstants.B_SUPER_CP_NAV_EXPOSURE,params);
    }

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    private void initMagicIndicator() {
        commonNavigatorAdapter = new CommonNavigatorAdapter() {
            @Override
            public int getCount() {
                return viewPagerTitle == null ? 0 : viewPagerTitle.size();
            }

            @Override
            public IPagerTitleView getTitleView(Context context, int index) {
                SimplePagerTitleView simplePagerTitleView = new SimplePagerTitleView(context);
                simplePagerTitleView.setText(viewPagerTitle.get(index));
                simplePagerTitleView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
                simplePagerTitleView.setNormalColor(getResources().getColor(R.color.color_ccffffff));
                simplePagerTitleView.setSelectedColor(getResources().getColor(R.color.color_10D8C8));
                simplePagerTitleView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        viewpagerContainer.setCurrentItem(index);
                    }
                });
                int padding = UIUtil.dip2px(context, 8);
                simplePagerTitleView.setPadding(padding, 0, padding, 0);
                return simplePagerTitleView;
            }

            @Override
            public IPagerIndicator getIndicator(Context context) {
                LinePagerIndicator indicator = new LinePagerIndicator(context);
                indicator.setMode(LinePagerIndicator.MODE_EXACTLY);
                indicator.setLineHeight(UIUtil.dip2px(context, 3));
                indicator.setLineWidth(UIUtil.dip2px(context, 20));
                indicator.setRoundRadius(UIUtil.dip2px(context, 1.5f));


                indicator.setColors(getResources().getColor(R.color.color_10D8C8));
                return indicator;
            }
        };
        commonNavigator = new CommonNavigator(context);
        commonNavigator.setAdjustMode(false);
        commonNavigator.setAdapter(commonNavigatorAdapter);
        magicIndicatorTab.setNavigator(commonNavigator);
        ViewPagerHelper.bind(magicIndicatorTab,viewpagerContainer);
    }

}
