package com.liquid.poros.ui.activity.setting;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.liquid.base.utils.GlobalConfig;
import com.liquid.poros.R;
import com.liquid.poros.base.BaseActivity;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.ui.activity.WebViewActivity;
import com.liquid.poros.ui.activity.scan.ScanCodeActivity;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.base.utils.LogOut;
import com.liquid.poros.utils.Utils;
import com.liquid.poros.utils.ViewUtils;
import com.liquid.poros.utils.network.FileProgressRequestBody;
import com.liquid.poros.utils.network.HttpCallback;
import com.liquid.poros.utils.network.RetrofitHttpManager;

import org.json.JSONObject;

import java.io.File;
import java.math.BigDecimal;

import butterknife.BindView;
import butterknife.OnClick;
import okhttp3.MultipartBody;

public class AboutPorosActivity extends BaseActivity implements FileProgressRequestBody.ProgressListener {
    @BindView(R.id.tv_version_name)
    TextView tv_version_name;
    @BindView(R.id.progress)
    ProgressBar progressBar;
    @BindView(R.id.iv_logo)
    ImageView iv_logo;
    private int entryScanCode = 0;
    @Override
    protected void parseBundleExtras(Bundle extras) {

    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.activity_about_poros;
    }

    @Override
    protected void initViewsAndEvents(Bundle savedInstanceState) {
        //渠道&版本号
        tv_version_name.setText(GlobalConfig.instance().getChannelName() + "  V" + ViewUtils.getAppVersionName(this));
        iv_logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                entryScanCode++;
                if (entryScanCode >= 3) {
                    readyGo(ScanCodeActivity.class);
                    entryScanCode = 0;
                }
            }
        });
    }

    @Override
    protected String getPageId() {
        return TrackConstants.PAGE_ABOUT_POROS;
    }

    @OnClick({R.id.rl_user_protocol, R.id.rl_privacy_policy, R.id.rl_upload_error})
    public void onClick(View view) {
        if (ViewUtils.isFastClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.rl_user_protocol:
                Bundle bundle = new Bundle();
                bundle.putString(Constants.TARGET_PATH, Constants.BASE_URL_H5 + "web/user_agreement_new.html");
                readyGo(WebViewActivity.class, bundle);
                break;
            case R.id.rl_privacy_policy:
                Bundle bundle1 = new Bundle();
                bundle1.putString(Constants.TARGET_PATH, Constants.BASE_URL_H5 + "web/user_conceal_new.html");
                readyGo(WebViewActivity.class, bundle1);
                break;
            case R.id.rl_upload_error:
                showLoading();
                new Thread() {
                    @Override
                    public void run() {
                        final String filepath = Utils.compressFile(AboutPorosActivity.this);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (progressBar != null) {
                                    progressBar.setVisibility(View.VISIBLE);
                                    uploadLog(filepath, AccountUtil.getInstance().getUserId());
                                }
                            }
                        });
                    }
                }.start();
                break;
        }
    }

    public void uploadLog(final String path, String userId) {
        try {
            MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);//表单类型
            File file = new File(path);//filePath 图片地址
            FileProgressRequestBody requestBody = new FileProgressRequestBody(file, "multipart/form-data", this);

            builder.addFormDataPart("user_id", userId);
            builder.addFormDataPart("log_file", file.getName(), requestBody);
            MultipartBody body = builder.build();
            RetrofitHttpManager.getInstance().httpInterface.upload_log(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    LogOut.debug("user_upload_image = " + result);
                    if (isDestroyed() || isFinishing()) {
                        return;
                    }
                    progressBar.setVisibility(View.GONE);
                    try {
                        JSONObject object = new JSONObject(result);
                        int code = object.getInt("code");
                        String message = object.getString("msg");
                        closeLoading();
                        if (code == 0) {
                            showMessage(getString(R.string.about_poros_upload_log_success));
                        } else {
                            showMessage(message);
                        }
                    } catch (Exception e) {

                    }
                }

                @Override
                public void OnFailed(int ret, String result) {
                    LogOut.debug("EditUserData--error = " + result);
                    if (isDestroyed() || isFinishing()) {
                        return;
                    }
                    progressBar.setVisibility(View.GONE);
                    closeLoading();
                    showMessage(getString(R.string.about_poros_upload_log_fail));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void transferred(long total, long size) {
        if (progressBar != null) {
            BigDecimal soFar = new BigDecimal(total);
            BigDecimal total1 = new BigDecimal(size);
            double p = soFar.divide(total1, 2, BigDecimal.ROUND_HALF_UP).doubleValue();
            int progress = (int) (p * 100);
            progressBar.setProgress(progress);
        }
    }
}
