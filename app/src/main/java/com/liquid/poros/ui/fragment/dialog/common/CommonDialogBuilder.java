package com.liquid.poros.ui.fragment.dialog.common;

import android.content.pm.ActivityInfo;
import android.os.Bundle;

import com.liquid.poros.constant.Constants;

/**
 * 通用弹窗builder
 * 参数可选 设置完后create 再调用show即可
 */
public class CommonDialogBuilder {
    private int imageTitle;// 图片标题
    private int topImageTitle;// 顶部图片标题
    private String title; //标题
    private String middleTitle; //中部标题
    private String desc;  //描述
    private CharSequence descSequence;  //描述
    private boolean withCancel = true; //是否有取消
    private String confirm = "确认"; // 确认键描述
    private int confirmRes; // 确认键背景
    private String cancel = "取消"; // 确认键描述
    private boolean cancelAble = true; // 确认键描述

    private int orientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
    private CommonDialogFragment mCommonDialogFragment = new CommonDialogFragment();
    private CommonActionListener listener; //点击事件监听

    public CommonDialogBuilder() {
    }

    //静态内部类
    public static CommonDialogBuilder getInstance() {
        return CommonDialog.mInstance == null ? CommonDialog.mInstance = new CommonDialogBuilder() : CommonDialog.mInstance;
    }

    public static class CommonDialog {
        public static CommonDialogBuilder mInstance = new CommonDialogBuilder();
    }

    public CommonDialogBuilder setTitle(String title) {
        this.title = title;
        return this;
    }
    public CommonDialogBuilder setMiddleTitle(String middleTitle) {
        this.middleTitle = middleTitle;
        return this;
    }
    public CommonDialogBuilder setImageTitleRes(int res) {
        this.imageTitle = res;
        return this;
    }
    public  CommonDialogBuilder setTopImageRes(int res){
        this.topImageTitle = res;
        return this;
    }

    public CommonDialogBuilder setDesc(String desc) {
        this.desc = desc;
        return this;
    }

    public CommonDialogBuilder setDesc(CharSequence desc) {
        this.descSequence = desc;
        return this;
    }

    public CommonDialogBuilder setWithCancel(boolean withCancel) {
        this.withCancel = withCancel;
        return this;

    }

    public CommonDialogBuilder setOrientation(int orientation) {
        this.orientation = orientation;
        return this;
    }

    public CommonDialogBuilder setCancelAble(boolean cancelAble) {
        this.cancelAble = cancelAble;
        return this;
    }

    public CommonDialogBuilder setConfirm(String confirm) {
        this.confirm = confirm;
        return this;

    }

    public CommonDialogBuilder setConfirmBg(int confirmRes) {
        this.confirmRes = confirmRes;
        return this;

    }

    public CommonDialogBuilder setCancel(String cancel) {
        this.cancel = cancel;
        return this;

    }

    public CommonDialogBuilder setListener(CommonActionListener listener) {
        this.listener = listener;
        return this;
    }

    public CommonDialogFragment create() {
        Bundle bundle = new Bundle();
        bundle.putInt(Constants.PARAMS_IMAGE_TITLE, imageTitle);
        bundle.putInt(Constants.PARAMS_TOP_IMAGE_TITLE, topImageTitle);
        bundle.putString(Constants.PARAMS_TITLE, title);
        bundle.putString(Constants.PARAMS_MIDDLE_TITLE, middleTitle);
        bundle.putString(Constants.PARAMS_DESC, desc);
        bundle.putCharSequence(Constants.PARAMS_DESC_SEQUENCE, descSequence);
        bundle.putBoolean(Constants.PARAMS_CANCEL_ABLE, cancelAble);
        bundle.putBoolean(Constants.PARAMS_WITH_CANCEL, withCancel);
        bundle.putString(Constants.PARAMS_CONFIRM, confirm);
        bundle.putInt(Constants.PARAMS_CONFIRM_BACKGROUND, confirmRes);
        bundle.putString(Constants.PARAMS_CANCEL, cancel);
        bundle.putInt(Constants.PARAMS_ORIENTATION, orientation);
        mCommonDialogFragment.setCommonActionListener(listener);
        mCommonDialogFragment.setArguments(bundle);
        return mCommonDialogFragment;
    }
}
