package com.liquid.poros.ui.activity.contacts;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.liquid.poros.R;
import com.liquid.poros.adapter.ContactAdapter;
import com.liquid.poros.base.BaseActivity;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.contract.contacts.ContactsContract;
import com.liquid.poros.contract.contacts.ContactsPresenter;
import com.liquid.poros.entity.ContactsInfo;
import com.liquid.poros.helper.JinquanResourceHelper;
import com.liquid.poros.ui.activity.user.ReportActivity;
import com.liquid.poros.ui.popupwindow.FriendMessagePopWindow;
import com.liquid.poros.widgets.LetterView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;

@Deprecated
public class ContactsActivity extends BaseActivity implements ContactsContract.View, ContactAdapter.Listener, FriendMessagePopWindow.Listener {
    @BindView(R.id.contact_list)
    RecyclerView contactList;
    private LinearLayoutManager layoutManager;
    @BindView(R.id.letter_view)
    LetterView letterView;
    @BindView(R.id.tv_contact_point)
    TextView tv_contact_point;
    @BindView(R.id.container)
    View container;
    @BindView(R.id.ll_empty_result)
    View ll_empty_result;
    private ContactAdapter adapter;
    private ContactsPresenter mPresenter;
    private String mSessionId;


    @Override
    protected void parseBundleExtras(Bundle extras) {

    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.activity_contacts;
    }

    @Override
    protected void initViewsAndEvents(Bundle savedInstanceState) {
        mPresenter = new ContactsPresenter();
        mPresenter.attachView(this);
        mPresenter.getFriendList();
        layoutManager = new LinearLayoutManager(this);
        adapter = new ContactAdapter(this);
        contactList.setLayoutManager(layoutManager);
        contactList.setAdapter(adapter);
        adapter.setOnListening(this);
        letterView.setOnTouchingLetterChangedListener(new LetterView.OnTouchingLetterChangedListener() {
            @Override
            public void onTouchingLetterChanged(String s, int c, float height) {
                tv_contact_point.setVisibility(View.VISIBLE);
                layoutManager.scrollToPositionWithOffset(adapter.getScrollPosition(s), 0);
                if (c > 27) {
                    tv_contact_point.setY(height * 27);
                } else {
                    tv_contact_point.setY((height * c) + 300);
                }
                tv_contact_point.setText(s);
            }

            @Override
            public void onTouchCancel() {
                tv_contact_point.setVisibility(View.INVISIBLE);
            }
        });
    }

    @Override
    protected String getPageId() {
        return TrackConstants.PAGE_CONTACTS;
    }

    @Override
    public void onContactFetch(ContactsInfo contactsInfo) {
        if (contactsInfo.getData().size() > 0) {
            contactList.setVisibility(View.VISIBLE);
            ll_empty_result.setVisibility(View.GONE);
            letterView.setVisibility(View.VISIBLE);
            String[] contactNames = new String[contactsInfo.getData().size()];
            for (int i = 0; i < contactsInfo.getData().size(); i++) {
                contactNames[i] = contactsInfo.getData().get(i).getFirst_letter();
            }
            adapter.updateData(contactNames, contactsInfo.getData());
        } else {
            contactList.setVisibility(View.GONE);
            ll_empty_result.setVisibility(View.VISIBLE);
            letterView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onDeleteFriendFetch() {
        mPresenter.getFriendList();
    }


    @Override
    public void onItemLongClicked(String sessionId, View view, View contentView) {
        mSessionId = sessionId;
        FriendMessagePopWindow friendMessagePopWindow = new FriendMessagePopWindow(mContext);
        friendMessagePopWindow.setOnListening(this);
        friendMessagePopWindow.showPop(view, false, false);
        JinquanResourceHelper.getInstance(ContactsActivity.this).setBackgroundColorByAttr(contentView, R.attr.bg_item_message);
    }

    @Override
    public void onItemClicked(String text) {
        switch (text) {
            case "删除好友":
                mPresenter.deleteFriend(mSessionId);
                break;
            case "举报":
                Bundle bundle = new Bundle();
                bundle.putString(Constants.USER_ID, mSessionId);
                readyGo(ReportActivity.class, bundle);
                break;
        }
    }

    @Override
    public void onDismiss() {
        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }
}
