package com.liquid.poros.ui.activity.scan;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.liquid.poros.R;
import com.liquid.poros.base.BaseActivity;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.ui.activity.WebViewActivity;
import com.yzq.zxinglibrary.android.CaptureActivity;
import com.yzq.zxinglibrary.common.Constant;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.OnClick;

/**
 * 扫码
 */
public class ScanCodeActivity extends BaseActivity {
    @BindView(R.id.tv_scan_code)
    TextView tv_scan_code;
    @Override
    protected void parseBundleExtras(Bundle extras) {

    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.activity_scan_code;
    }

    @Override
    protected String getPageId() {
        return null;
    }

    @Override
    protected void initViewsAndEvents(Bundle savedInstanceState) {

    }

    @OnClick({R.id.tv_scan_code})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_scan_code:
                Intent intent = new Intent(this, CaptureActivity.class);
                startActivityForResult(intent, 100);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == 100) {
            if (data != null) {
                String content = data.getStringExtra(Constant.CODED_CONTENT);
                Bundle bundle = new Bundle();
                bundle.putString(Constants.TARGET_PATH, content);
                readyGo(WebViewActivity.class, bundle);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
