package com.liquid.poros.ui.fragment.dialog.live.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.liquid.library.retrofitx.RetrofitX
import com.liquid.poros.entity.CpLiveGiftRankInfo
import com.liquid.poros.entity.LiveGiftRankInfo
import com.liquid.poros.http.ApiUrl
import com.liquid.poros.http.observer.ObjectObserver
import com.liquid.poros.http.utils.getPoros

/**
 * 视频房礼物排行榜
 */
class LiveGiftRankViewModel : ViewModel() {
    val ptaSuitListLiveData: MutableLiveData<LiveGiftRankInfo> by lazy { MutableLiveData<LiveGiftRankInfo>() }
    val failed: MutableLiveData<LiveGiftRankInfo> by lazy { MutableLiveData<LiveGiftRankInfo>() }
    val cpGiftRankListSuccess: MutableLiveData<CpLiveGiftRankInfo> by lazy { MutableLiveData<CpLiveGiftRankInfo>() }
    val cpGiftRankListFailed: MutableLiveData<CpLiveGiftRankInfo> by lazy { MutableLiveData<CpLiveGiftRankInfo>() }


    /**
     * 获取pk房礼物排行榜
     */
    fun getPkGiftRankList(roomId: String, position: Int, page: Int, pageSize: Int) {
        RetrofitX.url(ApiUrl.PK_GIFT_RANK_LIST)
                .param("room_id", roomId)
                .param("position", position)
                .param("page_idx", page)
                .param("page_size", pageSize)
                .getPoros()
                .subscribe(object : ObjectObserver<LiveGiftRankInfo>() {
                    override fun success(result: LiveGiftRankInfo) {
                        ptaSuitListLiveData.postValue(result)
                    }

                    override fun failed(result: LiveGiftRankInfo) {
                        failed.postValue(result)
                    }

                })
    }

    /**
     * 获取cp房礼物排行榜
     */
    fun getCpGiftRankList(roomId: String?, position: Int) {
        RetrofitX.url(ApiUrl.CP_GIFT_RANK_LIST)
                .param("room_id", roomId)
                .param("position", position)
                .param("page_idx", 0)
                .param("page_size", 100)
                .getPoros()
                .subscribe(object : ObjectObserver<CpLiveGiftRankInfo>() {
                    override fun success(result: CpLiveGiftRankInfo) {
                        cpGiftRankListSuccess.postValue(result)
                    }

                    override fun failed(result: CpLiveGiftRankInfo) {
                        cpGiftRankListFailed.postValue(result)
                    }

                })
    }
}