package com.liquid.poros.ui.activity.account;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alipay.sdk.app.PayTask;
import com.liquid.base.adapter.CommonAdapter;
import com.liquid.base.adapter.CommonViewHolder;
import com.liquid.base.event.EventCenter;
import com.liquid.poros.R;
import com.liquid.poros.alipay.PayResult;
import com.liquid.poros.base.BaseActivity;
import com.liquid.poros.analytics.utils.MultiTracker;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.contract.account.RechargeContract;
import com.liquid.poros.contract.account.RechargePresenter;
import com.liquid.poros.entity.AlipayInfo;
import com.liquid.poros.entity.RechargeInfo;
import com.liquid.poros.entity.RechargeListInfo;
import com.liquid.poros.entity.RechargeResultInfo;
import com.liquid.poros.entity.RechargeWayInfo;
import com.liquid.poros.entity.UserCoinInfo;
import com.liquid.poros.entity.WeChatPayInfo;
import com.liquid.poros.ui.fragment.dialog.account.PayWaysChooseDialogFragment;
import com.liquid.poros.ui.fragment.dialog.common.CommonActionListener;
import com.liquid.poros.ui.fragment.dialog.common.CommonDialogBuilder;
import com.liquid.poros.ui.fragment.dialog.common.CommonDialogFragment;
import com.liquid.poros.utils.NumberFormatUtil;
import com.liquid.poros.utils.StringUtil;
import com.liquid.poros.utils.ViewUtils;
import com.liquid.poros.widgets.WrapContentLinearLayoutManager;
import com.liquid.poros.wxapi.WXUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;

public class RechargeActivity extends BaseActivity implements RechargeContract.View {
    @BindView(R.id.tv_balance)
    TextView tv_balance;
    @BindView(R.id.tv_game_card_count)
    TextView tv_game_card_count;
    @BindView(R.id.tv_mic_card_count)
    TextView tv_mic_card_count;
    @BindView(R.id.tv_recharge_tips)
    TextView tv_recharge_tips;
    @BindView(R.id.mic_card_container)
    LinearLayout mic_card_container;
    @BindView(R.id.tv_recharge_details)
    TextView tv_recharge_details;
    @BindView(R.id.rv_recharge)
    RecyclerView rv_recharge;
    @BindView(R.id.tv_withdrawal_balance)
    TextView tv_withdrawal_balance;
    private RechargePresenter mPresenter;
    private CommonAdapter<RechargeListInfo> mCommonAdapter;
    private List<RechargeWayInfo> rechargeWayInfoList = new ArrayList<>();
    private String mOrderId;
    private String mUnit;
    private static final int SDK_PAY_FLAG = 1;

    @Override
    protected void parseBundleExtras(Bundle extras) {
    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.activity_recharge;
    }

    @Override
    protected void initViewsAndEvents(Bundle savedInstanceState) {
        if (Constants.hide_peking_user) {
            tv_recharge_details.setVisibility(View.GONE);
            mic_card_container.setVisibility(View.GONE);
        }else {
            tv_recharge_details.setVisibility(View.VISIBLE);
            mic_card_container.setVisibility(View.VISIBLE);
        }
        rv_recharge.setLayoutManager(new WrapContentLinearLayoutManager(RechargeActivity.this));
        mCommonAdapter = new CommonAdapter<RechargeListInfo>(RechargeActivity.this, R.layout.item_choose_recharge_number) {
            @Override
            public void convert(CommonViewHolder holder, RechargeListInfo rechargeListInfo, int pos) {
                TextView tv_recharge_coin = holder.getView(R.id.tv_recharge_coin);
                TextView tv_recharge_money = holder.getView(R.id.tv_recharge_money);
                TextView tv_recharge_ori_money = holder.getView(R.id.tv_recharge_ori_money);
                if (!TextUtils.isEmpty(mUnit)) {
                    tv_recharge_coin.setText(StringUtil.matcherSearchText(true, getThemeTag() == 1 ? getResources().getColor(R.color.custom_color_main_title_color_day) : getResources().getColor(R.color.custom_color_main_title_color_night), rechargeListInfo.getCoin() + " " + mUnit, mUnit));
                } else {
                    tv_recharge_coin.setText(String.valueOf(rechargeListInfo.getCoin()));
                    tv_recharge_coin.setTextColor(getThemeTag() == 1 ? getResources().getColor(R.color.custom_color_main_title_color_day) : getResources().getColor(R.color.custom_color_main_title_color_night));
                }
                holder.setVisibility(R.id.tv_recharge_coin_tip, TextUtils.isEmpty(rechargeListInfo.getHint()) ? View.GONE : View.VISIBLE);
                holder.setText(R.id.tv_recharge_coin_tip, rechargeListInfo.getHint());
                holder.setText(R.id.tv_recharge_coin_tag, rechargeListInfo.getTag());
                holder.setVisibility(R.id.tv_recharge_coin_tag, TextUtils.isEmpty(rechargeListInfo.getTag()) ? View.GONE : View.VISIBLE);
                long money = rechargeListInfo.getMoney();
                BigDecimal bigDecimal = new BigDecimal(money);
                BigDecimal number = bigDecimal.divide(new BigDecimal(100), 10, BigDecimal.ROUND_HALF_DOWN);
                String rechargeMoney;
                if (new BigDecimal(number.intValue()).compareTo(number) == 0) {
                    rechargeMoney = "￥" + money / 100;
                } else {
                    rechargeMoney = "￥" + bigDecimal.divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_UP);
                }
                tv_recharge_money.setText(rechargeMoney);
                if (!TextUtils.isEmpty(String.valueOf(rechargeListInfo.getOri_money())) && !"0".equals(String.valueOf(rechargeListInfo.getOri_money()))) {
                    tv_recharge_ori_money.setVisibility(View.VISIBLE);
                    long oriMoney = rechargeListInfo.getOri_money();
                    BigDecimal oriBigDecimal = new BigDecimal(oriMoney);
                    BigDecimal oriNumber = oriBigDecimal.divide(new BigDecimal(100), 10, BigDecimal.ROUND_HALF_DOWN);
                    if (new BigDecimal(oriNumber.intValue()).compareTo(oriNumber) == 0) {
                        tv_recharge_ori_money.setText("￥" + oriMoney / 100);
                    } else {
                        tv_recharge_ori_money.setText("￥" + oriBigDecimal.divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_UP));
                    }
                    tv_recharge_ori_money.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                } else {
                    tv_recharge_ori_money.setVisibility(View.GONE);
                }
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showPayWaysChooseDialog(rechargeListInfo.getType(), String.valueOf(rechargeListInfo.getCoin()), rechargeMoney, money, rechargeWayInfoList);
                    }
                });
            }
        };
        rv_recharge.setAdapter(mCommonAdapter);
        mPresenter = new RechargePresenter();
        mPresenter.attachView(this);
        mPresenter.rechargeInfo(null);
    }

    @Override
    protected boolean needSetTransparent() {
        return false;
    }

    @Override
    protected String getPageId() {
        return TrackConstants.PAGE_WALLET;
    }

    @OnClick({R.id.iv_left, R.id.tv_recharge_details,R.id.rl_withdrawal})
    public void onClick(View view) {
        if (ViewUtils.isFastClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_left:
                finish();
                break;
            case R.id.tv_recharge_details:
                //账单详情
                readyGo(RechargeDetailsActivity.class);
                break;
            case R.id.rl_withdrawal:
                //提现
                Bundle bundle = new Bundle();
                bundle.putBoolean(Constants.START_WITHDRAWAL_TYPE, true);
                readyGo(WithdrawalActivity.class, bundle);
                break;
        }
    }

    @Override
    protected void onEventComing(EventCenter eventCenter) {
        super.onEventComing(eventCenter);
        switch (eventCenter.getEventCode()) {
            case Constants.EVENT_CODE_PAY_RECHARGE:
                int baseRespCode = (int) eventCenter.getData();
                if (baseRespCode != 0) {//订单取消
                    if (!TextUtils.isEmpty(mOrderId)) {
                        mPresenter.closeRechargeOrder(mOrderId);
                    }
                }
                if (!TextUtils.isEmpty(mOrderId)) {
                    mPresenter.rechargeResult(mOrderId);
                }
                break;
            case Constants.EVENT_CODE_WITHDRAW_SUCCESS:
                if (mPresenter != null) {
                    mPresenter.rechargeInfo(null);
                }
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.getUserCoin();
    }

    private void showPayWaysChooseDialog(String orderType, String coin, String money, long payMoney, List<RechargeWayInfo> rechargeWaysList) {
        if (isFinishing() || isDestroyed()) {
            return;
        }
        PayWaysChooseDialogFragment.newInstance().setRechargeWays(orderType, String.valueOf(coin), money, payMoney, rechargeWaysList).setRechargeWaysListener(new PayWaysChooseDialogFragment.PayWaysChooseListener() {
            @Override
            public void wechatOrder(int payMoney, String orderType) {
                mPresenter.wechatOrder(payMoney, orderType);
            }

            @Override
            public void aliPayOrder(int payMoney, String orderType) {
                mPresenter.alipayOrder(payMoney, orderType);
            }
        }).show(getSupportFragmentManager(), PayWaysChooseDialogFragment.class.getSimpleName());

    }

    @SuppressLint("ResourceType")
    @Override
    public void onRechargeInfoFetch(RechargeInfo rechargeInfo) {
        if (rechargeInfo == null || rechargeInfo.getData() == null) {
            return;
        }
        rechargeWayInfoList.clear();
        RechargeInfo.Data rechargeInfos = rechargeInfo.getData();
        if (rechargeInfos.getRecharge_list() != null && rechargeInfos.getRecharge_list().size() > 0) {
            mCommonAdapter.update(rechargeInfos.getRecharge_list());
        }
        if (rechargeInfos.getRecharge_way() != null) {
            rechargeWayInfoList.addAll(rechargeInfos.getRecharge_way());
        }
        tv_recharge_tips.setText(rechargeInfo.getData().getTips());
        mUnit = rechargeInfo.getData().getUnit();
        //提现余额
        tv_withdrawal_balance.setText(StringUtil.matcherSearchText(true, getThemeTag() == 1 ? Color.parseColor(getString(R.color.custom_color_main_title_color_day)) : Color.parseColor(getString(R.color.custom_color_main_title_color_night)), NumberFormatUtil.balanceFormat(rechargeInfos.getCach_balance()) + "元", "元"));
    }

    @Override
    public void onWechatSuccessFetch(WeChatPayInfo weChatPayInfo) {
        if (weChatPayInfo == null || weChatPayInfo.getData() == null || weChatPayInfo.getData().getRecharge_data() == null) {
            return;
        }
        WeChatPayInfo.Data.RechargeData rechargeData = weChatPayInfo.getData().getRecharge_data();
        mOrderId = weChatPayInfo.getData().getOrder_id();
        WXUtils.getInstance(RechargeActivity.this)
                .recharge(rechargeData.getAppid()
                        , rechargeData.getPartnerid()
                        , rechargeData.getPrepayid()
                        , rechargeData.getNoncestr()
                        , rechargeData.getTimestamp()
                        , rechargeData.getPackageValue()
                        , rechargeData.getSign());
    }

    @Override
    public void onAlipaySuccessFetch(AlipayInfo alipayInfo) {
        if (alipayInfo == null || alipayInfo.getData() == null || alipayInfo.getData().getRecharge_data() == null) {
            return;
        }
        mOrderId = alipayInfo.getData().getOrder_id();
        String order_string = alipayInfo.getData().getRecharge_data().getOrder_string();
        Runnable payRunnable = new Runnable() {
            @Override
            public void run() {
                PayTask alipay = new PayTask(RechargeActivity.this);
                Map<String, String> result = alipay.payV2(order_string, true);
                Message msg = new Message();
                msg.what = SDK_PAY_FLAG;
                msg.obj = result;
                mHandler.sendMessage(msg);
            }
        };
        // 必须异步调用
        Thread payThread = new Thread(payRunnable);
        payThread.start();
    }

    private Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case SDK_PAY_FLAG:
                    if (TextUtils.isEmpty(mOrderId)) {
                        return;
                    }
                    PayResult payResult = new PayResult((Map<String, String>) msg.obj);
                    String resultStatus = payResult.getResultStatus();
                    if (!StringUtil.isEquals(resultStatus, "9000")) {
                        //订单取消/失败
                        mPresenter.closeRechargeOrder(mOrderId);
                    }
                    mPresenter.rechargeResult(mOrderId);
                    break;
            }
        }
    };

    @Override
    public void onRechargeResultFetch(RechargeResultInfo rechargeResultInfo) {
        if (rechargeResultInfo.getCode() == 0) {
            mPresenter.rechargeInfo(null);
            EventBus.getDefault().post(new EventCenter(Constants.EVENT_CODE_COIN_CHANGE));
            //充值成功
            getHandler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (isFinishing() || isDestroyed()) {
                        return;
                    }
                    new CommonDialogBuilder()
                            .setWithCancel(true)
                            .setImageTitleRes(R.mipmap.icon_recharge_success)
                            .setDesc(StringUtil.matcherSearchText(false, Color.parseColor("#FFD13D"), "恭喜您！成功充值" + rechargeResultInfo.getData().getCoin() + "金币", rechargeResultInfo.getData().getCoin()))
                            .setCancel(getString(R.string.recharge_keep))
                            .setListener(new CommonActionListener() {
                                @Override
                                public void onConfirm() {
                                }

                                @Override
                                public void onCancel() {

                                }
                            }).create().show(getSupportFragmentManager(), CommonDialogFragment.class.getSimpleName());
                    MultiTracker.onEvent(TrackConstants.P_PAYMENT_SUCCESS);
                }
            }, 200);
        } else if (rechargeResultInfo.getCode() == 16006) {
            //充值失败
            getHandler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (isFinishing() || isDestroyed()) {
                        return;
                    }
                    new CommonDialogBuilder()
                            .setWithCancel(true)
                            .setImageTitleRes(R.mipmap.icon_recharge_fail)
                            .setDesc(getString(R.string.recharge_fail_tip))
                            .setConfirm(getString(R.string.recharge_again))
                            .setListener(new CommonActionListener() {
                                @Override
                                public void onConfirm() {
                                }

                                @Override
                                public void onCancel() {

                                }
                            }).create().show(getSupportFragmentManager(), CommonDialogFragment.class.getSimpleName());
                    MultiTracker.onEvent(TrackConstants.P_PAYMENT_FAILED);
                }
            }, 200);

        } else if (rechargeResultInfo.getCode() == 16007) {
            //充值处理中

        }
    }

    @SuppressLint("ResourceType")
    @Override
    public void onUserCoinSuccessFetch(UserCoinInfo userCoinInfo) {
        if (userCoinInfo == null || userCoinInfo.getData() == null) {
            return;
        }
        UserCoinInfo.Data userCoinInfos = userCoinInfo.getData();
        //金币余额
        tv_balance.setText(StringUtil.matcherSearchText(true, Color.parseColor(getString(R.color.white)), userCoinInfos.getCoin() + " " + "枚", " " + "枚"));
        if (userCoinInfos.getMic_cards() != null && StringUtil.isNotEmpty(userCoinInfos.getMic_cards().getCount())) {
            //上麦卡
            tv_mic_card_count.setText(StringUtil.matcherSearchText(true, Color.parseColor(getString(R.color.cyan)), userCoinInfos.getMic_cards().getCount() + " " + userCoinInfos.getMic_cards().getUnit(), " " + userCoinInfos.getMic_cards().getUnit()));
        }
        if (userCoinInfos.getTeam_cards() != null && StringUtil.isNotEmpty(userCoinInfos.getTeam_cards().getCount())) {
            //视频卡
            tv_game_card_count.setText(StringUtil.matcherSearchText(true, Color.parseColor(getString(R.color.cyan)), userCoinInfos.getTeam_cards().getCount() + " " + userCoinInfos.getTeam_cards().getUnit(), " " + userCoinInfos.getTeam_cards().getUnit()));
        }
    }

    @Override
    public void onCloseRechargeOrderFetch() {
        mPresenter.rechargeInfo(null);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mHandler != null) {
            mHandler.removeCallbacksAndMessages(null);
            mHandler = null;
        }
        mPresenter.detachView();
    }

}
