package com.liquid.poros.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.liquid.base.tools.GT;
import com.liquid.base.utils.ContextUtils;
import com.liquid.poros.R;
import com.liquid.poros.base.BaseActivity;
import com.liquid.poros.analytics.utils.MultiTracker;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.entity.CpLevelUpDetail;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.poros.utils.StringUtil;
import com.liquid.poros.utils.ViewUtils;
import com.liquid.poros.utils.network.HttpCallback;
import com.liquid.poros.utils.network.RetrofitHttpManager;

import org.json.JSONObject;

import java.util.HashMap;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.OnClick;
import okhttp3.RequestBody;

/**
 * CP晋级宣言页面
 */
public class DeclarationActivity extends BaseActivity {

    @BindView(R.id.et_count_tv)
    TextView et_count_tv;
    @BindView(R.id.cp_declaration_et)
    EditText cp_declaration_et;
    @BindView(R.id.change_declaration_ll)
    LinearLayout change_declaration_ll;
    @BindView(R.id.cp_declaration_comfirm_tv)
    TextView cp_declaration_comfirm_tv;
    @BindView(R.id.iv_back)
    ImageView iv_back;

    private String female_user_id;
    private String female_user_head;
    private String rise_to;
    private String intimacy;

    @Override
    protected String getPageId() {
        return TrackConstants.PAGE_DECLARATION;
    }

    @Override
    protected void parseBundleExtras(Bundle extras) {
        female_user_id = extras.getString(Constants.SESSION_ID);
        female_user_head = extras.getString("target_user_head");

        rise_to = extras.getString("rise_to");
        intimacy = extras.getString("intimacy");
    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.acitivity_declaration;
    }
    private final int num = 100;
    @Override
    protected void initViewsAndEvents(Bundle savedInstanceState) {
        if(cp_declaration_et != null){
            cp_declaration_et.addTextChangedListener(new TextWatcher() {
                private CharSequence wordNum;//记录输入的字数
                private int selectionStart;
                private int selectionEnd;
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    wordNum= s;//实时记录输入的字数
                }

                @Override
                public void afterTextChanged(Editable s) {
                    et_count_tv.setText(s.length() + "/" + num);
                    //fix  bugly上角标越界bug，移动到布局中使用
//                    selectionStart=cp_declaration_et.getSelectionStart();
//                    selectionEnd = cp_declaration_et.getSelectionEnd();
//                    if (wordNum.length() > num) {
//                        //删除多余输入的字（不会显示出来）
//                        s.delete(selectionStart - 1, selectionEnd);
//                        int tempSelection = selectionEnd;
//                        cp_declaration_et.setText(s);
//                        cp_declaration_et.setSelection(tempSelection);//设置光标在最后
//                    }
                }
            });
        }

        showLoading();
        checkCpLevelDetail();

        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", AccountUtil.getInstance().getUserId());
        params.put("female_guest_id", female_user_id);
        params.put("event_time", String.valueOf(System.currentTimeMillis()));
        params.put("rise_to",rise_to);
        params.put("intimacy", intimacy);
        MultiTracker.onEvent("b_exposure_write_oath_page", params);
    }

    @OnClick({R.id.iv_back, R.id.change_declaration_ll, R.id.cp_declaration_comfirm_tv})
    public void onClick(View view) {
        if (ViewUtils.isFastClick()) {
            return;
        }

        switch (view.getId()) {
            case R.id.change_declaration_ll:
                getRandomSwear();
                break;
            case R.id.iv_back:
                finish();
                break;
            case R.id.cp_declaration_comfirm_tv:
                String etString = cp_declaration_et.getText().toString();

                if(etString != null && StringUtil.isEmpty(etString)){
                    Toast.makeText(ContextUtils.getApplicationContext(), "宣言不能为空" , Toast.LENGTH_SHORT).show();
                    return;
                }

                Bundle bundl1 = new Bundle();
                bundl1.putString("et_string", etString);
                bundl1.putString(Constants.SESSION_ID, female_user_id);
                bundl1.putString("target_user_head", female_user_head);
                bundl1.putString("rise_to",rise_to);
                bundl1.putString("intimacy", intimacy);
                readyGoForResult(DeclarationKeepsakeActivity.class,1005, bundl1);

                HashMap<String, String> params = new HashMap<>();
                params.put("user_id", AccountUtil.getInstance().getUserId());
                params.put("female_guest_id", female_user_id);
                params.put("event_time", String.valueOf(System.currentTimeMillis()));
                params.put("rise_to",rise_to);
                params.put("intimacy", intimacy);
                MultiTracker.onEvent("b_button_write_oath_next", params);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == 1005) {
            setResult(RESULT_OK);
            finish();
        }
    }

    private void checkCpLevelDetail (){
        try {
            JSONObject object = new JSONObject();
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("female_user_id",female_user_id);
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.checkCpLevelDetail(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    closeLoading();
                    CpLevelUpDetail cpLevelUpDetail = GT.fromJson(result, CpLevelUpDetail.class);
                    if(cpLevelUpDetail != null){
                        CpLevelUpDetail.DataBean dataBean = cpLevelUpDetail.getData();
                        if (dataBean != null && cp_declaration_et != null) {
                            cp_declaration_et.setText(dataBean.getSwear_words());
                        }

                        int code = cpLevelUpDetail.getCode();
                        if(code != 0){
                            String msg = cpLevelUpDetail.getMsg();
                            if(com.liquid.base.tools.StringUtil.isNotEmpty(msg)){
                                Toast.makeText(ContextUtils.getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }

                @Override
                public void OnFailed(int ret, String result) {
                    closeLoading();
                }
            });
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    private void getRandomSwear(){
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.getRandomSwear(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    try {
                        JSONObject obj = new JSONObject(result);
                        int code = obj.getInt("code");
                        if(code == 0){
                            JSONObject dataObj = obj.getJSONObject("data");
                            String swear_words = dataObj.getString("swear_words");
                            if(StringUtil.isNotEmpty(swear_words)){
                                cp_declaration_et.setText(swear_words);
                            }
                        }
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void OnFailed(int ret, String result) {

                }
            });
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
}
