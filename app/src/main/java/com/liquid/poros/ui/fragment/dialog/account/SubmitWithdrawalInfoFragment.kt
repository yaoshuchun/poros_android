package com.liquid.poros.ui.fragment.dialog.account

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.TextUtils
import android.util.DisplayMetrics
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.liquid.base.event.EventCenter
import com.liquid.base.utils.ToastUtil
import com.liquid.poros.R
import com.liquid.poros.base.BaseDialogFragment
import com.liquid.poros.constant.Constants
import com.liquid.poros.constant.TrackConstants
import com.liquid.poros.entity.BaseBean
import com.liquid.poros.ui.fragment.dialog.account.viewmodel.SubmitWithdrawalInfoModel
import com.liquid.poros.utils.NumberCheckUtil
import com.liquid.poros.utils.ViewUtils
import de.greenrobot.event.EventBus
import kotlinx.android.synthetic.main.fragment_submit_withdrawal_dialog.*

/**
 * 实名认证弹窗
 */
class SubmitWithdrawalInfoFragment : BaseDialogFragment(), View.OnClickListener {
    private var submitWithdrawalInfoModel: SubmitWithdrawalInfoModel? = null
    override fun getPageId(): String {
        return TrackConstants.B_VERIFIED_ALERT_EXPOSURE
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return LayoutInflater.from(activity).inflate(R.layout.fragment_submit_withdrawal_dialog, null)
    }

    override fun onStart() {
        super.onStart()
        val win = dialog!!.window
        // 一定要设置Background，如果不设置，window属性设置无效
        win.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val dm = DisplayMetrics()
        requireActivity().windowManager.defaultDisplay.getMetrics(dm)
        val params = win.attributes
        params.gravity = Gravity.CENTER
        // 使用ViewGroup.LayoutParams，以便Dialog 宽度充满整个屏幕
        params.width = (dm.widthPixels * 0.8).toInt()
        win.attributes = params
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tv_real_name_sure.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        if (ViewUtils.isFastClick()) {
            return
        }
        when (view.id) {
            R.id.tv_real_name_sure -> {
                val realName = et_real_name!!.text.toString()
                val identityNumber = et_identity_number!!.text.toString()
                val phoneNumber = et_phone_number!!.text.toString()
                //确定绑定实名认证
                if (TextUtils.isEmpty(realName)) {
                    ToastUtil.show(getString(R.string.dialog_real_name_empty))
                    return
                }
                if (TextUtils.isEmpty(identityNumber)) {
                    ToastUtil.show(getString(R.string.dialog_identity_number_empty))
                    return
                }
                if (TextUtils.isEmpty(phoneNumber)) {
                    ToastUtil.show(getString(R.string.dialog_phone_number_empty))
                    return
                }
                val isPhoneNum = NumberCheckUtil.isMobileNO(phoneNumber)
                if (!isPhoneNum) {
                    ToastUtil.show(getString(R.string.dialog_phone_number_check_tip))
                    return
                }
                submitWithdrawalInfoModel = getDialogFragmentViewModel(SubmitWithdrawalInfoModel::class.java)
                submitWithdrawalInfoModel?.submitRealNameData(realName, identityNumber, phoneNumber)
                submitWithdrawalInfoModel?.submitWithdrawalInfoSuccess?.observe(this, Observer { baseBean: BaseBean ->
                    //实名认证成功
                    EventBus.getDefault().post(EventCenter<Any?>(Constants.EVENT_CODE_REAL_NAME_SUCCESS))
                    ToastUtil.show(baseBean.msg)
                    dismissAllowingStateLoss()
                })
                submitWithdrawalInfoModel?.submitWithdrawalInfoFail?.observe(this, Observer { baseBean: BaseBean -> ToastUtil.show(baseBean.msg) })
            }
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(): SubmitWithdrawalInfoFragment {
            return SubmitWithdrawalInfoFragment()
        }
    }
}