package com.liquid.poros.ui.floatball;

import android.content.Context;
import android.graphics.Point;
import android.graphics.drawable.Animatable;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.request.RequestOptions;
import com.liquid.poros.R;
import com.liquid.poros.widgets.RippleView;
import java.util.Map;


/**
 * Created by sundroid on 08/07/2019.
 */
public class FloatMenu extends LinearLayout {
    public int height = 250;
    public int width = 100;
    private int networkQuality = -1;

    private static final String TAG = "FloatMenu";

    private boolean isAnchoring = false;
    private boolean isShowing = false;
    private WindowManager windowManager = null;
    private WindowManager.LayoutParams mParams = null;
    private float startX;
    private float startY;
    private float tempX;
    private float tempY;
    private String sessionId;
    private ImageView iv_mic_tips;
    private RippleView view_ripple;
    public void updateMicVolume(Map<String, Integer> speakers) {
        if (view_ripple == null) {
            return;
        }
        if (speakers.get(sessionId) != null) {
            int volume = speakers.get(sessionId);
            if (volume < 100) {
                if (view_ripple.getVisibility() == View.VISIBLE) {
                    view_ripple.setVisibility(GONE);
                }
                view_ripple.stopWaveAnimation();
            } else {
                if (view_ripple.getVisibility() == View.GONE) {
                    view_ripple.setVisibility(VISIBLE);
                }
                view_ripple.startWaveAnimation();
            }

        }
    }


    private onExpandClickListener listener;

    public FloatMenu(final Context context, String avatarUrl, String sessionId, onExpandClickListener listener) {
        super(context);
        this.listener = listener;
        this.sessionId = sessionId;
        View root = View.inflate(context, R.layout.anchor_desktop_widget, null);
        addView(root);

        windowManager = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
        view_ripple = root.findViewById(R.id.view_ripple);
        iv_mic_tips = root.findViewById(R.id.iv_mic_tips);
        ImageView iv_head = root.findViewById(R.id.iv_head);
        RequestOptions options = new RequestOptions()
                .placeholder(R.mipmap.img_default_avatar)
                .transform(new CircleCrop())
                .error(R.mipmap.img_default_avatar);
        Glide.with(this)
                .load(avatarUrl)
                .apply(options)
                .into(iv_head);
    }

    public void micStatus(int status) {
        iv_mic_tips.setVisibility(VISIBLE);
        if (status == 0) {
            iv_mic_tips.setImageResource(R.mipmap.icon_out_mic_soon);
        } else {
            iv_mic_tips.setImageResource(R.mipmap.icon_allready_left_mic);
        }
    }

    public void setParams(WindowManager.LayoutParams params) {
        mParams = params;
    }

    public void setIsShowing(boolean isShowing) {
        this.isShowing = isShowing;
    }


    public void hideOrShowMenu() {
        if (listener != null) {
            listener.onExpandClick(1);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (isAnchoring) {
            return true;
        }
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                startX = event.getRawX();
                startY = event.getRawY();

                tempX = event.getRawX();
                tempY = event.getRawY();

                break;
            case MotionEvent.ACTION_MOVE:
                float x = event.getRawX() - startX;
                float y = event.getRawY() - startY;
                //计算偏移量，刷新视图
                mParams.x += x;
                mParams.y += y;
                startX = event.getRawX();
                startY = event.getRawY();

                // 手指移动的时候更新小悬浮窗的位置
                updateViewPosition();
                break;
            case MotionEvent.ACTION_UP:
                if (Math.abs(startX - tempX) < ViewConfiguration.get(getContext()).getScaledTouchSlop() && Math.abs(startY - tempY) < ViewConfiguration.get(getContext()).getScaledTouchSlop()) {
                    hideOrShowMenu();
                } else {
                    anchorToSide();
                }
                break;

            default:
                break;
        }
        return true;
    }

    //获取屏幕宽度
    private int getScreenWidth() {
        Point point = new Point();
        windowManager.getDefaultDisplay().getSize(point);
        return point.x;
    }


    private void anchorToSide() {
        isAnchoring = true;
        Point size = new Point();
        windowManager.getDefaultDisplay().getSize(size);
        int screenWidth = size.x;
        int screenHeight = size.y;
        int middleX = mParams.x + getWidth() / 2;


        int animTime = 0;
        int xDistance = 0;
        int yDistance = 0;

        int dp_25 = dp2px(0);

        //1
        if (middleX <= dp_25 + getWidth() / 2) {
            xDistance = dp_25 - mParams.x;
        }
        //2
        else if (middleX <= screenWidth / 2) {
            xDistance = dp_25 - mParams.x;
        }
        //3
        else if (middleX >= screenWidth - getWidth() / 2 - dp_25) {
            xDistance = screenWidth - mParams.x - getWidth() - dp_25;
        }
        //4
        else {
            xDistance = screenWidth - mParams.x - getWidth() - dp_25;
        }

        //1
//        if (mParams.y < dp_25) {
//            yDistance = dp_25 - mParams.y;
//        }
//        //2
//        else if (mParams.y + getHeight() + dp_25 >= screenHeight) {
//            yDistance = screenHeight - dp_25 - mParams.y - getHeight();
//        }
        Log.e(TAG, "xDistance  " + xDistance + "   yDistance" + yDistance);

        animTime = Math.abs(xDistance) > Math.abs(yDistance) ? (int) (((float) xDistance / (float) screenWidth) * 600f)
                : (int) (((float) yDistance / (float) screenHeight) * 900f);
        this.post(new AnchorAnimRunnable(Math.abs(animTime), xDistance, yDistance, System.currentTimeMillis()));
    }

    public int dp2px(float dp) {
        final float scale = getContext().getResources().getDisplayMetrics().density;
        return (int) (dp * scale + 0.5f);
    }

    private class AnchorAnimRunnable implements Runnable {

        private int animTime;
        private long currentStartTime;
        private Interpolator interpolator;
        private int xDistance;
        private int yDistance;
        private int startX;
        private int startY;

        public AnchorAnimRunnable(int animTime, int xDistance, int yDistance, long currentStartTime) {
            this.animTime = animTime;
            this.currentStartTime = currentStartTime;
            interpolator = new AccelerateDecelerateInterpolator();
            this.xDistance = xDistance;
            this.yDistance = yDistance;
            startX = mParams.x;
            startY = mParams.y;
        }

        @Override
        public void run() {
            if (!isShowing) {
                return;
            }
            if (System.currentTimeMillis() >= currentStartTime + animTime) {
                if (mParams.x != (startX + xDistance) || mParams.y != (startY + yDistance)) {
                    mParams.x = startX + xDistance;
                    mParams.y = startY + yDistance;
                    windowManager.updateViewLayout(FloatMenu.this, mParams);
                }
                isAnchoring = false;
                return;
            }
            float delta = interpolator.getInterpolation((System.currentTimeMillis() - currentStartTime) / (float) animTime);
            int xMoveDistance = (int) (xDistance * delta);
            int yMoveDistance = (int) (yDistance * delta);
            Log.e(TAG, "delta:  " + delta + "  xMoveDistance  " + xMoveDistance + "   yMoveDistance  " + yMoveDistance);
            mParams.x = startX + xMoveDistance;
            mParams.y = startY + yMoveDistance;
            windowManager.updateViewLayout(FloatMenu.this, mParams);
            FloatMenu.this.postDelayed(this, 16);
        }
    }

    private void updateViewPosition() {
        //增加移动误差
        Log.e(TAG, "x  " + mParams.x + "   y  " + mParams.y);
        windowManager.updateViewLayout(this, mParams);
    }


}