package com.liquid.poros.ui.activity.user;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import com.liquid.poros.R;
import com.liquid.poros.base.BaseActivity;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.contract.info.GameInfoContract;
import com.liquid.poros.contract.info.GameInfoPresenter;
import com.liquid.poros.entity.PorosUserInfo;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.poros.utils.ViewUtils;
import com.liquid.poros.widgets.CustomRadioGroup;

import butterknife.BindView;
import butterknife.OnClick;

public class SubmitGameInfoActivity extends BaseActivity implements GameInfoContract.View {
    @BindView(R.id.rg_group)
    public CustomRadioGroup rg_group;
    @BindView(R.id.rb_group_wechat)
    public RadioButton rb_group_wechat;
    @BindView(R.id.rb_group_qq)
    public RadioButton rb_group_qq;
    @BindView(R.id.et_game_name)
    public EditText et_game_name;
    @BindView(R.id.tv_game_game_count)
    public TextView tv_game_game_count;
    private String gameArea;
    private GameInfoPresenter mPresenter;

    @Override
    protected void parseBundleExtras(Bundle extras) {

    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.activity_submit_game_info;
    }

    @Override
    protected void initViewsAndEvents(Bundle savedInstanceState) {
        mPresenter = new GameInfoPresenter();
        mPresenter.attachView(this);
        //和平精英游戏昵称
        et_game_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int content = et_game_name.getText().length();
                tv_game_game_count.setText(String.format(getString(R.string.game_room_name_count), content));
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        rg_group.setOnCheckedChangeListener(new CustomRadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CustomRadioGroup group, int checkedId) {
                if (checkedId == rb_group_wechat.getId()) {
                    gameArea = "微信";
                } else if (checkedId == rb_group_qq.getId()) {
                    gameArea = "QQ";
                }
            }
        });
        PorosUserInfo userInfo = AccountUtil.getInstance().getUserInfo();
        if (userInfo != null) {
            gameArea = userInfo.getHpjy_game_area();
            if (!TextUtils.isEmpty(userInfo.getHpjy_game_area())) {
                if ("QQ".equals(userInfo.getHpjy_game_area())) {
                    rb_group_qq.setChecked(true);
                    rb_group_wechat.setChecked(false);
                } else if ("微信".equals(userInfo.getHpjy_game_area())) {
                    rb_group_qq.setChecked(false);
                    rb_group_wechat.setChecked(true);
                }
            }
            if (!TextUtils.isEmpty(userInfo.getHpjy_game_nick()) && !"null".equals(userInfo.getHpjy_game_nick())) {
                et_game_name.setText(userInfo.getHpjy_game_nick());
                et_game_name.setSelection(et_game_name.getText().toString().trim().length());
            }
        }
    }

    @Override
    protected String getPageId() {
        return TrackConstants.PAGE_SUBMIT_GAME_INFO;
    }

    @OnClick({R.id.tv_save})
    public void onClick(View view) {
        if (ViewUtils.isFastClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.tv_save:
                String gameNick = et_game_name.getText().toString();
                if (TextUtils.isEmpty(gameNick) || TextUtils.isEmpty(gameArea)) {
                    showToast(getResources().getString(R.string.game_not_info_tip));
                    return;
                }
                mPresenter.setAccountInfo(gameNick, gameArea);
                break;
        }
    }

    @Override
    public void onAccountInfoFetched() {
        finish();
        showToast(getResources().getString(R.string.save_success));
    }

    @Override
    public void onAccountInfoFailFetched() {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }
}
