package com.liquid.poros.ui.dialog

import android.widget.TextView
import com.bumptech.glide.Glide
import com.liquid.poros.R
import com.liquid.poros.base.BaseDialogFragment2
import com.liquid.poros.constant.Constants
import com.liquid.poros.ui.activity.user.SessionActivity

class GiftBoxKeyNotEnoughDialog: BaseDialogFragment2() {

    private lateinit var tv_sure:TextView
    private lateinit var tv_cancel:TextView



    companion object{
        const val INFO = "info"
    }
    override fun contentXmlId(): Int {
        return R.layout.dialog_f_gift_box_key_not_enough

    }

    override fun initView() {

        tv_sure = rootView?.findViewById(R.id.tv_sure)!!
        tv_cancel = rootView?.findViewById(R.id.tv_cancel)!!

        Glide.with(mContext!!)
                .load(Constants.BASE_URL_RESOURCE + "gift_box/icon_gift_box_not_enough_key.png")
                .placeholder(R.mipmap.icon_default)
                .error(R.mipmap.icon_default)
                .into(rootView?.findViewById(R.id.iv_icon)!!)
    }

    override fun initListener() {
        tv_cancel.setOnClickListener {
            dismiss()
        }

        tv_sure.setOnClickListener {
            dismiss()
            if (activity is SessionActivity){
                val sessionActivity:SessionActivity = activity as SessionActivity;
                sessionActivity.dismissGiftBoxDialog()
                sessionActivity.showBuyCpDialog(1,"",true)
            }
        }
    }



}