package com.liquid.poros.ui.fragment.home

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.request.RequestOptions
import com.liquid.base.event.EventCenter
import com.liquid.base.guideview.Component
import com.liquid.base.guideview.GuideBuilder
import com.liquid.poros.R
import com.liquid.poros.analytics.utils.MultiTracker
import com.liquid.poros.base.BaseFragment
import com.liquid.poros.constant.Constants
import com.liquid.poros.constant.TrackConstants
import com.liquid.poros.contract.message.MessageContract
import com.liquid.poros.contract.message.MessagePresenter
import com.liquid.poros.entity.FriendMessageInfo
import com.liquid.poros.entity.FriendMessageListInfo
import com.liquid.poros.helper.JinquanResourceHelper
import com.liquid.poros.ui.MainActivity
import com.liquid.poros.ui.activity.IntimateActivity
import com.liquid.poros.ui.activity.TempFriendActivity
import com.liquid.poros.ui.activity.pta.PtaSettingActivity
import com.liquid.poros.ui.activity.user.ReportActivity
import com.liquid.poros.ui.fragment.MessageFriendItemView
import com.liquid.poros.ui.popupwindow.FriendMessagePopWindow
import com.liquid.poros.utils.ScreenUtil
import com.liquid.poros.utils.SharePreferenceUtil
import com.liquid.poros.utils.ViewUtils
import com.liquid.poros.widgets.ComponentView
import com.liquid.poros.widgets.pulltorefresh.BaseRefreshListener
import com.liquid.poros.widgets.pulltorefresh.ViewStatus
import kotlinx.android.synthetic.main.fragment_message.*
import kotlinx.android.synthetic.main.view_friend_temp.*
import kotlinx.android.synthetic.main.view_message_header.*
import java.util.*

/**
 * 消息列表页面
 */
class MessageFragment : BaseFragment(), MessageContract.View, BaseRefreshListener, FriendMessagePopWindow.Listener {

    private var mCommonAdapter: MessageAdapter? = null
    private var mPresenter: MessagePresenter? = null
    private var mPosition = 0
    private var mUserId: String? = null
    private var mGuideBuilder: GuideBuilder? = null
    private var friendMessagePopWindow: FriendMessagePopWindow? = null
    override fun getPageId(): String {
        return TrackConstants.MESSAGE_FRIEND
    }

    override fun onFirstUserVisible() {}
    override fun onUserVisible() {}
    override fun onUserInvisible() {}
    override fun getLoadingTargetView(): View? {
        return null
    }

    override fun initViewsAndEvents(savedInstanceState: Bundle?) {
        friendMessagePopWindow = FriendMessagePopWindow(activity)
        friendMessagePopWindow!!.setOnListening(this)
        try {
            (rv_friend_message_list.itemAnimator as DefaultItemAnimator?)!!.supportsChangeAnimations = false
        } catch (e: Exception) {
        }
        rv_friend_message_list!!.layoutManager = LinearLayoutManager(activity)
        mCommonAdapter = MessageAdapter()
        rv_friend_message_list!!.adapter = mCommonAdapter
        mPresenter = MessagePresenter()
        mPresenter!!.setMessageFragment(this)
        mPresenter!!.attachView(this)
        container!!.setCanRefresh(true)
        container!!.setCanLoadMore(false)
        container!!.setRefreshListener(this)
        ll_contacts.setOnClickListener {
            if (ViewUtils.isFastClick()) {
                return@setOnClickListener
            }
            readyGo(PtaSettingActivity::class.java)
        }

        ll_header_message.setOnClickListener {
            readyGo(IntimateActivity::class.java)
        }

        ll_friend_temp.setOnClickListener {
            TrackPoint().expireFriEnterClick()
            readyGo(TempFriendActivity::class.java)
        }
    }



    override fun getContentViewLayoutID(): Int {
        return R.layout.fragment_message
    }

    override fun onResume() {
        super.onResume()
        mPresenter!!.getFriendMessageList()
        isPause = false
    }

    override fun onPause() {
        super.onPause()
        isPause = true
    }

    private var lastTime: Long = 0
    override fun onEventComing(eventCenter: EventCenter<*>) {
        if (eventCenter.eventCode == Constants.EVENT_CODE_REFRESH_MESSAGE || eventCenter.eventCode == Constants.EVENT_CODE_UNREAD_MESSAGE || System.currentTimeMillis() - lastTime > 10000) {
            if (mPresenter != null) {
                mPresenter!!.getFriendMessageList()
                lastTime = System.currentTimeMillis()
            }
        }
    }

    override fun onFriendMessageListFetched(friendMessageListInfo: FriendMessageListInfo?) {
        container!!.finishRefresh()
        if (friendMessageListInfo == null || friendMessageListInfo.data == null || friendMessageListInfo.data.session_list == null || friendMessageListInfo.data.session_list.size == 0) {
            container!!.showView(ViewStatus.EMPTY_STATUS)
            return
        }
        val activity = activity as MainActivity?
        activity?.updateUnreadCount(friendMessageListInfo.data.unread_msg_cnt)
        container!!.showView(ViewStatus.CONTENT_STATUS)
        mCommonAdapter!!.update(friendMessageListInfo.data.session_list)
        if (mPresenter!!.hasTimerItem(mCommonAdapter!!.mDatas)) {
            mPresenter!!.startListTimer(rv_friend_message_list, mCommonAdapter, mCommonAdapter!!.mDatas as ArrayList<FriendMessageInfo>?)
        } else {
            mPresenter!!.stopListTimer()
        }

        val highInfo = friendMessageListInfo.data.high_score_entry_info

        if (highInfo != null) {
            ll_header_message.visibility = View.VISIBLE
//            container!!.addView(LayoutInflater.from(getActivity()).inflate(R.layout.view_message_header, null),0)
            val options = RequestOptions()
                    .placeholder(R.mipmap.img_default_avatar)
                    .apply(RequestOptions.bitmapTransform(CircleCrop()))
                    .override(ScreenUtil.dip2px(58f), ScreenUtil.dip2px(58f))
                    .error(R.mipmap.img_default_avatar)
            Glide.with(this)
                    .load(highInfo.icon)
                    .apply(options)
                    .into(iv_head)
            tv_intimate_title.text = highInfo.title
            tv_content.text = highInfo.sub_title

            if (highInfo.unread_msg_cnt > 0) {
                iv_red.visibility = View.VISIBLE
            } else {
                iv_red.visibility = View.GONE
            }
        } else {
            ll_header_message.visibility = View.GONE
        }
        if (friendMessageListInfo.data.isShow_expire_list) {
            if(friendMessageListInfo.data.isExpire_list_red_dot) {
                iv_red_fri.visibility = View.VISIBLE
            }else {
                iv_red_fri.visibility = View.GONE
            }
            ll_friend_temp.visibility = View.VISIBLE
        }else {
            ll_friend_temp.visibility = View.GONE
        }

        if(friendMessageListInfo.data.isShow_expire_list || highInfo != null) {
            layout_header_view.visibility = View.VISIBLE
        }else {
            layout_header_view.visibility = View.GONE
        }
    }

    override fun deleteMessageFetched(position: Int) {
        mPresenter!!.getFriendMessageList()
    }

    override fun topOrUnTopMsgFetched() {
        mPresenter!!.getFriendMessageList()
    }

    override fun refresh() {
        mPresenter!!.getFriendMessageList()
    }

    override fun loadMore() {}
    override fun onDestroy() {
        super.onDestroy()
        mPresenter!!.detachView()
    }

    @SuppressLint("UseRequireInsteadOfGet")
    override fun notifyByThemeChanged() {
        JinquanResourceHelper.getInstance(activity!!).setBackgroundColorByAttr(rl_container!!, R.attr.custom_attr_app_bg)
        JinquanResourceHelper.getInstance(activity!!).setTextColorByAttr(tv_title!!, R.attr.custom_attr_main_title_color)
        JinquanResourceHelper.getInstance(activity!!).setImageResourceByAttr(iv_contacts!!, R.attr.bg_dress_up_tip)
        JinquanResourceHelper.getInstance(activity!!).setTextColorByAttr(tv_contacts!!, R.attr.custom_attr_main_title_color)
        JinquanResourceHelper.getInstance(activity!!).setTextColorByAttr(tv_intimate_title!!, R.attr.custom_attr_nickname_text_color)
        JinquanResourceHelper.getInstance(activity!!).setTextColorByAttr(tv_content!!, R.attr.custom_attr_sub_text_color)
        JinquanResourceHelper.getInstance(activity!!).setBackgroundResourceByAttr(iv_right_arrow!!, R.attr.bg_right_more)
        JinquanResourceHelper.getInstance(activity!!).setBackgroundColorByAttr(view_message_header_line!!, R.attr.custom_shape_bg)
        if (mCommonAdapter != null) {
            mCommonAdapter!!.notifyDataSetChanged()
        }
    }

    override fun notifyNetworkChanged(isConnected: Boolean) {
        container!!.showView(if (isConnected) ViewStatus.CONTENT_STATUS else ViewStatus.ERROR_STATUS)
        container!!.setCanRefresh(isConnected)
    }

    override fun setUserVisibleHint(isVisible: Boolean) {
        super.setUserVisibleHint(isVisible)
        //        if (isVisible) {
//            ll_contacts.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    boolean isDressUpTip = SharePreferenceUtil.getBoolean(SharePreferenceUtil.FILE_USER_ACCOUNT_DATA, SharePreferenceUtil.KEY_DRESS_UP_TIP_FIRST, true);
//                    if (isDressUpTip && ll_contacts != null) {
//                        showDressGuideView(ll_contacts);
//                    }
//                }
//            }, 200);
//        }
    }

    override fun onItemClicked(text: String) {
        when (text) {
            "置顶消息" -> mPresenter!!.topMessage(mUserId)
            "取消置顶" -> mPresenter!!.unTopMessage(mUserId)
            "加为好友" -> mPresenter!!.addFriend(mUserId)
            "删除消息" -> mPresenter!!.deleteMessage(mPosition, mUserId,false)
            "举报" -> {
                val bundle = Bundle()
                bundle.putString(Constants.USER_ID, mUserId)
                readyGo(ReportActivity::class.java, bundle)
            }
        }
    }

    override fun onDismiss() {
        if (mCommonAdapter != null) {
            mCommonAdapter!!.notifyDataSetChanged()
        }
    }

    private fun showDressGuideView(anchor: View?) {
        if (anchor == null) {
            return
        }
        mGuideBuilder = GuideBuilder()
        mGuideBuilder!!.setTargetView(anchor)
                .setAlpha(100)
                .setOverlayTarget(false)
                .setHighTargetCorner(40)
                .setHighTargetPaddingLeft(10)
                .setHighTargetPaddingRight(10)
                .setHighTargetPaddingBottom(5)
                .setHighTargetPaddingTop(5)
                .setHighTargetGraphStyle(Component.ROUNDRECT)
        mGuideBuilder!!.setOnVisibilityChangedListener(object : GuideBuilder.OnVisibilityChangedListener {
            override fun onShown() {}
            override fun onDismiss() {
                SharePreferenceUtil.saveBoolean(SharePreferenceUtil.FILE_USER_ACCOUNT_DATA, SharePreferenceUtil.KEY_DRESS_UP_TIP_FIRST, false)
                mGuideBuilder = null
                readyGo(PtaSettingActivity::class.java)
            }
        })
        mGuideBuilder!!.addComponent(ComponentView(R.mipmap.icon_dress_up_tip_guide, Component.ANCHOR_BOTTOM, Component.FIT_END, 0, 20))
        val guide = mGuideBuilder!!.createGuide()
        guide.show(activity)
    }

    fun tempFriendExpired() {
        if (ll_friend_temp != null && ll_friend_temp.visibility == View.GONE) {
            ll_friend_temp.visibility = View.VISIBLE
            layout_header_view.visibility = View.VISIBLE
        }
        if (iv_red_fri != null && iv_red_fri.visibility == View.GONE) {
            iv_red_fri.visibility = View.VISIBLE
        }
    }
    /**
     * 消息Adapter
     */
    private inner class MessageAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
        var mDatas: MutableList<FriendMessageInfo> = ArrayList()
            private set
        fun update(data: List<FriendMessageInfo>?) {
            if (data != null) {
                mDatas.clear()
                mDatas.addAll(data)
                notifyDataSetChanged()
            }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
            return object : RecyclerView.ViewHolder(MessageFriendItemView(parent.context,false)) {}
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
            val itemView = holder.itemView as MessageFriendItemView
            val friendMessageInfo = mDatas[position]
            itemView.setData(friendMessageInfo)
            itemView.setOnLongClickListener {
                mPosition = holder.layoutPosition
                mUserId = friendMessageInfo.user_id
                friendMessagePopWindow!!.showPop(itemView.friendName, true, friendMessageInfo.pin_priority == -1)
                JinquanResourceHelper.getInstance(activity!!).setBackgroundColorByAttr(itemView.itemContent, R.attr.bg_item_message)
                true
            }
        }

        override fun getItemCount(): Int {
            return mDatas.size
        }
    }

    companion object {
        @JvmField
        var isPause = false

        @JvmStatic
        fun newInstance(): MessageFragment {
            return MessageFragment()
        }
    }
    inner class  TrackPoint{
        internal fun expireFriEnterClick() {
            val params = HashMap<String?,String?>()
            MultiTracker.onEvent(TrackConstants.B_CHAT_LIST_EXPIRED_PAGE_ENTRANCE_CLICK,params)
        }
    }
}