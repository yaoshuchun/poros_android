package com.liquid.poros.ui.activity.audiomatch;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.liquid.base.guideview.Component;
import com.liquid.base.guideview.DimenUtil;
import com.liquid.base.guideview.Guide;
import com.liquid.base.guideview.GuideBuilder;
import com.google.gson.Gson;
import com.liquid.base.event.EventCenter;
import com.liquid.poros.R;
import com.liquid.poros.base.BaseActivity;
import com.liquid.poros.analytics.utils.MultiTracker;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.contract.audiomatch.AudioMatchInfoContract;
import com.liquid.poros.contract.audiomatch.AudioMatchInfoPresenter;
import com.liquid.poros.entity.AudioMatchEditGameBean;
import com.liquid.poros.entity.AudioMatchGameBean;
import com.liquid.poros.entity.AudioMatchInfo;
import com.liquid.poros.entity.StartAudioMatchInfo;
import com.liquid.poros.ui.activity.audiomatch.viewmodel.AudioMatchInfoViewModel;
import com.liquid.poros.ui.dialog.RechargeCoinDialog;
import com.liquid.poros.ui.floatball.GiftPackageFloatManager;
import com.liquid.poros.ui.fragment.dialog.audiomatch.AudioMatchInfoDialogFragment;
import com.liquid.poros.ui.fragment.dialog.common.CommonActionListener;
import com.liquid.poros.ui.fragment.dialog.common.CommonDialogBuilder;
import com.liquid.poros.ui.fragment.dialog.common.CommonDialogFragment;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.poros.utils.ScreenUtil;
import com.liquid.poros.utils.SharePreferenceUtil;
import com.liquid.poros.utils.StatusBarUtil;
import com.liquid.poros.utils.ViewUtils;
import com.liquid.poros.widgets.ComponentView;
import com.liquid.poros.widgets.CustomHeader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import butterknife.BindView;
import butterknife.OnClick;

import static com.liquid.poros.ui.activity.audiomatch.CPMatchingActivity.CP_TYPE;
import static com.liquid.poros.ui.activity.audiomatch.CPMatchingActivity.GAME_AREA;
import static com.liquid.poros.ui.activity.audiomatch.CPMatchingActivity.GAME_LEVEL;
import static com.liquid.poros.ui.activity.audiomatch.CPMatchingActivity.MATCHING_TIME_MAX;
import static com.liquid.poros.ui.dialog.RechargeCoinDialog.POSITION_DRESS_SEND;

/**
 * 语音速配信息设置界面
 */
public class AudioMatchInfoActivity extends BaseActivity implements AudioMatchInfoContract.View {
    @BindView(R.id.iv_match_info_head_tip)
    ImageView iv_match_info_head_tip;
    @BindView(R.id.tv_header)
    public CustomHeader mHeader;
    @BindView(R.id.iv_match_game_type)
    ImageView iv_match_game_type;
    @BindView(R.id.tv_audio_match_info_tip)
    TextView tv_audio_match_info_tip;
    @BindView(R.id.tv_match_info_area)
    TextView tv_match_info_area;
    @BindView(R.id.tv_match_info_rank)
    TextView tv_match_info_rank;
    @BindView(R.id.tv_audio_match_free)
    TextView tv_audio_match_free;
    @BindView(R.id.tv_audio_match_free_tip)
    TextView tv_audio_match_free_tip;
    @BindView(R.id.ll_match_game_info)
    LinearLayout ll_match_game_info;
    @BindView(R.id.view_tip)
    View view_tip;
    @BindView(R.id.guide_gift_package_iv)
    ImageView guide_gift_package_iv;
    private String mTitle;
    private String mMatchType;
    private int mEntryType = 1;
    private final String TAG_WANGZHE = "wzry";
    private final String TAG_HEPING = "hpjy";
    private final String TAG_CHAT = "chat";
    private AudioMatchInfoPresenter mPresenter;
    private AudioMatchEditGameBean mStaticGameBean;
    private AudioMatchInfo.Data.UserGameInfo userGameInfo;
    private String honor_zoom;
    private String honor_rank;
    private List<Integer> mZoneList = new ArrayList<>();
    private List<Integer> mRankList = new ArrayList<>();
    private RechargeCoinDialog rechargeCoinDialog;
    private GuideBuilder mGuideBuilder;
    private AudioMatchInfoViewModel infoViewModel;

    @Override
    protected void parseBundleExtras(Bundle extras) {
        mTitle = extras.getString(Constants.AUDIO_MATCH_INFO_TITLE);
        mMatchType = extras.getString(Constants.AUDIO_MATCH_INFO_TYPE);
        mEntryType = extras.getInt(Constants.AUDIO_MATCH_ENTRY_TYPE);
    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.activity_audio_match_info;
    }

    @Override
    protected String getPageId() {
        return TrackConstants.PAGE_AUDIO_MATCH;
    }

    @Override
    protected void initViewsAndEvents(Bundle savedInstanceState) {
        if (TAG_WANGZHE.equals(mMatchType)) {
            iv_match_game_type.setImageResource(R.mipmap.icon_match_info_wzry);
            iv_match_info_head_tip.setBackgroundResource(R.mipmap.icon_match_info_head_tip);
        } else if (TAG_HEPING.equals(mMatchType)) {
            iv_match_game_type.setImageResource(R.mipmap.icon_match_info_hpjy);
            iv_match_info_head_tip.setBackgroundResource(R.mipmap.icon_match_info_head_tip);
        } else {
            iv_match_game_type.setImageResource(R.mipmap.icon_match_info_chat);
            iv_match_info_head_tip.setBackgroundResource(R.mipmap.icon_chat_match_tip);
        }
        mPresenter = new AudioMatchInfoPresenter();
        mPresenter.attachView(this);
        mPresenter.getAudioMatchInfo(mMatchType);
        rechargeCoinDialog = new RechargeCoinDialog();
        showGiftPackageGuide();
    }

    private void showGiftPackageGuide(){
        guide_gift_package_iv.setVisibility(AccountUtil.getInstance().isOpen()?View.VISIBLE:View.GONE);
    }


    @OnClick({R.id.tv_edit_audio_match, R.id.rl_start_audio_match})
    public void onClick(View view) {
        if (ViewUtils.isFastClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.tv_edit_audio_match:
                if (mStaticGameBean != null && mStaticGameBean.honor_of_kings != null) {
                    ArrayList<AudioMatchGameBean> peace_elite = new ArrayList<>();
                    peace_elite.add(mStaticGameBean.honor_of_kings.zone);
                    peace_elite.add(mStaticGameBean.honor_of_kings.rank);
                    AudioMatchInfoDialogFragment.newInstance()
                            .setEditGameBeans(peace_elite)
                            .setHonorBean(userGameInfo)
                            .show(getSupportFragmentManager(), AudioMatchInfoDialogFragment.class.getSimpleName());
                }
                break;
            case R.id.rl_start_audio_match:
                if (GiftPackageFloatManager.getInstance().checkGiftPackage(5)){
                    return;
                }

                startAudioMatch(false);
                break;
        }
    }

    /**
     * 开始匹配
     *
     * @param isForce
     */
    private void startAudioMatch(boolean isForce) {
        if (!TAG_CHAT.equals(mMatchType)) {
            if (mZoneList.size() == 0 || mZoneList.get(0) == 0 || mRankList.size() == 0 || mRankList.get(0) == 0) {
                showMessage(getString(R.string.game_not_info_tip));
                return;
            }
        }
        mPresenter.startAudioMatch(mMatchType, mZoneList.size() == 0 ? 0 : mZoneList.get(0), mRankList.size() == 0 ? 0 : mRankList.get(0), isForce,mEntryType);
    }

    @Override
    public void fetchAudioMatchInfo(AudioMatchInfo audioMatchInfo) {
        if (audioMatchInfo == null || audioMatchInfo.getData() == null) {
            return;
        }
        if (!TextUtils.isEmpty(audioMatchInfo.getData().getTitle())) {
            mHeader.setTitile(audioMatchInfo.getData().getTitle());
        } else {
            mHeader.setTitile(mTitle);
        }
        tv_audio_match_info_tip.setText(audioMatchInfo.getData().getText());
        tv_audio_match_free.setVisibility(audioMatchInfo.getData().isFree_tag() ? View.VISIBLE : View.GONE);
        tv_audio_match_free_tip.setText(audioMatchInfo.getData().getPrice_hint());
        if (!TAG_CHAT.equals(mMatchType)) {
            ll_match_game_info.setVisibility(View.VISIBLE);
            view_tip.setVisibility(View.GONE);
            mStaticGameBean = audioMatchInfo.getStaticGameBean();
            userGameInfo = new Gson().fromJson(new Gson().toJson(audioMatchInfo.getData().getUser_game_info()), AudioMatchInfo.Data.UserGameInfo.class);
            checkGameData();
            setGameData();
        } else {
            ll_match_game_info.setVisibility(View.GONE);
            view_tip.setVisibility(View.VISIBLE);
            setViewHeight(view_tip);
        }
        if (audioMatchInfo.getData().isMatch_room_newbie_hint()){
            ll_match_game_info.post(new Runnable() {
                @Override
                public void run() {
                    showGuideView(ll_match_game_info);
                }
            });
        }
    }

    private void setViewHeight(View view) {
        ViewGroup.MarginLayoutParams margin = new ViewGroup.MarginLayoutParams(view.getLayoutParams());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(margin);
        layoutParams.height = ScreenUtil.dip2px(120) - StatusBarUtil.getNavigationBarHeight(this);
        view.setLayoutParams(layoutParams);
    }

    @Override
    public void startAudioMatchFetch(StartAudioMatchInfo startAudioMatchInfo, int zone, int rank) {
        if (startAudioMatchInfo == null || startAudioMatchInfo.getData() == null) {
            return;
        }
        if (startAudioMatchInfo.getData().isShow_recharge_hint()) {
            new CommonDialogBuilder()
                    .setWithCancel(true)
                    .setImageTitleRes(R.mipmap.icon_team_lack_time)
                    .setCancel(getString(R.string.audio_match_keep))
                    .setConfirm(getString(R.string.live_room_private_room_recharge))
                    .setDesc(startAudioMatchInfo.getData().getText())
                    .setListener(new CommonActionListener() {
                        @Override
                        public void onConfirm() {
                            if (rechargeCoinDialog.isAdded()) {
                                return;
                            }
                            rechargeCoinDialog.show(getSupportFragmentManager(), RechargeCoinDialog.class.getName());
                            rechargeCoinDialog.setWindowFrom("match_info_not_duration");
                            MultiTracker.onEvent(TrackConstants.B_MATCH_NOT_ENOUGH_DURATION_RACHARGE);
                        }

                        @Override
                        public void onCancel() {
                            //继续匹配
                            startAudioMatch(true);
                            HashMap<String, String> params = new HashMap<>();
                            params.put("event_time", String.valueOf(System.currentTimeMillis()));
                            params.put("match_type", mMatchType);
                            params.put("partition", String.valueOf(zone));
                            params.put("rank", String.valueOf(rank));
                            params.put("act_type", "fail");
                            params.put("entry_type", mEntryType==1?"square":"message");
                            MultiTracker.onEvent(TrackConstants.B_KEEP_FIND_USER_MATCH, params);
                        }
                    }).create().show(getSupportFragmentManager(), CommonDialogFragment.class.getSimpleName());
        } else {
            Bundle bundle = new Bundle();
            bundle.putString(CP_TYPE, mMatchType);
            bundle.putInt(Constants.AUDIO_MATCH_INFO_ZONE, zone);
            bundle.putInt(Constants.AUDIO_MATCH_INFO_RANK, rank);
            bundle.putString(GAME_AREA, tv_match_info_area.getText().toString());
            bundle.putString(GAME_LEVEL, tv_match_info_rank.getText().toString());
            bundle.putLong(MATCHING_TIME_MAX, startAudioMatchInfo.getData().getSeconds());
            readyGo(CPMatchingActivity.class, bundle);
            finish();
        }
        HashMap<String, String> params = new HashMap<>();
        params.put("event_time", String.valueOf(System.currentTimeMillis()));
        params.put("match_type", mMatchType);
        params.put("partition", String.valueOf(zone));
        params.put("rank", String.valueOf(rank));
        params.put("act_type", startAudioMatchInfo.getData().isShow_recharge_hint() ? "fail" : "success");
        params.put("entry_type", mEntryType==1?"square":"message");
        MultiTracker.onEvent(TrackConstants.B_FIND_USER_IMMEDIATELY, params);
    }

    @Override
    public void notEnoughCoinFetch(String content) {
        new CommonDialogBuilder()
                .setWithCancel(true)
                .setImageTitleRes(R.mipmap.icon_not_enough_money)
                .setCancel(getString(R.string.cancel))
                .setConfirm(getString(R.string.match_charge))
                .setDesc(content)
                .setListener(new CommonActionListener() {
                    @Override
                    public void onConfirm() {
                        if (rechargeCoinDialog.isAdded()) {
                            return;
                        }
                        rechargeCoinDialog.setPositon(RechargeCoinDialog.POSITION_SEND_GIFT);
                        rechargeCoinDialog.show(getSupportFragmentManager(), RechargeCoinDialog.class.getName());
                        rechargeCoinDialog.setWindowFrom("match_info_not_coin");
                        MultiTracker.onEvent(TrackConstants.B_MATCH_NOT_ENOUGH_COIN_RACHARGE);
                    }

                    @Override
                    public void onCancel() {
                    }
                }).create().show(getSupportFragmentManager(), CommonDialogFragment.class.getSimpleName());

    }

    @Override
    protected void onEventComing(EventCenter eventCenter) {
        super.onEventComing(eventCenter);
        switch (eventCenter.getEventCode()) {
            case Constants.EVENT_CODE_UPDATE_AUDIO_MATCH_GAME_INFO:
                //保存
                HashMap<String, Object> honorMap = (HashMap<String, Object>) eventCenter.getData();
                List<AudioMatchGameBean> honorData = (List<AudioMatchGameBean>) honorMap.get("static");
                mStaticGameBean.honor_of_kings.zone = honorData.get(0);
                mStaticGameBean.honor_of_kings.rank = honorData.get(1);
                AudioMatchInfo.Data.UserGameInfo data = (AudioMatchInfo.Data.UserGameInfo) honorMap.get("data");
                userGameInfo.setRank(data.getRank());
                userGameInfo.setZone(data.getZone());
                checkGameData();
                setGameData();
                break;
            case Constants.EVENT_CODE_UPDATE_GIFT_PACKAGE_STATUS:
                showGiftPackageGuide();
                break;
        }
    }


    private void checkGameData() {
        if (userGameInfo != null && mStaticGameBean != null) {
            mZoneList.clear();
            mRankList.clear();
            mZoneList.add(userGameInfo.getZone());
            mRankList.add(userGameInfo.getRank());
            honor_zoom = setSelect(mZoneList, mStaticGameBean.honor_of_kings.zone);
            honor_rank = setSelect(mRankList, mStaticGameBean.honor_of_kings.rank);
        }
    }

    private void setGameData() {
        tv_match_info_area.setVisibility(View.VISIBLE);
        tv_match_info_rank.setVisibility(View.VISIBLE);
        if (userGameInfo.getRank() != 0 && userGameInfo.getZone() != 0) {
            tv_match_info_area.setText("大区:" + honor_zoom);
            tv_match_info_rank.setText("段位:" + honor_rank);
        } else {
            tv_match_info_area.setText("大区: 未填写");
            tv_match_info_rank.setText("段位: 未填写");
        }
    }

    private String setSelect(List<Integer> item, AudioMatchGameBean data) {
        StringBuilder stringBuilder = new StringBuilder();
        if (item != null && item.size() > 0) {
            for (int i = 0; i < item.size(); i++) {
                try {
                    data.items.get(item.get(i) - 1).select = true;
                    stringBuilder.append(data.items.get(item.get(i) - 1).name);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return stringBuilder.toString();
    }
    private void showGuideView(View anchor) {
        if (anchor == null) {
            return;
        }
        mGuideBuilder = new GuideBuilder();
        mGuideBuilder.setTargetView(anchor)
                .setOverlayTarget(false)
                .setHighTargetCorner(DimenUtil.dp2px(getApplicationContext(), 10))
                .setHighTargetPaddingLeft(DimenUtil.dp2px(getApplicationContext(), 5))
                .setHighTargetPaddingRight(DimenUtil.dp2px(getApplicationContext(), 5))
                .setHighTargetPaddingTop(DimenUtil.dp2px(getApplicationContext(), 10))
                .setHighTargetPaddingBottom(DimenUtil.dp2px(getApplicationContext(), 10))
                .setAlpha(153)
                .setHighTargetGraphStyle(Component.ROUNDRECT);
        mGuideBuilder.setOnVisibilityChangedListener(new GuideBuilder.OnVisibilityChangedListener() {
            @Override
            public void onShown() {

            }

            @Override
            public void onDismiss() {
                mGuideBuilder = null;
            }
        });
        mGuideBuilder.addComponent(new ComponentView(R.mipmap.icon_audio_match_info_guide, Component.ANCHOR_TOP, Component.FIT_CENTER, 0, 20));
        Guide guide = mGuideBuilder.createGuide();
        guide.show(AudioMatchInfoActivity.this);
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }
}
