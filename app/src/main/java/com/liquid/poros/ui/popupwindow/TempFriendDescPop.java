package com.liquid.poros.ui.popupwindow;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.liquid.poros.R;
import com.liquid.poros.analytics.utils.MultiTracker;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.ui.activity.user.SessionActivity;

import java.util.HashMap;

import razerdp.basepopup.BasePopupWindow;

/**
 * 点击临时好友倒计时弹出的气泡
 */
public class TempFriendDescPop extends BasePopupWindow {

    private View contentView;
    private TextView tv_temp_friend_desc;
    private TextView tv_immediately_ask_for;
    public static final String TEMP_FRIEND_BUBBLE = "countdown";
    public static final String EXPIRE_FRIEND = "expired";

    @Override
    public void onViewCreated(@NonNull View contentView) {
        super.onViewCreated(contentView);
        MultiTracker.onEvent(TrackConstants.B_CHAT_COUNTDOWN_SYSTEM_BUBBLE_EXPOSURE, getCommonTrackParams());
    }

    public void updateTv_temp_friend_desc(String desc) {
        if (null != tv_temp_friend_desc){
            tv_temp_friend_desc.setText(desc);
        }
    }

    public TempFriendDescPop(Context context) {
        super(context);
    }

    @Override
    public View onCreateContentView() {
        contentView = createPopupById(R.layout.layout_bubble_temp_friend_desc);
        tv_temp_friend_desc = contentView.findViewById(R.id.tv_temp_friend_desc);
        tv_immediately_ask_for = contentView.findViewById(R.id.tv_immediately_ask_for);
        tv_immediately_ask_for.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MultiTracker.onEvent(TrackConstants.B_CHAT_COUNTDOWN_IMMEDIATELY_PRESENT_CLICK,  getCommonTrackParams());
                Activity context = getContext();
                dismiss();
                if (context instanceof SessionActivity){
                    SessionActivity sessionActivity = (SessionActivity) context;
                    sessionActivity.sendGiftForTempFriend(TEMP_FRIEND_BUBBLE);
                }
            }
        });
        return contentView;
    }

    private HashMap<String, String> getCommonTrackParams(){
        HashMap<String, String> params = new HashMap<>();
        if (getContext() instanceof SessionActivity){
            params.put("guest_id", ((SessionActivity)getContext()).getFemaleGuestId());
        }
        return params;
    }
}