package com.liquid.poros.ui.dialog

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.liquid.poros.R
import com.liquid.poros.analytics.utils.MultiTracker
import com.liquid.poros.base.BaseDialogFragment2
import com.liquid.poros.constant.Constants
import com.liquid.poros.constant.TrackConstants
import com.liquid.poros.entity.Contacts
import com.liquid.poros.entity.GiftOfBox
import com.liquid.poros.ui.activity.user.SessionActivity
import com.liquid.poros.ui.fragment.viewmodel.BagViewModel

class GiftDetailDialog: BaseDialogFragment2() {
    private lateinit var gift:GiftOfBox

    private lateinit var iv_gift_bg:ImageView
    private lateinit var iv_icon:ImageView
    private lateinit var tv_name:TextView
    private lateinit var tv_value:TextView
    private lateinit var tv_action:TextView
    //private lateinit var tv_desc:TextView


    private val bagViewModel by lazy { getViewModel(BagViewModel::class.java) }
    companion object{
        const val INFO = "info"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bagViewModel.fastSendBagGiftData.observe(this, Observer{
            dismiss()
            if (activity is SessionActivity){
                (activity as SessionActivity).dismissGiftBoxDialog()
                (activity as SessionActivity).showGiftAnimBag(gift.gift_id, gift.num, gift.gift_image, gift.effect?.audio, gift.gift_name, Constants.GIFT_NORMAL)
            }
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog?.window?.setWindowAnimations(R.style.dialogScaleAnim)
    }
    override fun contentXmlId(): Int {
        return R.layout.dialog_f_gift_detail

    }

    override fun initView() {

        iv_icon = rootView?.findViewById(R.id.iv_icon)!!
        iv_gift_bg = rootView?.findViewById(R.id.iv_gift_bg)!!
        tv_name = rootView?.findViewById(R.id.tv_name)!!
        tv_value = rootView?.findViewById(R.id.tv_value)!!
        tv_action = rootView?.findViewById(R.id.tv_action)!!
        //tv_desc = rootView?.findViewById(R.id.tv_desc)!!

        rootView?.postDelayed(Runnable { dismiss() },3000)


    }

    override fun initListener() {
        tv_action.setOnClickListener {
            gift?.let {
                when(gift.box_gift_type){
                    GiftOfBox.EXCLUSIVE_GIFT, GiftOfBox.ORDINARY_GIFT -> {
                        bagViewModel.fastSendBagGift(gift.bag_detail_id,gift.target_user_id)
                        val msgParams = HashMap<String?, String?>()
                        msgParams["guest_id"] = gift.target_user_id
                        msgParams["prize_type"] = gift.box_gift_type.toString()
                        MultiTracker.onEvent(TrackConstants.B_WIN_A_LOTTERY_SEND_GIFT_CLICK, msgParams)
                    }
                    else -> {
                        dismiss()
                        val msgParams = HashMap<String?, String?>()
                        msgParams["guest_id"] = gift.target_user_id
                        msgParams["prize_type"] = gift.box_gift_type.toString()
                        MultiTracker.onEvent(TrackConstants.B_WIN_A_LOTTERY_RECIVE_GIFT_CLICK, msgParams)
                    }
                }
            }

        }

    }

    override fun initData() {
        gift = arguments?.get(INFO) as GiftOfBox

        gift.apply {

            tv_name.text = gift.gift_name
            tv_value.text = gift.value
            var actonTvStr = when(gift.box_gift_type){
                GiftOfBox.EXCLUSIVE_GIFT, GiftOfBox.ORDINARY_GIFT -> "送给TA"
                else -> "开心收下"
            }
            tv_action.text = actonTvStr
            //tv_desc.text = gift.desc
            
            Glide.with(mContext!!)
                    .load(gift.gift_image+"")
                    .into(iv_icon)

            Glide.with(mContext!!)
                    .load(gift.bg_image+"")
                    .into(iv_gift_bg)


            val msgParams = HashMap<String?, String?>()
            msgParams["guest_id"] = gift.target_user_id
            msgParams["prize_type"] = gift.box_gift_type.toString()
            MultiTracker.onEvent(TrackConstants.B_WIN_A_LOTTERY_ALERT_EXPOSURE, msgParams)
        }



    }

    override fun getDimAmount(): Float {
        return 0.7f
    }

}