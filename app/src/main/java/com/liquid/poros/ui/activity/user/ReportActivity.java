package com.liquid.poros.ui.activity.user;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.liquid.poros.R;
import com.liquid.poros.base.BaseActivity;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.constant.TrackConstants;

import com.liquid.poros.contract.report.ReportConstract;
import com.liquid.poros.contract.report.ReportPresenter;
import com.liquid.poros.utils.ViewUtils;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 举报界面
 */
public class ReportActivity extends BaseActivity implements ReportConstract.View {
    @BindView(R.id.et_content)
    public EditText et_content;
    @BindView(R.id.tv_desc_count)
    public TextView tv_desc_count;
    @BindView(R.id.tv_report)
    TextView tv_report;
    private String room_id; //房间id
    private String user_id; //房间id
    private ReportPresenter mPresenter;
    private boolean hasContent = false;

    @Override
    protected String getPageId() {
        return TrackConstants.PAGE_REPORT;
    }

    @Override
    protected void parseBundleExtras(Bundle extras) {
        room_id = extras.getString(Constants.ROOM_ID);
        user_id = extras.getString(Constants.USER_ID);
    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.activity_report;
    }

    @Override
    protected void initViewsAndEvents(Bundle savedInstanceState) {
        et_content.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int content = et_content.getText().length();
                tv_desc_count.setText(String.format(getString(R.string.group_desc_count), content));//字数限制
            }

            @Override
            public void afterTextChanged(Editable s) {
                hasContent = et_content.getText().length() > 0 && !TextUtils.isEmpty(s.toString().trim());
                changeStatus();
            }
        });
        mPresenter = new ReportPresenter();
        mPresenter.attachView(this);
    }

    @OnClick(R.id.tv_report)
    public void onClick(View view) {
        //举报
        if (ViewUtils.isFastClick()) {
            return;
        }
        String content = et_content.getText().toString();
        if (!TextUtils.isEmpty(room_id)) {
            mPresenter.reportChatRoom(room_id, content, user_id);
        } else {
            mPresenter.reportUser(content, user_id);
        }
    }

    private void changeStatus() {
        if (hasContent) {
            tv_report.setEnabled(true);
        } else {
            tv_report.setEnabled(false);
        }
    }

    @Override
    public void onReportSucceed() {
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }
}
