package com.liquid.poros.ui.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import com.liquid.poros.R;
import com.liquid.poros.base.AppConfigViewModel;
import com.liquid.poros.base.BaseActivity;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.constant.FileDownloadConstant;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.sdk.HeartBeatUtils;
import com.liquid.poros.ui.MainActivity;
import com.liquid.poros.ui.activity.user.SubmitUserInfoActivity;
import com.liquid.poros.ui.activity.user.TestSubmitUserInfoActivity;
import com.liquid.poros.ui.floatball.GiftPackageFloatManager;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.poros.utils.AppConfigUtil;
import com.liquid.poros.utils.FileDownloadUtil;
import com.liquid.poros.utils.SharePreferenceUtil;
import com.liquid.poros.utils.StringUtil;
import com.liquid.poros.utils.network.HttpCallback;
import com.liquid.poros.utils.network.RetrofitHttpManager;
import com.tonyodev.fetch2.Priority;

import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;

public class WelcomeActivity extends BaseActivity {


    @Override
    protected String getPageId() {
        return TrackConstants.PAGE_WELCOME;
    }

    @Override
    protected void parseBundleExtras(Bundle extras) {

    }

    @Override
    protected int getStatusBarColor() {
        return Color.parseColor("#15080D");
    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.layout_welcome_activity;
    }

    @Override
    protected boolean needSetTheme() {
        return false;
    }

    @Override
    protected void initViewsAndEvents(Bundle savedInstanceState) {
        if((getIntent().getFlags() & Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT) != 0){
            finish();
            return;
        }
        initGlobalConfig();//配置请求类，异步请求
        FileDownloadUtil.INSTANCE.downloadList(FileDownloadConstant.INSTANCE.getGiftList(), Priority.NORMAL,null);
        String account_token = SharePreferenceUtil.getString(SharePreferenceUtil.FILE_USER_ACCOUNT_DATA, SharePreferenceUtil.KEY_USER_ACCOUNT_TOKEN, "");
        if (StringUtil.isNotEmpty(account_token)) {
            AccountUtil.getInstance().auto_login(account_token, new AccountUtil.AutoLoginListener() {
                @Override
                public void onSucceed() {
                    boolean isNeedSetBasicInfo = AccountUtil.getInstance().isNeedSetBasicInfo();
                    boolean isMatchControl = AccountUtil.getInstance().isMatchControl();
                    if (isNeedSetBasicInfo) {
                        if (isMatchControl) {
                            //对照组
                            readyGo(SubmitUserInfoActivity.class);
                        } else {
                            //实验组
                            readyGo(TestSubmitUserInfoActivity.class);
                        }
                    } else {
                        readyGo(MainActivity.class);
                    }
                    finish();
                }

                @Override
                public void onFailed() {
                    SharePreferenceUtil.saveString(SharePreferenceUtil.FILE_USER_ACCOUNT_DATA, SharePreferenceUtil.KEY_USER_ACCOUNT_TOKEN, "");
                    readyGo(LoginActivity.class);
                    finish();
                }
            });
        } else {
            readyGo(LoginActivity.class);
            finish();
        }

    }

    public void initGlobalConfig() {
        AppConfigViewModel appViewModel = getActivityViewModel(AppConfigViewModel.class);
        appViewModel.getAppConfigDataSuccess().observe(this, AppBaseConfigBean -> {

            try {
                //@TODO  ----保持原有逻辑不变，逐步替换
                Constants.SHARE_GROUP_LINK = AppBaseConfigBean.getShare_group_url() + "%s";
                Constants.FEEDBACK_GROUP = AppBaseConfigBean.getFeedback_group();
                Constants.team_leader_tips = AppBaseConfigBean.getTeam_leader_tips();
                Constants.team_user_tips = AppBaseConfigBean.getTeam_user_tips();
                Constants.live_tips = AppBaseConfigBean.getLive_tips();
                Constants.together_room_anchor_tips = AppBaseConfigBean.getTogether_room_anchor_tips();
                Constants.together_room_other_tips = AppBaseConfigBean.getTogether_room_other_tips();
                Constants.show_age_level_continue = AppBaseConfigBean.getShow_age_level_continue();
                Constants.SERVER_TIMESTAMP_DETLA = System.currentTimeMillis() - AppBaseConfigBean.getServer_timestamp();
                Constants.cp_room_anchor_tips = AppBaseConfigBean.getCp_room_anchor_tips();
                Constants.cp_room_mic_user_tips = AppBaseConfigBean.getCp_room_mic_user_tips();
                Constants.cp_room_watch_user_tips = AppBaseConfigBean.getCp_room_watch_user_tips();
                Constants.private_room_tips = AppBaseConfigBean.getPrivate_room_tips();
                Constants.hide_makeup = AppBaseConfigBean.getHide_makeup();
                Constants.info_games = AppBaseConfigBean.getGames();
                Constants.cp_right_content = AppBaseConfigBean.getCp_right_content();
                Constants.cp_right_title = AppBaseConfigBean.getCp_right_title();
                Constants.cp_voice_chat_price = AppBaseConfigBean.getCp_voice_chat_price();
                Constants.hide_peking_user = AppBaseConfigBean.getIs_forbid_user();
                Constants.show_feed = AppBaseConfigBean.getShow_feed();
                Constants.recharge_way = AppBaseConfigBean.getRecharge_way();
                Constants.is_sounds_vip = AppBaseConfigBean.isIs_sounds_vip();
                Constants.buy_vip_money = AppBaseConfigBean.getBuy_vip_money();
                Constants.buy_vip_desc = AppBaseConfigBean.getBuy_vip_desc();
                Constants.buy_vip_time_limit = AppBaseConfigBean.getBuy_vip_time_limit();
                Constants.using_new_live_room = AppBaseConfigBean.isUsing_new_live_room();
                Constants.get_gift_box = AppBaseConfigBean.getGet_gift_box();
                Constants.buy_vip_get_coin = AppBaseConfigBean.getBuy_vip_get_coin();
                //SharePreferenceUtil.saveInt(SharePreferenceUtil.FILE_INFO_GAMES_DATA, SharePreferenceUtil.KEY_SHOW_SQUARE_FEED,AppBaseConfigBean.getShow_square_feed());
                SharePreferenceUtil.saveStringArray(SharePreferenceUtil.FILE_INFO_GAMES_DATA, SharePreferenceUtil.KEY_INFO_GAMES, AppBaseConfigBean.getGames());
                GiftPackageFloatManager.getInstance().recharge_way = AppBaseConfigBean.getRecharge_way();
                //@TODO  ---保持原有逻辑不变，逐步替换
                AppConfigUtil.saveBaseConfig(AppBaseConfigBean);
            } catch (Throwable e) {
                e.printStackTrace();
            }
        });

        appViewModel.getAppConfigDataFail().observe(this, AppBaseConfigBean -> {

        });



        getHandler().post(new Runnable() {
            @Override
            public void run() {
                appViewModel.getAppConfig();
                cancelMatchRoom(true);
            }
        });

    }


    public void cancelMatchRoom(boolean on_startup) {
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("on_startup", on_startup);
        } catch (Exception e) {
            e.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
        RetrofitHttpManager.getInstance().httpInterface.cancelMatchRoom(body).enqueue(new HttpCallback() {
            @Override
            public void OnSucceed(String result) {
            }

            @Override
            public void OnFailed(int ret, String result) {
            }
        });
    }
}
