package com.liquid.poros.ui.fragment

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.liquid.base.utils.ToastUtil
import com.liquid.im.kit.custom.InviteCoupleAttachment
import com.liquid.poros.R
import com.liquid.poros.adapter.CpCardDaysAdapter
import com.liquid.poros.analytics.utils.MultiTracker
import com.liquid.poros.base.BaseFragment
import com.liquid.poros.constant.Constants
import com.liquid.poros.constant.TrackConstants
import com.liquid.poros.entity.CoupleCardInfo
import com.liquid.poros.ui.dialog.MultiGiftSendDialog
import com.liquid.poros.ui.fragment.viewmodel.BuyCpCardViewModel
import com.liquid.poros.utils.StringUtil
import com.netease.nimlib.sdk.msg.MessageBuilder
import com.netease.nimlib.sdk.msg.constant.SessionTypeEnum
import com.netease.nimlib.sdk.msg.model.IMMessage
import kotlinx.android.synthetic.main.fragment_buy_cp_card.*
import java.util.*

class BuyCpCardFragment : BaseFragment() {
    private var viewModel: BuyCpCardViewModel? = null
    private var sessionId: String? = null
    private var cpOrderId: String? = null
    private var adapter: CpCardDaysAdapter? = null
    private var cardInfo: CoupleCardInfo? = null
    private var listener: OnSendMessageListener? = null
    private var selectDay: String? = null
    private var myBuyStyle : Boolean? = false

    fun setListener(listener: OnSendMessageListener) {
        this.listener = listener
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = getFragmentViewModel(BuyCpCardViewModel::class.java)
        adapter = CpCardDaysAdapter(context)
        adapter?.setListener { info ->
            cardInfo = info
            Glide.with(mContext).load(info?.img).into(super_cp_card)
            gift_key_num?.text = "礼盒钥匙 x" + info?.gift_box_keys
            card_price?.text = info?.card_price?.toString() + ""
            selectDay = cardInfo?.cp_time?.toString()
            trackUploadSuperCpDayClick()
        }
        val layoutManager = LinearLayoutManager(context)
        layoutManager.orientation = LinearLayoutManager.HORIZONTAL
        rv_cp_days?.layoutManager = layoutManager
        rv_cp_days?.adapter = adapter
        viewModel?.buyCpCardDataFail?.observe(viewLifecycleOwner, Observer { })
        viewModel?.buyCpCardDataSuccess?.observe(viewLifecycleOwner, Observer { data ->
            myBuyStyle = data?.isBuy_style
            if (data?.isBuy_style!!) {
                invite_to_be_super_cp?.text = "续费超级CP"
            } else {
                invite_to_be_super_cp?.text = "邀请成为超级CP"
            }
            (parentFragment as MultiGiftSendDialog?)?.updateCoinCount(data?.coin.toString())
            adapter?.setCpCardTime(data?.cp_card_time)
            adapter?.setDatas(data?.card_list)
        })
        viewModel?.buyCpCardFail?.observe(viewLifecycleOwner, Observer { data ->
            ToastUtil.show(data?.msg)
            listener?.fail()
        })
        viewModel?.buyCpCardSuccess?.observe(viewLifecycleOwner, Observer { data ->
            if (StringUtil.isNotEmpty(data?.cp_order_id)) {
                val attachment = InviteCoupleAttachment(data?.cp_order_id, "待确认", data?.cp_card_desc, 1, true,
                        data?.cp_card_title1, data?.cp_card_title3, data?.cp_card_title5, data?.cp_card_title6)
                val inviteMessage = MessageBuilder.createCustomMessage(
                        sessionId, SessionTypeEnum.P2P, "激活超级cp", attachment
                )
                listener?.sendMsg(inviteMessage, false)
            } else {
                listener?.updateP2PUi()
            }
            trackUploadCoupleSuperCp()
        })
        invite_to_be_super_cp?.setOnClickListener {
            cardInfo?.let {
                viewModel?.buyCouple(it.card_type, it.card_price.toString(), sessionId, cpOrderId)
            }
        }
        val argument = arguments
        sessionId = argument?.getString(Constants.FEMALE_GUEST_ID)
        cpOrderId = argument?.getString(Constants.CP_ORDER_ID)
        viewModel?.buyCpCard(sessionId, "CP", false)
    }

    override fun onFirstUserVisible() {

    }

    override fun notifyByThemeChanged() {

    }

    override fun onUserVisible() {

    }

    override fun getLoadingTargetView(): View? {
        return null
    }

    private fun trackUploadCoupleSuperCp() {
        val params = HashMap<String?, String?>()
        params[Constants.GUEST_ID] = sessionId
        params[Constants.IS_SUPER_CP] = myBuyStyle.toString()
        params[Constants.DAY_NUMBER] = selectDay
        MultiTracker.onEvent(TrackConstants.B_MAKE_SUPER_CP_CLICK, params)
    }

    private fun trackUploadSuperCpDayClick() {
        val params = HashMap<String?, String?>()
        params[Constants.GUEST_ID] = sessionId
        params[Constants.IS_SUPER_CP] = myBuyStyle.toString()
        params[Constants.DAY_NUMBER] = selectDay
        MultiTracker.onEvent(TrackConstants.B_SUPER_CP_DURATION_BUTTON_CLICK, params)
    }

    fun setCpOrderId(cpOrderId: String) {
        this.cpOrderId = cpOrderId
    }

    override fun getPageId(): String {
        return ""
    }

    override fun notifyNetworkChanged(isConnected: Boolean) {

    }

    override fun initViewsAndEvents(savedInstanceState: Bundle?) {
    }

    override fun onUserInvisible() {

    }

    override fun getContentViewLayoutID(): Int {
        return R.layout.fragment_buy_cp_card
    }

    interface OnSendMessageListener {
        fun sendMsg(inviteMessage: IMMessage?, resend: Boolean)
        fun fail()
        fun updateP2PUi()
    }

    companion object {
        @JvmStatic
        fun newInstance(arguments: Bundle?): BuyCpCardFragment {
            val fragment = BuyCpCardFragment()
            fragment.arguments = arguments
            return fragment
        }
    }
}