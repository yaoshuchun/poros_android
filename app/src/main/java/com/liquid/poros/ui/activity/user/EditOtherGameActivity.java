package com.liquid.poros.ui.activity.user;

import android.graphics.Rect;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.liquid.base.adapter.CommonAdapter;
import com.liquid.base.adapter.CommonViewHolder;
import com.liquid.base.event.EventCenter;
import com.liquid.poros.R;
import com.liquid.poros.base.BaseActivity;
import com.liquid.poros.analytics.utils.MultiTracker;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.entity.EditGameBean;
import com.liquid.poros.utils.ScreenUtil;
import com.liquid.poros.utils.ViewUtils;
import com.liquid.poros.widgets.HorizontalItemDecoration;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;

/**
 * 编辑其他游戏
 */
public class EditOtherGameActivity extends BaseActivity {
    @BindView(R.id.rv_other_game)
    RecyclerView rv_other_game;
    private CommonAdapter<EditGameBean.Item> mAdapter;
    private List<EditGameBean.Item> otherGames = new ArrayList<>();
    private String mOtherGames;

    @Override
    protected void parseBundleExtras(Bundle extras) {
        mOtherGames = extras.getString(Constants.USER_OTHER_GAME);
    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.activity_edit_other_game;
    }

    @Override
    protected String getPageId() {
        return TrackConstants.PAGE_EDIT_OTHER_GAME;
    }

    @Override
    protected void initViewsAndEvents(Bundle savedInstanceState) {
        this.otherGames = new Gson().fromJson(mOtherGames, new TypeToken<List<EditGameBean.Item>>() {
        }.getType());
        rv_other_game.setLayoutManager(new GridLayoutManager(EditOtherGameActivity.this, 3));
        rv_other_game.addItemDecoration(new HorizontalItemDecoration(ScreenUtil.dip2px(12)));
        rv_other_game.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                outRect.top = ScreenUtil.dip2px(8);
            }
        });
        mAdapter = new CommonAdapter<EditGameBean.Item>(EditOtherGameActivity.this, R.layout.item_edit_other_game) {
            @Override
            public void convert(CommonViewHolder holder, final EditGameBean.Item data, int position) {
                Glide.with(EditOtherGameActivity.this).load(data.image_url).into((ImageView) holder.getView(R.id.iv_other_game));
                holder.getView(R.id.item_other_game_line).setBackground(EditOtherGameActivity.this.getDrawable(data.select ? R.drawable.bg_color_10d8c8_line_20 : R.drawable.bg_color_transparent_line_20));
                holder.setVisibility(R.id.item_iv_other_game_check, data.select ? View.VISIBLE : View.GONE);
                holder.setOnClickListener(R.id.layout, v -> {
                    data.select = !data.select;
                    notifyDataSetChanged();
                });
            }
        };
        mAdapter.update(otherGames);
        rv_other_game.setAdapter(mAdapter);
    }

    @OnClick({R.id.save})
    public void onClick(View view) {
        if (ViewUtils.isFastClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.save:
                EventBus.getDefault().post(new EventCenter(Constants.EVENT_CODE_UPDATE_GAME_INFO_OTHER, otherGames));
                MultiTracker.onEvent(TrackConstants.B_SAVE_OTHER_GAME);
                finish();
                break;
        }
    }
}
