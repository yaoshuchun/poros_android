package com.liquid.poros.ui.fragment.dialog.user;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.liquid.poros.R;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * 举报弹窗
 */
public class FeedMoreDialogFragment extends BottomSheetDialogFragment implements View.OnClickListener {
    private TextView tv_report;
    private TextView tv_cancel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
        View view = View.inflate(getContext(), R.layout.fragment_feed_more_dialog, null);
        dialog.setContentView(view);
        //设置透明背景
        dialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);
        View root = dialog.getDelegate().findViewById(R.id.design_bottom_sheet);
        BottomSheetBehavior behavior = BottomSheetBehavior.from(root);
        behavior.setHideable(false);
        initializeView(view);
        return dialog;
    }

    @SuppressLint("ResourceType")
    private void initializeView(View view) {
        tv_report = view.findViewById(R.id.tv_report);
        tv_cancel = view.findViewById(R.id.tv_cancel);
        tv_report.setOnClickListener(this);
        tv_cancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_report:
                if (listener != null) {
                    listener.onReport();
                }
                dismissAllowingStateLoss();
                break;
            case R.id.tv_cancel:
                dismissAllowingStateLoss();
                break;
        }
    }

    private FeedMoreDialogFragment.Listener listener;

    public FeedMoreDialogFragment reportListener(FeedMoreDialogFragment.Listener listener) {
        this.listener = listener;
        return this;
    }

    public interface Listener {
        //举报
        void onReport();
    }
}
