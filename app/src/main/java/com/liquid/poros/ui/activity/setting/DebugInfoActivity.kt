package com.liquid.poros.ui.activity.setting

import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.FragmentActivity
import com.liquid.base.event.EventCenter
import com.liquid.base.tools.GT
import com.liquid.base.utils.BaseConstants
import com.liquid.base.utils.GlobalConfig
import com.liquid.base.utils.LogOut
import com.liquid.base.utils.ToastUtil
import com.liquid.im.kit.custom.CpInviteMaleAttachment
import com.liquid.im.kit.custom.DaoliuAttachment
import com.liquid.library.retrofitx.RetrofitX.Companion.url
import com.liquid.poros.R
import com.liquid.poros.ad.RewardVideoActivity
import com.liquid.poros.base.BaseActivity
import com.liquid.poros.base.BaseDialogFragment
import com.liquid.poros.constant.Constants
import com.liquid.poros.constant.Constants.TYPE_DIVERSION
import com.liquid.poros.constant.Constants.VIDEO_ROOM_DIVERSION_TYPE
import com.liquid.poros.constant.TrackConstants
import com.liquid.poros.entity.UpdateInfo
import com.liquid.poros.http.ApiUrl
import com.liquid.poros.http.observer.SimpleObserver
import com.liquid.poros.ui.LiveRoomDiversionMaleFragment.Companion.newInstance
import com.liquid.poros.ui.activity.live.RoomLiveActivityV2
import com.liquid.poros.ui.activity.scan.ScanCodeActivity
import com.liquid.poros.ui.activity.setting.magic.MagicApiActivity
import com.liquid.poros.ui.activity.user.SessionActivity
import com.liquid.poros.ui.dialog.LeadVideoRoomFragment
import com.liquid.poros.ui.fragment.dialog.common.CommonDialogFragment
import com.liquid.poros.ui.fragment.home.DownloadMessageDialogFragment
import com.liquid.poros.utils.*
import com.liquid.poros.utils.UIUtils.startLottieAnimation
import com.liquid.poros.utils.network.FileProgressRequestBody
import com.liquid.poros.utils.network.HttpCallback
import com.liquid.poros.utils.network.RetrofitHttpManager
import com.opensource.svgaplayer.SVGAParser
import com.opensource.svgaplayer.SVGAVideoEntity
import com.umeng.commonsdk.statistics.common.DeviceConfig
import de.greenrobot.event.EventBus
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.activity_debug_info.*
import okhttp3.MultipartBody
import org.json.JSONObject
import java.io.File

/**
 * 测试空间
 *
 * @author yejiurui
 */
class DebugInfoActivity : BaseActivity(), View.OnClickListener {
    private var entryScanCode = 0
    private val fileName = "latest_release.apk"
    override fun parseBundleExtras(extras: Bundle) {}
    override fun getContentViewLayoutID(): Int {
        return R.layout.activity_debug_info
    }

    override fun initViewsAndEvents(savedInstanceState: Bundle?) {
        //渠道&版本号
        tv_version_name!!.text = GlobalConfig.instance().channelName + "  V" + ViewUtils.getAppVersionName(this)
        iv_logo!!.setOnClickListener {
            entryScanCode++
            if (entryScanCode >= 3) {
                readyGo(ScanCodeActivity::class.java)
                entryScanCode = 0
            }
        }
        rl_user_protocol.setOnClickListener(this)
        rl_goto_ad.setOnClickListener(this)
        rl_toast.setOnClickListener(this)
//        rl_upload_error.setOnClickListener(this)
        rl_goto_dialog.setOnClickListener(this)
        rl_umeng_device.setOnClickListener(this)
        rl_download_apk.setOnClickListener(this)
        rl_api.setOnClickListener(this)
        rl_service.setOnClickListener(this)
        tv_start_svga.setOnClickListener(this)
        tv_start_lottie.setOnClickListener(this)
        rl_goto_message.setOnClickListener(this)

    }

    override fun getPageId(): String {
        return TrackConstants.PAGE_ABOUT_POROS
    }

    override fun onClick(view: View) {
        if (ViewUtils.isFastClick()) {
            return
        }
        when (view.id) {
            R.id.rl_toast -> ToastUtil.show("演示演示")
            R.id.rl_goto_dialog -> showLeadVideoRoomDialog()
            R.id.rl_umeng_device -> {
                val deviceInfo = getTestDeviceInfo(this)
                try {
                    if (deviceInfo != null && deviceInfo.size > 1) {
                        tv_device_info!!.text = "{'device_id':" + deviceInfo[0] + ",'mac':" + deviceInfo[1] + "}"
                    }
                } catch (e: Throwable) {
                    e.printStackTrace()
                }
            }
            R.id.rl_user_protocol -> readyGo(ScanCodeActivity::class.java)
            R.id.rl_goto_ad -> {
                ToastUtil.show("广告的appid:" + AppConfigUtil.getAd_csj_appid() + "  appcodeid:" + AppConfigUtil.getAd_csj_codeid(), Toast.LENGTH_LONG)
                readyGo(RewardVideoActivity::class.java)
            }
//            R.id.rl_upload_error -> {
//                showLoading()
//                object : Thread() {
//                    override fun run() {
//                        val filepath = Utils.compressFile(this@DebugInfoActivity)
//                        uploadLog(filepath, AccountUtil.getInstance().userId)
//                    }
//                }.start()
//            }
            R.id.rl_download_apk -> updateAPK()
            R.id.rl_api -> readyGo(MagicApiActivity::class.java)
            R.id.rl_service -> {
                ToastUtil.show("JQService已停用")
            }
            R.id.tv_start_svga -> {
                //SVGA动画
                var svgaName = et_svga_name.text.toString().trim()
                if (StringUtil.isEmpty(svgaName)) {
                    ToastUtil.show("请输入SVGA资源名称")
                    return
                }
                KeyboardHelper.hintKeyBoard(this)
                var svgaParser = SVGAParser(this)
                SVGAParser.shareParser().init(this)
                svgaParser.decodeFromAssets("${svgaName}.svga", object : SVGAParser.ParseCompletion {
                    override fun onComplete(videoItem: SVGAVideoEntity) {
                        if (view_svga != null) {
                            view_svga.visibility = View.VISIBLE
                            view_svga.setVideoItem(videoItem)
                            view_svga.stepToFrame(0, true)
                        }
                    }

                    override fun onError() {

                    }
                })
            }
            R.id.tv_start_lottie -> {
                //Lottie动画
                var lottieName = et_lottie_name.text.toString().trim()
                if (StringUtil.isEmpty(lottieName)) {
                    ToastUtil.show("请输入Lottie资源名称")
                    return
                }
                KeyboardHelper.hintKeyBoard(this)
                startLottieAnimation(view_lottie, lottieName, null)
            }
            R.id.rl_goto_message -> {
                val userId = et_goto_message!!.text.toString()
                if (StringUtil.isEmpty(userId)) {
                    ToastUtil.show("请在输入指定用户UserId")
                    return
                } else {
                    val bundle = Bundle()
                    bundle.putString(Constants.SESSION_ID, userId)
                    readyGo(SessionActivity::class.java, bundle)
                }
            }
        }
    }

    /**
     * 展示DialogFragment
     */
    private fun showLeadVideoRoomDialog() {
        val actionAttachment = DaoliuAttachment("")
        actionAttachment.nick_name = "泰岳男神"
        actionAttachment.age = "20"
        actionAttachment.avatar_url = "http://g.hiphotos.baidu.com/image/pic/item/6d81800a19d8bc3e770bd00d868ba61ea9d345f2.jpg"
        actionAttachment.couple_desc = "hello，我喜欢爱旅游和摄影的男孩子，一起打游戏旅游…"
        actionAttachment.nike_name = "小小旗令官"
        actionAttachment.game = "王者荣耀、刺激战场"
        actionAttachment.type = TYPE_DIVERSION
        actionAttachment.diversion_id = "310"
        actionAttachment.room_id = "11552733"
        actionAttachment.main_title = "主持人邀请你参观哦"
        actionAttachment.sub_title = "快来围观啦！"
        actionAttachment.anchor_avatar_url = "http://g.hiphotos.baidu.com/image/pic/item/6d81800a19d8bc3e770bd00d868ba61ea9d345f2.jpg"
        actionAttachment.source = TYPE_DIVERSION


        val videoRoomAttachment = DaoliuAttachment("")
        videoRoomAttachment.nick_name = "测试弹窗"
        videoRoomAttachment.age = "22"
        videoRoomAttachment.avatar_url = "http://g.hiphotos.baidu.com/image/pic/item/6d81800a19d8bc3e770bd00d868ba61ea9d345f2.jpg"
        videoRoomAttachment.couple_desc = "hello，我喜欢爱旅游和摄影的男孩子，一起打游戏旅游…"
        videoRoomAttachment.nike_name = "大大同学"
        videoRoomAttachment.game = "王者荣耀、刺激战场"
        videoRoomAttachment.type = VIDEO_ROOM_DIVERSION_TYPE
        videoRoomAttachment.diversion_id = "310"
        videoRoomAttachment.room_id = "11552733"
        videoRoomAttachment.main_title = "主持人邀请你参观哦"
        videoRoomAttachment.sub_title = "快来围观啦！"
        videoRoomAttachment.anchor_avatar_url = "http://g.hiphotos.baidu.com/image/pic/item/6d81800a19d8bc3e770bd00d868ba61ea9d345f2.jpg"
        videoRoomAttachment.source = VIDEO_ROOM_DIVERSION_TYPE

//        LiveRoomDiversionMaleFragment.Companion.newInstance().setAttachment(actionAttachment).setActivity(this@DebugInfoActivity).show(this@DebugInfoActivity.getSupportFragmentManager(), BaseDialogFragment::class.java.simpleName)

//        LeadVideoRoomFragment.newInstance("s").setAttachment(actionAttachment).setContext(this@DebugInfoActivity).show(this@DebugInfoActivity.supportFragmentManager, CommonDialogFragment::class.java.simpleName)
//        Handler().postDelayed({ replaceYourDialogFragment(actionAttachment) }, 1000)

        enterRoom(actionAttachment, null);
        actionAttachment.nick_name = "泰岳男神2222"
        enterRoom(actionAttachment, null);
        actionAttachment.nick_name = "泰岳男神3333"
        enterRoom(actionAttachment, null);

        enterRoom(videoRoomAttachment, null);
        videoRoomAttachment.nick_name = "测试弹窗2222"
        enterRoom(videoRoomAttachment, null);
        videoRoomAttachment.nick_name = "测试弹窗33333"
        enterRoom(videoRoomAttachment, null);
    }

    private fun enterRoom(actionAttachment: DaoliuAttachment, cpInviteMaleAttachment: CpInviteMaleAttachment?) {
        val room_id = actionAttachment.room_id
        val activity = MyActivityManager.getInstance().currentActivity as FragmentActivity
        if (activity != null) {
            if (activity is RoomLiveActivityV2) {
                val mRoomid = activity.mRoomId
                if (StringUtil.isEquals(mRoomid, room_id)) {
                    if (cpInviteMaleAttachment != null) {
                        EventBus.getDefault().post(EventCenter<Any?>(Constants.EVENT_CODE_INVITE_OPEN_MIC, cpInviteMaleAttachment))
                    }
                    return
                }
            }
            val ft = supportFragmentManager.beginTransaction()
            val fragmentToRemove = supportFragmentManager.findFragmentByTag(BaseDialogFragment::class.java.simpleName)

            LogOut.debug("tag:" + fragmentToRemove)
            if (fragmentToRemove != null) {
                ft.remove(fragmentToRemove)
                ft.commit()
            }
            //如果之前有弹窗则把之前的给移除了再弹出一个
            if (StringUtil.isEquals(actionAttachment.type, Constants.VIDEO_ROOM_DIVERSION_TYPE)) {
                newInstance().setAttachment(actionAttachment).setActivity(activity).show(activity.supportFragmentManager, BaseDialogFragment::class.java.simpleName)
            } else if (StringUtil.isEquals(actionAttachment.type, Constants.TYPE_DIVERSION) || StringUtil.isEquals(actionAttachment.type, Constants.TYPE_CP_GROUP_INVITE)) {
                LeadVideoRoomFragment.newInstance("s").setAttachment(actionAttachment).setContext(activity).show(activity.supportFragmentManager, BaseDialogFragment::class.java.simpleName)
            } else {
                LeadVideoRoomFragment.newInstance("s").setAttachment(actionAttachment).setContext(activity).show(activity.supportFragmentManager, BaseDialogFragment::class.java.simpleName)
            }
        }
    }

    private fun closeYourDialogFragment() {
        val ft = supportFragmentManager.beginTransaction()
        val fragmentToRemove = supportFragmentManager.findFragmentByTag(CommonDialogFragment::class.java.simpleName)
        if (fragmentToRemove != null) {
            ft.remove(fragmentToRemove)
        }
        ft.commit() // or ft.commitAllowingStateLoss()
    }

    private fun replaceYourDialogFragment(actionAttachment: DaoliuAttachment) {
        closeYourDialogFragment()
        LeadVideoRoomFragment.newInstance("s").setAttachment(actionAttachment).setContext(this@DebugInfoActivity).show(this@DebugInfoActivity.supportFragmentManager, CommonDialogFragment::class.java.simpleName)
    }

    /**
     * 获取下载文件
     *
     * @return
     */
    private fun getDownloadFile(): File? {
        val parentPath: String = this.getExternalCacheDir().getPath()
        return File(parentPath, "download" + File.separator + fileName)
    }

    private fun updateAPK() {
        if (getDownloadFile()?.exists()!!) {
            getDownloadFile()?.delete()
        }
        url(ApiUrl.APK_UPDATE_URL)
                .param("app_name", "jinquan")
                .param(BaseConstants.REMOTE_KEY.VERSION_NAME, "1.4.4.9")
                .get()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : SimpleObserver() {
                    override fun onNext(result: String?) {
                        val updateInfo = GT.fromJson(result, UpdateInfo::class.java)
                        if (updateInfo != null && updateInfo.code == 100) {
                            DownloadMessageDialogFragment.newInstance(false, updateInfo.desc, updateInfo.url, updateInfo.md5, false).show(supportFragmentManager, "DownloadMessage")
                        }
                    }
                })
    }

    fun uploadLog(path: String?, userId: String?) {
        try {
            val builder = MultipartBody.Builder().setType(MultipartBody.FORM) //表单类型
            val file = File(path) //filePath 图片地址
            val requestBody = FileProgressRequestBody(file, "multipart/form-data", null)
            builder.addFormDataPart("user_id", userId!!)
            builder.addFormDataPart("log_file", file.name, requestBody)
            val body = builder.build()
            RetrofitHttpManager.getInstance().httpInterface.upload_log(body).enqueue(object : HttpCallback() {
                override fun OnSucceed(result: String) {
                    LogOut.debug("user_upload_image = $result")
                    if (isDestroyed || isFinishing) {
                        return
                    }
                    try {
                        val `object` = JSONObject(result)
                        val code = `object`.getInt("code")
                        val message = `object`.getString("msg")
                        closeLoading()
                        if (code == 0) {
                            showMessage(getString(R.string.about_poros_upload_log_success))
                        } else {
                            showMessage(message)
                        }
                    } catch (e: Exception) {
                    }
                }

                override fun OnFailed(ret: Int, result: String) {
                    LogOut.debug("EditUserData--error = $result")
                    if (isDestroyed || isFinishing) {
                        return
                    }
                    closeLoading()
                    showMessage(getString(R.string.about_poros_upload_log_fail))
                }
            })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    companion object {
        fun getTestDeviceInfo(context: Context?): Array<String?> {
            val deviceInfo = arrayOfNulls<String>(2)
            try {
                if (context != null) {
                    deviceInfo[0] = DeviceConfig.getDeviceIdForGeneral(context)
                    deviceInfo[1] = DeviceConfig.getMac(context)
                }
            } catch (e: Exception) {
            }
            return deviceInfo
        }
    }
}