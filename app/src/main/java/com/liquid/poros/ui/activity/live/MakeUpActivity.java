package com.liquid.poros.ui.activity.live;

/*import android.content.res.Configuration;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.faceunity.nama.FURenderer;
import com.faceunity.nama.ui.BeautyControlView;
import com.liquid.im.kit.permission.MPermission;
import com.liquid.im.kit.permission.annotation.OnMPermissionDenied;
import com.liquid.im.kit.permission.annotation.OnMPermissionGranted;
import com.liquid.im.kit.permission.annotation.OnMPermissionNeverAskAgain;
import com.liquid.poros.R;
import com.liquid.poros.base.BaseActivity;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.helper.SimpleAVChatStateObserver;
import com.netease.nimlib.sdk.avchat.AVChatManager;
import com.netease.nimlib.sdk.avchat.AVChatStateObserver;
import com.netease.nimlib.sdk.avchat.constant.AVChatMediaCodecMode;
import com.netease.nimlib.sdk.avchat.constant.AVChatUserRole;
import com.netease.nimlib.sdk.avchat.constant.AVChatVideoCaptureOrientation;
import com.netease.nimlib.sdk.avchat.constant.AVChatVideoCropRatio;
import com.netease.nimlib.sdk.avchat.constant.AVChatVideoFrameRate;
import com.netease.nimlib.sdk.avchat.constant.AVChatVideoQuality;
import com.netease.nimlib.sdk.avchat.constant.AVChatVideoScalingType;
import com.netease.nimlib.sdk.avchat.model.AVChatParameters;
import com.netease.nimlib.sdk.avchat.video.AVChatCameraCapturer;
import com.netease.nimlib.sdk.avchat.video.AVChatSurfaceViewRenderer;
import com.netease.nimlib.sdk.avchat.video.AVChatVideoCapturerFactory;
import com.netease.nrtc.sdk.common.VideoFilterParameter;
import com.netease.nrtc.sdk.video.VideoFrame;
import com.netease.nrtc.video.coding.VideoFrameFormat;

import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;*/

import android.os.Bundle;

import com.liquid.poros.base.BaseActivity;

public class MakeUpActivity extends BaseActivity {
    @Override
    protected String getPageId() {
        return null;
    }

    @Override
    protected void parseBundleExtras(Bundle extras) {

    }

    @Override
    protected int getContentViewLayoutID() {
        return 0;
    }

    @Override
    protected void initViewsAndEvents(Bundle savedInstanceState) {

    }

    /*@BindView(R.id.video_render)

    @BindView(R.id.fu_beauty_control)
    BeautyControlView beautyControlView;

    private boolean mIsFirstFrame = true;
    private FURenderer mFURenderer;
    private int mSkippedFrames;
    private int mCameraFacing = Camera.CameraInfo.CAMERA_FACING_FRONT;
    private boolean isReleaseEngine = true;
    private Handler mVideoEffectHandler;

    @Override
    protected void parseBundleExtras(Bundle extras) {

    }


    AVChatStateObserver stateObserver = new SimpleAVChatStateObserver() {

        @Override
        public void onJoinedChannel(int code, String s, String s1, int i1) {
            AVChatManager.getInstance().setSpeaker(true);
        }

        @Override
        public void onUserJoined(String account) {

        }

        @Override
        public void onUserLeave(String account, int i) {

        }

        @Override
        public void onCallEstablished() {
            AVChatManager.getInstance().enableAudienceRole(false);
        }


        @Override
        public void onFirstVideoFrameRendered(String s) {

        }

        @Override
        public void onReportSpeaker(Map<String, Integer> speakers, int mixedEnergy) {

        }

        private byte[] i420Byte;
        private byte[] readbackByte;

        @Override
        public boolean onVideoFrameFilter(VideoFrame input, VideoFrame[] outputFrames, VideoFilterParameter videoFilterParameter) {
            if (mFURenderer == null) {
                return true;
            }
            VideoFrame.Buffer buffer = input.getBuffer();
            int width = buffer.getWidth();
            int height = buffer.getHeight();
            int rotation = input.getRotation();
            int format = buffer.getFormat();
            if (mIsFirstFrame) {
                mVideoEffectHandler = new Handler(Looper.myLooper());
                mFURenderer.onSurfaceCreated();
                int dataSize = width * height * 3 / 2;
                i420Byte = new byte[dataSize];
                readbackByte = new byte[dataSize];
                mIsFirstFrame = false;
            }

            // I420 格式
            if (format == VideoFrameFormat.kVideoI420) {
                buffer.toBytes(i420Byte);
                // FU 美颜滤镜
                mFURenderer.onDrawFrameSingleInput(i420Byte, width, height, FURenderer.INPUT_FORMAT_I420);
                if (mSkippedFrames > 0) {
                    mSkippedFrames--;
                    VideoFrame.Buffer rotatedBuffer = buffer.rotate(videoFilterParameter.frameRotation);
                    VideoFrame outputFrame = new VideoFrame(rotatedBuffer, rotation, input.getTimestampMs());
                    outputFrames[0] = outputFrame;
                } else {
                    // 数据回传
                    try {
                        VideoFrame.Buffer outputBuffer = VideoFrame.asBuffer(i420Byte, format, width, height);
                        VideoFrame.Buffer rotatedBuffer = outputBuffer.rotate(videoFilterParameter.frameRotation);
                        VideoFrame outputFrame = new VideoFrame(rotatedBuffer, rotation, input.getTimestampMs());
                        outputFrames[0] = outputFrame;
                        outputBuffer.release();
                    } catch (IllegalAccessException e) {
                        Log.e(TAG, "onVideoFrameFilter: ", e);
                    }
                }
            }
            input.release();
            return true;
        }
    };

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.activity_make_up;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        MPermission.onRequestPermissionsResult(this, requestCode, permissions, grantResults);
    }

    private void checkPermission() {
        List<String> lackPermissions = AVChatManager.checkPermission(MakeUpActivity.this);
        if (lackPermissions.isEmpty()) {
            onBasicPermissionSuccess();
        } else {
            String[] permissions = new String[lackPermissions.size()];
            for (int i = 0; i < lackPermissions.size(); i++) {
                permissions[i] = lackPermissions.get(i);
            }
            MPermission.with(MakeUpActivity.this)
                    .setRequestCode(BASIC_PERMISSION_REQUEST_CODE)
                    .permissions(permissions)
                    .request();
        }
    }

    private void startEngine() {
        if (!isReleaseEngine) {
            closeLoading();
            return;
        }
        AVChatManager.getInstance().enableRtc();
        isReleaseEngine = false;
        if (mVideoCapturer == null) {
            mVideoCapturer = AVChatVideoCapturerFactory.createCamera2Capturer(true, false);
        }
        AVChatManager.getInstance().setupVideoCapturer(mVideoCapturer);
        AVChatParameters parameters = new AVChatParameters();
        parameters.setBoolean(AVChatParameters.KEY_SESSION_LIVE_MODE, true);
        parameters.setInteger(AVChatParameters.KEY_SESSION_MULTI_MODE_USER_ROLE, AVChatUserRole.NORMAL);
        parameters.setString(AVChatParameters.KEY_VIDEO_ENCODER_MODE, AVChatMediaCodecMode.MEDIA_CODEC_SOFTWARE);
        parameters.setString(AVChatParameters.KEY_VIDEO_DECODER_MODE, AVChatMediaCodecMode.MEDIA_CODEC_AUTO);
        parameters.setInteger(AVChatParameters.KEY_VIDEO_QUALITY, AVChatVideoQuality.QUALITY_540P);
        parameters.setBoolean(AVChatParameters.KEY_AUDIO_REPORT_SPEAKER, true);//开启声音强度汇报
        parameters.setInteger(AVChatParameters.KEY_VIDEO_FRAME_RATE, AVChatVideoFrameRate.FRAME_RATE_15);
        parameters.setInteger(AVChatParameters.KEY_VIDEO_FIXED_CROP_RATIO, AVChatVideoCropRatio.CROP_RATIO_16_9);
        parameters.setBoolean(AVChatParameters.KEY_VIDEO_ROTATE_IN_RENDING, false);
        parameters.setBoolean(AVChatParameters.KEY_VIDEO_FRAME_FILTER_NEW, true);
        int videoOrientation = getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT ?
                AVChatVideoCaptureOrientation.ORIENTATION_PORTRAIT : AVChatVideoCaptureOrientation.ORIENTATION_LANDSCAPE_RIGHT;

        parameters.setInteger(AVChatParameters.KEY_VIDEO_CAPTURE_ORIENTATION, videoOrientation);
        AVChatManager.getInstance().setParameters(parameters);

        AVChatManager.getInstance().enableVideo();
        AVChatManager.getInstance().setupLocalVideoRender(null, false, AVChatVideoScalingType.SCALE_ASPECT_FIT);
        AVChatManager.getInstance().setupLocalVideoRender(videoRender, false, AVChatVideoScalingType.SCALE_ASPECT_FIT);
        AVChatManager.getInstance().startVideoPreview();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        releaseVideoEffect();
        releaseEngine();
        registerAudienceObservers(false);
    }

    private void releaseVideoEffect() {
        Log.d(TAG, "releaseVideoEffect() called");
        // 释放资源
        if (mVideoEffectHandler != null) {
            mVideoEffectHandler.post(new Runnable() {
                @Override
                public void run() {
                    if (mFURenderer != null) {
                        mFURenderer.onSurfaceDestroyed();
                        mFURenderer = null;
                    }
                }
            });
        }

    }

    private void releaseEngine() {
        if (isReleaseEngine) {
            return;
        }
        AVChatManager.getInstance().stopVideoPreview();
        AVChatManager.getInstance().disableVideo();
        AVChatManager.getInstance().disableRtc();
        isReleaseEngine = true;
    }

    @Override
    protected void initViewsAndEvents(Bundle savedInstanceState) {
        beautyControlView.hideSettingAction();
        beautyControlView.hideMakeUp(Constants.hide_makeup);
        getHandler().postDelayed(new Runnable() {
            @Override
            public void run() {
                registerAudienceObservers(true);
                initRender();
                checkPermission();
            }
        }, 200);
    }

    private void initRender() {
        FURenderer.initFURenderer(this);
        if (mFURenderer == null) {
            mFURenderer = new FURenderer.Builder(this)
                    .setCreateEGLContext(true)
                    .setInputTextureType(FURenderer.INPUT_2D_TEXTURE)
                    .setCameraType(mCameraFacing)
                    .setInputImageOrientation(FURenderer.getCameraOrientation(mCameraFacing))
                    .build();
        }
        if (beautyControlView != null) {
            beautyControlView.setOnFaceUnityControlListener(mFURenderer);
        }
    }

    @OnClick({R.id.tv_cancel, R.id.tv_confirm})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_confirm:
                beautyControlView.saveMakeUpParams();
                finish();
                break;
            case R.id.tv_cancel:
                finish();
                break;
        }

    }

    @OnMPermissionGranted(BASIC_PERMISSION_REQUEST_CODE)
    public void onBasicPermissionSuccess() {
        onPermissionChecked();
    }

    @OnMPermissionDenied(BASIC_PERMISSION_REQUEST_CODE)
    @OnMPermissionNeverAskAgain(BASIC_PERMISSION_REQUEST_CODE)
    public void onBasicPermissionFailed() {
        Toast.makeText(this, "音视频通话所需权限未全部授权，部分功能可能无法正常运行！", Toast.LENGTH_SHORT).show();
    }

    private void onPermissionChecked() {
        startEngine();
    }

    private void registerAudienceObservers(boolean register) {
        AVChatManager.getInstance().observeAVChatState(stateObserver, register);
    }
    @Override
    protected String getPageId() {
        return TrackConstants.PAGE_MAKE_UP;
    }*/
}
