package com.liquid.poros.ui.activity.pta;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.liquid.poros.R;
import com.liquid.poros.base.BaseActivity;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.entity.PtaShopCartInfo;
import com.liquid.poros.ui.fragment.dialog.common.CommonActionListener;
import com.liquid.poros.ui.fragment.dialog.common.CommonDialogBuilder;
import com.liquid.poros.ui.fragment.dialog.common.CommonDialogFragment;
import com.liquid.poros.ui.fragment.dialog.pta.PtaShopCartDialogFragment;
import com.liquid.poros.utils.ViewUtils;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.OnClick;

/**
 * 个人装扮设置界面
 */
@Deprecated
public class PtaSettingActivity extends BaseActivity {
    @BindView(R.id.tv_pta_shop_cart_count)
    TextView tv_pta_shop_cart_count;
    private ArrayList<PtaShopCartInfo.Data.PtaShopItem> ptaShopItems = new ArrayList<>();
    private PtaShopCartDialogFragment ptaShopCartDialogFragment = PtaShopCartDialogFragment.newInstance();
    @Override
    protected void parseBundleExtras(Bundle extras) {

    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.activity_pta_setting;
    }

    @Override
    protected String getPageId() {
        return TrackConstants.PAGE_PTA_SETTING;
    }

    @Override
    protected void initViewsAndEvents(Bundle savedInstanceState) {
        for (int i = 0; i < 2; i++) {
            PtaShopCartInfo.Data.PtaShopItem ptaShopItem = new PtaShopCartInfo.Data.PtaShopItem();
            ptaShopItem.setPta_title("神秘车头套装" + i);
            ptaShopItem.setPta_coin("11" + i);
            ptaShopItem.setPta_id(String.valueOf(i));
            ArrayList<PtaShopCartInfo.Data.PtaShopItem.PtaDurationItem> ptaShopItemsType = new ArrayList<>();
            PtaShopCartInfo.Data.PtaShopItem.PtaDurationItem ptaDurationItem1 = new PtaShopCartInfo.Data.PtaShopItem.PtaDurationItem(3, 1,true);
            PtaShopCartInfo.Data.PtaShopItem.PtaDurationItem ptaDurationItem2 = new PtaShopCartInfo.Data.PtaShopItem.PtaDurationItem(7, 2,false);
            PtaShopCartInfo.Data.PtaShopItem.PtaDurationItem ptaDurationItem3 = new PtaShopCartInfo.Data.PtaShopItem.PtaDurationItem(15, 3,false);
            PtaShopCartInfo.Data.PtaShopItem.PtaDurationItem ptaDurationItem4 = new PtaShopCartInfo.Data.PtaShopItem.PtaDurationItem(30, 4,false);
            ptaShopItemsType.add(ptaDurationItem1);
            ptaShopItemsType.add(ptaDurationItem2);
            ptaShopItemsType.add(ptaDurationItem3);
            ptaShopItemsType.add(ptaDurationItem4);
            ptaShopItem.setPtaDuration(ptaShopItemsType);
            ptaShopItems.add(ptaShopItem);
        }
        tv_pta_shop_cart_count.setText(String.valueOf(ptaShopItems.size()));
    }

    @OnClick({R.id.iv_left, R.id.fl_shop_cart})
    public void onClick(View view) {
        if (ViewUtils.isFastClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_left:
                back();
                break;
            case R.id.fl_shop_cart:
                if (ptaShopCartDialogFragment.isAdded()){
                    return;
                }
                ptaShopCartDialogFragment .setPtaShopList(ptaShopItems).show(getSupportFragmentManager(), PtaShopCartDialogFragment.class.getSimpleName());
                break;
        }
    }

    @Override
    public void onBackPressed() {
        back();
    }

    private void back() {
        hideKeyBoard();
        new CommonDialogBuilder()
                .setDesc(getString(R.string.dialog_pta_setting_back_title))
                .setListener(new CommonActionListener() {
                    @Override
                    public void onConfirm() {
                        //TODO 保存装扮  操作
                    }

                    @Override
                    public void onCancel() {
                        finish();
                    }
                }).create().show(getSupportFragmentManager(), CommonDialogFragment.class.getSimpleName());
    }
}