package com.liquid.poros.ui.dialog

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.View.OnClickListener
import android.view.ViewGroup
import android.widget.*
import androidx.viewpager.widget.ViewPager
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.liquid.base.utils.ToastUtil
import com.liquid.poros.R
import com.liquid.poros.analytics.utils.MultiTracker
import com.liquid.poros.base.BaseActivity
import com.liquid.poros.constant.GiftTypeEnum
import com.liquid.poros.constant.TrackConstants
import com.liquid.poros.entity.MicUserInfo
import com.liquid.poros.entity.MsgUserModel
import com.liquid.poros.entity.MsgUserModel.GiftNums
import com.liquid.poros.utils.FileDownloadUtil
import com.liquid.poros.utils.LottieAnimationUtils
import com.liquid.poros.ui.activity.live.RoomLiveActivityV2
import com.liquid.poros.ui.activity.live.viewmodel.RoomLiveViewModel
import com.liquid.poros.utils.AccountUtil
import com.liquid.poros.utils.ViewUtils
import com.liquid.poros.widgets.GiftPagerAdapter
import com.liquid.poros.widgets.SelectGiftNumView
import com.liquid.poros.widgets.SelectGiftNumView.OnGiftNumItemSelectListener
import com.luck.picture.lib.tools.ToastUtils
import com.netease.neliveplayer.playerkit.common.net.NetworkUtil
import java.io.File
import kotlin.collections.HashMap

class SendGiftView : LinearLayout {
    private var vpGiftContainer: ViewPager? = null
    private var indicatorDot: LinearLayout? = null
    private var giftTopBar:RelativeLayout? = null
    private var btnSendGift: TextView? = null

    @JvmField
    var giftPanel: LinearLayout? = null
    private var selectGiftNum: TextView? = null
    private var tvCoinCount: TextView? = null
    private var listener: OnSendGiftClickListener? = null
    private var pagerAdapter: GiftPagerAdapter? = null
    var showGiftNum: LinearLayout? = null
    private var selectGiftArrow: ImageView? = null
    private var toRechargePage: TextView? = null
    private var sessionId: String? = null
    private var from: String? = null
    private var source: String? = null
    private var norGiftTitle: TextView? = null
    private var roomGiftTitle: LinearLayout? = null
    private var addFriendTip: LinearLayout? = null
    private var giftPresenterBtn:LinearLayout?=null
    var selectGiftNumPanel: SelectGiftNumView? = null
        private set
    private var giftBeans: List<MsgUserModel.GiftBean>? = null
    private var options: RequestOptions? = null
    private var userAvatar: ImageView? = null
    private var userName: TextView? = null
    private var mIsLiveRoom = false
    private var isAnchor = false
    private var coinCount: Long = 0
    private var sendGiftNum = 1
    private var pageCount = 0
    private var selectPos = 0
    private var toPos = 0
    private var retryCount = 0
    private var mListener:CustomTouchListener? = null
    private var micUserInfos: Array<MicUserInfo?>? = null
    private var micUserInfo: MicUserInfo? = null
    private var roomType = 0
    private var mContext:Context? = null
    private var roomId: String? = null
    private var trackBlindBoxLevel :String?=null //埋点:盲盒等级（初级、中级、高级）
    private val rechargeCoinDialog: RechargeCoinDialog by lazy {
        val dialog = RechargeCoinDialog()
        dialog.setRoomId(roomId)
        dialog.setListener { coin ->
            setCoinCount(coin.toLong())
        }
        dialog
    }
    private val PK_STATUS_PK_DONGING = 0

    constructor(context: Context) : super(context) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(context)
    }

    fun setListener(listener: OnSendGiftClickListener?) {
        this.listener = listener
    }

    private fun init(context: Context) {
        mContext = context
        orientation = VERTICAL
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        inflater.inflate(R.layout.dialog_show_all_gift, this)
        options = RequestOptions.bitmapTransform(RoundedCorners(20))
        isClickable = true
        initView()
    }

    fun setIsTrial(isTrial: Boolean) {
        if(isTrial) {
            giftTopBar !!.visibility = GONE
        }else {
            giftTopBar !!.visibility = VISIBLE
        }
    }

    fun setIsLiveRoom(isLiveRoom: Boolean) {
        mIsLiveRoom = isLiveRoom
        if (mIsLiveRoom) {
            norGiftTitle!!.visibility = GONE
            roomGiftTitle!!.visibility = VISIBLE
            addFriendTip!!.visibility = VISIBLE
        } else {
            norGiftTitle!!.visibility = VISIBLE
            roomGiftTitle!!.visibility = GONE
            addFriendTip!!.visibility = GONE
        }
    }

    fun setAddFriendTipVisible(spendCoin: Int) {
        if (addFriendTip != null) {
            if (spendCoin >= 52) {
                addFriendTip!!.visibility = GONE
            } else {
                addFriendTip!!.visibility = VISIBLE
            }
        }
    }

    fun setCustomListener(listener:CustomTouchListener) {
        mListener = listener
    }

    /**
     * 选中盲盒位置
     */
    fun setBlindBoxPosition(blindBoxPosition:Int) {
        selectPos = blindBoxPosition
        pagerAdapter?.notifyBlindBox(blindBoxPosition)
        pagerAdapter?.notifyDataSetChanged()
    }

    interface CustomTouchListener{
        fun onTouch()
        fun giftSelect(select:Int)
    }

    override fun onVisibilityChanged(changedView: View, visibility: Int) {
        super.onVisibilityChanged(changedView, visibility)
        if (visibility == VISIBLE) {
            val params = HashMap<String?, String?>()
            if (micUserInfos != null) {
                params["anchor_id"] = micUserInfos!![0]!!.user_id
                if (roomType == RoomLiveActivityV2.ROOM_TYPE_PK) {
                    if (micUserInfos!![1] != null) {
                        params["left_female_guest_id"] = micUserInfos!![1]!!.user_id
                    }
                    if (micUserInfos!![2] != null) {
                        params["right_female_guest_id"] = micUserInfos!![2]!!.user_id
                    }
                    if (toPos != 0 && micUserInfo != null) {
                        params["female_guest_id"] = micUserInfo!!.user_id
                    }
                    params["room_type"] = "pk_room"
                }
            }
            if(source != null){
                params["source"] = source
            }
            if (isAnchor) {
                params["host_gift_window_from"] = from
                MultiTracker.onEvent(TrackConstants.B_SEND_GIFT_TO_HOST_EXPOSURE, params)
            } else {
                params["guest_gift_window_from"] = from
                MultiTracker.onEvent(TrackConstants.B_SEND_GIFT_TO_GUEST_EXPOSURE, params)
            }
        }
    }

    fun setFrom(from: String?) {
        this.from = from
    }

    fun setSource(source: String?) {
        this.source = source
    }

    fun setRoomId(roomId: String?) {
        this.roomId = roomId
    }

    fun setUserInfo(micUserInfos: Array<MicUserInfo?>?, isAnchor: Boolean, isFriend: Boolean, toPos: Int, roomType: Int) {
        if (micUserInfos.isNullOrEmpty() || micUserInfos.size <= toPos) return
        this.micUserInfos = micUserInfos
        micUserInfo = micUserInfos[toPos]
        this.toPos = toPos
        this.isAnchor = isAnchor
        this.roomType = roomType
        if (isFriend) {
            addFriendTip!!.visibility = VISIBLE
        } else {
            addFriendTip!!.visibility = GONE
        }
        if (userName != null && micUserInfo != null) {
            userName!!.text = micUserInfo!!.nick_name
            if (userAvatar != null) {
                Glide.with(context!!).applyDefaultRequestOptions(options!!).load(micUserInfo!!.avatar_url).into(userAvatar!!)
            }
        }
    }

    fun setData(giftBeans: List<MsgUserModel.GiftBean>?, giftNums: List<GiftNums?>?) {
        if (giftBeans == null || giftBeans.size == 0 || this.giftBeans != null) return
        this.giftBeans = giftBeans
        pagerAdapter = GiftPagerAdapter(giftBeans, context, selectPos)
        vpGiftContainer!!.adapter = pagerAdapter
        pageCount = Math.ceil((giftBeans.size.toFloat() / 8f).toDouble()).toInt()
        indicatorDot!!.removeAllViews()
        for (i in 0 until pageCount) {
            val view = View(context)
            val params = LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            params.setMargins(10, 0, 10, 0)
            params.height = 16
            params.width = 16
            view.layoutParams = params
            if (i == 0) {
                view.setBackgroundResource(R.drawable.bg_indicator_dot_focus)
            } else {
                view.setBackgroundResource(R.drawable.bg_indicator_dot_unfocus)
            }
            indicatorDot!!.addView(view)
        }
        pagerAdapter!!.setListener { pos ->
            if (selectGiftNumPanel!!.visibility == VISIBLE) {
                selectGiftNumPanel!!.visibility = GONE
            }
            selectPos = pos
            mListener?.giftSelect(selectPos)
            //盲盒礼物,隐藏数量选择
            val giftBean = giftBeans!![selectPos]
            selectGiftArrow?.visibility = if (giftBean.giftType == GiftTypeEnum.GIFT_BLIND) View.INVISIBLE else View.VISIBLE
            if (giftBean.giftType == GiftTypeEnum.GIFT_BLIND) {
                //选中盲盒,数量充值为1
                sendGiftNum = 1
                selectGiftNum?.text = "1"
                trackSelectBlindBoxGift(giftBean.name)
            }
        }
        if (giftNums != null && giftNums.size != 0) {
            selectGiftNumPanel!!.setData(giftNums)
        }
    }

    fun setCoinCount(coinCount: Long) {
        this.coinCount = coinCount
        tvCoinCount!!.text = coinCount.toString() + ""
    }

    fun setCoinCount(coinCountStr: String?) {
        try {
            val iCoinCount = Integer.valueOf(coinCountStr)
            tvCoinCount!!.text = (coinCount + iCoinCount).toString() + ""
        } catch (e: Exception) {
        }
    }

    fun setCoinBalance(coinCountStr: String?) {
        try {
            tvCoinCount!!.text = coinCountStr
        } catch (e: Exception) {
        }
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
    }

    fun setSelectGiftNumPanelVisible() {
        selectGiftNumPanel!!.visibility = GONE
    }

    @SuppressLint("ClickableViewAccessibility", "SetTextI18n")
    private fun initView() {
        vpGiftContainer = findViewById(R.id.vp_gift_container)
        indicatorDot = findViewById(R.id.indicator_dot)
        btnSendGift = findViewById(R.id.btn_send_gift)
        giftPanel = findViewById(R.id.gift_panel)
        tvCoinCount = findViewById(R.id.coin_count)
        showGiftNum = findViewById(R.id.show_gift_count)
        selectGiftArrow = findViewById(R.id.select_gift_arrow)
        selectGiftNum = findViewById(R.id.select_gift_num)
        toRechargePage = findViewById(R.id.to_recharge_page)
        norGiftTitle = findViewById(R.id.normal_gift_title)
        roomGiftTitle = findViewById(R.id.room_gift_title)
        userAvatar = findViewById(R.id.user_avatar)
        userName = findViewById(R.id.user_name)
        giftPresenterBtn = findViewById(R.id.gift_present_btn)
        giftTopBar = findViewById(R.id.gift_top_bar)
        addFriendTip = findViewById(R.id.add_friend_tip)
        selectGiftNumPanel = findViewById(R.id.select_gift_num_panel)
        setOnTouchListener { v: View?, event: MotionEvent ->
            mListener?.onTouch()
            if (selectGiftNumPanel!!.visibility == VISIBLE && !isTouchPointInView(selectGiftNumPanel, event.rawX.toInt(), event.rawY.toInt())) {
                selectGiftNumPanel!!.visibility = GONE
            }
            true
        }
        vpGiftContainer?.addOnPageChangeListener(object : OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
            override fun onPageSelected(position: Int) {
                for (i in 0 until pageCount) {
                    if (position == i) {
                        indicatorDot!!.getChildAt(position).setBackgroundResource(R.drawable.bg_indicator_dot_focus)
                    } else {
                        indicatorDot!!.getChildAt(i).setBackgroundResource(R.drawable.bg_indicator_dot_unfocus)
                    }
                }
            }

            override fun onPageScrollStateChanged(state: Int) {}
        })
        btnSendGift?.setOnClickListener(OnClickListener {
            if (selectGiftNumPanel?.visibility == VISIBLE) {
                selectGiftNumPanel?.visibility = GONE
            }
            if (ViewUtils.isFastClick()) {
                ToastUtils.s(context, "点击太过频繁")
                return@OnClickListener
            }
            if (!NetworkUtil.isNetAvailable(context)) {
                Toast.makeText(context, getContext().getString(R.string.network_not_available), Toast.LENGTH_SHORT).show()
                return@OnClickListener
            }
            val giftBean = giftBeans!![selectPos]
            if (giftBean.giftType != GiftTypeEnum.GIFT_BLIND) {
                val giftFileName = LottieAnimationUtils.getGiftFileName(giftBean.id)
                if (!File(FileDownloadUtil.parentPath + File.separator + giftFileName).exists()) {
                    ToastUtil.show("请稍等，正在下载资源")
                    MultiTracker.onEvent(TrackConstants.GIFT_RESOURCE_UNDOWNLOAD, HashMap<String, String>())
                    retryCount++
                    if (retryCount >= 3) {
                        FileDownloadUtil.download(giftFileName)
                        retryCount = 0
                    }
                    return@OnClickListener
                }
                //如果是视频房Activity 则走最新的逻辑
                if (context is RoomLiveActivityV2) {
                    if (roomType == RoomLiveActivityV2.ROOM_TYPE_PK) {
                        (context as BaseActivity).getActivityViewModel(RoomLiveViewModel::class.java).sendGift(roomId, micUserInfo!!.user_id, giftBean.id, giftBean.name, sendGiftNum, "anchor" == micUserInfo!!.room_role, toPos, false)
                    } else {
                        (context as BaseActivity).getActivityViewModel(RoomLiveViewModel::class.java).sendGift(roomId, micUserInfo!!.user_id, giftBean.id, giftBean.name, sendGiftNum, "anchor" == micUserInfo!!.room_role, -1, false)
                    }
                    trackSendGift(giftBean.id, giftBean.name, toPos, sendGiftNum, 0)
                }
            }
            if (listener != null) {
                if (giftBean != null) {
                    val effect = giftBean.effect
                    var audio: String? = null
                    var json: String? = null
                    if (effect != null) {
                        audio = effect.audio
                        json = effect.json;
                    }
                    listener!!.sendGift(giftBean.id, giftBean.icon, sendGiftNum, audio,
                            giftBean.name, json, toPos,source)
                }
            }
        })
        showGiftNum?.setOnClickListener(OnClickListener {
            val giftBean = giftBeans!![selectPos]
            if (giftBean.giftType != GiftTypeEnum.GIFT_BLIND) {
                if (selectGiftNumPanel?.visibility == VISIBLE) {
                    selectGiftNumPanel?.visibility = GONE
                } else {
                    uploadSelectGiftNumPanelExtend()
                    selectGiftNumPanel?.visibility = VISIBLE
                }
            }
        })
        selectGiftNumPanel?.setListener(OnGiftNumItemSelectListener { num: Int ->
            sendGiftNum = num
            selectGiftNum?.text = num.toString() + ""
        })
        toRechargePage?.setOnClickListener(OnClickListener { view: View? ->
            if (selectGiftNumPanel?.visibility == VISIBLE) {
                selectGiftNumPanel?.visibility = GONE
            }
            if (context is RoomLiveActivityV2) {
                rechargeCoinDialog.setPositon(RechargeCoinDialog.POSITION_GIFT_ALERT)
                rechargeCoinDialog.show((context as RoomLiveActivityV2).supportFragmentManager, RechargeCoinDialog::class.java.name)
                rechargeCoinDialog.setWindowFrom("gift_jump_to_recharge")
            }
            if (listener != null) {
                listener!!.jumpRecharge()
                listener!!.jumpRecharge(RechargeCoinDialog.POSITION_GIFT_ALERT)
            }
        })
    }

    //TODO PK status需要维护
    private fun trackSendGift(giftId: Int, name: String, toPos: Int, giftNum: Int, pkStatus: Int) {
        val params = HashMap<String?, String?>()
        params["room_id"] = roomId
        params["anchor_id"] = micUserInfos!![0]!!.user_id
        if (roomType == RoomLiveActivityV2.ROOM_TYPE_PK) {
            if (micUserInfos!![1] != null) {
                params["left_female_guest_id"] = micUserInfos!![1]!!.user_id
            }
            if (micUserInfos!![2] != null) {
                params["right_female_guest_id"] = micUserInfos!![2]!!.user_id
            }
            params["room_type"] = "pk_room"
        }
        params["gift_id"] = giftId.toString()
        params["gift_name"] = name
        params["gift_num"] = giftNum.toString()
        params["pk_status"] = if (pkStatus == PK_STATUS_PK_DONGING) "1" else "2"
        params["send_to"] = if (toPos == 0) "anchor" else "guest"
        MultiTracker.onEvent(TrackConstants.B_USER_SEND_GIFT_CLICK, params)
    }

    //(x,y)是否在view的区域内
    private fun isTouchPointInView(view: View?, x: Int, y: Int): Boolean {
        if (view == null) {
            return false
        }
        val location = IntArray(2)
        view.getLocationOnScreen(location)
        val left = location[0]
        val top = location[1]
        val right = left + view.measuredWidth
        val bottom = top + view.measuredHeight
        return y >= top && y <= bottom && x >= left && x <= right
    }

    fun setSessionId(sessionId: String?) {
        this.sessionId = sessionId
    }

    private fun uploadSelectGiftNumPanelExtend() {
        val params = HashMap<String?, String?>()
        params["user_id"] = AccountUtil.getInstance().userId
        params["female_guest_id"] = sessionId
        params["event_time"] = System.currentTimeMillis().toString()
        MultiTracker.onEvent(TrackConstants.B_SELECT_GIFT_NUM_PANEL, params)
    }

    interface OnSendGiftClickListener {
        /**
         * @param source 来源
         */
        fun sendGift(giftId: Int, icon: String?, num: Int, audio: String?, name: String, json: String?, toPos: Int,source: String?)
        fun jumpRecharge()
        fun jumpRecharge(position: String?)
    }

    fun setGiftPresenterBtnVisibly(isVisible:Boolean) {
        if (isVisible) {
            giftPresenterBtn?.visibility = View.VISIBLE
        }else {
            giftPresenterBtn?.visibility = View.GONE
        }
    }

    /**
     * 盲盒礼物图标点击埋点
     * @param blind_box_name 盲盒名字
     */
    private fun trackSelectBlindBoxGift(blind_box_name:String){
        val params = HashMap<String?, String?>()
        params["user_id"] = AccountUtil.getInstance().userId
        params["female_guest_id"] = sessionId
        params["event_time"] = System.currentTimeMillis().toString()
        trackBlindBoxLevel = when (blind_box_name) {
            "初级盲盒" -> {
                "primary"
            }
            "中级盲盒" -> {
                "middle"
            }
            else -> {
                "high"
            }
        }
        params["blind_box_level"] = trackBlindBoxLevel
        params["channel"] = source //点击渠道（盲盒介绍弹窗、其他）
        MultiTracker.onEvent(TrackConstants.B_PRIMARY_BLIND_BOX_ICON_CLICK, params)
    }
}