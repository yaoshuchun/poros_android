package com.liquid.poros.ui.activity.audiomatch

import android.content.Intent
import android.os.Handler
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.liquid.base.event.EventCenter
import com.liquid.base.mvp.MvpActivity
import com.liquid.base.retrofit.HttpCallback
import com.liquid.library.retrofitx.RetrofitX
import com.liquid.poros.PorosApplication
import com.liquid.poros.R
import com.liquid.poros.analytics.utils.MultiTracker
import com.liquid.poros.constant.Constants
import com.liquid.poros.constant.TrackConstants
import com.liquid.poros.entity.MatchingRoomModel
import com.liquid.poros.entity.PlayloadInfo
import com.liquid.poros.http.ApiUrl
import com.liquid.poros.http.observer.ObjectObserver
import com.liquid.poros.http.utils.postPoros
import com.liquid.poros.ui.activity.live.RoomLiveActivityV2
import com.liquid.poros.ui.activity.user.MatchVoiceCallActivity
import com.liquid.poros.ui.dialog.RechargeCoinDialog
import com.liquid.poros.ui.fragment.dialog.common.CommonActionListener
import com.liquid.poros.ui.fragment.dialog.common.CommonDialogBuilder
import com.liquid.poros.ui.fragment.dialog.common.CommonDialogFragment
import com.liquid.poros.utils.AccountUtil
import com.liquid.poros.utils.FloatWindowViewManager
import com.liquid.poros.utils.StringUtil
import com.liquid.poros.utils.network.RetrofitHttpManager
import com.liquid.poros.widgets.AudioMatchWindowView
import com.liquid.poros.widgets.BaseWindowView
import de.greenrobot.event.EventBus
import de.greenrobot.event.Subscribe
import de.greenrobot.event.ThreadMode
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.RequestBody
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.lang.ref.WeakReference
import java.util.*

/**
 * 速配全局类，用于记录当前是否进行了速配，等功能
 */
class CPMatchingGlobal private constructor() {
    private val matchingBall: AudioMatchWindowView
    @JvmField
    var isMatching //判断当前是在进行匹配
            = false
    private var weakReference: WeakReference<AppCompatActivity>? = null
    private val dialogHandler = Handler()
    private var zoneId = 0
    private var rankId = 0
    private var zoneStr: String? = null
    private var rankStr: String? = null
    lateinit var currentCPType: String
        private set
    var matchingTimeMax = MATCHING_TIME //匹配时间3分钟
    private val rechargeCoinDialog: RechargeCoinDialog
    private val failedMsg = Runnable { alertMatchFailedDialog() }
    fun saveCurrentCPParams(currentCPType: String, zone: Int, rank: Int, zoneStr: String?, rankStr: String?) {
        this.currentCPType = currentCPType
        zoneId = zone
        rankId = rank
        this.zoneStr = zoneStr
        this.rankStr = rankStr
    }

    /**
     * 设置速配最大时间
     *
     * @param millions
     */
    fun setMatchingMaxTime(millions: Long) {
        if (millions > 0) {
            matchingTimeMax = millions
        }
    }

    /**
     * 开始倒计时弹出速配失败对话框
     */
    @JvmOverloads
    fun startCountDown(millions: Long = matchingTimeMax) {
        isMatching = true
        dialogHandler.postDelayed(failedMsg, MATCHING_TIME)
    }

    @Subscribe(threadMode = ThreadMode.MainThread)
    fun onEventBus(eventCenter: EventCenter<*>?) {
        if (null != eventCenter && eventCenter.eventCode == Constants.EVENT_MATCHING_SUCCESS) {
            isMatching = false
            dialogHandler.removeCallbacks(failedMsg)
            val playloadInfo = eventCenter.data as PlayloadInfo
            hideMatchingBall()
            goToMatchResult(playloadInfo.room_id)
            if (Constants.getUseTestApi()) {
//            if (BuildConfig.user_host_test_api) {
                Toast.makeText(PorosApplication.context, "匹配成功", Toast.LENGTH_SHORT).show()
            }
        }
    }

    /**
     * 跳转到速配等待页面
     */
    private fun goToMatchingPage() {
        val activity = weakReference!!.get() ?: return
        val intent = Intent(activity, CPMatchingActivity::class.java)
        intent.putExtra(CPMatchingActivity.CP_TYPE, currentCPType)
        intent.putExtra(CPMatchingActivity.GAME_AREA, zoneStr)
        intent.putExtra(CPMatchingActivity.GAME_LEVEL, rankStr)
        intent.putExtra(Constants.AUDIO_MATCH_INFO_ZONE, zoneId)
        intent.putExtra(Constants.AUDIO_MATCH_INFO_RANK, rankId)
        intent.putExtra(CPMatchingActivity.REQUEST_MATCHING, false)
        activity.startActivity(intent)
    }

    private fun goToMatchResult(roomId: String) {
        val mvpActivity = weakReference!!.get() ?: return
        val intent = Intent(mvpActivity, MatchVoiceCallActivity::class.java)
        intent.putExtra(Constants.ROOM_ID, roomId)
        mvpActivity.startActivity(intent)
        if ( mvpActivity is CPMatchingActivity || mvpActivity is RoomLiveActivityV2){
            mvpActivity.finish()
        }
    }

    /**
     * 弹出匹配失败对话框
     */
    private fun alertMatchFailedDialog() {
        isMatching = false
        val activity = weakReference!!.get()
        if (activity != null) CommonDialogBuilder()
                .setImageTitleRes(R.mipmap.icon_matching_failed)
                .setDesc("当前没有找到合适的小姐姐\n重新发起更容易找到哟")
                .setCancel("取消")
                .setConfirm("重新发起")
                .setCancelAble(false)
                .setListener(object : CommonActionListener {
                    override fun onConfirm() {
                        reMatching(false, "")
                    }

                    override fun onCancel() {
                        if (activity is CPMatchingActivity) {
                            activity.finish()
                        } else {
                            hideMatchingBall()
                        }
                    }
                }).create().show(activity.supportFragmentManager, CommonDialogFragment::class.java.simpleName)
    }

    /**
     * 弹出充值对话框
     */
    private fun gotoRechargePage() {
        val mvpActivity = weakReference!!.get() ?: return
        if (rechargeCoinDialog.isAdded) {
            return
        }
        rechargeCoinDialog.show(mvpActivity.supportFragmentManager, RechargeCoinDialog::class.java.name)
        rechargeCoinDialog.setWindowFrom("")
        val hashMap = HashMap<String?, String?>()
        hashMap["match_type"] = global!!.currentCPType
        hashMap["game_info"] = global!!.gameInfo
        hashMap["event_time"] = System.currentTimeMillis().toString()
        MultiTracker.onEvent(TrackConstants.B_RECHARGE_ALERT_EXPOSURE, hashMap)
    }

    /**
     * 重新匹配，如果当前页面是速配页面则重新请求接口，如果不是则跳转到速配页面
     */
    fun reMatching(isChange: Boolean, sessionId: String?) {
        val mvpActivity = weakReference!!.get() ?: return
        if (mvpActivity is CPMatchingActivity) {
            requestMatching(isChange, sessionId)
        } else {
            val intent = Intent(mvpActivity, CPMatchingActivity::class.java)
            intent.putExtra(CPMatchingActivity.CP_TYPE, currentCPType)
            intent.putExtra(CPMatchingActivity.GAME_AREA, zoneStr)
            intent.putExtra(CPMatchingActivity.GAME_LEVEL, rankStr)
            intent.putExtra(Constants.AUDIO_MATCH_INFO_ZONE, zoneId)
            intent.putExtra(Constants.AUDIO_MATCH_INFO_RANK, rankId)
            intent.putExtra(CPMatchingActivity.REQUEST_MATCHING, true)
            intent.putExtra(CPMatchingActivity.REQUEST_IS_CHANGE, isChange)
            mvpActivity.startActivity(intent)
        }
    }

    fun requestMatching(isChange: Boolean, sessionId: String?) {
        requestMatching(false, isChange, sessionId)
    }

    fun requestMatching(force: Boolean, isChange: Boolean, sessionId: String?) {
        isMatching = true

        val params = mutableMapOf<String, Any>()
        params["match_type"] = currentCPType;
        params["force"] = force;
        if (!CPMatchingActivity.CP_AUDIO.equals(currentCPType)) {
            params["zone"] = zoneId;
            params["rank"] = rankId;
        }
        RetrofitX.url(ApiUrl.CP_MATCHING)
                .paramMap(params.toMap())
                .postPoros().observeOn(AndroidSchedulers.mainThread()).subscribe(object : ObjectObserver<MatchingRoomModel>() {
                    override fun failed(result: MatchingRoomModel) {
                        if (result.code != -1 && result.show_recharge_hint) {
                            showMatchingResultMsg(result.text, result.request_success);
                        } else {
                            setMatchingMaxTime(result.seconds * 1000);
                            alertMatchFailedDialog();
                        }
                        uploadBehavior(false, isChange, sessionId);
                    }

                    override fun success(result: MatchingRoomModel) {
                        //跳转到匹配结果页面
                        if (result.show_recharge_hint) {
                            showMatchingResultMsg(result.text, result.request_success);
                        } else {
                            setMatchingMaxTime(result.seconds * 1000)
                            startCountDown();
                        }
                        uploadBehavior(true, isChange, sessionId);
                    }
                })
    }

    /**
     * 显示提交速配请求结果对话框
     *
     * @param msg
     * @param success
     */
    private fun showMatchingResultMsg(msg: String, success: Boolean) {
        val activity = weakReference!!.get()
        if (activity != null) CommonDialogBuilder()
                .setImageTitleRes(if (success) R.mipmap.icon_team_lack_time else R.mipmap.icon_not_enough_money)
                .setDesc(msg)
                .setCancel(if (success) "继续匹配" else "取消")
                .setConfirm("立即充值")
                .setCancelAble(false)
                .setListener(object : CommonActionListener {
                    override fun onConfirm() {
                        gotoRechargePage()
                    }

                    override fun onCancel() {
                        //如果true 则是继续匹配
                        if (success) {
                            requestMatching(true, false, "")
                            //如果false 则是取消匹配
                        } else {
                            if (activity is CPMatchingActivity) {
                                activity.finish()
                            } else {
                                hideMatchingBall()
                            }
                        }
                    }
                }).create().show(activity.supportFragmentManager, CommonDialogFragment::class.java.simpleName)
    }

    /**
     * 取消速配请求
     */
    fun cancelMatching() {
        isMatching = false
        dialogHandler.removeCallbacks(failedMsg)
        val tmp = JSONObject()
        try {
            tmp.put("token", AccountUtil.getInstance().account_token)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        val body: RequestBody = RequestBody.create("application/json;charset=utf-8".toMediaType(), tmp.toString())
        RetrofitHttpManager.getInstance().httpInterface.cancelMatchRoom(body).enqueue(object : HttpCallback() {
            override fun OnSucceed(result: String) {}
            override fun OnFailed(ret: Int, result: String) {}
        })
    }

    /**
     * 显示匹配悬浮球
     */
    fun showMatchingBall() {
        //如果没有在匹配则不展示悬浮球
        if (!isMatching) {
            return
        }
        val mvpActivity = weakReference!!.get()
        if (mvpActivity == null || mvpActivity is CPMatchingActivity) {
            return
        }
        matchingBall?.showBallAnimation()
        FloatWindowViewManager.getInstance().saveSingleView(matchingBall)
        FloatWindowViewManager.getInstance().showSingleView(mvpActivity, AudioMatchWindowView::class.java, 0, 0,false)
    }

    /**
     * 隐藏匹配悬浮球
     */
    fun hideMatchingBall() {
        matchingBall?.hideBallAnimation()
        FloatWindowViewManager.getInstance().removeSingleView(AudioMatchWindowView::class.java)
    }

    val gameInfo: String
        get() {
            if (StringUtil.isEquals("chat", currentCPType)) {
                return ""
            }
            val jsonObject = JSONObject()
            try {
                val game = JSONObject()
                val jsonArray = JSONArray()
                jsonArray.put(rankId)
                game.put(zoneId.toString() + "", jsonArray)
                jsonObject.put(currentCPType, game)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return jsonObject.toString()
        }

    private fun uploadBehavior(success: Boolean, isChange: Boolean, sessionId: String?) {
        if (isChange) {
            val hashMap = HashMap<String?, String?>()
            hashMap["female_guest_id"] = sessionId
            hashMap["match_type"] = currentCPType
            hashMap["game_info"] = gameInfo
            hashMap["act_type"] = if (success) "success" else "fail"
            hashMap["event_time"] = System.currentTimeMillis().toString()
            MultiTracker.onEvent(TrackConstants.B_TEAM_JOIN_NEXT_BUTTON_CLICK, hashMap)
        }
    }

    companion object {
        @JvmStatic
        var global: CPMatchingGlobal? = null
            private set
        const val MATCHING_TIME = (1000 * 60 * 3 //匹配时间3分钟
                ).toLong()

        @JvmStatic
        fun getInstance(activity: AppCompatActivity): CPMatchingGlobal{
            if (global == null) {
                global = CPMatchingGlobal()
            }
            global!!.weakReference = WeakReference(activity)
            return global!!
        }
    }

    init {
        rechargeCoinDialog = RechargeCoinDialog()
        matchingBall = AudioMatchWindowView(PorosApplication.context!!)
        matchingBall.setOnViewClickListener { liveCircleWindowView: BaseWindowView? -> goToMatchingPage() }
        EventBus.getDefault().register(this)
    }
}