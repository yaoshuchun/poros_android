package com.liquid.poros.ui.activity.user;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.liquid.base.adapter.CommonAdapter;
import com.liquid.base.adapter.CommonViewHolder;
import com.liquid.base.event.EventCenter;
import com.liquid.poros.R;
import com.liquid.poros.base.BaseActivity;
import com.liquid.poros.analytics.utils.MultiTracker;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.contract.user.TestSubmitUserInfoContract;
import com.liquid.poros.contract.user.TestSubmitUserInfoPresenter;
import com.liquid.poros.entity.EditGameBean;
import com.liquid.poros.entity.NormalGame;
import com.liquid.poros.entity.PorosUserInfo;
import com.liquid.poros.entity.SettingEditGameBean;
import com.liquid.poros.entity.TestSubmitUserInfo;
import com.liquid.poros.entity.UserCenterInfo;
import com.liquid.poros.entity.ZodiacInfo;
import com.liquid.poros.ui.MainActivity;
import com.liquid.poros.ui.fragment.dialog.user.EditGameDialogFragment;
import com.liquid.poros.ui.fragment.dialog.user.EditOtherGameDialogFragment;
import com.liquid.poros.utils.KeyboardHelper;
import com.liquid.poros.utils.ScreenUtil;
import com.liquid.poros.utils.TimeUtils;
import com.liquid.poros.utils.ViewUtils;
import com.liquid.poros.widgets.CommonInputView;
import com.liquid.poros.widgets.HorizontalItemDecoration;
import com.liquid.poros.widgets.WrapContentGridLayoutManager;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.OnClick;

/**
 * 实验组信息录入
 */
public class TestSubmitUserInfoActivity extends BaseActivity implements TestSubmitUserInfoContract.View, CommonInputView.OnInputListener {
    @BindView(R.id.ll_user_age)
    LinearLayout ll_user_age;
    @BindView(R.id.ll_test_sex)
    LinearLayout ll_test_sex;
    @BindView(R.id.iv_test_male)
    ImageView iv_test_male;
    @BindView(R.id.iv_test_female)
    ImageView iv_test_female;
    @BindView(R.id.et_test_year)
    CommonInputView et_test_year;
    @BindView(R.id.rv_zodiac)
    RecyclerView rv_zodiac;
    @BindView(R.id.ll_game)
    LinearLayout ll_game;
    @BindView(R.id.ll_game_wangzhe_content)
    LinearLayout ll_game_wangzhe_content;
    @BindView(R.id.tv_game_wangzhe_platform)
    TextView tv_game_wangzhe_platform;
    @BindView(R.id.tv_game_wangzhe_area)
    TextView tv_game_wangzhe_area;
    @BindView(R.id.tv_game_wangzhe_rank)
    TextView tv_game_wangzhe_rank;
    @BindView(R.id.tv_game_wangzhe_info)
    TextView tv_game_wangzhe_info;
    @BindView(R.id.ll_game_heping_content)
    LinearLayout ll_game_heping_content;
    @BindView(R.id.tv_game_heping_platform)
    TextView tv_game_heping_platform;
    @BindView(R.id.tv_game_heping_area)
    TextView tv_game_heping_area;
    @BindView(R.id.tv_game_heping_rank)
    TextView tv_game_heping_rank;
    @BindView(R.id.tv_game_heping_info)
    TextView tv_game_heping_info;
    @BindView(R.id.ll_game_other_content)
    LinearLayout ll_game_other_content;
    @BindView(R.id.rv_other_game)
    RecyclerView rv_other_game;
    @BindView(R.id.tv_game_other_info)
    TextView tv_game_other_info;
    @BindView(R.id.tv_save)
    TextView tv_save;
    @BindView(R.id.iv_heping)
    ImageView iv_heping;
    @BindView(R.id.iv_wangzhe)
    ImageView iv_wangzhe;
    @BindView(R.id.iv_other)
    ImageView iv_other;
    private int mGender = 0;
    private static final int TYPE_AGE = 1;
    private static final int TYPE_GAME = 2;
    private String mSelectItem = null;
    private int mZodiac = -1;
    private TestSubmitUserInfoPresenter mPresenter;
    private SettingEditGameBean mStaticGameBean;
    private UserCenterInfo.Data.UserInfo.GameInfoBean gameInfoBean;
    private CommonAdapter<NormalGame> mOtherGameAdapter;
    private String honor_platform;
    private String honor_zoom;
    private String honor_rank;
    private String peace_platform;
    private String peace_zoom;
    private String peace_rank;
    private String other_games;
    private List<ZodiacInfo> zodiacInfoList = new ArrayList<>();
    private CommonAdapter<ZodiacInfo> mCommonAdapter;
    private ArrayList<Integer> mIsSelectList = new ArrayList<>();

    @Override
    protected void parseBundleExtras(Bundle extras) {

    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.activity_test_submit_user_info;
    }

    @Override
    protected String getPageId() {
        return TrackConstants.PAGE_TEST_USER_INFO;
    }

    @Override
    protected void initViewsAndEvents(Bundle savedInstanceState) {
        rv_zodiac.setLayoutManager(new WrapContentGridLayoutManager(this, 6));
        mCommonAdapter = new CommonAdapter<ZodiacInfo>(this, R.layout.item_user_zodiac) {
            @Override
            public void convert(CommonViewHolder holder, ZodiacInfo zodiacInfo, int pos) {
                ImageView iv_zodiac = holder.getView(R.id.iv_zodiac);
                iv_zodiac.setBackgroundResource(zodiacInfo.getZodiacPic());
                holder.setText(R.id.tv_zodiac_name, zodiacInfo.getZodiacName());
                if (mIsSelectList.contains(pos)) {
                    holder.setVisibility(R.id.iv_zodiac_check, View.VISIBLE);
                    holder.getView(R.id.ll_zodiac).setBackgroundResource(R.drawable.bg_gradual_14d4d6_5);
                } else {
                    holder.getView(R.id.ll_zodiac).setBackgroundResource(R.drawable.bg_color_f2f4f7_5);
                    holder.setVisibility(R.id.iv_zodiac_check, View.GONE);
                }
                holder.getView(R.id.ll_zodiac).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!mIsSelectList.contains(pos)) {
                            mIsSelectList.clear();
                            mIsSelectList.add(pos);
                        }
                        mZodiac = zodiacInfoList.get(mIsSelectList.get(0)).getType();
                        notifyDataSetChanged();
                    }
                });
            }
        };
        rv_zodiac.setAdapter(mCommonAdapter);
        mPresenter = new TestSubmitUserInfoPresenter();
        mPresenter.attachView(this);
        mPresenter.getUserGame();
        switchView(TYPE_AGE);
        initGameData();
        et_test_year.setOnInputListener(this);
    }

    @OnClick({R.id.ll_submit, R.id.tv_save, R.id.iv_test_male, R.id.iv_test_female, R.id.rl_game_heping, R.id.rl_game_wangzhe, R.id.rl_game_other,})
    public void onClick(View view) {
        if (ViewUtils.isFastClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.ll_submit:
                KeyboardHelper.hintKeyBoard(this);
                break;
            case R.id.iv_test_male:
                switchChooseSex(true);
                break;
            case R.id.iv_test_female:
                switchChooseSex(false);
                break;
            case R.id.rl_game_heping:
                if (mStaticGameBean == null) {
                    return;
                }
                ArrayList<EditGameBean> peace_elite = new ArrayList<>();
                peace_elite.add(mStaticGameBean.peace_elite.platform);
                peace_elite.add(mStaticGameBean.peace_elite.zone);
                peace_elite.add(mStaticGameBean.peace_elite.rank);
                EditGameDialogFragment.newInstance()
                        .setTitle(getString(R.string.test_submit_info_game_hepingjingying))
                        .setEditGameBeans(peace_elite)
                        .setPeaceBean(gameInfoBean.getHpjy())
                        .show(getSupportFragmentManager(), EditGameDialogFragment.class.getSimpleName());
                break;
            case R.id.rl_game_wangzhe:
                if (mStaticGameBean == null) {
                    return;
                }
                ArrayList<EditGameBean> honor_of_kings = new ArrayList<>();
                honor_of_kings.add(mStaticGameBean.honor_of_kings.platform);
                honor_of_kings.add(mStaticGameBean.honor_of_kings.zone);
                honor_of_kings.add(mStaticGameBean.honor_of_kings.rank);
                EditGameDialogFragment.newInstance()
                        .setTitle(getString(R.string.test_submit_info_game_wangzhe))
                        .setEditGameBeans(honor_of_kings)
                        .setHonorBean(gameInfoBean.getWzry())
                        .show(getSupportFragmentManager(), EditGameDialogFragment.class.getSimpleName());
                break;
            case R.id.rl_game_other:
                if (mStaticGameBean == null) {
                    return;
                }
                EditOtherGameDialogFragment.newInstance()
                        .setTitle(getString(R.string.test_submit_info_game_other))
                        .setEditGameBeans(mStaticGameBean.other.list)
                        .show(getSupportFragmentManager(), EditOtherGameDialogFragment.class.getSimpleName());
                break;
            case R.id.tv_save:
                if (tv_save.getText().equals(getString(R.string.submit_user_info_next_tip))) {
                    if (mSelectItem == null || mGender == 0 || mZodiac == -1 || gameInfoBean == null) {
                        Toast.makeText(TestSubmitUserInfoActivity.this, getString(R.string.submit_user_info_tip), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (ll_user_age.getVisibility() == View.VISIBLE) {
                        switchView(TYPE_GAME);
                    }
                    HashMap<String, String> map = new HashMap<>();
                    map.put("age", mSelectItem);
                    map.put("gender", String.valueOf(mGender));
                    map.put("zodiac", String.valueOf(mZodiac));
                    MultiTracker.onEvent(TrackConstants.B_USER_NEXT_STEP);
                    return;
                }
                int age = TimeUtils.getYear() - new BigDecimal(mSelectItem).intValue();
                mPresenter.updateUserInfo(mGender, age, mZodiac, gameInfoBean);
                break;
        }
    }

    private void switchView(int type) {
        tv_save.setVisibility(View.VISIBLE);
        switch (type) {
            case TYPE_AGE:
                ll_user_age.setVisibility(View.VISIBLE);
                ll_game.setVisibility(View.GONE);
                tv_save.setText(getString(R.string.submit_user_info_next_tip));
                tv_save.setEnabled(true);
                tv_save.setTextColor(getResources().getColor(R.color.tv_status_online));
                tv_save.setBackgroundResource(R.drawable.bg_common_button_save);
                break;
            case TYPE_GAME:
                ll_user_age.setVisibility(View.GONE);
                ll_game.setVisibility(View.VISIBLE);
                tv_save.setText(getString(R.string.submit_user_info_well_done));
                tv_save.setTextColor(getResources().getColor(R.color.tv_status_offline));
                tv_save.setBackgroundResource(R.drawable.bg_color_f5f7fa_22);
                tv_save.setEnabled(false);
                break;
        }

    }

    @Override
    public void onSuccess(String content) {
        int year = TimeUtils.getYear() - new BigDecimal(content).intValue();
        if (!TimeUtils.isDate(content) || year < 0) {
            et_test_year.showEmptyCode();
            mSelectItem = null;
            Toast.makeText(this, getString(R.string.test_submit_info_year_tip), Toast.LENGTH_SHORT).show();
            return;
        }
        KeyboardHelper.hintKeyBoard(this);
        mSelectItem = content;
    }

    @Override
    public void onInput(String content) {
        if (TextUtils.isEmpty(content) || content.length() != 4) {
            mSelectItem = null;
        }
    }

    /**
     * 选择性别
     *
     * @param isMale
     */
    private void switchChooseSex(boolean isMale) {
        iv_test_female.setBackgroundResource(isMale ? R.mipmap.icon_test_female_normal : R.mipmap.icon_test_female_select);
        iv_test_male.setBackgroundResource(isMale ? R.mipmap.icon_test_male_select : R.mipmap.icon_test_male_normal);
        mGender = isMale ? 1 : 2;
    }

    @Override
    protected void onEventComing(EventCenter eventCenter) {
        super.onEventComing(eventCenter);
        switch (eventCenter.getEventCode()) {
            case Constants.EVENT_CODE_UPDATE_GAME_INFO_HONOR:
                HashMap<String, Object> honorMap = (HashMap<String, Object>) eventCenter.getData();
                List<EditGameBean> honorData = (List<EditGameBean>) honorMap.get("static");
                mStaticGameBean.honor_of_kings.platform = honorData.get(0);
                mStaticGameBean.honor_of_kings.zone = honorData.get(1);
                mStaticGameBean.honor_of_kings.rank = honorData.get(2);
                UserCenterInfo.Data.UserInfo.GameInfoBean.WzryBean data = (UserCenterInfo.Data.UserInfo.GameInfoBean.WzryBean) honorMap.get("data");
                gameInfoBean.setWzry(data);
                processData();
                setGameData();
                updateSaveBtn();
                break;
            case Constants.EVENT_CODE_UPDATE_GAME_INFO_PEACE:
                HashMap<String, Object> peaceMap = (HashMap<String, Object>) eventCenter.getData();
                List<EditGameBean> peaceData = (List<EditGameBean>) peaceMap.get("static");
                mStaticGameBean.peace_elite.platform = peaceData.get(0);
                mStaticGameBean.peace_elite.zone = peaceData.get(1);
                mStaticGameBean.peace_elite.rank = peaceData.get(2);
                UserCenterInfo.Data.UserInfo.GameInfoBean.HpjyBean peaceBean = (UserCenterInfo.Data.UserInfo.GameInfoBean.HpjyBean) peaceMap.get("data");
                gameInfoBean.setHpjy(peaceBean);
                processData();
                setGameData();
                updateSaveBtn();
                break;
            case Constants.EVENT_CODE_UPDATE_GAME_INFO_OTHER:
                List<EditGameBean.Item> otherData = (List<EditGameBean.Item>) eventCenter.getData();
                mStaticGameBean.other.list = (ArrayList<EditGameBean.Item>) otherData;
                gameInfoBean.getOther().clear();
                for (EditGameBean.Item item : mStaticGameBean.other.list) {
                    if (item.select) {
                        NormalGame normalGame = new NormalGame();
                        normalGame.set_id(Integer.parseInt(item.id));
                        normalGame.setIcon(item.image_url);
                        normalGame.setName(item.name);
                        gameInfoBean.getOther().add(normalGame);
                    }
                }
                processData();
                setGameData();
                updateSaveBtn();
                break;
        }

    }

    private void initGameData() {
        gameInfoBean = new UserCenterInfo.Data.UserInfo.GameInfoBean();
        UserCenterInfo.Data.UserInfo.GameInfoBean.WzryBean wzryBean = new UserCenterInfo.Data.UserInfo.GameInfoBean.WzryBean();
        wzryBean.setZone(new ArrayList<>());
        wzryBean.setRank(new ArrayList<>());
        wzryBean.setPlatform(new ArrayList<>());
        gameInfoBean.setWzry(wzryBean);
        UserCenterInfo.Data.UserInfo.GameInfoBean.HpjyBean hpjyBean = new UserCenterInfo.Data.UserInfo.GameInfoBean.HpjyBean();
        hpjyBean.setZone(new ArrayList<>());
        hpjyBean.setRank(new ArrayList<>());
        hpjyBean.setPlatform(new ArrayList<>());
        gameInfoBean.setHpjy(hpjyBean);
        gameInfoBean.setOther(new ArrayList<NormalGame>());

        //属相
        ZodiacInfo shuZodiacInfo = new ZodiacInfo();
        shuZodiacInfo.setType(0);
        shuZodiacInfo.setZodiacName(getString(R.string.test_submit_info_game_zodiac_shu));
        shuZodiacInfo.setZodiacPic(R.mipmap.ic_laoshu);

        ZodiacInfo niuZodiacInfo = new ZodiacInfo();
        niuZodiacInfo.setType(1);
        niuZodiacInfo.setZodiacName(getString(R.string.test_submit_info_game_zodiac_niu));
        niuZodiacInfo.setZodiacPic(R.mipmap.ic_niu);

        ZodiacInfo huZodiacInfo = new ZodiacInfo();
        huZodiacInfo.setType(2);
        huZodiacInfo.setZodiacName(getString(R.string.test_submit_info_game_zodiac_hu));
        huZodiacInfo.setZodiacPic(R.mipmap.ic_hu);

        ZodiacInfo tuZodiacInfo = new ZodiacInfo();
        tuZodiacInfo.setType(3);
        tuZodiacInfo.setZodiacName(getString(R.string.test_submit_info_game_zodiac_tu));
        tuZodiacInfo.setZodiacPic(R.mipmap.ic_tu);

        ZodiacInfo longZodiacInfo = new ZodiacInfo();
        longZodiacInfo.setType(4);
        longZodiacInfo.setZodiacName(getString(R.string.test_submit_info_game_zodiac_long));
        longZodiacInfo.setZodiacPic(R.mipmap.ic_long);

        ZodiacInfo sheZodiacInfo = new ZodiacInfo();
        sheZodiacInfo.setType(5);
        sheZodiacInfo.setZodiacName(getString(R.string.test_submit_info_game_zodiac_she));
        sheZodiacInfo.setZodiacPic(R.mipmap.ic_she);

        ZodiacInfo maZodiacInfo = new ZodiacInfo();
        maZodiacInfo.setType(6);
        maZodiacInfo.setZodiacName(getString(R.string.test_submit_info_game_zodiac_ma));
        maZodiacInfo.setZodiacPic(R.mipmap.ic_ma);

        ZodiacInfo yangZodiacInfo = new ZodiacInfo();
        yangZodiacInfo.setType(7);
        yangZodiacInfo.setZodiacName(getString(R.string.test_submit_info_game_zodiac_yang));
        yangZodiacInfo.setZodiacPic(R.mipmap.ic_yang);

        ZodiacInfo houZodiacInfo = new ZodiacInfo();
        houZodiacInfo.setType(8);
        houZodiacInfo.setZodiacName(getString(R.string.test_submit_info_game_zodiac_hou));
        houZodiacInfo.setZodiacPic(R.mipmap.ic_hou);

        ZodiacInfo jiZodiacInfo = new ZodiacInfo();
        jiZodiacInfo.setType(9);
        jiZodiacInfo.setZodiacName(getString(R.string.test_submit_info_game_zodiac_ji));
        jiZodiacInfo.setZodiacPic(R.mipmap.ic_ji);

        ZodiacInfo gouZodiacInfo = new ZodiacInfo();
        gouZodiacInfo.setType(10);
        gouZodiacInfo.setZodiacName(getString(R.string.test_submit_info_game_zodiac_gou));
        gouZodiacInfo.setZodiacPic(R.mipmap.ic_gou);

        ZodiacInfo zhuZodiacInfo = new ZodiacInfo();
        zhuZodiacInfo.setType(11);
        zhuZodiacInfo.setZodiacName(getString(R.string.test_submit_info_game_zodiac_zhu));
        zhuZodiacInfo.setZodiacPic(R.mipmap.ic_zhu);

        zodiacInfoList.add(shuZodiacInfo);
        zodiacInfoList.add(niuZodiacInfo);
        zodiacInfoList.add(huZodiacInfo);
        zodiacInfoList.add(tuZodiacInfo);
        zodiacInfoList.add(longZodiacInfo);
        zodiacInfoList.add(sheZodiacInfo);
        zodiacInfoList.add(maZodiacInfo);
        zodiacInfoList.add(yangZodiacInfo);
        zodiacInfoList.add(houZodiacInfo);
        zodiacInfoList.add(jiZodiacInfo);
        zodiacInfoList.add(gouZodiacInfo);
        zodiacInfoList.add(zhuZodiacInfo);
        mCommonAdapter.update(zodiacInfoList);
    }

    private void processData() {
        peace_platform = setSelect(gameInfoBean.getHpjy().getPlatform(), mStaticGameBean.peace_elite.platform);
        peace_zoom = setSelect(gameInfoBean.getHpjy().getZone(), mStaticGameBean.peace_elite.zone);
        List<Integer> hpRank = gameInfoBean.getHpjy().getRank();
        Collections.reverse(hpRank);
        peace_rank = setSelect(hpRank, mStaticGameBean.peace_elite.rank);

        honor_platform = setSelect(gameInfoBean.getWzry().getPlatform(), mStaticGameBean.honor_of_kings.platform);
        honor_zoom = setSelect(gameInfoBean.getWzry().getZone(), mStaticGameBean.honor_of_kings.zone);
        List<Integer> wzRank = gameInfoBean.getWzry().getRank();
        Collections.reverse(wzRank);
        honor_rank = setSelect(wzRank, mStaticGameBean.honor_of_kings.rank);

        StringBuilder stringBuilder = new StringBuilder();
        List<NormalGame> other = gameInfoBean.getOther();
        if (other != null && other.size() > 0) {
            for (int i = 0; i < other.size(); i++) {
                try {
                    mStaticGameBean.other.list.get(other.get(i).get_id() - 1).select = true;
                    stringBuilder.append(other.get(i).getName());
                    if (i != other.size() - 1) {
                        stringBuilder.append(" | ");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        other_games = stringBuilder.toString();
    }

    private String setSelect(List<Integer> item, EditGameBean data) {
        StringBuilder stringBuilder = new StringBuilder();
        if (item != null && item.size() > 0) {
            for (int i = 0; i < item.size(); i++) {
                try {
                    data.items.get(item.get(i) - 1).select = true;
                    stringBuilder.append(data.items.get(item.get(i) - 1).name);
                    if (i != item.size() - 1) {
                        stringBuilder.append(" | ");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return stringBuilder.toString();
    }

    private void setGameData() {
        if (gameInfoBean.getHpjy() != null && gameInfoBean.getHpjy().getPlatform().size() > 0) {
            ll_game_heping_content.setVisibility(View.VISIBLE);
            tv_game_heping_info.setVisibility(View.GONE);

            tv_game_heping_platform.setText(peace_platform);
            tv_game_heping_area.setText(peace_zoom);
            tv_game_heping_rank.setText(peace_rank);
        } else {
            ll_game_heping_content.setVisibility(View.GONE);
            tv_game_heping_info.setVisibility(View.VISIBLE);
        }
        if (mStaticGameBean.peace_elite != null) {
            RequestOptions options = new RequestOptions()
                    .placeholder(R.mipmap.icon_hepingjingying)
                    .error(R.mipmap.icon_hepingjingying);
            Glide.with(this).load(mStaticGameBean.peace_elite.icon).apply(options).apply(RequestOptions.bitmapTransform(new RoundedCorners(12))).into(iv_heping);
        }
        if (gameInfoBean.getWzry() != null && gameInfoBean.getWzry().getPlatform().size() > 0) {
            ll_game_wangzhe_content.setVisibility(View.VISIBLE);
            tv_game_wangzhe_info.setVisibility(View.GONE);

            tv_game_wangzhe_platform.setText(honor_platform);
            tv_game_wangzhe_area.setText(honor_zoom);
            tv_game_wangzhe_rank.setText(honor_rank);
        } else {
            ll_game_wangzhe_content.setVisibility(View.GONE);
            tv_game_wangzhe_info.setVisibility(View.VISIBLE);
        }
        if (mStaticGameBean.honor_of_kings != null) {
            RequestOptions options = new RequestOptions()
                    .placeholder(R.mipmap.icon_wangzherongyao)
                    .error(R.mipmap.icon_wangzherongyao);
            Glide.with(this).load(mStaticGameBean.honor_of_kings.icon).apply(options).apply(RequestOptions.bitmapTransform(new RoundedCorners(12))).into(iv_wangzhe);
        }
        if (gameInfoBean.getOther() != null && gameInfoBean.getOther().size() > 0) {
            ll_game_other_content.setVisibility(View.VISIBLE);
            tv_game_other_info.setVisibility(View.GONE);
            mOtherGameAdapter = new CommonAdapter<NormalGame>(TestSubmitUserInfoActivity.this, R.layout.item_submit_other_game) {
                @Override
                public void convert(CommonViewHolder holder, NormalGame normalGame, int pos) {
                    ImageView iv_image = holder.getView(R.id.iv_image);
                    RequestOptions options = new RequestOptions()
                            .placeholder(R.mipmap.icon_default)
                            .error(R.mipmap.icon_default)
                            .transform(new CenterCrop(), new RoundedCorners(ScreenUtil.dip2px(6)));
                    Glide.with(TestSubmitUserInfoActivity.this)
                            .load(normalGame.getIcon())
                            .override(ScreenUtil.dip2px(40), ScreenUtil.dip2px(40))
                            .apply(options)
                            .into(iv_image);
                }
            };
            rv_other_game.setLayoutManager(new LinearLayoutManager(TestSubmitUserInfoActivity.this, RecyclerView.HORIZONTAL, false));
            mOtherGameAdapter.update(gameInfoBean.getOther());
            rv_other_game.setAdapter(mOtherGameAdapter);
            rv_other_game.addItemDecoration(new HorizontalItemDecoration(ScreenUtil.dip2px(8)));
        } else {
            ll_game_other_content.setVisibility(View.GONE);
            tv_game_other_info.setVisibility(View.VISIBLE);
        }
        if (mStaticGameBean.other != null) {
            RequestOptions options = new RequestOptions()
                    .transform(new CircleCrop())
                    .placeholder(R.mipmap.icon_wangzherongyao)
                    .error(R.mipmap.icon_wangzherongyao);
            Glide.with(this)
                    .load(mStaticGameBean.other.icon)
                    .apply(options)
                    .into(iv_other);
        }

    }

    private void updateSaveBtn() {
        if ((gameInfoBean.getHpjy() != null && gameInfoBean.getHpjy().getPlatform().size() != 0) || (gameInfoBean.getWzry() != null && gameInfoBean.getWzry().getPlatform().size() != 0) || (gameInfoBean.getOther() != null && gameInfoBean.getOther().size() != 0)) {
            tv_save.setText(getString(R.string.submit_user_info_well_done));
            tv_save.setEnabled(true);
            tv_save.setTextColor(getResources().getColor(R.color.tv_status_online));
            tv_save.setBackgroundResource(R.drawable.bg_common_button_save);
        } else {
            tv_save.setText(getString(R.string.submit_user_info_well_done));
            tv_save.setTextColor(getResources().getColor(R.color.tv_status_offline));
            tv_save.setBackgroundResource(R.drawable.bg_color_f5f7fa_22);
            tv_save.setEnabled(false);
        }
    }

    @Override
    public void uploadUserInfoSuccess(PorosUserInfo userInfo) {
        HashMap<String, String> map = new HashMap<>();
        map.put("submit_type", "user_experience_group");
        MultiTracker.onEvent(TrackConstants.B_USER_SUCCESS);
        readyGo(MainActivity.class);
        finish();
    }

    @Override
    public void uploadUserInfoFailed() {
        HashMap<String, String> map = new HashMap<>();
        map.put("submit_type", "user_experience_group");
        MultiTracker.onEvent(TrackConstants.B_USER_FAILED);
        readyGo(MainActivity.class);
        finish();
    }

    @Override
    public void fetchUserGame(TestSubmitUserInfo testSubmitUserInfo) {
        mStaticGameBean = testSubmitUserInfo.getStaticGameBean();
        setGameData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }

}
