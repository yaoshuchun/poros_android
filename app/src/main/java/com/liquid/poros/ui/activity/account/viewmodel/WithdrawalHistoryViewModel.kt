package com.liquid.poros.ui.activity.account.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.liquid.library.retrofitx.RetrofitX
import com.liquid.poros.entity.WithdrawalHistoryInfo
import com.liquid.poros.http.ApiUrl
import com.liquid.poros.http.observer.ObjectObserver
import com.liquid.poros.http.utils.postPoros

class WithdrawalHistoryViewModel : ViewModel() {
    val withdrawalHistorySuccess: MutableLiveData<WithdrawalHistoryInfo> by lazy { MutableLiveData<WithdrawalHistoryInfo>() }
    val withdrawalHistoryFail: MutableLiveData<WithdrawalHistoryInfo> by lazy { MutableLiveData<WithdrawalHistoryInfo>() }

    /**
     * 获取提现历史
     */
    fun getWithdrawalHistoryListData(page: Int) {
        RetrofitX.url(ApiUrl.WITHDRAWAL_HISTORY)
                .param("page", page)
                .param("page_size", 20)
                .postPoros()
                .subscribe(object : ObjectObserver<WithdrawalHistoryInfo>() {
                    override fun success(result: WithdrawalHistoryInfo) {
                        withdrawalHistorySuccess.postValue(result)
                    }

                    override fun failed(result: WithdrawalHistoryInfo) {
                        withdrawalHistoryFail.postValue(result)
                    }
                })
    }
}