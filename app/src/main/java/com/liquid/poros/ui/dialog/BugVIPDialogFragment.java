package com.liquid.poros.ui.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.liquid.poros.R;
import com.liquid.poros.analytics.utils.MultiTracker;
import com.liquid.poros.base.BaseDialogFragment;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.entity.RechargeWayInfo;
import com.liquid.poros.ui.fragment.dialog.account.PayWaysChooseDialogFragment;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.poros.utils.ScreenUtil;
import com.netease.nimlib.sdk.msg.model.IMMessage;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentActivity;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BugVIPDialogFragment extends BaseDialogFragment implements DialogInterface.OnDismissListener {

    public List<RechargeWayInfo> recharge_way;

    public String female_guest_id;
    public String room_id;
    public String alert_type;
    private TextView desc_tv;
    private TextView tv_title;
    private TextView confirm_tv;

    public static final String IMAGE = "image";
    public static final String VOICE = "voice";
    public static final String FIRST_RECHARGE_GIVE_VIP = "first_recharge_give_vip";

    public BugVIPDialogFragment setFemale_guest_id(String female_guest_id) {
        this.female_guest_id = female_guest_id;
        return this;
    }

    public BugVIPDialogFragment setRoom_id(String room_id) {
        this.room_id = room_id;
        return this;
    }

    public BugVIPDialogFragment setAlert_type(String alert_type) {
        this.alert_type = alert_type;
        return this;
    }

    @Override
    protected String getPageId() {
        return null;
    }

    public static BugVIPDialogFragment newInstance(String groupId) {
        BugVIPDialogFragment fragment = new BugVIPDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString(Constants.GROUP_ID, groupId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        if(getDialog() != null){
            Window win = getDialog().getWindow();
            // 一定要设置Background，如果不设置，window属性设置无效
            if(win != null){
                win.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                DisplayMetrics dm = new DisplayMetrics();
                if(getActivity() != null){
                    getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);

                    WindowManager.LayoutParams params = win.getAttributes();
                    params.gravity = Gravity.CENTER;
                    params.width = ScreenUtil.dip2px(280);
                    params.width = ScreenUtil.dip2px(300);
                    setCancelable(false);
                    // 使用ViewGroup.LayoutParams，以便Dialog 宽度充满整个屏幕
                    win.setAttributes(params);
                }
            }
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        if(getActivity() != null){
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_buy_vip, null);
            desc_tv = view.findViewById(R.id.desc_tv);
            tv_title = view.findViewById(R.id.tv_title);
            confirm_tv = view.findViewById(R.id.confirm_tv);

            BigDecimal bigDecimal = new BigDecimal(Constants.buy_vip_money);
            BigDecimal divide = bigDecimal.divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_UP);

            desc_tv.setText("购买VIP后：" + Constants.buy_vip_desc);
            tv_title.setText (divide + "元购买「VIP」");
            confirm_tv.setText (divide + "元购买VIP");
            builder.setView(view);
            ButterKnife.bind(this, view);
            setCancelable(true);

            Dialog dialog = builder.create();
            dialog.setCancelable(true);
            dialog.setCanceledOnTouchOutside(true);

            HashMap<String, String> params = new HashMap<>();
            params.put("user_id", AccountUtil.getInstance().getUserId());
            params.put("female_guest_id",female_guest_id);
            params.put("event_time", String.valueOf(System.currentTimeMillis()));
            params.put("alert_type",alert_type);
            params.put(Constants.POPUP_TYPE,FIRST_RECHARGE_GIVE_VIP);
            MultiTracker.onEvent(TrackConstants.B_VIP_ALERT_EXPOSURE, params);
            return dialog;
        }
        return super.onCreateDialog(savedInstanceState);
    }

    @OnClick({R.id.confirm_tv, R.id.cancel_tv})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.confirm_tv:
                OneCentPayDialog payDialog = new OneCentPayDialog();
                payDialog.setRechargeWays(Constants.buy_vip_money, Constants.recharge_way);
                payDialog.setTvPayConinString("VIP:" + Constants.buy_vip_desc);
                payDialog.show(((FragmentActivity)getActivity()).getSupportFragmentManager(), GiftPackagePayDialog.class.getSimpleName());
//                PayWaysChooseDialogFragment.newInstance().setRechargeWays("7", "10", "¥1", 100, Constants.recharge_way).setRechargeWaysListener(new PayWaysChooseDialogFragment.PayWaysChooseListener() {
//                    @Override
//                    public void wechatOrder(int payMoney, String orderType) {
//                        mPresenter.wechatOrder(payMoney, orderType);
//                    }
//
//                    @Override
//                    public void aliPayOrder(int payMoney, String orderType) {
//                        mPresenter.alipayOrder(payMoney, orderType);
//                    }
//                }).show(((FragmentActivity)getActivity()).getSupportFragmentManager(), PayWaysChooseDialogFragment.class.getSimpleName());

                HashMap<String, String> params = new HashMap<>();
                params.put("user_id", AccountUtil.getInstance().getUserId());
                params.put("female_guest_id",female_guest_id);
                params.put("event_time", String.valueOf(System.currentTimeMillis()));
                params.put("alert_type",alert_type);
                MultiTracker.onEvent(TrackConstants.B_VIP_ALERT_BUY_CLICK, params);

                dismissAllowingStateLoss();
                break;
            case R.id.cancel_tv:
                HashMap<String, String> cancelParams = new HashMap<>();
                cancelParams.put("user_id", AccountUtil.getInstance().getUserId());
                cancelParams.put("female_guest_id",female_guest_id);
                cancelParams.put("event_time", String.valueOf(System.currentTimeMillis()));
                cancelParams.put("alert_type",alert_type);
                MultiTracker.onEvent(TrackConstants.B_VIP_ALERT_CANCEL_CLICK, cancelParams);

                dismissAllowingStateLoss();
                break;
        }
    }
}
