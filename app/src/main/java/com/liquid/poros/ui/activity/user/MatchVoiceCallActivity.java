package com.liquid.poros.ui.activity.user;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothProfile;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Animatable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.liquid.base.event.EventCenter;
import com.liquid.base.tools.StringUtil;
import com.liquid.base.utils.LogOut;
import com.liquid.im.kit.custom.JoinRoomAttachment;
import com.liquid.im.kit.custom.MinuteChangeAttachment;
import com.liquid.im.kit.custom.RoomActionAttachment;
import com.liquid.im.kit.permission.MPermission;
import com.liquid.im.kit.permission.annotation.OnMPermissionDenied;
import com.liquid.im.kit.permission.annotation.OnMPermissionGranted;
import com.liquid.im.kit.permission.annotation.OnMPermissionNeverAskAgain;
import com.liquid.poros.R;
import com.liquid.poros.analytics.utils.MultiTracker;
import com.liquid.poros.base.BaseActivity;
import com.liquid.poros.constant.CardCategoryEnum;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.constant.CoupleVoiceAction;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.contract.user.MatchVoiceCallContract;
import com.liquid.poros.contract.user.MatchVoiceCallPresent;
import com.liquid.poros.entity.BuyCoupleCardInfo;
import com.liquid.poros.entity.MatchVoiceCallInfo;
import com.liquid.poros.entity.PlayloadInfo;
import com.liquid.poros.helper.TimeUtil;
import com.liquid.poros.ui.activity.audiomatch.CPMatchingGlobal;
import com.liquid.poros.ui.dialog.RechargeCoinDialog;
import com.liquid.poros.ui.fragment.dialog.account.BuyCoupleCardDialogFragment;
import com.liquid.poros.ui.fragment.dialog.common.CommonActionListener;
import com.liquid.poros.ui.fragment.dialog.common.CommonDialogBuilder;
import com.liquid.poros.ui.fragment.dialog.common.CommonDialogFragment;
import com.liquid.poros.ui.fragment.dialog.session.MatchCallCoinNotEnoughDialogFragment;
import com.liquid.poros.ui.fragment.dialog.session.MatchInvitePlayDialogFragment;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.poros.utils.ViewUtils;
import com.netease.lava.nertc.sdk.NERtcEx;
import com.netease.nimlib.sdk.AbortableFuture;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.Observer;
import com.netease.nimlib.sdk.RequestCallback;
import com.netease.nimlib.sdk.chatroom.ChatRoomMessageBuilder;
import com.netease.nimlib.sdk.chatroom.ChatRoomService;
import com.netease.nimlib.sdk.chatroom.ChatRoomServiceObserver;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomInfo;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;
import com.netease.nimlib.sdk.chatroom.model.EnterChatRoomData;
import com.netease.nimlib.sdk.chatroom.model.EnterChatRoomResultData;
import com.netease.nimlib.sdk.msg.MsgServiceObserve;
import com.netease.nimlib.sdk.msg.constant.SessionTypeEnum;
import com.netease.nimlib.sdk.msg.model.IMMessage;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;
import de.greenrobot.event.Subscribe;
import de.greenrobot.event.ThreadMode;

/**
 * 速配通话页面
 */
public class MatchVoiceCallActivity extends BaseActivity implements MatchVoiceCallContract.View {

    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.tv_change)
    TextView tv_change;
    @BindView(R.id.icon_head)
    ImageView icon_head;
    @BindView(R.id.icon_head_anim)
    ImageView icon_head_anim;
    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.tv_duration)
    TextView tv_duration;
    @BindView(R.id.tv_cost)
    TextView tv_cost;
    @BindView(R.id.tv_duration_des)
    TextView tv_duration_des;
    @BindView(R.id.tv_charge)
    TextView tv_charge;
    @BindView(R.id.tv_invite)
    TextView tv_invite;
    @BindView(R.id.bg_invite)
    View bg_invite;
    @BindView(R.id.iv_invite_guide)
    ImageView iv_invite_guide;
    @BindView(R.id.iv_invite)
    ImageView iv_invite;
    @BindView(R.id.tv_invite_guide)
    TextView tv_invite_guide;
    @BindView(R.id.tv_cost_unit)
    TextView tv_cost_unit;

    public static final int BASIC_PERMISSION_REQUEST_CODE = 0x100;

    private String currentTeamImId;
    private String currentTeamId;
    private MatchVoiceCallPresent mPresenter;
    private AudioManager mAudioManager;
    private MediaPlayer mMediaPlayer;
    private String sessionId;
    private String roomId;
    private String mAvatarUrl;
    private String textline;
    private int mCount = 0;
    private CountDownTimer mCountDownTimer;
    private RechargeCoinDialog fragment;
    private boolean hasConnect = false;
    private boolean isBack = false;
    private boolean isChange = false;
    private int countTime = 30;
    private int remainTime = 2;
    private boolean isInvite = false;
    private boolean buyCardDismiss = false;
    protected ChatRoomInfo roomInfo;
    private long joinTime;
    private long lastTime;

    @Override
    protected String getPageId() {
        return TrackConstants.PAGE_MATCH_VOICE_CALL;
    }

    @Override
    protected void parseBundleExtras(Bundle extras) {
        roomId = extras.getString(Constants.ROOM_ID);
    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.activity_match_voice_call;
    }

    @Override
    protected void initViewsAndEvents(Bundle savedInstanceState) {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);   //应用运行时，保持屏幕高亮，不锁屏
        mPresenter = new MatchVoiceCallPresent();
        mPresenter.init(this);
        mPresenter.attachView(this);
        registerObservers(true);
        checkPermission(BASIC_PERMISSION_REQUEST_CODE);
        mAudioManager = (AudioManager) getSystemService(Service.AUDIO_SERVICE);
        if (mAudioManager.isWiredHeadsetOn()) {
            mAudioManager.setWiredHeadsetOn(true);
        } else if (isBluetoothHeadsetConnected()) {
            mAudioManager.setBluetoothScoOn(true);
        } else {
            //mAudioManager.setSpeakerphoneOn(true);
        }

        fragment = new RechargeCoinDialog();
        fragment.setRoomId(roomId);
        fragment.setMatch_room(true);
    }

    public void checkPermission(int code) {
        List<String> lackPermissions = NERtcEx.getInstance().checkPermission(mContext);
        if (lackPermissions.isEmpty()) {
            onPermissionSuccess();
        } else {
            String[] permissions = new String[lackPermissions.size()];
            for (int i = 0; i < lackPermissions.size(); i++) {
                permissions[i] = lackPermissions.get(i);
            }
            MPermission.with(this)
                    .setRequestCode(code)
                    .permissions(permissions)
                    .request();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        MPermission.onRequestPermissionsResult(this, requestCode, permissions, grantResults);
    }

    @OnMPermissionGranted(BASIC_PERMISSION_REQUEST_CODE)
    public void onPermissionSuccess() {
        if (mPresenter != null) {
            mPresenter.getMatchedInfo(roomId);
            hasConnect = true;
        }
    }

    @OnMPermissionDenied(BASIC_PERMISSION_REQUEST_CODE)
    @OnMPermissionNeverAskAgain(BASIC_PERMISSION_REQUEST_CODE)
    public void onBasicPermissionFailed() {
        Toast.makeText(mContext, "音视频通话所需权限未全部授权，部分功能可能无法正常运行！", Toast.LENGTH_SHORT).show();
        if (mPresenter != null) {
            mPresenter.cancelMatchRoom(false);
            hasConnect = false;
        }
    }

    @Override
    protected int getThemeTag() {
        return 0;
    }

    private boolean isBluetoothHeadsetConnected() {
        BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
        if (BluetoothProfile.STATE_CONNECTED == adapter.getProfileConnectionState(BluetoothProfile.HEADSET)) {
            return true;
        }
        return false;
    }

    @Override
    protected HashMap<String, String> getParams() {
        HashMap<String, String> map = new HashMap<>();
        map.put("user_id", AccountUtil.getInstance().getUserId());
        map.put("female_guest_id", sessionId);
        map.put("event_time", String.valueOf(System.currentTimeMillis()));
        return map;
    }

    Observer<List<IMMessage>> incomingMessageObserver = new Observer<List<IMMessage>>() {
        @Override
        public void onEvent(List<IMMessage> messages) {
        }
    };

    private void registerObservers(boolean register) {
        MsgServiceObserve service = NIMClient.getService(MsgServiceObserve.class);
        service.observeReceiveMessage(incomingMessageObserver, register);
    }

    @Subscribe(threadMode = ThreadMode.MainThread)
    public void onEventBus(EventCenter eventCenter) {
        super.onEventBus(eventCenter);
        switch (eventCenter.getEventCode()) {
        }
    }

    @Override
    protected void onEventComing(EventCenter eventCenter) {
        super.onEventComing(eventCenter);
        switch (eventCenter.getEventCode()) {
            case Constants.EVENT_CODE_REPORT_SPEAKER:
                //动画
                Map<String, Integer> speakers = (Map<String, Integer>) eventCenter.getData();
                showVoiceAnim(speakers);
                break;
            case Constants.EVENT_CODE_REPORT_LOCALE_SPEAKER:
                break;
            case Constants.EVENT_CONNECT_INFO:
                break;
            case Constants.EVENT_CODE_COIN_CHANGE:
                tv_cost_unit.setVisibility(View.INVISIBLE);
                tv_cost.setVisibility(View.INVISIBLE);
                tv_duration_des.setVisibility(View.INVISIBLE);
                break;
            case Constants.EVENT_CODE_ACCEPT_PLAY_IN_MATCH_CALL:
                if (mPresenter != null) {
                    mPresenter.hasTeamCard(roomId, sessionId);
                }
                HashMap<String, String> map = new HashMap<>();
                map.put("female_guest_id", sessionId);
                map.put("match_type", CPMatchingGlobal.getGlobal().getCurrentCPType());
                map.put("game_info", CPMatchingGlobal.getGlobal().getGameInfo());
                map.put("event_time", String.valueOf(System.currentTimeMillis()));
                MultiTracker.onEvent(TrackConstants.B_ACCEPT_INVITE_CLICK, map);
                break;
            case Constants.EVENT_CODE_REFUSE_PLAY_IN_MATCH_CALL:
                if (mPresenter != null) {
                    mPresenter.matchRoomRefuse(roomId);
                }
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("female_guest_id", sessionId);
                hashMap.put("match_type", CPMatchingGlobal.getGlobal().getCurrentCPType());
                hashMap.put("game_info", CPMatchingGlobal.getGlobal().getGameInfo());
                hashMap.put("event_time", String.valueOf(System.currentTimeMillis()));
                MultiTracker.onEvent(TrackConstants.B_REFUSE_INVITE_CLICK, hashMap);
                break;
            case Constants.EVENT_CODE_MATCH_INVITE_PLAY:
                isInvite = false;
                PlayloadInfo playloadInfo = (PlayloadInfo) eventCenter.getData();
                if (playloadInfo != null && playloadInfo.details != null) {
                    MatchInvitePlayDialogFragment.newInstance()
                            .setUserInfo(mAvatarUrl, playloadInfo.details.price_ori, playloadInfo.details.price, playloadInfo.details.title)
                            .show(getSupportFragmentManager(), MatchInvitePlayDialogFragment.class.getSimpleName());

                    HashMap<String, String> playMap = new HashMap<>();
                    playMap.put("female_guest_id", sessionId);
                    playMap.put("match_type", CPMatchingGlobal.getGlobal().getCurrentCPType());
                    playMap.put("game_info", CPMatchingGlobal.getGlobal().getGameInfo());
                    playMap.put("event_time", String.valueOf(System.currentTimeMillis()));
                    MultiTracker.onEvent(TrackConstants.B_RECEIVE_INVITE_ALERT_EXPOSURE, playMap);
                }
                break;
            case Constants.EVENT_CODE_REFUSE_BY_REMOTE:
                PlayloadInfo info = (PlayloadInfo) eventCenter.getData();
                if (info != null) {
                    showMessage(info.txt);
                    setInviteTvStatus(false);
                }
                break;
            case Constants.EVENT_CODE_ACCEPT_BY_REMOTE:
                endCall();
                break;
            case Constants.EVENT_CODE_TIMEOUT_BY_REMOTE:
                PlayloadInfo loadInfo = (PlayloadInfo) eventCenter.getData();
                if (loadInfo != null) {
                    showMessage(loadInfo.txt);
                }
                setInviteTvStatus(false);
                break;
            case Constants.ACTION_MATCH_STOP:
                onMatchLeave();
                break;
            case Constants.ACTION_MATCH_JOIN_ROOM_SUCCESS:
                if (isFinishing() || isDestroyed()) return;
                if (mPresenter != null) {
                    mPresenter.setChannelId(roomId, (String) eventCenter.getData());
                }
                break;
        }
    }

    @Override
    protected boolean isShowMsgTip() {
        return false;
    }

    private void showVoiceAnim(Map<String, Integer> speakers) {
        if (speakers.get(sessionId) != null) {
            int volume = speakers.get(sessionId);

            if (!(icon_head_anim.getDrawable() instanceof Animatable)) {
                icon_head_anim.setImageResource(R.drawable.macth_voice_anim_list);
            }
            if (volume == 0) {
                ((Animatable) icon_head_anim.getDrawable()).stop();
                icon_head_anim.setVisibility(View.INVISIBLE);
            } else {
                if (icon_head_anim.getVisibility() == View.INVISIBLE) {
                    icon_head_anim.setVisibility(View.VISIBLE);
                }
                ((Animatable) icon_head_anim.getDrawable()).start();
            }
        }
    }

    private void onRoomActionReceived(IMMessage message) {
        int code = ((RoomActionAttachment) message.getAttachment()).getAction();
        CoupleVoiceAction action = CoupleVoiceAction.typeOfValue(code);
        switch (action) {
            case MATCH_ROOM_EXIT_MIC:
                showMessage(((RoomActionAttachment) message.getAttachment()).getReason_text());
                int subtype = ((RoomActionAttachment) message.getAttachment()).getSubtype();
                if (subtype != 6 && subtype != 1) {
                    isBack = true;
                    endCall();
                }
                break;
            case MINUTES_CHANGE:
                RoomActionAttachment attachment = (RoomActionAttachment) message.getAttachment();
                MinuteChangeAttachment minuteChangeAttachment = new MinuteChangeAttachment();
                minuteChangeAttachment.setCoins(attachment.getCoins());
                minuteChangeAttachment.setFree_minutes(attachment.getFree_minutes());
                minuteChangeAttachment.setMinutes(attachment.getMinutes());
                updateRemainTime(minuteChangeAttachment);
                break;
        }
    }

    private void updateRemainTime(MinuteChangeAttachment minuteChangeAttachment) {
        if (minuteChangeAttachment.getMinutes() < 2) {
            tv_cost.setText(minuteChangeAttachment.getCoins());
            tv_cost.setVisibility(View.VISIBLE);
            tv_cost_unit.setVisibility(View.VISIBLE);
            if (minuteChangeAttachment.getFree_minutes() > 0) {
                tv_duration_des.setVisibility(View.VISIBLE);
                tv_duration_des.setText(String.format("（%s分钟免费时长）", minuteChangeAttachment.getFree_minutes()));
            } else {
                tv_duration_des.setVisibility(View.INVISIBLE);
            }
            remainTime = minuteChangeAttachment.getMinutes() + 1;
            countTime = remainTime * 60;
            showCoinNotEnoughDialog();
        } else {
            tv_cost_unit.setVisibility(View.INVISIBLE);
            tv_cost.setVisibility(View.INVISIBLE);
            tv_duration_des.setVisibility(View.INVISIBLE);
        }
    }

    private boolean isMyMessage(IMMessage message) {
        return message.getSessionType() == SessionTypeEnum.P2P
                && message.getSessionId() != null
                && message.getSessionId().equals(sessionId);
    }

    /**
     * 连麦异常语音提示
     */
    private void playMicFailedTip() {
        try {
            if (mMediaPlayer == null) {
                mMediaPlayer = MediaPlayer.create(this, R.raw.mic_failed_tip);
                mMediaPlayer.setLooping(false);
                mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        if (mMediaPlayer != null) {
                            mMediaPlayer.release();
                            mMediaPlayer = null;
                        }
                    }
                });
                mMediaPlayer.start();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick({R.id.iv_back, R.id.tv_charge, R.id.tv_invite, R.id.tv_change})
    public void onClick(View view) {
        if (ViewUtils.isFastClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_back:
                isBack = true;
                new CommonDialogBuilder()
                        .setTitle(getString(R.string.end_call))
                        .setDesc(getString(R.string.confirm_call))
                        .setConfirm(getString(R.string.not_change))
                        .setCancel(getString(R.string.reject))
                        .setListener(new CommonActionListener() {
                            @Override
                            public void onConfirm() {
                                isBack = false;

                                HashMap<String, String> params = new HashMap<>();
                                params.put("event_time", String.valueOf(System.currentTimeMillis()));
                                params.put("user_id", AccountUtil.getInstance().getUserId());
                                params.put("female_guest_id", sessionId);
                                params.put("result", "talk");
                                MultiTracker.onEvent("b_stop_talk_quick_team_join_click", params);
                            }

                            @Override
                            public void onCancel() {
                                endCall();

                                HashMap<String, String> params = new HashMap<>();
                                params.put("event_time", String.valueOf(System.currentTimeMillis()));
                                params.put("user_id", AccountUtil.getInstance().getUserId());
                                params.put("female_guest_id", sessionId);
                                params.put("result", "refuse");
                                MultiTracker.onEvent("b_stop_talk_quick_team_join_click", params);
                            }
                        }).create().show(getSupportFragmentManager(), CommonDialogFragment.class.getSimpleName());

                HashMap<String, String> params = new HashMap<>();
                params.put("event_time", String.valueOf(System.currentTimeMillis()));
                params.put("user_id", AccountUtil.getInstance().getUserId());
                params.put("female_guest_id", sessionId);
                MultiTracker.onEvent("b_stop_talk_quick_team_join_exposure", params);
                break;
            case R.id.tv_charge:
                showChargeDialog();
                break;
            case R.id.tv_invite:
                isInvite = true;
                if (mPresenter != null) {
                    mPresenter.hasTeamCard(roomId, sessionId);
                }
                HashMap<String, String> map = new HashMap<>();
                map.put("female_guest_id", sessionId);
                map.put("match_type", CPMatchingGlobal.getGlobal().getCurrentCPType());
                map.put("game_info", CPMatchingGlobal.getGlobal().getGameInfo());
                map.put("event_time", String.valueOf(System.currentTimeMillis()));
                MultiTracker.onEvent(TrackConstants.B_INVITE_TEAM_JOIN_BUTTON_CLICK, map);
                break;
            case R.id.tv_change:
                isChange = true;
                new CommonDialogBuilder()
                        .setTitle(getString(R.string.change))
                        .setDesc(getString(R.string.confirm_change))
                        .setConfirm(getString(R.string.not_change))
                        .setCancel(getString(R.string.change))
                        .setListener(new CommonActionListener() {
                            @Override
                            public void onConfirm() {
                                isChange = false;
                            }

                            @Override
                            public void onCancel() {
                                endCall();
                            }
                        }).create().show(getSupportFragmentManager(), CommonDialogFragment.class.getSimpleName());
                break;
        }
    }

    private void endCall() {
        showLoading();
        mPresenter.leaveRoom();
        if (isBack || isChange) {
            mPresenter.cancelMatchRoom(false);
        } else {
            quit();
        }
    }


    private void invitePlay() {
        buyCardDismiss = false;
        BuyCoupleCardDialogFragment.newInstance()
                .setBuy_position(Constants.quick_join)
                .setCpMatch(true)
//                .setMatchId()
                .setCpCardInfo(false, false, roomId, "", sessionId, "", CardCategoryEnum.TEAM_CARD.getCode())
                .setBuyListener(new BuyCoupleCardDialogFragment.BuyCardListener() {
                    @Override
                    public void goRecharge() {
                        //跳转充值界面
                        showChargeDialog();
                    }

                    @Override
                    public void buyCardSuccess(boolean isAgree, String targetId, BuyCoupleCardInfo buyCoupleCardInfo) {
                    }

                    @Override
                    public void buyCardMsg(String msg) {
                        showMessage(msg);
                    }

                    @Override
                    public void userTeamCardSuccess() {
                        buyCardDismiss = true;
                        if (!isInvite) {
                            endCall();
                        } else {
                            setInviteTvStatus(true);
                            showMessage(getString(R.string.invite_success));
                        }
                    }

                    @Override
                    public void userMicCardSuccess() {

                    }

                    @Override
                    public void onDismiss() {
                        if (!buyCardDismiss && !isInvite) {
                            buyCardDismiss = true;
                            if (mPresenter != null) {
                                mPresenter.matchRoomRefuse(roomId);
                            }
                            showMessage(getString(R.string.reject_invite));
                        }
                    }

                    @Override
                    public void goRecharge(String position) {

                    }
                }).show(getSupportFragmentManager(), BuyCoupleCardDialogFragment.class.getName());

    }

    private void showChargeDialog() {
        if (fragment.isAdded()) {
            return;
        }
        fragment.show(getSupportFragmentManager(), RechargeCoinDialog.class.getName());
        fragment.setWindowFrom(TrackConstants.PAGE_MATCH_VOICE_CALL);

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("female_guest_id", sessionId);
        hashMap.put("match_type", CPMatchingGlobal.getGlobal().getCurrentCPType());
        hashMap.put("game_info", CPMatchingGlobal.getGlobal().getGameInfo());
        hashMap.put("event_time", String.valueOf(System.currentTimeMillis()));
        MultiTracker.onEvent(TrackConstants.B_RECHARGE_ALERT_EXPOSURE, hashMap);
    }

    /*@Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_VOLUME_UP:
                mAudioManager.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_RAISE, AudioManager.FLAG_PLAY_SOUND | AudioManager.FLAG_SHOW_UI);
                return true;
            case KeyEvent.KEYCODE_VOLUME_DOWN:
                mAudioManager.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_LOWER, AudioManager.FLAG_PLAY_SOUND | AudioManager.FLAG_SHOW_UI);
                return true;
            default:
                break;
        }
        return super.onKeyDown(keyCode, event);
    }*/

    @Override
    protected void onDestroy() {
        mPresenter.detachView();
        super.onDestroy();
        leaveChatRoom();
    }

    @Override
    public void onBackPressed() {
        iv_back.performClick();
    }

    @Override
    protected boolean needSetTransparent() {
        return true;
    }

    @Override
    public void onMatchedInfo(MatchVoiceCallInfo info) {
        updateUI(info);
    }

    @Override
    public void onQuitRoom() {
        quit();
    }

    private void quit() {
        if (hasConnect) {
            mPresenter.closeConnection();
        }

        if (!isBack && !isChange) {
            Bundle bundle = new Bundle();
            bundle.putString(Constants.SESSION_ID, sessionId);
            bundle.putString(Constants.ROOM_ID, roomId);
            mContext.startActivity(new Intent(mContext, SessionActivity.class).putExtras(bundle));
        }

        if (isChange) {
            CPMatchingGlobal.getInstance(this).reMatching(true, sessionId);
        }

        endCountTime();
        finish();
    }

    @Override
    public void onHasTeamCard(int code) {
        if (code == 0) {
            if (!isInvite) {
                endCall();
            } else {
                showMessage(getString(R.string.invite_success));
                setInviteTvStatus(true);
            }
        } else {
            invitePlay();
        }
    }

    private void setInviteTvStatus(boolean invited) {
        if (invited) {
            tv_invite.setTextColor(Color.parseColor("#10d8c8"));
            tv_invite.setText(getString(R.string.invited));
            iv_invite.setVisibility(View.GONE);
            bg_invite.setBackgroundResource(R.drawable.bg_color_10d8c8_alph_80_24);
        } else {
            tv_invite.setTextColor(Color.WHITE);
            tv_invite.setText(textline);
            iv_invite.setVisibility(View.VISIBLE);
            bg_invite.setBackgroundResource(R.drawable.bg_color_10d8c8_24);
        }
    }

    private void updateUI(MatchVoiceCallInfo info) {
        if (info != null && info.data != null) {
            if (info.data.girl_info != null) {
                mAvatarUrl = info.data.girl_info.avatar_url;
                Glide.with(mContext)
                        .load(mAvatarUrl)
                        .into(icon_head);
                tv_name.setText(info.data.girl_info.nick_name);
                sessionId = info.data.girl_info.user_id;
                mPresenter.initLink(sessionId);
            }

            textline = info.data.textline;
            setInviteTvStatus(false);
            currentTeamImId = info.data.im_room_id;
            currentTeamId = info.data.room_id;
            enterChatRoom();

            tv_invite_guide.setText(info.data.boy_team_text);

            if (info.data.boy_account != null) {
                tv_cost.setText(info.data.boy_account.coins);
                MatchVoiceCallInfo.Account boy_account = info.data.boy_account;
            }
            startCountTime();
        }
    }

    private void startCountTime() {
        if (mCountDownTimer == null) {
            mCountDownTimer = new CountDownTimer(Integer.MAX_VALUE, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    if (isFinishing() || isDestroyed()) {
                        return;
                    }
                    mCount++;
                    tv_duration.setText(TimeUtil.getTime(mCount));
                }

                @Override
                public void onFinish() {

                }
            };
            mCountDownTimer.start();
        }
    }

    private void endCountTime() {
        if (isFinishing() || isDestroyed()) {
            return;
        }

        if (mCountDownTimer != null) {
            mCountDownTimer.cancel();
            mCountDownTimer = null;
        }
    }

    private void showCoinNotEnoughDialog() {
        MatchCallCoinNotEnoughDialogFragment.newInstance()
                .setmCount(countTime)
                .setTime(remainTime)
                .setCommonActionListener(new CommonActionListener() {
                    @Override
                    public void onConfirm() {
                        showChargeDialog();
                    }

                    @Override
                    public void onCancel() {

                    }
                })
                .show(getSupportFragmentManager(), MatchCallCoinNotEnoughDialogFragment.class.getSimpleName());
    }

    /**
     * 进入聊天室
     */
    private void enterChatRoom() {
        if (isDestroyed() || isFinishing() || StringUtil.isEmpty(currentTeamImId)) {
            return;
        }
        NIMClient.getService(ChatRoomServiceObserver.class).observeReceiveMessage(mIcomingMsgObserver, false);
        NIMClient.getService(ChatRoomServiceObserver.class).observeReceiveMessage(mIcomingMsgObserver, true);
        EnterChatRoomData data = new EnterChatRoomData(currentTeamImId);
        AbortableFuture<EnterChatRoomResultData> enterRequest = NIMClient.getService(ChatRoomService.class).enterChatRoomEx(data, 10);
        enterRequest.setCallback(new RequestCallback<EnterChatRoomResultData>() {
            @Override
            public void onSuccess(EnterChatRoomResultData result) {
                if (isDestroyed() || isFinishing()) {
                    return;
                }

                roomInfo = result.getRoomInfo();
                if (roomInfo == null) {
                    isBack = true;
                    endCall();
                }
                if (System.currentTimeMillis() - joinTime > 3000) {
                    JoinRoomAttachment attachment = new JoinRoomAttachment(AccountUtil.getInstance().getUserId(), AccountUtil.getInstance().getUserInfo().getNick_name(), AccountUtil.getInstance().getUserInfo().isIs_super_admin());
                    ChatRoomMessage message = ChatRoomMessageBuilder.createChatRoomCustomMessage(currentTeamImId, attachment);
                    if (Constants.getUseTestApi()) {
//                    if (BuildConfig.user_host_test_api) {
                        message.setEnv("poros_test");
                    } else {
                        message.setEnv("poros_prod");
                    }
                    NIMClient.getService(ChatRoomService.class).sendMessage(message, false);
                    joinTime = System.currentTimeMillis();
                }
            }

            @Override
            public void onFailed(int code) {
            }

            @Override
            public void onException(Throwable exception) {
            }
        });
    }

    Observer<List<ChatRoomMessage>> mIcomingMsgObserver = new Observer<List<ChatRoomMessage>>() {
        @Override
        public void onEvent(List<ChatRoomMessage> messages) {
            if (messages == null || messages.isEmpty() || isFinishing() || isDestroyed()) {
                return;
            }
            for (ChatRoomMessage message : messages) {
                if ((message.getAttachment() instanceof RoomActionAttachment)) {
                    onRoomActionReceived(message);
                    continue;
                }
            }
        }
    };

    /**
     * 离开房间
     */
    protected void leaveChatRoom() {
        NIMClient.getService(ChatRoomServiceObserver.class).observeReceiveMessage(mIcomingMsgObserver, false);
        NIMClient.getService(ChatRoomService.class).exitChatRoom(currentTeamImId);
    }

    @Override
    public void onMatchLeave() {
        LogOut.debug("", "");
    }
}
