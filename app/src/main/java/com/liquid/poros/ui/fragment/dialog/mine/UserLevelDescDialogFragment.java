package com.liquid.poros.ui.fragment.dialog.mine;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.liquid.poros.R;
import com.liquid.poros.base.BaseDialogFragment;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.utils.ScreenUtil;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UserLevelDescDialogFragment extends BaseDialogFragment {

    @Override
    protected String getPageId() {
        return null;
    }

    public static UserLevelDescDialogFragment newInstance(String groupId) {
        UserLevelDescDialogFragment fragment = new UserLevelDescDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString(Constants.GROUP_ID, groupId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        if(getDialog() != null){
            Window win = getDialog().getWindow();
            // 一定要设置Background，如果不设置，window属性设置无效
            if(win != null){
                win.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                DisplayMetrics dm = new DisplayMetrics();
                if(getActivity() != null){
                    getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);

                    WindowManager.LayoutParams params = win.getAttributes();
                    params.gravity = Gravity.CENTER;
                    params.width = ScreenUtil.dip2px(280);
                    setCancelable(false);
                    // 使用ViewGroup.LayoutParams，以便Dialog 宽度充满整个屏幕
                    win.setAttributes(params);
                }
            }
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        if(getActivity() != null){
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_user_level, null);
            builder.setView(view);
            ButterKnife.bind(this, view);
            setCancelable(true);

            Dialog dialog = builder.create();
            dialog.setCancelable(true);
            dialog.setCanceledOnTouchOutside(true);
            return dialog;
        }
        return super.onCreateDialog(savedInstanceState);
    }

    @OnClick({R.id.confirm_tv})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.confirm_tv:
                dismissAllowingStateLoss();
                break;
        }
    }
}
