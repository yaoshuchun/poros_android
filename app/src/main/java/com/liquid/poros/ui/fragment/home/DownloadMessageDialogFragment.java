package com.liquid.poros.ui.fragment.home;


import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.blankj.utilcode.util.ToastUtils;
import com.liquid.base.tools.StringUtil;
import com.liquid.base.utils.LogOut;
import com.liquid.base.utils.ToastUtil;
import com.liquid.poros.R;
import com.liquid.poros.analytics.utils.MultiTracker;
import com.liquid.poros.base.BaseDialogFragment;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.utils.Utils;
import com.tonyodev.fetch2.AbstractFetchListener;
import com.tonyodev.fetch2.Download;
import com.tonyodev.fetch2.Error;
import com.tonyodev.fetch2.Fetch;
import com.tonyodev.fetch2.FetchConfiguration;
import com.tonyodev.fetch2.FetchListener;
import com.tonyodev.fetch2.NetworkType;
import com.tonyodev.fetch2.Priority;
import com.tonyodev.fetch2.Request;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.HashMap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import okio.Okio;

/**
 * C端进圈 更新功能
 */

public class DownloadMessageDialogFragment extends BaseDialogFragment {

    private ProgressBar progressBar;
    private TextView tv_confirm;
    private View iv_download_cancel;
    private View fl_progress;
    private Fetch fetch;
    private Request request;

    public static DownloadMessageDialogFragment newInstance(boolean forceUpdate, String updateLog, String download_url, String md5, boolean isUseBrowser) {
        DownloadMessageDialogFragment fragment = new DownloadMessageDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString(Constants.PARAMS_MSG, updateLog);
        bundle.putBoolean(Constants.PARAMS_FORCE_UPDATE, forceUpdate);
        bundle.putString(Constants.PARAMS_DOWNLOAD_URL, download_url);
        bundle.putString(Constants.PARAMS_DOWNLOAD_MD5, md5);
        bundle.putBoolean(Constants.PARAMS_DOWNLOAD_BROWSER, isUseBrowser);
        fragment.setArguments(bundle);
        return fragment;
    }

    private String updateLog;
    private String download_url;
    private String download_md5;
    private boolean forceUpdate;
    private boolean isUseBrowser;
    private OnDialogStatusListener onDialogStatusListener = null;
    private String fileName = "latest_release.apk";

    @Override
    protected String getPageId() {
        return TrackConstants.PAGE_UPDATE_NOTICE;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        updateLog = getArguments().getString(Constants.PARAMS_MSG);
        download_url = getArguments().getString(Constants.PARAMS_DOWNLOAD_URL);
        download_md5 = getArguments().getString(Constants.PARAMS_DOWNLOAD_MD5);
        forceUpdate = getArguments().getBoolean(Constants.PARAMS_FORCE_UPDATE);
        isUseBrowser = getArguments().getBoolean(Constants.PARAMS_DOWNLOAD_BROWSER, false);
        setCancelable(forceUpdate);
    }

    public void setCloseClick(OnDialogStatusListener onDialogStatusListener) {
        this.onDialogStatusListener = onDialogStatusListener;
    }


    @Override
    public void onStart() {
        super.onStart();
        Window win = getDialog().getWindow();
        // 一定要设置Background，如果不设置，window属性设置无效
        win.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);

        WindowManager.LayoutParams params = win.getAttributes();
        params.gravity = Gravity.CENTER;
        setCancelable(false);
        // 使用ViewGroup.LayoutParams，以便Dialog 宽度充满整个屏幕
        params.width = (int) (dm.widthPixels * 0.8);
        win.setAttributes(params);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_downlaod_message_dialog, null);
        TextView tv_download_content = view.findViewById(R.id.tv_download_content);
        tv_download_content.setText(updateLog);
        progressBar = view.findViewById(R.id.progress);
        progressBar.setMax(100);
        tv_confirm = view.findViewById(R.id.tv_confirm);
        iv_download_cancel = view.findViewById(R.id.iv_download_cancel);
        fl_progress = view.findViewById(R.id.fl_progress);
        iv_download_cancel.setVisibility(!forceUpdate ? View.VISIBLE : View.GONE);

        if (checkFileMd5(getDownloadFile())) {
            downloadOKInstall();
        } else {
            resetDownload();
        }

        iv_download_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (forceUpdate) {
                    getActivity().finish();
                } else {
                    if (onDialogStatusListener != null) {
                        onDialogStatusListener.onCloseClick();
                    }
                    dismissDialog();
                }
            }
        });
        tv_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkFileMd5(getDownloadFile())) {
                    Utils.install(getActivity(), getDownloadFile());
                } else {
                    gotoDownload();
                }
            }
        });
        builder.setView(view);
        return builder.create();
    }

    /**
     * 获取下载文件
     *
     * @return
     */
    private File getDownloadFile() {
        String parentPath = getActivity().getExternalCacheDir().getPath();
        File file1 = new File(parentPath, "download" + File.separator + fileName);
        return file1;
    }

    /**
     * 下载完成,等待安装
     */
    private void downloadOKInstall() {
        tv_confirm.setText("立即安装");
        tv_confirm.setVisibility(View.VISIBLE);
        fl_progress.setVisibility(View.GONE);
    }

    /**
     * 重新下载
     */
    private void resetDownload() {
        tv_confirm.setText("立即更新");
        tv_confirm.setVisibility(View.VISIBLE);
        progressBar.setProgress(0);
        fl_progress.setVisibility(View.GONE);
    }

    /**
     * 开始下载
     */
    private void startDownload() {
        progressBar.setProgress(0);
        tv_confirm.setVisibility(View.GONE);
        fl_progress.setVisibility(View.VISIBLE);
    }

    private void gotoDownload() {
        if (isUseBrowser) {
            openBrowser(getContext(), download_url);
            return;
        }
        try {
            MultiTracker.onEvent("app_update_start");

            startDownload();

            File file = getDownloadFile();
            if (file.exists()) {
                file.delete();
            }
            FetchConfiguration fetchConfiguration = new FetchConfiguration.Builder(getContext())
                    .setDownloadConcurrentLimit(3)
                    .setProgressReportingInterval(1)
                    .build();

            fetch = Fetch.Impl.getInstance(fetchConfiguration);

            request = new Request(download_url, file.getAbsolutePath());
            request.setPriority(Priority.HIGH);
            request.setNetworkType(NetworkType.ALL);
            fetch.addListener(fetchListener);
        } catch (Throwable e) {
            e.printStackTrace();
            resetDownload();
        }
        fetch.enqueue(request, null, null);
    }

    /**
     * 调用第三方浏览器打开
     *
     * @param context
     * @param url     要浏览的资源地址
     */
    public static void openBrowser(Context context, String url) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));
        // 注意此处的判断intent.resolveActivity()可以返回显示该Intent的Activity对应的组件名
        // 官方解释 : Name of the component implementing an activity that can display the intent
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            final ComponentName componentName = intent.resolveActivity(context.getPackageManager());
            // 打印Log   ComponentName到底是什么
            context.startActivity(Intent.createChooser(intent, "请选择浏览器"));
        } else {
            Toast.makeText(context.getApplicationContext(), "请下载浏览器", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * 检验下载文件MD5和服务器端的MD5是否一致
     *
     * @param filePath
     * @return
     */
    private boolean checkFileMd5(File filePath) {
        try {
            if (!filePath.exists()) {
                return false;
            }
            String md5hex = Okio.buffer(Okio.source(filePath)).readByteString().md5().hex();
            return md5hex.equals(download_md5);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (fetch != null) {
            fetch.removeListener(fetchListener);
            fetch.deleteAll();
            fetch.close();
        }
    }


    private final FetchListener fetchListener = new AbstractFetchListener() {
        final long startTime = System.currentTimeMillis();

        @Override
        public void onProgress(@NotNull Download download, long etaInMilliSeconds, long downloadedBytesPerSecond) {
            super.onProgress(download, etaInMilliSeconds, downloadedBytesPerSecond);
            if (getActivity() != null && !getActivity().isFinishing() && StringUtil.equals(download.getUrl(), download_url)) {
                progressBar.setProgress(download.getProgress());
            }
        }

        @Override
        public void onCompleted(@NotNull Download download) {
            if (getActivity() != null && StringUtil.equals(download.getUrl(), download_url)) {
                HashMap<String, String> map = new HashMap<>();
                int duration = (int) (System.currentTimeMillis() - startTime);
                map.put("duration", String.valueOf(duration));
                MultiTracker.onEvent(TrackConstants.B_DOWNLOAD_COST, map);
                MultiTracker.onEvent("app_update_end");

                LogOut.debug("下载时长:" + String.valueOf(duration));

                if (checkFileMd5(getDownloadFile())) {
                    downloadOKInstall();
                    Utils.install(getActivity(), getDownloadFile());
                } else {
                    ToastUtils.showShort("下载失败，请重新下载");
                    resetDownload();
                }
            }
        }

        @Override
        public void onError(@NotNull Download download, @NotNull Error error, @org.jetbrains.annotations.Nullable Throwable throwable) {
            MultiTracker.onEvent("app_update_download_fail");
            if (getActivity() != null && !getActivity().isFinishing() && StringUtil.equals(download.getUrl(), download_url)) {
                ToastUtil.show("下载失败，请重新下载");
                resetDownload();
            }
        }

    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    public interface OnDialogStatusListener {
        void onCloseClick();
    }

}
