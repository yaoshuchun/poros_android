package com.liquid.poros.ui.activity.live.view

import android.content.Context
import android.os.CountDownTimer
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import com.liquid.poros.R
import com.liquid.poros.helper.TimeUtil
import com.liquid.poros.ui.activity.live.RoomLiveActivityV2
import com.liquid.poros.ui.dialog.RechargeCoinDialog
import com.liquid.poros.ui.dialog.SendGiftPopupWindow
import kotlinx.android.synthetic.main.include_private_room_mic_duration.*
import kotlinx.android.synthetic.main.include_private_room_mic_duration.view.*

/**
 * 视频放私聊上麦计时View
 */
class LiveRoomPrivateMicView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    private var rechargeCoinDialog: RechargeCoinDialog? = null
    private var sendGiftDialog: SendGiftPopupWindow? = null
    private var mCountDownTimer: CountDownTimer? = null
    var mRoomId: String? = null
    private var mCount = 0

    init {
        inflate(context, R.layout.include_private_room_mic_duration, this)
        initRechargeDialog()
        tv_private_room_recharge.setOnClickListener {
            //专属房间充值
            if (rechargeCoinDialog!!.isAdded) {
                return@setOnClickListener
            }
            rechargeCoinDialog!!.show((context as RoomLiveActivityV2).supportFragmentManager, RechargeCoinDialog::class.java.name)
            rechargeCoinDialog!!.setWindowFrom("private_room_recharge")
        }
        tv_private_room_close.setOnClickListener {
            (context as RoomLiveActivityV2).leaveRoom()
        }
    }

    /**
     * 专属房更新金币消耗
     */
    fun updateRoomCoin(coin: String) {
        tv_private_room_coin!!.visibility = View.VISIBLE
        tv_private_room_coin!!.text = coin
    }

    /**
     * 专属房隐藏金币消耗
     */
    fun hideRoomCoin() {
        tv_private_room_coin!!.visibility = View.GONE
    }

    /**
     * 专属房开始计时
     */
    fun startCountTime() {
        if (mCountDownTimer == null) {
            mCountDownTimer = object : CountDownTimer(Int.MAX_VALUE.toLong(), 1000) {
                override fun onTick(millisUntilFinished: Long) {
                    mCount++
                    tv_private_room_duration!!.text = TimeUtil.getTime(mCount)
                }

                override fun onFinish() {}
            }
            mCountDownTimer!!.start()
        }
    }

    /**
     * 专属房停止计时
     */
    fun stopCountTime() {
        if (mCountDownTimer != null) {
            mCountDownTimer!!.cancel()
            mCountDownTimer = null
        }
        mCount = 0
        tv_private_room_duration?.text = "00:00:00"
    }

    /**
     * 初始化充值对话框
     */
    private fun initRechargeDialog() {
        rechargeCoinDialog = RechargeCoinDialog()
        rechargeCoinDialog!!.setRoomId(mRoomId)
        rechargeCoinDialog!!.setListener { coin ->
            sendGiftDialog?.setCoinCount(coin.toLong())
        }
    }

}