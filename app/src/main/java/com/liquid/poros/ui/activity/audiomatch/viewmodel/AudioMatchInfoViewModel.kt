package com.liquid.poros.ui.activity.audiomatch.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.liquid.library.retrofitx.RetrofitX
import com.liquid.poros.entity.AudioMatchInfo
import com.liquid.poros.entity.BaseBean
import com.liquid.poros.http.ApiUrl
import com.liquid.poros.http.observer.ObjectObserver
import com.liquid.poros.http.utils.postPoros

/**
 * 速配信息页ViewModel
 */
class AudioMatchInfoViewModel : ViewModel() {
    val matchInfoLiveData : MutableLiveData<BaseBean> by lazy{ MutableLiveData<BaseBean>() }
    val failed : MutableLiveData<BaseBean> by lazy{ MutableLiveData<BaseBean>() }

    /**
     * 获去匹配信息
     */
    fun getMatchInfo(matchType : String){
        RetrofitX.url(ApiUrl.MATCH_INFO)
                .param("match_type", matchType)
                .postPoros()
                .subscribe(object : ObjectObserver<BaseBean>() {
                    override fun success(result: BaseBean) {
                        matchInfoLiveData.postValue(result)
                    }

                    override fun failed(result: BaseBean) {
                        failed.postValue(result)
                    }

                })
    }
}