package com.liquid.poros.ui.fragment.dialog.user;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.liquid.poros.R;
import com.liquid.poros.base.BaseDialogFragment;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.utils.ScreenUtil;

/**
 * 群通知详情弹窗界面
 */
public class GroupNotificationDialogFragment extends BaseDialogFragment {
    private TextView tv_notification_title;
    private TextView tv_notification_content;
    private String mNotificationTitle;
    private String mNotificationContent;
    private ScrollView sv_notification;
    private TextView tv_notification_read;
    private boolean mIsNotification = true;

    @Override
    protected String getPageId() {
        return TrackConstants.PAGE_GROUP_NOTIFICATION;
    }

    public static GroupNotificationDialogFragment newInstance() {
        GroupNotificationDialogFragment fragment = new GroupNotificationDialogFragment();
        return fragment;
    }

    public GroupNotificationDialogFragment setGroupNotification(boolean isNotification, String notificationTitle, String notificationContent) {
        this.mIsNotification = isNotification;
        this.mNotificationTitle = notificationTitle;
        this.mNotificationContent = notificationContent;
        return this;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onStart() {
        super.onStart();
        Window win = getDialog().getWindow();
        // 一定要设置Background，如果不设置，window属性设置无效
        win.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams params = win.getAttributes();
        params.gravity = Gravity.CENTER;
        setCancelable(false);
        params.width = (int) (ScreenUtil.getDisplayWidth() * 0.75);
        win.setAttributes(params);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_group_notification_dialog, null);
        tv_notification_title = view.findViewById(R.id.tv_notification_title);
        sv_notification = view.findViewById(R.id.sv_notification);
        tv_notification_read = view.findViewById(R.id.tv_notification_read);
        if (mIsNotification) {
            sv_notification.setBackgroundResource(R.drawable.bg_color_f2f4f7_8);
            tv_notification_read.setText("知道了");
        } else {
            sv_notification.setBackgroundResource(R.drawable.bg_color_ffffff_10);
            tv_notification_read.setText("确定");
        }
        tv_notification_content = view.findViewById(R.id.tv_notification_content);
        tv_notification_title.setText(mNotificationTitle);
        tv_notification_content.setText(mNotificationContent);
        view.findViewById(R.id.tv_notification_read).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mIsNotification) {
                    if (listener != null) {
                        listener.onRead();
                    }
                }
                dismissAllowingStateLoss();
            }
        });
        builder.setView(view);
        return builder.create();
    }

    private GroupNotificationDialogFragment.ReadNotificationListener listener;

    public GroupNotificationDialogFragment setReadNotificationListener(GroupNotificationDialogFragment.ReadNotificationListener listener) {
        this.listener = listener;
        return this;
    }

    public interface ReadNotificationListener {
        void onRead();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


}
