package com.liquid.poros.ui;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.blankj.utilcode.util.AppUtils;
import com.google.android.material.tabs.TabLayout;
import com.igexin.sdk.PushManager;
import com.liquid.base.event.EventCenter;
import com.liquid.im.kit.custom.CoinNotEnoughAttachment;
import com.liquid.im.kit.custom.CpInviteMaleAttachment;
import com.liquid.im.kit.custom.DaoliuAttachment;
import com.liquid.im.kit.custom.GiveRewardAttachment;
import com.liquid.im.kit.custom.GuardCpAttachment;
import com.liquid.im.kit.custom.P2pActionAttachment;
import com.liquid.im.kit.emoji.StickerManager;
import com.liquid.poros.AvInterface;
import com.liquid.poros.R;
import com.liquid.poros.adapter.ViewPagerAdapter;
import com.liquid.poros.analytics.utils.MultiTracker;
import com.liquid.poros.base.BaseActivity;
import com.liquid.poros.base.BaseDialogFragment;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.constant.DailyRewardEnum;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.contract.home.MainContract;
import com.liquid.poros.contract.home.MainPresenter;
import com.liquid.poros.entity.KeyAdd;
import com.liquid.poros.entity.PrizeCoinInfo;
import com.liquid.poros.entity.UpdateInfo;
import com.liquid.poros.helper.JinquanResourceHelper;
import com.liquid.poros.helper.cache.NimUserInfoCache;
import com.liquid.poros.sdk.HeartBeatUtils;
import com.liquid.poros.service.AvCoreService;
import com.liquid.poros.service.JQIntentService;
import com.liquid.poros.service.JQPushService;
import com.liquid.poros.ui.activity.SoloVideoListActivity;
import com.liquid.poros.ui.activity.live.RoomLiveActivityV2;
import com.liquid.poros.ui.activity.live.VideoFastDatingActivity;
import com.liquid.poros.ui.activity.live.VideoMatchChattingActivity;
import com.liquid.poros.ui.dialog.GotKeyDialog;
import com.liquid.poros.ui.dialog.LeadVideoRoomFragment;
import com.liquid.poros.ui.floatball.FloatManager;
import com.liquid.poros.ui.fragment.home.DownloadMessageDialogFragment;
import com.liquid.poros.ui.fragment.home.FeedFragment;
import com.liquid.poros.ui.fragment.home.IndexFragment;
import com.liquid.poros.ui.fragment.home.MessageFragment;
import com.liquid.poros.ui.fragment.home.MineFragment;
import com.liquid.poros.ui.fragment.home.PrizeCardDialogFragment;
import com.liquid.poros.ui.fragment.home.PrizeCoinDialogFragment;
import com.liquid.poros.ui.fragment.home.PrizeVideoCardDialogFragment;
import com.liquid.poros.ui.fragment.home.VideoRoomFragment;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.poros.utils.AppConfigUtil;
import com.liquid.poros.utils.DeviceInfoUtils;
import com.liquid.poros.utils.IMUtil;
import com.liquid.poros.utils.MyActivityManager;
import com.liquid.poros.utils.StringUtil;
import com.liquid.poros.utils.SystemUtils;
import com.liquid.poros.utils.Utils;
import com.liquid.poros.utils.ViewUtils;
import com.liquid.poros.widgets.NoScrollViewPager;
import com.netease.lava.nertc.sdk.NERtcEx;
import com.netease.nim.uikit.business.session.custom.SendCupidAttachment;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.Observer;
import com.netease.nimlib.sdk.msg.MsgServiceObserve;
import com.netease.nimlib.sdk.msg.constant.SessionTypeEnum;
import com.netease.nimlib.sdk.msg.model.IMMessage;
import com.umeng.analytics.MobclickAgent;

import java.util.HashMap;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import butterknife.BindView;
import de.greenrobot.event.EventBus;

public class MainActivity extends BaseActivity implements MainContract.View {

    @BindView(R.id.viewpager)
    NoScrollViewPager viewpager;
    @BindView(R.id.tab_layout)
    TabLayout tabLayout;
    @BindView(R.id.rl_container)
    View rl_container;
    private boolean isAddCard;
    MessageFragment messageFragment;
    ViewPagerAdapter adapter;

    private MainPresenter mPresenter;
    /**
     * 菜单标题
     */
    private int[] TAB_TITLES;
    /**
     * 菜单图标
     */
    private int[] TAB_IMGS;

    private int[] TAB_ANIMS;
    private boolean isExit = false;
    private MediaPlayer mMediaPlayer;
    private int mShowSquareFeed;
    private long lastTime = 0;

    @Override
    protected void parseBundleExtras(Bundle extras) {

    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.activity_main;
    }

    /**
     * 设置页卡显示效果
     *
     * @param tabLayout
     * @param inflater
     * @param tabTitlees
     * @param tabImgs
     */
    private void setTabs(TabLayout tabLayout, LayoutInflater inflater, int[] tabTitlees, int[] tabImgs) {
        tabLayout.removeAllTabs();
        for (int i = 0; i < tabImgs.length; i++) {
            TabLayout.Tab tab = tabLayout.newTab();
            View view = inflater.inflate(R.layout.item_main_menu, null);
            // 使用自定义视图，目的是为了便于修改，也可使用自带的视图
            tab.setCustomView(view);

            TextView tvTitle = view.findViewById(R.id.txt_tab);
            tvTitle.setText(tabTitlees[i]);
            ImageView imgTab = view.findViewById(R.id.img_tab);
            JinquanResourceHelper.getInstance(MainActivity.this).setImageResourceByAttr(imgTab, tabImgs[i]);
            tabLayout.addTab(tab);
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.GRAY);
        }
        NimUserInfoCache.getInstance().registerObservers(true);

        DeviceInfoUtils.uploadAppListInfo();//上报app列表

        //友盟用户id上报
        MobclickAgent.onProfileSignIn(AccountUtil.getInstance().getUserId());

        mPresenter.resetOnlineStatus();
        //进入MainActivity后开启心跳
        HeartBeatUtils.start();
    }

    private void setupViewPager() {
        if (Constants.hide_peking_user) {
            viewpager.setOffscreenPageLimit(mShowSquareFeed == 1 ? 4 : 3);
        } else {
            viewpager.setOffscreenPageLimit(mShowSquareFeed == 1 ? 5 : 4);
        }
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(IndexFragment.newInstance());
        if (!Constants.hide_peking_user) {
            adapter.addFragment(VideoRoomFragment.newInstance());
        }
        if (mShowSquareFeed == 1) {
            adapter.addFragment(FeedFragment.newInstance());
        }
        messageFragment = MessageFragment.newInstance();
        adapter.addFragment(messageFragment);
        adapter.addFragment(MineFragment.newInstance());

        viewpager.setAdapter(adapter);
        viewpager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {

            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewpager.setCurrentItem(tab.getPosition(), false);
                ImageView img_tab = tab.getCustomView().findViewById(R.id.img_tab);
                JinquanResourceHelper.getInstance(MainActivity.this).setImageResourceByAttr(img_tab, TAB_ANIMS[tab.getPosition()]);
                AnimationDrawable animationDrawable = (AnimationDrawable) img_tab.getDrawable();
                getHandler().post(new Runnable() {
                    @Override
                    public void run() {
                        if (!animationDrawable.isRunning()) {
                            animationDrawable.start();
                        } else {
                            animationDrawable.stop();
                        }
                    }
                });
                if (!Constants.hide_peking_user && tab.getPosition() == 1) {
                    HashMap<String, String> msgParams = new HashMap<>();
                    msgParams.put("user_id", AccountUtil.getInstance().getUserId());
                    MultiTracker.onEvent("b_video_room_navigation_click", msgParams);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                ImageView img_tab = tab.getCustomView().findViewById(R.id.img_tab);
                JinquanResourceHelper.getInstance(MainActivity.this).setImageResourceByAttr(img_tab, TAB_IMGS[tab.getPosition()]);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    protected void onEventComing(EventCenter eventCenter) {
        super.onEventComing(eventCenter);
        switch (eventCenter.getEventCode()) {
            case Constants.EVENT_CODE_LEAVE_AVCHAT_ROOM:
                leaveRoom();
                closeConnection();
                break;
        }
    }

    Observer<List<IMMessage>> incomingMessageObserver = new Observer<List<IMMessage>>() {
        @Override
        public void onEvent(List<IMMessage> messages) {
            onIncomingMessage(messages);
        }
    };

    private boolean isMyMessage(IMMessage message) {
        return message.getSessionType() == SessionTypeEnum.P2P
                && message.getSessionId() != null
                && message.getSessionId().equals(FloatManager.getInstance().getSessionId());
    }

    public void onIncomingMessage(List<IMMessage> messages) {
        if (isDestroyed() || isFinishing()) {
            return;
        }
        for (IMMessage message : messages) {
            if (message.getAttachment() instanceof CpInviteMaleAttachment) {
                CpInviteMaleAttachment actionAttachment = (CpInviteMaleAttachment) message.getAttachment();
                showInviteDialog(actionAttachment);
            }
            if (message.getAttachment() instanceof SendCupidAttachment) {
                uploadCupidMsgData();
            }

            if (message.getAttachment() instanceof DaoliuAttachment) {
                DaoliuAttachment actionAttachment = (DaoliuAttachment) message.getAttachment();
                try {
                    actionAttachment.setFemale(true);
                    enterRoom(actionAttachment, null);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (isMyMessage(message) && !FloatManager.getInstance().isWindowDismiss()) {
                if (message.getAttachment() instanceof CoinNotEnoughAttachment) {
                    CoinNotEnoughAttachment attachment = (CoinNotEnoughAttachment) message.getAttachment();
                    if (attachment.getType() == 1) {
                        if (!FloatManager.getInstance().isWindowDismiss()) {
                            FloatManager.getInstance().micStatus(1);
                            try {
                                NERtcEx.getInstance().enableLocalAudio(false);
                            } catch (Exception e) {

                            }
                        }
                    } else {
                        if (!FloatManager.getInstance().isWindowDismiss()) {
                            FloatManager.getInstance().micStatus(0);
                        }
                    }
                }
                if (message.getAttachment() instanceof P2pActionAttachment) {
                    P2pActionAttachment actionAttachment = (P2pActionAttachment) message.getAttachment();
                    if (actionAttachment.getStatus() == 6 || actionAttachment.getStatus() == 3) {//房间关闭
                        if (!FloatManager.getInstance().isWindowDismiss()) {
                            FloatManager.getInstance().micStatus(1);
                            try {
                                NERtcEx.getInstance().enableLocalAudio(false);
                            } catch (Exception e) {

                            }
                            if (actionAttachment.isVoice_warning()) {
                                playMicFailedTip();
                                HashMap<String, String> params = new HashMap<>();
                                params.put("type", "main");
                                MultiTracker.onEvent(TrackConstants.B_MIC_FAILED_AUDIO_TIP, params);
                            }
                        }
                    } else if (actionAttachment.getStatus() == 8) {
                        currentTeamImId = actionAttachment.getTeam_id();
                        sessionId = actionAttachment.getFemale_user_id();
                        avatarUrl = actionAttachment.getFemale_avatar_url();
                        imVideoToken = actionAttachment.getIm_video_token();
                        connectMic();
                    }
                }
            }
            if (message.getAttachment() instanceof P2pActionAttachment) {
                P2pActionAttachment actionAttachment = (P2pActionAttachment) message.getAttachment();
                if (actionAttachment.getStatus() == 8) {
                    currentTeamImId = actionAttachment.getTeam_id();
                    sessionId = actionAttachment.getFemale_user_id();
                    avatarUrl = actionAttachment.getFemale_avatar_url();
                    imVideoToken = actionAttachment.getIm_video_token();
                    connectMic();
                }
            }
            if (message.getAttachment() instanceof GiveRewardAttachment) {
                GiveRewardAttachment attachment = (GiveRewardAttachment) message.getAttachment();
                String gVoiceUrl = attachment.getVoiceUrl();
                if (StringUtil.isNotEmpty(gVoiceUrl)) {
                    getHandler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Utils.playAudio(gVoiceUrl);
                        }
                    }, 3000);
                }
            }
            if (message.getAttachment() instanceof GuardCpAttachment) {
                GuardCpAttachment guardCpAttachment = (GuardCpAttachment) message.getAttachment();
                String gVoiceUrl = guardCpAttachment.getVoiceUrl();
                if (StringUtil.isNotEmpty(gVoiceUrl)) {
                    playVoiceTip(gVoiceUrl);
                }
            }
        }

    }

    //上传丘比特触发消息埋点
    private void uploadCupidMsgData() {
        HashMap<String, String> params = new HashMap();
        MultiTracker.onEvent(TrackConstants.B_USER_CUPID_COUNT, params);
    }

    private void showInviteDialog(CpInviteMaleAttachment cpInviteMaleAttachment) {
        DaoliuAttachment actionAttachment = new DaoliuAttachment("");
        actionAttachment.setAvatar_url(TextUtils.isEmpty(cpInviteMaleAttachment.getAvatar_url()) ? cpInviteMaleAttachment.getAnchor_avatar_url() : cpInviteMaleAttachment.getAvatar_url());
        actionAttachment.setNike_name(TextUtils.isEmpty(actionAttachment.getNike_name()) ? cpInviteMaleAttachment.getAnchor_nick_name() : cpInviteMaleAttachment.getNick_name());
        if (TextUtils.isEmpty(cpInviteMaleAttachment.getAvatar_url()) && TextUtils.isEmpty(cpInviteMaleAttachment.getNick_name())) {//有女嘉宾
            //没有女嘉宾
            actionAttachment.setFemale(false);
        } else {
            actionAttachment.setAvatar_url(cpInviteMaleAttachment.getAvatar_url());
            actionAttachment.setNike_name(cpInviteMaleAttachment.getNick_name());
            actionAttachment.setAnchor_avatar_url(cpInviteMaleAttachment.getAnchor_avatar_url());
            actionAttachment.setAnchor_nick_name(cpInviteMaleAttachment.getAnchor_nick_name());
            actionAttachment.setFemale(true);
        }
        actionAttachment.setAge(cpInviteMaleAttachment.getAge());
        actionAttachment.setGame(cpInviteMaleAttachment.getGame());
        actionAttachment.setRoom_id(cpInviteMaleAttachment.getRoom_id());
        actionAttachment.setIm_room_id(cpInviteMaleAttachment.getIm_room_id());
        actionAttachment.setInvite_id(cpInviteMaleAttachment.getInvite_id());
        actionAttachment.setSource(cpInviteMaleAttachment.getSource());
        actionAttachment.setCouple_desc(cpInviteMaleAttachment.getDesc());
        actionAttachment.setType(cpInviteMaleAttachment.getType());
        enterRoom(actionAttachment, cpInviteMaleAttachment);
    }

    private void enterRoom(DaoliuAttachment actionAttachment, CpInviteMaleAttachment cpInviteMaleAttachment) {
        if (System.currentTimeMillis() - lastTime < 2000) {
            lastTime = System.currentTimeMillis();
            getHandler().postDelayed(() -> showDialogFragment(actionAttachment, cpInviteMaleAttachment), 2000);
            return;
        }
        lastTime = System.currentTimeMillis();
        showDialogFragment(actionAttachment, cpInviteMaleAttachment);
    }

    private void showDialogFragment(DaoliuAttachment actionAttachment, CpInviteMaleAttachment cpInviteMaleAttachment) {
        String room_id = actionAttachment.getRoom_id();
        FragmentActivity activity = (FragmentActivity) MyActivityManager.getInstance().getCurrentActivity();
        if (activity != null) {
            if (activity instanceof RoomLiveActivityV2) {
                String mRoomid = ((RoomLiveActivityV2) activity).getMRoomId();
                if (StringUtil.isEquals(mRoomid, room_id)) {
                    if (cpInviteMaleAttachment != null) {
                        EventBus.getDefault().post(new EventCenter(Constants.EVENT_CODE_INVITE_OPEN_MIC, cpInviteMaleAttachment));
                    }

                    return;
                }
            }
            if (activity instanceof VideoFastDatingActivity) {//速配页屏蔽弹窗
                return;
            }
            if(activity instanceof SoloVideoListActivity){//速配页屏蔽弹窗
                return;
            }
            if(activity instanceof VideoMatchChattingActivity){//视频连接中屏蔽弹窗
                return;
            }
            try {
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                Fragment fragmentToRemove = getSupportFragmentManager().findFragmentByTag(BaseDialogFragment.class.getSimpleName());
                if (fragmentToRemove != null) {
                    ft.remove(fragmentToRemove);
                    ft.commit();
                }
                //如果之前有弹窗则把之前的给移除了再弹出一个
                if (StringUtil.isEquals(actionAttachment.getType(), Constants.VIDEO_ROOM_DIVERSION_TYPE)) {
                    LiveRoomDiversionMaleFragment.Companion.newInstance().setAttachment(actionAttachment).setActivity(activity).show(activity.getSupportFragmentManager(), BaseDialogFragment.class.getSimpleName());
                } else if (StringUtil.isEquals(actionAttachment.getType(), Constants.TYPE_DIVERSION) || StringUtil.isEquals(actionAttachment.getType(), Constants.TYPE_CP_GROUP_INVITE)) {
                    LeadVideoRoomFragment.newInstance("s").setAttachment(actionAttachment).setContext(activity).show(activity.getSupportFragmentManager(), BaseDialogFragment.class.getSimpleName());
                } else {
                    LeadVideoRoomFragment.newInstance("s").setAttachment(actionAttachment).setContext(activity).show(activity.getSupportFragmentManager(), BaseDialogFragment.class.getSimpleName());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 连麦异常语音提示
     */
    private void playMicFailedTip() {
        try {
            if (mMediaPlayer == null) {
                mMediaPlayer = MediaPlayer.create(this, R.raw.mic_failed_tip);
                mMediaPlayer.setLooping(false);
                mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        if (mMediaPlayer != null) {
                            mMediaPlayer.release();
                            mMediaPlayer = null;
                        }
                    }
                });
            }
            mMediaPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        NIMClient.getService(MsgServiceObserve.class).observeReceiveMessage(incomingMessageObserver, true);
    }

    @Override
    protected void initViewsAndEvents(Bundle savedInstanceState) {
        mShowSquareFeed = AppConfigUtil.getShow_square_feed();
        if (Constants.hide_peking_user) {
            if (mShowSquareFeed == 1) {
                TAB_TITLES = new int[]{R.string.menu_index, R.string.menu_feed, R.string.menu_message, R.string.menu_mine};
                TAB_IMGS = new int[]{R.attr.tab_res_index, R.attr.tab_res_feed, R.attr.tab_res_message,
                        R.attr.tab_res_mine};
                TAB_ANIMS = new int[]{R.attr.tab_res_index_active, R.attr.tab_res_feed_active, R.attr.tab_res_message_active,
                        R.attr.tab_res_mine_active};
            } else {
                TAB_TITLES = new int[]{R.string.menu_index, R.string.menu_message, R.string.menu_mine};
                TAB_IMGS = new int[]{R.attr.tab_res_index, R.attr.tab_res_message,
                        R.attr.tab_res_mine};
                TAB_ANIMS = new int[]{R.attr.tab_res_index_active, R.attr.tab_res_message_active,
                        R.attr.tab_res_mine_active};
            }
        } else {
            if (mShowSquareFeed == 1) {
                TAB_TITLES = new int[]{R.string.menu_index, R.string.menu_video, R.string.menu_feed, R.string.menu_message, R.string.menu_mine};
                TAB_IMGS = new int[]{R.attr.tab_res_index, R.attr.tab_res_video, R.attr.tab_res_feed, R.attr.tab_res_message,
                        R.attr.tab_res_mine};
                TAB_ANIMS = new int[]{R.attr.tab_res_index_active, R.attr.tab_res_video_active, R.attr.tab_res_feed_active, R.attr.tab_res_message_active,
                        R.attr.tab_res_mine_active};
            } else {
                TAB_TITLES = new int[]{R.string.menu_index, R.string.menu_video, R.string.menu_message, R.string.menu_mine};
                TAB_IMGS = new int[]{R.attr.tab_res_index, R.attr.tab_res_video, R.attr.tab_res_message,
                        R.attr.tab_res_mine};
                TAB_ANIMS = new int[]{R.attr.tab_res_index_active, R.attr.tab_res_video_active, R.attr.tab_res_message_active,
                        R.attr.tab_res_mine_active};
            }
        }
        mPresenter = new MainPresenter();
        mPresenter.attachView(this);
        mPresenter.checkUpdate(ViewUtils.getAppVersionName(this));

        IntentFilter configChangeFilter = new IntentFilter();
        configChangeFilter.addAction(Intent.ACTION_CONFIGURATION_CHANGED);
        configChangeFilter.addAction(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
        configChangeFilter.addAction(Intent.ACTION_BATTERY_CHANGED);
        configChangeFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);

        setupViewPager();
        setTabs(tabLayout, getLayoutInflater(), TAB_TITLES, TAB_IMGS);
        IMUtil.imLogin();
        StickerManager.getInstance().init(this);
        // 个推初始化
        PushManager.getInstance().initialize(getApplicationContext(), JQPushService.class);
        PushManager.getInstance().registerPushIntentService(getApplicationContext(), JQIntentService.class);
        SystemUtils.getInstance().listenerSignal(this);

        getWindow().getDecorView().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (Constants.get_gift_box == 1) {
                    //实验组 请求 钥匙接口
                    mPresenter.checkKeyStatus();
                }
            }
        }, 1000);
    }

    @Override
    protected String getPageId() {
        return TrackConstants.PAGE_HOME;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        NimUserInfoCache.getInstance().registerObservers(false);
        mPresenter.detachView();
    }


    @Override
    public void notifyByThemeChanged() {
        setTabs(tabLayout, getLayoutInflater(), TAB_TITLES, TAB_IMGS);
        if (Constants.hide_peking_user) {
            if (mShowSquareFeed == 1) {
                viewpager.setCurrentItem(3);
            } else {
                viewpager.setCurrentItem(2);
            }
        } else {
            if (mShowSquareFeed == 1) {
                viewpager.setCurrentItem(4);
            } else {
                viewpager.setCurrentItem(3);
            }
        }
        JinquanResourceHelper.getInstance(this).setBackgroundColorByAttr(rl_container, R.attr.custom_attr_app_bg);
        JinquanResourceHelper.getInstance(this).setBackgroundResourceByAttr(tabLayout, R.attr.bg_bottom_tab);
    }


    @Override
    public void onUpdateFetch(UpdateInfo updateInfo) {
        if (updateInfo != null && updateInfo.getCode() == 100 && !Constants.getUseTestApi()) {
            DownloadMessageDialogFragment dialogFragment = DownloadMessageDialogFragment.newInstance("force".equals(updateInfo.getMode()), updateInfo.getDesc(), updateInfo.getUrl(), updateInfo.getMd5(), false);
            dialogFragment.setCloseClick(new DownloadMessageDialogFragment.OnDialogStatusListener() {
                @Override
                public void onCloseClick() {
                    mPresenter.getDailyPrizeCoin();
                }
            });
            dialogFragment.show(getSupportFragmentManager(), "DownloadMessage");
        } else {
            mPresenter.getDailyPrizeCoin();
        }
    }

    @Override
    public void onUpdateFail() {
        //升级弹窗请求失败，也去请求登陆奖励弹窗
        mPresenter.getDailyPrizeCoin();
    }

    private MediaPlayer mMediaPlayerOne;

    private void playVoiceTip(String voiceUrl) {
        try {
            if (mMediaPlayerOne == null) {
                mMediaPlayerOne = MediaPlayer.create(mContext, Uri.parse(voiceUrl));
                mMediaPlayerOne.setLooping(false);
                mMediaPlayerOne.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mMediaPlayerOne.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        if (mMediaPlayerOne != null) {
                            mMediaPlayerOne.release();
                            mMediaPlayerOne = null;
                        }
                    }
                });
            }
            mMediaPlayerOne.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFetchCoin(PrizeCoinInfo baseModel) {
        if (baseModel.getData() != null && baseModel.getData().isShow_page() && baseModel.getData() != null) {
            DailyRewardEnum rewardType = DailyRewardEnum.typeOfValue(baseModel.getData().getReward_type());
            switch (rewardType) {
                case REWARD_CARD:
                    PrizeCardDialogFragment.newInstance().setPrizeCoin(baseModel.getData()).show(getSupportFragmentManager(), PrizeCardDialogFragment.class.getSimpleName());
                    break;
                case REWARD_COIN:
                    PrizeCoinDialogFragment.newInstance().setPrizeCoin(baseModel.getData()).show(getSupportFragmentManager(), PrizeCoinDialogFragment.class.getSimpleName());
                    break;
                case REWARD_VIDEO_CARD:
                    PrizeVideoCardDialogFragment.newInstance().setPrizeCoin(baseModel.getData().getPrize_items()).show(getSupportFragmentManager(), PrizeVideoCardDialogFragment.class.getSimpleName());
                    break;
            }
        }
    }

    @Override
    public void onShowKeyStatus(KeyAdd keyAdd) {
        if (keyAdd != null) {
            int now_count = keyAdd.getNow_count();
            int tomorrow = keyAdd.getTomorrow_count();
            if (now_count > 0) {
                GotKeyDialog.newInstance("got_key_dialog").
                        setFrom(GotKeyDialog.MAIN_PAGE).
                        setParams(now_count, tomorrow).
                        show(getSupportFragmentManager(), GotKeyDialog.class.getSimpleName());
            }
        }
    }

    /**
     * 设置未读消息数
     *
     * @param count
     */
    public void updateUnreadCount(int count) {
        if (tabLayout != null) {
            TabLayout.Tab tab;
            int itemPosition = adapter.getItemPosition(messageFragment);
            if (itemPosition < 0) {
                return;
            }
            tab = tabLayout.getTabAt(itemPosition);
            View view = tab.getCustomView();
            TextView textView = view.findViewById(R.id.tv_message_unread_count);
            textView.setVisibility(count > 0 ? View.VISIBLE : View.GONE);
            if (count < 100) {
                textView.setText(String.valueOf(count));
            } else {
                textView.setText("99+");
            }
        }
    }

    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            isExit = false;
        }
    };

    private void exit() {
        if (!isExit) {
            isExit = true;
            Toast.makeText(getApplicationContext(), "再按一次退出程序",
                    Toast.LENGTH_SHORT).show();
            // 利用handler延迟发送更改状态信息
            mHandler.sendEmptyMessageDelayed(0, 2000);
        } else {
            try {
                Intent intent = new Intent();
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setAction(Intent.ACTION_MAIN);
                startActivity(intent);
            } catch (Exception e) {
                finish();
                System.exit(0);
            }
        }
    }

    private AvInterface mAvManager;
    private String currentTeamImId;
    private String sessionId;
    private String avatarUrl;
    private String imVideoToken;
    private boolean isBind;

    public void connectMic() {
        if (!AppUtils.isAppForeground()) {
            isAddCard = true;
            bindService(new Intent(this, AvCoreService.class), mConnection, Context.BIND_AUTO_CREATE);
        }
    }

    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mAvManager = AvInterface.Stub.asInterface(service);
            try {
                mAvManager.stopAudioLink();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            isBind = true;
            autoJoinRoom();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }
    };

    private void autoJoinRoom() {
        //开始自动连麦
        try {
            if (mAvManager != null && !mAvManager.isAvRunning() && StringUtil.isNotEmpty(sessionId)) {
                mAvManager.startAudioLink(currentTeamImId, imVideoToken, sessionId, avatarUrl, false);
                mAvManager.setMicrophoneMute(true);
                getHandler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        FloatManager.getInstance().showMenuWindow();
                    }
                }, 1300);
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void leaveRoom() {
        if (isAddCard) {
            isAddCard = false;
            try {
                if (mAvManager != null) {
                    HashMap<String, String> params = new HashMap<>();
                    mAvManager.stopAudioLink();
                }
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    public void closeConnection() {
        if (isBind) {
            mContext.unbindService(mConnection);
            isBind = false;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            exit();
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }


}
