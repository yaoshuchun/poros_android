package com.liquid.poros.ui.fragment.dialog.live;


import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.liquid.poros.R;
import com.liquid.poros.base.BaseDialogFragment;
import com.liquid.poros.analytics.utils.MultiTracker;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.utils.StringUtil;
import com.liquid.poros.utils.ViewUtils;

import java.util.Timer;
import java.util.TimerTask;


public class ReceiveCoupleTeamDialogFragment extends BaseDialogFragment {

    private TimerTask mTask2;
    private Timer mTimer2;
    private int remainCount = 30;
    private long startTime;
    private String nickName;

    public ReceiveCoupleTeamDialogFragment setListener(onConfirmListener listener) {
        this.listener = listener;
        return this;
    }
    public static ReceiveCoupleTeamDialogFragment newInstance() {
        ReceiveCoupleTeamDialogFragment fragment = new ReceiveCoupleTeamDialogFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    public ReceiveCoupleTeamDialogFragment setNickName(String nickName) {
        this.nickName = nickName;
        return this;
    }

    @Override
    protected String getPageId() {
        return TrackConstants.P_INVITATION_CARD;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        Window win = getDialog().getWindow();
        // 一定要设置Background，如果不设置，window属性设置无效
        win.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);

        WindowManager.LayoutParams params = win.getAttributes();
        params.gravity = Gravity.CENTER;

        setCancelable(false);
        // 使用ViewGroup.LayoutParams，以便Dialog 宽度充满整个屏幕

        params.width = (int) (dm.widthPixels * 0.7);
        params.height = (int) (dm.heightPixels * 0.48);

        win.setAttributes(params);

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_receive_couple_team_dialog, null);
        startTime = System.currentTimeMillis();
        TextView tv_title = view.findViewById(R.id.tv_title);
        String desc = "是否进入与" + nickName + "的CP专属开黑房间";
        tv_title.setText(StringUtil.matcherSearchText(false, Color.parseColor("#47D1D1"), desc, nickName));
        view.findViewById(R.id.tv_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                releaseTimer();
                if (listener != null) {
                    listener.onCancel();
                }
                dismissDialog();
            }
        });
        view.findViewById(R.id.tv_confirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ViewUtils.isFastClick()) {
                    return;
                }

                if (listener != null) {
                    listener.onConfirm();
                }
                MultiTracker.onEvent(TrackConstants.B_AGREEL_INVITATION);
                dismissDialog();
            }
        });

        final TextView tv_time = view.findViewById(R.id.tv_time);
        builder.setView(view);
        if (mTimer2 == null && mTask2 == null) {
            mTimer2 = new Timer();
            mTask2 = new TimerTask() {
                @Override
                public void run() {
                    tv_time.post(new Runnable() {
                        @Override
                        public void run() {
                            long duration = System.currentTimeMillis() - startTime;
                            int time = remainCount - (int) (duration / 1000);
                            if (time <= 0) {
                                if (listener != null) {
                                    listener.onCancel();
                                }
                                releaseTimer();
                                dismissDialog();
                            } else {
                                tv_time.setText(String.format("(%d)秒后自动拒绝", time));
                            }
                        }
                    });

                }
            };
            mTimer2.schedule(mTask2, 0, 1000);
        }

        return builder.create();
    }

    private void releaseTimer() {
        if (mTimer2 != null) {
            mTimer2.cancel();
            mTask2.cancel();
            mTask2 = null;
            mTimer2 = null;
        }
    }


    private onConfirmListener listener;

    public interface onConfirmListener {
        void onConfirm();

        void onCancel();

    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        releaseTimer();
    }
}
