package com.liquid.poros.ui.floatball;

import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;
import android.view.Gravity;
import android.view.WindowManager;

import com.liquid.base.event.EventCenter;
import com.liquid.poros.R;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.ui.activity.user.SessionActivity;
import com.liquid.poros.ui.floatball.rom.HuaweiUtils;
import com.liquid.poros.ui.floatball.rom.MeizuUtils;
import com.liquid.poros.ui.floatball.rom.MiuiUtils;
import com.liquid.poros.ui.floatball.rom.OppoUtils;
import com.liquid.poros.ui.floatball.rom.QikuUtils;
import com.liquid.poros.ui.floatball.rom.RomUtils;
import com.liquid.poros.ui.fragment.dialog.common.CommonActionListener;
import com.liquid.poros.ui.fragment.dialog.common.CommonDialogBuilder;
import com.liquid.poros.ui.fragment.dialog.common.CommonDialogFragment;
import com.liquid.poros.utils.ViewUtils;
import java.lang.ref.WeakReference;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;

import androidx.appcompat.app.AppCompatActivity;
import de.greenrobot.event.EventBus;


public class FloatManager implements onExpandClickListener {
    private static final String TAG = "FloatBallManager";

    private static volatile FloatManager instance;

    private boolean isWindowDismiss = true;
    private WindowManager windowManager = null;
    private WindowManager.LayoutParams mParams = null;
    private FloatMenu floatMenu = null;

    private String sessionId;
    private String avatarUrl;
    private boolean isHost;

    public String getSessionId() {
        return sessionId;
    }

    public boolean checkPermission(Context context) {
        //6.0 版本之后由于 google 增加了对悬浮窗权限的管理，所以方式就统一了
        if (Build.VERSION.SDK_INT < 23) {
            if (RomUtils.checkIsMiuiRom()) {
                return miuiPermissionCheck(context);
            } else if (RomUtils.checkIsMeizuRom()) {
                return meizuPermissionCheck(context);
            } else if (RomUtils.checkIsHuaweiRom()) {
                return huaweiPermissionCheck(context);
            } else if (RomUtils.checkIs360Rom()) {
                return qikuPermissionCheck(context);
            } else if (RomUtils.checkIsOppoRom()) {
                return oppoROMPermissionCheck(context);
            }
        }
        return commonROMPermissionCheck(context);
    }

    private boolean huaweiPermissionCheck(Context context) {
        return HuaweiUtils.checkFloatWindowPermission(context);
    }

    private boolean miuiPermissionCheck(Context context) {
        return MiuiUtils.checkFloatWindowPermission(context);
    }

    private boolean meizuPermissionCheck(Context context) {
        return MeizuUtils.checkFloatWindowPermission(context);
    }

    private boolean qikuPermissionCheck(Context context) {
        return QikuUtils.checkFloatWindowPermission(context);
    }

    private boolean oppoROMPermissionCheck(Context context) {
        return OppoUtils.checkFloatWindowPermission(context);
    }

    public static void commonROMPermissionApplyInternal(Context context) throws NoSuchFieldException, IllegalAccessException {
        Class clazz = Settings.class;
        Field field = clazz.getDeclaredField("ACTION_MANAGE_OVERLAY_PERMISSION");

        Intent intent = new Intent(field.get(null).toString());
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setData(Uri.parse("package:" + context.getPackageName()));
        context.startActivity(intent);
    }

    private boolean commonROMPermissionCheck(Context context) {
        //最新发现魅族6.0的系统这种方式不好用，天杀的，只有您是奇葩，没办法，单独适配一下
        if (RomUtils.checkIsMeizuRom()) {
            return meizuPermissionCheck(context);
        } else {
            Boolean result = true;
            if (Build.VERSION.SDK_INT >= 23) {
                try {
                    Class clazz = Settings.class;
                    Method canDrawOverlays = clazz.getDeclaredMethod("canDrawOverlays", Context.class);
                    result = (Boolean) canDrawOverlays.invoke(null, context);
                } catch (Exception e) {
                    Log.e(TAG, Log.getStackTraceString(e));
                }
            }
            return result;
        }
    }


    private WeakReference<Context> contextWeakReference;

    public static FloatManager getInstance() {
        if (instance == null) {
            synchronized (FloatManager.class) {
                if (instance == null) {
                    instance = new FloatManager();
                }
            }
        }
        return instance;
    }

    public void setUpParams(String sessionId, String avatarUrl, boolean isHost, Context context) {
        this.contextWeakReference = new WeakReference<>(context);
        this.sessionId = sessionId;
        this.avatarUrl = avatarUrl;
        this.isHost = isHost;
    }

    public boolean isHost() {
        return isHost;
    }

    private long startTime;

    public void onAudioVolume(Map<String, Integer> speakers) {
        if (floatMenu == null) {
            return;
        }
        if (speakers != null && System.currentTimeMillis() - startTime > 1000) {
            floatMenu.updateMicVolume(speakers);
            startTime = System.currentTimeMillis();
        }
    }

    @Override
    public void onExpandClick(int from) {
        if (ViewUtils.isFastClick()) {
            return;
        }
        EventBus.getDefault().post(new EventCenter(Constants.EVENT_CODE_FINISH_SESSION_ACTIVITY));
        if (contextWeakReference.get() != null) {
            Context context = contextWeakReference.get();
            context.startActivity(new Intent(context, SessionActivity.class)
                    .putExtra(Constants.SESSION_ID, sessionId)
                    .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
        }
    }

    //获取状态栏高度
    private int getStatusHeight() {
        if (contextWeakReference.get() != null) {
            Context context = contextWeakReference.get();
            try {
                Class<?> c = Class.forName("com.android.internal.R$dimen");
                Object object = c.newInstance();
                Field field = c.getField("status_bar_height");
                int x = (Integer) field.get(object);
                return context.getResources().getDimensionPixelSize(x);
            } catch (Exception e) {
                return 0;
            }
        }
        return 0;
    }

    public boolean isWindowDismiss() {
        return isWindowDismiss;
    }

    public void showMenuWindow() {
        if (contextWeakReference.get() != null) {
            Context context = contextWeakReference.get();
            if (!isWindowDismiss || context == null) {
                return;
            }

            isWindowDismiss = false;
            if (windowManager == null) {
                windowManager = (WindowManager) context.getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
            }

            Point size = new Point();
            windowManager.getDefaultDisplay().getSize(size);
            floatMenu = new FloatMenu(context, avatarUrl, sessionId, this);
            mParams = new WindowManager.LayoutParams();
            mParams.packageName = context.getPackageName();
            mParams.width = WindowManager.LayoutParams.WRAP_CONTENT;
            mParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
            mParams.gravity = Gravity.CENTER | Gravity.LEFT;
            mParams.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL;
            int mType;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                mType = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
            } else {
                mType = WindowManager.LayoutParams.TYPE_SYSTEM_ERROR;
            }
            mParams.type = mType;
            mParams.format = PixelFormat.RGBA_8888;

            floatMenu.setParams(mParams);
            floatMenu.setIsShowing(true);
            windowManager.addView(floatMenu, mParams);
        }

    }

    public void hideMenuWindow() {
        if (isWindowDismiss) {
            Log.e(TAG, "window can not be dismiss cause it has not been added");
            return;
        }
        isWindowDismiss = true;
        floatMenu.setIsShowing(false);
        if (windowManager != null && floatMenu != null) {
            windowManager.removeViewImmediate(floatMenu);
        }
    }

    public void micStatus(int status) {
        if (floatMenu != null) {
            floatMenu.micStatus(status);
        }
    }

    public boolean checkFloatPermission(Context context){
        if (!checkPermission(context)){
           new CommonDialogBuilder()
                    .setDesc(context.getString(R.string.float_title))
                    .setImageTitleRes(R.mipmap.icon_open_float)
                    .setConfirm(context.getString(R.string.float_open))
                    .setCancel(context.getString(R.string.float_cancel))
                    .setListener(new CommonActionListener() {
                        @Override
                        public void onConfirm() {
                            try {
                                if (Build.VERSION.SDK_INT < 23) {
                                    if (RomUtils.checkIsMiuiRom()) {
                                        MiuiUtils.applyMiuiPermission(context);
                                    } else if (RomUtils.checkIsMeizuRom()) {
                                        MeizuUtils.applyPermission(context);
                                    } else if (RomUtils.checkIsHuaweiRom()) {
                                        HuaweiUtils.applyPermission(context);
                                    } else if (RomUtils.checkIs360Rom()) {
                                        QikuUtils.applyPermission(context);
                                    } else if (RomUtils.checkIsOppoRom()) {
                                        OppoUtils.applyOppoPermission(context);
                                    }
                                } else {
                                    if (RomUtils.checkIsMeizuRom()) {
                                        MeizuUtils.applyPermission(context);
                                    } else {
                                        FloatManager.commonROMPermissionApplyInternal(context);
                                    }
                                }
                            } catch (Throwable e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onCancel() {

                        }
            }).create()
                    .show(((AppCompatActivity)context).getSupportFragmentManager(), CommonDialogFragment.class.getSimpleName());
           return false;
        }else {
            return true;
        }
    }
}
