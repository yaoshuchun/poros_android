package com.liquid.poros.ui.fragment.dialog.session

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.blankj.utilcode.util.ToastUtils
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.liquid.base.adapter.CommonAdapter
import com.liquid.base.adapter.CommonViewHolder
import com.liquid.base.event.EventCenter
import com.liquid.im.kit.custom.AskPTAGiftAttachment
import com.liquid.library.retrofitx.RetrofitX
import com.liquid.poros.R
import com.liquid.poros.analytics.utils.MultiTracker
import com.liquid.poros.base.BaseDialogFragment
import com.liquid.poros.constant.Constants
import com.liquid.poros.constant.TrackConstants
import com.liquid.poros.entity.BaseBean
import com.liquid.poros.http.ApiUrl
import com.liquid.poros.http.observer.ObjectObserver
import com.liquid.poros.http.utils.postPoros
import com.liquid.poros.utils.ViewUtils
import de.greenrobot.event.EventBus
import de.greenrobot.event.Subscribe
import de.greenrobot.event.ThreadMode
import java.util.*

/**
 * 赠送虚拟装扮
 */
class SendVirtualDialogFragment : BaseDialogFragment() {

    lateinit var askPTAGiftAttachment: AskPTAGiftAttachment

    var day = 0

    var coin :Long = 0

    var cost = 0

    var femaleGuestId = ""

    override fun getPageId(): String {
        return TrackConstants.PAGE_SEND_VIRTUAL_DIALOG
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        EventBus.getDefault().register(this)
    }

    @Subscribe(threadMode = ThreadMode.MainThread)
    fun onEventBus(eventCenter: EventCenter<*>?) {
        if (null != eventCenter) {
            if (eventCenter.eventCode == Constants.EVENT_CODE_COIN_CHANGE) {
                val data = eventCenter.data as String
                try {
                    coin = data.toLong()
                }catch (e:Exception){
                    e.printStackTrace()
                }
            }
        }
    }

    override fun onStart() {
        super.onStart()
        val win = dialog!!.window
        // 一定要设置Background，如果不设置，window属性设置无效
        win.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val dm = DisplayMetrics()
        activity!!.windowManager.defaultDisplay.getMetrics(dm)
        val params = win.attributes
        params.gravity = Gravity.CENTER
        isCancelable = true
        // 使用ViewGroup.LayoutParams，以便Dialog 宽度充满整个屏幕
        params.width = (dm.widthPixels * 0.8).toInt()
        win.attributes = params
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(activity!!)
        val view = LayoutInflater.from(activity).inflate(R.layout.fragment_send_virtual_dialog, null)

        Glide.with(context!!).load(askPTAGiftAttachment.icon_url).into(view.findViewById(R.id.iv_icon))
        val tv_cost = view.findViewById<TextView>(R.id.tv_cost)

        val type = object : TypeToken<List<AskPTAGiftAttachment.Option>>() {}.type
        val list = Gson().fromJson<List<AskPTAGiftAttachment.Option>>(askPTAGiftAttachment.duration_options, type)
        if (!list.isNullOrEmpty()){
            list[0].select = true

            view.findViewById<RecyclerView>(R.id.cost_rv).apply {
                layoutManager = GridLayoutManager(context, 4)
                adapter = object : CommonAdapter<AskPTAGiftAttachment.Option>(context, R.layout.item_send_virtual_cost) {
                    override fun convert(holder: CommonViewHolder, t: AskPTAGiftAttachment.Option, pos: Int) {
                        holder.getView<TextView>(R.id.tv_duration).isSelected = t.select
                        holder.setText(R.id.tv_duration,  "${t.days}天")
                        holder.setOnClickListener(R.id.tv_duration, View.OnClickListener {
                            if (!t.select){
                                list.forEachIndexed { index, option ->
                                    option.select = pos == index
                                }
                                tv_cost.text = "${t.coins}"
                                day = t.days
                                cost = t.coins
                                notifyDataSetChanged()
                            }
                        })
                    }
                }.apply { add(list) }
            }

            tv_cost.text = "${list[0].coins}"
            day = list[0].days
            cost = list[0].coins
        }

        view.findViewById<TextView>(R.id.buy).setOnClickListener {
            if (ViewUtils.isFastClick()){
                return@setOnClickListener
            }

            if (coin >= cost){
                RetrofitX.url(ApiUrl.CARTOON_ASK_GRANT)
                        .param("ask_id", askPTAGiftAttachment.ask_id)
                        .param("days", day)
                        .postPoros()
                        .subscribe(object : ObjectObserver<BaseBean>() {
                            override fun success(result: BaseBean) {
                                if (isDetached){
                                    return
                                }

                                when(result.code){
                                    0 -> {dismissAllowingStateLoss()
                                        ToastUtils.showShort("购买成功")
                                        val params = HashMap<String?, String?>()
                                        params["female_guest_id"] = femaleGuestId
                                        params["dress_id"] = askPTAGiftAttachment.suit_id
                                        params["dress_time"] = "$day"
                                        MultiTracker.onEvent(TrackConstants.B_BUY_SEND_DRESS_SUCCESS, params)
                                    }

                                    20005 -> {
                                        ToastUtils.showShort("金币不足请充值")
                                        EventBus.getDefault().post(EventCenter<Any?>(Constants.EVENT_CODE_BUY_COIN_NOT_ENOUGH))

                                    }
                                    else ->{
                                        ToastUtils.showShort("购买失败")
                                    }
                                }
                            }

                            override fun failed(result: BaseBean) {
                                ToastUtils.showShort("购买失败")
                            }
                        })
            }else{
                EventBus.getDefault().post(EventCenter<Any?>(Constants.EVENT_CODE_BUY_COIN_NOT_ENOUGH))
            }

            val params = HashMap<String?, String?>()
            params["female_guest_id"] = femaleGuestId
            params["dress_id"] = askPTAGiftAttachment.suit_id
            params["dress_time"] = "$day"
            MultiTracker.onEvent(TrackConstants.B_BUY_SEND_DRESS_CLICK, params)
        }

        builder.setView(view)
        return builder.create()
    }

    companion object {
        fun newInstance(): SendVirtualDialogFragment {
            val fragment = SendVirtualDialogFragment()
            val bundle = Bundle()
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        EventBus.getDefault().unregister(this)
    }
}