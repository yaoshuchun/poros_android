package com.liquid.poros.ui.fragment.dialog.account;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.liquid.base.event.EventCenter;
import com.liquid.poros.R;
import com.liquid.poros.adapter.CoupleCardAdapter;
import com.liquid.poros.base.BaseActivity;
import com.liquid.poros.analytics.utils.MultiTracker;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.contract.account.BuyCoupleContract;
import com.liquid.poros.contract.account.BuyCouplePresenter;
import com.liquid.poros.entity.BaseModel;
import com.liquid.poros.entity.BuyCoupleCardInfo;
import com.liquid.poros.entity.CoupleCardInfo;
import com.liquid.poros.entity.CoupleCardListInfo;
import com.liquid.poros.entity.UserCoinInfo;
import com.liquid.poros.ui.dialog.RechargeCoinDialog;
import com.liquid.poros.ui.fragment.dialog.user.GroupNotificationDialogFragment;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.poros.utils.ViewUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import de.greenrobot.event.EventBus;
import de.greenrobot.event.Subscribe;
import de.greenrobot.event.ThreadMode;

/**
 * 购买cp卡界面
 */
public class BuyCoupleCardDialogFragment extends BottomSheetDialogFragment implements BuyCoupleContract.View {
    private TextView tv_coin;
    private TextView tv_recharge;
    private TextView tv_cp_card_right;
    private TextView tv_cp_card_title;
    private TextView tv_cp_card_desc;
    private RecyclerView rv_cp_card;
    private TextView tv_buy_cp_card;
    private TextView tv_not_couple;
    private int mPos = 0;
    private boolean mIsAgreeCp = false;//是否是邀请
    private boolean mIsCpRoom = true;//区分组cp直播房间 和 私聊界面
    private String mRoomId;
    private String mRoomAnchor;
    private String mTargetId;
    private String mCpOrderId;
    private String mCardCategory;
    private BuyCouplePresenter mPresenter;
    private List<CoupleCardInfo> mCpCardList = new ArrayList<>();
    private CoupleCardAdapter mAdapter;
    private CoupleCardListInfo mCoupleCardListInfo;
    private boolean isEnoughCoin = false; //金币是否足够
    private boolean isEnoughCard = false; //开黑&上麦卡是否足够
    private boolean isCpMatch = false;
    private String matchId;
    private String price;
    private String card_price;//开黑卡购买埋点参数
    private String buy_position;//开黑卡购买埋点参数

    public BuyCoupleCardDialogFragment setBuy_position(String buy_position) {
        this.buy_position = buy_position;
        return this;
    }

    public BuyCoupleCardDialogFragment setMatchId(String matchId) {
        this.matchId = matchId;
        return this;
    }

    public BuyCoupleCardDialogFragment setCpMatch(boolean cpMatch) {
        isCpMatch = cpMatch;
        return this;
    }

    public static BuyCoupleCardDialogFragment newInstance() {
        BuyCoupleCardDialogFragment fragment = new BuyCoupleCardDialogFragment();
        return fragment;
    }

    public BuyCoupleCardDialogFragment setCpCardInfo(boolean isAgreeCp, boolean isCpRoom, String roomId, String roomAnchor, String targetId, String cpOrderId, String cardCategory) {
        this.mIsAgreeCp = isAgreeCp;
        this.mIsCpRoom = isCpRoom;
        this.mRoomId = roomId;
        this.mRoomAnchor = roomAnchor;
        this.mTargetId = targetId;
        this.mCpOrderId = cpOrderId;
        this.mCardCategory = cardCategory;
        return this;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        try {
            super.show(manager, tag);
        } catch (Exception e) {

        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
        View view = View.inflate(getContext(), R.layout.fragment_cp_card_dialog, null);
        dialog.setContentView(view);
        //设置透明背景
        dialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);
        View root = dialog.getDelegate().findViewById(R.id.design_bottom_sheet);
        BottomSheetBehavior behavior = BottomSheetBehavior.from(root);
        behavior.setHideable(false);
        initializeView(view);
        mPresenter = new BuyCouplePresenter();
        mPresenter.attachView(this);
        mPresenter.getUserCoin();
        mPresenter.getCpCardList(mIsCpRoom, mTargetId, mCardCategory);
        return dialog;
    }

    @Subscribe(threadMode = ThreadMode.MainThread)
    public void onEventBus(EventCenter eventCenter) {
        switch (eventCenter.getEventCode()) {
            case Constants.EVENT_CODE_COIN_CHANGE:
                if (mPresenter != null) {
                    mPresenter.getUserCoin();
                }
                break;
        }
    }

    private void initializeView(View view) {
        tv_coin = view.findViewById(R.id.tv_coin);
        tv_recharge = view.findViewById(R.id.tv_recharge);
        tv_cp_card_right = view.findViewById(R.id.tv_cp_card_right);
        tv_cp_card_title = view.findViewById(R.id.tv_cp_card_title);
        tv_cp_card_desc = view.findViewById(R.id.tv_cp_card_desc);
        rv_cp_card = view.findViewById(R.id.rv_cp_card);
        tv_buy_cp_card = view.findViewById(R.id.tv_buy_cp_card);
        tv_not_couple = view.findViewById(R.id.tv_not_couple);
        rv_cp_card.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        mAdapter = new CoupleCardAdapter(getActivity(), mIsCpRoom, mCpCardList);
        rv_cp_card.setAdapter(mAdapter);
        mAdapter.setOnItemListener(new CoupleCardAdapter.OnItemListener() {
            @Override
            public void onClick(View v, int pos) {
                mPos = pos;
                mAdapter.setDefSelect(pos);
                mCurrentCardInfo = mCpCardList.get(mPos);
                changeStatus();
            }
        });
        tv_recharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 充值按钮
                if (listener != null) {
                    listener.goRecharge(RechargeCoinDialog.POSITION_TEAM_JOIN_CARD_ALERT);
                    listener.goRecharge();
                }
                dismissAllowingStateLoss();
            }
        });
        //购买cp卡
        tv_buy_cp_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ViewUtils.isFastClick()) {
                    return;
                }
                if (listener != null && mCpCardList != null && mCpCardList.size() > 0) {
                    CoupleCardInfo coupleCardInfo = mCpCardList.get(mPos);
                    if (isEnoughCard) {
                        switch (coupleCardInfo.getCard_category()) {
                            case "TEAM"://开黑卡
                                card_price = "";
                                mPresenter.userTeamCard(mTargetId, mRoomId, true, "");
                                price = "0";
                                break;
                            case "MIC"://上麦卡
                                mPresenter.userMicCard(mRoomId, true, "");
                                break;
                            case "CP"://cp卡
                                mPresenter.buyCouple(mIsAgreeCp, mRoomId, mRoomAnchor, coupleCardInfo.getCard_type(), String.valueOf(coupleCardInfo.getCard_price()), mTargetId, mCpOrderId);
                                break;
                        }
                    } else if (isEnoughCoin) {
                        switch (coupleCardInfo.getCard_category()) {
                            case "TEAM"://开黑卡
                                card_price = String.valueOf(coupleCardInfo.getCard_price());
                                mPresenter.userTeamCard(mTargetId, mRoomId, false, String.valueOf(coupleCardInfo.getCard_price()));
                                price = String.valueOf(coupleCardInfo.getCard_price());
                                break;
                            case "MIC"://上麦卡
                                mPresenter.userMicCard(mRoomId, false, String.valueOf(coupleCardInfo.getCard_price()));
                                break;
                            case "CP"://cp卡
                                mPresenter.buyCouple(mIsAgreeCp, mRoomId, mRoomAnchor, coupleCardInfo.getCard_type(), String.valueOf(coupleCardInfo.getCard_price()), mTargetId, mCpOrderId);
                                break;
                        }
                    } else {
                        // 点击底部 金额不足的按钮
                        listener.goRecharge(RechargeCoinDialog.POSITION_BUY_AND_USE_TEAM_CARD);
                        listener.goRecharge();
                    }

                }
            }
        });
    }

    @Override
    public void cpCardListFetch(String targetId, CoupleCardListInfo coupleCardListInfo) {
        if (coupleCardListInfo == null || coupleCardListInfo.getData() == null || coupleCardListInfo.getData().getCard_list() == null || coupleCardListInfo.getData().getCard_list().size() == 0) {
            return;
        }
        rv_cp_card.setVisibility(View.VISIBLE);
        tv_not_couple.setVisibility(View.GONE);
        mCoupleCardListInfo = coupleCardListInfo;
        mCpCardList.clear();
        mCpCardList.addAll(coupleCardListInfo.getData().getCard_list());
        if (mCpCardList != null && mCpCardList.size() > 0) {
            mCurrentCardInfo = mCpCardList.get(mPos);
            changeStatus();
        }
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void buyCpSuccessFetch(String targetId, BuyCoupleCardInfo buyCoupleCardInfo) {
        if (listener != null) {
            listener.buyCardSuccess(mIsAgreeCp, targetId, buyCoupleCardInfo);
        }
        dismissAllowingStateLoss();
    }

    @Override
    public void onUserCoinFetch(UserCoinInfo userCoinInfo) {
        if (userCoinInfo != null && userCoinInfo.getCode() == 0 && userCoinInfo.getData() != null && !TextUtils.isEmpty(userCoinInfo.getData().getCoin())) {
            AccountUtil.getInstance().setCoin(userCoinInfo.getData().getCoin());
            tv_coin.setText(userCoinInfo.getData().getCoin());
            changeStatus();
        }
    }

    @Override
    public void buyCardMsg(String msg) {
        if (listener != null) {
            listener.buyCardMsg(msg);
        }
    }

    @Override
    public void notCoupleMsg() {
        //无法与对方组CP
        rv_cp_card.setVisibility(View.GONE);
        tv_not_couple.setVisibility(View.VISIBLE);
        tv_not_couple.setText(R.string.no_couple_tip);
        tv_not_couple.setTextColor(mIsCpRoom ? Color.parseColor("#ffffff") : Color.parseColor("#242424"));
    }

    @Override
    public void userTeamCardFetch(BaseModel baseModel) {
        if (baseModel != null){
            if (baseModel.getCode() == 0){
                if (listener != null) {
                    listener.userTeamCardSuccess();
                }
                dismissAllowingStateLoss();
                uploadBehavior(true);
                uploadBuyEvent("success");
            }else {
                uploadBehavior(false);
                uploadBuyEvent("fail");
            }
        }else {
            uploadBehavior(false);
            uploadBuyEvent("fail");
        }
    }

    private void uploadBuyEvent(String result){
        HashMap<String, String> params = new HashMap<>();
        params.put("card_price", card_price);
        params.put("buy_position", buy_position);
        params.put("result",result);
        MultiTracker.onEvent(TrackConstants.B_BUY_TEAM_JOIN_CARD,params);
    }

    private void uploadBuyDialogExposure(){
        HashMap<String, String> params = new HashMap<>();
        params.put("buy_position", buy_position);
        MultiTracker.onEvent(TrackConstants.B_TEAM_JOIN_CARD_BUY_ALERT_EXPOSURE,params);
    }

    @Override
    public void onResume() {
        super.onResume();
        uploadBuyDialogExposure();
    }

    @Override
    public void userMicCardFetch(BaseModel baseModel) {
        if (listener != null) {
            listener.userMicCardSuccess();
        }
        dismissAllowingStateLoss();
    }

    private CoupleCardInfo mCurrentCardInfo;

    private void changeStatus() {
        if (mCurrentCardInfo == null) {
            return;
        }
        String coin = AccountUtil.getInstance().getCoin();
        BigDecimal bigDecimal1 = new BigDecimal(coin);
        BigDecimal bigDecimal2 = new BigDecimal(mCurrentCardInfo.getCard_price());
        int coinCode = bigDecimal1.compareTo(bigDecimal2);
        isEnoughCoin = coinCode >= 0;
        if (!TextUtils.isEmpty(mCurrentCardInfo.getCard_cnt())) {
            int code = new BigDecimal(mCurrentCardInfo.getCard_cnt()).compareTo(BigDecimal.ZERO);
            isEnoughCard = code > 0;
            if (isEnoughCard) {
                tv_buy_cp_card.setBackgroundResource(R.drawable.bg_common_button_save);
                switch (mCurrentCardInfo.getCard_category()) {
                    case "TEAM":
                        tv_buy_cp_card.setText(R.string.couple_use_team_card);
                        break;
                    case "MIC":
                        tv_buy_cp_card.setText(R.string.couple_use_mic_card);
                        break;
                }
            } else {
                if (isEnoughCoin) {
                    tv_buy_cp_card.setBackgroundResource(R.drawable.bg_common_button_save);
                    tv_buy_cp_card.setText(R.string.couple_card_buy);
                } else {
                    tv_buy_cp_card.setText(R.string.couple_no_enough_coin);
                    tv_buy_cp_card.setBackgroundResource(R.drawable.bg_button_no_enough);
                }
            }
        } else {
            isEnoughCard = false;
        }
        tv_cp_card_title.setText(mCurrentCardInfo.getTitle());
        tv_cp_card_desc.setText(mCurrentCardInfo.getDesc());
        if (mCurrentCardInfo.isShow_cp_right()) {
            if (isEnoughCoin) {
                if ("TEAM".equals(mCurrentCardInfo.getCard_category())) {
                    tv_buy_cp_card.setText(R.string.couple_card_kaihei);
                } else {
                    tv_buy_cp_card.setText(R.string.couple_card_buy);
                }
                tv_buy_cp_card.setBackgroundResource(R.drawable.bg_common_button_save);
            } else {
                tv_buy_cp_card.setText(R.string.couple_no_enough_coin);
                tv_buy_cp_card.setBackgroundResource(R.drawable.bg_button_no_enough);
            }
            String cpCardRightFirst = mCurrentCardInfo.getTip();
            String cpCardRightSecond = getString(R.string.couple_right);
            SpannableString spannableString = new SpannableString(cpCardRightFirst + cpCardRightSecond);
            //设置背景颜色
            spannableString.setSpan(new TextViewURLSpan(), cpCardRightFirst.length(), cpCardRightFirst.length() + cpCardRightSecond.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            spannableString.setSpan(new UnderlineSpan(), cpCardRightFirst.length(), cpCardRightFirst.length() + cpCardRightSecond.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            spannableString.setSpan(new ForegroundColorSpan(Color.parseColor("#509DEB")), cpCardRightFirst.length(), cpCardRightFirst.length() + cpCardRightSecond.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            tv_cp_card_right.setText(spannableString);
            //设置可点击
            tv_cp_card_right.setMovementMethod(LinkMovementMethod.getInstance());
        } else {
            tv_cp_card_right.setText(mCurrentCardInfo.getTip());
            if (isEnoughCoin) {
                if ("TEAM".equals(mCurrentCardInfo.getCard_category())) {
                    tv_buy_cp_card.setText(R.string.couple_card_kaihei);
                } else {
                    tv_buy_cp_card.setText(R.string.couple_card_buy);
                }
            }
        }

    }

    class TextViewURLSpan extends ClickableSpan {
        @Override
        public void updateDrawState(TextPaint ds) {
        }

        @Override
        public void onClick(View widget) {
            GroupNotificationDialogFragment.newInstance().setGroupNotification(false, mCoupleCardListInfo.getData().getCp_right_title(), mCoupleCardListInfo.getData().getCp_right_content()).show(getActivity().getSupportFragmentManager(), GroupNotificationDialogFragment.class.getSimpleName());

        }
    }

    @Override
    public void showLoading() {
        BaseActivity activity = (BaseActivity) getActivity();
        activity.showLoading();
    }

    @Override
    public void showSuccess() {

    }

    @Override
    public void closeLoading() {
        BaseActivity activity = (BaseActivity) getActivity();
        activity.closeLoading();
    }

    @Override
    public void onError() {

    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSuccess(Object data) {

    }


    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);
        if (listener != null) {
            listener.onDismiss();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
        if (listener != null) {
            listener.onDismiss();
            listener = null;
        }
        EventBus.getDefault().unregister(this);
    }

    private BuyCardListener listener;

    public BuyCoupleCardDialogFragment setBuyListener(BuyCardListener listener) {
        this.listener = listener;
        return this;
    }

    public interface BuyCardListener {
        //跳转充值
        void goRecharge();

        //购买CP卡成功
        void buyCardSuccess(boolean isAgree, String targetId, BuyCoupleCardInfo buyCoupleCardInfo);

        //CP卡列表提示信息
        void buyCardMsg(String msg);

        //购买/使用开黑卡成功
        void userTeamCardSuccess();

        //购买/使用上麦卡成功
        void userMicCardSuccess();

        void onDismiss();

        void goRecharge(String position);

    }

    private void uploadBehavior(boolean success){
        if (isCpMatch){
            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put("female_guest_id",mTargetId);
            hashMap.put("act_type",success?"success":"fail");
            hashMap.put("card_amount",price);
            hashMap.put("match_id",matchId);
            hashMap.put("event_time", String.valueOf(System.currentTimeMillis()));
            MultiTracker.onEvent(TrackConstants.B_USE_TEAM_JOIN_CARD,hashMap);
        }
    }
}
