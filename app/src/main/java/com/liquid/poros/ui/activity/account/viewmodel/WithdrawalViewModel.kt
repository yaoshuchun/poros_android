package com.liquid.poros.ui.activity.account.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.liquid.library.retrofitx.RetrofitX
import com.liquid.poros.entity.BaseBean
import com.liquid.poros.entity.WithdrawalInfo
import com.liquid.poros.http.ApiUrl
import com.liquid.poros.http.observer.ObjectObserver
import com.liquid.poros.http.utils.postPoros

class WithdrawalViewModel : ViewModel() {
    val withdrawalInfoSuccess: MutableLiveData<WithdrawalInfo> by lazy { MutableLiveData<WithdrawalInfo>() }
    val withdrawalInfoFail: MutableLiveData<WithdrawalInfo> by lazy { MutableLiveData<WithdrawalInfo>() }
    val withdrawalBingWechatSuccess: MutableLiveData<BaseBean> by lazy { MutableLiveData<BaseBean>() }
    val withdrawalBingWechatFail: MutableLiveData<BaseBean> by lazy { MutableLiveData<BaseBean>() }
    val userWithdrawalSuccess: MutableLiveData<BaseBean> by lazy { MutableLiveData<BaseBean>() }
    val userWithdrawalFail: MutableLiveData<BaseBean> by lazy { MutableLiveData<BaseBean>() }

    /**
     * 获取提现详情
     * @param withdraw_type cash 现金提现,candy 糖果提现
     */
    fun getWithdrawalInfo(withdraw_type: String) {
        RetrofitX.url(ApiUrl.GET_WITHDRAWAL_INFO)
                .param("withdraw_type", withdraw_type)
                .postPoros()
                .subscribe(object : ObjectObserver<WithdrawalInfo>() {
                    override fun success(result: WithdrawalInfo) {
                        withdrawalInfoSuccess.postValue(result)
                    }

                    override fun failed(result: WithdrawalInfo) {
                        withdrawalInfoFail.postValue(result)
                    }
                })
    }
    /**
     * 绑定微信
     */
    fun bindWechat(code: String) {
        RetrofitX.url(ApiUrl.BIND_WECHAT)
                .param("code", code)
                .postPoros()
                .subscribe(object : ObjectObserver<BaseBean>() {
                    override fun success(result: BaseBean) {
                        withdrawalBingWechatSuccess.postValue(result)
                    }

                    override fun failed(result: BaseBean) {
                        withdrawalBingWechatFail.postValue(result)
                    }
                })
    }

    /**
     * 发起提现
     * @param withdraw_type 提现类型
     * @param withdraw_channel 提现方式 1:微信提现,2:支付宝(暂未支持)
     */
    fun userWithDraw(withdraw_type: String) {
        RetrofitX.url(ApiUrl.USER_WITHDRAW)
                .param("withdraw_type", withdraw_type)
                .param("withdraw_channel", 1)
                .postPoros()
                .subscribe(object : ObjectObserver<BaseBean>() {
                    override fun success(result: BaseBean) {
                        userWithdrawalSuccess.postValue(result)
                    }

                    override fun failed(result: BaseBean) {
                        userWithdrawalFail.postValue(result)
                    }
                })
    }
}