package com.liquid.poros.ui.fragment

import android.content.Context
import android.content.Intent
import android.graphics.drawable.Animatable
import android.os.Bundle
import android.text.Html
import android.text.TextUtils
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import android.widget.RelativeLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.request.RequestOptions
import com.liquid.poros.R
import com.liquid.poros.analytics.utils.MultiTracker
import com.liquid.poros.constant.Constants
import com.liquid.poros.constant.TrackConstants
import com.liquid.poros.entity.FriendMessageInfo
import com.liquid.poros.helper.JinquanResourceHelper
import com.liquid.poros.ui.activity.user.SessionActivity
import com.liquid.poros.ui.activity.user.SessionActivityV2
import com.liquid.poros.utils.DateUtil
import com.liquid.poros.utils.ScreenUtil
import com.liquid.poros.utils.TimeUtils
import com.liquid.poros.utils.ViewUtils
import kotlinx.android.synthetic.main.item_friend_message.view.*

/**
 * 首页消息Tab内的朋友消息Item
 */
class MessageFriendItemView @JvmOverloads constructor(
        context: Context,isExpireFri:Boolean, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    private lateinit var friendMessageInfo: FriendMessageInfo
    lateinit var friendName:TextView
    lateinit var itemContent:RelativeLayout
    var clientId = 0;

    init {
        inflate(context, R.layout.item_friend_message, this)
        friendName = tv_friend_name
        itemContent = rl_content
        setOnClickListener {
            if (ViewUtils.isFastClick()) {
                return@setOnClickListener
            }
            val bundle = Bundle()
            if (isExpireFri) {
                bundle.putString(Constants.P2P_CHAT_FROM,"temp")
            }else {
                bundle.putString(Constants.P2P_CHAT_FROM,"init")
            }
            bundle.putString(Constants.SESSION_ID, friendMessageInfo.user_id)
            var intent = if(clientId == 2) {
                Intent(context, SessionActivityV2::class.java)
            }else {
                Intent(context, SessionActivity::class.java)
            }
            intent.putExtras(bundle)
            context.startActivity(intent)
            MultiTracker.onEvent(TrackConstants.B_MESSAGE)
        }
    }

    fun setData(friendMessageInfo: FriendMessageInfo) {
        this.friendMessageInfo = friendMessageInfo
        if (friendMessageInfo.isGuard_tag) {
            guard_angel_tag.visibility = VISIBLE
        } else {
            guard_angel_tag.visibility = GONE
        }
        clientId = friendMessageInfo.client
        val options = RequestOptions()
                .placeholder(R.mipmap.img_default_avatar)
                .apply(RequestOptions.bitmapTransform(CircleCrop()))
                .override(ScreenUtil.dip2px(58f), ScreenUtil.dip2px(58f))
                .error(R.mipmap.img_default_avatar)
        Glide.with(context)
                .load(friendMessageInfo.avatar_url)
                .apply(options)
                .into(iv_friend_icon)
        if (!TextUtils.isEmpty(friendMessageInfo.cp_tag)) {
            Glide.with(context).load(friendMessageInfo.cp_tag).into(iv_cp_tag)
            iv_cp_tag.visibility = VISIBLE
        } else {
            iv_cp_tag.visibility = GONE
        }
        tv_friend_name.text = friendMessageInfo.nick_name
        tv_friend_content.text = if (!TextUtils.isEmpty(friendMessageInfo.newest_msg_body)) Html.fromHtml(friendMessageInfo.newest_msg_body) else ""
        tv_message_time.visibility = if (TextUtils.isEmpty(friendMessageInfo.newest_msg_body)) GONE else VISIBLE
        tv_message_time.text = TimeUtils.getTimeShowString(friendMessageInfo.newest_msg_time, false)
        tv_message_unread_count.visibility = if (friendMessageInfo.unread_msg_cnt > 0) VISIBLE else GONE
        if (friendMessageInfo.unread_msg_cnt < 100) {
            tv_message_unread_count.text = friendMessageInfo.unread_msg_cnt.toString()
        } else {
            tv_message_unread_count.text = "99+"
        }
        //在线状态
        iv_online_state.setBackgroundResource(if (friendMessageInfo.isIs_online) R.drawable.bg_on_line else R.drawable.bg_off_line)
        //上麦状态
        iv_voice_state.visibility = if (friendMessageInfo.isIs_on_mic) VISIBLE else GONE
        //直播中状态
        if (friendMessageInfo.isIs_on_video) {
            ll_friend_live.visibility = VISIBLE
            if (friendMessageInfo.isIs_in_private_room) {
                tv_friend_status.text = resources.getString(R.string.index_private_room_tip)
                ll_friend_live.setBackgroundResource(R.drawable.bg_oval_99ff598c)
            } else {
                tv_friend_status.text = resources.getString(R.string.index_live_tip)
                ll_friend_live.setBackgroundResource(R.drawable.bg_oval_9910d8c8)
            }
        } else {
            ll_friend_live.visibility = GONE
        }
        iv_anim.visibility = if (friendMessageInfo.isIs_on_video || friendMessageInfo.isIs_in_private_room) VISIBLE else GONE
        if (iv_anim.visibility == VISIBLE) {
            (iv_anim.drawable as Animatable).start()
        } else {
            (iv_anim.drawable as Animatable).stop()
        }
        //置顶消息
        //置顶消息
        if (friendMessageInfo.pin_priority != -1) {
            JinquanResourceHelper.getInstance(context).setBackgroundColorByAttr(rl_content, R.attr.custom_shape_bg)
        } else {
            JinquanResourceHelper.getInstance(context).setBackgroundColorByAttr(rl_content, R.attr.custom_attr_app_bg)
        }
        JinquanResourceHelper.getInstance(context).setTextColorByAttr(tv_friend_name, R.attr.custom_attr_main_title_color)
        JinquanResourceHelper.getInstance(context).setTextColorByAttr(tv_message_time, R.attr.custom_attr_sub_text_color)

        if (friendMessageInfo.curr_temp_friend_remain_time > 0) {
            tv_temp_friend_timer.visibility = View.VISIBLE
            tv_temp_friend_timer.text = "对话于${DateUtil.secondToDHMSStr(friendMessageInfo.curr_temp_friend_remain_time)}后消失"
        } else {
            tv_temp_friend_timer.visibility = View.GONE
        }
    }
}