package com.liquid.poros.ui.fragment.dialog.session;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.liquid.base.adapter.CommonAdapter;
import com.liquid.base.adapter.CommonViewHolder;
import com.liquid.poros.R;
import com.liquid.poros.base.BaseDialogFragment;
import com.liquid.poros.analytics.utils.MultiTracker;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.entity.MsgUserModel;
import com.liquid.poros.ui.activity.DeclarationActivity;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.poros.utils.ViewUtils;

import java.util.HashMap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * CP等级弹窗
 */
public class CoupleLevelTestDialogFragment extends BaseDialogFragment {

    private String sessionId;
    private String avatarUrl;
    private String intimacy;

    public CoupleLevelTestDialogFragment setInfo(MsgUserModel.P2PRankUpgradeAwardInfo info, String sessionId,String avatarUrl,String intimacy) {
        this.info = info;
        this.sessionId = sessionId;
        this.avatarUrl = avatarUrl;
        this.intimacy = intimacy;
        return this;
    }

    private MsgUserModel.P2PRankUpgradeAwardInfo info;

    public static CoupleLevelTestDialogFragment newInstance() {
        CoupleLevelTestDialogFragment fragment = new CoupleLevelTestDialogFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected String getPageId() {
        return TrackConstants.PAGE_CP_LEVEL_TEST_DIALOG;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onStart() {
        super.onStart();
        Window win = getDialog().getWindow();
        // 一定要设置Background，如果不设置，window属性设置无效
        win.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams params = win.getAttributes();
        params.gravity = Gravity.CENTER;
        // 使用ViewGroup.LayoutParams，以便Dialog 宽度充满整个屏幕
        params.width = (int) (dm.widthPixels * 0.8);
        win.setAttributes(params);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_cp_level_test_dialog, null);

        if (info != null) {
            ((TextView) view.findViewById(R.id.des)).setText(String.format("成功晋级【%s】后\n" +
                    "领取每日%s奖励", info.group_name, info.group_name));

            RecyclerView reward_rv = view.findViewById(R.id.reward_rv);
            if (info.award_list != null && info.award_list.size() > 0) {
                reward_rv.setLayoutManager(new GridLayoutManager(getContext(), info.award_list.size()));

                CommonAdapter<MsgUserModel.P2PRankUpgradeAwardInfo.Info> adapter = new CommonAdapter<MsgUserModel.P2PRankUpgradeAwardInfo.Info>(getContext(), R.layout.item_cp_upgrade_award) {
                    @Override
                    public void convert(CommonViewHolder holder, final MsgUserModel.P2PRankUpgradeAwardInfo.Info item, int pos) {
                        holder.setText(R.id.award_des, item.title);
                        Glide.with(getContext())
                                .load(item.icon)
                                .into((ImageView) holder.getView(R.id.award_iv));
                    }
                };

                adapter.add(info.award_list);
                reward_rv.setAdapter(adapter);
            }

            view.findViewById(R.id.upgrade).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (ViewUtils.isFastClick()) {
                        return;
                    }

                    Intent intent = new Intent(getActivity(), DeclarationActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.SESSION_ID, sessionId);
                    bundle.putString("target_user_head", avatarUrl);
                    bundle.putString("rise_to", info.group_name);
                    bundle.putString("intimacy", intimacy);
                    intent.putExtras(bundle);
                    getActivity().startActivity(intent);

                    HashMap<String, String> params = new HashMap<>();
                    params.put("user_id", AccountUtil.getInstance().getUserId());
                    params.put("female_guest_id", sessionId);
                    params.put("event_time", String.valueOf(System.currentTimeMillis()));
                    params.put("rise_to", info.group_name);
                    params.put("intimacy", intimacy);
                    MultiTracker.onEvent(TrackConstants.B_CLICK_POPUP_RISE_CP_LEVEL, params);

                    dismissDialog();
                }
            });
            builder.setView(view);
        }
        return builder.create();
    }
}
