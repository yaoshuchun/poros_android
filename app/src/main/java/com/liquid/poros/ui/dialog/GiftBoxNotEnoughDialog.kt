package com.liquid.poros.ui.dialog

import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.liquid.poros.R
import com.liquid.poros.base.BaseDialogFragment2
import com.liquid.poros.entity.GetGiftBoxExplain

class GiftBoxNotEnoughDialog: BaseDialogFragment2() {
    private lateinit var explain: GetGiftBoxExplain
    private lateinit var tv_sure:TextView

    private lateinit var tv_name:TextView
    private lateinit var iv_icon:ImageView



    companion object{
        const val INFO = "info"
    }


    override fun contentXmlId(): Int {
        return R.layout.dialog_f_gift_box_not_enough

    }

    override fun initView() {

        tv_name = rootView?.findViewById(R.id.tv_name)!!
        iv_icon = rootView?.findViewById(R.id.iv_icon)!!
        tv_sure = rootView?.findViewById(R.id.tv_sure)!!

    }

    override fun initData() {
        super.initData()
        explain = arguments?.get(INFO) as GetGiftBoxExplain

        explain?.let {

            tv_name.text = explain.title
            Glide.with(mContext!!)
                    .load(explain.imgUrl+"")
                    .placeholder(R.mipmap.icon_default)
                    .error(R.mipmap.icon_default)
                    .into(iv_icon)
            tv_sure.text = explain.btnText

        }
    }

    override fun initListener() {
        tv_sure.setOnClickListener {
            dismiss()
        }
    }



}