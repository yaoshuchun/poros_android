package com.liquid.poros.ui.fragment.home;

import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.liquid.base.guideview.Component;
import com.liquid.base.guideview.DimenUtil;
import com.liquid.base.guideview.Guide;
import com.liquid.base.guideview.GuideBuilder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.liquid.base.adapter.CommonAdapter;
import com.liquid.base.adapter.CommonViewHolder;
import com.liquid.base.adapter.MultiItemCommonAdapter;
import com.liquid.base.adapter.MultiItemTypeSupport;
import com.liquid.base.utils.ToastUtil;
import com.liquid.im.kit.permission.MPermission;
import com.liquid.im.kit.permission.annotation.OnMPermissionDenied;
import com.liquid.im.kit.permission.annotation.OnMPermissionGranted;
import com.liquid.im.kit.permission.annotation.OnMPermissionNeverAskAgain;
import com.liquid.im.kit.permission.MPermission;
import com.liquid.poros.R;
import com.liquid.poros.base.BaseFragment;
import com.liquid.poros.analytics.utils.MultiTracker;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.constant.DataOptimize;
import com.liquid.poros.constant.DispatchFemaleEnum;
import com.liquid.poros.constant.SquareStatusEnum;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.contract.home.IndexContract;
import com.liquid.poros.contract.home.IndexPresenter;
import com.liquid.poros.entity.CpListInfo;
import com.liquid.poros.entity.IndexCpInviteInfo;
import com.liquid.poros.entity.RoomDetailsInfo;
import com.liquid.poros.helper.JinquanResourceHelper;
import com.liquid.poros.ui.MainActivity;
import com.liquid.poros.ui.RouterUtils;
import com.liquid.poros.ui.activity.SoloVideoListActivity;
import com.liquid.poros.ui.activity.audiomatch.AudioMatchInfoActivity;
import com.liquid.poros.ui.activity.audiomatch.CPMatchingGlobal;
import com.liquid.poros.ui.activity.live.RoomLiveActivityV2;
import com.liquid.poros.ui.activity.live.VideoFastDatingActivity;
import com.liquid.poros.ui.activity.user.UserCenterActivity;
import com.liquid.poros.ui.fragment.dialog.common.CommonActionListener;
import com.liquid.poros.ui.fragment.dialog.common.CommonDialogBuilder;
import com.liquid.poros.ui.fragment.dialog.common.CommonDialogFragment;
import com.liquid.poros.ui.fragment.dialog.live.ReceiveIndexInviteCpDialogFragment;
import com.liquid.poros.ui.fragment.dialog.videomatch.VideoMatchTipDialog;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.poros.analytics.utils.ExposureUtils;
import com.liquid.poros.utils.ScreenUtil;
import com.liquid.poros.utils.StringUtil;
import com.liquid.poros.utils.TimeUtils;
import com.liquid.poros.utils.ViewUtils;
import com.liquid.poros.widgets.ComponentView;
import com.liquid.poros.widgets.FlowLayout;
import com.liquid.poros.widgets.WrapContentLinearLayoutManager;
import com.liquid.poros.widgets.pulltorefresh.BaseRefreshListener;
import com.liquid.poros.widgets.pulltorefresh.PullToRefreshLayout;
import com.liquid.poros.widgets.pulltorefresh.ViewStatus;
import com.netease.lava.nertc.sdk.NERtc;
import com.netease.lava.nertc.sdk.NERtcEx;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

import static com.liquid.base.utils.ContextUtils.getApplicationContext;
import static com.liquid.poros.base.BaseActivity.BASIC_PERMISSION_REQUEST_CODE;

/**
 * 4Tab 之一 广场页面
 */
public class IndexFragment extends BaseFragment implements IndexContract.View, BaseRefreshListener {
   public static final int BASIC_PERMISSION_REQUEST_CODE = 0x100;
    @BindView(R.id.rv_group)
    RecyclerView rvGroup;
    @BindView(R.id.container)
    PullToRefreshLayout container;
    @BindView(R.id.iv_title)
    ImageView iv_title;
    @BindView(R.id.fl_head)
    View fl_head;
    @BindView(R.id.rl_container)
    View rl_container;
    @BindView(R.id.ll_audio_match)
    LinearLayout ll_audio_match;
    @BindView(R.id.hs_video_match)
    HorizontalScrollView hs_video_match;
    private IndexPresenter mPresenter;
    private int page = 0;
    private List<String> videoMatchDuration;
    private String videoMatchDurationDesc;
    private CommonAdapter<CpListInfo.UserItem> mCommonAdapter;
    private ArrayList<CpListInfo.UserItem> userItems = new ArrayList<>();
    private ExposureUtils<CommonAdapter, CpListInfo.UserItem> squareExposureUtils;
    private final String TAG_WANGZHE = "wzry";
    private final String TAG_HEPING = "hpjy";
    private final String TAG_CHAT = "chat";
    private String mTitle;
    private GuideBuilder mGuideBuilder;

    public static IndexFragment newInstance() {
        return new IndexFragment();
    }

    @Override
    protected String getPageId() {
        return TrackConstants.PAGE_HOME_INDEX;
    }

    @Override
    protected void onFirstUserVisible() {

    }

    @Override
    protected void onUserVisible() {

    }

    @Override
    protected void onUserInvisible() {

    }

    @Override
    protected View getLoadingTargetView() {
        return null;
    }

    @Override
    protected void initViewsAndEvents(Bundle savedInstanceState) {
        mPresenter = new IndexPresenter();
        mPresenter.attachView(this);
        mPresenter.fetchFetchCPList(page);
        container.setRefreshListener(this);

        mCommonAdapter = new CommonAdapter<CpListInfo.UserItem>(getActivity(), R.layout.item_cp_user) {
            @Override
            public void convert(CommonViewHolder holder, CpListInfo.UserItem userItem, int pos) {
                TextView tv_user_name = holder.getView(R.id.tv_user_name);
                JinquanResourceHelper.getInstance(getActivity()).setTextColorByAttr(tv_user_name, R.attr.custom_attr_main_text_color);
                TextView tv_want_game = holder.getView(R.id.tv_want_game);
                JinquanResourceHelper.getInstance(getActivity()).setTextColorByAttr(tv_want_game, R.attr.custom_attr_tag_text_color);
                JinquanResourceHelper.getInstance(getActivity()).setBackgroundResourceByAttr(tv_want_game, R.attr.custom_attr_tag_bg_color);

                TextView tv_desc = holder.getView(R.id.tv_desc);
                JinquanResourceHelper.getInstance(getActivity()).setTextColorByAttr(tv_desc, R.attr.custom_attr_main_text_color);

                ImageView iv_head = holder.getView(R.id.iv_user_head);
                RequestOptions headOptions = new RequestOptions()
                        .placeholder(R.mipmap.img_default_avatar)
                        .transform(new CircleCrop())
                        .error(R.mipmap.img_default_avatar);
                Glide.with(getActivity())
                        .load(userItem.getAvatar_url())
                        .apply(headOptions)
                        .into(iv_head);
                holder.itemView.setTag(userItem.getUser_id());
                holder.setText(R.id.tv_user_name, userItem.getNick_name());
                holder.setText(R.id.tv_status, userItem.getStatus_str());
                switch (userItem.getStatus()) {
                    case SQUARE_ONLINE://在线
                        holder.setTextColor(R.id.tv_status, getResources().getColor(R.color.tv_status_online));
                        break;
                    case SQUARE_LIVE://组队中
                    case SQUARE_PK_ROOM://pk中
                        holder.setTextColor(R.id.tv_status, getResources().getColor(R.color.tv_status_friend));
                        break;
                    case SQUARE_BUSY://忙碌
                    case SQUARE_PRIVATE_ROOM://专属房间中
                        holder.setTextColor(R.id.tv_status, getResources().getColor(R.color.tv_status_busy));
                        break;
                    case SQUARE_OFFLINE://离线
                        holder.setTextColor(R.id.tv_status, getResources().getColor(R.color.tv_status_offline));
                        break;
                    case SQUARE_ORDERING://正在找队友
                        holder.setTextColor(R.id.tv_status, getResources().getColor(R.color.tv_status_finding));
                        break;
                }
                ImageView iv_to_room_tip = holder.getView(R.id.iv_to_room_tip);
                TextView tv_to_room = holder.getView(R.id.tv_to_room);
                ImageView bottomBg = holder.getView(R.id.bg_anim);
                ImageView iv_anim = holder.getView(R.id.iv_anim);
                if (!StringUtil.isEmpty(userItem.getCp_room_id())) {
                    tv_to_room.setVisibility(View.VISIBLE);
                    iv_to_room_tip.setVisibility(View.VISIBLE);
                    iv_anim.setVisibility(View.VISIBLE);
                    bottomBg.setVisibility(View.VISIBLE);
                    if (userItem.getStatus() == SquareStatusEnum.SQUARE_LIVE || userItem.getStatus() == SquareStatusEnum.SQUARE_PK_ROOM) {
                        iv_to_room_tip.setBackgroundResource(R.drawable.bg_oval_00aaff_line);
                        tv_to_room.setBackgroundResource(R.drawable.bg_gradual_14d4d6_5);
                        bottomBg.setBackgroundResource(R.drawable.bg_video_room);
                        tv_to_room.setText(userItem.getStatus() == SquareStatusEnum.SQUARE_LIVE ? getString(R.string.index_live_tip) : getString(R.string.index_live_pk_tip));
                    } else if (userItem.getStatus() == SquareStatusEnum.SQUARE_PRIVATE_ROOM) {
                        iv_to_room_tip.setBackgroundResource(R.drawable.bg_oval_ff598c_line);
                        tv_to_room.setBackgroundResource(R.drawable.bg_gradual_fc6573_5);
                        bottomBg.setBackgroundResource(R.drawable.bg_private_video_room);
                        tv_to_room.setText(getString(R.string.index_private_room_tip));
                    }

                    iv_anim.setVisibility(StringUtil.isEmpty(userItem.getCp_room_id()) ? View.GONE : View.VISIBLE);
                    if (iv_anim.getVisibility() == View.VISIBLE) {
                        ((Animatable) iv_anim.getDrawable()).start();
                    } else {
                        ((Animatable) iv_anim.getDrawable()).stop();
                    }
                } else {
                    tv_to_room.setVisibility(View.GONE);
                    iv_to_room_tip.setVisibility(View.GONE);
                    iv_anim.setVisibility(View.GONE);
                    ((Animatable) iv_anim.getDrawable()).stop();
                    bottomBg.setVisibility(View.GONE);
                }
                holder.setVisibility(R.id.iv_feed_has_new, userItem.getNew_feed() == 1 ? View.VISIBLE : View.GONE);
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (ViewUtils.isFastClick()) {
                            return;
                        }
                        if (userItem.getStatus() == SquareStatusEnum.SQUARE_LIVE || userItem.getStatus() == SquareStatusEnum.SQUARE_PRIVATE_ROOM || userItem.getStatus() == SquareStatusEnum.SQUARE_PK_ROOM) {
                            mPresenter.fetchRoomDetails(userItem.getCp_room_id());
                        } else {
                            mPresenter.fetchDispatchFemale(DispatchFemaleEnum.DIS_USER_CENTER.getCode(), userItem.getUser_id());
                            Bundle bundle = new Bundle();
                            bundle.putString(Constants.USER_ID, userItem.getUser_id());
                            bundle.putString(Constants.GIFT_RANK_PATH, "square");
                            readyGo(UserCenterActivity.class, bundle);
                            TrackPoint.guestClick(AccountUtil.getInstance().getUserId(),userItem.getUser_id());
                            TrackPoint.footPrintEnter(AccountUtil.getInstance().getUserId(),userItem.getUser_id(), "square_details");

                        }

                    }
                });

                holder.setText(R.id.tv_desc, StringUtil.replaceBlank(userItem.getCouple_desc()));
                holder.setVisibility(R.id.tv_desc, StringUtil.isEmpty(userItem.getCouple_desc()) ? View.GONE : View.VISIBLE);
                //年龄
                TextView tv_room_mic_age = holder.getView(R.id.tv_user_age);
                tv_room_mic_age.setBackgroundResource(userItem.getGender() == 1 ? R.drawable.bg_color_00aaff_16 : R.drawable.bg_color_ff75a3_16);
                tv_room_mic_age.setVisibility(TextUtils.isEmpty(userItem.getAge()) ? View.GONE : View.VISIBLE);
                Drawable ageDrawable = getActivity().getResources().getDrawable(userItem.getGender() == 1 ? R.mipmap.icon_male : R.mipmap.icon_female);
                ageDrawable.setBounds(0, 0, ageDrawable.getMinimumWidth(), ageDrawable.getMinimumHeight());
                tv_room_mic_age.setCompoundDrawables(ageDrawable, null, null, null);
                tv_room_mic_age.setCompoundDrawablePadding(3);
                tv_room_mic_age.setText(userItem.getAge());
                List<String> tagString = new ArrayList<>();
                tagString.clear();
                if (userItem.getGames() != null && userItem.getGames().size() > 0) {
                    tagString.addAll(userItem.getGames());
                }
                if (tagString != null && tagString.size() > 0) {
                    if (userItem.getStatus() == SquareStatusEnum.SQUARE_PRIVATE_ROOM) {
                        JinquanResourceHelper.getInstance(getActivity()).setBackgroundResourceByAttr(tv_want_game, R.attr.custom_private_tag_bg_color);
                        tv_want_game.setTextColor(getResources().getColor(R.color.tv_status_busy));
                    } else if (userItem.getStatus() == SquareStatusEnum.SQUARE_LIVE || userItem.getStatus() == SquareStatusEnum.SQUARE_ORDERING || userItem.getStatus() == SquareStatusEnum.SQUARE_PK_ROOM) {
                        JinquanResourceHelper.getInstance(getActivity()).setBackgroundResourceByAttr(tv_want_game, R.attr.custom_attr_tag_bg_color);
                        JinquanResourceHelper.getInstance(getActivity()).setTextColorByAttr(tv_want_game, R.attr.custom_attr_tag_text_color);
                    }
                    tv_want_game.setVisibility(userItem.getStatus() == SquareStatusEnum.SQUARE_LIVE || userItem.getStatus() == SquareStatusEnum.SQUARE_ORDERING || userItem.getStatus() == SquareStatusEnum.SQUARE_PRIVATE_ROOM || userItem.getStatus() == SquareStatusEnum.SQUARE_PK_ROOM ? View.VISIBLE : View.GONE);
                    SpannableStringBuilder spannableString = new SpannableStringBuilder(getString(R.string.cp_want_play));
                    for (int i = 0; i < tagString.size(); i++) {
                        spannableString.append(" ");
                        if (i == tagString.size() - 1) {
                            spannableString.append(tagString.get(i));
                        } else {
                            spannableString.append(tagString.get(i) + " |");
                        }
                    }
                    tv_want_game.setText(spannableString);
                } else {
                    tv_want_game.setVisibility(View.GONE);
                }
                FlowLayout fl_tags = holder.getView(R.id.fl_game);
                if (tagString != null && tagString.size() > 0) {
                    fl_tags.setVisibility(userItem.getStatus() == SquareStatusEnum.SQUARE_LIVE || userItem.getStatus() == SquareStatusEnum.SQUARE_ORDERING || userItem.getStatus() == SquareStatusEnum.SQUARE_PRIVATE_ROOM || userItem.getStatus() == SquareStatusEnum.SQUARE_PK_ROOM ? View.GONE : View.VISIBLE);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    layoutParams.setMargins(0, 0, ScreenUtil.dip2px(8), ScreenUtil.dip2px(4));
                    fl_tags.removeAllViews();
                    for (String item : tagString) {
                        final TextView tv = new TextView(getContext());
                        tv.setPadding(ScreenUtil.dip2px(8), ScreenUtil.dip2px(2), ScreenUtil.dip2px(8), ScreenUtil.dip2px(2));
                        tv.setText(item);
                        tv.setMaxEms(10);
                        tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 10);
                        tv.setGravity(Gravity.CENTER);
                        tv.setSingleLine();
                        tv.setLayoutParams(layoutParams);
                        JinquanResourceHelper.getInstance(getActivity()).setTextColorByAttr(tv, R.attr.custom_attr_flow_item_text_color);
                        JinquanResourceHelper.getInstance(getActivity()).setBackgroundResourceByAttr(tv, R.attr.custom_attr_bg_flow_item);
                        fl_tags.addView(tv, layoutParams);
                    }
                } else {
                    fl_tags.setVisibility(View.GONE);
                }

                RecyclerView fl_img = holder.getView(R.id.fl_img);
                fl_img.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        return holder.itemView.onTouchEvent(event);
                    }
                });

                if (userItem.getPhoto_list() != null && userItem.getPhoto_list().size() > 0) {
                    List<CpListInfo.RefItem> photoLists = userItem.getPhoto_list().subList(0, userItem.getPhoto_list().size() >= 3 ? 3 : userItem.getPhoto_list().size());
                    CommonAdapter<CpListInfo.RefItem> refAdapter = new MultiItemCommonAdapter<CpListInfo.RefItem>(getActivity(), new MultiItemTypeSupport<CpListInfo.RefItem>() {
                        @Override
                        public int getLayoutId(int itemType) {
                            if (itemType == 1) {
                                return R.layout.item_cp_user_ref_img;
                            }
                            return R.layout.item_cp_user_ref_video;
                        }

                        @Override
                        public int getItemViewType(int position, CpListInfo.RefItem refItem) {
                            return refItem.getType();
                        }
                    }) {
                        @Override
                        public void convert(CommonViewHolder holder, CpListInfo.RefItem refItem, int pos) {
                            ImageView iv_image = holder.getView(R.id.iv_image);
                            final int itemHeight = (((ScreenUtil.adjustWidgetWidth() - ScreenUtil.dip2px(120)))) / 3;
                            holder.itemView.getLayoutParams().height = itemHeight;
                            holder.itemView.getLayoutParams().width = itemHeight;
                            RequestOptions headOptions = new RequestOptions()
                                    .placeholder(R.mipmap.icon_photo_default)
                                    .error(R.mipmap.icon_photo_default)
                                    .transform(new CenterCrop(), new RoundedCorners(ScreenUtil.dip2px(8)));
                            Glide.with(getActivity())
                                    .load(refItem.getPath())
                                    .apply(headOptions)
                                    .into(iv_image);
                        }
                    };
                    refAdapter.update(photoLists);
                    fl_img.setLayoutManager(new GridLayoutManager(getActivity(), 3));
                    fl_img.setAdapter(refAdapter);
                    fl_img.setVisibility(View.VISIBLE);
                } else {
                    fl_img.setVisibility(View.GONE);

                }

            }
        };
        rvGroup.setLayoutManager(new WrapContentLinearLayoutManager(getActivity()));
        rvGroup.setAdapter(mCommonAdapter);
        rvGroup.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    mPresenter.fetchIndexCpInvite();
                }
            }
        });

    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.fragment_index;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (squareExposureUtils == null) {
            squareExposureUtils = new ExposureUtils(mCommonAdapter, userItems);
            squareExposureUtils.setmRecyclerView(rvGroup);
        }
        squareExposureUtils.startFeedExposureStatistics();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (squareExposureUtils != null) {
            squareExposureUtils.calExplosure();
            squareExposureUtils.stopFeedExposureStatistics();
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisible) {
        super.setUserVisibleHint(isVisible);
        if (isResumed()) {
            if (isVisible) {
                startExpore();
            } else {
                if (squareExposureUtils != null) {
                    squareExposureUtils.calExplosure();
                    squareExposureUtils.stopFeedExposureStatistics();
                }
            }
        }
    }

    public void startExpore() {
        if (squareExposureUtils == null) {
            squareExposureUtils = new ExposureUtils<>(mCommonAdapter, userItems);
            squareExposureUtils.setmRecyclerView(rvGroup);
        }
        squareExposureUtils.startFeedExposureStatistics();
    }

    @Override
    public void onCPListFetched(CpListInfo cpListInfo) {
        container.finishRefresh();
        container.finishLoadMore();
        if ((page == 0) && (cpListInfo == null || cpListInfo.getData() == null || cpListInfo.getData().getUser_list() == null || cpListInfo.getData().getUser_list().size() == 0)) {
            container.showView(ViewStatus.EMPTY_STATUS);
            container.setCanLoadMore(false);
            return;
        }
        videoMatchDuration = cpListInfo.getData().getVideo_match_open_time();
        videoMatchDurationDesc = cpListInfo.getData().getVideo_match_open_time_desc();
        ll_audio_match.setVisibility(cpListInfo.getData().isVideo_match_show() ? View.GONE : View.VISIBLE);
        hs_video_match.setVisibility(cpListInfo.getData().isVideo_match_show() ? View.VISIBLE : View.GONE);
        if (cpListInfo.getData().isVideo_match_show()) {
            TrackPoint.videoMatchExposure();
        }
        container.setCanLoadMore(true);
        container.showView(ViewStatus.CONTENT_STATUS);

        if (page == 0) {
            userItems.clear();
            userItems.addAll(cpListInfo.getData().getUser_list());
            mCommonAdapter.update(cpListInfo.getData().getUser_list());
        } else {
            userItems.addAll(cpListInfo.getData().getUser_list());
            mCommonAdapter.add(cpListInfo.getData().getUser_list());
        }
    }



    @Override
    public void onCPInviteFetched(IndexCpInviteInfo indexCpInviteInfo) {
        if (indexCpInviteInfo == null || indexCpInviteInfo.getData() == null) {
            return;
        }
        if (indexCpInviteInfo.getData().isShow()) {
            ReceiveIndexInviteCpDialogFragment.newInstance()
                    .setUserInfo(indexCpInviteInfo.getData())
                    .show(getSupportFragmentManager(), ReceiveIndexInviteCpDialogFragment.class.getSimpleName());
        }
    }

    @Override
    public void onRoomDetailsFetched(RoomDetailsInfo roomDetailsInfo) {
        if (roomDetailsInfo.getData() == null) {
            return;
        }
        if (roomDetailsInfo.getData().getRoom_type() == RoomLiveActivityV2.ROOM_TYPE_PRIVATE) {
            //专属房
            if (!TextUtils.isEmpty(roomDetailsInfo.getData().getHint())) {
                showMessage(roomDetailsInfo.getData().getHint());
            }
        } else {
            //Cp房 直播中
            Bundle bundle = new Bundle();
            bundle.putString(Constants.ROOM_ID, roomDetailsInfo.getData().getRoom_id());
            bundle.putString(Constants.IM_ROOM_ID, roomDetailsInfo.getData().getIm_room_id());
            bundle.putString(Constants.AUDIENCE_FROM, DataOptimize.SQUARE);
            RouterUtils.goToRoomLiveActivity(getContext(),bundle);
            TrackPoint.goToRoomLive(AccountUtil.getInstance().getUserId(),roomDetailsInfo.getData().getGirl_id(),"square");
        }
    }

    @OnClick({R.id.iv_audio_wangzhe, R.id.iv_audio_heping, R.id.iv_audio_chat,
    R.id.iv_king_match,R.id.iv_peace_match,R.id.iv_radio_match,R.id.iv_video_match})
    public void onClick(View view) {
        if (ViewUtils.isFastClick()) {
            return;
        }
        if (CPMatchingGlobal.getInstance((MainActivity) getActivity()).isMatching) {
            showMessage(getString(R.string.audio_matching_tip));
            return;
        }
        switch (view.getId()) {
            case R.id.iv_king_match:
            case R.id.iv_audio_wangzhe:
                matchInfo(TAG_WANGZHE);
                break;
            case R.id.iv_peace_match:
            case R.id.iv_audio_heping:
                matchInfo(TAG_HEPING);
                break;
            case R.id.iv_radio_match:
            case R.id.iv_audio_chat:
                matchInfo(TAG_CHAT);
                break;
            case R.id.iv_video_match:
               TrackPoint.videoMatchClick();
                checkPermission();
                break;
        }
    }



    private void checkPermission() {
        //获取权限
        List<String> lackPermissions = NERtcEx.getInstance().checkPermission(IndexFragment.this.getActivity());
        if (!lackPermissions.isEmpty()) {
            String[] permissions = new String[lackPermissions.size()];
            for (int i = 0; i < lackPermissions.size(); i++) {
                permissions[i] = lackPermissions.get(i);
            }
            MPermission.with(IndexFragment.this)
                    .setRequestCode(BASIC_PERMISSION_REQUEST_CODE)
                    .permissions(permissions)
                    .request();
            return;
        }
        checkTime();

    }
    //判断是否在开启时间内
    private VideoMatchTipDialog dialog;
    boolean isBetweenDuration;
    private void checkTime() {
        if (videoMatchDuration == null || videoMatchDuration.size() < 2){
            return;
        }
        isBetweenDuration = videoMatchDuration.get(0).compareTo(TimeUtils.getHourAndMin(System.currentTimeMillis())) <= 0 &&
                videoMatchDuration.get(1).compareTo(TimeUtils.getHourAndMin(System.currentTimeMillis())) >= 0;
        if (!isBetweenDuration) {
            TrackPoint.videoMatchNotStartTip();
            if (dialog == null) {
                dialog = new VideoMatchTipDialog(mContext);
            }
            dialog.show();
            dialog.setQuickMatchContent(videoMatchDurationDesc);
        }else {
//            readyGo(VideoFastDatingActivity.class);
            readyGo(SoloVideoListActivity.class);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        MPermission.onRequestPermissionsResult(this, requestCode, permissions, grantResults);
    }
    @OnMPermissionGranted(BASIC_PERMISSION_REQUEST_CODE)
    public void onBasicPermissionSuccess() {
        checkTime();
    }
    @OnMPermissionDenied(BASIC_PERMISSION_REQUEST_CODE)
    @OnMPermissionNeverAskAgain(BASIC_PERMISSION_REQUEST_CODE)
    void onBasicPermissionFailed() {
        ToastUtil.show("音视频通话所需权限未全部授权，部分功能可能无法正常运行！");
    }



    private void matchInfo(String type) {
        if (TAG_WANGZHE.equals(type)) {
            mTitle = getString(R.string.audio_match_info_wangzhe_title);
        } else if (TAG_HEPING.equals(type)) {
            mTitle = getString(R.string.audio_match_info_heping_title);
        } else {
            mTitle = getString(R.string.audio_match_info_chat_title);
        }
        Bundle bundle = new Bundle();
        bundle.putString(Constants.AUDIO_MATCH_INFO_TITLE, mTitle);
        bundle.putString(Constants.AUDIO_MATCH_INFO_TYPE, type);
        bundle.putInt(Constants.AUDIO_MATCH_ENTRY_TYPE, Constants.ENTRY_SQUARE);
        readyGo(AudioMatchInfoActivity.class, bundle);
        TrackPoint.teamJoinStartClick(String.valueOf(System.currentTimeMillis()),type,"square");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }

    @Override
    public void refresh() {
        page = 0;
        mPresenter.fetchFetchCPList(page);
    }

    @Override
    public void loadMore() {
        ++page;
        mPresenter.fetchFetchCPList(page);
    }

    @Override
    public void notifyByThemeChanged() {
        JinquanResourceHelper.getInstance(getActivity()).setBackgroundColorByAttr(rl_container, R.attr.custom_attr_app_bg);
        JinquanResourceHelper.getInstance(getActivity()).setBackgroundColorByAttr(fl_head, R.attr.custom_attr_app_bg);
        JinquanResourceHelper.getInstance(getActivity()).setImageResourceByAttr(iv_title, R.attr.bg_index_title);
        if (mCommonAdapter != null) {
            mCommonAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void notifyNetworkChanged(boolean isConnected) {
        container.showView(isConnected ? ViewStatus.CONTENT_STATUS : ViewStatus.ERROR_STATUS);
        container.setCanRefresh(isConnected);
    }

    private void showGuideView(View anchor) {
        if (anchor == null) {
            return;
        }
        mGuideBuilder = new GuideBuilder();
        mGuideBuilder.setTargetView(anchor)
                .setOverlayTarget(false)
                .setHighTargetCorner(DimenUtil.dp2px(getApplicationContext(), 10))
                .setHighTargetPaddingLeft(DimenUtil.dp2px(getApplicationContext(), 5))
                .setHighTargetPaddingRight(DimenUtil.dp2px(getApplicationContext(), 5))
                .setHighTargetPaddingTop(DimenUtil.dp2px(getApplicationContext(), 10))
                .setHighTargetPaddingBottom(DimenUtil.dp2px(getApplicationContext(), 10))
                .setAlpha(153)
                .setHighTargetGraphStyle(Component.ROUNDRECT);
        mGuideBuilder.setOnVisibilityChangedListener(new GuideBuilder.OnVisibilityChangedListener() {
            @Override
            public void onShown() {

            }

            @Override
            public void onDismiss() {
                mGuideBuilder = null;
            }
        });
        mGuideBuilder.addComponent(new ComponentView(R.mipmap.icon_index_audio_match_guide, Component.ANCHOR_BOTTOM, Component.FIT_CENTER, 0, 20));
        Guide guide = mGuideBuilder.createGuide();
        guide.show(getActivity());
    }
    static class TrackPoint{
        //视频速配入口曝光埋点
        private static void videoMatchExposure() {
            HashMap<String,String> params = new HashMap<>();
            MultiTracker.onEvent(TrackConstants.B_TEAM_VIDEO_JOIN_START_BUTTON_EXPOSURE,params);
        }
        //视频速配入口点击埋点
        private static void videoMatchClick() {
            HashMap<String,String> params = new HashMap<>();
            MultiTracker.onEvent(TrackConstants.B_TEAM_VIDEO_JOIN_START_BUTTON_CLICK,params);
        }
        //速配未开启提示
        private static void videoMatchNotStartTip() {
            HashMap<String,String> params = new HashMap<>();
            MultiTracker.onEvent(TrackConstants.B_VIDEO_MATCH_NOT_OPEN_POPUP_EXPOSURE,params);
        }


        public static void goToRoomLive(String userId, String girl_id, String from) {
            HashMap<String, String> params = new HashMap<>();
            params.put("user_id", userId);
            params.put("female_guest_id", girl_id);
            params.put("video_room_click_from", from);
            MultiTracker.onEvent(TrackConstants.B_GO_TEAM, params);
        }

        /**
         * 速配开始点击
         * @param eventTime
         * @param type
         * @param from
         */
        public static void teamJoinStartClick(String eventTime, String type, String from) {
            HashMap<String, String> params = new HashMap<>();
            params.put("event_time", eventTime);
            params.put("match_type", type);
            params.put("entry_type", from);
            MultiTracker.onEvent(TrackConstants.B_TEAM_JOIN_START_BUTTON_CLICK, params);
        }

        /**
         * 足迹场景触发
         * @param userId
         * @param femaleUserId
         * @param from
         */
        public static void footPrintEnter(String userId, String femaleUserId, String from) {
            HashMap<String, String> femaleParams = new HashMap<>();
            femaleParams.put("user_id", userId);
            femaleParams.put("female_guest_id", femaleUserId);
            femaleParams.put("dispatch_female_type", from);
            MultiTracker.onEvent(TrackConstants.B_DISPATCH_FEMALE, femaleParams);
        }

        /**
         * 点击女嘉宾
         * @param userId
         * @param femaleUserId
         */
        public static void guestClick(String userId, String femaleUserId) {
            HashMap<String, String> params = new HashMap<>();
            params.put("user_id", userId);
            params.put("female_guest_id", femaleUserId);
            MultiTracker.onEvent(TrackConstants.B_SQUARE_USER, params);
        }
    }
}
