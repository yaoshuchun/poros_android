package com.liquid.poros.ui.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.liquid.base.tools.GT;
import com.liquid.base.tools.StringUtil;
import com.liquid.base.utils.ContextUtils;
import com.liquid.poros.R;
import com.liquid.poros.base.BaseActivity;
import com.liquid.poros.analytics.utils.MultiTracker;
import com.liquid.poros.entity.CpLevelUpDetail;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.poros.utils.network.HttpCallback;
import com.liquid.poros.utils.network.RetrofitHttpManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;
import okhttp3.RequestBody;

public class DeclarationDetailActivity extends BaseActivity {

    @BindView(R.id.iv_back)
    ImageView iv_back;

    @BindView(R.id.target_declaration_iv)
    ImageView target_declaration_iv;
    @BindView(R.id.target_declaration_head)
    ImageView target_declaration_head;
    @BindView(R.id.target_user_name)
    TextView target_user_name;
    @BindView(R.id.target_declaration_desc)
    TextView target_declaration_desc;

    @BindView(R.id.me_user_head)
    ImageView me_user_head;
    @BindView(R.id.me_user_gift_img)
    ImageView me_user_gift_img;
    @BindView(R.id.me_user_name)
    TextView me_user_name;
    @BindView(R.id.me_declaration_desc)
    TextView me_declaration_desc;

    @BindView(R.id.me_user_content_ll)
    RelativeLayout me_user_content_ll;
    @BindView(R.id.me_user_empty_ll)
    RelativeLayout me_user_empty_ll;

    @BindView(R.id.target_user_content_ll)
    RelativeLayout target_user_content_ll;
    @BindView(R.id.target_user_empty_ll)
    RelativeLayout target_user_empty_ll;
    @BindView(R.id.cp_declaration_comfirm_tv)
    TextView cp_declaration_comfirm_tv;

    private String female_user_id;
    private String female_user_head;
    private String female_user_name;
    private int level = -1;
    private String intimacy;

    @Override
    protected String getPageId() {
        return null;
    }

    @Override
    protected void parseBundleExtras(Bundle extras) {
        female_user_id = extras.getString("female_user_id");
        female_user_head = extras.getString("female_user_head");
        level = extras.getInt("level", -1);
        female_user_name = extras.getString("female_user_name");
        intimacy = extras.getString("intimacy");
    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.acitivity_declaration_detail;
    }

    @Override
    protected void initViewsAndEvents(Bundle savedInstanceState) {
        checkCpLevelDetail();

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        cp_declaration_comfirm_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void checkCpLevelDetail (){
        try {
            JSONObject object = new JSONObject();
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("female_user_id",female_user_id);
            if(level != -1){
                object.put("level",level);
            }
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.checkCpLevelDetail(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    if (isDestroyed() || isFinishing()){
                        return;
                    }
                    closeLoading();
                    CpLevelUpDetail cpLevelUpDetail = GT.fromJson(result, CpLevelUpDetail.class);
                    if(cpLevelUpDetail != null){
                        CpLevelUpDetail.DataBean dataBean = cpLevelUpDetail.getData();
                        // 我自己是否发过宣言
                        if(dataBean.isLevel_swear()){
                            //我发过
                            me_user_content_ll.setVisibility(View.VISIBLE);
                            me_user_empty_ll.setVisibility(View.GONE);

                            CpLevelUpDetail.DataBean.AdvanceDetailBean advanceDetailBean = dataBean.getAdvance_detail();
                            if(advanceDetailBean != null ){
                                CpLevelUpDetail.DataBean.AdvanceDetailBean.GiftBean giftBean = advanceDetailBean.getGift();
                                String giftPic = giftBean.getGift_pic_url();
                                Glide.with(DeclarationDetailActivity.this)
                                        .load(giftPic)
                                        .into(me_user_gift_img);
                                me_user_name.setText(AccountUtil.getInstance().getUserInfo().getNick_name());
                            }

                            Glide.with(DeclarationDetailActivity.this)
                                    .load(AccountUtil.getInstance().getUserInfo().getAvatar_url())
                                    .into(me_user_head);

                            String swear_words = dataBean.getSwear_words();
                            me_declaration_desc.setText(swear_words);
                        }else {
                            me_user_content_ll.setVisibility(View.GONE);
                            me_user_empty_ll.setVisibility(View.VISIBLE);
                        }

                        if(dataBean.isTarget_level_swear()){
                            target_user_content_ll.setVisibility(View.VISIBLE);
                            target_user_empty_ll.setVisibility(View.GONE);

                            CpLevelUpDetail.DataBean.AdvanceDetailBean advanceDetailBean = dataBean.getAdvance_detail();
                            if(advanceDetailBean != null ){
                                CpLevelUpDetail.DataBean.AdvanceDetailBean.GiftBean giftBean = advanceDetailBean.getGift();
                                String giftPic = giftBean.getGift_pic_url();
                                Glide.with(DeclarationDetailActivity.this)
                                        .load(giftPic)
                                        .into(target_declaration_iv);
                                target_user_name.setText(female_user_name);//null
                            }

                            Glide.with(DeclarationDetailActivity.this)
                                    .load(female_user_head)//null
                                    .into(target_declaration_head);

                            String swear_words = dataBean.getTarget_swear_words();
                            target_declaration_desc.setText(swear_words);
                        }else{
                            target_user_content_ll.setVisibility(View.GONE);
                            target_user_empty_ll.setVisibility(View.VISIBLE);
                        }

                        if(dataBean != null){
                            String swear_words = dataBean.getSwear_words();
                            String target_swear_words = dataBean.getTarget_swear_words();

                            HashMap<String, String> params = new HashMap<>();
                            params.put("user_id", AccountUtil.getInstance().getUserId());
                            params.put("female_guest_id", female_user_id);
                            params.put("event_time", String.valueOf(System.currentTimeMillis()));
                            params.put("rise_to",level + "");
                            params.put("intimacy", intimacy);
                            params.put("user_oath", swear_words);
                            params.put("guest_oath", target_swear_words);
                            MultiTracker.onEvent("b_exposure_oath_page", params);
                        }

                        int code = cpLevelUpDetail.getCode();
                        if(code != 0){
                            String msg = cpLevelUpDetail.getMsg();
                            if(StringUtil.isNotEmpty(msg)){
                                Toast.makeText(ContextUtils.getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }

                @Override
                public void OnFailed(int ret, String result) {
                    closeLoading();
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
