package com.liquid.poros.ui.activity;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.liquid.base.event.EventCenter;
import com.liquid.poros.R;
import com.liquid.poros.adapter.FansCommentAdapter;
import com.liquid.poros.base.BaseActivity;
import com.liquid.poros.analytics.utils.MultiTracker;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.contract.user.FeedDetailContract;
import com.liquid.poros.contract.user.FeedDetailPresenter;
import com.liquid.poros.entity.CommentBean;
import com.liquid.poros.entity.CommentInfo;
import com.liquid.poros.entity.FeedDetailBean;
import com.liquid.poros.entity.SingleCommentBean;
import com.liquid.poros.helper.JinquanResourceHelper;
import com.liquid.poros.ui.activity.user.ReportActivity;
import com.liquid.poros.ui.activity.user.UserCenterActivity;
import com.liquid.poros.ui.fragment.dialog.user.FeedMoreDialogFragment;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.poros.utils.StringUtil;
import com.liquid.poros.utils.ViewUtils;
import com.liquid.poros.utils.glide.GlideEngine;
import com.liquid.poros.widgets.MultiImageView;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.tools.ToastUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;

public class FeedDetailActivity extends BaseActivity implements FeedDetailContract.View {
    public static final String FEED_ID = "feed_id";
    public static final String GUEST_ID = "guest_id";
    public static final String LAUNCH_MODE = "launch_mode";
    public static final int COMMENT_MODE = 1; //评论打开
    public static final int LOOK_MODE = 0; //评论打开
    FeedDetailContract.Presenter mPresent;
    private RoundedCorners roundedCorners;
    private RequestOptions options;
    @BindView(R.id.person_avatar)
    ImageView personAvatar;
    @BindView(R.id.news_user_name)
    TextView newsUserName;
    @BindView(R.id.news_time)
    TextView newsTime;
    @BindView(R.id.rv_comment)
    RecyclerView rvCommentList;
    @BindView(R.id.sound_news)
    RelativeLayout soundNews;
    @BindView(R.id.sound_news_anim)
    ImageView soundNewsAnim;
    @BindView(R.id.ll_comment)
    LinearLayout llComment;
    @BindView(R.id.sound_new_time)
    TextView soundNewsTime;
    @BindView(R.id.text_news)
    TextView textNews;
    @BindView(R.id.iv_like)
    ImageView ivLike;
    @BindView(R.id.comment_send)
    TextView commentSend;
    @BindView(R.id.input_comment)
    EditText inputComment;
    @BindView(R.id.mu_image)
    MultiImageView mu_image;
    @BindView(R.id.comment_count)
    TextView commentCount;
    @BindView(R.id.iv_like_tag)
    ImageView likeTag;
    @BindView(R.id.like_count)
    TextView likeCount;
    @BindView(R.id.divider)
    TextView divider;
    @BindView(R.id.ll_comment_empty)
    LinearLayout ll_comment_empty;
    FansCommentAdapter commentAdapter;
    String feedId;
    String guestId;
    String voice;
    private int launchMode;
    private MediaPlayer mMediaPlayer;
    private Timer mTimer2;
    private TimerTask mTask2;
    private int mAudioDuration;
    private int mAudioTimer;
    private boolean isPause = false;//是否暂停
    private FeedMoreDialogFragment mFeedMoreDialogFragment = new FeedMoreDialogFragment();
    private String mFeedDetailsType;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        roundedCorners = new RoundedCorners(100);
        options = new RequestOptions()
                .placeholder(R.mipmap.icon_default)
                .error(R.mipmap.icon_default)
                .transform(new CenterCrop(), roundedCorners);
        mMediaPlayer = new MediaPlayer();
    }

    @Override
    protected String getPageId() {
        return TrackConstants.MOMENT_DETAIL_PAGE;
    }

    @Override
    protected HashMap<String, String> getParams() {
        HashMap<String, String> params = new HashMap<>();
        params.put(Constants.USER_ID, AccountUtil.getInstance().getUserId());
        params.put(Constants.PATH, "guest_home");
        params.put(Constants.MOMENT_ID, feedId);
        params.put(Constants.FEMALE_GUEST_ID, guestId);
        return params;
    }

    @Override
    protected void parseBundleExtras(Bundle extras) {
        feedId = extras.getString(FEED_ID);
        guestId = extras.getString(GUEST_ID);
        launchMode = extras.getInt(LAUNCH_MODE);
        mFeedDetailsType = extras.getString(Constants.FEED_DETAILS_TYPE);
    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.activity_feed_detail;
    }

    @Override
    protected void initViewsAndEvents(Bundle savedInstanceState) {
        mPresent = new FeedDetailPresenter();
        mPresent.attachView(this);
        inputComment.addTextChangedListener(listener);
        commentAdapter = new FansCommentAdapter(this);
        rvCommentList.setLayoutManager(new LinearLayoutManager(this));
        rvCommentList.setAdapter(commentAdapter);
        mPresent.getFeedDetail(feedId, 1,mFeedDetailsType);
        if (launchMode == 1) {
            showKeyBoard(inputComment);
        }
    }

    private TextWatcher listener = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            if (editable.length() == 0) {
                if (ivLike != null && commentSend != null) {
                    ivLike.setVisibility(View.VISIBLE);
                    commentSend.setVisibility(View.GONE);
                }
            } else {
                if (ivLike != null && commentSend != null) {
                    ivLike.setVisibility(View.GONE);
                    commentSend.setVisibility(View.VISIBLE);
                }
            }
        }
    };

    private SingleCommentBean feedInfo;

    @Override
    public void updateFeedInfo(FeedDetailBean bean) {
        if (isFinishing()||isDestroyed()||bean == null){
            return;
        }
        feedInfo = bean.feed_info;
        if (feedInfo != null) {
            voice = feedInfo.voice;
            Glide.with(mContext).load(feedInfo.avatar_url).apply(options).into(personAvatar);
            if (feedInfo.like_count == 0) {
                likeCount.setText("点赞");
            } else {
                likeCount.setText(feedInfo.like_count + "");
            }
            if (feedInfo.comment_count == 0) {
                commentCount.setText("评论");
            } else {
                commentCount.setText(feedInfo.comment_count + "");
            }
            commentAdapter.setSessionId(feedInfo.user_id);
            if (!StringUtil.isEmpty(feedInfo.voice)) {
                soundNews.setVisibility(View.VISIBLE);
                try {
                    mMediaPlayer.reset();
                    mMediaPlayer.setDataSource(feedInfo.voice);
                    mMediaPlayer.prepare();
                    mAudioDuration = feedInfo.voice_time;
                    mAudioTimer = mAudioDuration;
                    setAudioDuration(soundNewsTime);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                soundNews.setVisibility(View.GONE);
            }
            if (feedInfo.liked) {
                likeTag.setImageResource(R.mipmap.ic_like_selected);
                ivLike.setImageResource(R.mipmap.ic_like_selected);
                likeCount.setTextColor(Color.parseColor("#F06078"));
            } else {
                JinquanResourceHelper.getInstance(mContext).setImageResourceByAttr(likeTag, R.attr.bg_like);
                JinquanResourceHelper.getInstance(mContext).setImageResourceByAttr(ivLike, R.attr.bg_like);
                JinquanResourceHelper.getInstance(mContext).setTextColorByAttr(likeCount, R.attr.custom_attr_nickname_text_color);
            }
            if (StringUtil.isEmpty(feedInfo.text)) {
                textNews.setVisibility(View.GONE);
            } else {
                textNews.setVisibility(View.VISIBLE);
                textNews.setText(feedInfo.text);
            }
            if (StringUtil.isEmpty(feedInfo.nick_name)) {
                newsUserName.setVisibility(View.GONE);
            } else {
                newsUserName.setVisibility(View.VISIBLE);
                newsUserName.setText(feedInfo.nick_name);
            }
            if (StringUtil.isEmpty(feedInfo.create_time)) {
                newsTime.setVisibility(View.GONE);
            } else {
                newsTime.setVisibility(View.VISIBLE);
                newsTime.setText(feedInfo.create_time);
            }
            mu_image.setList(feedInfo.images);
            mu_image.setOnItemClickListener(new MultiImageView.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {
                    if (ViewUtils.isFastClick()) {
                        return;
                    }
                    PictureSelector.create((Activity) mContext)
                            .themeStyle(R.style.picture_default_style)
                            .isNotPreviewDownload(true)
                            .isPreviewVideo(true)
                            .imageEngine(GlideEngine.createGlideEngine())
                            .openExternalPreview(position, getSelectList(feedInfo.images));
                    mPresent.feedActionReport(feedId, Constants.FEED_ACTION_PICTURE, mFeedDetailsType);
                    HashMap<String, String> params = new HashMap<>();
                    params.put("female_guest_id", guestId);
                    params.put("moment_id", feedId);
                    params.put("create_ts",String.valueOf(System.currentTimeMillis()));
                    params.put("from", Constants.SOURCE_FEED_SQUARE_DETAIL.equals(mFeedDetailsType) ? Constants.TYPE_FEED_SQUARE : Constants.TYPE_FEED_PERSONAL);
                    MultiTracker.onEvent(TrackConstants.B_FEED_PIC_CLICK, params);
                }
            });
            if (commentAdapter != null) {
                commentAdapter.setDatas(bean.comment_list);
                if (bean.comment_list != null && bean.comment_list.size() > 0) {
                    divider.setText("");
                    rvCommentList.setVisibility(View.VISIBLE);
                    ll_comment_empty.setVisibility(View.GONE);
                } else {
                    divider.setText("");
                    rvCommentList.setVisibility(View.GONE);
                    ll_comment_empty.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    private List<LocalMedia> getSelectList(List<String> data) {
        List<LocalMedia> localMedia = new ArrayList<>();
        for (String photos : data) {
            localMedia.add(new LocalMedia(photos, 0, 0, ""));
        }
        return localMedia;
    }

    /**
     * 播放语音
     */
    private AnimationDrawable animation;

    private void startAudio(ImageView sound_news_anim, TextView sound_new_time, String voice) {
        sound_news_anim.setBackgroundResource(R.drawable.anim_user_audio_list);
        animation = (AnimationDrawable) sound_news_anim.getBackground();
        animation.start();
        if (!StringUtil.isEmpty(voice) && mMediaPlayer != null) {
            try {
                mMediaPlayer.reset();
                mMediaPlayer.setDataSource(voice);
                mMediaPlayer.prepare();
                mMediaPlayer.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (mTimer2 == null && mTask2 == null) {
            mTimer2 = new Timer();
            mTask2 = new TimerTask() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mAudioTimer--;
                            if (mAudioTimer <= 0) {
                                mAudioTimer = mAudioDuration;
                                endAudio(sound_news_anim, sound_new_time);
                            } else {
                                setAudioDuration(sound_new_time);
                            }
                        }
                    });
                }
            };
            mTimer2.schedule(mTask2, 0, 1000);
        }
    }

    /**
     * 暂停语音
     */
    private void endAudio(ImageView sound_news_anim, TextView sound_new_time) {
        if (sound_news_anim == null) {
            return;
        }
        if (animation != null) {
            animation.selectDrawable(0);
            animation.stop();
        }
        mAudioTimer = mAudioDuration;
        setAudioDuration(sound_new_time);
        releaseTimer();
    }

    private void setAudioDuration(TextView soundNewTime) {
        if (soundNewTime != null) {
            soundNewTime.setText(mAudioTimer + "s");
        }
    }

    private void releaseTimer() {
        if (mTask2 != null) {
            mTask2.cancel();
            mTask2 = null;
        }
        if (mTimer2 != null) {
            mTimer2.cancel();
            mTimer2 = null;
        }
    }

    @Override
    public void updateComment(CommentBean data) {
        if (commentAdapter != null && data != null) {
            List<CommentInfo> commentList = data.comment_list;
            commentAdapter.setDatas(commentList);
            if (rvCommentList != null && commentList != null) {
                divider.setText("");
                rvCommentList.setVisibility(View.VISIBLE);
                ll_comment_empty.setVisibility(View.GONE);
                commentCount.setText(String.valueOf(commentList.size()));
            }
        }
        EventBus.getDefault().post(new EventCenter(Constants.EVENT_UPDATE_PERSON_NEWS));
    }

    @Override
    public void updateCommentFail(String msg) {
        ToastUtils.s(this, msg);
    }

    @Override
    public void updateLikeResult() {
        if (feedInfo != null) {
            if (feedInfo.liked) {
                JinquanResourceHelper.getInstance(mContext).setImageResourceByAttr(ivLike, R.attr.bg_like);
                JinquanResourceHelper.getInstance(mContext).setImageResourceByAttr(likeTag, R.attr.bg_like);
                JinquanResourceHelper.getInstance(mContext).setTextColorByAttr(likeCount, R.attr.custom_attr_nickname_text_color);
                feedInfo.like_count--;
                if (feedInfo.like_count == 0) {
                    likeCount.setText("点赞");
                } else {
                    likeCount.setText(feedInfo.like_count + "");
                }
                feedInfo.liked = false;
            } else {
                likeTag.setImageResource(R.mipmap.ic_like_selected);
                ivLike.setImageResource(R.mipmap.ic_like_selected);
                feedInfo.like_count++;
                if (feedInfo.like_count == 0) {
                    likeCount.setText("点赞");
                } else {
                    likeCount.setText(feedInfo.like_count + "");
                }
                feedInfo.liked = true;
                likeCount.setTextColor(Color.parseColor("#F06078"));
            }
            EventBus.getDefault().post(new EventCenter(Constants.EVENT_UPDATE_PERSON_NEWS));
        }
    }

    @OnClick({R.id.comment_send, R.id.back, R.id.sound_news, R.id.iv_like, R.id.iv_like_tag, R.id.iv_feed_more, R.id.person_avatar,R.id.ll_comment})
    public void onClick(View view) {
        if (ViewUtils.isFastClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.comment_send:
                String text = inputComment.getText().toString().trim();
                if (mPresent != null) {
                    mPresent.uploadComment(text, feedId);
                    hideKeyBoard();
                }
                inputComment.setText("");
                HashMap<String, String> commentParams = new HashMap<>();
                commentParams.put("female_guest_id", guestId);
                commentParams.put("moment_id", feedId);
                commentParams.put("create_ts",String.valueOf(System.currentTimeMillis()));
                commentParams.put("from", Constants.SOURCE_FEED_SQUARE_DETAIL.equals(mFeedDetailsType) ? Constants.TYPE_FEED_SQUARE : Constants.TYPE_FEED_PERSONAL);
                MultiTracker.onEvent(TrackConstants.B_FEED_COMMENT_CLICK, commentParams);
                break;
            case R.id.back:
                finish();
                break;
            case R.id.sound_news:
                if (mMediaPlayer.isPlaying() && !isPause) {
                    isPause = true;
                    endAudio(soundNewsAnim, soundNewsTime);
                } else {
                    isPause = false;
                    startAudio(soundNewsAnim, soundNewsTime, voice);
                }
                mPresent.feedActionReport(feedId, Constants.FEED_ACTION_VOICE, mFeedDetailsType);
                HashMap<String, String> params = new HashMap<>();
                params.put("female_guest_id", guestId);
                params.put("moment_id", feedId);
                params.put("create_ts",String.valueOf(System.currentTimeMillis()));
                params.put("from", Constants.SOURCE_FEED_SQUARE_DETAIL.equals(mFeedDetailsType) ? Constants.TYPE_FEED_SQUARE : Constants.TYPE_FEED_PERSONAL);
                MultiTracker.onEvent(TrackConstants.B_FEED_VOICE_CLICK, params);
                break;
            case R.id.iv_like:
            case R.id.iv_like_tag:
                if (ViewUtils.isFastClick()) return;
                if (feedInfo != null) {
                    if (feedInfo.liked) {
                        mPresent.feedLike(feedId, 0, mFeedDetailsType);
                    } else {
                        mPresent.feedLike(feedId, 1, mFeedDetailsType);
                    }
                }
                HashMap<String, String> likeParams = new HashMap<>();
                likeParams.put("female_guest_id", guestId);
                likeParams.put("moment_id", feedId);
                likeParams.put("create_ts",String.valueOf(System.currentTimeMillis()));
                likeParams.put("from", Constants.SOURCE_FEED_SQUARE_DETAIL.equals(mFeedDetailsType) ? Constants.TYPE_FEED_SQUARE : Constants.TYPE_FEED_PERSONAL);
                MultiTracker.onEvent(TrackConstants.B_FEED_LIKE_CLICK, likeParams);
                break;
            case R.id.iv_feed_more:
                mFeedMoreDialogFragment.reportListener(new FeedMoreDialogFragment.Listener() {
                    @Override
                    public void onReport() {
                        if (feedInfo != null) {
                            Bundle bundle = new Bundle();
                            bundle.putString(Constants.USER_ID, feedInfo.user_id);
                            readyGo(ReportActivity.class, bundle);
                        }

                    }
                }).show(getSupportFragmentManager(), FeedMoreDialogFragment.class.getSimpleName());
                break;
            case R.id.person_avatar:
                if (Constants.SOURCE_FEED_SQUARE_DETAIL.equals(mFeedDetailsType)) {
                    if (feedInfo != null) {
                        Bundle bundle = new Bundle();
                        bundle.putString(Constants.USER_ID, feedInfo.user_id);
                        bundle.putString(Constants.GIFT_RANK_PATH, mFeedDetailsType);
                        readyGo(UserCenterActivity.class, bundle);
                    }
                    return;
                }
                EventBus.getDefault().post(new EventCenter(Constants.EVENT_UPDATE_LIST_POSITION));
                finish();
                break;
            case R.id.ll_comment:
                showKeyBoard(inputComment);
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresent.detachView();
    }
}
