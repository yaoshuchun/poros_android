package com.liquid.poros.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager.widget.ViewPager;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.liquid.base.utils.ToastUtil;
import com.liquid.poros.R;
import com.liquid.poros.adapter.GiftPagerAdapter;
import com.liquid.poros.analytics.utils.MultiTracker;
import com.liquid.poros.base.BaseActivity;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.entity.MicUserInfo;
import com.liquid.poros.entity.MsgUserModel;
import com.liquid.poros.utils.FileDownloadUtil;
import com.liquid.poros.utils.LottieAnimationUtils;
import com.liquid.poros.ui.activity.live.RoomLiveActivityV2;
import com.liquid.poros.ui.activity.live.viewmodel.RoomLiveViewModel;
import com.liquid.poros.ui.activity.user.SessionActivity;
import com.liquid.poros.ui.fragment.BagFragment;
import com.liquid.poros.ui.fragment.GiftSendFragment;
import com.liquid.poros.utils.SharePreferenceUtil;
import com.liquid.poros.utils.ViewUtils;
import com.liquid.poros.widgets.CustomSimplePagerTitleView;
import com.liquid.poros.widgets.SelectGiftNumView;
import com.liquid.poros.widgets.bottomdialog.ViewPagerBottomSheetBehavior;
import com.liquid.poros.widgets.bottomdialog.ViewPagerBottomSheetDialog;
import com.liquid.poros.widgets.bottomdialog.ViewPagerBottomSheetDialogFragment;
import com.luck.picture.lib.tools.ToastUtils;
import com.netease.neliveplayer.playerkit.common.net.NetworkUtil;
import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.ViewPagerHelper;
import net.lucode.hackware.magicindicator.buildins.UIUtil;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.SimplePagerTitleView;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class LiveMultiGiftSendDialog extends ViewPagerBottomSheetDialogFragment {
    public static final String GIFT_BEANS = "gift_beans";
    public static final String GIFT_NUMS = "gift_nums";
    public static final String FROM = "from";
    public static final String GIFT_SOURCE = "gift_source";
    public static final String MIC_USER_INFO = "MIC_USER_INFO";
    public static final String TO_ANCHOR = "to_anchor";
    public static final String SHOW_ADD_FRIEND_BUTTON = "show_add_friend_button";
    public static final String POS = "pos";
    public static final String ROOM_TYPE = "room_type";
    private RequestOptions options;
    private Context context;
    private CustomViewPager viewpagerContainer;
    private MagicIndicator magicIndicatorTab;
    private ImageView userAvatar;
    private TextView userName;
    private TextView coinCount;
    private LinearLayout showGiftCount;
    private ImageView recharge;
    private LinearLayout addFriendTip;
    private List<Fragment> fragments;
    private String from;
    private String giftSource;
    private MicUserInfo micUserInfo;
    private boolean toAnchor;
    private TextView btnSendGift;
    private boolean showAddFriendButton;
    private SelectGiftNumView giftNumPanel;
    private ArrayList<MicUserInfo> micUserInfos;
    private int giftType;
    private int pos;
    private boolean mIsBagEmpty;
    private int mRoomType;
    private String mRoomId;
    private String coin;
    private int selectGiftNum = 1;
    private TextView showGiftNum;
    private LinearLayout normalSendBtn;
    private ArrayList<MsgUserModel.GiftNums> giftNums;
    private List<String> viewPagerTitle;
    private CommonNavigatorAdapter commonNavigatorAdapter;
    private CommonNavigator commonNavigator;
    private GiftSendFragment fragmentOne;
    private BagFragment fragmentThree;
    public TextView giftLeftLive;
    public TextView giftRightLive;
    private int pagePos;
    private GiftPagerAdapter pagerAdapter;
    private RechargeCoinDialog rechargeCoinDialog;
    private String femaleGuestId;
    private ArrayList<MsgUserModel.GiftBean> giftBeans;
    private LinearLayout anotherSendBtn;
    private int giftSelectPos;
    private int retryCount;
    private String audienceFrom;
    private OnSendGiftClickListener listener;
    private int PK_STATUS_PK_DONGING = 0;
    private ImageView iv_help_box;

    public LiveMultiGiftSendDialog(Context context) {
        this.context = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        options = RequestOptions.bitmapTransform(new RoundedCorners(20));
        initData();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        ViewPagerBottomSheetDialog dialog = (ViewPagerBottomSheetDialog) super.onCreateDialog(savedInstanceState);
        View view = View.inflate(getContext(), R.layout.view_live_send_gift_multi, null);
        dialog.setContentView(view);
        //设置透明背景
        dialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);
        WindowManager.LayoutParams params = new WindowManager.LayoutParams();
        params.type =  1000;
        dialog.getWindow().setAttributes(params);
        View root = dialog.getDelegate().findViewById(R.id.design_bottom_sheet);
        ViewPagerBottomSheetBehavior behavior = ViewPagerBottomSheetBehavior.from(root);
        behavior.setHideable(false);
        initializeView(view);
        return dialog;
    }

    private void initializeView(View view) {
        viewpagerContainer = view.findViewById(R.id.viewpager_container);
        magicIndicatorTab = view.findViewById(R.id.magic_indicator_tab);
        coinCount = view.findViewById(R.id.coin_count);
        recharge = view.findViewById(R.id.recharge);
        userAvatar = view.findViewById(R.id.user_avatar);
        userName = view.findViewById(R.id.user_name);
        addFriendTip = view.findViewById(R.id.add_friend_tip);
        showGiftCount = view.findViewById(R.id.show_gift_count);
        giftNumPanel = view.findViewById(R.id.select_gift_num_panel);
        showGiftNum = view.findViewById(R.id.select_gift_num);
        btnSendGift = view.findViewById(R.id.btn_send_gift);
        normalSendBtn = view.findViewById(R.id.normal_send_btn);
        anotherSendBtn = view.findViewById(R.id.another_send_btn);
        giftLeftLive = view.findViewById(R.id.left_tv_live);
        giftRightLive = view.findViewById(R.id.right_tv_live);
        iv_help_box = view.findViewById(R.id.iv_help_box);
        selectGiftNum = 1;
        if (getActivity() instanceof RoomLiveActivityV2){
            RoomLiveActivityV2 activity = (RoomLiveActivityV2) getActivity();
            if (activity.hasBoost()){
                iv_help_box.setVisibility(View.VISIBLE);
            } else {
                iv_help_box.setVisibility(View.INVISIBLE);//这里要用INVISIBLE
            }
        }
        normalSendBtn.setVisibility(View.VISIBLE);
        anotherSendBtn.setVisibility(View.GONE);
        viewpagerContainer.setScanScroll(true);
        giftSelectPos = 0;
        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                if (giftNumPanel.getVisibility() == View.VISIBLE && !isTouchPointInView(giftNumPanel, event.getRawX(), event.getRawY())) {
                    giftNumPanel.setVisibility(View.GONE);
                }
                return true;
            }
        });
        btnSendGift.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendGift();
            }
        });
        recharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (giftNumPanel.getVisibility() == View.VISIBLE) {
                    giftNumPanel.setVisibility(View.GONE);
                }
                if (rechargeCoinDialog == null) {
                    rechargeCoinDialog = new RechargeCoinDialog();
                }
                ((RoomLiveActivityV2)context).getSupportFragmentManager().beginTransaction().remove(rechargeCoinDialog).commit();
                rechargeCoinDialog.setPositon(RechargeCoinDialog.POSITION_GIFT_ALERT);
                rechargeCoinDialog.show(((RoomLiveActivityV2)context).getSupportFragmentManager(), RechargeCoinDialog.class.getName());
                rechargeCoinDialog.setWindowFrom("gift_jump_to_recharge");
            }
        });
        giftNumPanel.setListener(new SelectGiftNumView.OnGiftNumItemSelectListener() {
            @Override
            public void numSelect(int num) {
                if (pagePos == 1 && fragmentThree != null) {
                    fragmentThree.setGiftNum(num);
                    giftLeftLive.setText(fragmentThree.getSpan());
                }else {
                    selectGiftNum = num;
                    showGiftNum.setText(String.valueOf(num));
                }
            }
        });
        showGiftCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (giftNumPanel.getVisibility() == View.VISIBLE) {
                    giftNumPanel.setVisibility(View.GONE);
                }else {
                    giftNumPanel.setVisibility(View.VISIBLE);
                }
            }
        });
        giftLeftLive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (giftType == 4 || giftType == 5) {
                    if (giftNumPanel.getVisibility() == View.VISIBLE) {
                        giftNumPanel.setVisibility(View.GONE);
                    }else {
                        giftNumPanel.setVisibility(View.VISIBLE);
                    }
                }
            }
        });

        viewpagerContainer.setOffscreenPageLimit(fragments.size());
        viewpagerContainer.setAdapter(pagerAdapter);
        initMagicIndicator();
        initListener();
        giftNumPanel.setData(giftNums);
        coinCount.setText(coin);
        if (showAddFriendButton) {
            addFriendTip.setVisibility(View.VISIBLE);
        } else {
            addFriendTip.setVisibility(View.GONE);
        }
        if (userName != null && micUserInfo != null) {
            userName.setText(micUserInfo.getNick_name());
            if (userAvatar != null) {
                Glide.with(context).applyDefaultRequestOptions(options).load(micUserInfo.getAvatar_url()).into(userAvatar);
            }
        }
    }

    public void setAudienceFrom(String audienceFrom) {
        if (fragmentThree != null) {
            fragmentThree.setAudienceFrom(audienceFrom);
        }
        this.audienceFrom = audienceFrom;
    }

    public void setShowAddFriendButton(boolean isShowAddFriendButton) {
        if (addFriendTip == null) return;
        if (isShowAddFriendButton) {
            addFriendTip.setVisibility(View.VISIBLE);
        }else {
            addFriendTip.setVisibility(View.GONE);
        }
    }

    public void sendGift () {
        if (giftNumPanel.getVisibility() == View.VISIBLE) {
            giftNumPanel.setVisibility(View.GONE);
        }
        if (ViewUtils.isFastClick()) {
            ToastUtils.s(context, "点击太过频繁");
            return;
        }
        if (!NetworkUtil.isNetAvailable(context)) {
            Toast.makeText(context, getContext().getString(R.string.network_not_available), Toast.LENGTH_SHORT).show();
            return;
        }
        MsgUserModel.GiftBean giftBean = giftBeans.get(giftSelectPos);
        String giftFileName = LottieAnimationUtils.INSTANCE.getGiftFileName(giftBean.id);
        if(!new File(FileDownloadUtil.INSTANCE.getParentPath() + File.separator + giftFileName).exists()){
            ToastUtil.show("请稍等，正在下载资源");
            MultiTracker.onEvent(TrackConstants.GIFT_RESOURCE_UNDOWNLOAD,new HashMap<String,String>());
            retryCount++;
            if (retryCount >= 3){
                FileDownloadUtil.INSTANCE.download(giftFileName,null);
                retryCount = 0;
            }
            return;
        }
        //如果是视频房Activity 则走最新的逻辑
        if (context instanceof RoomLiveActivityV2) {
            if (mRoomType == RoomLiveActivityV2.ROOM_TYPE_PK) {
                ((BaseActivity)context).getActivityViewModel(RoomLiveViewModel.class).sendGift(mRoomId, micUserInfo.getUser_id(), giftBean.id,giftBean.name, selectGiftNum, "anchor".equals(micUserInfo.getRoom_role()),pos,false);
            } else {
                ((BaseActivity)context).getActivityViewModel(RoomLiveViewModel.class).sendGift(mRoomId, micUserInfo.getUser_id(), giftBean.id,giftBean.name, selectGiftNum, "anchor".equals(micUserInfo.getRoom_role()), -1,true);
            }
            trackSendGift(giftBean.id, giftBean.name,pos,selectGiftNum, 0);
        }
        if (listener != null) {
            if (giftBean != null) {
                MsgUserModel.EffectBean effect = giftBean.effect;
                String audio = null;
                String json = null;
                if (effect != null) {
                    audio = effect.audio;
                    json = effect.json;
                }
                listener.sendGift(giftBean.id, giftBean.icon, selectGiftNum, audio,
                        giftBean.name, json,pos);
            }
        }
    }

    interface OnSendGiftClickListener {
        void sendGift(int giftId,String icon,int num,String audio,String name,String json,int toPos);

    }

    private void trackSendGift(int giftId,String name,int toPos,int giftNum,int pkStatus) {
        HashMap<String,String> params = new HashMap<String, String>();
        params.put("room_id",mRoomId);
        params.put("anchor_id",micUserInfos.get(0).getUser_id());
        if (mRoomType == RoomLiveActivityV2.ROOM_TYPE_PK) {
            if (micUserInfos.get(1) != null) {
                params.put("left_female_guest_id",micUserInfos.get(1).getUser_id());
            }
            if (micUserInfos.get(2) != null) {
                params.put("right_female_guest_id",micUserInfos.get(2).getUser_id());
            }
            params.put("room_type","pk_room");
        }
        params.put("audience_from",audienceFrom);
        params.put("gift_id",String.valueOf(giftId));
        params.put("gift_name",name);
        params.put("gift_num",String.valueOf(giftNum));
        params.put("pk_status",pkStatus == PK_STATUS_PK_DONGING? "1" : "2");
        params.put("send_to",toPos == 0 ? "anchor" : "guest");
        MultiTracker.onEvent(TrackConstants.B_USER_SEND_GIFT_CLICK, params);
    }

    private boolean isTouchPointInView(SelectGiftNumView view, float x, float y) {
        if (view == null) {
            return false;
        }
        int[] location = new int[2];
        view.getLocationOnScreen(location);
        int left = location[0]-1000;
        int top = location[1];
        int right = left + view.getMeasuredWidth() + 1000;
        int bottom = top + view.getMeasuredHeight() + 1000;
        return y >= top && y <= bottom && x >= left && x <= right;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public LiveMultiGiftSendDialog show(LiveMultiGiftSendDialog fragment, FragmentActivity activity,
                     String tag, ArrayList<MsgUserModel.GiftBean> giftBeans,
                     ArrayList<MsgUserModel.GiftNums> giftNums,
                     String sessionId,
                     String from, String giftSource,
                     ArrayList<MicUserInfo> micUserInfos,
                     boolean toAnchor, boolean showAddFriendButton,
                     int pos, int mRoomType) {
        if (activity.isDestroyed() || activity.isFinishing()) return null;
        Bundle bundle = new Bundle();
        bundle.putSerializable(GIFT_BEANS,giftBeans);
        bundle.putSerializable(GIFT_NUMS,giftNums);
        bundle.putString(Constants.FEMALE_GUEST_ID,sessionId);
        bundle.putString(FROM,from);
        bundle.putString(GIFT_SOURCE,giftSource);
        bundle.putSerializable(MIC_USER_INFO,micUserInfos);
        bundle.putBoolean(TO_ANCHOR,toAnchor);
        bundle.putBoolean(SHOW_ADD_FRIEND_BUTTON,showAddFriendButton);
        bundle.putInt(POS,pos);
        bundle.putInt(ROOM_TYPE,mRoomType);
        fragment.setArguments(bundle);
        fragment.show(activity.getSupportFragmentManager(),tag);
        return fragment;
    }

    public void sendGift(int giftId,String icon, int num,String audio,String name,String json, int toPos) {
        if (context != null) {
            ((SessionActivity)context).sendGift(giftId,icon,num,audio,name,json,toPos);
        }
    }

    public void setCurrentItem(int pos) {
        if (viewpagerContainer != null) {
            viewpagerContainer.setCurrentItem(pos);
        }
    }

    public void updateCoinCount(String coin){
        this.coin = coin;
        if (coinCount != null) {
            coinCount.setText(coin);
        }
    }

    public void addCoinCount(String addCoin){
        this.coin = addCoinString(addCoin);
        if (coinCount != null) {
            coinCount.setText(coin);
        }
    }

    private String addCoinString(String addCoin) {
        return (Integer.valueOf(coin) + Integer.valueOf(addCoin)) + "";
    }

    public void setRoomId(String roomId) {
        mRoomId = roomId;
    }

    private void initData() {
        Bundle arguments = getArguments();
        if (arguments != null) {
            giftBeans = (ArrayList<MsgUserModel.GiftBean>) arguments.get(GIFT_BEANS);
            giftNums = (ArrayList<MsgUserModel.GiftNums>) arguments.get(GIFT_NUMS);
            from = arguments.getString(FROM);
            giftSource = arguments.getString(GIFT_SOURCE);
            toAnchor = arguments.getBoolean(TO_ANCHOR);
            showAddFriendButton = arguments.getBoolean(SHOW_ADD_FRIEND_BUTTON);
            micUserInfos = (ArrayList<MicUserInfo>) arguments.getSerializable(MIC_USER_INFO);
            pos = arguments.getInt(POS);
            mRoomType = arguments.getInt(ROOM_TYPE);
            femaleGuestId = arguments.getString(Constants.FEMALE_GUEST_ID);
            fragments = new ArrayList<>();
            viewPagerTitle = new ArrayList<>();
            viewPagerTitle.add("礼物");
            viewPagerTitle.add("背包");
            if (micUserInfos != null &&  micUserInfos.size() > pos) {
                micUserInfo = micUserInfos.get(pos);
            }
            Bundle params = new Bundle();
            params.putSerializable(GIFT_NUMS,giftNums);
            params.putSerializable(GIFT_BEANS,giftBeans);
            Bundle paramsThree = new Bundle();
            paramsThree.putSerializable(GIFT_NUMS,giftNums);
            paramsThree.putString(Constants.FEMALE_GUEST_ID,femaleGuestId);
            fragmentOne = GiftSendFragment.newInstance(params);
            fragmentThree = BagFragment.Companion.newInstance(paramsThree);
            fragmentThree.setRoomId(mRoomId);
            fragmentThree.setAudienceFrom(audienceFrom);
            fragmentThree.setMicUserInfos(micUserInfos);
            fragmentThree.setContentGpVisibility(false);
            fragmentOne.setGiftPresenterVisibly(false);
            fragmentOne.setRoomId(mRoomId);
            MicUserInfo[] micUserInfos = this.micUserInfos.toArray(new MicUserInfo[this.micUserInfos.size()]);
            fragmentOne.setUserInfo(micUserInfos,toAnchor,showAddFriendButton,pos,mRoomType);
            fragmentOne.setListener(new GiftSendFragment.OnTouchListener() {
                @Override
                public void onTouch() {
                    if (giftNumPanel.getVisibility() == View.VISIBLE) {
                        giftNumPanel.setVisibility(View.GONE);
                    }
                }

                @Override
                public void giftSelect(int selectPos) {
                    if (giftNumPanel.getVisibility() == View.VISIBLE) {
                        giftNumPanel.setVisibility(View.GONE);
                    }
                    giftSelectPos = selectPos;
                }
            });
            fragmentThree.setListener(new BagFragment.OnBagSelectListener() {
                @Override
                public void bagIsEmpty(boolean isEmpty) {
                    mIsBagEmpty = isEmpty;
                    if (isEmpty) {
                        anotherSendBtn.setVisibility(View.GONE);
                    }else {
                        anotherSendBtn.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void giftTypeSelect(int type) {
                    giftType = type;
                }
            });
            fragments.add(fragmentOne);
            fragments.add(fragmentThree);
            pagerAdapter = new GiftPagerAdapter(getChildFragmentManager(),fragments,viewPagerTitle);
        }
    }

    private void initListener() {
        viewpagerContainer.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if (giftNumPanel != null && giftNumPanel.getVisibility() == View.VISIBLE){
                    giftNumPanel.setVisibility(View.GONE);
                }
            }

            @Override
            public void onPageSelected(int position) {
                pagePos = position;
                if (position == 0) {
                    normalSendBtn.setVisibility(View.VISIBLE);
                    anotherSendBtn.setVisibility(View.GONE);
                }else if (position == 1){
                    bagPageExposure();
                    normalSendBtn.setVisibility(View.GONE);
                    if (mIsBagEmpty) {
                        anotherSendBtn.setVisibility(View.GONE);
                    }else {
                        anotherSendBtn.setVisibility(View.VISIBLE);
                    }

                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        iv_help_box.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() instanceof RoomLiveActivityV2){
                    RoomLiveActivityV2 activity = (RoomLiveActivityV2) getActivity();
                    //没有点击过，进行助力引导
                    if (!SharePreferenceUtil.getBoolean(SharePreferenceUtil.FILE_USER_ACCOUNT_DATA, SharePreferenceUtil.KEY_GUIDE_HELP_BOX, false)
                            && activity.isGiftSwitchVisiable()) {
                        activity.showHelpGuide();
                        SharePreferenceUtil.saveBoolean(SharePreferenceUtil.FILE_USER_ACCOUNT_DATA, SharePreferenceUtil.KEY_GUIDE_HELP_BOX, true);
                    } else {//已经点击过，打开助力抽奖弹窗
                        ((RoomLiveActivityV2)getActivity() ).showBoostLotteryDialog();
                        HashMap<String, String> params = new HashMap<>();
                        params.put("anchor_id", activity.getAnchorId());
                        MultiTracker.onEvent(TrackConstants.B_GIFT_TOP_TREASURE_BOX_ENTRANCE_CLICK, params);
                    }
                    dismiss();
                }
            }
        });
    }

    //背包页面曝光
    private void bagPageExposure() {
        HashMap<String,String> params = new HashMap<>();
        params.put("anchor_id",micUserInfos.get(0).getUser_id());
        MultiTracker.onEvent(TrackConstants.B_BAG_PAGE_EXPOSURE,params);
    }

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    private void initMagicIndicator() {
        commonNavigatorAdapter = new CommonNavigatorAdapter() {
            @Override
            public int getCount() {
                return viewPagerTitle == null ? 0 : viewPagerTitle.size();
            }

            @Override
            public IPagerTitleView getTitleView(Context context, int index) {
                SimplePagerTitleView simplePagerTitleView = new CustomSimplePagerTitleView(context);
                simplePagerTitleView.setText(viewPagerTitle.get(index));
                if (index == 0) {
                    simplePagerTitleView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                }else {
                    simplePagerTitleView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
                }
                simplePagerTitleView.setNormalColor(getResources().getColor(R.color.color_ccffffff));
                simplePagerTitleView.setSelectedColor(getResources().getColor(R.color.color_10D8C8));
                simplePagerTitleView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (index == 1) {
                            bagEnterClick();
                        }
                        viewpagerContainer.setCurrentItem(index);
                    }
                });
                int padding = UIUtil.dip2px(context, 8);
                simplePagerTitleView.setPadding(padding, 0, padding, 0);
                return simplePagerTitleView;
            }

            @Override
            public IPagerIndicator getIndicator(Context context) {
                return null;
            }
        };
        commonNavigator = new CommonNavigator(context);
        commonNavigator.setAdjustMode(false);
        commonNavigator.setAdapter(commonNavigatorAdapter);
        magicIndicatorTab.setNavigator(commonNavigator);
        ViewPagerHelper.bind(magicIndicatorTab,viewpagerContainer);
    }

    //背包入口点击埋点
    private void bagEnterClick() {
        HashMap<String,String> params = new HashMap<>();
        params.put("anchor_id",micUserInfos.get(0).getUser_id());
        MultiTracker.onEvent(TrackConstants.B_BAG_ENTRANCE_CLICK,params);
    }
}
