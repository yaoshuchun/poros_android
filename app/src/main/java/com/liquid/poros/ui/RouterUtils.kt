package com.liquid.poros.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.liquid.poros.ui.activity.live.RoomLiveActivityV2

/**
 * Activity跳转统一类
 */
object RouterUtils {

    /**
     * 跳转到视频房
     */
    @JvmStatic
    fun goToRoomLiveActivity(context: Context, bundle: Bundle) {
        val intent = Intent(context, RoomLiveActivityV2::class.java)
        intent.putExtras(bundle)
        context.startActivity(intent)
    }
}