package com.liquid.poros.ui.dialog;


import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.ActivityUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.faceunity.nama.fromapp.utils.ToastUtil;
import com.liquid.base.utils.ContextUtils;
import com.liquid.im.kit.custom.DaoliuAttachment;
import com.liquid.poros.R;
import com.liquid.poros.analytics.utils.MultiTracker;
import com.liquid.poros.base.BaseDialogFragment;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.constant.DataOptimize;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.ui.RouterUtils;
import com.liquid.poros.ui.activity.live.RoomLiveActivityV2;
import com.liquid.poros.ui.dialog.viewmodel.DialogViewModel;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.poros.utils.MyActivityManager;
import com.liquid.poros.utils.ScreenUtil;
import com.liquid.poros.utils.StringUtil;
import com.liquid.poros.utils.glide.RoundedCornersTransform;
import com.luck.picture.lib.tools.ToastUtils;

import java.util.HashMap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.liquid.poros.constant.Constants.TYPE_CP_GROUP_INVITE;
import static com.liquid.poros.constant.Constants.TYPE_DIVERSION;

/**
 * 视频房邀请弹窗
 */
public class LeadVideoRoomFragment extends BaseDialogFragment {

    String title;
    String desc;
    String invite_id;
    String im_room_id;
    String room_id;

    DaoliuAttachment daoliuAttachment;
    @BindView(R.id.layout_head)
    RelativeLayout layoutHead;
    @BindView(R.id.tv_cp_desc)
    TextView tvCpDesc;
    @BindView(R.id.layout_diversion)
    RelativeLayout layoutDiversion;
    @BindView(R.id.cancel_tv)
    TextView cancelTv;
    @BindView(R.id.confirm_tv)
    TextView confirmTv;
    @BindView(R.id.iv_head_icon)
    ImageView ivHeadIcon;
    @BindView(R.id.tv_girl_title)
    TextView tvTitle;
    @BindView(R.id.tv_desc)
    TextView tvDesc;
    @BindView(R.id.iv_gender)
    ImageView genderIv;
    @BindView(R.id.tv_age)
    TextView tvAge;

    @BindView(R.id.layout_girl_show)
    RelativeLayout layout_girl_show;
    @BindView(R.id.layout_normal_show)
    RelativeLayout layout_normal_show;
    @BindView(R.id.tv_normal_title)
    TextView tv_normal_title;
    @BindView(R.id.iv_zuoyinhao)
    ImageView iv_zuoyinhao;
    @BindView(R.id.iv_youyinhao)
    ImageView iv_youyinhao;
    @BindView(R.id.ll_desc)
    LinearLayout descLl;
    @BindView(R.id.ll_master_information)
    LinearLayout masterInformationLl;
    @BindView(R.id.iv_master_avatar)
    ImageView masterAvatarIv;
    @BindView(R.id.tv_master_nickname)
    TextView masterNicknameTv;
    private Activity context;

    private DialogViewModel dialogViewModel;


    public static LeadVideoRoomFragment newInstance(String groupId) {
        LeadVideoRoomFragment fragment = new LeadVideoRoomFragment();
        Bundle bundle = new Bundle();
        bundle.putString(Constants.GROUP_ID, groupId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected String getPageId() {
        return null;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        Window win = getDialog().getWindow();
        // 一定要设置Background，如果不设置，window属性设置无效
        win.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);

        WindowManager.LayoutParams params = win.getAttributes();
        params.gravity = Gravity.CENTER;
        params.width = ScreenUtil.getDisplayWidth() * 3 / 4;
        setCancelable(false);
//         使用ViewGroup.LayoutParams，以便Dialog 宽度充满整个屏幕
        win.setAttributes(params);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_lead_video_room, null);
        builder.setView(view);
        ButterKnife.bind(this, view);
        setCancelable(false);


        if (daoliuAttachment != null) {
            title = daoliuAttachment.getMain_title();
            desc = daoliuAttachment.getSub_title();
            invite_id = daoliuAttachment.getInvite_id();
            room_id = daoliuAttachment.getRoom_id();
            im_room_id = daoliuAttachment.getIm_room_id();

            String type = daoliuAttachment.getType();
            if (StringUtil.isEquals(type, TYPE_DIVERSION) || StringUtil.isEquals(type, TYPE_CP_GROUP_INVITE)) {//主持人邀请弹窗
                layout_girl_show.setVisibility(View.VISIBLE);
                layout_normal_show.setVisibility(View.GONE);


                tvTitle.setText(daoliuAttachment.getNike_name());
                tvDesc.setText(daoliuAttachment.getGame());
                if (daoliuAttachment.isFemale()) {
                    tvAge.setText(daoliuAttachment.getAge());
                    if (!TextUtils.isEmpty(daoliuAttachment.getAnchor_avatar_url()) && !TextUtils.isEmpty(daoliuAttachment.getAnchor_nick_name())) {
                        descLl.setVisibility(View.GONE);
                        masterInformationLl.setVisibility(View.VISIBLE);
                        Glide.with(context).load(daoliuAttachment.getAnchor_avatar_url()).circleCrop().into(masterAvatarIv);
                        masterNicknameTv.setText(daoliuAttachment.getAnchor_nick_name());
                    }


                } else {
                    descLl.setVisibility(View.VISIBLE);
                    masterInformationLl.setVisibility(View.GONE);
                    tvAge.setText("主持人");
                    genderIv.setVisibility(View.GONE);
                }

                if (StringUtil.isNotEmpty(daoliuAttachment.getCouple_desc())) {
                    tvCpDesc.setText(daoliuAttachment.getCouple_desc());
                    tvCpDesc.setTextColor(Color.parseColor("#181E25"));
                } else {
                    tvCpDesc.setText("TA还没有书写心目中的CP");
                    tvCpDesc.setTextColor(Color.parseColor("#A4A9B3"));
                }


                RoundedCornersTransform transform = new RoundedCornersTransform(ContextUtils.getApplicationContext(), ScreenUtil.dip2px(24));
                transform.setNeedCorner(true, true, false, false);
                RequestOptions roomOptions = new RequestOptions()
                        .transform(transform);
                Glide.with(ContextUtils.getApplicationContext()).load(daoliuAttachment.getAvatar_url()).apply(roomOptions).into(ivHeadIcon);

                HashMap<String, String> msgParams = new HashMap<>();
                msgParams.put("flow_invite_id", daoliuAttachment.getDiversion_id());
                MultiTracker.onEvent("b_flow_alert_exposure", msgParams);

            } else {//普通弹窗
                layout_girl_show.setVisibility(View.GONE);
                layout_normal_show.setVisibility(View.VISIBLE);

                RoundedCornersTransform transform = new RoundedCornersTransform(ContextUtils.getApplicationContext(), ScreenUtil.dip2px(24));
                transform.setNeedCorner(true, true, false, false);
                RequestOptions roomOptions = new RequestOptions()
                        .transform(transform);
                Glide.with(ContextUtils.getApplicationContext()).load(daoliuAttachment.getAvatar_url()).apply(roomOptions).into(ivHeadIcon);

                tv_normal_title.setText(daoliuAttachment.getMain_title());
                tvCpDesc.setText(daoliuAttachment.getSub_title());


                HashMap<String, String> msgParams = new HashMap<>();
                msgParams.put("user_id", AccountUtil.getInstance().getUserId());
                msgParams.put("room_id", room_id);
                try {
                    FragmentActivity activity = (FragmentActivity) MyActivityManager.getInstance().getCurrentActivity();
                    if (activity != null) {
                        msgParams.put("activity", activity.getClass().getSimpleName());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                MultiTracker.onEvent("b_video_room_invite_alert_exposure", msgParams);
            }
            if (StringUtil.isEquals(daoliuAttachment.getType(), TYPE_CP_GROUP_INVITE)) {
                HashMap params = new HashMap<String, String>();
                params.put("invite_id", daoliuAttachment.getInvite_id());
                params.put("room_id", daoliuAttachment.getRoom_id());
                params.put("from", "super_cp_team");
                params.put("is_in_room", String.valueOf(0));
                MultiTracker.onEvent(TrackConstants.B_INVITE_MIC_SHOW, params);
            }
        }


        dialogViewModel = getDialogFragmentViewModel(DialogViewModel.class);

        dialogViewModel.getRoomInfoDataSuccess().observe(this, RoomLiveInfo -> {
            if (RoomLiveInfo != null && RoomLiveInfo.getRoom_type() == RoomLiveActivityV2.ROOM_TYPE_PRIVATE) {
                ToastUtils.s(ContextUtils.getApplicationContext(), "此房间是专属房，暂无法加入");
                dismissAllowingStateLoss();
            } else {
                gotoLiveHome();
                if (StringUtil.isEquals(daoliuAttachment.getType(), TYPE_CP_GROUP_INVITE)) {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("anchor_id", RoomLiveInfo.getAnchor_id());
                    params.put("guest_id", RoomLiveInfo.getGirl_id());
                    params.put("from", "super_cp_team");
                    params.put("is_in_room", String.valueOf(0));
                    MultiTracker.onEvent(TrackConstants.B_ACCEPT_CP_INVITE_SUCCESS, params);
                }
            }
        });
        dialogViewModel.getRoomInfoDataFail().observe(this, RoomLiveInfo -> {
            ToastUtil.showToast(ContextUtils.getApplicationContext(), RoomLiveInfo.getMsg());
        });

        return builder.create();
    }

    @OnClick({R.id.cancel_tv, R.id.confirm_tv})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cancel_tv:
                if (StringUtil.isEquals(daoliuAttachment.getType(), TYPE_DIVERSION)) {//主持人邀请弹窗
                    HashMap<String, String> msgParams2 = new HashMap<>();
                    msgParams2.put("flow_invite_id", daoliuAttachment.getDiversion_id());
                    MultiTracker.onEvent("b_flow_alert_cancel_click", msgParams2);
                } else {
                    HashMap<String, String> msgParams = new HashMap<>();
                    msgParams.put("user_id", AccountUtil.getInstance().getUserId());
                    msgParams.put("room_id", room_id);
                    msgParams.put("option", "cancel");
                    MultiTracker.onEvent("b_video_room_invite_alert_click", msgParams);
                }

                dismissAllowingStateLoss();
                break;
            case R.id.confirm_tv:
                dialogViewModel.getLiveRoomInfo(room_id);
                if (StringUtil.isEquals(daoliuAttachment.getType(), TYPE_CP_GROUP_INVITE)) {
                    HashMap<String, String> msgParams = new HashMap<>();
                    msgParams.put("user_id", AccountUtil.getInstance().getUserId());
                    msgParams.put("room_id", room_id);
                    msgParams.put("invite_id", daoliuAttachment.getInvite_id());
                    msgParams.put("event_time", String.valueOf(System.currentTimeMillis()));
                    msgParams.put("is_in_room", String.valueOf(0));
                    msgParams.put("from", "super_cp_team");
                    msgParams.put("guest_id", daoliuAttachment.getGuest_id());
                    MultiTracker.onEvent(TrackConstants.B_ACCEPT_CP_INVITE, msgParams);
                }
                break;
        }
    }

    private void gotoLiveHome() {
        try {
            Activity activity = ActivityUtils.getTopActivity();
            if (activity != null) {
                if (activity instanceof RoomLiveActivityV2) {
                    String mRoomid = ((RoomLiveActivityV2) activity).getMRoomId();
                    if (!StringUtil.isEquals(mRoomid, room_id)) {
                        activity.finish();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (daoliuAttachment != null) {
//            Intent intent = new Intent(context, RoomLiveActivityV2.class);
            Bundle liveBundle = new Bundle();
            liveBundle.putString(Constants.ROOM_ID, room_id);
            liveBundle.putString(Constants.IM_ROOM_ID, im_room_id);
            liveBundle.putString(Constants.IM_ROOM_SOURCE, daoliuAttachment.getSource());
            liveBundle.putString(DaoliuAttachment.INVITE_ID, daoliuAttachment.getInvite_id());
            if (StringUtil.isEquals(daoliuAttachment.getType(), TYPE_DIVERSION)) {//主持人邀请弹窗
                if (TextUtils.isEmpty(daoliuAttachment.getInvite_id())) {
                    liveBundle.putString(DaoliuAttachment.INVITE_ID, daoliuAttachment.getDiversion_id());
                } else {
                    liveBundle.putString(DaoliuAttachment.INVITE_ID, daoliuAttachment.getInvite_id());
                }
                liveBundle.putString(Constants.AUDIENCE_FROM, DataOptimize.CALL_CARD);
                HashMap<String, String> msgParams2 = new HashMap<>();
                msgParams2.put("flow_invite_id", daoliuAttachment.getDiversion_id());
                MultiTracker.onEvent("b_flow_alert_goto_see_click", msgParams2);
            } else {
                HashMap<String, String> msgParams2 = new HashMap<>();
                msgParams2.put("user_id", AccountUtil.getInstance().getUserId());
                msgParams2.put("room_id", room_id);
                msgParams2.put("option", "go_video_room");
                MultiTracker.onEvent("b_video_room_invite_alert_click", msgParams2);
                liveBundle.putString(DaoliuAttachment.INVITE_ID, invite_id);
                if (StringUtil.isEquals(daoliuAttachment.getType(), TYPE_CP_GROUP_INVITE)) {
                    liveBundle.putString(Constants.AUDIENCE_FROM, "cp_call");
                } else {
                    liveBundle.putString(Constants.AUDIENCE_FROM, DataOptimize.VIDEO_ACTIVITY);
                }

            }
//            intent.putExtras(liveBundle);
//            context.startActivity(intent);
            RouterUtils.goToRoomLiveActivity(getContext(), liveBundle);

            dismissAllowingStateLoss();
        }
    }

    public LeadVideoRoomFragment setAttachment(DaoliuAttachment daoliuAttachment) {
        this.daoliuAttachment = daoliuAttachment;
        return this;
    }

    public LeadVideoRoomFragment setContext(Activity activity) {
        this.context = activity;
        return this;
    }
//    public LeadVideoRoomFragment setInviteId(String invite_id) {
//        this.invite_id = invite_id;
//        return this;
//    }
//
//    public LeadVideoRoomFragment setRoomId(String im_room_id, String room_id) {
//        this.im_room_id = im_room_id;
//        this.room_id = room_id;
//        return this;
//    }

}
