package com.liquid.poros.ui.fragment.home;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.liquid.base.adapter.CommonAdapter;
import com.liquid.base.adapter.CommonViewHolder;
import com.liquid.poros.R;
import com.liquid.poros.base.BaseDialogFragment;
import com.liquid.poros.analytics.utils.MultiTracker;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.entity.PrizeCoinInfo;
import com.liquid.poros.utils.ScreenUtil;
import com.liquid.poros.widgets.GridSpacingItemDecoration;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * 视频上麦卡奖励弹窗界面
 */
public class PrizeVideoCardDialogFragment extends BaseDialogFragment implements View.OnClickListener {
    private TextView tv_receive_reward;
    private RecyclerView rv_video_card_reward;

    private List<PrizeCoinInfo.PrizeItem> mPrizeCoin;

    @Override
    protected String getPageId() {
        return TrackConstants.PAGE_VIDEO_CARD_REWARD;
    }

    public static PrizeVideoCardDialogFragment newInstance() {
        PrizeVideoCardDialogFragment fragment = new PrizeVideoCardDialogFragment();
        return fragment;
    }

    public PrizeVideoCardDialogFragment setPrizeCoin(List<PrizeCoinInfo.PrizeItem> prizeCoin) {
        this.mPrizeCoin = prizeCoin;
        return this;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onStart() {
        super.onStart();
        Window win = getDialog().getWindow();
        // 一定要设置Background，如果不设置，window属性设置无效
        win.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams params = win.getAttributes();
        params.gravity = Gravity.CENTER;
        setCancelable(false);
        params.width = (int) (ScreenUtil.getDisplayWidth() * 0.79);
        win.setAttributes(params);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_prize_video_card_dialog, null);
        tv_receive_reward = view.findViewById(R.id.tv_receive_reward);
        rv_video_card_reward = view.findViewById(R.id.rv_video_card_reward);
        if (mPrizeCoin != null) {
            rv_video_card_reward.setLayoutManager(new GridLayoutManager(getActivity(), 2));
            rv_video_card_reward.addItemDecoration(new GridSpacingItemDecoration(2, ScreenUtil.dip2px(10), false));
            CommonAdapter<PrizeCoinInfo.PrizeItem> adapter = new CommonAdapter<PrizeCoinInfo.PrizeItem>(getActivity(), R.layout.item_prize_video_card) {
                @Override
                public void convert(CommonViewHolder holder, PrizeCoinInfo.PrizeItem prizeItem, int pos) {
                    ImageView iv_video_reward = holder.getView(R.id.iv_video_reward);
                    TextView tv_video_reward_title = holder.getView(R.id.tv_video_reward_title);
                    TextView tv_video_reward_tip = holder.getView(R.id.tv_video_reward_tip);
                    tv_video_reward_title.setText(prizeItem.getCard_str());
                    tv_video_reward_tip.setText(prizeItem.getDesc());
                    RequestOptions options = new RequestOptions()
                            .placeholder(R.mipmap.img_default_avatar)
                            .override(LinearLayout.LayoutParams.MATCH_PARENT, ScreenUtil.dip2px(72))
                            .error(R.mipmap.img_default_avatar);
                    Glide.with(getActivity()).load(prizeItem.getImage_url()).apply(options).into(iv_video_reward);
                }
            };
            adapter.update(mPrizeCoin);
            rv_video_card_reward.setAdapter(adapter);
        }
        tv_receive_reward.setOnClickListener(this);
        builder.setView(view);
        return builder.create();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_receive_reward:
                dismissAllowingStateLoss();
                MultiTracker.onEvent(TrackConstants.B_VIDEO_CARD_REWARD);
                break;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


}
