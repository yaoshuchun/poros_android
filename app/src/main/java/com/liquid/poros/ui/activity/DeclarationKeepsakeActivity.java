package com.liquid.poros.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.liquid.base.tools.GT;
import com.liquid.base.utils.ContextUtils;
import com.liquid.poros.R;
import com.liquid.poros.base.BaseActivity;
import com.liquid.poros.analytics.utils.MultiTracker;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.entity.CpLevelUpDetail;
import com.liquid.poros.ui.activity.account.RechargeActivity;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.poros.utils.StringUtil;
import com.liquid.poros.utils.ViewUtils;
import com.liquid.poros.utils.network.HttpCallback;
import com.liquid.poros.utils.network.RetrofitHttpManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import okhttp3.RequestBody;

/**
 * cp信物 页面
 */
public class DeclarationKeepsakeActivity extends BaseActivity {

    String et_string;
    String female_user_id;
    String female_user_head;

    private String rise_to;
    private String intimacy;

    @BindView(R.id.target_user_head)
    ImageView target_user_head;
    @BindView(R.id.target_user_gift_img)
    ImageView target_user_gift_img;

    @BindView(R.id.me_user_head)
    ImageView me_user_head;
    @BindView(R.id.me_user_gift_img)
    ImageView me_user_gift_img;

    @BindView(R.id.gift_img)
    ImageView gift_img;

    @BindView(R.id.gift_name)
    TextView gift_name;
    @BindView(R.id.gift_price)
    TextView gift_price;

    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.title_desc)
    TextView title_desc;
    @BindView(R.id.cp_declaration_comfirm_tv)
    TextView cp_declaration_comfirm_tv;

    private boolean isExetedSendAdvance = false;

    @Override
    protected String getPageId() {
        return TrackConstants.PAGE_DECLARATION_KEEPSAKE;
    }

    @Override
    protected void parseBundleExtras(Bundle extras) {
        et_string = extras.getString("et_string");
        female_user_id = extras.getString(Constants.SESSION_ID);
        female_user_head = extras.getString("target_user_head");

        rise_to = extras.getString("rise_to");
        intimacy = extras.getString("intimacy");
    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.acitivity_declaration_keepsake;
    }

    @Override
    protected void initViewsAndEvents(Bundle savedInstanceState) {
        String me_user_head_String = AccountUtil.getInstance().getUserInfo().getAvatar_url();

        Glide.with(mContext)
                .load(me_user_head_String)
                .into(me_user_head);

        Glide.with(mContext)
                .load(female_user_head)
                .into(target_user_head);

        checkCpLevelDetail();

        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", AccountUtil.getInstance().getUserId());
        params.put("female_guest_id", female_user_id);
        params.put("event_time", String.valueOf(System.currentTimeMillis()));
        params.put("rise_to",rise_to);
        params.put("intimacy", intimacy);
        MultiTracker.onEvent("b_exposure_present_gift_rise_cp", params);
    }

    @OnClick({R.id.iv_back, R.id.cp_declaration_comfirm_tv})
    public void onClick(View view) {
        if (ViewUtils.isFastClick()) {
            return;
        }

        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.cp_declaration_comfirm_tv:
                HashMap<String, String> params = new HashMap<>();
                params.put("user_id", AccountUtil.getInstance().getUserId());
                params.put("female_guest_id", female_user_id);
                params.put("event_time", String.valueOf(System.currentTimeMillis()));
                params.put("rise_to",rise_to);
                params.put("intimacy", intimacy);
                MultiTracker.onEvent("b_button_present_gift_rise_cp_submit", params);

                sendAdvance();
                break;
        }
    }

    private void setenAble() {
        getHandler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(cp_declaration_comfirm_tv !=null){
                    cp_declaration_comfirm_tv.setEnabled(true);
                }
            }
        },500);
    }

    private void sendAdvance (){
        if (cp_declaration_comfirm_tv != null) {
            cp_declaration_comfirm_tv.setEnabled(false);
        }
        try {
            JSONObject object = new JSONObject();
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("female_user_id",female_user_id);
            object.put("male_swear_words",et_string);

            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.sendAdvance(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    try {
                        JSONObject obj = new JSONObject(result);
                        int code = obj.getInt("code");
                        if(code == 0){
                            HashMap<String, String> params = new HashMap<>();
                            params.put("user_id", AccountUtil.getInstance().getUserId());
                            params.put("female_guest_id", female_user_id);
                            params.put("event_time", String.valueOf(System.currentTimeMillis()));
                            params.put("rise_to",rise_to);
                            params.put("intimacy", intimacy);
                            MultiTracker.onEvent("b_success_present_gift_rise_cp", params);

                            setResult(RESULT_OK);
                            finish();
                        }else if(code == 17039){
                            Toast.makeText(ContextUtils.getApplicationContext(), "金币余额不足", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(DeclarationKeepsakeActivity.this, RechargeActivity.class));
                        }else if (code == 2072) {
                            String msg = obj.optString("msg");
                            if(StringUtil.isNotEmpty(msg)){
                                Toast.makeText(ContextUtils.getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                            }
                        }else {
                            String msg = obj.optString("msg");
                            if(StringUtil.isNotEmpty(msg)){
                                Toast.makeText(ContextUtils.getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                            }
                        }

                    } catch (Throwable e) {
                        e.printStackTrace();
                    }

                    setenAble();
                }

                @Override
                public void OnFailed(int ret, String result) {
                    setenAble();
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void checkCpLevelDetail (){
        try {
            JSONObject object = new JSONObject();
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("female_user_id",female_user_id);
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.checkCpLevelDetail(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    if (isFinishing() || isDestroyed()){
                        return;
                    }

                    closeLoading();
                    CpLevelUpDetail cpLevelUpDetail = GT.fromJson(result, CpLevelUpDetail.class);
                    if(cpLevelUpDetail != null){
                        if(cpLevelUpDetail.getCode() == 0){
                            CpLevelUpDetail.DataBean dataBean = cpLevelUpDetail.getData();
                            if(dataBean != null){
                                CpLevelUpDetail.DataBean.AdvanceDetailBean advanceDetailBean = dataBean.getAdvance_detail();
                                if(advanceDetailBean != null){
                                    CpLevelUpDetail.DataBean.AdvanceDetailBean.GiftBean giftBean = advanceDetailBean.getGift();
                                    if(giftBean != null){
                                        String gift_pic_url = giftBean.getGift_pic_url();
                                        String gift_name_String = giftBean.getGift_name();
                                        double gift_price_double = giftBean.getGift_price();

                                        Glide.with(mContext)
                                                .load(gift_pic_url)
                                                .into(gift_img);
                                        gift_name.setText(gift_name_String);
                                        gift_price.setText(""+ (int)gift_price_double);


                                        boolean level_swear = dataBean.isLevel_swear();
                                        if(level_swear){
                                            Glide.with(mContext)
                                                    .load(gift_pic_url)
                                                    .into(me_user_gift_img);
                                        }

                                        boolean target_level_swear = dataBean.isTarget_level_swear();
                                        if(target_level_swear){
                                            Glide.with(mContext)
                                                    .load(gift_pic_url)
                                                    .into(target_user_gift_img);
                                        }
                                    }
                                }

                                CpLevelUpDetail.DataBean.GroupBean groupBean = dataBean.getGroup();
                                if(groupBean != null){
                                    String name = groupBean.getName();

                                    title_desc.setText("相互赠送礼物后晋级[" + name + "]");
                                }

                                int code = cpLevelUpDetail.getCode();
                                if(code != 0){
                                    String msg = cpLevelUpDetail.getMsg();
                                    if(com.liquid.base.tools.StringUtil.isNotEmpty(msg)){
                                        Toast.makeText(ContextUtils.getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }

                        }else if (cpLevelUpDetail.getCode() == 2072){
                            // 您已经晋级此等级
                        }
                    }
                }

                @Override
                public void OnFailed(int ret, String result) {
                    closeLoading();
                }
            });
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
}
