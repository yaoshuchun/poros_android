package com.liquid.poros.ui.fragment.dialog.user;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.liquid.poros.R;
import com.liquid.poros.widgets.CustomRadioGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * 选择性别弹窗
 */
public class ChooseSexDialogFragment extends BottomSheetDialogFragment {
    private CustomRadioGroup rg_group;
    private RadioButton rb_choose_male;
    private RadioButton rb_choose_female;
    private String mSex = null;

    public static ChooseSexDialogFragment newInstance() {
        ChooseSexDialogFragment fragment = new ChooseSexDialogFragment();
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
        View view = View.inflate(getContext(), R.layout.fragment_choose_sex_dialog, null);
        dialog.setContentView(view);
        //设置透明背景
        dialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);
        View root = dialog.getDelegate().findViewById(R.id.design_bottom_sheet);
        BottomSheetBehavior behavior = BottomSheetBehavior.from(root);
        behavior.setHideable(false);
        initializeView(view);
        return dialog;
    }

    private void initializeView(View view) {
        rg_group = view.findViewById(R.id.rg_group);
        rb_choose_male = view.findViewById(R.id.rb_choose_male);
        rb_choose_female = view.findViewById(R.id.rb_choose_female);
        Drawable ageDrawable = getActivity().getResources().getDrawable(R.mipmap.icon_select);
        ageDrawable.setBounds(0, 0, ageDrawable.getMinimumWidth(), ageDrawable.getMinimumHeight());
        rg_group.setOnCheckedChangeListener(new CustomRadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CustomRadioGroup group, int checkedId) {
                if (checkedId == rb_choose_male.getId()) {
                    rb_choose_male.setCompoundDrawables(null, null, null, ageDrawable);
                    rb_choose_female.setCompoundDrawables(null, null, null, null);
                    mSex = "男";
                } else if (checkedId == rb_choose_female.getId()) {
                    rb_choose_female.setCompoundDrawables(null, null, null, ageDrawable);
                    rb_choose_male.setCompoundDrawables(null, null, null, null);
                    mSex = "女";
                }
            }
        });
        view.findViewById(R.id.tv_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.chooseSexSuccess(mSex);
                }
                dismissAllowingStateLoss();
            }
        });
        view.findViewById(R.id.tv_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissAllowingStateLoss();
            }
        });
    }

    private ChooseSexDialogFragment.ChooseSexListener listener;

    public ChooseSexDialogFragment setChooseSexListener(ChooseSexDialogFragment.ChooseSexListener listener) {
        this.listener = listener;
        return this;
    }

    public interface ChooseSexListener {
        void chooseSexSuccess(String sex);
    }

}
