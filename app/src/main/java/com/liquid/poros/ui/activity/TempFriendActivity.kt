package com.liquid.poros.ui.activity

import android.os.Bundle
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.liquid.base.event.EventCenter
import com.liquid.poros.R
import com.liquid.poros.analytics.utils.MultiTracker
import com.liquid.poros.base.BaseActivity
import com.liquid.poros.constant.Constants
import com.liquid.poros.constant.TrackConstants
import com.liquid.poros.contract.message.MessagePresenter
import com.liquid.poros.entity.AppBaseConfigBean
import com.liquid.poros.entity.FriendMessageInfo
import com.liquid.poros.entity.MessageListBean
import com.liquid.poros.helper.JinquanResourceHelper
import com.liquid.poros.ui.activity.pta.PtaSettingActivity
import com.liquid.poros.ui.activity.viewmodel.MessageListModel
import com.liquid.poros.ui.fragment.MessageFriendItemView
import com.liquid.poros.ui.fragment.home.MessageFragment
import com.liquid.poros.ui.popupwindow.FriendMessagePopWindow
import com.liquid.poros.utils.ViewUtils
import com.liquid.poros.widgets.pulltorefresh.ViewStatus
import com.luck.picture.lib.config.PictureSelectionConfig.listener
import kotlinx.android.synthetic.main.activity_intimate.*
import kotlinx.android.synthetic.main.fragment_message.*
import kotlinx.android.synthetic.main.fragment_message.container
import java.util.ArrayList
import com.liquid.poros.widgets.pulltorefresh.BaseRefreshListener as PulltorefreshBaseRefreshListener

/**
 * 亲密消息入口页面
 * Created by yejiurui on 1/26/21.
 * Mail:yejiurui@liqiudnetwork.com
 */
internal class TempFriendActivity : BaseActivity(), PulltorefreshBaseRefreshListener {

    private var messageListModel: MessageListModel? = null
    private var mCommonAdapter: MessageAdapter? = null

    override fun getPageId(): String {
        return TrackConstants.INTIMATE_PAGE
    }

    override fun parseBundleExtras(extras: Bundle?) {

    }

    override fun getContentViewLayoutID(): Int {
        return R.layout.activity_intimate
    }

    override fun initViewsAndEvents(savedInstanceState: Bundle?) {
        rv_message_list!!.layoutManager = LinearLayoutManager(this)


        messageListModel = getActivityViewModel(MessageListModel::class.java)

        mCommonAdapter = MessageAdapter()
        rv_message_list!!.adapter = mCommonAdapter
        messageListModel?.getIntimateListData(true)
        head_title.setTitile("过期临时好友")
        container!!.setCanRefresh(true)
        container!!.setCanLoadMore(false)
        container!!.setRefreshListener(this)
//        ll_contacts.setOnClickListener {
//            if (ViewUtils.isFastClick()) {
//                return@setOnClickListener
//            }
//            readyGo(PtaSettingActivity::class.java)
//        }

        messageListModel?.messageListDataSuccess?.observe(this, Observer {
            container!!.finishRefresh()
            if (it == null || it.session_list == null || it.session_list.size == 0) {
                container!!.showView(ViewStatus.EMPTY_STATUS)
            } else {
                container!!.showView(ViewStatus.CONTENT_STATUS)
                mCommonAdapter!!.update(it.session_list)
            }
        })

        messageListModel?.messageListDataFail?.observe(this, Observer {
            container!!.finishRefresh()
            container!!.showView(ViewStatus.EMPTY_STATUS)
        })

    }

    override fun onStart() {
        super.onStart()
        val params = HashMap<String?,String?>()
        MultiTracker.onEvent(TrackConstants.B_CHAT_LIST_EXPIRED_LIST_PAGE,params)
    }

    override fun loadMore() {
        TODO("Not yet implemented")
    }

    override fun refresh() {
        messageListModel?.getIntimateListData(true)
    }

    override fun onResume() {
        super.onResume()
        messageListModel?.getIntimateListData(true)
    }

    private var lastTime: Long = 0
    override fun onEventComing(eventCenter: EventCenter<*>) {
        if (eventCenter.eventCode == Constants.EVENT_CODE_REFRESH_MESSAGE || eventCenter.eventCode == Constants.EVENT_CODE_UNREAD_MESSAGE || System.currentTimeMillis() - lastTime > 10000) {
//            if (mPresenter != null) {
//                mPresenter!!.getFriendMessageList()
//                lastTime = System.currentTimeMillis()
//            }
            messageListModel?.getIntimateListData(true)
        }
    }

    /**
     * 消息Adapter
     */
    private inner class MessageAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
        private val mDatas: MutableList<FriendMessageInfo> = ArrayList()
        fun update(data: List<FriendMessageInfo>?) {
            if (data != null) {
                mDatas.clear()
                mDatas.addAll(data)
                notifyDataSetChanged()
            }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
            return object : RecyclerView.ViewHolder(MessageFriendItemView(parent.context,true)) {}
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
            val itemView = holder.itemView as MessageFriendItemView
            val friendMessageInfo = mDatas[position]
            itemView.setData(friendMessageInfo)
//            itemView.setOnLongClickListener {
//                mPosition = holder.layoutPosition
//                mUserId = friendMessageInfo.user_id
//                friendMessagePopWindow!!.showPop(itemView.friendName, true, friendMessageInfo.pin_priority == -1)
//                JinquanResourceHelper.getInstance(activity!!).setBackgroundColorByAttr(itemView.itemContent, R.attr.bg_item_message)
//                true
//            }
        }

        override fun getItemCount(): Int {
            return mDatas.size
        }
    }

    companion object {
        @JvmField
        var isPause = false

        @JvmStatic
        fun newInstance(): MessageFragment {
            return MessageFragment()
        }
    }
}

