package com.liquid.poros.ui.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.liquid.poros.R;
import com.liquid.poros.base.BaseDialogFragment;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.helper.TimeUtil;
import com.liquid.poros.ui.floatball.GiftPackageFloatManager;
import com.liquid.poros.utils.TimeUtils;
import com.liquid.poros.utils.ViewUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

public class GiftPackageDialog extends BaseDialogFragment{
    TextView time;
    private long mCount = 10 * 60 * 1000;
    private CountDownTimer mCountDownTimer;

    public GiftPackageDialog setmCount(long mCount) {
        this.mCount = mCount;
        return this;
    }

    public static GiftPackageDialog newInstance() {
        GiftPackageDialog fragment = new GiftPackageDialog();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected String getPageId() {
        return TrackConstants.PAGE_GIFT_PACKAGE_DIALOG;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        Window win = getDialog().getWindow();
        // 一定要设置Background，如果不设置，window属性设置无效
        win.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams params = win.getAttributes();
        params.gravity = Gravity.CENTER;
        setCancelable(false);
        // 使用ViewGroup.LayoutParams，以便Dialog 宽度充满整个屏幕
        params.width = (int) (dm.widthPixels * 1.0);
        win.setAttributes(params);
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);
        endCountTime();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_gift_package_dialog, null);
        time = view.findViewById(R.id.time);

        view.findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ViewUtils.isFastClick()) {
                    return;
                }
                GiftPackageFloatManager.getInstance().showPayDialog();
                dismissDialog();
            }
        });
        view.findViewById(R.id.close_ic).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ViewUtils.isFastClick()) {
                    return;
                }
                dismissDialog();
            }
        });
        startCountTime();
        builder.setView(view);
        return builder.create();
    }

    private void startCountTime() {
        if (mCountDownTimer == null && mCount > 0) {
            mCountDownTimer = new CountDownTimer(mCount, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    mCount = millisUntilFinished;
                    if (time != null) {
                        time.setText(String.format(getString(R.string.remain_time), TimeUtils.getFormatTime(millisUntilFinished)));
                    }
                }

                @Override
                public void onFinish() {
                    endCountTime();
                    dismissDialog();
                }
            };
            mCountDownTimer.start();
        }
    }

    private void endCountTime() {
        if (mCountDownTimer != null) {
            mCountDownTimer.cancel();
            mCountDownTimer = null;
        }
    }
}
