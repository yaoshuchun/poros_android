package com.liquid.poros.ui.activity.setting.magic;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import com.blankj.utilcode.util.KeyboardUtils;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.reflect.TypeToken;
import com.liquid.base.adapter.CommonAdapter;
import com.liquid.base.adapter.CommonViewHolder;
import com.liquid.base.utils.ContextUtils;
import com.liquid.base.utils.GsonUtil;
import com.liquid.base.utils.ToastUtil;
import com.liquid.poros.R;
import com.liquid.poros.base.BaseActivity;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.poros.utils.AppConfigUtil;
import com.liquid.poros.utils.SharePreferenceUtil;
import com.liquid.poros.utils.StringUtil;
import com.umeng.analytics.MobclickAgent;

import java.io.Serializable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.StringDef;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.OnClick;

/**
 * API手动切换功能，在debug环境，可快捷切换到任意接口
 *
 * @author yejiurui
 */
public class MagicApiActivity extends BaseActivity {
    private static final String API = "api";
    private static final String H5 = "新h5";

    @BindView(R.id.tabLayout)
    TabLayout mTabLayout;
    @BindView(R.id.et_url)
    EditText mEtUrl;
    @BindView(R.id.btn_add)
    Button mBtnAdd;
    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.tv_save)
    TextView tv_save;

    private CommonAdapter<MagicBean> mCommonAdapter;

    @Override
    protected String getPageId() {
        return null;
    }

    @Override
    protected void parseBundleExtras(Bundle extras) {

    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.magic_api_activity;
    }


    private final String[] TABS = {API};
    private String[] VALUES = new String[TABS.length];

    private void setDefaultData() {
        initApi();
    }

    private void initApi() {
        List<MagicBean> apiList = SPMagic.getData(API);
        if (apiList.isEmpty()) {
            apiList.add(new MagicBean(Constants.BASE_URL, TextUtils.equals(Constants.BASE_URL, Constants.BASE_DEVELOP_URL)));
            apiList.add(new MagicBean("http://testporos.liquidnetwork.com:83/service/", false));
            apiList.add(new MagicBean(Constants.BASE_PRODUCT_URL, false));
        } else {
            for (MagicBean magicBean : apiList) {
                magicBean.selected = magicBean.url.equals(Constants.BASE_URL);
            }
        }
        SPMagic.saveData(API, apiList);
    }

    /**
     * 重置API
     */
    private void resetApi() {
        List<MagicBean> apiList = new ArrayList<>();
        apiList.add(new MagicBean(Constants.BASE_URL, true));
        apiList.add(new MagicBean("http://testporos.liquidnetwork.com:83/service/", false));
        apiList.add(new MagicBean(Constants.BASE_PRODUCT_URL, false));
        SPMagic.saveData(API, apiList);
        saveApi();
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void initViewsAndEvents(Bundle savedInstanceState) {
        setDefaultData();

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setOnTouchListener((v, event) -> {
            KeyboardUtils.hideSoftInput(v);
            return false;
        });

        mCommonAdapter = new CommonAdapter<MagicBean>(this, R.layout.item_magic) {
            @Override
            public void convert(CommonViewHolder viewHolder, MagicBean itemData, int pos) {
                TextView tvUrl = viewHolder.getView(R.id.tv_url);
                RadioButton radioButton = viewHolder.getView(R.id.radioButton);

                tvUrl.setText(itemData.url);
                radioButton.setChecked(itemData.selected);

                viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (itemData.selected) {
                            return;
                        }
                        int selectedPosition = mTabLayout.getSelectedTabPosition();
                        List<MagicBean> list = mCommonAdapter.getDatas();
                        for (MagicBean magicBean : list) {
                            if (magicBean.url.equals(itemData.url)) {
                                magicBean.selected = true;
                                VALUES[selectedPosition] = magicBean.url;
                                mEtUrl.setText(magicBean.url);
                            } else {
                                magicBean.selected = false;
                            }
                        }
                        mCommonAdapter.notifyDataSetChanged();
                        SPMagic.saveData(TABS[selectedPosition], list);
                    }
                });
            }
        };
        mRecyclerView.setAdapter(mCommonAdapter);

        mTabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                load();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        for (String tab : TABS) {
            mTabLayout.addTab(mTabLayout.newTab().setText(tab));
        }

        tv_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveApi();
            }
        });
    }


    @Retention(RetentionPolicy.SOURCE)
    @StringDef({API, H5})
    private @interface WHICH {
    }


    private void load() {
        String which = TABS[mTabLayout.getSelectedTabPosition()];
        List<MagicBean> list = SPMagic.getData(which);
        mCommonAdapter.updateData(list);
        for (MagicBean item : list) {
            if (item.selected) {
                mEtUrl.setText(item.url);
                break;
            }
        }
    }

    @OnClick({R.id.btn_add, R.id.tv_reset_api})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_add:
                add();
                break;
            case R.id.tv_reset_api:
                resetApi();
                break;
        }


    }

    private void add() {
        String url = mEtUrl.getText().toString().trim();
        if (!URLUtil.isNetworkUrl(url)) {
            mEtUrl.setError("url不合法");
            return;
        }
        if (!url.endsWith("/")) {
            // 如果末尾木有/ 主动加上
            url = url + "/";
        }
        for (MagicBean item : mCommonAdapter.getDatas()) {
            if (item.url.equals(url)) {
                ToastUtil.show("url已存在, 请直接使用", Gravity.CENTER);
                return;
            }
        }
        List<MagicBean> list = mCommonAdapter.getDatas();
        for (MagicBean magicBean : list) {
            magicBean.selected = false;
        }
        list.add(0, new MagicBean(url, true));
        mCommonAdapter.notifyDataSetChanged();
        int selectedPosition = mTabLayout.getSelectedTabPosition();
        VALUES[selectedPosition] = url;
        SPMagic.saveData(TABS[selectedPosition], mCommonAdapter.getDatas());
    }

    /**
     * 保存变更API
     */
    private void saveApi() {
        for (int i = 0; i < TABS.length; i++) {
            String value = VALUES[i];
            if (StringUtil.isNotEmpty(value)) {
                String item = TABS[i];
                List<MagicBean> list = SPMagic.getData(item);
                for (MagicBean magicBean : list) {
                    magicBean.selected = magicBean.url.equals(value);
                }
                SPMagic.saveData(item, list);
                switch (item) {
                    case API:
                        saveDebugApi(value);
                        break;
                }
            }
        }
        MagicUtil.showRestartAppAlertDialog(this, new MagicUtil.RestartAppAlertCallback() {
            @Override
            public void onPositive() {
                //复制 from SettingActivity
                SharePreferenceUtil.removeKey(SharePreferenceUtil.FILE_USER_ACCOUNT_DATA, SharePreferenceUtil.KEY_USER_ACCOUNT_TOKEN);
                SharePreferenceUtil.removeKey(SharePreferenceUtil.FILE_USER_ACCOUNT_DATA, SharePreferenceUtil.KEY_USER_ACCOUNT_INFO);
                SharePreferenceUtil.removeKey(SharePreferenceUtil.FILE_USER_ACCOUNT_DATA, SharePreferenceUtil.FILE_USER_ACCOUNT_DATA);
                //友盟账号登出
                MobclickAgent.onProfileSignOff();

                AppConfigUtil.removeBaseConfig();
                AccountUtil.getInstance().setUserInfo(null);
            }
        });
    }


    private void saveDebugApi(String value) {
        SharePreferenceUtil.saveString(SharePreferenceUtil.FILE_API_CONFIG, SharePreferenceUtil.KEY_API_URL, value);
    }

    private static class MagicBean implements Serializable {
        private static final long serialVersionUID = -7086161337227547743L;

        public String url;
        public boolean selected;

        public MagicBean(String url) {
            this(url, false);
        }

        public MagicBean(String url, boolean selected) {
            this.url = url;
            this.selected = selected;
        }

        @Override
        public boolean equals(Object obj) {
            try {
                return url.equals(((MagicBean) obj).url);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return super.equals(obj);
        }
    }

    private static class SPMagic {

        private static final String SP_MAGIC = "magic";

        private static final String MAGICS = "magics";
        private static final String API_MAGICS = "api_magics";
        private static final String WEB_MAGICS = "web_magics";

        private SPMagic() {
            throw new AssertionError("cannot be instantiated");
        }

        private static SharedPreferences getSharedPreferences() {
            return ContextUtils.getApplicationContext().getSharedPreferences(SP_MAGIC, Context.MODE_PRIVATE);
        }

        public static List<MagicBean> getData(@WHICH String which) {
            String json = getSharedPreferences().getString(MAGICS + which, "");
            List<MagicBean> list = GsonUtil.fromJson(json, new TypeToken<List<MagicBean>>() {
            });
            if (list == null) {
                list = new ArrayList<>();
            }
            return list;
        }

        public static void saveData(@WHICH String which, List<MagicBean> list) {
            String magic = GsonUtil.toJson(list);
            getSharedPreferences().edit().putString(MAGICS + which, magic).apply();
        }
    }

}
