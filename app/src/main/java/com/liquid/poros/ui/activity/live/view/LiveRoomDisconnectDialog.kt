package com.liquid.poros.ui.activity.live.view

import android.content.Context
import android.view.Gravity
import android.view.View
import android.widget.TextView
import com.liquid.poros.R
import razerdp.basepopup.BasePopupWindow

/**
 * 视频房网络断开Dialog
 */
class LiveRoomDisconnectDialog(context: Context) : BasePopupWindow(context) {

    var reConnectListener: (() -> Unit)? = null

    override fun onCreateContentView(): View {
        setBackPressEnable(false)
        setOutSideDismiss(false)
        popupGravity = Gravity.CENTER
        return createPopupById(R.layout.dialog_live_room_disconnect)
    }

    override fun onViewCreated(contentView: View) {
        super.onViewCreated(contentView)
        contentView.findViewById<TextView>(R.id.reconnect).setOnClickListener {
            reConnectListener?.invoke()
        }
    }

}