package com.liquid.poros.ui.fragment.blindbox.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.liquid.library.retrofitx.RetrofitX
import com.liquid.poros.entity.BlindBoxRecordInfo
import com.liquid.poros.http.ApiUrl
import com.liquid.poros.http.observer.ObjectObserver
import com.liquid.poros.http.utils.getPoros

/**
 * 盲盒中奖记录
 */
class BlindBoxRecordVM : ViewModel() {
    val blindBoxRecordLiveData: MutableLiveData<BlindBoxRecordInfo> by lazy { MutableLiveData<BlindBoxRecordInfo>() }
    val blindBoxRecordFail: MutableLiveData<BlindBoxRecordInfo> by lazy { MutableLiveData<BlindBoxRecordInfo>() }


    /**
     * 助力抽奖记录
     */
    fun blindBoxRecord(blindBoxType: Int) {
        RetrofitX.url(ApiUrl.BLIND_BOX_RECORD)
                .param("blind_box_type", blindBoxType)
                .getPoros()
                .subscribe(object : ObjectObserver<BlindBoxRecordInfo>() {
                    override fun success(result: BlindBoxRecordInfo) {
                        blindBoxRecordLiveData.postValue(result)
                    }

                    override fun failed(result: BlindBoxRecordInfo) {
                        blindBoxRecordFail.postValue(result)
                    }

                })
    }

}