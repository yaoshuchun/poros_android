package com.liquid.poros.ui.fragment.dialog.live;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.request.RequestOptions;
import com.liquid.poros.R;
import com.liquid.poros.base.BaseDialogFragment;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.utils.ViewUtils;

/**
 * 直播房间开黑邀请
 */
public class CpInviteTeamDialogFragment extends BaseDialogFragment {
    private String mAvatarUrl;
    private String mNickName;
    private String mContent;

    public CpInviteTeamDialogFragment setUserInfo(String avatarUrl, String nickName, String content) {
        this.mAvatarUrl = avatarUrl;
        this.mNickName = nickName;
        this.mContent = content;
        return this;
    }

    public static CpInviteTeamDialogFragment newInstance() {
        CpInviteTeamDialogFragment fragment = new CpInviteTeamDialogFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    protected String getPageId() {
        return TrackConstants.PAGE_RECEIVE_INVITE_CP_TEAM;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onStart() {
        super.onStart();
        Window win = getDialog().getWindow();
        // 一定要设置Background，如果不设置，window属性设置无效
        win.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams params = win.getAttributes();
        params.gravity = Gravity.CENTER;
        setCancelable(false);
        // 使用ViewGroup.LayoutParams，以便Dialog 宽度充满整个屏幕
        params.width = (int) (dm.widthPixels * 0.8);
        win.setAttributes(params);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_cp_invite_team_dialog, null);
        ImageView iv_invite_cp_head = view.findViewById(R.id.iv_invite_cp_head);
        TextView tv_name = view.findViewById(R.id.tv_name);
        TextView tv_user_desc = view.findViewById(R.id.tv_user_desc);
        RequestOptions options = new RequestOptions()
                .transform(new CircleCrop())
                .placeholder(R.mipmap.img_default_avatar)
                .error(R.mipmap.img_default_avatar);
        Glide.with(getActivity())
                .load(mAvatarUrl)
                .apply(options)
                .into(iv_invite_cp_head);
        tv_user_desc.setText(mContent);
        tv_name.setText(mNickName);
        view.findViewById(R.id.tv_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissDialog();
            }
        });
        view.findViewById(R.id.tv_confirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ViewUtils.isFastClick()) {
                    return;
                }
                if (listener != null) {
                    listener.userTeamCard();
                }
                dismissDialog();
            }
        });
        builder.setView(view);
        return builder.create();
    }

    private CpInviteTeamDialogFragment.UseCardListener listener;

    public CpInviteTeamDialogFragment setBuyListener(CpInviteTeamDialogFragment.UseCardListener listener) {
        this.listener = listener;
        return this;
    }

    public interface UseCardListener {
        //使用开黑卡
        void userTeamCard();
    }
}
