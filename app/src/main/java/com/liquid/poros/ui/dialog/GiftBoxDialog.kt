package com.liquid.poros.ui.dialog

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.airbnb.lottie.LottieAnimationView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.liquid.base.utils.ToastUtil
import com.liquid.poros.R
import com.liquid.poros.analytics.utils.MultiTracker
import com.liquid.poros.base.BaseBottomSheetDialogFragment
import com.liquid.poros.constant.Constants
import com.liquid.poros.constant.TrackConstants
import com.liquid.poros.entity.GetGiftBoxExplain
import com.liquid.poros.entity.GiftBoxInfo
import com.liquid.poros.ui.activity.WebViewActivity
import com.liquid.poros.ui.activity.gifbox.GetedGiftOfBoxNoticesActivity
import com.liquid.poros.ui.activity.user.SessionActivity
import com.liquid.poros.utils.AnimListener
import com.liquid.poros.utils.UIUtils
import com.liquid.poros.utils.ViewUtils
import com.liquid.poros.viewmodel.GiftBoxVM

class GiftBoxDialog : BaseBottomSheetDialogFragment() {

    private lateinit var tv_gift_box_count: TextView
    private lateinit var iv_add_gift_box: ImageView
    private lateinit var tv_gift_box_key_count: TextView
    private lateinit var iv_add_gift_box_key: ImageView
    private lateinit var iv_icon_to_gift_pool: ImageView
    private lateinit var tv_get_gift_bubble: TextView
    private lateinit var tv_open: TextView
    private lateinit var lottie_open_gift_box: LottieAnimationView
    private lateinit var iv_gift_box: ImageView
    private lateinit var view_stage: View
    private lateinit var giftBoxVM: GiftBoxVM
    private lateinit var from_user_id: String

    private val giftBoxUrl = Constants.BASE_URL_RESOURCE + "gift_box/icon_gift_box.png"

    companion object {
        const val FROM_USER_ID = "from_user_id"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        giftBoxVM = getViewModel(GiftBoxVM::class.java)
        giftBoxVM.giftBoxInfoData.observe(this, { boxInfo ->
            updateView(boxInfo)
        })
        giftBoxVM.openGiftBoxData.observe(this, {
            if (it.code == 0){
                giftBoxVM.handleGiftBoxInfoAfterOpen()
                startOpenGiftBoxAnim()
                val msgParams = HashMap<String?, String?>()
                msgParams["guest_id"] = from_user_id
                MultiTracker.onEvent(TrackConstants.B_UNPACK_THE_GIFT_BOX_SUCCESS, msgParams)
            }else{
                ToastUtil.show(it.msg)
            }

        })
    }

    override fun contentXmlId(): Int {
        return R.layout.dialog_f_gift_box

    }

    override fun initView() {
        tv_gift_box_count = rootView?.findViewById(R.id.tv_gift_box_count)!!
        iv_add_gift_box = rootView?.findViewById(R.id.iv_add_gift_box)!!
        tv_gift_box_key_count = rootView?.findViewById(R.id.tv_gift_box_key_count)!!
        iv_add_gift_box_key = rootView?.findViewById(R.id.iv_add_gift_box_key)!!
        iv_icon_to_gift_pool = rootView?.findViewById(R.id.iv_icon_to_gift_pool)!!
        tv_get_gift_bubble = rootView?.findViewById(R.id.tv_get_gift_bubble)!!
        tv_open = rootView?.findViewById(R.id.tv_open)!!
        lottie_open_gift_box = rootView?.findViewById(R.id.lottie_open_gift_box)!!
        iv_gift_box = rootView?.findViewById(R.id.iv_gift_box)!!
        view_stage = rootView?.findViewById(R.id.view_stage)!!
        Glide.with(mContext!!)
                .load(giftBoxUrl)
                .into(iv_gift_box)
    }

    override fun initListener() {
        view_stage.setOnClickListener {
            handleOpenBox()
        }
        iv_gift_box.setOnClickListener {
            handleOpenBox()
        }
        tv_open.setOnClickListener {
            handleOpenBox()
        }

        tv_get_gift_bubble.setOnClickListener {
            if(ViewUtils.isFastClick()) return@setOnClickListener
            GetedGiftOfBoxNoticesActivity.start(mContext,null)
            val msgParams = HashMap<String?, String?>()
            msgParams["guest_id"] = from_user_id
            MultiTracker.onEvent(TrackConstants.B_WIN_A_LOTTERY_NOTICE_CLICK, msgParams)
        }

        iv_add_gift_box.setOnClickListener {
            val giftBoxDialog = GiftBoxNotEnoughDialog()
            val bundle = Bundle()
            bundle.putSerializable(GiftDetailDialog.INFO, GetGiftBoxExplain("如何获得亲密礼盒",giftBoxUrl,"我知道了"))
            giftBoxDialog.arguments = bundle
            giftBoxDialog.show(requireActivity().supportFragmentManager)
            val msgParams = HashMap<String?, String?>()
            msgParams["guest_id"] = from_user_id
            MultiTracker.onEvent(TrackConstants.B_GIFT_BOX_BALANCE_CLICK, msgParams)
        }
        iv_add_gift_box_key.setOnClickListener {
            if (activity is SessionActivity) {
                val sessionActivity: SessionActivity = activity as SessionActivity
                sessionActivity.showBuyCpDialog(1, "",true)
                dismiss()
                val msgParams = HashMap<String?, String?>()
                msgParams["guest_id"] = from_user_id
                MultiTracker.onEvent(TrackConstants.B_KEY_BALANCE_CLICK, msgParams)
            }
        }

        iv_icon_to_gift_pool.setOnClickListener {
            if(ViewUtils.isFastClick()) return@setOnClickListener
            val bundle = Bundle()
            bundle.putString(Constants.TARGET_PATH, Constants.GIFT_BOX_RULE_URL)
            bundle.putString(Constants.FROM, "open_gift")
            val intent: Intent = Intent(requireActivity(), WebViewActivity::class.java)
            intent.putExtras(bundle)
            startActivity(intent)
            val msgParams = HashMap<String?, String?>()
            msgParams["guest_id"] = from_user_id
            MultiTracker.onEvent(TrackConstants.B_GIFT_BOX_MORE_GIFT_CLICK, msgParams)
        }
    }


    override fun initData() {
        from_user_id = arguments?.get(FROM_USER_ID) as String
        giftBoxVM.getGiftBoxInfo(from_user_id)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val msgParams = HashMap<String?, String?>()
        msgParams["guest_id"] = from_user_id
        MultiTracker.onEvent(TrackConstants.B_GIFT_BOX_DLALOG_EXPOSURE, msgParams)
    }

    private fun updateView(giftBoxInfo: GiftBoxInfo?){
        giftBoxInfo?.let {
            tv_gift_box_count.text = it.gift_box_count.toString()
            tv_gift_box_key_count.text = it.gift_box_key_count.toString()

            val roomOptions = RequestOptions()
                    .placeholder(R.mipmap.icon_default)
                    .error(R.mipmap.icon_default)
            Glide.with(mContext!!)
                    .load(it.prize_list_image)
                    .apply(roomOptions)
                    .into(iv_icon_to_gift_pool)

            it.reward_info?.run {
                tv_get_gift_bubble.text = recordListStr
            }

        }
    }

    private fun handleOpenBox(){
        if (giftBoxVM.isOpening.get()){
            ToastUtil.show("拆开中，请稍后")
            return
        }
        if (lottie_open_gift_box.isAnimating)
            return
        val msgParams = HashMap<String?, String?>()
        msgParams["guest_id"] = from_user_id
        MultiTracker.onEvent(TrackConstants.B_UNPACK_THE_GIFT_BOX_CLICK, msgParams)
        giftBoxVM.giftBoxInfoData.value?.let { box ->
            if (box.gift_box_count < 1) {
                activity?.let { ac ->
                    val giftBoxDialog = GiftBoxNotEnoughDialog()
                    val bundle = Bundle()
                    bundle.putSerializable(GiftBoxNotEnoughDialog.INFO, GetGiftBoxExplain("礼盒已经拆完啦！", Constants.BASE_URL_RESOURCE + "gift_box/icon_empty_gift_box.png", "确定"))
                    giftBoxDialog.arguments = bundle
                    giftBoxDialog.show(ac.supportFragmentManager)
                    val msgParams = HashMap<String?, String?>()
                    msgParams["guest_id"] = from_user_id
                    MultiTracker.onEvent(TrackConstants.B_GIFT_BOX_INSUFFICIENT_ALERT_EXPOSURE, msgParams)
                }
                return@let
            }

            if (box.gift_box_key_count < box.open_box_need_key) {
                activity?.let { ac ->
                    GiftBoxKeyNotEnoughDialog().show(ac.supportFragmentManager)
                    val msgParams = HashMap<String?, String?>()
                    msgParams["guest_id"] = from_user_id
                    MultiTracker.onEvent(TrackConstants.B_KEY_INSUFFICIENT_ALERT_EXPOSURE, msgParams)
                }
                return@let
            }

            giftBoxVM.openGiftBox(from_user_id)
        }
    }

    private fun startOpenGiftBoxAnim() {
        UIUtils.startLottieAnimation(iv_gift_box,lottie_open_gift_box,"opengiftbox", object : AnimListener {
            override fun onAnimEnd() {
                if (null == activity || activity!!.isDestroyed) {
                    return
                }
                if (activity is SessionActivity) {
                    val sessionActivity = activity as SessionActivity
                    sessionActivity.showGiftDialog(giftBoxVM.openGiftBoxData.value)
                }
            }
        })
    }

}