package com.liquid.poros.ui.fragment.dialog.pta;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.liquid.base.adapter.CommonAdapter;
import com.liquid.base.adapter.CommonViewHolder;
import com.liquid.base.event.EventCenter;
import com.liquid.base.utils.ToastUtil;
import com.liquid.library.retrofitx.RetrofitX;
import com.liquid.poros.R;
import com.liquid.poros.analytics.utils.MultiTracker;
import com.liquid.poros.base.BaseDialogFragment;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.entity.BaseBean;
import com.liquid.poros.entity.PtaSuitListInfo;
import com.liquid.poros.http.ApiUrl;
import com.liquid.poros.http.observer.ObjectObserver;
import com.liquid.poros.ui.activity.user.SessionActivity;
import com.liquid.poros.ui.dialog.RechargeCoinDialog;
import com.liquid.poros.ui.fragment.dialog.common.CommonActionListener;
import com.liquid.poros.ui.fragment.dialog.common.CommonDialogBuilder;
import com.liquid.poros.ui.fragment.dialog.common.CommonDialogFragment;
import com.liquid.poros.ui.fragment.dialog.pta.viewmodel.PtaSuitListInfoViewModel;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.poros.utils.ScreenUtil;
import com.liquid.poros.utils.ViewUtils;
import com.liquid.stat.boxtracker.util.LtNetworkUtil;

import org.jetbrains.annotations.NotNull;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import de.greenrobot.event.Subscribe;
import de.greenrobot.event.ThreadMode;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;

import static com.liquid.poros.ui.dialog.RechargeCoinDialog.POSITION_DRESS_ALERT;
import static com.liquid.poros.ui.dialog.RechargeCoinDialog.POSITION_DRESS_SEND;

/**
 * 个人形象装扮界面
 */
@Deprecated
public class PtaSettingDialogFragment extends BaseDialogFragment implements View.OnClickListener {
    private TextView tv_user_coin;
    private RecyclerView rv_pta_setting;
    private TextView tv_pta_pay_coin;
    private TextView tv_pta_pay;
    private RecyclerView rv_pta_setting_duration;
    private TextView item_tv_recharge;
    private CommonAdapter<PtaSuitListInfo.SuitListInfo> mCommonAdapter;
    private CommonAdapter<PtaSuitListInfo.SuitListInfo.PriceListInfo> mPtaDurationItemAdapter;
    private ArrayList<PtaSuitListInfo.SuitListInfo> mPtaSuitItemList = new ArrayList<>();
    private PtaSuitListInfoViewModel ptaSuitListInfoViewModel;
    private int mSelectPos;
    private int mPriceSelectPos;
    private long mUserCoin;
    private String mFemaleGuestId;
    private String mCartoonSuitId;
    private RechargeCoinDialog mRechargeCoinDialog = new RechargeCoinDialog();
    private PtaSuitPreviewDialogFragment mPtaSuitPreviewDialogFragment = PtaSuitPreviewDialogFragment.newInstance();

    public static PtaSettingDialogFragment newInstance() {
        PtaSettingDialogFragment fragment = new PtaSettingDialogFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    public PtaSettingDialogFragment setPtaTargetId(String femaleGuestId, long userCoin) {
        this.mUserCoin = userCoin;
        this.mFemaleGuestId = femaleGuestId;
        this.mUserCoin = userCoin;
        return this;
    }

    public PtaSettingDialogFragment updateCoin(long userCoin) {
        this.mUserCoin = userCoin;
        if (tv_user_coin != null) {
            tv_user_coin.setText(String.valueOf(mUserCoin));
        }
        return this;
    }

    public PtaSettingDialogFragment updateCartoonSuitId(String cartoonSuitId) {
        this.mCartoonSuitId = cartoonSuitId;
        return this;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        Window win = getDialog().getWindow();
        // 一定要设置Background，如果不设置，window属性设置无效
        win.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams params = win.getAttributes();
        params.width = ScreenUtil.getDisplayWidth();
        params.height = ScreenUtil.getDisplayWidth();
        params.gravity = Gravity.BOTTOM;
        win.setAttributes(params);
    }

    @Override
    protected String getPageId() {
        return TrackConstants.PAGE_PTA_SETTING_DIALOG;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_pta_setting_dialog, null);
        initView(view);
        builder.setView(view);
        AlertDialog alertDialog = builder.create();
        return alertDialog;
    }

    private void initView(View view) {
        tv_user_coin = view.findViewById(R.id.item_tv_coin);
        tv_user_coin.setText(String.valueOf(mUserCoin));
        rv_pta_setting = view.findViewById(R.id.rv_pta_setting);
        tv_pta_pay_coin = view.findViewById(R.id.tv_pta_pay_coin);
        tv_pta_pay = view.findViewById(R.id.item_tv_pta_pay);
        rv_pta_setting_duration = view.findViewById(R.id.rv_pta_setting_duration);
        item_tv_recharge = view.findViewById(R.id.item_tv_recharge);
        item_tv_recharge.setOnClickListener(this);
        tv_pta_pay.setOnClickListener(this);
        mPriceSelectPos = 0;
        mSelectPos = 0;
        mCommonAdapter = new CommonAdapter<PtaSuitListInfo.SuitListInfo>(getContext(), R.layout.item_pta_setting) {
            @Override
            public void convert(CommonViewHolder holder, final PtaSuitListInfo.SuitListInfo data, int position) {
                RelativeLayout rl_pta_image = holder.getView(R.id.rl_pta_image);
                ImageView item_iv_pta_setting = holder.getView(R.id.item_iv_pta_setting);
                if (mSelectPos == position) {
                    rl_pta_image.setBackgroundResource(R.drawable.bg_color_eaeef4_line_8);
                } else {
                    rl_pta_image.setBackgroundResource(R.drawable.bg_color_eaeef4_8);
                }
                RequestOptions options = new RequestOptions()
                        .placeholder(R.mipmap.icon_default)
                        .error(R.mipmap.icon_default)
                        .transform(new CenterCrop(), new RoundedCorners(ScreenUtil.dip2px(2)));
                Glide.with(getActivity())
                        .load(data.getIcon_url())
                        .override(ScreenUtil.dip2px(85), ScreenUtil.dip2px(95))
                        .apply(options)
                        .into(item_iv_pta_setting);
                holder.setText(R.id.item_iv_pta_name, data.getName());
                holder.setText(R.id.item_tv_pta_pay_coin, data.getPrice_coins_list().get(0).getCoin());
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mPtaSuitItemList == null || mPtaSuitItemList.size() == 0) {
                            return;
                        }
                        changePtaItemStatus(true, position);
                    }
                });
                holder.getView(R.id.item_iv_pta_preview).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mPtaSuitItemList == null || mPtaSuitItemList.size() == 0) {
                            return;
                        }
                        changePtaItemStatus(false, position);
                        mPtaSuitPreviewDialogFragment.setCartoonInfo(data.getIcon_url(), mPtaSuitItemList.get(mSelectPos).getPrice_coins_list().get(mPriceSelectPos).getCoin(), data.getName()).show(getActivity().getSupportFragmentManager(), PtaSuitPreviewDialogFragment.class.getSimpleName());
                        HashMap<String, String> params = new HashMap<>();
                        params.put("female_guest_id", mFemaleGuestId);
                        params.put("dress_id", data.get_id());
                        MultiTracker.onEvent(TrackConstants.B_DRESS_PREVIEW_CLICK, params);
                    }
                });
            }
        };
        mPtaDurationItemAdapter = new CommonAdapter<PtaSuitListInfo.SuitListInfo.PriceListInfo>(getContext(), R.layout.item_pta_shop_duration) {
            @Override
            public void convert(CommonViewHolder commonViewHolder, PtaSuitListInfo.SuitListInfo.PriceListInfo priceListInfo, int pos) {
                LinearLayout item_ll_pta_duration = commonViewHolder.getView(R.id.item_ll_pta_duration);
                TextView item_tv_pta_duration = commonViewHolder.getView(R.id.item_tv_pta_duration);
                item_tv_pta_duration.setText(priceListInfo.getDays() + "天");
                if (mPriceSelectPos == pos) {
                    item_ll_pta_duration.setBackgroundResource(R.drawable.bg_gradual_fc6573_22);
                    item_tv_pta_duration.setTextColor(Color.parseColor("#ffffff"));
                } else {
                    item_ll_pta_duration.setBackgroundResource(R.drawable.bg_color_f5f7fa_22);
                    item_tv_pta_duration.setTextColor(Color.parseColor("#CBCDD3"));
                }
                item_ll_pta_duration.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mPriceSelectPos = pos;
                        tv_pta_pay_coin.setText(priceListInfo.getCoin());
                        mPtaDurationItemAdapter.notifyDataSetChanged();
                    }
                });
            }
        };
        rv_pta_setting_duration.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL, false));
        rv_pta_setting.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        ptaSuitListInfoViewModel = getDialogFragmentViewModel(PtaSuitListInfoViewModel.class);
        ptaSuitListInfoViewModel.getPtaSuitList();
        ptaSuitListInfoViewModel.getPtaSuitListLiveData().observe(this, ptaSuitListInfo -> {
            mPtaSuitItemList.clear();
            mPtaSuitItemList.addAll(ptaSuitListInfo.getSuit_list());
            if (mPtaSuitItemList == null || mPtaSuitItemList.size() == 0) {
                return;
            }
            mCommonAdapter.update(mPtaSuitItemList);
            mPtaDurationItemAdapter.update(mPtaSuitItemList.get(mSelectPos).getPrice_coins_list());
            tv_pta_pay_coin.setText(mPtaSuitItemList.get(mSelectPos).getPrice_coins_list().get(mPriceSelectPos).getCoin());
        });
        rv_pta_setting.setAdapter(mCommonAdapter);
        rv_pta_setting_duration.setAdapter(mPtaDurationItemAdapter);
    }

    @Override
    public void onClick(View v) {
        if (ViewUtils.isFastClick() || !LtNetworkUtil.isNetworkAvailable(getActivity())) {
            if (!LtNetworkUtil.isNetworkAvailable(getActivity())) {
                ToastUtil.show(R.string.net_error);
            }
            return;
        }
        switch (v.getId()) {
            case R.id.item_tv_pta_pay:
                //购买
                if (mPtaSuitItemList == null || mPtaSuitItemList.size() == 0) {
                    return;
                }
                HashMap<String, String> params = new HashMap<>();
                params.put("female_guest_id", mFemaleGuestId);
                params.put("dress_id", mPtaSuitItemList.get(mSelectPos).get_id());
                params.put("dress_time", mPtaSuitItemList.get(mSelectPos).getPrice_coins_list().get(mPriceSelectPos).getDays());
                MultiTracker.onEvent(TrackConstants.B_BUY_DRESS_CLICK, params);
                if (checkNotEnoughCoinTip()) {
                    return;
                }
                if (!TextUtils.isEmpty(mCartoonSuitId) && !mCartoonSuitId.equals(mPtaSuitItemList.get(mSelectPos).get_id())) {
                    new CommonDialogBuilder()
                            .setWithCancel(true)
                            .setTitle(getString(R.string.dialog_pta_pay_title))
                            .setDesc(getString(R.string.dialog_pta_pay_tip))
                            .setCancel(getString(R.string.cancel))
                            .setConfirm(getString(R.string.sure))
                            .setListener(new CommonActionListener() {
                                @Override
                                public void onConfirm() {
                                    HashMap<String, String> params = new HashMap<>();
                                    params.put("female_guest_id", mFemaleGuestId);
                                    params.put("dress_id", mPtaSuitItemList.get(mSelectPos).get_id());
                                    params.put("dress_time", mPtaSuitItemList.get(mSelectPos).getPrice_coins_list().get(mPriceSelectPos).getDays());
                                    MultiTracker.onEvent(TrackConstants.B_BUY_DRESS_ALERT_EXPOSURE, params);
                                    buyCartoonSuit();
                                }

                                @Override
                                public void onCancel() {

                                }
                            }).create().show(getActivity().getSupportFragmentManager(), CommonDialogFragment.class.getSimpleName());
                    return;
                }
                buyCartoonSuit();
                break;
            case R.id.item_tv_recharge:
                if (mRechargeCoinDialog.isAdded()) {
                    return;
                }
                mRechargeCoinDialog.setPositon(POSITION_DRESS_ALERT);
                mRechargeCoinDialog.show(getActivity().getSupportFragmentManager(), RechargeCoinDialog.class.getName());
                break;
        }
    }

    @Subscribe(threadMode = ThreadMode.MainThread)
    public void onEventBus(EventCenter eventCenter) {
        if (null != eventCenter) {
            if (eventCenter.getEventCode() == Constants.EVENT_CODE_COIN_CHANGE) {
                String coin = (String) eventCenter.getData();
                tv_user_coin.setText(coin);
            }
        }
    }

    /**
     * 购买装扮
     */
    private void buyCartoonSuit() {
        Map<String, Object> map = new HashMap<>();
        map.put("token", AccountUtil.getInstance().getAccount_token());
        map.put("target_user_id", mFemaleGuestId);
        map.put("price", Integer.valueOf(tv_pta_pay_coin.getText().toString()));
        map.put("suit_id", mPtaSuitItemList.get(mSelectPos).get_id());
        map.put("days", Integer.valueOf(mPtaSuitItemList.get(mSelectPos).getPrice_coins_list().get(mPriceSelectPos).getDays()));
        RetrofitX.Companion.url(ApiUrl.PTA_BUY_SUIT).paramMap(map).postJSON().observeOn(AndroidSchedulers.mainThread()).subscribe(new ObjectObserver<BaseBean>() {
            @Override
            public void failed(@NotNull BaseBean result) {
                HashMap<String, String> params = new HashMap<>();
                params.put("female_guest_id", mFemaleGuestId);
                params.put("dress_id", mPtaSuitItemList.get(mSelectPos).get_id());
                params.put("dress_time", mPtaSuitItemList.get(mSelectPos).getPrice_coins_list().get(mPriceSelectPos).getDays());
                MultiTracker.onEvent(TrackConstants.B_BUY_DRESS_FAILED, params);
                Toast.makeText(getActivity(), R.string.dialog_pta_pay_failed_tip, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void success(@NotNull BaseBean result) {
                HashMap<String, String> params = new HashMap<>();
                params.put("female_guest_id", mFemaleGuestId);
                params.put("dress_id", mPtaSuitItemList.get(mSelectPos).get_id());
                params.put("dress_time", mPtaSuitItemList.get(mSelectPos).getPrice_coins_list().get(mPriceSelectPos).getDays());
                MultiTracker.onEvent(TrackConstants.B_BUY_DRESS_SUCCESS, params);
                Toast.makeText(getActivity(), R.string.dialog_pta_pay_success_tip, Toast.LENGTH_SHORT).show();
                dismissDialog();
            }
        });
    }

    /**
     * 校验金币数量
     */
    private boolean checkNotEnoughCoinTip() {
        BigDecimal bigDecimal1 = new BigDecimal(mUserCoin);
        BigDecimal bigDecimal2 = new BigDecimal(tv_pta_pay_coin.getText().toString());
        if (bigDecimal1.compareTo(bigDecimal2) < 0) {
            Toast.makeText(getActivity(), getString(R.string.couple_no_enough_coin), Toast.LENGTH_SHORT).show();
            if (!mRechargeCoinDialog.isAdded()) {
                mRechargeCoinDialog.setPositon(POSITION_DRESS_SEND);
                mRechargeCoinDialog.show(getActivity().getSupportFragmentManager(), RechargeCoinDialog.class.getName());
                HashMap<String, String> params = new HashMap<>();
                params.put("female_guest_id", mFemaleGuestId);
                params.put("dress_id", mPtaSuitItemList.get(mSelectPos).get_id());
                params.put("dress_time", mPtaSuitItemList.get(mSelectPos).getPrice_coins_list().get(mPriceSelectPos).getDays());
                MultiTracker.onEvent(TrackConstants.B_BUY_DRESS_PAY_ALERT_EXPOSURE, params);
            }
            return true;
        }
        return false;
    }

    private void changePtaItemStatus(boolean isClickSuitItem, int position) {
        mSelectPos = position;
        if (isClickSuitItem) {
            tv_pta_pay_coin.post(new Runnable() {
                @Override
                public void run() {
                    mPriceSelectPos = 0;
                    tv_pta_pay_coin.setText(mPtaSuitItemList.get(mSelectPos).getPrice_coins_list().get(0).getCoin());
                }
            });
        }
        mPtaDurationItemAdapter.update(mPtaSuitItemList.get(mSelectPos).getPrice_coins_list());
        mCommonAdapter.notifyDataSetChanged();
    }
}
