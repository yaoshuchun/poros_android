package com.liquid.poros.ui.fragment.live.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.liquid.library.retrofitx.RetrofitX
import com.liquid.poros.entity.BoostLotteryRecordInfo
import com.liquid.poros.http.ApiUrl
import com.liquid.poros.http.observer.ObjectObserver
import com.liquid.poros.http.utils.getPoros

/**
 * 助力抽奖记录
 */
class BoostLotteryRecordVM : ViewModel() {
    val boostLotteryRecordLiveData: MutableLiveData<BoostLotteryRecordInfo> by lazy { MutableLiveData<BoostLotteryRecordInfo>() }
    val boostLotteryRecordFail: MutableLiveData<BoostLotteryRecordInfo> by lazy { MutableLiveData<BoostLotteryRecordInfo>() }


    /**
     * 助力抽奖记录
     */
    fun boostLotteryRecord(boostLotteryType: String, lastId: String) {
        RetrofitX.url(ApiUrl.BOOST_LOTTERY_RECORD)
                .param("boost_lottery_type", boostLotteryType)
                .param("last_id", lastId)
                .getPoros()
                .subscribe(object : ObjectObserver<BoostLotteryRecordInfo>() {
                    override fun success(result: BoostLotteryRecordInfo) {
                        boostLotteryRecordLiveData.postValue(result)
                    }

                    override fun failed(result: BoostLotteryRecordInfo) {
                        boostLotteryRecordFail.postValue(result)
                    }

                })
    }

}