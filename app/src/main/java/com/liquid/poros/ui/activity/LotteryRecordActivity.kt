package com.liquid.poros.ui.activity

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.request.RequestOptions
import com.liquid.base.adapter.CommonAdapter
import com.liquid.base.adapter.CommonViewHolder
import com.liquid.base.utils.LogOut
import com.liquid.base.utils.ToastUtil
import com.liquid.poros.R
import com.liquid.poros.analytics.utils.MultiTracker
import com.liquid.poros.base.BaseActivity
import com.liquid.poros.constant.TrackConstants
import com.liquid.poros.entity.LotteryInfoList.LotterryInfo
import com.liquid.poros.entity.LotteryInfoModel
import com.liquid.poros.utils.AccountUtil
import com.liquid.poros.utils.ScreenUtil
import com.liquid.poros.widgets.CustomHeader
import java.util.*

//中奖纪录页面
@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class LotteryRecordActivity : BaseActivity() {
    var recyclerView: RecyclerView? = null
    var commonAdapter: CommonAdapter<LotterryInfo>? = null
    var ivLeft : ImageView ?= null
    var customHeader: CustomHeader?= null
    var mGuestId: String? = ""
    override fun getPageId(): String {
        return TrackConstants.P_LOTTERY_RECORD
    }

    override fun parseBundleExtras(extras: Bundle) {
        mGuestId = extras.getString("guest_id")
    }
    override fun getContentViewLayoutID(): Int {
        return R.layout.activity_lottery_record
    }

    override fun initViewsAndEvents(savedInstanceState: Bundle?) {
        customHeader = findViewById(R.id.tv_header);
        customHeader?.setTitile("中奖记录")

        recyclerView = findViewById(R.id.recyclerView)
        recyclerView?.layoutManager = LinearLayoutManager(this)
        commonAdapter = object : CommonAdapter<LotterryInfo>(this@LotteryRecordActivity, R.layout.item_lottery_record_list) {
            override fun convert(holder: CommonViewHolder, lotterryInfo: LotterryInfo, pos: Int) {
                val img = holder.getView<ImageView>(R.id.img)
                val giftName = holder.getView<TextView>(R.id.gift_name)
                val lotteryTimeTv = holder.getView<TextView>(R.id.lottery_time_tv)

                Glide.with(this@LotteryRecordActivity).applyDefaultRequestOptions(RequestOptions()
                        .placeholder(R.mipmap.img_default_avatar)
                        .apply(RequestOptions.bitmapTransform(CircleCrop()))
                        .override(ScreenUtil.dip2px(24f), ScreenUtil.dip2px(24f))
                        .error(R.mipmap.img_default_avatar)).load(lotterryInfo.gift_image).into(img)

                giftName.text = lotterryInfo.gift_name
                lotteryTimeTv.text = lotterryInfo.create_time
            }
        }
        recyclerView?.adapter = commonAdapter
        val lotteryInfoModel = getActivityViewModel(LotteryInfoModel::class.java)
        lotteryInfoModel.getLotteryList()
        lotteryInfoModel.success.observe(this, {
            val record_list: ArrayList<LotterryInfo>? = it.record_list
            LogOut.debug("213", "record_list = " + record_list)
            if (record_list?.size!! <= 0) {
                ToastUtil.show("暂无中奖数据")
            }
            commonAdapter?.updateData(record_list);
        })
        lotteryInfoModel.fail.observe(this, { ToastUtil.show("数据获取异常，请稍后尝试") })

        ivLeft = findViewById(R.id.iv_left)
        ivLeft?.setOnClickListener(View.OnClickListener {
            finish()
        })
    }

    override fun getParams(): HashMap<String?, String?> {
        val params = HashMap<String?, String?>()
        params["user_id"] = AccountUtil.getInstance().userId
        params["guest_id"] = mGuestId
        return params
    }
}