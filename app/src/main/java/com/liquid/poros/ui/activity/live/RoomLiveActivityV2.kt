package com.liquid.poros.ui.activity.live

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.text.Html
import android.text.TextUtils
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.animation.DecelerateInterpolator
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.annotation.DrawableRes
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.blankj.utilcode.util.AppUtils
import com.blankj.utilcode.util.SizeUtils
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.liquid.base.adapter.CommonAdapter
import com.liquid.base.adapter.CommonViewHolder
import com.liquid.base.event.EventCenter
import com.liquid.base.guideview.Component
import com.liquid.base.guideview.DimenUtil
import com.liquid.base.guideview.GuideBuilder
import com.liquid.base.utils.LogOut
import com.liquid.base.utils.ToastUtil
import com.liquid.im.kit.bean.CoupleGiftBoostBean
import com.liquid.im.kit.custom.CpInviteMaleAttachment
import com.liquid.im.kit.custom.DaoliuAttachment
import com.liquid.im.kit.custom.JoinRoomAttachment
import com.liquid.im.kit.custom.RoomActionAttachment
import com.liquid.im.kit.permission.MPermission
import com.liquid.im.kit.permission.annotation.OnMPermissionGranted
import com.liquid.poros.AvInterface
import com.liquid.poros.BuildConfig
import com.liquid.poros.R
import com.liquid.poros.adapter.ChatRoomMessageAdapter
import com.liquid.poros.analytics.utils.MultiTracker
import com.liquid.poros.base.BaseActivity
import com.liquid.poros.constant.*
import com.liquid.poros.entity.*
import com.liquid.poros.helper.AvHelper
import com.liquid.poros.helper.LiveRoomListener
import com.liquid.poros.ui.activity.live.view.GiftAnimationPopupWindow
import com.liquid.poros.ui.activity.live.view.LiveRoomDisconnectDialog
import com.liquid.poros.ui.activity.live.view.LivingView
import com.liquid.poros.ui.activity.live.viewmodel.RoomLiveViewModel
import com.liquid.poros.ui.dialog.LiveMultiGiftSendDialog
import com.liquid.poros.ui.dialog.RechargeCoinDialog
import com.liquid.poros.ui.floatball.FloatManager
import com.liquid.poros.ui.floatball.VoiceChatPlayingBallManager
import com.liquid.poros.ui.floatball.rom.*
import com.liquid.poros.ui.fragment.AppraiseAnchorDialogfragment
import com.liquid.poros.ui.fragment.dialog.common.CommonActionListener
import com.liquid.poros.ui.fragment.dialog.common.CommonDialogBuilder
import com.liquid.poros.ui.fragment.dialog.common.CommonDialogFragment
import com.liquid.poros.ui.fragment.dialog.live.AnchorGiftHelpDetailsPW
import com.liquid.poros.ui.fragment.dialog.live.BoostGiftRewardPW
import com.liquid.poros.ui.fragment.dialog.live.LiveInviteMicDialogFragment
import com.liquid.poros.ui.fragment.dialog.live.PrivateRoomRechargeDialogFragment
import com.liquid.poros.ui.fragment.dialog.live.PrivateRoomRechargeDialogFragment.PrivateRoomRechargeListener
import com.liquid.poros.ui.popupwindow.FemaleUserInfoPopWindow
import com.liquid.poros.utils.*
import com.liquid.poros.utils.KeyboardHelper.SoftKeyboardStateListener
import com.liquid.poros.widgets.ComponentView
import com.liquid.poros.widgets.TextSwitcherAnimation
import com.netease.lava.nertc.sdk.NERtc
import com.netease.lava.nertc.sdk.NERtcConstants
import com.netease.lava.nertc.sdk.NERtcEx
import com.netease.lava.nertc.sdk.stats.*
import com.netease.lava.nertc.sdk.video.NERtcVideoView
import com.netease.neliveplayer.playerkit.common.net.NetworkUtil
import com.netease.neliveplayer.playerkit.sdk.LivePlayer
import com.netease.neliveplayer.playerkit.sdk.LivePlayerObserver
import com.netease.neliveplayer.playerkit.sdk.PlayerManager
import com.netease.neliveplayer.playerkit.sdk.model.*
import com.netease.neliveplayer.sdk.model.NEAutoRetryConfig.OnRetryListener
import com.netease.nimlib.sdk.NIMClient
import com.netease.nimlib.sdk.Observer
import com.netease.nimlib.sdk.RequestCallback
import com.netease.nimlib.sdk.ResponseCode
import com.netease.nimlib.sdk.chatroom.ChatRoomMessageBuilder
import com.netease.nimlib.sdk.chatroom.ChatRoomService
import com.netease.nimlib.sdk.chatroom.ChatRoomServiceObserver
import com.netease.nimlib.sdk.chatroom.model.ChatRoomInfo
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage
import com.netease.nimlib.sdk.chatroom.model.EnterChatRoomData
import com.netease.nimlib.sdk.chatroom.model.EnterChatRoomResultData
import com.netease.nimlib.sdk.msg.constant.SessionTypeEnum
import de.greenrobot.event.EventBus
import de.greenrobot.event.Subscribe
import de.greenrobot.event.ThreadMode
import kotlinx.android.synthetic.main.activity_room_live_new.*
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit

/**
 * 视频房Activity V2版本
 */
class RoomLiveActivityV2 : BaseActivity() {
    private var info: CpInviteMaleAttachment? = null
    private var isChannelCpInvite: Boolean = false
    private var roomLiveViewModel: RoomLiveViewModel? = null
    private val PK_STATUS_PK_DONGING = 0
    private val PK_STATUS_PK_END = 1
    private val PK_STATUS_PK_CLEAR = 2
    var mRoomId: String? = null //视频放ID
    private lateinit var mRoomInfo: CoupleRoom

    private val mAdapter: ChatRoomMessageAdapter by lazy { ChatRoomMessageAdapter(this) }
    private var mLivePlayer: LivePlayer? = null
    private var canJoinMic = false
    private var toAnchor = false
    private var imRoomId: String? = null  //Im聊天框RoomId
    private var imVideoToken: String? = null
    var anchorId: String? = null
    private var giftNum = 0
    private var giftToPos = 0
    private var giftFrom: String? = null
    private val rechargeCoinDialog: RechargeCoinDialog by lazy {
        val dialog = RechargeCoinDialog()
        dialog.setRoomId(mRoomId)
        dialog
    }
    var mAvManager: AvInterface? = null
    var mAvHelper: AvHelper? = null

    //女嘉宾PopupWindow
    private val femaleUserInfoPopWindow: FemaleUserInfoPopWindow by lazy {
        FemaleUserInfoPopWindow(
            this
        )
    }

    //女嘉宾信息展示过
    private var isFemaleUserInfoShown: Boolean = false
    private val mMsgObserver: MsgObserver by lazy { MsgObserver() }
    private var mFirstInviteMicTime: Long = Long.MAX_VALUE //第一次展示聊天对话框的时间

    //是否有视频上麦卡
    private var isEnoughCard = false

    //专属视频房心跳
    private var mScheduledExecutorService: ScheduledExecutorService? = null

    private var mRoomType = 0
    private var mPkStatus = 0
    private var mGiftPos = 0
    private val mLiveInviteMicDialogFragment = LiveInviteMicDialogFragment.newInstance()


    private var my_session_id = "unkown"
    private var isFirstIn = true
    private var invite_id: String? = null
    private var source: String? = null
    private var audience_from: String? = null
    private var is_new: Boolean? = false
    private var enter_timp: Long = 0
    private var ifSendGift = false
    private val micUserInfos = arrayOfNulls<MicUserInfo>(3) //0号位 主持人  1号位 男嘉宾 2号位 女嘉宾
    private var networkQuality = -1
    var lastUpdateVolumeTime: Long = 0 //上次更新音量的时间
    private var roomInfo: ChatRoomInfo? = null
    private var joinTime: Long = 0
    private val liveRoomListener: LiveRoomListener by lazy { RoomListener() }
    private val giftAnimationDialog: GiftAnimationPopupWindow by lazy {
        GiftAnimationPopupWindow(
            this
        )
    }
    private val liveRoomDisconnectDialog: LiveRoomDisconnectDialog by lazy {
        val dialog = LiveRoomDisconnectDialog(this)
        dialog.reConnectListener = {
            if (mLivePlayer != null) {
                mLivePlayer = null
                initPullLivePlayer()
            } else if (mAvHelper != null) {
                mAvHelper!!.joinAvRoom(this)
                registerObservers(true)
            }
            dialog.dismiss()
        }
        dialog
    }
    private val mPrivateRoomRechargeDialogFragment by lazy {
        val dialog = PrivateRoomRechargeDialogFragment.newInstance()
        dialog.setListener(object : PrivateRoomRechargeListener {
            override fun onRecharge() {
                if (rechargeCoinDialog.isAdded) {
                    return
                }
                rechargeCoinDialog.show(supportFragmentManager, RechargeCoinDialog::class.java.name)
                rechargeCoinDialog.setWindowFrom("apply_private_room_not_enough_dialog")
            }

            override fun onCancel() {
                //倒计时结束  执行下麦并退出专属房
                if (micUserInfos[1] != null) {
                    roomLiveViewModel?.quitMic(mRoomId)
                }
                finish()
            }
        })
        dialog
    }

    private val buyCpFragment: LiveMultiGiftSendDialog by lazy {
        val fragment = LiveMultiGiftSendDialog(this)
        fragment
    }

    //这里直播可以用 LivePlayerObserver 点播用 VodPlayerObserver
    private val playerObserver: LivePlayerObserver by lazy { SimpleLivePlayerObserver() }

    //助力礼物列表
    private var mBoostListCommonAdapter: CommonAdapter<CoupleGiftBoostBean>? = null

    //助力礼物详情
    private val anchorGiftHelpDetailsPW: AnchorGiftHelpDetailsPW by lazy {
        AnchorGiftHelpDetailsPW(
            this
        )
    }

    //助力礼物轮播
    private var mTextSwitcherAnimation: TextSwitcherAnimation? = null

    //助力抽奖
    private val boostGiftRewardPW: BoostGiftRewardPW by lazy { BoostGiftRewardPW(this) }

    //助力宝箱抽奖次数
    private var boostLotteryCount: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (BuildConfig.DEBUG) {
            ToastUtil.show("当前为RoomLive重构V2页面")
        }
    }

    override fun parseBundleExtras(extras: Bundle) {
        mRoomId = extras.getString(Constants.ROOM_ID)
        imRoomId = extras.getString(Constants.IM_ROOM_ID)
        invite_id = extras.getString(DaoliuAttachment.INVITE_ID)
        source = extras.getString(Constants.IM_ROOM_SOURCE)
        mRoomType = extras.getInt(Constants.ROOM_TYPE, ROOM_TYPE_NORMAL)
        audience_from = extras.getString(Constants.AUDIENCE_FROM)
        is_new = extras.getBoolean(Constants.IS_NEW)
        AccountUtil.getInstance().auto_login(null, null)//请求用户信息
        LogOut.debug("RoomLiveActivity", "audience_from = $audience_from")
    }

    override fun initViewsAndEvents(savedInstanceState: Bundle?) {
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON) //应用运行时，保持屏幕高亮，不锁屏
        initView()
        enterChatRoom()
        showLoading()
        val keyboardHelper = KeyboardHelper(findViewById(R.id.ll_container))
        keyboardHelper.addSoftKeyboardStateListener(object : SoftKeyboardStateListener {
            override fun onSoftKeyboardOpened(keyboardHeightInPx: Int) {
                //打开
            }

            override fun onSoftKeyboardClosed() {
                hideKeyBoard()
                ll_live_input.visibility = View.VISIBLE
                ll_normal_input.visibility = View.GONE
                rv_message_list.scrollToPosition(mAdapter.itemCount - 1)
            }
        })
        roomLiveViewModel = getActivityViewModel(RoomLiveViewModel::class.java)
        roomLiveViewModel?.getRoomInfo(mRoomId!!, invite_id, source, is_new)
//        //上麦&专属上麦列表
//        roomLiveViewModel?.getMicList(mRoomId!!, 0)
//        roomLiveViewModel?.getPrivateMicList(mRoomId!!, 0)
        enter_timp = System.currentTimeMillis()
        registerViewModelObserver()
        this.mTextSwitcherAnimation = TextSwitcherAnimation(ts_boost_gift_switch)
    }

    /**
     * 注册ViewModel Observer
     */
    private fun registerViewModelObserver() {
        roomLiveViewModel?.cpRoomLiveData?.observe(this, {
            updateRoomInfo(it)
        })
        roomLiveViewModel?.sendGiftSuccessLiveData?.observe(this) {
            sendGiftSuccess(it)
        }
        roomLiveViewModel?.sendGiftFailedLiveData?.observe(this) {
            sendGiftFailed(it)
        }
        roomLiveViewModel?.cpFailedLiveData?.observe(this) {
            closeLoading()
            if (it.code == 17006) {
                if (rechargeCoinDialog.isAdded) {
                    return@observe
                }
                rechargeCoinDialog.show(supportFragmentManager, RechargeCoinDialog::class.java.name)
                rechargeCoinDialog.setWindowFrom("buy_mic_coin_not_enough")
                showMessage(it.msg)
            }
        }
        roomLiveViewModel?.quitMicSuccessLiveData?.observe(this) {
            onPrivateRoomQuitMic()
        }
        roomLiveViewModel?.quitMicFailedLiveData?.observe(this) {
            showMessage(it.msg)
        }
        roomLiveViewModel?.joinMicSuccessLiveData?.observe(this) {
            onJoinMicSuccess(it)
        }
        roomLiveViewModel?.joinMicFailedLiveData?.observe(this) {
            showMessage(it.msg)
        }
        roomLiveViewModel?.joinPrivateMicSuccessLiveData?.observe(this) {
            onPrivateRoomMicSuccess(it)
        }
        roomLiveViewModel?.joinPrivateMicFailedLiveData?.observe(this) {
            onPrivateRoomMicFailed(it)
        }
        roomLiveViewModel?.enterPrivateSuccessLiveData?.observe(this) {
            trackApplyRoomSuccessParams("success")
        }
        roomLiveViewModel?.enterPrivateFailedLiveData?.observe(this) {
            if (it.code == 17044) {
                onNotEnoughCoin(true)
            }
            trackApplyRoomSuccessParams("failed")
            showMessage(it.msg)
        }
        roomLiveViewModel?.privateJHeartBeatSuccessLiveData?.observe(this) {
            closeLoading()
            onPrivateRoomHeartBeatSuccess(it)
        }
        roomLiveViewModel?.privateJHeartBeatFailedLiveData?.observe(this) {
            closeLoading()
            onPrivateRoomJoinMicFailed()
            showMessage(it.msg)
        }
        roomLiveViewModel?.joinCpGroupSuccessLiveData?.observe(this) {
            joinCpGroupSuccess()
        }
    }

    /**
     * 初始化3个音视频View
     */
    private fun initView() {
        initClickListener()
        //随机设置背景
        val resId =
            resources.getIdentifier("bg_live_random${(0..6).random()}", "mipmap", packageName)
        rv_message_list!!.setBackgroundResource(resId)
        rv_message_list.setHasFixedSize(true)
        rv_message_list!!.layoutManager = LinearLayoutManager(this)
        rv_message_list!!.adapter = mAdapter

        showGiftPackageGuide()
    }

    /**
     * 初始化ClickListener
     */
    private fun initClickListener() {
        back.setOnClickListener {
            leaveRoom()
        }
        tv_group_operation.setOnClickListener {
            if (true != mRoomInfo.cpgroup_info?.user_in_group && !StringUtil.isEquals(
                    tv_group_operation.text as String?,
                    "已加团"
                )
            ) {
                roomLiveViewModel?.postJoinCpGroup(mRoomId!!)
                isChannelCpInvite = true

                val params = HashMap<String?, String?>()
                params["anchor_id"] = mRoomInfo.anchor_info?.user_id
                params["guest_id"] = mRoomInfo.pos_2_info?.user_id
                MultiTracker.onEvent(TrackConstants.B_JOIN_GROUP_CLICK, params)
            }
        }
        ll_live_input.setOnClickListener {
            ll_normal_input!!.visibility = View.VISIBLE
            ll_live_input!!.visibility = View.GONE
            showInputMethod(et_content)
        }
        tv_send.setOnClickListener {
            val content = et_content.text.toString()
            if (content.isBlank()) {
                return@setOnClickListener
            }
            val message = ChatRoomMessageBuilder.createChatRoomTextMessage(imRoomId, content)
            val map: MutableMap<String, Any> = HashMap()
            map["user_level"] = AccountUtil.getInstance().userInfo.user_level
            map["from"] = "client"
            map["is_super_admin"] = AccountUtil.getInstance().userInfo.isIs_super_admin
            map["user_charm"] = AccountUtil.getInstance().userInfo.user_charm
            message.remoteExtension = map
            if (Constants.getUseTestApi()) {
                message.env = "poros_test"
            } else {
                message.env = "poros_prod"
            }
            val msgParams = HashMap<String?, String?>()
            msgParams["user_id"] = AccountUtil.getInstance().userId
            msgParams["msg_content"] = content
            msgParams["msg_content_id"] = message.uuid.toString()
            msgParams["live_room_id"] = imRoomId
            MultiTracker.onEvent("b_video_room_send_msg_click", msgParams)
            NIMClient.getService(ChatRoomService::class.java).sendMessage(message, false)
                .setCallback(object : RequestCallback<Void?> {
                    override fun onSuccess(aVoid: Void?) {
                        MultiTracker.onEvent("b_video_room_send_msg_success", msgParams)
                    }

                    override fun onFailed(code: Int) {
                        MultiTracker.onEvent("b_video_room_send_msg_fail", msgParams)
                        when (code) {
                            ResponseCode.RES_CHATROOM_MUTED.toInt() -> {
                                showMessage("用户被禁言")
                            }
                            ResponseCode.RES_CHATROOM_ROOM_MUTED.toInt() -> {
                                showMessage("全体禁言")
                            }
                            else -> {
                                showMessage("消息发送失败")
                            }
                        }
                    }

                    override fun onException(throwable: Throwable) {
                        MultiTracker.onEvent("b_video_room_send_msg_fail", msgParams)
                        showMessage("消息发送失败")
                    }
                })
            mAdapter.add(message)
            if (mAdapter.datas.size > 0) {
                rv_message_list!!.smoothScrollToPosition(mAdapter.itemCount - 1)
            }
            et_content!!.setText("")
        }
        iv_live_input_gift.setOnClickListener {
            val giftParams = HashMap<String?, String?>()
            val micUserInfo = micUserInfos[2]
            if (micUserInfo != null) {
                showSendGiftView(micUserInfo, "iv_input", 2, micUserInfo.user_id)
                giftParams["target_id"] = micUserInfo.user_id
                giftParams["is_on_mic"] = "true"
            } else {
                showMessage(getString(R.string.live_room_input_gift_tip))
                giftParams["is_on_mic"] = "false"
            }
            giftParams["live_room_id"] = mRoomId
            MultiTracker.onEvent(TrackConstants.B_VIDEO_GIFT_CLICK, giftParams)
        }
        iv_switch_to_spci.setOnClickListener {
            switchToSpeciData()
            if (StringUtil.isNotEmpty(micUserInfos[1]?.user_id) && AccountUtil.getInstance().userId != micUserInfos[1]?.user_id) {
                ToastUtil.show("有其他男用户正在麦上，无法转专属")
                return@setOnClickListener
            }
            if (StringUtil.isEmpty(micUserInfos[2]?.user_id)) {
                ToastUtil.show("没有上麦女嘉宾，无法转专属")
                return@setOnClickListener
            }
            showAgreePrivateApply()
        }
    }

    //金牌师傅转专属点击
    private fun switchToSpeciData() {
        val params = HashMap<String?, String?>()
        params["anchor_id"] = micUserInfos[0]?.user_id
        params["female_guest_id"] = micUserInfos[2]?.user_id
        MultiTracker.onEvent(TrackConstants.B_TRANSFER_EXCLUSIVE_CLICK, params)
    }

    /**
     * 更新视频放信息
     */
    private fun updateRoomInfo(roomInfo: CoupleRoom) {
        closeLoading()
        if (!roomInfo.is_online) {
            closeRoom()
            return
        }
        //充值后需要更新金币数量等信息，会刷新房间信息接口
        buyCpFragment?.setRoomId(mRoomId)
        buyCpFragment?.setAudienceFrom(audience_from)
        buyCpFragment?.updateCoinCount(roomInfo?.coin?.toString())
        mRoomInfo = roomInfo
        imRoomId = roomInfo.im_room_id
        imVideoToken = roomInfo.im_video_token
        canJoinMic = roomInfo.gender == 1
        anchorId = roomInfo.anchor_info?.user_id
        mRoomType = roomInfo.room_type
        mPkStatus = roomInfo.pk_status
        if (roomInfo.room_type == ROOM_TYPE_PRIVATE) {
            room_desc.text = roomInfo.room_desc
            room_desc.visibility = View.VISIBLE
            rl_cp_group_title.visibility = View.GONE
        } else {
            room_desc.visibility = View.GONE
            rl_cp_group_title.visibility = View.VISIBLE
            val options = RequestOptions()
                .placeholder(R.mipmap.img_default_avatar)
                .override(SizeUtils.dp2px(26f), SizeUtils.dp2px(26f))
                .error(R.mipmap.img_default_avatar)
            Glide.with(this@RoomLiveActivityV2)
                .load(roomInfo.anchor_info?.avatar_url)
                .apply(options)
                .into(iv_master_avatar)
            tv_master_nickname?.text = roomInfo.anchor_info?.nick_name
            tv_fans_group_num?.text =
                getString(R.string.cp_group_num, roomInfo.cpgroup_info?.members_count)
            if (true == roomInfo.cpgroup_info?.user_in_group) {
                tv_group_operation?.text = "已加团"
            } else {
                tv_group_operation?.text = "加团"
            }
        }
        if (roomInfo.room_type == ROOM_TYPE_NORMAL &&
            StringUtil.isNotEmpty(AccountUtil.getInstance().goldMasterId)
        ) {
            iv_switch_to_spci.visibility = View.VISIBLE
        } else {
            iv_switch_to_spci.visibility = View.GONE
        }
        handleMicUsers(roomInfo)
        //进入房间时通过拉流获取图像
        if (mAvHelper == null) {
            initPullLivePlayer()
        }

        iv_voice_chat_playing.isVisible = roomInfo.only_voice_room && roomInfo.voice_chatting != 0

    }

    override fun getPageId(): String {
        return TrackConstants.PAGE_ROOM_LIVE
    }

    override fun getContentViewLayoutID(): Int {
        return R.layout.activity_room_live_new
    }

    private fun transformString(strings: Array<String>?): StringBuffer {
        val stringBuffer = StringBuffer()
        if (strings != null && strings.isNotEmpty()) {
            for (i in strings.indices) {
                stringBuffer.append("    ")
                stringBuffer.append(i + 1)
                stringBuffer.append(",")
                stringBuffer.append(strings[i])
            }
        }
        return stringBuffer
    }

    /**
     * 处理连麦用户
     */
    private fun handleMicUsers(roomInfo: CoupleRoom) {
        micUserInfos[0] = roomInfo.anchor_info
        micUserInfos[1] = roomInfo.pos_1_info
        micUserInfos[2] = roomInfo.pos_2_info
        (micUserInfos[giftToPos])?.isShow_add_friend_button?.let {
            buyCpFragment?.setShowAddFriendButton(
                it
            )
        }
        rl_apply_mic.setData(roomInfo, mRoomId!!, micUserInfos)
        living_anchor.pos = 0
        living_anchor.isVoiceRoom(mRoomInfo.only_voice_room)
        living_anchor.updateViewByUser(mRoomId!!, imRoomId, roomInfo, micUserInfos)
        ll_interaction_boy.pos = 1
        ll_interaction_boy.isVoiceRoom(mRoomInfo.only_voice_room)
        ll_interaction_boy.updateViewByUser(mRoomId!!, imRoomId, roomInfo, micUserInfos)
        ll_interaction_girl.pos = 2
        ll_interaction_girl.isVoiceRoom(mRoomInfo.only_voice_room)
        ll_interaction_girl.updateViewByUser(mRoomId!!, imRoomId, roomInfo, micUserInfos)
        //如果当前音视频已销毁则给当前的男嘉宾下麦
        if (roomInfo.pos_1_info != null) {
            if (StringUtil.isEquals(
                    roomInfo.pos_1_info!!.user_id,
                    AccountUtil.getInstance().userId
                ) && mAvHelper == null && isFirstIn
            ) {
                roomLiveViewModel?.kickMic(mRoomId, roomInfo.pos_1_info!!.user_id)
            }
            isFirstIn = false
        }
        if (roomInfo.pos_2_info != null) {
            //不是PK房则用popupWindow展示女嘉宾的信息,如果展示过女嘉宾信息则不再展示
            //助力详情弹窗显示时,不展示女嘉宾信息弹窗
            if (!isPKRoom && !isFemaleUserInfoShown && !anchorGiftHelpDetailsPW?.isShowing) {
                isFemaleUserInfoShown = true
                femaleUserInfoPopWindow?.setData(roomInfo.pos_2_info!!.user_id)
                femaleUserInfoPopWindow?.showPopupWindow(ll_interaction_girl)
            }
        } else {
            femaleUserInfoPopWindow?.dismiss()
        }
        if (isPKRoom) {
            rl_apply_mic!!.visibility = View.GONE
            updatePkIv()
            if (roomInfo.pk_info != null) {
                updatePkInfo(roomInfo.pk_info!!)
            }
            iv_live_input_gift!!.visibility = View.GONE
            rl_boost_gift.visibility = View.GONE
        } else {
            rl_apply_mic!!.visibility =
                if (mRoomType == ROOM_TYPE_NORMAL) View.VISIBLE else View.GONE
            rl_apply_mic!!.setBackgroundResource(if (mRoomInfo.request_onmic) R.drawable.bg_gradual_14d4d6_24 else R.drawable.bg_gradual_ff8080_24)
            iv_live_input_gift!!.visibility =
                if (roomInfo.show_gift_icon) View.VISIBLE else View.GONE
        }
        if (mRoomType == ROOM_TYPE_PRIVATE) {
            tv_tips!!.text = transformString(Constants.private_room_tips)
            mic_group!!.visibility = View.GONE
            //礼物助力
            updateBoostView(roomInfo.gift_boost_status)
        } else {
            tv_tips!!.text = transformString(Constants.cp_room_watch_user_tips)
            if (isPKRoom) {
                mic_group!!.visibility = View.INVISIBLE
                rl_boost_gift.visibility = View.GONE
            } else {
//                mic_group!!.visibility = View.VISIBLE
                mic_group!!.visibility = View.GONE
                //礼物助力
                updateBoostView(roomInfo.gift_boost_status)
            }
        }
        living_anchor.updateCpGiftInfo(roomInfo.pos_0_rank_top_avatars)
        ll_interaction_girl.updateCpGiftInfo(roomInfo.pos_2_rank_top_avatars)
        //助力礼物列表
        roomInfo.gift_boost_brief_info?.gift_boost_info_list?.let {
            updateBoostGiftList(it)
        }
    }


    /**
     * 助力分桶显示控制
     * 1.为空,对照组
     * 2.false 未设置助力 不显示助力view; true 已设置,显示助力view
     */
    private fun updateBoostView(gift_boost_status: Any) {
        if (gift_boost_status == null) {
            rl_boost_gift.visibility = View.GONE
        } else {
            rl_boost_gift.visibility = if ((true == gift_boost_status)) View.VISIBLE else View.GONE
        }
    }

    /**
     * 助力礼物列表
     */
    private fun updateBoostGiftList(giftBoostList: List<CoupleGiftBoostBean>) {
        rv_boost_gift.visibility =
            if (giftBoostList != null && giftBoostList.isNotEmpty()) View.VISIBLE else View.GONE
        if (rv_boost_gift.adapter == null) {
            mBoostListCommonAdapter =
                object : CommonAdapter<CoupleGiftBoostBean>(this, R.layout.item_live_boost_gift) {
                    override fun convert(
                        holder: CommonViewHolder,
                        giftBoostBean: CoupleGiftBoostBean,
                        pos: Int
                    ) {
                        holder.setText(R.id.item_tv_boost_gift_name, giftBoostBean.gift_name)
                        holder.setText(
                            R.id.item_tv_boost_gift_progress,
                            "(${giftBoostBean.boost_count}/${giftBoostBean.total_count})"
                        )
                        val item_iv_boost_gift = holder.getView<ImageView>(R.id.item_iv_boost_gift)
                        val options = RequestOptions()
                            .placeholder(R.mipmap.img_default_avatar)
                            .error(R.mipmap.img_default_avatar)
                            .override(ScreenUtil.dip2px(14f), ScreenUtil.dip2px(14f))
                        Glide.with(this@RoomLiveActivityV2).load(giftBoostBean.gift_icon)
                            .apply(options).into(item_iv_boost_gift)
                    }
                }
            rv_boost_gift!!.layoutManager = LinearLayoutManager(this)
            mBoostListCommonAdapter?.update(giftBoostList)
            rv_boost_gift!!.adapter = mBoostListCommonAdapter
        } else {
            val adapter = rv_boost_gift.adapter
            if (adapter is CommonAdapter<*>) {
                (adapter as CommonAdapter<CoupleGiftBoostBean>).update(giftBoostList)
                rv_boost_gift.adapter = adapter
            }
        }
        //助力列表消失动画
        boostGiftAnim(rl_boost_gift)
        //助力礼物轮播
        switchBoostGift(giftBoostList)
    }

    /**
     * 助力列表动画
     */
    private fun boostGiftAnim(view: View?) {
        val animatorSetsuofang = AnimatorSet()
        val scaleX = ObjectAnimator.ofFloat(view, "translationY", -500f, 0f)
        val anim = ObjectAnimator.ofFloat(view, "alpha", 0f, 1f)
        animatorSetsuofang.duration = 2000
        animatorSetsuofang.interpolator = DecelerateInterpolator()
        animatorSetsuofang.play(scaleX).with(anim)
        animatorSetsuofang.start()
        animatorSetsuofang.addListener(object : Animator.AnimatorListener {
            override fun onAnimationStart(animator: Animator) {}
            override fun onAnimationEnd(animator: Animator) {
                rl_boost_gift.visibility = View.GONE
                ts_boost_gift_switch.visibility = if (isPKRoom) View.GONE else View.VISIBLE
            }

            override fun onAnimationCancel(animator: Animator) {}
            override fun onAnimationRepeat(animator: Animator) {}
        })
    }

    /**
     * 助力礼物轮播
     */
    private fun switchBoostGift(giftBoostList: List<CoupleGiftBoostBean>) {
        if (giftBoostList.isNullOrEmpty()) {
            return
        }
        var switchTexts = arrayOfNulls<String>(giftBoostList.size)
        for (i in giftBoostList.indices!!) {
            switchTexts[i] =
                "${giftBoostList[i].gift_name}(${giftBoostList[i].boost_count}/${giftBoostList[i].total_count})"
        }
        if (ts_boost_gift_switch != null && ts_boost_gift_switch.childCount < 2) {
            ts_boost_gift_switch.setFactory(ViewSwitcher.ViewFactory {
                val textView = TextView(mContext)
                textView.setTextColor(Color.parseColor("#10BCFF"))
                textView.textSize = 10f
                textView.gravity = Gravity.CENTER
                textView.setSingleLine(true)
                textView.layoutParams = FrameLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT
                )
                textView.setOnClickListener {
                    //助力礼物
                    anchorGiftHelpDetailsPW.setHostHead(
                        mRoomInfo?.anchor_info?.avatar_url,
                        mRoomInfo?.gift_boost_brief_info?.h5,
                        if (mRoomInfo?.pos_2_info == null) "" else mRoomInfo?.pos_2_info?.user_id,
                        mRoomInfo?.anchor_info?.user_id
                    )
                    if (boostLotteryCount == null) {
                        boostLotteryCount =
                            if (mRoomInfo?.lottery_times == null) "0" else mRoomInfo?.lottery_times
                    }
                    anchorGiftHelpDetailsPW.setAudienceFrom(audience_from)
                    anchorGiftHelpDetailsPW.getBoostGiftDetails(
                        mRoomId!!,
                        mRoomInfo?.anchor_info!!.user_id,
                        boostLotteryCount!!
                    )
                    anchorGiftHelpDetailsPW.showPopupWindow()
                    val params: MutableMap<String, String> = java.util.HashMap<String, String>()
                    params["anchor_id"] = mRoomInfo?.anchor_info?.user_id!!
                    MultiTracker.onEvent(TrackConstants.B_ASSIST_CLICK, params)
                }
                textView
            })
        }
        mTextSwitcherAnimation!!.setTexts(switchTexts).create()
        val params: MutableMap<String, String> = java.util.HashMap<String, String>()
        params["anchor_id"] = mRoomInfo?.anchor_info?.user_id!!
        MultiTracker.onEvent(TrackConstants.B_ASSIST_EXPOSURE, params)
    }

    /**
     * 更新PK结果
     */
    private fun updatePKResult() {
        ll_interaction_boy.updatePKResult()
        ll_interaction_girl.updatePKResult()
    }

    /**
     * 更新PK信息
     */
    private fun updatePkInfo(pk_Info: CoupleRoom.PkInfo) {
        ll_interaction_boy.updatePkInfo(pk_Info)
        ll_interaction_girl.updatePkInfo(pk_Info)
        if (mPkStatus == PK_STATUS_PK_END) {
            updatePKResult()
        }
    }

    /**
     * 更新PK icon
     */
    private fun updatePkIv() {
        if (isPKRoom) {
            pk_iv.visibility = View.VISIBLE
            pk_iv.setImageResource(if (mPkStatus == PK_STATUS_PK_DONGING) R.mipmap.icon_pking else R.mipmap.icon_pk)
        } else {
            pk_iv.visibility = View.GONE
        }
    }

    override fun onEventComing(eventCenter: EventCenter<*>) {
        super.onEventComing(eventCenter)
        when (eventCenter.eventCode) {
            Constants.EVENT_CODE_REPORT_SPEAKER -> {
                //暂时无用
            }
            Constants.EVENT_CODE_COIN_CHANGE -> {
                roomLiveViewModel?.getUserLevel()
                buyCpFragment?.addCoinCount(eventCenter.data.toString())
                mLiveInviteMicDialogFragment.updateUserCoin()
                mPrivateRoomRechargeDialogFragment.dismissDialog()
            }
            Constants.EVENT_CODE_UPDATE_GIFT_PACKAGE_STATUS -> showGiftPackageGuide()
        }
    }

    /**
     * 展示送礼对话框
     */
    fun showSendGiftView(micUserInfo: MicUserInfo, from: String?, pos: Int, femaleGuestId: String) {
        giftFrom = from
        mGiftPos = pos
        toAnchor = "anchor" == micUserInfo.room_role
        var giftSource = if (StringUtil.isEquals(source, "auto_cp_invite")) {
            source
        } else {
            null
        }
        showBuyCpDialog(
            femaleGuestId,
            from,
            giftSource,
            micUserInfos,
            toAnchor,
            micUserInfo,
            pos,
            mRoomType
        )
    }

    // 显示键盘布局
    private fun showInputMethod(editTextMessage: EditText?) {
        editTextMessage!!.requestFocus()
        //如果已经显示,则继续操作时不需要把光标定位到最后
        val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(editTextMessage, 0)
    }

    private fun showBuyCpDialog(
        femaleGuestId: String,
        from: String?,
        giftSource: String?,
        micUserInfos: Array<MicUserInfo?>,
        toAnchor: Boolean,
        micUserInfo: MicUserInfo,
        pos: Int,
        mRoomType: Int
    ) {
        try {
            val ft = supportFragmentManager.beginTransaction()
            if (buyCpFragment != null) {
                ft.remove(buyCpFragment)
                ft.commit()
            }
            giftToPos = pos
            buyCpFragment?.show(
                buyCpFragment, this@RoomLiveActivityV2, LiveMultiGiftSendDialog::class.java.name,
                mRoomInfo?.gift_list as ArrayList<MsgUserModel.GiftBean>?,
                mRoomInfo?.gift_num_list as ArrayList<MsgUserModel.GiftNums>?,
                femaleGuestId, from, giftSource, micUserInfos.toList() as ArrayList,
                toAnchor, micUserInfo.isShow_add_friend_button, pos, mRoomType
            )
        } catch (e: Exception) {

        }
    }

    /**
     * 在用户进入视频模式时，需要释放拉流，通过音视频流获取图像
     * 释放拉流
     */
    private fun releasePlayer() {
        mLivePlayer?.registerPlayerObserver(playerObserver, false)
        mLivePlayer?.setupRenderView(null, VideoScaleMode.NONE)
        live_player?.releaseSurface()
        mLivePlayer?.stop()
        mLivePlayer = null
    }

    // 初始化拉流  在C端用户下麦时需要重新拉流
    private fun initPullLivePlayer() {
        if (mLivePlayer != null) {
            return
        }
        val options = VideoOptions()
        options.hardwareDecode = true
        options.isPlayLongTimeBackground = true
        options.bufferStrategy = VideoBufferStrategy.LOW_LATENCY
        mLivePlayer = PlayerManager.buildLivePlayer(this, mRoomInfo.rtmp_pull_url, options)
        LogOut.debug("RoomLiveActivity", "当前拉流地址：${mRoomInfo.rtmp_pull_url}")
        mLivePlayer!!.setupRenderView(live_player, VideoScaleMode.FILL)
        mLivePlayer!!.setMute(false)
        mLivePlayer!!.setVolume(1.0f)
        val autoRetryConfig = AutoRetryConfig()
        autoRetryConfig.count = 3
        autoRetryConfig.delayDefault = 1000
        autoRetryConfig.delayArray = longArrayOf(100, 500, 500)
        autoRetryConfig.retryListener = OnRetryListener { what, extra ->
            if (pb_player != null) {
                pb_player!!.visibility = View.VISIBLE
                closeLoading()
            }
        }
        // 将重试配置设置给播放器
        mLivePlayer!!.setAutoRetryConfig(autoRetryConfig)
        mLivePlayer!!.registerPlayerObserver(playerObserver, true)
        mLivePlayer!!.start()
    }

    /**
     * 当上麦成功以后，回调
     */
    private fun onJoinMicSuccess(roomOnMicV2: RoomOnMicV2) {
        if (mRoomInfo.only_voice_room) {
            showFloatDialog()
        }
        initAvHelperAndJoinRoom()
        isFirstIn = false
        roomLiveViewModel?.getRoomInfo(mRoomId!!)
        if (isChannelCpInvite && StringUtil.isEquals(info?.type, Constants.TYPE_CP_GROUP_INVITE)) {
            isChannelCpInvite = false
            val params = HashMap<String?, String?>()
            params["anchor_id"] = mRoomInfo.anchor_info?.user_id
            params["guest_id"] = mRoomInfo.pos_2_info?.user_id
            params["from"] = "super_cp_team"
            params["is_in_room"] = 1.toString()
            params["is_new_user"] =
                AccountUtil.getInstance().userInfo.isIs_live_unactive_user.toString()
            MultiTracker.onEvent(TrackConstants.B_ACCEPT_CP_INVITE_SUCCESS, params)
        } else {
            val params = HashMap<String?, String?>()
            params["is_new_user"] =
                AccountUtil.getInstance().userInfo.isIs_live_unactive_user?.toString()
            MultiTracker.onEvent(TrackConstants.B_MICROPHONE_SUCCESS, params)
        }
        my_session_id = roomOnMicV2.session_id!!
    }

    /**
     * 送礼成功
     */
    private fun sendGiftSuccess(giftInfo: GiftInfo) {
        if (toAnchor) {
            trackGiftResult(TrackConstants.B_SEND_GIFT_TO_HOST_SUCCESS, giftInfo)
        } else {
            trackGiftResult(TrackConstants.B_SEND_GIFT_TO_GUEST_SUCCESS, giftInfo)
        }
        ifSendGift = true
        if (giftInfo.gift_boost_success && isGiftSwitchVisiable()) {
            //助力礼物成功 埋点
            anchorGiftHelpDetailsPW?.trackHelpBoost()
        }
    }

    /**
     * 送礼失败
     */
    private fun sendGiftFailed(giftInfo: GiftInfo) {
        ToastUtil.show(giftInfo.msg)
        if (giftInfo.code == -1) {
            return
        }
        if (giftInfo.code == 17039) {
            if (rechargeCoinDialog.isAdded) {
                return
            }
            rechargeCoinDialog.setPositon(RechargeCoinDialog.POSITION_SEND_GIFT)
            rechargeCoinDialog.show(supportFragmentManager, RechargeCoinDialog::class.java.name)
            rechargeCoinDialog.setWindowFrom("buy_gift_coin_not_enough")
        }
        if (toAnchor) {
            trackGiftResult(TrackConstants.B_SEND_GIFT_TO_HOST_FAIL, giftInfo)
        } else {
            trackGiftResult(TrackConstants.B_SEND_GIFT_TO_GUEST_FAIL, giftInfo)
        }
    }

    /**
     * 专属房点击申请埋点
     */
    private fun trackApplyRoomSuccessParams(actType: String) {
        val params = HashMap<String?, String?>()
        params["female_guest_id"] = micUserInfos[2]?.user_id ?: ""
        params["room_id"] = mRoomId
        params["anchor_id"] = anchorId
        params["event_time"] = System.currentTimeMillis().toString()
        params["act_type"] = actType
        MultiTracker.onEvent(TrackConstants.B_EXCLUSIVE_AFFIRM_APPLY_BUTTON, params)
    }

    /**
     * 没钱需充值
     */
    private fun onNotEnoughCoin(isApply: Boolean) {
        if (rechargeCoinDialog.isAdded) {
            return
        }
        rechargeCoinDialog.show(supportFragmentManager, RechargeCoinDialog::class.java.name)
        rechargeCoinDialog.setWindowFrom("apply_private_room_not_enough")
        if (!isApply) {
            trackJoinRoomParams("failed")
        }
    }

    /**
     * 上麦 埋点
     */
    private fun trackJoinRoomParams(actType: String) {
        val params = HashMap<String?, String?>()
        params["female_guest_id"] = micUserInfos[2]?.user_id ?: ""
        params["room_id"] = mRoomId
        params["act_type"] = actType
        params["anchor_id"] = anchorId
        params["event_time"] = System.currentTimeMillis().toString()
        MultiTracker.onEvent(TrackConstants.B_EXCLUSIVE_ENGAGEMENT_BUTTON, params)
    }

    /**
     * 专属房心跳成功
     */
    private fun onPrivateRoomHeartBeatSuccess(privateRoomHeartBeatInfo: PrivateRoomHeartBeatInfo) {
        LogOut.debug("RoomLiveActivity", "专属房心跳，当前金币：${privateRoomHeartBeatInfo.coin!!}")
        buyCpFragment.updateCoinCount(privateRoomHeartBeatInfo.coin)
        if (privateRoomHeartBeatInfo.popup != null) {
            rl_private_room.updateRoomCoin(privateRoomHeartBeatInfo.coin!!)
            val popupData = privateRoomHeartBeatInfo.popup!!
            mPrivateRoomRechargeDialogFragment.setDialogInfo(
                popupData.content,
                popupData.desc,
                popupData.countdown
            )
                .show(supportFragmentManager, PrivateRoomRechargeDialogFragment::class.java.name)
        } else {
            rl_private_room.hideRoomCoin()
        }
    }

    /**
     * 专属房心跳失败
     */
    private fun onPrivateRoomJoinMicFailed() {
        if (BuildConfig.DEBUG) {
            ToastUtil.show("专属房心跳失败(API请求接口失败)退出房间")
        }
        //专属房上麦失败
        trackJoinRoomParams("failed")
        finish()
    }

    /**
     * 专属房上麦成功
     */
    private fun onPrivateRoomMicSuccess(privateRoomMicInfo: PrivateRoomMicInfo) {
        initAvHelperAndJoinRoom()
        closeLoading()
        trackJoinRoomParams("success")
        //专属视频房 上麦
        isFirstIn = false
        roomLiveViewModel?.getRoomInfo(mRoomId!!)
        my_session_id = privateRoomMicInfo.session_id!!
        LogOut.debug(
            "RoomLiveActivity",
            "onPrivateRoomMicSuccess -- > my_session_id = $my_session_id"
        )
    }

    /**
     * 专属房上麦失败
     */
    private fun onPrivateRoomMicFailed(privateRoomMicInfo: PrivateRoomMicInfo) {
        closeLoading()
        if (privateRoomMicInfo.code == 17044) {
            onNotEnoughCoin(false)
        } else if (privateRoomMicInfo.code == 17004 || privateRoomMicInfo.code == 17042) {
            //麦上有用户 或已是专属房
            onPrivateRoomJoinMicFailed()
        } else {
            onPrivateRoomJoinMicFailed()
        }
        showMessage(privateRoomMicInfo.msg)
    }

    /**
     * 专属房下麦
     */
    private fun onPrivateRoomQuitMic() {
        //停止计时
        rl_private_room.stopCountTime()
        //停止心跳
        closePrivateRoomHeartBeat()
        showMessage(getString(R.string.live_room_private_room_quit_mic))
        finish()
    }

    /**
     * 加入CP团成功
     */
    private fun joinCpGroupSuccess() {
        ToastUtil.show(AccountUtil.getInstance().userInfo?.nick_name + "加入CP团")
        tv_group_operation.text = "已加团"
    }

    /**
     * 送礼结果Track
     */
    private fun trackGiftResult(eventName: String, giftInfo: GiftInfo) {
        val params = HashMap<String?, String?>()
        params["room_id"] = mRoomId
        params["anchor_id"] = anchorId
        params["audience_from"] = audience_from
        params["gift_num"] = giftInfo.giftNum.toString()
        params["gift_id"] = giftInfo.gift_id.toString()
        params["gift_name"] = giftInfo.giftName
        if (source != null) {
            params["source"] = source
        }
        if (isPKRoom) {
            if (giftToPos == 0) {
                if (micUserInfos[1] != null) {
                    params["left_female_guest_id"] = micUserInfos[1]!!.user_id
                }
                if (micUserInfos[2] != null) {
                    params["right_female_guest_id"] = micUserInfos[2]!!.user_id
                }
            } else {
                if (micUserInfos[giftToPos] != null) {
                    params["female_guest_id"] = micUserInfos[giftToPos]!!.user_id
                }
                params["guest_position"] = giftToPos.toString()
            }
            params["room_type"] = "pk_room"
            params["pk_status"] = if (mPkStatus == PK_STATUS_PK_DONGING) "1" else "2"
        } else {
            params["female_guest_id"] = micUserInfos[2]?.user_id ?: ""
            params["guest_gift_window_from"] = giftFrom
        }
        MultiTracker.onEvent(eventName, params)
    }

    override fun isShowMsgTip(): Boolean {
        return if (micUserInfos[1] == null) {
            true
        } else {
            !(micUserInfos[1]!!.user_id != null && micUserInfos[1]!!.user_id == AccountUtil.getInstance().userId)
        }
    }

    /**
     * 注册网络状态监听
     */
    private fun registerObservers(register: Boolean) {
        if (register) {
            NERtcEx.getInstance().setStatsObserver(LiveNeRtcStatsObserver())
        } else {
            NERtcEx.getInstance().setStatsObserver(null)
        }
    }

    /**
     * 释放音视频资源
     */
    private fun releaseAVHelper() {
        if (mAvHelper != null) {
            registerObservers(false)
            mAvHelper!!.releaseAvRoom()
            mAvHelper = null
        }
    }

    /**
     * 主持人同意专属房申请埋点
     */
    private fun trackAppointmentExposureEvent() {
        val params = HashMap<String?, String?>()
        params["female_guest_id"] = micUserInfos[2]?.user_id ?: ""
        params["room_id"] = mRoomId
        params["anchor_id"] = anchorId
        params["event_time"] = System.currentTimeMillis().toString()
        MultiTracker.onEvent(TrackConstants.B_APPOINTMENT_DIALOG_EXPOSURE, params)
    }

    private fun closeRoom() {
        CommonDialogBuilder()
            .setImageTitleRes(R.mipmap.icon_room_close)
            .setConfirm(getString(R.string.dialog_gotit_tip))
            .setCancelAble(false)
            .setWithCancel(false)
            .setDesc(getString(R.string.dialog_room_close_tip))
            .setListener(object : CommonActionListener {
                override fun onConfirm() {
                    finish()
                }

                override fun onCancel() {}
            }).create().show(supportFragmentManager, CommonDialogFragment::class.java.simpleName)
    }

    /**
     * 是否是我的消息
     */
    private fun isMyMessage(message: ChatRoomMessage): Boolean {
        return message.sessionType == SessionTypeEnum.ChatRoom && message.sessionId != null && message.sessionId == imRoomId
    }

    /**
     * 查找当前在线人数
     */
    private fun fetchOnlineCount() {
        NIMClient.getService(ChatRoomService::class.java).fetchRoomInfo(imRoomId)
            .setCallback(object : RequestCallback<ChatRoomInfo> {
                override fun onSuccess(param: ChatRoomInfo) {
                    if (isFinishing || isDestroyed) {
                        return
                    }
                    tv_online_count!!.text = param.onlineUserCount.toString()
                }

                override fun onFailed(code: Int) {}
                override fun onException(exception: Throwable) {}
            })
    }

    /**
     * 进入聊天室
     */
    private fun enterChatRoom() {
        if (roomInfo != null) {
            return
        }
        NIMClient.getService(ChatRoomServiceObserver::class.java)
            .observeReceiveMessage(mMsgObserver, false)
        NIMClient.getService(ChatRoomServiceObserver::class.java)
            .observeReceiveMessage(mMsgObserver, true)
        NIMClient.getService(ChatRoomService::class.java)
            .enterChatRoomEx(EnterChatRoomData(imRoomId), 10)
            .setCallback(object : RequestCallback<EnterChatRoomResultData> {
                override fun onSuccess(result: EnterChatRoomResultData) {
                    if (isDestroyed || isFinishing) {
                        return
                    }
                    roomInfo = result.roomInfo
                    if (roomInfo == null) {
                        finish()
                        return
                    }
                    fetchOnlineCount()
                    if (System.currentTimeMillis() - joinTime > 3000) {
                        val attachment = JoinRoomAttachment(
                            AccountUtil.getInstance().userId,
                            AccountUtil.getInstance().userInfo.nick_name,
                            AccountUtil.getInstance().userInfo.isIs_super_admin
                        )
                        attachment.user_level = AccountUtil.getInstance().userInfo.user_level
                        attachment.from = "client"
                        val message =
                            ChatRoomMessageBuilder.createChatRoomCustomMessage(imRoomId, attachment)
                        if (Constants.getUseTestApi()) {
                            message.env = "poros_test"
                        } else {
                            message.env = "poros_prod"
                        }
                        NIMClient.getService(ChatRoomService::class.java)
                            .sendMessage(message, false)
                        mAdapter.add(message)
                        joinTime = System.currentTimeMillis()
                    }
                }

                override fun onFailed(code: Int) {}
                override fun onException(exception: Throwable) {}
            })
    }

    @OnMPermissionGranted(BASIC_PERMISSION_REQUEST_CODE)
    fun onBasicPermissionSuccess() {
        if (mRoomType == ROOM_TYPE_PRIVATE) {
            //专属房上麦
            roomLiveViewModel?.fetchPrivateRoomOnMic(mRoomId)
        } else {
            LogOut.debug("RoomLiveActivity", "普通房上麦")
            //普通房上麦
            roomLiveViewModel?.joinMic(false, mRoomId, 20, isEnoughCard, source, invite_id)
            if (StringUtil.isEquals(info?.type, Constants.TYPE_CP_GROUP_INVITE)) {
                val params = HashMap<String?, String?>()
                params["room_id"] = mRoomInfo?.anchor_info?.user_id
                    ?: mRoomInfo?.anchor_info?.user_id
                params["guest_id"] = mRoomInfo?.pos_2_info?.user_id
                params["from"] = "super_cp_team"
                params["is_in_room"] = 1.toString()
                MultiTracker.onEvent(TrackConstants.B_ACCEPT_CP_INVITE, params)
            }

        }
    }

    /**
     * 初始化AVHelper并且加入视频房
     */
    private fun initAvHelperAndJoinRoom() {
        if (mAvHelper == null) {
            mAvHelper = AvHelper(imRoomId!!, imVideoToken!!, false, liveRoomListener)
            var enableVideo = true
            if (null != mRoomInfo) {
                enableVideo = !mRoomInfo.only_voice_room
            }
            mAvHelper!!.joinAvRoom(
                this@RoomLiveActivityV2,
                ll_interaction_boy.videoView,
                enableVideo
            )
            registerObservers(true)
            releasePlayer()
        }
    }

    @Subscribe(threadMode = ThreadMode.MainThread)
    override fun onEventBus(eventCenter: EventCenter<*>) {
        super.onEventBus(eventCenter)
        when (eventCenter.eventCode) {
            Constants.EVENT_CODE_REPORT_LOCALE_SPEAKER -> {
            }
            Constants.EVENT_CODE_INVITE_OPEN_MIC -> {
                //麦上有人则不弹框
                if (micUserInfos[1] != null) {
                    return
                }
                info = eventCenter.data as CpInviteMaleAttachment
                //如果当前邀请的是本人则立马展示
                if (TextUtils.isEmpty(info?.avatar_url)) {
                    info?.coin?.toString()?.let {
                        mLiveInviteMicDialogFragment.setUserInfo(
                            mRoomId,
                            info?.avatar_url,
                            info?.desc,
                            info?.desc,
                            it,
                            info?.source,
                            info?.invite_id
                        )
                            .also {
                                it.account = info!!.invite_id
                                it.anchorId = anchorId
                            }.show(
                                supportFragmentManager,
                                LiveInviteMicDialogFragment::class.java.simpleName
                            )
                    }
                } else {
                    mLiveInviteMicDialogFragment.setUserInfo(
                        mRoomId,
                        info?.anchor_avatar_url,
                        info?.desc,
                        info?.desc,
                        info?.coin.toString(),
                        info?.source,
                        info?.invite_id
                    )
                        .also {
                            it.account = info?.invite_id
                            it.anchorId = anchorId
                        }.show(
                            supportFragmentManager,
                            LiveInviteMicDialogFragment::class.java.simpleName
                        )
                }
                if (StringUtil.isEquals(info?.type, Constants.TYPE_CP_GROUP_INVITE)) {
                    val params = HashMap<String?, String?>()
                    params["room_id"] = info?.room_id
                    params["invite_id"] = info?.invite_id
                    params["from"] = "super_cp_team"
                    params["is_in_room"] = 1.toString()
                    MultiTracker.onEvent(TrackConstants.B_INVITE_MIC_SHOW, params)
                }

            }
            Constants.EVENT_CODE_UPDATE_BOOST_COUNT -> {
                //助力宝箱个数
                var boostCount = eventCenter.data as Int
                boostLotteryCount = boostCount.toString()
            }
        }
    }

    override fun onDestroy() {
        releaseAVHelper()
        LivingView.pkInfo = null
        living_anchor.release()
        ll_interaction_boy.release()
        ll_interaction_girl.release()
        super.onDestroy()
        EventBus.getDefault().post(EventCenter<Any>(Constants.EVENT_CODE_DISPATCH_FEMALE))
        EventBus.getDefault().post(EventCenter(Constants.EVENT_CODE_CANCEL_APPLY_MIC, mRoomId))
        NIMClient.getService(ChatRoomServiceObserver::class.java)
            .observeReceiveMessage(mMsgObserver, false)
        NIMClient.getService(ChatRoomService::class.java).exitChatRoom(imRoomId)
        releasePlayer()
        //停止计时
        rl_private_room.stopCountTime()
        //停止心跳
        closePrivateRoomHeartBeat()
        if (mTextSwitcherAnimation != null) {
            mTextSwitcherAnimation!!.stop()
            mTextSwitcherAnimation == null
        }
    }

    override fun onBackPressed() {
        leaveRoom()
    }

    /**
     * 离开房间
     */
    fun leaveRoom() {
        CommonDialogBuilder()
            .setTitle(if (mAvHelper != null) "是否确定下麦" else "是否退出房间")
            .setCancel("取消")
            .setConfirm("确定")
            .setListener(object : CommonActionListener {
                override fun onConfirm() {
                    if (mAvHelper != null) {
                        roomLiveViewModel?.quitMic(mRoomId)
                    } else {
                        releasePlayer()
                    }
                    try {
                        //退出房间
                        var duration: Long = 0
                        if (enter_timp > 0) {
                            duration = System.currentTimeMillis() - enter_timp
                        }
                        val map: MutableMap<String?, String?> = HashMap()
                        map[Constants.AUDIENCE_FROM] = audience_from
                        map["duration"] = duration.toString()
                        map[Constants.USER_ID] = AccountUtil.getInstance().userId
                        map["roomType"] = mRoomType.toString()
                        map["on_mic"] =
                            (micUserInfos[1] != null && micUserInfos[1]!!.user_id == AccountUtil.getInstance().userId).toString()
                        map["send_gift"] = ifSendGift.toString()
                        map["room_id"] = mRoomId
                        LogOut.debug("RoomLiveActivity", "map_value$map")
                        MultiTracker.onEvent(DataOptimize.USER_VIDEO_ROOM_ACTION, map)
                    } catch (e: Throwable) {
                        e.printStackTrace()
                    }
                    rl_private_room.stopCountTime()
                    finish()
                }

                override fun onCancel() {}
            }).create().show(supportFragmentManager, CommonDialogFragment::class.java.simpleName)
    }

    /**
     * 展示同意专属房申请弹框
     */
    private fun showAgreePrivateApply(coin_consume_str: String?) {
        //如果当前以及是专属房则不弹窗
        if (mRoomType == ROOM_TYPE_PRIVATE) {
            return
        }
        var coinStr = ""
        if (StringUtil.isNotEmpty(coin_consume_str)) {
            coinStr = "<font color='#A4A9B3'>$coin_consume_str</font>"
        }
        CommonDialogBuilder.getInstance()
            .setTitle(getString(R.string.live_room_invite_private_room))
            .setDesc(Html.fromHtml(getString(R.string.live_room_invite_private_room_content) + "<br />" + coinStr))
            .setCancel(getString(R.string.cancel))
            .setConfirm(getString(R.string.live_room_private_room_invite_confirm))
            .setListener(object : CommonActionListener {
                override fun onConfirm() {
                    if (ViewUtils.isFastClick()) {
                        return
                    }
                    //同意进入专属视频房
                    mRoomType = ROOM_TYPE_PRIVATE
                    checkPermission(false, null, null)
                    CommonDialogBuilder.CommonDialog.mInstance = null
                }

                override fun onCancel() {
                    CommonDialogBuilder.CommonDialog.mInstance = null
                }
            }).create().show(supportFragmentManager, CommonDialogFragment::class.java.simpleName)
    }

    /**
     * 金牌师傅转专属房
     */
    private fun showAgreePrivateApply() {
        //如果当前以及是专属房则不弹窗
        if (mRoomType == ROOM_TYPE_PRIVATE) {
            return
        }
        CommonDialogBuilder.getInstance()
            .setTitle(getString(R.string.live_room_switch_to_specific))
            .setDesc(getString(R.string.live_room_switch_to_specific_tip))
            .setCancel(getString(R.string.cancel))
            .setConfirm(getString(R.string.sure))
            .setListener(object : CommonActionListener {
                override fun onConfirm() {
                    convertToSpeciRoomClick("confirm")
                    if (ViewUtils.isFastClick()) {
                        return
                    }
                    //同意进入专属视频房
                    mRoomType = ROOM_TYPE_PRIVATE
                    checkPermission(false, null, null)
                    CommonDialogBuilder.CommonDialog.mInstance = null
                }

                override fun onCancel() {
                    convertToSpeciRoomClick("cancel")
                    CommonDialogBuilder.CommonDialog.mInstance = null
                }
            }).create().show(supportFragmentManager, CommonDialogFragment::class.java.simpleName)
    }

    private fun convertToSpeciRoomClick(clickType: String) {
        val params = HashMap<String?, String?>()
        params["anchor_id"] = micUserInfos[0]?.user_id
        params["female_guest_id"] = micUserInfos[2]?.user_id
        params["click_type"] = clickType
        MultiTracker.onEvent(TrackConstants.B_TRANSFER_EXCLUSIVE_POPUP_CLICK, params)
    }

    /**
     * 初始化专属房心跳
     */
    private fun initScheduleService() {
        closePrivateRoomHeartBeat()
        mScheduledExecutorService = Executors.newScheduledThreadPool(3)
        mScheduledExecutorService!!.scheduleAtFixedRate({
            roomLiveViewModel?.fetchPrivateRoomHeartBeat(
                mRoomId
            )
        }, 15, 15, TimeUnit.SECONDS)
    }

    /**
     * 停止心跳计时收费
     */
    private fun closePrivateRoomHeartBeat() {
        if (mScheduledExecutorService != null) {
            mScheduledExecutorService!!.shutdownNow()
            mScheduledExecutorService = null
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        MPermission.onRequestPermissionsResult(this, requestCode, permissions, grantResults)
    }

    fun checkPermission(onlyUseCard: Boolean = false, mSource: String?, mInviteId: String?) {
        isEnoughCard = onlyUseCard
        val lackPermissions = NERtc.checkPermission(this@RoomLiveActivityV2)
        if (lackPermissions.isEmpty()) {
            this.source = mSource
            this.invite_id = mInviteId
            onBasicPermissionSuccess()
        } else {
            val permissions = arrayOfNulls<String>(lackPermissions.size)
            for (i in lackPermissions.indices) {
                permissions[i] = lackPermissions[i]
            }
            MPermission.with(this@RoomLiveActivityV2)
                .setRequestCode(BASIC_PERMISSION_REQUEST_CODE)
                .permissions(*permissions)
                .request()
        }
    }

    /**
     * 引导Image 展示
     */
    private fun showGiftPackageGuide() {
        guide_gift_package_iv!!.visibility =
            if (AccountUtil.getInstance().isOpen) View.VISIBLE else View.GONE
    }

    override fun notifyNetworkChanged(isConnected: Boolean) {
        super.notifyNetworkChanged(isConnected)
        if (isConnected) {
            roomLiveViewModel?.getRoomInfo(mRoomId!!)
        }
    }

    override fun getStatusBarColor(): Int {
        return resources.getColor(R.color.bg_live_title)
    }

    override fun needSetTransparent(): Boolean {
        return false
    }

    override fun onPause() {
        super.onPause()
        val params = HashMap<String?, String?>()
        params["duration"] = duration.toString()
        params["live_room_id"] = mRoomId
        params["user_id"] = AccountUtil.getInstance().userId
        MultiTracker.onEvent(TrackConstants.B_STAY_LIVE_ROOM, params)
    }

    override fun onStop() {
        super.onStop()
        if (!AppUtils.isAppForeground() && iv_voice_chat_playing.isVisible) {
            VoiceChatPlayingBallManager.show(this@RoomLiveActivityV2)
        }
    }

    override fun onResume() {
        super.onResume()
        VoiceChatPlayingBallManager.hide()
    }

    inner class RoomListener : LiveRoomListener {
        override fun onUserLeave(account: String) {
            if (micUserInfos[2]?.user_id == account || micUserInfos[2]?.user_id == null) {
                ll_interaction_girl.release()
            }
            LogOut.debug(
                "RoomLiveActivity",
                "当前用户ID：${AccountUtil.getInstance().userId},用户${account},离开房间"
            )
        }

        override fun onUserJoined(account: String) {
            LogOut.debug(
                "RoomLiveActivity",
                "当前用户ID：${AccountUtil.getInstance().userId},用户${account},加入房间"
            )
        }

        override fun onJoinRoomSuccess(channelId: Long) {
            LogOut.debug(
                "  ",
                "当前用户ID：${AccountUtil.getInstance().userId},加入channelId=${channelId}房间成功"
            )
            ll_interaction_boy.showLocalView()
            roomLiveViewModel?.updateRTMPTask(mRoomId)
        }

        override fun onJoinRoomFailed(resultCode: Int) {
            //加入房间失败以后再调用接口退出房间
            roomLiveViewModel?.quitMic(mRoomId)
        }

        override fun onVideoLeave() {
            living_anchor.release()
            ll_interaction_boy.release()
            ll_interaction_girl.release()
        }

        override fun onMatchLeave() {

        }

        override fun onUserVideoStart(uid: Long, maxProfile: Int) {
            showVideoRenderByAccount(uid.toString())
        }

        override fun onDisconnect() {
            liveRoomDisconnectDialog.showPopupWindow()
        }

        /**
         * 根据account 决定显示哪个音视频画面
         * 除了主播，另一个画面就是女主播，也就是2号位
         */
        private fun showVideoRenderByAccount(account: String) {
            if (micUserInfos[0]?.user_id == account) {
                living_anchor.showRemoteView(account)
                LogOut.debug(
                    "RoomLiveActivity",
                    "当前用户ID：${AccountUtil.getInstance().userId},房主${account},打开音视频画面"
                )
            } else {
                ll_interaction_girl.showRemoteView(account)
                LogOut.debug(
                    "RoomLiveActivity",
                    "当前用户ID：${AccountUtil.getInstance().userId},女嘉宾${account},打开音视频画面"
                )
            }
        }
    }

    inner class LiveNeRtcStatsObserver : NERtcStatsObserver {
        override fun onRtcStats(neRtcStats: NERtcStats) {}
        override fun onLocalAudioStats(neRtcAudioSendStats: NERtcAudioSendStats) {}
        override fun onRemoteAudioStats(neRtcAudioRecvStats: Array<NERtcAudioRecvStats>) {}
        override fun onLocalVideoStats(neRtcVideoSendStats: NERtcVideoSendStats) {}
        override fun onRemoteVideoStats(neRtcVideoRecvStats: Array<NERtcVideoRecvStats>) {}
        override fun onNetworkQuality(neRtcNetworkQualityInfos: Array<NERtcNetworkQualityInfo>) {
            LogOut.debug("RoomLiveActivity", "当前视频房网络状态回调~")
            if (neRtcNetworkQualityInfos.isEmpty()) return
            val i = neRtcNetworkQualityInfos[0].upStatus
            if (networkQuality == -1) {
                networkQuality = i
            }
            if (!NetworkUtil.isNetAvailable(applicationContext)) {
                setNetStatus(
                    R.string.network_disconnect,
                    Color.parseColor("#F54562"),
                    R.mipmap.ic_xinhao_wu
                )
            } else {
                when (i) {
                    NERtcConstants.NetworkStatus.DOWN, NERtcConstants.NetworkStatus.VERYBAD, NERtcConstants.NetworkStatus.BAD, NERtcConstants.NetworkStatus.POOR -> setNetStatus(
                        R.string.network_poor,
                        Color.parseColor("#FFD13D"),
                        R.mipmap.ic_xinhao_cha
                    )
                    NERtcConstants.NetworkStatus.GOOD, NERtcConstants.NetworkStatus.EXCELLENT -> setNetStatus(
                        R.string.network_excellent,
                        Color.parseColor("#10D8C8"),
                        R.mipmap.ic_xianhao_lianghao
                    )
                }
            }
        }

        private fun setNetStatus(netDes: Int, color: Int, @DrawableRes resId: Int) {
            val gradientDrawable = ll_net_status.background as GradientDrawable
            gradientDrawable.cornerRadius = ScreenUtil.dip2px(22f).toFloat()
            gradientDrawable.setColor(color)
            gradientDrawable.alpha = 80
            ll_net_status.background = gradientDrawable
            ll_net_status.visibility = View.VISIBLE
            tv_net_status.setTextColor(color)
            tv_net_status.setText(netDes)
            iv_net_status.setImageResource(resId)
        }
    }

    /**
     * 云信消息观察者
     */
    private inner class MsgObserver : Observer<List<ChatRoomMessage>> {
        private var lastTime: Long = 0
        override fun onEvent(messages: List<ChatRoomMessage>?) {
            if (messages == null || messages.isEmpty() || isFinishing || isDestroyed) {
                return
            }
            val addedListItems: MutableList<ChatRoomMessage> = ArrayList(messages.size)
            for (message in messages) {
                if (message.attachment is RoomActionAttachment && isMyMessage(message)) {
                    onRoomActionReceived(message)
                    continue
                }
                if (isMyMessage(message)) {
                    addedListItems.add(message)
                }
            }
            mAdapter.add(addedListItems)
            if (addedListItems.size > 0) {
                rv_message_list!!.smoothScrollToPosition(mAdapter.itemCount - 1)
            }
            if (System.currentTimeMillis() - lastTime > 3000) {
                fetchOnlineCount()
                lastTime = System.currentTimeMillis()
            }
        }

        private fun onRoomActionReceived(message: ChatRoomMessage) {
            val roomActionAttachment = message.attachment as RoomActionAttachment
            val code = roomActionAttachment.action
            val account = roomActionAttachment.account
            when (CoupleVoiceAction.typeOfValue(code)) {
                CoupleVoiceAction.CLOSE_ROOM -> {
                    closeRoom()
                    if (StringUtil.isEquals(account, AccountUtil.getInstance().userId)) {
                        roomLiveViewModel?.quitMic(mRoomId)
                        roomLiveViewModel?.fetchDispatchFemale(DispatchFemaleEnum.DIS_OUT_LIVE.code)
                        val params = HashMap<String?, String?>()
                        params["user_id"] = AccountUtil.getInstance().userId
                        params["room_id"] = mRoomId
                        params["anchor_id"] = anchorId
                        params["dispatch_female_type"] = "anchor_close_room"
                        MultiTracker.onEvent(TrackConstants.B_DISPATCH_FEMALE, params)
                    }
                }
                CoupleVoiceAction.ON_MIC -> {
                    //非主持人用户加入刷新房间接口
                    if (!TextUtils.equals(account, micUserInfos[0]?.user_id)) {
                        roomLiveViewModel?.getRoomInfo(mRoomId!!)
                    }
                    LogOut.debug("RoomLiveActivity", "用户${account}上麦~")
                }
                CoupleVoiceAction.EXIT_ON_MIC -> {
                    LogOut.debug(
                        "RoomLiveActivity",
                        "用户${account}下麦~==${AccountUtil.getInstance().userId}"
                    )
                    //如果是专属房，且下麦的是当前用户，则退出当前房间
                    if (StringUtil.isEquals(account, AccountUtil.getInstance().userId)) {
                        if (mRoomType == ROOM_TYPE_PRIVATE) {
                            onPrivateRoomQuitMic()
                            //如果不是则释放音视频流
                        } else {
                            releaseAVHelper()
                            ll_interaction_boy.release()
                            AppraiseAnchorDialogfragment.newInstance()
                                .setAnchor(anchorId, mRoomId, mRoomInfo?.pos_2_info?.user_id).show(
                                supportFragmentManager,
                                AppraiseAnchorDialogfragment::class.java.simpleName
                            )
                        }
                    }
                    ll_net_status!!.visibility = View.GONE
                    roomLiveViewModel?.getRoomInfo(mRoomId!!)
                }
                CoupleVoiceAction.MIC_TIME_END, CoupleVoiceAction.MONEY_NOT_ENOUGH -> {
                    releaseAVHelper()
                    roomLiveViewModel?.getRoomInfo(mRoomId!!)
                    if (StringUtil.isEquals(account, AccountUtil.getInstance().userId)) {
                        CommonDialogBuilder()
                            .setWithCancel(false)
                            .setCancelAble(false)
                            .setImageTitleRes(R.mipmap.icon_mic_time_end)
                            .setConfirm(getString(R.string.dialog_gotit_tip))
                            .setDesc(roomActionAttachment.content).create().show(
                                supportFragmentManager,
                                CommonDialogFragment::class.java.simpleName
                            )
                    }
                }
                //邀请上麦
                CoupleVoiceAction.VIDEO_INVITE_MIC -> {
                    //麦上有人则不弹框
                    if (micUserInfos[1] != null) {
                        return
                    }
                    //如果当前是B端一键邀请，则在一定时间内只展示一次
                    if (StringUtil.isEquals(account, "all")) {
                        val canShowInviteDialog =
                            (mFirstInviteMicTime - System.currentTimeMillis()) / 1000 > roomActionAttachment.show_window
                        if (!canShowInviteDialog) {
                            return
                        }
                        mFirstInviteMicTime = System.currentTimeMillis()
                    } else if (!StringUtil.isEquals(account, AccountUtil.getInstance().userId)) {
                        return
                    }
                    //如果当前邀请的是本人则立马展示
                    mLiveInviteMicDialogFragment.setUserInfo(
                        mRoomId,
                        roomActionAttachment.avatar_url,
                        roomActionAttachment.content,
                        roomActionAttachment.text,
                        roomActionAttachment.need_coins
                    )
                        .also {
                            it.account = account
                            it.anchorId = anchorId
                        }.show(
                            supportFragmentManager,
                            LiveInviteMicDialogFragment::class.java.simpleName
                        )
                }
                CoupleVoiceAction.VIDEO_ADD_FRIEND_SUCCESS -> {
                    val friendContent = roomActionAttachment.content
                    if (StringUtil.isEquals(account, AccountUtil.getInstance().userId)) {
                        showMessage(friendContent)
                        roomLiveViewModel?.getRoomInfo(mRoomId!!)
                    }
                }
                CoupleVoiceAction.VIDEO_RECEPT_GIFT -> {
                    giftAnimationDialog.showGiftAnimation(roomActionAttachment)
                    roomLiveViewModel?.getRoomInfo(mRoomId!!)
                }
                //用户同意进入专属房
                CoupleVoiceAction.AGREE_PRIVATE_ROOM_INVITE ->
                    if (StringUtil.isEquals(account, AccountUtil.getInstance().userId)) {
                        LogOut.debug("RoomLiveActivity", "同意加入")
                        showAgreePrivateApply(mRoomInfo.private_room_coin_consume_str)
                        trackAppointmentExposureEvent()
                    }
                CoupleVoiceAction.CP_ROOM_SWITCH_PRIVATE -> {
                    //CP房转换成专属房
                    mRoomType = ROOM_TYPE_PRIVATE
                    val privateContent = roomActionAttachment.content
                    val whiteUserIds = roomActionAttachment.white_user_ids
                    LogOut.debug(
                        "RoomLiveActivity",
                        "专属房切换，白名单ID：${whiteUserIds.contentToString()}"
                    )
                    //不在白名单里的用户退出专属房
                    if (whiteUserIds?.contains(AccountUtil.getInstance().userId) == false) {
                        showMessage(privateContent)
                        //如果有别的观众邀请女嘉宾进入专属房，且女嘉宾同意后，则观众和男嘉宾会进入专属房
                        //而当前的男嘉宾会被踢出去
                        if (mAvHelper != null) {
                            roomLiveViewModel?.quitMic(mRoomId)
                        }
                        finish()
                    } else {
                        //专属房间心跳
                        initScheduleService()
                        rv_message_list!!.visibility = View.GONE
                        rl_apply_mic!!.visibility = View.INVISIBLE
                        rl_private_room!!.visibility = View.VISIBLE
                        ll_live_input!!.visibility = View.GONE
                        ll_normal_input!!.visibility = View.GONE
                        ll_interaction_girl.hideAddFriendLayout()
                        mic_group!!.visibility = View.GONE
                        rl_private_room.startCountTime()
                        roomLiveViewModel?.getRoomInfo(mRoomId!!)
                    }
                }
                CoupleVoiceAction.PRIVATE_SWITCH_CP_ROOM -> {
                    //专属转化成CP房
                    mRoomType = ROOM_TYPE_NORMAL
                    roomLiveViewModel?.getRoomInfo(mRoomId!!)
                    rv_message_list!!.visibility = View.VISIBLE
                    rl_apply_mic!!.visibility = View.VISIBLE
//                    mic_group!!.visibility = View.VISIBLE
                    mic_group!!.visibility = View.GONE
                    rl_private_room!!.visibility = View.GONE
                    ll_live_input!!.visibility = View.VISIBLE
                    ll_normal_input!!.visibility = View.GONE
                    hideKeyBoard()
                    //停止计时
                    rl_private_room.stopCountTime()
                    //停止心跳
                    closePrivateRoomHeartBeat()
                }
                //专属房踢人下麦(金币不够、心跳失败，服务端踢人下麦)
                CoupleVoiceAction.PRIVATE_ROOM_EXIT_MIC ->
                    if (StringUtil.isEquals(account, AccountUtil.getInstance().userId)) {
                        roomLiveViewModel?.quitMic(mRoomId)
                        finish()
                        LogOut.debug("RoomLiveActivity", "专属房服务端踢人")
                    }
                CoupleVoiceAction.PK_START_PK -> {
                    mPkStatus = PK_STATUS_PK_DONGING
                    updatePkIv()
                    ll_interaction_boy.startPK()
                    ll_interaction_girl.startPK()
                }
                CoupleVoiceAction.PK_END_PK -> {
                    mPkStatus = PK_STATUS_PK_END
                    updatePkIv()
                    updatePKResult()
                }
                CoupleVoiceAction.PK_CLEAR_DATA -> {
                    mPkStatus = PK_STATUS_PK_CLEAR
                    updatePkIv()
                    ll_interaction_boy.clearPKInfo()
                    ll_interaction_girl.clearPKInfo()
                }
                CoupleVoiceAction.PK_UPDATE_SCORE -> {
                    val pkInfo = CoupleRoom.PkInfo()
                    if (roomActionAttachment.pos == 1) {
                        pkInfo.pos_1_pk_score = roomActionAttachment.score
                        pkInfo.pos_1_rank_top_avatars = roomActionAttachment.topn_avatar_list
                        ll_interaction_boy.updatePkInfo(pkInfo)
                    } else {
                        pkInfo.pos_2_pk_score = roomActionAttachment.score
                        pkInfo.pos_2_rank_top_avatars = roomActionAttachment.topn_avatar_list
                        ll_interaction_girl.updatePkInfo(pkInfo)
                    }
                }
                CoupleVoiceAction.UPDATE_CP_GROUP_NUMBER -> {
                    var count = (message.attachment as RoomActionAttachment).count
                    tv_fans_group_num?.text = getString(R.string.cp_group_num, count)
                }
                CoupleVoiceAction.UPDATE_CP_ROOM_GIFT_RANK -> {
                    if (roomActionAttachment.pos == 0) {
                        living_anchor.updateCpGiftInfo(roomActionAttachment.topn_avatar_list)
                    } else if (roomActionAttachment.pos == 2) {
                        ll_interaction_girl.updateCpGiftInfo(roomActionAttachment.topn_avatar_list)
                    }
                }
                CoupleVoiceAction.ANCHOR_BOOST_GIFT_SUCCESS -> {
                    rl_boost_gift.visibility = View.VISIBLE
                    roomActionAttachment?.gift_boost_info_list?.let {
                        updateBoostGiftList(it)
                    }
                }
                CoupleVoiceAction.UPDATE_SEND_BOOST_GIFT_PROGRESS -> {
                    roomActionAttachment?.gift_boost_info_list?.let {
                        switchBoostGift(it)
                    }
                }
                CoupleVoiceAction.UPDATE_UPDATE_LOTTERY_COUNT -> {
                    if (StringUtil.isEquals(account, AccountUtil.getInstance().userId)) {
                        boostLotteryCount =
                            (roomActionAttachment?.common_count.toInt() + roomActionAttachment?.advance_count.toInt()).toString()
                        anchorGiftHelpDetailsPW?.getBoostGiftDetails(
                            mRoomId!!,
                            mRoomInfo.anchor_info!!.user_id,
                            boostLotteryCount!!
                        )
                    }
                }
                CoupleVoiceAction.VOICE_CHAT_START, CoupleVoiceAction.VOICE_CHAT_STOP -> {
                    roomLiveViewModel?.getRoomInfo(mRoomId!!)
                }

//                else -> roomLiveViewModel?.getRoomInfo(mRoomId!!)
            }
        }

    }

    //这里直播可以用 LivePlayerObserver 点播用 VodPlayerObserver
    private inner class SimpleLivePlayerObserver : LivePlayerObserver {
        override fun onPreparing() {
            pb_player!!.visibility = View.VISIBLE
            LogOut.debug("RoomLiveActivity", "音视频拉流准备")
        }

        override fun onPrepared(info: MediaInfo) {
            pb_player!!.visibility = View.GONE
            LogOut.debug("RoomLiveActivity", "音视频拉流准备完毕")
        }

        override fun onError(code: Int, extra: Int) {
            if (mLivePlayer != null) {
                liveRoomDisconnectDialog.showPopupWindow()
            }
            pb_player!!.visibility = View.GONE
            closeLoading()
            LogOut.debug("RoomLiveActivity", "音视频拉流错误，code=$code")
        }

        override fun onFirstVideoRendered() {
            pb_player!!.visibility = View.GONE
            closeLoading()
            LogOut.debug("RoomLiveActivity", "音视频拉流绘制完成")
        }

        override fun onFirstAudioRendered() {}
        override fun onBufferingStart() {}
        override fun onBufferingEnd() {}
        override fun onBuffering(percent: Int) {}
        override fun onVideoDecoderOpen(value: Int) {}
        override fun onStateChanged(stateInfo: StateInfo) {}
        override fun onHttpResponseInfo(code: Int, header: String) {}
    }

    private val isPKRoom: Boolean
        get() = mRoomType == ROOM_TYPE_PK

    companion object {
        const val ROOM_TYPE_NORMAL = 0
        const val ROOM_TYPE_PRIVATE = 1
        const val ROOM_TYPE_PK = 2
    }

    override fun getParams(): HashMap<String?, String?> {
        var customParams = HashMap<String?, String?>()
        try {
            customParams["is_new_user"] =
                AccountUtil.getInstance().userInfo.isIs_live_unactive_user.toString()
        } catch (th: Throwable) {
        }
        return customParams
    }

    fun getBoyNeRtcVideoView(): NERtcVideoView {
        return ll_interaction_boy.videoView
    }

    /**
     * 助力引导
     */
    fun showHelpGuide() {
        val guideBuilder = GuideBuilder()
        guideBuilder.setTargetView(ts_boost_gift_switch)
            .setOverlayTarget(false)
            .setHighTargetCorner(DimenUtil.dp2px(this, 90f))
            .setHighTargetPaddingLeft(DimenUtil.dp2px(this, 5f))
            .setHighTargetPaddingRight(DimenUtil.dp2px(this, 5f))
            .setHighTargetPaddingTop(DimenUtil.dp2px(this, 5f))
            .setHighTargetPaddingBottom(DimenUtil.dp2px(this, 5f))
            .setAlpha(153)
            .setHighTargetGraphStyle(Component.ROUNDRECT)
        guideBuilder.addComponent(
            ComponentView(
                R.mipmap.icon_guide_help_gift,
                Component.ANCHOR_BOTTOM,
                Component.FIT_START,
                10,
                10
            )
        )
        val guide = guideBuilder.createGuide()
        guide.show(this)
        val params: MutableMap<String, String> = java.util.HashMap<String, String>()
        params["anchor_id"] = mRoomInfo.anchor_info?.user_id!!
        MultiTracker.onEvent(TrackConstants.B_ASSIST_FULL_SCREEN_EXPOSURE, params)
    }

    fun isGiftSwitchVisiable(): Boolean {
        return ts_boost_gift_switch.visibility == View.VISIBLE
    }

    /**
     * 是否有助力活动
     */
    fun hasBoost(): Boolean {
        if (null != mRoomInfo && null != mRoomInfo.gift_boost_status && true == mRoomInfo.gift_boost_status && !isPKRoom) {
            return true
        }
        return false
    }

    /**
     * 展示抽奖弹窗
     */
    fun showBoostLotteryDialog() {
        boostGiftRewardPW?.getBoostLotteryInfo(
            mRoomId!!,
            if (mRoomInfo?.pos_2_info == null) "" else mRoomInfo?.pos_2_info?.user_id,
            mRoomInfo.anchor_info?.user_id,
            if (mRoomInfo.pos_1_info == null) "" else mRoomInfo.pos_1_info?.user_id
        )
        boostGiftRewardPW?.showPopupWindow()
    }

    /**
     * 悬浮球弹窗
     */
    private fun showFloatDialog(): Boolean {
        FloatManager.getInstance().checkFloatPermission(this@RoomLiveActivityV2)
        return false
    }
}