package com.liquid.poros.ui.activity.live.view

import android.content.Context
import android.graphics.Color
import android.text.Html
import android.util.AttributeSet
import android.view.Gravity
import android.view.View
import android.view.View.OnClickListener
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.blankj.utilcode.util.SizeUtils
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.request.RequestOptions
import com.faceunity.nama.ui.CircleImageView
import com.liquid.base.adapter.CommonAdapter
import com.liquid.base.adapter.CommonViewHolder
import com.liquid.base.mvp.MvpActivity
import com.liquid.base.utils.LogOut
import com.liquid.poros.R
import com.liquid.poros.R.*
import com.liquid.poros.analytics.utils.MultiTracker
import com.liquid.poros.constant.TrackConstants
import com.liquid.poros.entity.CoupleRoom
import com.liquid.poros.entity.MicUserInfo
import com.liquid.poros.helper.AvExceptionUploadHelper
import com.liquid.poros.ui.activity.audiomatch.CPMatchingGlobal.Companion.getInstance
import com.liquid.poros.ui.activity.live.RoomLiveActivityV2
import com.liquid.poros.ui.activity.live.viewmodel.RoomLiveViewModel
import com.liquid.poros.ui.dialog.SendGiftPopupWindow
import com.liquid.poros.ui.fragment.dialog.account.UpdateMakeUpDialogFragment
import com.liquid.poros.ui.fragment.dialog.common.CommonActionListener
import com.liquid.poros.ui.fragment.dialog.common.CommonDialogBuilder
import com.liquid.poros.ui.fragment.dialog.common.CommonDialogFragment
import com.liquid.poros.ui.fragment.dialog.live.CoupleUserInfoDialogFragment
import com.liquid.poros.ui.fragment.dialog.live.CpLiveGiftRankPopupWindow
import com.liquid.poros.ui.fragment.dialog.live.LiveGiftRankListDialogFragment
import com.liquid.poros.ui.fragment.dialog.live.LiveUserInfoDialogFragment
import com.liquid.poros.utils.AccountUtil
import com.liquid.poros.utils.ScreenUtil
import com.liquid.poros.utils.StringUtil
import com.liquid.poros.utils.ViewUtils
import com.netease.lava.nertc.sdk.NERtc
import com.netease.lava.nertc.sdk.NERtcEx
import com.netease.lava.nertc.sdk.video.NERtcRemoteVideoStreamType
import com.netease.lava.nertc.sdk.video.NERtcVideoView
import com.netease.neliveplayer.playerkit.common.net.NetworkUtil
import kotlinx.android.synthetic.main.layout_interaction_member_new.view.*
import razerdp.basepopup.BasePopupWindow
import java.util.*

/**
 * RoomLiving 直播页面视频窗口
 */
class LivingView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {
    var imRoomId: String? = null
    var mRoomId: String? = null
    var mIsVoiceRoom:Boolean = false
    val user: MicUserInfo?
        get() {
            return micUserInfos?.get(pos)
        }
    var micUserInfos: Array<MicUserInfo?>? = null //0号位 主持人  1号位 男嘉宾 2号位 女嘉宾
    var roomInfo: CoupleRoom? = null
        set(value) {
            mPkStatus = value!!.pk_status
            sendGiftDialog.setCoinCount(value.coin)
            sendGiftDialog.setData(value.gift_list, value.gift_num_list)
            field = value
        }
    var pos: Int = 0
    private val PK_STATUS_PK_DONGING = 0
    private val PK_STATUS_PK_END = 1
    private val PK_STATUS_PK_CLEAR = 2
    private var mPkStatus: Int? = null
    private val sendGiftDialog: SendGiftPopupWindow by lazy { SendGiftPopupWindow(context) }
    private val userActionPopup: UserActionPopup by lazy { UserActionPopup(context) }
    var mMicName: String? = null//麦位用户昵称

    var videoView: NERtcVideoView

    companion object {
        var pkInfo: CoupleRoom.PkInfo? = CoupleRoom.PkInfo()
    }

    init {
        if (pkInfo == null) {
            pkInfo = CoupleRoom.PkInfo()
        }
        inflate(context, layout.layout_interaction_member_new, this)
        videoView = video_render
        setOnClickListener {
            if (user == null) {
                return@setOnClickListener
            }
            //如果是不是PK房切1位置则为男嘉宾，需要展示男嘉宾信息
            //否则均弹出送礼对话框
            if (!isPKRoom() && pos == 1) {
                CoupleUserInfoDialogFragment.newInstance().setTargetId(user?.user_id).show((context as RoomLiveActivityV2).supportFragmentManager, CoupleUserInfoDialogFragment::class.java.name)
            } else {
                //sendGiftDialog.setIsLiveRoom(true)
                showSendGiftView()
            }
        }

        iv_more.setOnClickListener { v ->
            userActionPopup.showPopupWindow(v)
        }
        iv_interaction_head.setOnClickListener {
            CoupleUserInfoDialogFragment.newInstance().setTargetId(user?.user_id).show((context as RoomLiveActivityV2).supportFragmentManager, CoupleUserInfoDialogFragment::class.java.name)
        }
        iv_gift.setOnClickListener {
            (context as RoomLiveActivityV2).showSendGiftView(user!!, "iv_gift", pos,user!!.user_id)
            if (pos == 0) {
                val params = HashMap<String?, String?>()
                params["host_id"] = user!!.user_id
                MultiTracker.onEvent(TrackConstants.B_HOST_GIFT_CLICK, params)
            } else if (pos == 2) {
                val params = HashMap<String?, String?>()
                params["female_guest_id"] = user!!.user_id
                params["live_room_id"] = imRoomId
                MultiTracker.onEvent(TrackConstants.B_GUEST_GIFT_CLICK, params)
            }
        }
        tv_add_or_private.setOnClickListener {
            //加好友
            if (user!!.isShow_add_friend_button) {
                (context as RoomLiveActivityV2).showSendGiftView(user!!, "add_friend", pos,user!!.user_id)
                val params = HashMap<String?, String?>()
                params["female_guest_id"] = user!!.user_id
                params["live_room_id"] = imRoomId
                MultiTracker.onEvent(TrackConstants.B_ADD_FRIENDS_CLICK, params)
                //去约会
            } else {
                if (getInstance(context as MvpActivity).isMatching) {
                    Toast.makeText(context, context.getString(string.audio_match_float_ball_tip), Toast.LENGTH_SHORT).show()
                    return@setOnClickListener
                }
                applyPrivateRoom(roomInfo?.private_room_coin_consume_str)
                clickToAppointMent()
            }
        }
        rl_interaction_info.setOnClickListener {
            if (!NetworkUtil.isNetAvailable(context.applicationContext)) {
                Toast.makeText(context, context.getString(string.network_not_available), Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
//            if (pos == 2 || isPKRoom() && pos == 0) {
            if (pos == 2 || pos == 0 || isPKRoom() && pos != 0) {
                LiveUserInfoDialogFragment.newInstance(imRoomId, roomInfo?.room_type!!)
                        .setTargetId(user?.user_id)
                        .setListener { showSendGiftView("iv_info") }
                        .show((context as RoomLiveActivityV2).supportFragmentManager, LiveUserInfoDialogFragment::class.java.name)
                val params = HashMap<String?, String?>()
                params["female_guest_id"] = user?.user_id
                params["live_room_id"] = imRoomId
                MultiTracker.onEvent(TrackConstants.B_GUEST_AVATAR_CLICK, params)
            }
        }
        pk_info_ll.setOnClickListener(OnClickListener {
            if (pk_rank_rv.childCount == 0) {
                Toast.makeText(context, context.getString(string.pk_rank_empty_tip), Toast.LENGTH_SHORT).show()
                return@OnClickListener
            }
            if (!NetworkUtil.isNetAvailable(context.applicationContext)) {
                Toast.makeText(context, context.getString(string.network_not_available), Toast.LENGTH_SHORT).show()
                return@OnClickListener
            }
            LiveGiftRankListDialogFragment().setMicPositionRank(mRoomId, pos).show((context as RoomLiveActivityV2).supportFragmentManager, LiveGiftRankListDialogFragment::class.java.simpleName)
            val params = HashMap<String?, String?>()
            params["room_id"] = imRoomId
            params["anchor_id"] = roomInfo?.anchor_info?.user_id
            user?.let {
                params[if (pos == 1) "left_female_guest_id" else "right_female_guest_id"] = it.user_id
            }
            params["room_type"] = "pk_room"
            params["exposure_from"] = if (pos == 1) "left" else "right"
            params["pk_status"] = if (mPkStatus == PK_STATUS_PK_DONGING) "1" else "2"
            MultiTracker.onEvent(TrackConstants.B_PK_GIFT_RANK_EXPOSURE, params)
        })
        pk_rank_rv.setOnTouchListener { v, event -> pk_info_ll.onTouchEvent(event) }
        ll_cp_rank_rv.setOnClickListener {
            //cp房礼物榜
            mMicName = if (pos == 0) roomInfo?.anchor_info?.nick_name else roomInfo?.pos_2_info?.nick_name
            val cpLiveGiftRankPopupWindow = CpLiveGiftRankPopupWindow(context)
            cpLiveGiftRankPopupWindow.setCpGiftRankData(mRoomId, pos,mMicName)
            cpLiveGiftRankPopupWindow.popupGravity = Gravity.CENTER
            cpLiveGiftRankPopupWindow.showPopupWindow()
            trackCpGiftRank()
         }
    }

    fun isVoiceRoom(isVoiceRoom:Boolean) {
        mIsVoiceRoom = isVoiceRoom
    }

    /**
     * 礼物榜埋点
     */
    private fun trackCpGiftRank(){
        val params = HashMap<String?, String?>()
        params["room_id"] = imRoomId
        params["anchor_id"] = roomInfo?.anchor_info?.user_id
        params["guest_id"] = roomInfo?.pos_2_info?.user_id
        MultiTracker.onEvent(TrackConstants.B_GIFT_LIST_CLICK, params)
    }

    /**
     * 和女嘉宾约会，申请进入专属房
     */
    private fun applyPrivateRoom(coin_consume_str: String?) {
        //邀请弹窗显示过滤专属房类型
        if (roomInfo?.room_type == RoomLiveActivityV2.ROOM_TYPE_PRIVATE) {
            return
        }
        var coinStr = ""
        if (StringUtil.isNotEmpty(coin_consume_str)) {
            coinStr = "<font color='#A4A9B3'>$coin_consume_str</font>"
        }
        CommonDialogBuilder.getInstance()
                .setTitle(context.getString(string.live_room_apply_private_room))
                .setDesc(Html.fromHtml(context.getString(string.live_room_apply_private_room_content) + "<br />" + coinStr))
                .setCancel(context.getString(string.cancel))
                .setConfirm(context.getString(string.live_room_private_room_apply_confirm))
                .setListener(object : CommonActionListener {
                    override fun onConfirm() {
                        if (ViewUtils.isFastClick()) {
                            return
                        }
                        //申请进入专属视频房
                        (context as RoomLiveActivityV2).getActivityViewModel(RoomLiveViewModel::class.java).fetchApplyPrivateRoom(mRoomId)
                    }

                    override fun onCancel() {}
                }).create().show((context as RoomLiveActivityV2).supportFragmentManager, CommonDialogFragment::class.java.simpleName)
    }

    /**
     * 展示送礼对话框
     */
    private fun showSendGiftView(from: String = "iv_interaction") {
        (context as RoomLiveActivityV2).showSendGiftView(user!!, "iv_gift", pos,user!!.user_id)
        /*sendGiftDialog.setRoomId(mRoomId!!)
        sendGiftDialog.setFrom(from)
        sendGiftDialog.setUserInfo(micUserInfos!!, "anchor" == user!!.room_role, user!!.isShow_add_friend_button, pos, roomInfo?.room_type
                ?: 0)
        sendGiftDialog.showPopupWindow()*/
        //如果为主播或者女嘉宾则需要埋点
        if (pos == 0) {
            MultiTracker.onEvent(TrackConstants.B_HOST_VIDEO_CLICK, mapOf("host_id" to user!!.user_id))
        } else if (pos == 2) {
            MultiTracker.onEvent(TrackConstants.B_GUEST_VIDEO_CLICK, mapOf("female_guest_id" to user!!.user_id, "live_room_id" to imRoomId))
        }
    }

    /**
     * 更新直播间麦序
     */
    fun updateViewByUser(roomId: String, imRoomId: String?, roomInfo: CoupleRoom, micUserInfos: Array<MicUserInfo?>) {
        mRoomId = roomId
        this.imRoomId = imRoomId
        this.roomInfo = roomInfo
        this.micUserInfos = micUserInfos
        if (mIsVoiceRoom) {
            ll_empty.visibility =  VISIBLE
        }else {
            ll_empty.visibility = if (StringUtil.isEmpty(user?.user_id)) VISIBLE else GONE
        }
        setBackgroundColor(if (StringUtil.isEmpty(user?.user_id) || user == null) Color.parseColor("#474794") else Color.TRANSPARENT)
        user?.also { userInfo ->
            rl_interaction_info.visibility = VISIBLE
            tv_interaction_name.text = userInfo.nick_name
            val options = RequestOptions()
                    .placeholder(mipmap.img_default_avatar)
                    .apply(RequestOptions.bitmapTransform(CircleCrop()))
                    .override(ScreenUtil.dip2px(20f), ScreenUtil.dip2px(20f))
                    .error(mipmap.img_default_avatar)
            Glide.with(this)
                    .load(userInfo.avatar_url)
                    .apply(options)
                    .into(iv_interaction_head)
            iv_more.visibility = if (isMySelf()) VISIBLE else GONE
            iv_gift.visibility = if (userInfo.isShow_gift_icon) VISIBLE else GONE
            video_render.visibility = if (isCurrentUserOnMic()) VISIBLE else GONE
            when (pos) {
                0 -> {
                    tv_interaction_tag.visibility = VISIBLE
                    iv_interaction_head.visibility = GONE
                    iv_user_icon.setImageResource(mipmap.icon_interaction_girl)
                    tv_user_name.text = context.getString(string.interaction_member_empty_anchor)
                    if (mIsVoiceRoom) {
                        Glide.with(context).load(userInfo.avatar_url).into(iv_voice_avatar)
                        iv_voice_avatar.visibility = View.VISIBLE
                        tv_user_name.visibility = View.GONE
                    }else {
                        iv_voice_avatar.visibility = View.GONE
                        tv_user_name.visibility = View.VISIBLE
                    }
                    iv_interaction_head.visibility = GONE
                    pk_info_ll.visibility = GONE
                    pk_result_iv.visibility = GONE
                    if (userInfo.isShow_add_friend_button) {
                        tv_add_or_private.visibility = VISIBLE
                        tv_add_or_private.text = context.getString(string.live_room_and_friend)
                    } else {
                        tv_add_or_private.visibility = View.GONE
                    }
                }
                1 -> {
                    tv_interaction_tag.visibility = GONE
                    iv_interaction_head.visibility = VISIBLE
                    if (mIsVoiceRoom) {
                        Glide.with(context).load(userInfo.avatar_url).into(iv_voice_avatar)
                        iv_voice_avatar.visibility = View.VISIBLE
                        tv_user_name.visibility = View.GONE
                    }else {
                        iv_user_icon.setImageResource(if (isPKRoom()) mipmap.icon_interaction_girl else mipmap.icon_interaction_boy)
                        iv_voice_avatar.visibility = View.GONE
                        tv_user_name.visibility = View.VISIBLE
                    }
                    tv_user_name.text = if (isPKRoom()) context.getString(string.interaction_member_empty_girl_position) else context.getString(string.interaction_member_empty_boy)
                    iv_interaction_head.visibility = VISIBLE
                    //PK房男嘉宾位置可以加好友
                    if (userInfo.isShow_add_friend_button && isPKRoom()) {
                        tv_add_or_private.visibility = VISIBLE
                        tv_add_or_private.text = context.getString(string.live_room_and_friend)
                    } else {
                        tv_add_or_private.visibility = GONE
                    }
                }
                2 -> {
                    tv_interaction_tag.visibility = GONE
                    iv_interaction_head.visibility = VISIBLE
                    if (mIsVoiceRoom) {
                        Glide.with(context).load(userInfo.avatar_url).into(iv_voice_avatar)
                        iv_voice_avatar.visibility = View.VISIBLE
                        tv_user_name.visibility = View.GONE
                    }else {
                        tv_user_name.visibility = View.VISIBLE
                        iv_voice_avatar.visibility = View.GONE
                        iv_user_icon.setImageResource(mipmap.icon_interaction_girl)
                    }
                    tv_user_name.text = if (isPKRoom()) context.getString(string.interaction_member_empty_girl_position) else context.getString(string.interaction_member_empty_girl)
                    iv_interaction_head.visibility = VISIBLE
                    //所有房间均可以加好友
                    //专属房和PK房不显示去约会，但是可以加好友
                    if (userInfo.isShow_add_friend_button) {
                        tv_add_or_private.visibility = VISIBLE
                        tv_add_or_private.text = context.getString(string.live_room_and_friend)
                    } else {
                        tv_add_or_private.visibility = GONE
                    }
                    //如果是专属房和PK房且加过好友以后就隐藏掉
                    if (this.roomInfo?.room_type == RoomLiveActivityV2.ROOM_TYPE_PRIVATE || isPKRoom()) {
                        if (!userInfo.isShow_add_friend_button) {
                            tv_add_or_private.visibility = GONE
                        }
                    } else {
                        //如果是普通房加过好友以后就去约会
                        if (!userInfo.isShow_add_friend_button) {
                            tv_add_or_private.visibility = VISIBLE
                            tv_add_or_private.text = context.getString(string.live_room_to_private_room)
                        }
                    }
                }
            }
        }
        //如果当前用户信息为null 则下麦
        if (user == null) {
            exitMic()
        }
    }

    /**
     * 当前用户在上麦
     */
    private fun isCurrentUserOnMic(): Boolean {
        return micUserInfos?.get(1) != null && micUserInfos!![1]!!.user_id == AccountUtil.getInstance().userId
    }

    private fun clickToAppointMent() {
        val params = HashMap<String?, String?>()
        params["female_guest_id"] = micUserInfos?.get(2)?.user_id
        params["room_id"] = mRoomId
        params["anchor_id"] = micUserInfos?.get(0)?.user_id
        params["event_time"] = System.currentTimeMillis().toString()
        MultiTracker.onEvent(TrackConstants.B_GO_ENGAGEMENT_BUTTON, params)
    }

    /**
     * 下麦
     */
    private fun exitMic() {
        video_render.visibility = GONE
        rl_interaction_info.visibility = GONE
        pk_info_ll.visibility = GONE
        pk_result_iv.visibility = GONE
        iv_gift.visibility = GONE
        iv_more.visibility = GONE
        ll_empty.visibility = VISIBLE
        iv_voice_avatar.visibility = View.GONE
        tv_user_name.visibility = View.VISIBLE
        if (pos == 1 && roomInfo?.gender == 1) {
            iv_user_icon.setImageResource(if (isPKRoom()) mipmap.icon_interaction_girl else mipmap.icon_interaction_boy)
            tv_user_name.text = if (isPKRoom()) context.getString(string.interaction_member_empty_girl_position) else context.getString(string.interaction_member_empty_boy)
        }
        if (pos == 2) {
            iv_user_icon.setImageResource(mipmap.icon_interaction_girl)
            tv_user_name.text = if (isPKRoom()) context.getString(string.interaction_member_empty_girl_position) else context.getString(string.interaction_member_empty_girl)
        }
    }

    /**
     * 显示音视频远端用户画面
     */
    fun showRemoteView(account: String) {
        try {
            video_render.visibility = VISIBLE
            LogOut.debug("RoomLiveActivity", "${video_render}当前用户${account}，播放远程画面")
            NERtcEx.getInstance().subscribeRemoteVideoStream(account.toLong(), NERtcRemoteVideoStreamType.kNERtcRemoteVideoStreamTypeHigh, true)
            NERtcEx.getInstance().setupRemoteVideoCanvas(video_render, account.toLong())
        } catch (e: Throwable) {
            e.printStackTrace()
        }
    }

    /**
     * 显示音视频C端用户画面
     */
    fun showLocalView() {
        /*try {
            LogOut.debug("RoomLiveActivity", "当前用户${user?.user_id}，播放本地画面")
            video_render.visibility = VISIBLE
            //加入房间后预览,进行视频的采集发送与预览
            val i = NERtc.getInstance().enableLocalVideo(true)
            if (i != 0) {
                AvExceptionUploadHelper.initException("enableLocalVideo return code:$i", mRoomId!!)
            }
            NERtcEx.getInstance().setupLocalVideoCanvas(video_render)
        } catch (e: Throwable) {
            e.printStackTrace()
        }*/
    }

    fun hideAddFriendLayout() {
        tv_add_or_private.visibility = View.GONE
    }

    /**
     * 清除PK信息
     * 隐藏PK icon
     */
    fun clearPKInfo() {
        pk_info_ll.visibility = GONE
        pk_result_iv.visibility = GONE
        pk_score_tv.text = "0"
        pkInfo?.pos_1_pk_score = 0
        pkInfo?.pos_2_pk_score = 0
        pkInfo?.pos_1_rank_top_avatars = null
        pkInfo?.pos_2_rank_top_avatars = null
        val adapter = pk_rank_rv.adapter
        if (adapter is CommonAdapter<*>) {
            if (adapter.datas != null) {
                adapter.datas.clear()
                adapter.notifyDataSetChanged()
            }
        }
    }

    fun startPK() {
        pk_info_ll.visibility = VISIBLE
    }

    /**
     * 更新PK信息
     */
    fun updatePkInfo(pk_Info: CoupleRoom.PkInfo) {
        if (roomInfo?.pk_status == PK_STATUS_PK_CLEAR) {
            pk_info_ll.visibility = GONE
            pk_result_iv.visibility = GONE
        } else {
            pk_info_ll.visibility = if (isPKRoom()) VISIBLE else GONE
            pk_result_iv.visibility = if (isPKRoom() && roomInfo?.pk_status == PK_STATUS_PK_END) VISIBLE else GONE
        }
        pk_score_tv.text = if (pos == 1) pk_Info.pos_1_pk_score.toString() else pk_Info.pos_2_pk_score.toString()

        if (pos == 1) {
            pkInfo?.pos_1_pk_score = pk_Info.pos_1_pk_score
        } else {
            pkInfo?.pos_2_pk_score = pk_Info.pos_2_pk_score
        }
        val totalAvatar = if (pos == 1) {
            pk_Info.pos_1_rank_top_avatars ?: listOf()
        } else {
            pk_Info.pos_2_rank_top_avatars ?: listOf()
        }
        updatePkRank(pk_rank_rv, totalAvatar)
    }

    /**
     * 更新送礼头像列表
     */
    private fun updatePkRank(pk_rank_rv: RecyclerView, pk_rank: List<String>) {
        if (pk_rank_rv.adapter == null) {
            val linearLayoutManager = LinearLayoutManager(context)
            linearLayoutManager.orientation = LinearLayoutManager.HORIZONTAL
            pk_rank_rv.layoutManager = linearLayoutManager
            val commonAdapter: CommonAdapter<String> = object : CommonAdapter<String>(context, layout.item_pk_room_gift_rank) {
                override fun convert(holder: CommonViewHolder, s: String?, pos: Int) {
                    val options = RequestOptions()
                            .placeholder(mipmap.img_default_avatar)
                            .override(ScreenUtil.dip2px(16f), ScreenUtil.dip2px(16f))
                            .error(mipmap.img_default_avatar)
                    Glide.with(context)
                            .load(s)
                            .apply(options)
                            .into((holder.getView<View>(R.id.iv_pk_room_gift_head) as ImageView))
                    if (holder.getView<View>(R.id.ll_pk_room_gift).layoutParams is ViewGroup.MarginLayoutParams) {
                        val marginLayoutParams = holder.getView<View>(R.id.ll_pk_room_gift).layoutParams as ViewGroup.MarginLayoutParams
                        if (pos == datas.size - 1) {
                            marginLayoutParams.setMargins(0, 0, 0, 0)
                        } else {
                            marginLayoutParams.setMargins(0, 0, -8, 0)
                        }
                        holder.getView<View>(R.id.ll_pk_room_gift).invalidate()
                    }
                }
            }
            pk_rank_rv.adapter = commonAdapter
            commonAdapter.update(pk_rank)
        } else {
            val adapter = pk_rank_rv.adapter
            if (adapter is CommonAdapter<*>) {
                (adapter as CommonAdapter<String>).update(pk_rank)
            }
        }
    }

    /**
     * 更新PK结果
     */
    fun updatePKResult() {
        if (pkInfo == null) {
            return
        }
        pk_result_iv.visibility = View.VISIBLE
        when {
            pkInfo!!.pos_1_pk_score > pkInfo!!.pos_2_pk_score -> {
                pk_result_iv.setImageResource(if (pos == 1) mipmap.icon_pk_win else mipmap.icon_pk_lose)
            }
            pkInfo!!.pos_1_pk_score == pkInfo!!.pos_2_pk_score -> {
                pk_result_iv.setImageResource(mipmap.icon_pk_draw)
            }
            pkInfo!!.pos_1_pk_score < pkInfo!!.pos_2_pk_score -> {
                pk_result_iv.setImageResource(if (pos == 1) mipmap.icon_pk_lose else mipmap.icon_pk_win)
            }
        }
    }

    /**
     * 更新cp房礼物榜
     */
    var cpRankReverse = ArrayList<String>()
    fun updateCpGiftInfo(cp_rank: List<String>?) {
        if (isCPRoom()) {
            ll_cp_rank_rv.visibility = if (cp_rank != null && cp_rank.isNotEmpty()) VISIBLE else GONE
            if (cp_rank != null) {
                cpRankReverse.clear()
                when (cp_rank.size) {
                    2 -> {
                        Collections.reverse(cp_rank)
                        cpRankReverse.addAll(cp_rank)
                    }
                    3 -> {
                        cpRankReverse.add(cp_rank[1])
                        cpRankReverse.add(cp_rank[0])
                        cpRankReverse.add(cp_rank[2])
                    }
                    else -> {
                        cpRankReverse.addAll(cp_rank)
                    }
                }
            }
            updateCpGiftRank(cp_rank_rv, cpRankReverse)
        } else {
            ll_cp_rank_rv.visibility = GONE
        }
    }

    /**
     * 更新cp房礼物排行榜头像列表
     */
    private fun updateCpGiftRank(cp_rank_rv: RecyclerView, cp_rank: List<String>?) {
        if (cp_rank_rv.adapter == null) {
            val linearLayoutManager = LinearLayoutManager(context)
            linearLayoutManager.orientation = LinearLayoutManager.HORIZONTAL
            cp_rank_rv.layoutManager = linearLayoutManager
            val commonAdapter: CommonAdapter<String> = object : CommonAdapter<String>(context, layout.item_cp_room_gift_rank) {
                override fun convert(holder: CommonViewHolder, s: String?, pos: Int) {
                    val options = RequestOptions()
                            .placeholder(mipmap.img_default_avatar)
                            .override(ScreenUtil.dip2px(24f), ScreenUtil.dip2px(24f))
                            .error(mipmap.img_default_avatar)
                    Glide.with(context)
                            .load(s)
                            .apply(options)
                            .into((holder.getView<View>(R.id.iv_cp_room_gift_head) as ImageView))
                    when (pos) {
                       0 -> {
                            holder.setBackgroundResource(R.id.iv_cp_room_gift_head_tip, if (datas.size == 1) R.mipmap.icon_cp_gift_rank_num_one else R.mipmap.icon_cp_gift_rank_num_two)
                            (holder.getView<View>(R.id.iv_cp_room_gift_head) as CircleImageView).borderColor = if (datas.size == 1) resources.getColor(R.color.cp_room_gift_rank_num_one) else resources.getColor(R.color.cp_room_gift_rank_num_two)
                        }
                        1 -> {
                            holder.setBackgroundResource(R.id.iv_cp_room_gift_head_tip, if (datas.size != 1) R.mipmap.icon_cp_gift_rank_num_one else R.mipmap.icon_cp_gift_rank_num_two)
                            (holder.getView<View>(R.id.iv_cp_room_gift_head) as CircleImageView).borderColor = if (datas.size != 1) resources.getColor(R.color.cp_room_gift_rank_num_one) else resources.getColor(R.color.cp_room_gift_rank_num_two)
                        }
                        2 ->{
                            holder.setBackgroundResource(R.id.iv_cp_room_gift_head_tip, mipmap.icon_cp_gift_rank_num_three)
                            (holder.getView<View>(R.id.iv_cp_room_gift_head) as CircleImageView).borderColor =resources.getColor(color.cp_room_gift_rank_num_three)
                        }
                    }
                    if (holder.getView<View>(R.id.ll_cp_room_gift).layoutParams is ViewGroup.MarginLayoutParams) {
                        val marginLayoutParams = holder.getView<View>(R.id.ll_cp_room_gift).layoutParams as ViewGroup.MarginLayoutParams
                        if (datas.size >= 2) {
                            holder.itemView.scaleX = if (pos == 1) 1f else 0.8f
                            holder.itemView.scaleY = if (pos == 1) 1f else 0.8f
                            if (datas.size == 3 && pos == 1) {
                                marginLayoutParams.setMargins(-20, 0, -20, 0)
                                holder.itemView.translationZ = 10f
                            } else if (pos == 1) {
                                marginLayoutParams.setMargins(-20, 0, 0, 0)
                            } else {
                                marginLayoutParams.setMargins(0, 0, 0, 0)
                            }
                        } else {
                            holder.itemView.scaleX = 1f
                            holder.itemView.scaleY = 1f
                            marginLayoutParams.setMargins(0, 0, 0, 0)
                        }
                        holder.getView<View>(R.id.ll_cp_room_gift).invalidate()
                    }
                }
            }
            cp_rank_rv.adapter = commonAdapter
            commonAdapter.update(cp_rank)
        } else {
            val adapter = cp_rank_rv.adapter
            if (adapter is CommonAdapter<*>) {
                (adapter as CommonAdapter<String>).update(cp_rank)
                cp_rank_rv.adapter = adapter
            }
        }
        cp_rank_rv.isLayoutFrozen = true
    }

    /**
     * 是否是本人
     */
    private fun isMySelf(): Boolean {
        return AccountUtil.getInstance().userId == user?.user_id
    }

    /**
     * 是否是PK房
     */
    private fun isPKRoom(): Boolean {
        return roomInfo?.room_type == RoomLiveActivityV2.ROOM_TYPE_PK
    }

    /**
     * 是否是CP房
     */
    private fun isCPRoom(): Boolean {
        return roomInfo?.room_type == RoomLiveActivityV2.ROOM_TYPE_NORMAL
    }

    /**
     * 释放资源
     */
    fun release() {
        LogOut.debug("RoomLiveActivity", "当前用户${user?.user_id}，释放画面")
        video_render.release()
        video_render.visibility = GONE
    }

    /**
     * 音视频View 美颜设置和下麦弹出PopupWindow
     */
    private inner class UserActionPopup(context: Context) : BasePopupWindow(context) {
        override fun onCreateContentView(): View {
            popupGravity = Gravity.END or Gravity.BOTTOM
            setPopupGravityMode(GravityMode.ALIGN_TO_ANCHOR_SIDE, GravityMode.RELATIVE_TO_ANCHOR)
            offsetX = SizeUtils.dp2px(5.0f)
            return createPopupById(layout.popu_room_live_user_action_new)
        }

        override fun onViewCreated(contentView: View) {
            super.onViewCreated(contentView)
            contentView.findViewById<View>(R.id.tv_set_makeup).setOnClickListener {
                if ((context as RoomLiveActivityV2).mAvHelper?.fURenderer != null) {
                    UpdateMakeUpDialogFragment.newInstance().setFuRender((context as RoomLiveActivityV2).mAvHelper?.fURenderer).show((context as RoomLiveActivityV2).supportFragmentManager, UpdateMakeUpDialogFragment::class.java.simpleName)
                }
                dismiss()
            }
            contentView.findViewById<View>(R.id.tv_left_mic).setOnClickListener {
                CommonDialogBuilder()
                        .setTitle("是否确定下麦")
                        .setCancel("取消")
                        .setConfirm("确定")
                        .setListener(object : CommonActionListener {
                            override fun onConfirm() {
                                (context as RoomLiveActivityV2).getActivityViewModel(RoomLiveViewModel::class.java).quitMic(mRoomId)
                            }

                            override fun onCancel() {}
                        }).create().show((context as RoomLiveActivityV2).supportFragmentManager, CommonDialogFragment::class.java.simpleName)
                dismiss()
            }
        }
    }
}