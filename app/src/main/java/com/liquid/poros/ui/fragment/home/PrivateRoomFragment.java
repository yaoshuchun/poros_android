package com.liquid.poros.ui.fragment.home;

import android.graphics.Rect;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import com.liquid.poros.R;
import com.liquid.poros.adapter.PrivateRoomAdapter;
import com.liquid.poros.base.BaseFragment;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.contract.couple.PrivateRoomContract;
import com.liquid.poros.contract.couple.PrivateRoomPresenter;
import com.liquid.poros.entity.PrivateRoomListInfo;
import com.liquid.poros.entity.RoomDetailsInfo;
import com.liquid.poros.helper.JinquanResourceHelper;
import com.liquid.poros.ui.activity.live.RoomLiveActivityV2;
import com.liquid.poros.utils.ScreenUtil;
import com.liquid.poros.widgets.WrapContentGridLayoutManager;
import com.liquid.poros.widgets.pulltorefresh.BaseRefreshListener;
import com.liquid.poros.widgets.pulltorefresh.PullToRefreshLayout;
import com.liquid.poros.widgets.pulltorefresh.ViewStatus;

import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;

/**
 * 专属房间
 */
public class PrivateRoomFragment extends BaseFragment implements PrivateRoomContract.View, BaseRefreshListener {
    @BindView(R.id.rv_group)
    RecyclerView rvGroup;
    @BindView(R.id.container)
    PullToRefreshLayout container;
    @BindView(R.id.ll_container)
    View ll_container;
    private PrivateRoomPresenter mPresenter;
    private int mPage = 0;
    private String mBanner;
    private PrivateRoomAdapter privateRoomAdapter;

    public static PrivateRoomFragment newInstance() {
        return new PrivateRoomFragment();
    }

    @Override
    protected void onFirstUserVisible() {

    }

    @Override
    protected void onUserVisible() {

    }

    @Override
    protected void onUserInvisible() {

    }

    @Override
    protected View getLoadingTargetView() {
        return null;
    }

    @Override
    protected void initViewsAndEvents(Bundle savedInstanceState) {
        WrapContentGridLayoutManager wrapContentGridLayoutManager = new WrapContentGridLayoutManager(getActivity(), 2);
        rvGroup.setLayoutManager(wrapContentGridLayoutManager);
        privateRoomAdapter = new PrivateRoomAdapter(getActivity());
        rvGroup.setAdapter(privateRoomAdapter);
        wrapContentGridLayoutManager.setSpanSizeLookup(new WrapContentGridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return privateRoomAdapter.isHeader(position) ? wrapContentGridLayoutManager.getSpanCount() : 1;
            }
        });
        rvGroup.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                WrapContentGridLayoutManager.LayoutParams layoutParams = (WrapContentGridLayoutManager.LayoutParams) view.getLayoutParams();
                int spanIndex = layoutParams.getSpanIndex();
                outRect.top = ScreenUtil.dip2px(7);
                if (spanIndex == 0) {
                    outRect.left = ScreenUtil.dip2px(12);
                    outRect.right = ScreenUtil.dip2px(12);
                } else {
                    outRect.right = ScreenUtil.dip2px(12);
                    outRect.left = ScreenUtil.dip2px(12) / 4;
                }
            }
        });
        privateRoomAdapter.setOnItemListener(new PrivateRoomAdapter.OnListener() {
            @Override
            public void onItemClick(String roomId) {
                mPresenter.fetchRoomDetails(roomId);
            }
        });
        mPresenter = new PrivateRoomPresenter();
        mPresenter.attachView(this);
        mPresenter.fetchPrivateRoom(mPage);
        container.setRefreshListener(this);
        container.setCanLoadMore(true);
    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.fragment_private_room;
    }

    @Override
    protected String getPageId() {
        return TrackConstants.PAGE_PRIVATE_ROOM;
    }

    @Override
    public void onPrivateRoomListFetched(PrivateRoomListInfo privateRoomListInfo) {
        container.finishRefresh();
        container.finishLoadMore();
        if ((mPage == 0) && (privateRoomListInfo == null || privateRoomListInfo.getData() == null || privateRoomListInfo.getData().getRoom_list().size() == 0)) {
            container.showView(ViewStatus.EMPTY_STATUS);
            container.setCanLoadMore(false);
            return;
        }
        mBanner = privateRoomListInfo.getData().getTop_img();
        container.showView(ViewStatus.CONTENT_STATUS);
        privateRoomAdapter.setBanner(mBanner);
        privateRoomAdapter.update(privateRoomListInfo.getData().getRoom_list());
    }

    @Override
    public void onRoomDetailsFetched(RoomDetailsInfo roomDetailsInfo) {
        if (roomDetailsInfo.getData() == null) {
            return;
        }
        if (roomDetailsInfo.getData().getRoom_type() == RoomLiveActivityV2.ROOM_TYPE_PRIVATE) {
            if (!TextUtils.isEmpty(roomDetailsInfo.getData().getHint())) {
                showMessage(roomDetailsInfo.getData().getHint());
            }
        }
    }


    @Override
    public void refresh() {
        mPage = 0;
        mPresenter.fetchPrivateRoom(mPage);
    }

    @Override
    public void loadMore() {
        ++mPage;
        mPresenter.fetchPrivateRoom(mPage);
    }

    @Override
    public void notifyByThemeChanged() {
        JinquanResourceHelper.getInstance(getActivity()).setBackgroundColorByAttr(ll_container, R.attr.custom_attr_app_bg);
    }

    @Override
    public void notifyNetworkChanged(boolean isConnected) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }


}
