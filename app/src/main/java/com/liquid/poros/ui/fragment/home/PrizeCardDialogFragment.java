package com.liquid.poros.ui.fragment.home;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.liquid.base.adapter.CommonAdapter;
import com.liquid.base.adapter.CommonViewHolder;
import com.liquid.poros.R;
import com.liquid.poros.base.BaseDialogFragment;
import com.liquid.poros.analytics.utils.MultiTracker;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.entity.PrizeCoinInfo;
import com.liquid.poros.utils.ScreenUtil;
import com.liquid.poros.utils.StringUtil;
import com.liquid.poros.widgets.GridSpacingItemDecoration;

/**
 * 群通知详情弹窗界面
 */
public class PrizeCardDialogFragment extends BaseDialogFragment {

    private PrizeCoinInfo.PrizeCoin mPrizeCoin;

    @Override
    protected String getPageId() {
        return TrackConstants.PAGE_REWARD;
    }

    public static PrizeCardDialogFragment newInstance() {
        PrizeCardDialogFragment fragment = new PrizeCardDialogFragment();
        return fragment;
    }

    public PrizeCardDialogFragment setPrizeCoin(PrizeCoinInfo.PrizeCoin prizeCoin) {
        this.mPrizeCoin = prizeCoin;
        return this;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onStart() {
        super.onStart();
        Window win = getDialog().getWindow();
        // 一定要设置Background，如果不设置，window属性设置无效
        win.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams params = win.getAttributes();
        params.gravity = Gravity.CENTER;
        setCancelable(false);
        params.width = (int) (ScreenUtil.getDisplayWidth() * 0.85);
        win.setAttributes(params);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_prize_card_dialog, null);
        if (mPrizeCoin != null) {
            TextView tv_title = view.findViewById(R.id.tv_title);
            if (!TextUtils.isEmpty(mPrizeCoin.getContent())) {
                tv_title.setText(mPrizeCoin.getContent());
            }
            TextView tv_desc = view.findViewById(R.id.tv_desc);
            tv_desc.setText(mPrizeCoin.getTodayReward());
            RecyclerView rv_coin = view.findViewById(R.id.rv_coin);
            int itemLength = mPrizeCoin.getPrize_items().size();
            if (itemLength > 4) {
                rv_coin.setLayoutManager(new GridLayoutManager(getActivity(), 4));
                rv_coin.addItemDecoration(new GridSpacingItemDecoration(4, ScreenUtil.dip2px(8), false));
            } else {
                rv_coin.setLayoutManager(new GridLayoutManager(getActivity(), 3));
                rv_coin.addItemDecoration(new GridSpacingItemDecoration(3, ScreenUtil.dip2px(39), false));
            }
            CommonAdapter<PrizeCoinInfo.PrizeItem> adapter = new CommonAdapter<PrizeCoinInfo.PrizeItem>(getActivity(), R.layout.item_prize_card) {
                @Override
                public void convert(CommonViewHolder holder, PrizeCoinInfo.PrizeItem prizeItem, int pos) {
                    holder.setText(R.id.tv_day_str, prizeItem.getDay_str());
                    FrameLayout fl_bg_day = holder.getView(R.id.fl_bg_day);
                    fl_bg_day.setBackgroundResource(prizeItem.isIs_current() ? R.mipmap.ic_tiaofu : R.mipmap.ic_tiaofu_2);
                    holder.setText(R.id.tv_mic_str, prizeItem.getMic_str());
                    holder.setText(R.id.tv_team_str, prizeItem.getTeam_str());

                    holder.setVisibility(R.id.tv_mic_str, StringUtil.isEmpty(prizeItem.getMic_str()) ? View.GONE : View.VISIBLE);
                    holder.setVisibility(R.id.tv_team_str, StringUtil.isEmpty(prizeItem.getTeam_str()) ? View.GONE : View.VISIBLE);

                    holder.setVisibility(R.id.fl_has_receive, prizeItem.isHas_rcv() && !prizeItem.isIs_current() ? View.VISIBLE : View.GONE);
                    holder.setVisibility(R.id.fl_un_receive, !prizeItem.isHas_rcv() && !prizeItem.isFuture() ? View.VISIBLE : View.GONE);

                    holder.itemView.setBackgroundResource(prizeItem.isIs_current() ? R.drawable.bg_coin_is_current : R.drawable.bg_coin_normal);
                    ImageView iv_card = holder.getView(R.id.iv_card);
                    Glide.with(getActivity()).load(prizeItem.getImage_url()).into(iv_card);
                }
            };
            adapter.update(mPrizeCoin.getPrize_items());
            rv_coin.setAdapter(adapter);
        }
        view.findViewById(R.id.tv_notification_read).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MultiTracker.onEvent(TrackConstants.B_REWARD_CLOSE);
                dismissAllowingStateLoss();
            }
        });
        builder.setView(view);
        return builder.create();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


}
