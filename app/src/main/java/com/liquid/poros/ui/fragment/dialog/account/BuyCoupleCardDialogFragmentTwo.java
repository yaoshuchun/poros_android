package com.liquid.poros.ui.fragment.dialog.account;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.liquid.poros.R;
import com.liquid.poros.adapter.CoupleCardAdapter;
import com.liquid.poros.base.BaseActivity;
import com.liquid.poros.contract.account.BuyCoupleContract;
import com.liquid.poros.contract.account.BuyCouplePresenter;
import com.liquid.poros.entity.BaseModel;
import com.liquid.poros.entity.BuyCoupleCardInfo;
import com.liquid.poros.entity.CoupleCardInfo;
import com.liquid.poros.entity.CoupleCardListInfo;
import com.liquid.poros.entity.UserCoinInfo;
import com.liquid.poros.ui.fragment.dialog.user.GroupNotificationDialogFragment;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.poros.utils.ViewUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * 购买cp卡界面
 */
public class BuyCoupleCardDialogFragmentTwo extends BottomSheetDialogFragment implements BuyCoupleContract.View {
    private TextView tv_coin;
    private TextView tv_recharge;
    private TextView tv_cp_card_right;
    private TextView tv_cp_card_title;
    private TextView tv_cp_card_desc;
    private RecyclerView rv_cp_card;
    private TextView tv_buy_cp_card;
    private TextView tv_not_couple;
    private int mPos = 0;
    private boolean mIsAgreeCp = false;//是否是邀请
    private boolean mIsCpRoom = true;//区分组cp直播房间 和 私聊界面
    private String mRoomId;
    private String mRoomAnchor;
    private String mTargetId;
    private String mCpOrderId;
    private String mCardCategory;
    private TextView sure;
    private TextView cancel;
    private BuyCouplePresenter mPresenter;
    private List<CoupleCardInfo> mCpCardList = new ArrayList<>();
    private CoupleCardAdapter mAdapter;
    private CoupleCardListInfo mCoupleCardListInfo;
    private boolean isEnoughCoin = false; //金币是否足够
    private boolean isEnoughCard = false; //开黑&上麦卡是否足够

    public static BuyCoupleCardDialogFragmentTwo newInstance() {
        BuyCoupleCardDialogFragmentTwo fragment = new BuyCoupleCardDialogFragmentTwo();
        return fragment;
    }

    public BuyCoupleCardDialogFragmentTwo setCpCardInfo(boolean isAgreeCp, boolean isCpRoom, String roomId, String roomAnchor, String targetId, String cpOrderId, String cardCategory) {
        this.mIsAgreeCp = isAgreeCp;
        this.mIsCpRoom = isCpRoom;
        this.mRoomId = roomId;
        this.mRoomAnchor = roomAnchor;
        this.mTargetId = targetId;
        this.mCpOrderId = cpOrderId;
        this.mCardCategory = cardCategory;
        return this;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        try {
            super.show(manager, tag);
        } catch (Exception e) {

        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
        View view = View.inflate(getContext(), R.layout.fragment_cp_card_dialog_two, null);
        dialog.setContentView(view);
        //设置透明背景
        dialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);
        View root = dialog.getDelegate().findViewById(R.id.design_bottom_sheet);
        BottomSheetBehavior behavior = BottomSheetBehavior.from(root);
        behavior.setHideable(false);
        initializeView(view);
        mPresenter = new BuyCouplePresenter();
        mPresenter.attachView(this);
        mPresenter.getUserCoin();
        mPresenter.getCpCardList(mIsCpRoom, mTargetId, mCardCategory);
        return dialog;
    }

    private void initializeView(View view) {
        tv_coin = view.findViewById(R.id.tv_coin);
        tv_recharge = view.findViewById(R.id.tv_recharge);
        tv_cp_card_right = view.findViewById(R.id.tv_cp_card_right);
        tv_cp_card_title = view.findViewById(R.id.tv_cp_card_title);
        tv_cp_card_desc = view.findViewById(R.id.tv_cp_card_desc);
        rv_cp_card = view.findViewById(R.id.rv_cp_card);
        tv_buy_cp_card = view.findViewById(R.id.tv_buy_cp_card);
        tv_not_couple = view.findViewById(R.id.tv_not_couple);
        sure = view.findViewById(R.id.sure);
        cancel = view.findViewById(R.id.cancel);
        rv_cp_card.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        mAdapter = new CoupleCardAdapter(getActivity(), mIsCpRoom, mCpCardList);
        rv_cp_card.setAdapter(mAdapter);
        mAdapter.setOnItemListener(new CoupleCardAdapter.OnItemListener() {
            @Override
            public void onClick(View v, int pos) {
                mPos = pos;
                mAdapter.setDefSelect(pos);
                mCurrentCardInfo = mCpCardList.get(mPos);
                changeStatus();
            }
        });
        tv_recharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.goRecharge();
                }
                dismissAllowingStateLoss();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        //购买cp卡
        sure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ViewUtils.isFastClick()) {
                    return;
                }
                if (listener != null && mCpCardList != null && mCpCardList.size() > 0) {
                    CoupleCardInfo coupleCardInfo = mCpCardList.get(mPos);
                    if (isEnoughCard) {
                        switch (coupleCardInfo.getCard_category()) {
                            case "TEAM"://开黑卡
                                mPresenter.userTeamCard(mTargetId, mRoomId, true, "");
                                break;
                            case "MIC"://上麦卡
                                mPresenter.userMicCard(mRoomId, true, "");
                                break;
                            case "CP"://cp卡
                                mPresenter.buyCouple(mIsAgreeCp, mRoomId, mRoomAnchor, coupleCardInfo.getCard_type(), String.valueOf(coupleCardInfo.getCard_price()), mTargetId, mCpOrderId);
                                break;
                        }
                    } else {
                        if (isEnoughCoin) {
                            switch (coupleCardInfo.getCard_category()) {
                                case "TEAM"://开黑卡
                                    mPresenter.userTeamCard(mTargetId, mRoomId, false, String.valueOf(coupleCardInfo.getCard_price()));
                                    break;
                                case "MIC"://上麦卡
                                    mPresenter.userMicCard(mRoomId, false, String.valueOf(coupleCardInfo.getCard_price()));
                                    break;
                                case "CP"://cp卡
                                    mPresenter.buyCouple(mIsAgreeCp, mRoomId, mRoomAnchor, coupleCardInfo.getCard_type(), String.valueOf(coupleCardInfo.getCard_price()), mTargetId, mCpOrderId);
                                    break;
                            }
                        } else {
                            listener.goRecharge();
                            dismissAllowingStateLoss();
                        }
                    }
                }
            }
        });
    }

    @Override
    public void cpCardListFetch(String targetId, CoupleCardListInfo coupleCardListInfo) {
        if (coupleCardListInfo == null || coupleCardListInfo.getData() == null || coupleCardListInfo.getData().getCard_list() == null || coupleCardListInfo.getData().getCard_list().size() == 0) {
            return;
        }
        rv_cp_card.setVisibility(View.VISIBLE);
        tv_not_couple.setVisibility(View.GONE);
        mCoupleCardListInfo = coupleCardListInfo;
        mCpCardList.clear();
        mCpCardList.addAll(coupleCardListInfo.getData().getCard_list());
        if (mCpCardList != null && mCpCardList.size() > 0) {
            mCurrentCardInfo = mCpCardList.get(mPos);
            changeStatus();
        }
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void buyCpSuccessFetch(String targetId, BuyCoupleCardInfo buyCoupleCardInfo) {
        if (listener != null) {
            listener.buyCardSuccess(mIsAgreeCp, targetId, buyCoupleCardInfo);
        }
        dismissAllowingStateLoss();
    }

    @Override
    public void onUserCoinFetch(UserCoinInfo userCoinInfo) {
        if (userCoinInfo != null && userCoinInfo.getCode() == 0 && userCoinInfo.getData() != null && !TextUtils.isEmpty(userCoinInfo.getData().getCoin())) {
            AccountUtil.getInstance().setCoin(userCoinInfo.getData().getCoin());
            tv_coin.setText(userCoinInfo.getData().getCoin());
            changeStatus();
        }
    }

    @Override
    public void buyCardMsg(String msg) {
        if (listener != null) {
            listener.buyCardMsg(msg);
        }
    }

    @Override
    public void notCoupleMsg() {
        //无法与对方组CP
        rv_cp_card.setVisibility(View.GONE);
        tv_not_couple.setVisibility(View.VISIBLE);
        tv_not_couple.setText(R.string.no_couple_tip);
        tv_not_couple.setTextColor(mIsCpRoom ? Color.parseColor("#ffffff") : Color.parseColor("#242424"));
    }

    @Override
    public void userTeamCardFetch(BaseModel baseModel) {
        if (baseModel != null && baseModel.getCode() == 0) {
            if (listener != null) {
                listener.userTeamCardSuccess();
            }
            dismissAllowingStateLoss();
        }
    }

    @Override
    public void userMicCardFetch(BaseModel baseModel) {
        if (listener != null) {
            listener.userMicCardSuccess();
        }
        dismissAllowingStateLoss();
    }

    private CoupleCardInfo mCurrentCardInfo;

    private void changeStatus() {
        if (mCurrentCardInfo == null) {
            return;
        }
        String coin = AccountUtil.getInstance().getCoin();
        BigDecimal bigDecimal1 = new BigDecimal(coin);
        BigDecimal bigDecimal2 = new BigDecimal(mCurrentCardInfo.getCard_price());
        int coinCode = bigDecimal1.compareTo(bigDecimal2);
        isEnoughCoin = coinCode >= 0;
        if (!TextUtils.isEmpty(mCurrentCardInfo.getCard_cnt())) {
            int code = new BigDecimal(mCurrentCardInfo.getCard_cnt()).compareTo(BigDecimal.ZERO);
            isEnoughCard = code > 0;
            if (isEnoughCard) {
                tv_buy_cp_card.setBackgroundResource(R.drawable.bg_common_button_save);
                switch (mCurrentCardInfo.getCard_category()) {
                    case "TEAM":
                        tv_buy_cp_card.setText(R.string.couple_use_team_card);
                        break;
                    case "MIC":
                        tv_buy_cp_card.setText(R.string.couple_use_mic_card);
                        break;
                }
            } else {
                if (isEnoughCoin) {
                    tv_buy_cp_card.setBackgroundResource(R.drawable.bg_common_button_save);
                    tv_buy_cp_card.setText(R.string.couple_card_buy);
                } else {
                    tv_buy_cp_card.setText(R.string.couple_no_enough_coin);
                    tv_buy_cp_card.setBackgroundResource(R.drawable.bg_button_no_enough);
                }
            }
        } else {
            isEnoughCard = false;
        }
        tv_cp_card_title.setText(mCurrentCardInfo.getTitle());
        tv_cp_card_desc.setText(mCurrentCardInfo.getDesc());
        if (mCurrentCardInfo.isShow_cp_right()) {
            if (isEnoughCoin) {
                if ("TEAM".equals(mCurrentCardInfo.getCard_category())) {
                    tv_buy_cp_card.setText(R.string.couple_card_kaihei);
                } else {
                    tv_buy_cp_card.setText(R.string.couple_card_buy);
                }
                tv_buy_cp_card.setBackgroundResource(R.drawable.bg_common_button_save);
            } else {
                tv_buy_cp_card.setText(R.string.couple_no_enough_coin);
                tv_buy_cp_card.setBackgroundResource(R.drawable.bg_button_no_enough);
            }
            String cpCardRightFirst = mCurrentCardInfo.getTip();
            String cpCardRightSecond = getString(R.string.couple_right);
            SpannableString spannableString = new SpannableString(cpCardRightFirst + cpCardRightSecond);
            //设置背景颜色
            spannableString.setSpan(new TextViewURLSpan(), cpCardRightFirst.length(), cpCardRightFirst.length() + cpCardRightSecond.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            spannableString.setSpan(new UnderlineSpan(), cpCardRightFirst.length(), cpCardRightFirst.length() + cpCardRightSecond.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            spannableString.setSpan(new ForegroundColorSpan(Color.parseColor("#509DEB")), cpCardRightFirst.length(), cpCardRightFirst.length() + cpCardRightSecond.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            tv_cp_card_right.setText(spannableString);
            //设置可点击
            tv_cp_card_right.setMovementMethod(LinkMovementMethod.getInstance());
        } else {
            tv_cp_card_right.setText(mCurrentCardInfo.getTip());
            if (isEnoughCoin) {
                if ("TEAM".equals(mCurrentCardInfo.getCard_category())) {
                    tv_buy_cp_card.setText(R.string.couple_card_kaihei);
                } else {
                    tv_buy_cp_card.setText(R.string.couple_card_buy);
                }
            }
        }

    }

    class TextViewURLSpan extends ClickableSpan {
        @Override
        public void updateDrawState(TextPaint ds) {
        }

        @Override
        public void onClick(View widget) {
            GroupNotificationDialogFragment.newInstance().setGroupNotification(false, mCoupleCardListInfo.getData().getCp_right_title(), mCoupleCardListInfo.getData().getCp_right_content()).show(getActivity().getSupportFragmentManager(), GroupNotificationDialogFragment.class.getSimpleName());

        }
    }

    @Override
    public void showLoading() {
        BaseActivity activity = (BaseActivity) getActivity();
        activity.showLoading();
    }

    @Override
    public void showSuccess() {

    }

    @Override
    public void closeLoading() {
        BaseActivity activity = (BaseActivity) getActivity();
        activity.closeLoading();
    }

    @Override
    public void onError() {

    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSuccess(Object data) {

    }


    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);
        if (listener != null) {
            listener.onDismiss();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }

    private BuyCardListener listener;

    public BuyCoupleCardDialogFragmentTwo setBuyListener(BuyCardListener listener) {
        this.listener = listener;
        return this;
    }

    public interface BuyCardListener {
        //跳转充值
        void goRecharge();

        //购买CP卡成功
        void buyCardSuccess(boolean isAgree, String targetId, BuyCoupleCardInfo buyCoupleCardInfo);

        //CP卡列表提示信息
        void buyCardMsg(String msg);

        //购买/使用开黑卡成功
        void userTeamCardSuccess();

        //购买/使用上麦卡成功
        void userMicCardSuccess();

        void onDismiss();

    }
}
