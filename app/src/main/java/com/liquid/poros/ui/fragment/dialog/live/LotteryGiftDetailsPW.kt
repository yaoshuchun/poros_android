package com.liquid.poros.ui.fragment.dialog.live

import android.content.Context
import android.view.View
import com.bumptech.glide.Glide
import com.liquid.poros.R
import com.liquid.poros.analytics.utils.MultiTracker
import com.liquid.poros.constant.TrackConstants
import com.liquid.poros.databinding.PopupLotteryGiftDetailsBinding
import razerdp.basepopup.BasePopupWindow

/**
 * 助力抽奖 -- 抽中礼物详情弹窗
 */
class LotteryGiftDetailsPW(context: Context) : BasePopupWindow(context) {
    private lateinit var binding: PopupLotteryGiftDetailsBinding
    private var mGuestId: String? = null//女嘉宾id
    private var mAnchorId: String? = null//主持人id
    private var mPos1Id: String? = null//一号麦id
    private var mTreasureBoxType: String? = "primary"//助力宝箱类型  默认初级
    override fun onCreateContentView(): View {
        return createPopupById(R.layout.popup_lottery_gift_details)
    }

    override fun onViewCreated(contentView: View) {
        super.onViewCreated(contentView)
        binding = PopupLotteryGiftDetailsBinding.bind(contentView)
        initListener()
    }

    override fun onShowing() {
        super.onShowing()
        contentView?.postDelayed(Runnable { dismiss() }, 2000)
    }

    private fun initListener() {
        binding.tvAction.setOnClickListener {
            dismiss()
            val params: MutableMap<String, String?> = HashMap<String, String?>()
            params["anchor_id"] = mAnchorId
            params["guest_id"] = mGuestId
            params["talk_user_id"] = mPos1Id
            params["get_prize_name"] = binding.tvName.text.toString()
            params["treasure_box_type"] = mTreasureBoxType
            MultiTracker.onEvent(TrackConstants.B_REWARD_POPUP_HAPPY_ACCEPT_CLICK, params)
        }
    }

    /**
     * 设置抽奖礼物
     */
    fun setLotteryGiftData(giftImage: String?, giftName: String?, boostLotteryCount: Int, guestId: String?, anchorId: String?, pos1MicId: String?, treasureBoxType: String) {
        mGuestId = guestId
        mAnchorId = anchorId
        mPos1Id = pos1MicId
        mTreasureBoxType = treasureBoxType
        Glide.with(context)
                .load(giftImage)
                .placeholder(R.mipmap.icon_default)
                .error(R.mipmap.icon_default)
                .into(binding.ivIcon)
        binding.tvName.text = if (boostLotteryCount > 1) "${giftName}*${boostLotteryCount}" else giftName
        val params: MutableMap<String, String?> = HashMap<String, String?>()
        params["anchor_id"] = mAnchorId
        params["guest_id"] = mGuestId
        params["talk_user_id"] = mPos1Id
        params["get_prize_name"] = binding.tvName.text.toString()
        params["treasure_box_type"] = mTreasureBoxType
        MultiTracker.onEvent(TrackConstants.B_REWARD_POPUP_EXPOSURE, params)
    }
}