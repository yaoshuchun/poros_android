package com.liquid.poros.ui.fragment.dialog.user;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.liquid.base.adapter.CommonAdapter;
import com.liquid.base.adapter.CommonViewHolder;
import com.liquid.base.event.EventCenter;
import com.liquid.poros.R;
import com.liquid.poros.base.BaseDialogFragment;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.entity.EditGameBean;
import com.liquid.poros.entity.UserCenterInfo;
import com.liquid.poros.helper.JinquanResourceHelper;
import com.liquid.poros.utils.ScreenUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import de.greenrobot.event.EventBus;

/**
 * 女嘉宾任务描述弹框
 */
public class EditGameDialogFragment extends BaseDialogFragment {
    private CommonAdapter<EditGameBean> mAdapter;
    private List<EditGameBean> editGameBeans = new ArrayList<>();
    private List<EditGameBean> editGameBeansTemp = new ArrayList<>();
    private UserCenterInfo.Data.UserInfo.GameInfoBean.HpjyBean peaceBean;
    private UserCenterInfo.Data.UserInfo.GameInfoBean.WzryBean honorBean;

    private UserCenterInfo.Data.UserInfo.GameInfoBean.HpjyBean peaceBeanTemp;
    private UserCenterInfo.Data.UserInfo.GameInfoBean.WzryBean honorBeanTemp;
    private String title;

    @Override
    protected String getPageId() {
        return TrackConstants.PAGE_TEST_EDIT_GAME;
    }

    public EditGameDialogFragment setPeaceBean(UserCenterInfo.Data.UserInfo.GameInfoBean.HpjyBean peaceBean) {
        this.peaceBean = peaceBean;
        Gson gson = new Gson();
        peaceBeanTemp = gson.fromJson(gson.toJson(peaceBean), UserCenterInfo.Data.UserInfo.GameInfoBean.HpjyBean.class);
        return this;
    }

    public EditGameDialogFragment setHonorBean(UserCenterInfo.Data.UserInfo.GameInfoBean.WzryBean honorBean) {
        this.honorBean = honorBean;
        Gson gson = new Gson();
        honorBeanTemp = gson.fromJson(gson.toJson(honorBean), UserCenterInfo.Data.UserInfo.GameInfoBean.WzryBean.class);
        return this;
    }

    public static EditGameDialogFragment newInstance() {
        EditGameDialogFragment fragment = new EditGameDialogFragment();
        return fragment;
    }

    public EditGameDialogFragment setEditGameBeans(List<EditGameBean> editGameBeans) {
        this.editGameBeans = editGameBeans;
        Gson gson = new Gson();
        this.editGameBeansTemp = gson.fromJson(gson.toJson(editGameBeans), new TypeToken<List<EditGameBean>>() {
        }.getType());
        return this;
    }

    public EditGameDialogFragment setTitle(String title) {
        this.title = title;
        return this;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        Window win = getDialog().getWindow();
        // 一定要设置Background，如果不设置，window属性设置无效
        win.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams params = win.getAttributes();
        params.width = ScreenUtil.getDisplayWidth();
        params.gravity = Gravity.BOTTOM;
        win.setAttributes(params);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_edit_game_dialog, null);
        TextView game_name = view.findViewById(R.id.tv_game_name);
        game_name.setText(title);
        view.findViewById(R.id.ll_close_game).setOnClickListener(v -> {
            dismissDialog();
        });
        view.findViewById(R.id.ll_delete).setOnClickListener(v -> {
            closeDialog();
        });
        view.findViewById(R.id.save).setOnClickListener(v -> {
            HashMap<String, Object> map = new HashMap<>();
            if (honorBeanTemp != null) {
                if (honorBeanTemp.getPlatform().size() == 0) {
                    Toast.makeText(getContext(), getString(R.string.test_dialog_game_platform_tip), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (honorBeanTemp.getZone().size() == 0) {
                    Toast.makeText(getContext(), getString(R.string.test_dialog_game_area_tip), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (honorBeanTemp.getRank().size() == 0) {
                    Toast.makeText(getContext(), getString(R.string.test_dialog_game_rank_tip), Toast.LENGTH_SHORT).show();
                    return;
                }
                honorBean = honorBeanTemp;
                map.put("static", editGameBeansTemp);
                map.put("data", honorBean);
                EventBus.getDefault().post(new EventCenter(Constants.EVENT_CODE_UPDATE_GAME_INFO_HONOR, map));
            }
            if (peaceBeanTemp != null) {
                if (peaceBeanTemp.getPlatform().size() == 0) {
                    Toast.makeText(getContext(), getString(R.string.test_dialog_game_platform_tip), Toast.LENGTH_SHORT).show();
                    return;
                }

                if (peaceBeanTemp.getZone().size() == 0) {
                    Toast.makeText(getContext(), getString(R.string.test_dialog_game_area_tip), Toast.LENGTH_SHORT).show();
                    return;
                }

                if (peaceBeanTemp.getRank().size() == 0) {
                    Toast.makeText(getContext(), getString(R.string.test_dialog_game_rank_tip), Toast.LENGTH_SHORT).show();
                    return;
                }
                peaceBean = peaceBeanTemp;
                map.put("static", editGameBeansTemp);
                map.put("data", peaceBean);
                EventBus.getDefault().post(new EventCenter(Constants.EVENT_CODE_UPDATE_GAME_INFO_PEACE, map));
            }
            dismissDialog();
        });
        RecyclerView rv = view.findViewById(R.id.rv_game);
        rv.setLayoutManager(new LinearLayoutManager(getContext()));
        mAdapter = new CommonAdapter<EditGameBean>(getContext(), R.layout.item_edit_game) {
            @Override
            public void convert(CommonViewHolder holder, final EditGameBean data, int position) {
                holder.setText(R.id.item_game_title, data.des);
                RecyclerView item_rv = holder.getView(R.id.item_rv);
                item_rv.setLayoutManager(new GridLayoutManager(getContext(), 3));
                CommonAdapter<EditGameBean.Item> mItemAdapter = new CommonAdapter<EditGameBean.Item>(getContext(), R.layout.item_edit_game_item) {
                    @Override
                    public void convert(CommonViewHolder holderItem, EditGameBean.Item item, int pos) {
                        holderItem.setText(R.id.item_game_content, item.name);
                        if (item.select) {
                            holderItem.setTextColor(R.id.item_game_content, getResources().getColor(R.color.white));
                            holderItem.setBackgroundResource(R.id.item_game_content, R.drawable.bg_gradual_14d4d6_5);
                        } else {
                            JinquanResourceHelper.getInstance(getActivity()).setTextColorByAttr(holderItem.getView(R.id.item_game_content), R.attr.custom_attr_unknown_text_color);
                            JinquanResourceHelper.getInstance(getActivity()).setBackgroundResourceByAttr(holderItem.getView(R.id.item_game_content), R.attr.bg_test_dialog_game_item);
                        }

                        holderItem.setOnClickListener(R.id.item_game_content, v -> {
                            item.select = !item.select;
                            if (position == 0) {
                                if (peaceBeanTemp != null) {
                                    updateGameData(peaceBeanTemp.getPlatform(), Integer.valueOf(item.id), item.select);
                                }
                                if (honorBeanTemp != null) {
                                    updateGameData(honorBeanTemp.getPlatform(), Integer.valueOf(item.id), item.select);
                                }
                            } else if (position == 1) {
                                if (peaceBeanTemp != null) {
                                    updateGameData(peaceBeanTemp.getZone(), Integer.valueOf(item.id), item.select);
                                }
                                if (honorBeanTemp != null) {
                                    updateGameData(honorBeanTemp.getZone(), Integer.valueOf(item.id), item.select);
                                }
                            } else {
                                if (peaceBeanTemp != null) {
                                    updateGameData(peaceBeanTemp.getRank(), Integer.valueOf(item.id), item.select);
                                }
                                if (honorBeanTemp != null) {
                                    updateGameData(honorBeanTemp.getRank(), Integer.valueOf(item.id), item.select);
                                }
                            }
                            notifyDataSetChanged();
                        });
                    }
                };
                mItemAdapter.update(data.items);
                item_rv.setAdapter(mItemAdapter);
                item_rv.addItemDecoration(new RecyclerView.ItemDecoration() {
                    @Override
                    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
                        super.getItemOffsets(outRect, view, parent, state);
                        GridLayoutManager.LayoutParams layoutParams = (GridLayoutManager.LayoutParams) view.getLayoutParams();
                        int spanIndex = layoutParams.getSpanIndex();
                        outRect.top = ScreenUtil.dip2px(8);
                        if (spanIndex == 0) {
                            // left
                            outRect.left = 0;
                            outRect.right = ScreenUtil.dip2px(4);
                        } else if (spanIndex == 1) {
                            outRect.left = ScreenUtil.dip2px(4);
                            outRect.right = ScreenUtil.dip2px(4);
                        } else {
                            outRect.left = ScreenUtil.dip2px(4);
                            outRect.right = 0;
                        }
                    }
                });
            }
        };
        mAdapter.update(editGameBeansTemp);
        rv.setAdapter(mAdapter);
        builder.setView(view);
        AlertDialog alertDialog = builder.create();
        return alertDialog;
    }

    private void updateGameData(List<Integer> data, Integer id, boolean select) {
        if (select) {
            data.add(id);
        } else {
            data.remove(id);
        }
        Collections.sort(data);
    }

    private void closeDialog() {
        for (EditGameBean editGameBean : editGameBeansTemp) {
            for (EditGameBean.Item item : editGameBean.items) {
                item.select = false;
            }
        }
        HashMap<String, Object> map = new HashMap<>();
        if (honorBean != null) {
            honorBean.getPlatform().clear();
            honorBean.getRank().clear();
            honorBean.getZone().clear();
            map.put("static", editGameBeansTemp);
            map.put("data", honorBean);
            EventBus.getDefault().post(new EventCenter(Constants.EVENT_CODE_UPDATE_GAME_INFO_HONOR, map));
        }
        if (peaceBean != null) {
            peaceBean.getPlatform().clear();
            peaceBean.getRank().clear();
            peaceBean.getZone().clear();
            map.put("static", editGameBeansTemp);
            map.put("data", peaceBean);
            EventBus.getDefault().post(new EventCenter(Constants.EVENT_CODE_UPDATE_GAME_INFO_PEACE, map));
        }
        dismissDialog();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
