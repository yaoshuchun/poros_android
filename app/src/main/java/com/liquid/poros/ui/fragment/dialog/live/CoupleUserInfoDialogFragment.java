package com.liquid.poros.ui.fragment.dialog.live;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.faceunity.nama.ui.CircleImageView;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.liquid.base.adapter.CommonAdapter;
import com.liquid.base.adapter.CommonViewHolder;
import com.liquid.base.adapter.MultiItemCommonAdapter;
import com.liquid.base.adapter.MultiItemTypeSupport;
import com.liquid.poros.R;
import com.liquid.poros.contract.live.LiveUserInfoContract;
import com.liquid.poros.contract.live.LiveUserInfoPresenter;
import com.liquid.poros.entity.NormalGame;
import com.liquid.poros.entity.UserCenterInfo;
import com.liquid.poros.entity.UserPhotoInfo;
import com.liquid.poros.utils.ScreenUtil;
import com.liquid.poros.utils.StringUtil;
import com.liquid.poros.utils.glide.GlideEngine;
import com.liquid.poros.widgets.FlowLayout;
import com.liquid.poros.widgets.HorizontalItemDecoration;
import com.liquid.poros.widgets.WrapContentGridLayoutManager;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.entity.LocalMedia;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * cp直播间用户个人信息弹窗
 */
public class CoupleUserInfoDialogFragment extends BottomSheetDialogFragment implements LiveUserInfoContract.View, View.OnClickListener {
    CircleImageView iv_user_head;
    TextView tv_user_cp_nickname;
    TextView tv_charm_value_level;
    TextView tv_user_cp_content;
    RelativeLayout rl_cp_want;
    FlowLayout fl_user_tag;
    TextView tv_game_want;
    TextView tv_user_tag;
    LinearLayout ll_game_want;
    RecyclerView rv_other_game;
    TextView tv_user_tag_seat;
    RecyclerView rv_pics;
    TextView tv_user_pics_seat;

    TextView tv_game_wangzhe_platform;
    TextView tv_game_wangzhe_area;
    TextView tv_game_wangzhe_rank;
    RelativeLayout rl_game_wangzhe;
    TextView tv_game_heping_platform;
    TextView tv_game_heping_area;
    TextView tv_game_heping_rank;
    RelativeLayout rl_game_heping;
    RelativeLayout rl_game_other;
    ImageView iv_wangzhe;
    ImageView iv_heping;
    TextView tv_game_want_empty;
    //展开&收起
    LinearLayout ll_center_pic_tip;
    TextView tv_center_pic_tip;
    ImageView iv_center_pic_tip;

    private LiveUserInfoPresenter mPresenter;
    private UserCenterInfo.Data.UserInfo mUserInfoBean;
    private String mTargetId;
    private List<String> mTagStrList = new ArrayList<>();
    private CommonAdapter<UserPhotoInfo.Data.Photos> mAdapter;
    private CommonAdapter<NormalGame> mOtherGameAdapter;
    private boolean isWangZheGame = false;
    private boolean isHePingGame = false;
    private boolean isOtherGame = false;
    private boolean isPicOpen = false; //展开&收起
    private List<UserPhotoInfo.Data.Photos> mPhotosList;

    public static CoupleUserInfoDialogFragment newInstance() {
        CoupleUserInfoDialogFragment fragment = new CoupleUserInfoDialogFragment();
        return fragment;
    }

    public CoupleUserInfoDialogFragment setTargetId(String targetId) {
        this.mTargetId = targetId;
        return this;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        try {
            super.show(manager, tag);
        } catch (Exception e) {

        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
        View view = View.inflate(getContext(), R.layout.fragment_cp_user_info_dialog, null);
        dialog.setContentView(view);
        //设置透明背景
        dialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);
        initView(view);
        mPresenter = new LiveUserInfoPresenter();
        mPresenter.attachView(this);
        mPresenter.getUserInfo(mTargetId);
        mPresenter.getUserPhotos(mTargetId);
        return dialog;
    }

    private void initView(View view) {
        iv_user_head = view.findViewById(R.id.iv_user_head);
        tv_user_cp_nickname = view.findViewById(R.id.tv_user_cp_nickname);
        tv_charm_value_level = view.findViewById(R.id.tv_charm_value_level);
        tv_game_want = view.findViewById(R.id.tv_game_want);
        ll_game_want = view.findViewById(R.id.ll_game_want);
        tv_user_tag = view.findViewById(R.id.tv_user_tag);

        rv_other_game = view.findViewById(R.id.rv_other_game);
        tv_user_cp_content = view.findViewById(R.id.tv_user_cp_content);
        rl_cp_want = view.findViewById(R.id.rl_cp_want);
        fl_user_tag = view.findViewById(R.id.fl_user_tag);
        tv_user_tag_seat = view.findViewById(R.id.tv_user_tag_seat);
        rv_pics = view.findViewById(R.id.rv_pics);
        tv_user_pics_seat = view.findViewById(R.id.tv_user_pics_seat);

        tv_game_wangzhe_platform = view.findViewById(R.id.tv_game_wangzhe_platform);
        tv_game_wangzhe_rank = view.findViewById(R.id.tv_game_wangzhe_rank);
        tv_game_wangzhe_area = view.findViewById(R.id.tv_game_wangzhe_area);
        tv_game_heping_platform = view.findViewById(R.id.tv_game_heping_platform);
        tv_game_heping_rank = view.findViewById(R.id.tv_game_heping_rank);
        tv_game_heping_area = view.findViewById(R.id.tv_game_heping_area);
        rl_game_heping = view.findViewById(R.id.rl_game_heping);
        rl_game_wangzhe = view.findViewById(R.id.rl_game_wangzhe);
        rl_game_other = view.findViewById(R.id.rl_game_other);
        iv_heping = view.findViewById(R.id.iv_heping);
        iv_wangzhe = view.findViewById(R.id.iv_wangzhe);
        tv_game_want_empty = view.findViewById(R.id.tv_game_want_empty);

        ll_center_pic_tip = view.findViewById(R.id.ll_center_pic_tip);
        ll_center_pic_tip.setOnClickListener(this);
        tv_center_pic_tip = view.findViewById(R.id.tv_center_pic_tip);
        iv_center_pic_tip = view.findViewById(R.id.iv_center_pic_tip);
        tv_center_pic_tip.setText(getString(R.string.session_guard_game_open));
        iv_center_pic_tip.setBackgroundResource(R.mipmap.icon_user_photo_open_day);
        rv_pics.setLayoutManager(new WrapContentGridLayoutManager(getActivity(), 4));
        mAdapter = new MultiItemCommonAdapter<UserPhotoInfo.Data.Photos>(getActivity(), new MultiItemTypeSupport<UserPhotoInfo.Data.Photos>() {
            @Override
            public int getLayoutId(int itemType) {
                if (itemType == 1) {
                    return R.layout.item_cp_game_ref_img;
                }
                return R.layout.item_cp_game_ref_video;
            }

            @Override
            public int getItemViewType(int position, UserPhotoInfo.Data.Photos photos) {
                return Integer.valueOf(photos.getType());
            }
        }) {
            @Override
            public void convert(CommonViewHolder holder, UserPhotoInfo.Data.Photos refItem, int pos) {
                ImageView iv_image = holder.getView(R.id.iv_image);
                final int itemHeight = (((ScreenUtil.adjustWidgetWidth() - ScreenUtil.dip2px(45)))) / 4;
                holder.itemView.getLayoutParams().height = itemHeight;
                holder.itemView.getLayoutParams().width = itemHeight;
                RequestOptions headOptions = new RequestOptions()
                        .transform(new CenterCrop(), new RoundedCorners(ScreenUtil.dip2px(6)));
                Glide.with(getActivity())
                        .load(refItem.getPath())
                        .apply(headOptions)
                        .into(iv_image);
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mAdapter.getDatas() != null && mAdapter.getDatas().size() > 0) {
                            if (mAdapter.getDatas().get(pos).getType() == 2) {
                                PictureSelector.create((Activity) mContext).externalPictureVideo(mAdapter.getDatas().get(pos).getPath());
                            } else {
                                //点击图片预览
                                PictureSelector.create((Activity) mContext)
                                        .themeStyle(R.style.picture_default_style)
                                        .isNotPreviewDownload(true)
                                        .isPreviewVideo(true)
                                        .imageEngine(GlideEngine.createGlideEngine())
                                        .openExternalPreview(pos, getSelectList(mAdapter.getDatas()));
                            }
                        }

                    }
                });
            }
        };
        rv_pics.setNestedScrollingEnabled(false);
        rv_pics.setHasFixedSize(true);
        rv_pics.setAdapter(mAdapter);
        mOtherGameAdapter = new CommonAdapter<NormalGame>(getActivity(), R.layout.item_other_game) {
            @Override
            public void convert(CommonViewHolder holder, NormalGame normalGame, int pos) {
                ImageView iv_image = holder.getView(R.id.iv_image);
                RequestOptions options = new RequestOptions()
                        .placeholder(R.mipmap.icon_default)
                        .error(R.mipmap.icon_default)
                        .transform(new CenterCrop(), new RoundedCorners(ScreenUtil.dip2px(6)));
                Glide.with(getActivity())
                        .load(normalGame.getIcon())
                        .override(ScreenUtil.dip2px(56), ScreenUtil.dip2px(56))
                        .apply(options)
                        .into(iv_image);
            }
        };
        rv_other_game.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL, false));
        rv_other_game.setAdapter(mOtherGameAdapter);
        rv_other_game.addItemDecoration(new HorizontalItemDecoration(ScreenUtil.dip2px(12)));
    }

    @Override
    public void onUserInfoFetch(UserCenterInfo userCenterInfo) {
        if (userCenterInfo == null || userCenterInfo.getData() == null || userCenterInfo.getData().getUser_info() == null) {
            return;
        }
        mUserInfoBean = userCenterInfo.getData().getUser_info();
        Glide.with(getActivity())
                .load(mUserInfoBean.getAvatar_url())
                .into(iv_user_head);
        tv_user_cp_nickname.setText(mUserInfoBean.getNick_name());
        //魅力值
        int[] charmValuesDrawable = {R.drawable.ic_charm_00,R.drawable.ic_charm_20,R.drawable.ic_charm_40,R.drawable.ic_charm_60,R.drawable.ic_charm_80,R.drawable.ic_charm_100};
        tv_charm_value_level.setBackgroundResource(charmValuesDrawable[mUserInfoBean.getUser_charm()/20]);
        tv_charm_value_level.setText(Html.fromHtml(String.format("魅 • <b>%d</b>", mUserInfoBean.getUser_charm())));
        //理想CP
        if (!TextUtils.isEmpty(mUserInfoBean.getCouple_desc())) {
            rl_cp_want.setVisibility(View.VISIBLE);
            tv_user_cp_content.setText(StringUtil.replaceBlank(mUserInfoBean.getCouple_desc()));
        } else {
            rl_cp_want.setVisibility(View.GONE);
        }

        //和平精英
        setHpjyParams(userCenterInfo);
        //王者荣耀
        setWzryParams(userCenterInfo);
        UserCenterInfo.Data.UserInfo.GameInfoBean gameInfoBean = userCenterInfo.getData().getUser_info().getGame_info();
        //其他游戏
        if (gameInfoBean != null && gameInfoBean.getOther() != null && gameInfoBean.getOther().size() > 0) {
            rl_game_other.setVisibility(View.VISIBLE);
            List<NormalGame> list = gameInfoBean.getOther();
            mOtherGameAdapter.update(list);
            isOtherGame = false;
        } else {
            rl_game_other.setVisibility(View.GONE);
            isOtherGame = true;
        }
        if (mUserInfoBean.isMatch_control()) {
            tv_game_want.setVisibility(View.GONE);
            ll_game_want.setVisibility(View.GONE);
            //标签
            if (mUserInfoBean.getGames() != null && mUserInfoBean.getGames().size() > 0) {
                fl_user_tag.setVisibility(View.VISIBLE);
                tv_user_tag_seat.setVisibility(View.GONE);
                mTagStrList.clear();
                mTagStrList.addAll(mUserInfoBean.getGames());
                initLeaderTag(mTagStrList);
            } else {
                fl_user_tag.setVisibility(View.GONE);
                tv_user_tag_seat.setVisibility(View.VISIBLE);
            }
        } else {
            tv_game_want.setVisibility(View.VISIBLE);
            ll_game_want.setVisibility(View.VISIBLE);
            tv_user_tag.setVisibility(View.GONE);
            fl_user_tag.setVisibility(View.GONE);
            tv_user_tag_seat.setVisibility(View.GONE);
            if (isHePingGame && isWangZheGame && isOtherGame) {
                tv_game_want_empty.setVisibility(View.VISIBLE);
            } else {
                tv_game_want_empty.setVisibility(View.GONE);
            }
        }

    }

    @Override
    public void onUserPhotosFetch(UserPhotoInfo userPhotoInfo) {
        if (userPhotoInfo == null || userPhotoInfo.getData() == null) {
            return;
        }
        rv_pics.setVisibility(userPhotoInfo.getData().getPhotos().size() > 0 ? View.VISIBLE : View.GONE);
        tv_user_pics_seat.setVisibility(userPhotoInfo.getData().getPhotos().size() > 0 ? View.GONE : View.VISIBLE);
        mPhotosList = userPhotoInfo.getData().getPhotos();
        mAdapter.update(mPhotosList.size() > 4 ? mPhotosList.subList(0, 4) : mPhotosList);
        ll_center_pic_tip.setVisibility(mPhotosList.size() > 4 ? View.VISIBLE : View.GONE);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_center_pic_tip:
                if (isPicOpen) {
                    mAdapter.update(mPhotosList.size() > 4 ? mPhotosList.subList(0, 4) : mPhotosList);
                } else {
                    mAdapter.update(mPhotosList);
                }
                tv_center_pic_tip.setText(isPicOpen ? getString(R.string.session_guard_game_open) : getString(R.string.session_guard_game_close));
                iv_center_pic_tip.setBackgroundResource(isPicOpen ? R.mipmap.icon_user_photo_open_day : R.mipmap.icon_user_photo_close_day);
                isPicOpen = !isPicOpen;
                break;
        }
    }

    private void initLeaderTag(List<String> tagStrList) {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(0, ScreenUtil.dip2px(5), ScreenUtil.dip2px(10), ScreenUtil.dip2px(5));
        fl_user_tag.removeAllViews();
        for (final String item : tagStrList) {
            final TextView tv = new TextView(getActivity());
            tv.setGravity(Gravity.CENTER);
            tv.setText(item);
            tv.setMaxEms(10);
            tv.setTextSize(12);
            tv.setPadding(ScreenUtil.dip2px(5), ScreenUtil.dip2px(5), ScreenUtil.dip2px(5), ScreenUtil.dip2px(5));
            tv.setTextColor(Color.parseColor("#191E24"));
            tv.setBackground(getResources().getDrawable(R.drawable.bg_color_ebfcf7_2));
            tv.setSingleLine();
            tv.setLayoutParams(layoutParams);
            fl_user_tag.addView(tv);
        }
    }

    private List<LocalMedia> getSelectList(List<UserPhotoInfo.Data.Photos> data) {
        List<LocalMedia> localMedia = new ArrayList<>();
        for (UserPhotoInfo.Data.Photos photos : data) {
            if (photos.getType() != 2) {
                localMedia.add(new LocalMedia(photos.getPath(), 0, 0, ""));
            }
        }
        return localMedia;
    }

    private void setWzryParams(UserCenterInfo userCenterInfo) {
        UserCenterInfo.Data.UserInfo.GameInfoBean userInfo = userCenterInfo.getData().getUser_info().getGame_info();
        UserCenterInfo.WZRYDATA wzrydata = userCenterInfo.getWzrydata();
        if (userInfo != null && wzrydata != null) {
            UserCenterInfo.Data.UserInfo.GameInfoBean.WzryBean wzryBean = userInfo.getWzry();
            if (wzryBean != null) {
                if (wzryBean.getPlatform().size() == 0 && wzryBean.getRank().size() == 0 && wzryBean.getZone().size() == 0) {
                    rl_game_wangzhe.setVisibility(View.GONE);
                    isWangZheGame = true;
                } else {
                    rl_game_wangzhe.setVisibility(View.VISIBLE);
                    isWangZheGame = false;
                }
                StringBuilder wzry_platfrom = new StringBuilder();
                StringBuilder wzry_rank = new StringBuilder();
                StringBuilder wzry_zone = new StringBuilder();
                //平台
                List<Integer> platforms = wzryBean.getPlatform();
                ArrayList<Map<String, String>> wzry_platfrom_list = wzrydata.getWzry_platform_list();
                for (int i = 0; i < platforms.size(); i++) {
                    int num = platforms.get(i);

                    for (int j = 0; j < wzry_platfrom_list.size(); j++) {
                        Map<String, String> map = wzry_platfrom_list.get(j);
                        if (map.containsKey(num + "")) {
                            String s = map.get(num + "");
                            if (com.liquid.base.tools.StringUtil.isEmpty(wzry_platfrom.toString())) {
                                wzry_platfrom.append(s);
                            } else {
                                wzry_platfrom.append(" | ").append(s);
                            }
                        }
                    }
                }
                //排行
                List<Integer> ranks = wzryBean.getRank();
                ArrayList<Map<String, String>> wzry_rank_list = wzrydata.getWzry_rank_list();
                for (int i = 0; i < ranks.size(); i++) {
                    int num = ranks.get(i);

                    for (int j = 0; j < wzry_rank_list.size(); j++) {
                        Map<String, String> map = wzry_rank_list.get(j);
                        if (map.containsKey(num + "")) {
                            String s = map.get(num + "");
                            if (com.liquid.base.tools.StringUtil.isEmpty(wzry_rank.toString())) {
                                wzry_rank.append(s);
                            } else {
                                wzry_rank.append(" | ").append(s);
                            }
                        }
                    }
                }
                //大区
                List<Integer> zones = wzryBean.getZone();
                ArrayList<Map<String, String>> wzry_area_list = wzrydata.getWzry_area_list();
                for (int i = 0; i < zones.size(); i++) {
                    int num = zones.get(i);

                    for (int j = 0; j < wzry_area_list.size(); j++) {
                        Map<String, String> map = wzry_area_list.get(j);
                        if (map.containsKey(num + "")) {
                            String s = map.get(num + "");
                            if (com.liquid.base.tools.StringUtil.isEmpty(wzry_zone.toString())) {
                                wzry_zone.append(s);
                            } else {
                                wzry_zone.append(" | ").append(s);
                            }
                        }
                    }
                }

                tv_game_wangzhe_platform.setText(wzry_platfrom.toString());
                tv_game_wangzhe_rank.setText(wzry_rank.toString());
                tv_game_wangzhe_area.setText(wzry_zone.toString());
                RequestOptions options = new RequestOptions()
                        .placeholder(R.mipmap.icon_wangzherongyao)
                        .error(R.mipmap.icon_wangzherongyao);
                Glide.with(getActivity()).load(wzrydata.getIcon()).apply(options).apply(RequestOptions.bitmapTransform(new RoundedCorners(12))).into(iv_wangzhe);
            } else {
                rl_game_wangzhe.setVisibility(View.GONE);
            }
        }
    }

    private void setHpjyParams(UserCenterInfo userCenterInfo) {
        UserCenterInfo.Data.UserInfo.GameInfoBean userInfo = userCenterInfo.getData().getUser_info().getGame_info();
        UserCenterInfo.HPJYDATA hpjydata = userCenterInfo.getHpjydata();
        if (userInfo != null && hpjydata != null) {
            UserCenterInfo.Data.UserInfo.GameInfoBean.HpjyBean hpjyBean = userInfo.getHpjy();
            if (hpjyBean != null) {
                if (hpjyBean.getPlatform().size() == 0 && hpjyBean.getRank().size() == 0 && hpjyBean.getZone().size() == 0) {
                    rl_game_heping.setVisibility(View.GONE);
                    isHePingGame = true;
                } else {
                    rl_game_heping.setVisibility(View.VISIBLE);
                    isHePingGame = false;
                }
                StringBuilder hpjy_platfrom = new StringBuilder();
                StringBuilder hpjy_rank = new StringBuilder();
                StringBuilder hpjy_zone = new StringBuilder();
                //平台
                List<Integer> platforms = hpjyBean.getPlatform();
                ArrayList<Map<String, String>> Hpjy_platfrom_list = hpjydata.getHpjy_platform_list();
                for (int i = 0; i < platforms.size(); i++) {
                    int num = platforms.get(i);

                    for (int j = 0; j < Hpjy_platfrom_list.size(); j++) {
                        Map<String, String> map = Hpjy_platfrom_list.get(j);
                        if (map.containsKey(num + "")) {
                            String s = map.get(num + "");
                            if (com.liquid.base.tools.StringUtil.isEmpty(hpjy_platfrom.toString())) {
                                hpjy_platfrom.append(s);
                            } else {
                                hpjy_platfrom.append(" | ").append(s);
                            }
                        }
                    }
                }

                //排行
                List<Integer> ranks = hpjyBean.getRank();
                ArrayList<Map<String, String>> hpjy_rank_list = hpjydata.getHpjy_rank_list();
                for (int i = 0; i < ranks.size(); i++) {
                    int num = ranks.get(i);

                    for (int j = 0; j < hpjy_rank_list.size(); j++) {
                        Map<String, String> map = hpjy_rank_list.get(j);
                        if (map.containsKey(num + "")) {
                            String s = map.get(num + "");
                            if (com.liquid.base.tools.StringUtil.isEmpty(hpjy_rank.toString())) {
                                hpjy_rank.append(s);
                            } else {
                                hpjy_rank.append(" | ").append(s);
                            }
                        }
                    }
                }

                //大区
                List<Integer> zones = hpjyBean.getZone();
                ArrayList<Map<String, String>> wzry_area_list = hpjydata.getHpjy_area_list();
                for (int i = 0; i < zones.size(); i++) {
                    int num = zones.get(i);

                    for (int j = 0; j < wzry_area_list.size(); j++) {
                        Map<String, String> map = wzry_area_list.get(j);
                        if (map.containsKey(num + "")) {
                            String s = map.get(num + "");
                            if (com.liquid.base.tools.StringUtil.isEmpty(hpjy_zone.toString())) {
                                hpjy_zone.append(s);
                            } else {
                                hpjy_zone.append(" | ").append(s);
                            }
                        }
                    }
                }

                tv_game_heping_platform.setText(hpjy_platfrom.toString());
                tv_game_heping_rank.setText(hpjy_rank.toString());
                tv_game_heping_area.setText(hpjy_zone.toString());
                RequestOptions options = new RequestOptions()
                        .placeholder(R.mipmap.icon_hepingjingying)
                        .error(R.mipmap.icon_hepingjingying);
                Glide.with(getActivity()).load(hpjydata.getIcon()).apply(options).apply(RequestOptions.bitmapTransform(new RoundedCorners(12))).into(iv_heping);
            } else {
                rl_game_heping.setVisibility(View.GONE);
            }

        }
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void showSuccess() {

    }

    @Override
    public void closeLoading() {

    }

    @Override
    public void onError() {

    }

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void onSuccess(Object data) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }


}
