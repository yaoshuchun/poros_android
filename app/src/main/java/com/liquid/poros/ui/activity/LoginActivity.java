package com.liquid.poros.ui.activity;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.liquid.base.event.EventCenter;
import com.liquid.base.utils.GlobalConfig;
import com.liquid.im.kit.permission.MPermission;
import com.liquid.im.kit.permission.annotation.OnMPermissionDenied;
import com.liquid.im.kit.permission.annotation.OnMPermissionGranted;
import com.liquid.im.kit.permission.annotation.OnMPermissionNeverAskAgain;
import com.liquid.poros.BuildConfig;
import com.liquid.poros.PorosApplication;
import com.liquid.poros.R;
import com.liquid.poros.analytics.utils.MultiTracker;
import com.liquid.poros.base.AppConfigViewModel;
import com.liquid.poros.base.BaseActivity;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.sdk.HeartBeatUtils;
import com.liquid.poros.ui.MainActivity;
import com.liquid.poros.ui.activity.setting.magic.MagicApiActivity;
import com.liquid.poros.ui.activity.user.SubmitUserInfoActivity;
import com.liquid.poros.ui.activity.user.TestSubmitUserInfoActivity;
import com.liquid.poros.ui.floatball.GiftPackageFloatManager;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.poros.utils.AppConfigUtil;
import com.liquid.poros.utils.SharePreferenceUtil;
import com.liquid.poros.utils.StringUtil;
import com.liquid.poros.utils.ViewUtils;
import com.liquid.poros.utils.network.RetrofitHttpManager;
import com.liquid.poros.wxapi.WXUtils;
import com.tencent.mm.opensdk.modelmsg.SendAuth;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.Tencent;
import com.tencent.tauth.UiError;

import org.json.JSONObject;

import butterknife.OnClick;

public class LoginActivity extends BaseActivity {
    private Tencent mTencent;

    @Override
    protected String getPageId() {
        return TrackConstants.PAGE_LOGIN;
    }

    @Override
    protected void parseBundleExtras(Bundle extras) {

    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.activity_login;
    }


    @Override
    protected int getStatusBarColor() {
        return getResources().getColor(R.color.custom_color_app_bg_night);
    }

    @Override
    protected void initViewsAndEvents(Bundle savedInstanceState) {
        String account_token = SharePreferenceUtil.getString(SharePreferenceUtil.FILE_USER_ACCOUNT_DATA, SharePreferenceUtil.KEY_USER_ACCOUNT_TOKEN, "");
        if (!StringUtil.isEmpty(account_token)) {
            AccountUtil.getInstance().auto_login(account_token, new AccountUtil.AutoLoginListener() {
                @Override
                public void onSucceed() {
                    readyGo(MainActivity.class);
                    finish();
                }

                @Override
                public void onFailed() {

                }
            });
        }
        mTencent = Tencent.createInstance(Constants.APP_ID, this);

        String[] perms = {Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.READ_PHONE_STATE
        };
        MPermission.with(LoginActivity.this)
                .setRequestCode(BASIC_PERMISSION_REQUEST_CODE)
                .permissions(perms)
                .request();

        TextView debug_api = findViewById(R.id.tv_debug_api);
        if (BuildConfig.DEBUG) {
            debug_api.setVisibility(View.VISIBLE);
        } else {
            debug_api.setVisibility(View.GONE);
        }
    }

    @OnMPermissionGranted(BASIC_PERMISSION_REQUEST_CODE)
    public void onBasicPermissionSuccess() {
        GlobalConfig.instance().init(PorosApplication.context, true);
        RetrofitHttpManager.getInstance().forceInitOkHttpClient();
    }

    @OnMPermissionDenied(BASIC_PERMISSION_REQUEST_CODE)
    @OnMPermissionNeverAskAgain(BASIC_PERMISSION_REQUEST_CODE)
    public void onBasicPermissionFailed() {
    }

    private BaseUiListener mBaseUIListener = new BaseUiListener();

    private class BaseUiListener implements IUiListener {

        protected void doComplete(Object values) {
            try {
                JSONObject jsonObject = new JSONObject(values.toString());
                String token = jsonObject.getString("access_token");
                String openId = jsonObject.getString("openid");
                qqLogin(openId, token);
            } catch (Exception e) {
            }
            MultiTracker.onEvent(TrackConstants.B_LOGIN_SUCCESS);

        }

        @Override
        public void onComplete(Object response) {
            doComplete(response);
        }

        @Override
        public void onError(UiError e) {

        }

        @Override
        public void onCancel() {
        }
    }

    private void qqLogin(String openid, String access_token) {
        AccountUtil.getInstance().qqLogin(openid, access_token, new AccountUtil.WechatLoginListener() {
            @Override
            public void onLoginSucceed() {
                getAppConfig();
            }

            @Override
            public void onLoginFailed(String msg) {
                showMessage(msg);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (mTencent != null) {
            mTencent.onActivityResultData(requestCode, resultCode, data, mBaseUIListener);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        MPermission.onRequestPermissionsResult(this, requestCode, permissions, grantResults);

    }


    @OnClick({R.id.btn_qq, R.id.btn_wechat, R.id.tv_user_protocol, R.id.tv_privacy_policy, R.id.tv_debug_api})
    public void onClick(View view) {
        if (ViewUtils.isFastClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.tv_debug_api:
                readyGo(MagicApiActivity.class);
                break;
            case R.id.btn_qq:
                if (mTencent != null && mTencent.isSupportSSOLogin(this)) {
                    mTencent.login(this, "get_user_info", mBaseUIListener);
                } else {
                    showMessage("对不起!请先安装QQ客户端~");
                }
                MultiTracker.onEvent(TrackConstants.B_CLICK_LOGIN);
                break;
            case R.id.btn_wechat:
                SendAuth.Req req = new SendAuth.Req();
                req.scope = "snsapi_userinfo";
                req.state = "state" + System.currentTimeMillis();
                if (WXUtils.getInstance(PorosApplication.context).getIWXApi().isWXAppInstalled()) {
                    WXUtils.getInstance(PorosApplication.context).getIWXApi().sendReq(req);
                } else {
                    showMessage("请先安装微信");
                }
                MultiTracker.onEvent(TrackConstants.B_CLICK_LOGIN);

                break;
            case R.id.tv_user_protocol:
                Bundle bundle = new Bundle();
                bundle.putString(Constants.TARGET_PATH, Constants.BASE_URL_H5 + "web/user_agreement_new.html");
                readyGo(WebViewActivity.class, bundle);
                break;
            case R.id.tv_privacy_policy:
                Bundle bundle1 = new Bundle();
                bundle1.putString(Constants.TARGET_PATH, Constants.BASE_URL_H5 + "web/user_conceal_new.html");
                readyGo(WebViewActivity.class, bundle1);
                break;
        }
    }

    /**
     * 统一获取appconfig配置
     */
    public void getAppConfig() {
        AppConfigViewModel appViewModel = getActivityViewModel(AppConfigViewModel.class);
        appViewModel.getAppConfig();
        appViewModel.getAppConfigDataSuccess().observe(this, AppBaseConfigBean -> {
            try {
                //@TODO  ----保持原有逻辑不变，逐步替换
                Constants.SHARE_GROUP_LINK = AppBaseConfigBean.getShare_group_url() + "%s";
                Constants.FEEDBACK_GROUP = AppBaseConfigBean.getFeedback_group();
                Constants.team_leader_tips = AppBaseConfigBean.getTeam_leader_tips();
                Constants.team_user_tips = AppBaseConfigBean.getTeam_user_tips();
                Constants.live_tips = AppBaseConfigBean.getLive_tips();
                Constants.together_room_anchor_tips = AppBaseConfigBean.getTogether_room_anchor_tips();
                Constants.together_room_other_tips = AppBaseConfigBean.getTogether_room_other_tips();
                Constants.show_age_level_continue = AppBaseConfigBean.getShow_age_level_continue();
                Constants.SERVER_TIMESTAMP_DETLA = System.currentTimeMillis() - AppBaseConfigBean.getServer_timestamp();
                Constants.cp_room_anchor_tips = AppBaseConfigBean.getCp_room_anchor_tips();
                Constants.cp_room_mic_user_tips = AppBaseConfigBean.getCp_room_mic_user_tips();
                Constants.cp_room_watch_user_tips = AppBaseConfigBean.getCp_room_watch_user_tips();
                Constants.private_room_tips = AppBaseConfigBean.getPrivate_room_tips();
                Constants.hide_makeup = AppBaseConfigBean.getHide_makeup();
                Constants.info_games = AppBaseConfigBean.getGames();
                Constants.cp_right_content = AppBaseConfigBean.getCp_right_content();
                Constants.cp_right_title = AppBaseConfigBean.getCp_right_title();
                Constants.cp_voice_chat_price = AppBaseConfigBean.getCp_voice_chat_price();
                Constants.hide_peking_user = AppBaseConfigBean.getIs_forbid_user();
                Constants.show_feed = AppBaseConfigBean.getShow_feed();
                Constants.recharge_way = AppBaseConfigBean.getRecharge_way();
                Constants.is_sounds_vip = AppBaseConfigBean.isIs_sounds_vip();
                Constants.buy_vip_money = AppBaseConfigBean.getBuy_vip_money();
                Constants.buy_vip_desc = AppBaseConfigBean.getBuy_vip_desc();
                Constants.buy_vip_time_limit = AppBaseConfigBean.getBuy_vip_time_limit();
                Constants.get_gift_box = AppBaseConfigBean.getGet_gift_box();
                Constants.buy_vip_get_coin = AppBaseConfigBean.getBuy_vip_get_coin();
                //SharePreferenceUtil.saveInt(SharePreferenceUtil.FILE_INFO_GAMES_DATA, SharePreferenceUtil.KEY_SHOW_SQUARE_FEED,AppBaseConfigBean.getShow_square_feed());
                SharePreferenceUtil.saveStringArray(SharePreferenceUtil.FILE_INFO_GAMES_DATA, SharePreferenceUtil.KEY_INFO_GAMES, AppBaseConfigBean.getGames());
                GiftPackageFloatManager.getInstance().recharge_way = AppBaseConfigBean.getRecharge_way();
                //@TODO  ---保持原有逻辑不变，逐步替换
                AppConfigUtil.saveBaseConfig(AppBaseConfigBean);
            } catch (Throwable e) {
                e.printStackTrace();
            }
            startHome();
        });

        appViewModel.getAppConfigDataFail().observe(this, AppBaseConfigBean -> {
            startHome();
        });
    }

    //废弃
//    public void getConfig() {
//        JSONObject object = new JSONObject();
//        try {
//            object.put("token", AccountUtil.getInstance().getAccount_token());
//            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json;charset=utf-8"), object.toString());
//            RetrofitHttpManager.getInstance().httpInterface.get_config(body).enqueue(new HttpCallback() {
//                @Override
//                public void OnSucceed(String result) {
//                    LogOut.e("getConfig", result);
//                    AppConfig config = GT.fromJson(result, AppConfig.class);
//                    if (config != null && config.getData() != null && config.getCode() == 0) {
//                        Constants.SHARE_GROUP_LINK = config.getData().getShare_group_url() + "%s";
//                        Constants.FEEDBACK_GROUP = config.getData().getFeedback_group();
//                        Constants.team_leader_tips = config.getData().getTeam_leader_tips();
//                        Constants.team_user_tips = config.getData().getTeam_user_tips();
//                        Constants.live_tips = config.getData().getLive_tips();
//                        Constants.together_room_anchor_tips = config.getData().getTogether_room_anchor_tips();
//                        Constants.together_room_other_tips = config.getData().getTogether_room_other_tips();
//                        Constants.show_age_level_continue = config.getData().isShow_age_level_continue();
//                        Constants.SERVER_TIMESTAMP_DETLA = System.currentTimeMillis() - config.getData().getServer_timestamp();
//                        Constants.cp_room_anchor_tips = config.getData().getCp_room_anchor_tips();
//                        Constants.cp_room_mic_user_tips = config.getData().getCp_room_mic_user_tips();
//                        Constants.cp_room_watch_user_tips = config.getData().getCp_room_watch_user_tips();
//                        Constants.private_room_tips = config.getData().getPrivate_room_tips();
//                        Constants.hide_makeup = config.getData().isHide_makeup();
//                        Constants.info_games = config.getData().getGames();
//                        Constants.cp_right_content = config.getData().getCp_right_content();
//                        Constants.cp_right_title = config.getData().getCp_right_title();
//                        Constants.cp_voice_chat_price = config.getData().getCp_voice_chat_price();
//                        Constants.hide_peking_user = config.getData().isIs_forbid_user();
//                        Constants.show_feed = config.getData().getShow_feed();
//                        Constants.ad_csj_appid = config.getData().getAd_csj_appid();
//                        Constants.ad_csj_codeid = config.getData().getAd_csj_codeid();
//                        SharePreferenceUtil.saveStringArray(SharePreferenceUtil.FILE_INFO_GAMES_DATA, SharePreferenceUtil.KEY_INFO_GAMES, config.getData().getGames());
//                        SharePreferenceUtil.saveInt(SharePreferenceUtil.FILE_INFO_GAMES_DATA, SharePreferenceUtil.KEY_SHOW_SQUARE_FEED, config.getData().getShow_square_feed());
//                        GiftPackageFloatManager.getInstance().recharge_way = config.getData().getRecharge_way();
//                        startHome();
//                    } else {
//                        startHome();
//                    }
//                }
//
//                @Override
//                public void OnFailed(int ret, String result) {
//                    startHome();
//                }
//            });
//        } catch (Throwable e) {
//            e.printStackTrace();
//            startHome();
//        }
//    }

    @Override
    protected void onEventComing(EventCenter eventCenter) {
        if (eventCenter.getEventCode() == Constants.EVENT_CODE_WECHAT_AUTH_FINISH) {
            String code = (String) eventCenter.getData();
            wechatLogin(code);
            MultiTracker.onEvent("b_login_success");
        }
    }

    private void wechatLogin(String code) {
        AccountUtil.getInstance().wechatLogin(code, new AccountUtil.WechatLoginListener() {
            @Override
            public void onLoginSucceed() {
                getAppConfig();
            }

            @Override
            public void onLoginFailed(String msg) {
                showMessage(msg);
            }
        });
    }

    private void startHome() {
        boolean isNeedSetBasicInfo = AccountUtil.getInstance().isNeedSetBasicInfo();
        boolean isMatchControl = AccountUtil.getInstance().isMatchControl();
        if (isNeedSetBasicInfo) {
            if (isMatchControl) {
                //对照组
                readyGo(SubmitUserInfoActivity.class);
            } else {
                //实验组
                readyGo(TestSubmitUserInfoActivity.class);
            }
        } else {
            readyGo(MainActivity.class);
        }
        finish();
    }

}
