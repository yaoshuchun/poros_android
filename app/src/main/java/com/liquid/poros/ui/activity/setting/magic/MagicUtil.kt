package com.liquid.poros.ui.activity.setting.magic

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import com.liquid.poros.base.BaseActivity
import com.liquid.poros.ui.fragment.dialog.common.CommonActionListener
import com.liquid.poros.ui.fragment.dialog.common.CommonDialogBuilder
import com.liquid.poros.ui.fragment.dialog.common.CommonDialogFragment
import kotlin.system.exitProcess

/**
 * 重启弹窗
 * @author yejiurui
 */
object MagicUtil {

    @JvmStatic
    fun showRestartAppAlertDialog(activity: BaseActivity, callback: RestartAppAlertCallback) {
        CommonDialogBuilder()
                .setWithCancel(true)
                .setTitle("重启提示")
                .setDesc("重启后操作生效，App将自动重启，如果没有重启，请手动操作")
                .setCancel("取消")
                .setConfirm("确认")
                .setListener(object : CommonActionListener {
                    override fun onConfirm() {
                        callback.onPositive()
                        val intent = activity.packageManager.getLaunchIntentForPackage(activity.packageName)
                        intent?.let {
                            it.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                            val restartIntent = PendingIntent.getActivity(
                                    activity.applicationContext, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT)
                            val mgr = activity.getSystemService(Context.ALARM_SERVICE) as AlarmManager
                            mgr[AlarmManager.RTC, System.currentTimeMillis() + 200] = restartIntent // 定时重启应用
                        }
                        exitProcess(0)
                    }

                    override fun onCancel() {}
                }).create().show(activity.supportFragmentManager, CommonDialogFragment::class.java.simpleName)


    }

    interface RestartAppAlertCallback {
        fun onPositive()
    }
}