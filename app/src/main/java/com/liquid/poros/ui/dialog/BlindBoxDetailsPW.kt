package com.liquid.poros.ui.dialog

import android.content.Context
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.ViewParent
import com.blankj.utilcode.util.SizeUtils
import com.liquid.base.event.EventCenter
import com.liquid.poros.R
import com.liquid.poros.analytics.utils.MultiTracker
import com.liquid.poros.constant.Constants
import com.liquid.poros.constant.TrackConstants
import com.liquid.poros.databinding.PopupBlindBoxDetailsBinding
import com.liquid.poros.utils.InJavaScriptLocalObjUtils
import com.tencent.smtt.sdk.WebChromeClient
import com.tencent.smtt.sdk.WebSettings
import com.tencent.smtt.sdk.WebView
import de.greenrobot.event.EventBus
import de.greenrobot.event.Subscribe
import de.greenrobot.event.ThreadMode
import razerdp.basepopup.BasePopupWindow
import java.util.HashMap

/**
 * 盲盒详情弹窗
 */
class BlindBoxDetailsPW(context: Context) : BasePopupWindow(context) {
    private lateinit var binding: PopupBlindBoxDetailsBinding
    private var trackClickChannel = false//盲盒点击来源 ad_picture 广告图、other其他

    override fun onCreateContentView(): View {
        popupGravity = Gravity.END or Gravity.BOTTOM
        setPopupGravityMode(GravityMode.ALIGN_TO_ANCHOR_SIDE, GravityMode.RELATIVE_TO_ANCHOR)
        offsetX = SizeUtils.dp2px(5.0f)
        return createPopupById(R.layout.popup_blind_box_details)
    }

    override fun onViewCreated(contentView: View) {
        super.onViewCreated(contentView)
        binding = PopupBlindBoxDetailsBinding.bind(contentView)
        EventBus.getDefault().register(this)
        initWebView()
    }

    override fun onShowing() {
        super.onShowing()
        MultiTracker.onEvent(TrackConstants.B_BLIND_BOX_INTRODUCE_POPUP_EXPOSURE)
    }

    private fun initWebView() {
        val webSetting: WebSettings = binding.wvView.settings
        webSetting.allowFileAccess = true
        webSetting.layoutAlgorithm = WebSettings.LayoutAlgorithm.NARROW_COLUMNS
        webSetting.setSupportZoom(false)
        webSetting.builtInZoomControls = false
        webSetting.displayZoomControls = false
        webSetting.useWideViewPort = true
        webSetting.setSupportMultipleWindows(false)
        webSetting.loadWithOverviewMode = true
        webSetting.setAppCacheEnabled(true)
        webSetting.domStorageEnabled = true
        webSetting.javaScriptEnabled = true
        webSetting.setGeolocationEnabled(true)
        webSetting.setAppCacheMaxSize(Long.MAX_VALUE)
        webSetting.setAppCachePath(context.getDir("appcache", 0).path)
        webSetting.databasePath = context.getDir("databases", 0).path
        webSetting.setGeolocationDatabasePath(context.getDir("geolocation", 0).path)
        webSetting.pluginState = WebSettings.PluginState.ON_DEMAND
        webSetting.cacheMode = WebSettings.LOAD_DEFAULT
        var inJavaScriptLocalObjUtils = InJavaScriptLocalObjUtils(context, binding.wvView)
        binding.wvView.addJavascriptInterface(inJavaScriptLocalObjUtils, "JSObj")
    }

    /**
     * 设置盲盒详情url
     * @param trackClickChannel  点击来源 ad_picture 广告图、other其他
     */
    fun setBlindBoxH5(blindBoxH5: String?,trackClickChannel: Boolean) {
        this.trackClickChannel = trackClickChannel
        binding.pbProgressBar.max = 100
        binding.pbProgressBar.progressDrawable = context.resources.getDrawable(R.drawable.webview_progressbar_color)
        binding.wvView.webChromeClient = object : WebChromeClient() {
            override fun onProgressChanged(webView: WebView, progress: Int) {
                super.onProgressChanged(webView, progress)
                if (binding.pbProgressBar != null) {
                    binding.pbProgressBar.progress = progress
                    if (progress == 100) {
                        binding.pbProgressBar.visibility = View.GONE
                    } else {
                        binding.pbProgressBar.visibility = View.VISIBLE
                    }
                }
            }
        }
        binding.wvView.loadUrl(blindBoxH5)
    }

    @Subscribe(threadMode = ThreadMode.MainThread)
    fun onEventBus(eventCenter: EventCenter<*>) {
        when (eventCenter.eventCode) {
            Constants.EVENT_CODE_ENTRY_BLIND_BOX ->{
                dismiss()
                val params: MutableMap<String?, String?> = HashMap()
                //点击来源 ad_picture 广告图、other其他
                params["click_channel"] = if (trackClickChannel) "ad_picture" else "other"
                MultiTracker.onEvent(TrackConstants.B_BLIND_BOX_INTRODUCE_TO_SEE_BUTTON_CLICK,params)
            }
        }
    }
    override fun onDestroy() {
        super.onDestroy()
        EventBus.getDefault().unregister(this)
        if ( binding.wvView != null) {
            val parent: ViewParent =  binding.wvView.parent
            if (parent != null) {
                (parent as ViewGroup).removeView( binding.wvView)
            }
            binding.wvView.stopLoading()
            binding.wvView.settings.javaScriptEnabled = false
            binding.wvView.clearHistory()
            binding.wvView.clearView()
            binding.wvView.removeAllViews()
            binding.wvView.destroy()
        }
    }
}