package com.liquid.poros.ui.activity.live

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.media.AudioManager
import android.media.MediaPlayer
import android.os.Bundle
import android.os.Handler
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.TextView
import android.widget.ViewSwitcher
import androidx.lifecycle.Observer
import com.airbnb.lottie.LottieDrawable
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.request.RequestOptions
import com.liquid.base.event.EventCenter
import com.liquid.base.utils.ToastUtil
import com.liquid.library.retrofitx.utils.LogUtils
import com.liquid.poros.PorosApplication
import com.liquid.poros.R
import com.liquid.poros.analytics.utils.MultiTracker
import com.liquid.poros.base.BaseActivity
import com.liquid.poros.constant.Constants
import com.liquid.poros.constant.TrackConstants
import com.liquid.poros.entity.CheckInfo
import com.liquid.poros.entity.VideoMatchInfo
import com.liquid.poros.ui.activity.live.viewmodel.VideoFastDatingViewModel
import com.liquid.poros.ui.dialog.RechargeCoinDialog
import com.liquid.poros.ui.fragment.dialog.common.CommonActionListener
import com.liquid.poros.ui.fragment.dialog.common.CommonDialogBuilder
import com.liquid.poros.ui.fragment.dialog.common.CommonDialogFragment
import com.liquid.poros.utils.AccountUtil
import com.liquid.poros.utils.ScreenUtil
import com.liquid.poros.utils.StringUtil
import com.liquid.poros.utils.TextViewUtils
import com.liquid.poros.widgets.TextSwitcherAnimation
import de.greenrobot.event.Subscribe
import de.greenrobot.event.ThreadMode
import kotlinx.android.synthetic.main.activity_video_fast_dating.*
import kotlinx.coroutines.runBlocking
import java.math.BigDecimal

/**
 * 视频速配页
 */
class VideoFastDatingActivity : BaseActivity() {
    private var mMediaPlayer: MediaPlayer? = null
    private var mMatchInfo: VideoMatchInfo? = null
    private var video_match_price: Int = 0
    var videoFastDatingViewModel: VideoFastDatingViewModel? = null
    private var videoMatchSwitchList = ArrayList<String>()

    //匹配信息轮播
    private var mTextSwitcherAnimation: TextSwitcherAnimation? = null

    //匹配中
    private var isMatching: Boolean = true
    //速配来源
    private var mIsSquare: Boolean = true // 广场进入
    //进入速配通话界面
    private var ENTRY_VIDEO_MATCH:  Int= 200

    private var mHandler:Handler? = null

    override fun parseBundleExtras(extras: Bundle?) {

    }

    override fun getPageId(): String {
        return TrackConstants.PAGE_MATCHING
    }

    override fun getContentViewLayoutID(): Int {
        return R.layout.activity_video_fast_dating
    }

    override fun initViewsAndEvents(savedInstanceState: Bundle?) {
        videoFastDatingViewModel = getActivityViewModel(VideoFastDatingViewModel::class.java)
        this.mTextSwitcherAnimation = TextSwitcherAnimation(ts_video_match_switch)
        val options = RequestOptions()
                .placeholder(R.mipmap.img_default_avatar)
                .override(ScreenUtil.dip2px(120f), ScreenUtil.dip2px(120f))
                .error(R.mipmap.img_default_avatar)
                .apply(RequestOptions.bitmapTransform(CircleCrop()))
        Glide.with(this)
                .load(AccountUtil.getInstance()?.userInfo?.avatar_url)
                .apply(options)
                .into(iv_user_avatar)
        initObserver()
        initListener()
    }

    private fun initObserver() {
        videoFastDatingViewModel?.userCoinLiveData?.observe(this, Observer {
            checkUserCoin(it.video_free_minutes)
        })
        videoFastDatingViewModel?.videoMatchInfoLiveData?.observe(this, androidx.lifecycle.Observer {
            updateMatchingInfo(it)
        })
        videoFastDatingViewModel?.videoMatchCoinNotEnough?.observe(this, Observer {
            matchSuccessCheckCoin()
        })
        videoFastDatingViewModel?.cancelVideoMatchMicLiveData?.observe(this, Observer {
            LogUtils.d("下麦成功")
        })
        videoFastDatingViewModel?.checkVideoMatchMicLiveData?.observe(this,Observer {
           checkEnter(it)
        })
        videoFastDatingViewModel?.checkVideoMatchMicFailLiveData?.observe(this,Observer {
            ToastUtil.show("该房间暂不允许加入，请重试")
            startMatching()
        })
    }

    private fun checkEnter(info: CheckInfo?) {
        if (true == info?.is_join){
            releaseMatchingMedia()
            if(mMatchInfo == null|| mMatchInfo?.room_id.isNullOrEmpty()) {
                startMatching()
                return
            }
            mHandler = null
            releaseMatchingMedia()
            var bundle = Bundle()
            bundle.putString(Constants.ENTER_VIDEO_MATCH_SOURCE, Constants.ENTER_VIDEO_MATCH_FAST_DATING)
            bundle.putSerializable(Constants.VIDEO_MATCH_DATA_PASS_KEY, mMatchInfo)
//            readyGoForResult(VideoMatchChattingActivity::class.java, ENTRY_VIDEO_MATCH, bundle)
            readyGo(VideoMatchChattingActivity::class.java, bundle)
            finish()
        }else{
            ToastUtil.show("该房间暂不允许加入，请重试")
            startMatching()
        }
    }

    override fun onResume() {
        super.onResume()
        startMatching()
        val params: MutableMap<String, String> = HashMap<String, String>()
        params["enter_match_source"] = if (mIsSquare) Constants.ENTER_VIDEO_MATCH_SQUARE else Constants.ENTER_VIDEO_MATCH_NO_SQUARE
        MultiTracker.onEvent(TrackConstants.B_VIDEO_MATCH_PAGE_EXPOSURE, params)
    }

    private fun startMatching() {
        isMatching = true
        playMatchingMedia()
        //获取用户金币
        videoFastDatingViewModel?.getUserCoin()
    }

    override fun onPause() {
        super.onPause()
        releaseMatchingMedia()
    }

    private fun initListener() {
        iv_exit.setOnClickListener {
            showExitConfirmDialog()
            MultiTracker.onEvent("b_video_match_page_close_click")
        }
    }

    private fun showExitConfirmDialog() {
        CommonDialogBuilder()
                .setImageTitleRes(R.mipmap.icon_close_video_match)
                .setDesc(getString(R.string.video_match_close))
                .setCancel(getString(R.string.cancel))
                .setCancelAble(false)
                .setConfirm(getString(R.string.sure))
                .setListener(object : CommonActionListener {
                    override fun onConfirm() {
                        finishMatching()
                    }

                    override fun onCancel() {

                    }
                }).create().show(supportFragmentManager, CommonDialogFragment::class.java.simpleName)
    }

    /**
     * 速配等待语音提示
     */
    private fun playMatchingMedia() {
        try {
            if (mMediaPlayer != null) {
                releaseMatchingMedia()
            }
            mMediaPlayer = MediaPlayer.create(this, R.raw.audio_match_waiting)
            mMediaPlayer!!.isLooping = true
            mMediaPlayer!!.setAudioStreamType(AudioManager.STREAM_MUSIC)
            mMediaPlayer?.start()
        } catch (e: Throwable) {
            e.printStackTrace()
        }
    }

    private fun releaseMatchingMedia() {
        isMatching = false
        mMediaPlayer?.stop()
        mMediaPlayer?.reset()
        mMediaPlayer?.release()
        mMediaPlayer = null
    }

    private fun updateMatchingInfo(matchInfo: VideoMatchInfo) {
        mMatchInfo = matchInfo
        video_match_price = matchInfo.video_match_price
        if (!matchInfo.video_match_success_list.isNullOrEmpty()) {
            val videoMatchSuccessList = matchInfo.video_match_success_list
            runBlocking {
                videoMatchSwitchList?.clear()
                for (index in videoMatchSuccessList?.indices) {
                    val matchedInfo = videoMatchSuccessList[index]
                    var videoText = "${TextViewUtils.getColoredSpanned("&lt;${matchedInfo.female_name}&gt;", "#10D8C8")}" +
                            "${TextViewUtils.getColoredSpanned("和", "#898996")}" +
                            "${TextViewUtils.getColoredSpanned("&lt;${matchedInfo.male_name}&gt;", "#10D8C8")}" +
                            "${TextViewUtils.getColoredSpanned("正在快乐的聊天", "#898996")}"
                    videoMatchSwitchList?.add(videoText)
                }
                if (!videoMatchSwitchList.isNullOrEmpty()) {
                    ts_video_match_switch.visibility = View.VISIBLE
                    if (ts_video_match_switch != null && ts_video_match_switch.childCount < 2) {
                        ts_video_match_switch.setFactory(ViewSwitcher.ViewFactory {
                            val textView = TextView(this@VideoFastDatingActivity)
                            textView.setTextColor(Color.parseColor("#ffffff"))
                            textView.textSize = 12f
                            textView.gravity = Gravity.CENTER
                            textView.setSingleLine(true)
                            textView.layoutParams = FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
                            textView
                        })
                    }
                    mTextSwitcherAnimation!!.setHtmlTexts(true)
                    mTextSwitcherAnimation!!.setTexts(videoMatchSwitchList.toTypedArray()).create()
                }
            }
        }
        matchInfo.room_id?.let {

            if(!StringUtil.isEmpty(matchInfo?.live_id)&& !StringUtil.isEmpty(matchInfo?.room_id)&&
                    !StringUtil.isEmpty(matchInfo?.video_room_id)){
                isMatching = false
                videoFastDatingViewModel?.checkVideoMatchMic(matchInfo.live_id!!, matchInfo.room_id!!, matchInfo.video_room_id!!)
            }
        }

        if (isMatching) {//匹配成功前，每隔5s请求一次
            if (mHandler == null) {
                mHandler = Handler()
            }
            mHandler?.removeCallbacksAndMessages(null)
            mHandler?.postDelayed({
                videoFastDatingViewModel?.getVideoMatch()
            }, 5000)
        }

    }

    private fun checkUserCoin(coin: Int) {
        if (!checkNotEnoughCoinTip(coin)) {
            try {
                //金币数量充足,开启速配
                group_fast_dating.visibility = View.VISIBLE
                lav_video_avatar.setAnimation("videomatch/data.json")
                lav_video_avatar.repeatMode = LottieDrawable.REVERSE//设置播放模式
                lav_video_avatar.repeatCount = LottieDrawable.INFINITE//设置重复次数
                lav_video_avatar.playAnimation()
                videoFastDatingViewModel?.getVideoMatch()
                trackStartVideoMatch()
            } catch (e: Throwable) {

            }
        } else {
            //金币不足
            matchSuccessCheckCoin()
            val params: MutableMap<String, String?> = HashMap()
            params["record_id"] = mMatchInfo?.live_id
            params["room_id"] = mMatchInfo?.room_id
            params["enter_match_source"] = if (mIsSquare) Constants.ENTER_VIDEO_MATCH_SQUARE else Constants.ENTER_VIDEO_MATCH_NO_SQUARE
            MultiTracker.onEvent(TrackConstants.B_VIDEO_MATCH_NOT_ENOUGH_COIN_POPUP_EXPOSURE, params)
        }
    }


    @Subscribe(threadMode = ThreadMode.MainThread)
    override fun onEventBus(eventCenter: EventCenter<*>?) {
        if (null != eventCenter) {
            if (eventCenter.eventCode == Constants.EVENT_CODE_COIN_CHANGE) {
                val data = eventCenter.data as String
                try {
                    var coin = data.toLong()
                    checkUserCoin(coin.toInt())
                }catch (e:Exception){
                    e.printStackTrace()
                }
                val params: MutableMap<String, String?> = HashMap<String, String?>()
                params["record_id"] = mMatchInfo?.live_id
                params["room_id"] = mMatchInfo?.room_id
                params["enter_match_source"] = if (mIsSquare) Constants.ENTER_VIDEO_MATCH_SQUARE else Constants.ENTER_VIDEO_MATCH_NO_SQUARE
                MultiTracker.onEvent(TrackConstants.B_VIDEO_MATCH_COIN_RECHARGE_SUCCESS_EXPOSURE, params)
            }
        }
    }
    /**
     * 校验金币数量
     * @param videoFreeMinutes //速配分钟补贴 0 分钟用尽,调起充值弹窗，1 正常匹配
     */
    private fun checkNotEnoughCoinTip(videoFreeMinutes: Int): Boolean {
        if (videoFreeMinutes == 0) {
            return true
        }
        return false
    }

    /**
     * 匹配过程中,再次校验金币
     */
    private fun matchSuccessCheckCoin() {
        CommonDialogBuilder()
                .setTopImageRes(R.drawable.ic_coin_not_enough)
                .setTitle(getString(R.string.video_match_success))
                .setDesc("视频聊天20金币/分钟\n 当前金币余额不足，无法与对方视频聊天")
                .setConfirm(getString(R.string.live_room_private_room_recharge))
                .setCancel(getString(R.string.video_match_exit))
                .setListener(object : CommonActionListener {
                    override fun onConfirm() {
                        var fragment = RechargeCoinDialog()
                        fragment.setVideoMatchRoom(true)
                        fragment.show(supportFragmentManager, RechargeCoinDialog::class.java.name)
                        val params: MutableMap<String, String?> = HashMap<String, String?>()
                        params["record_id"] = mMatchInfo?.live_id
                        params["room_id"] = mMatchInfo?.room_id
                        params["enter_match_source"] = if (mIsSquare) Constants.ENTER_VIDEO_MATCH_SQUARE else Constants.ENTER_VIDEO_MATCH_NO_SQUARE
                        MultiTracker.onEvent(TrackConstants.B_VIDEO_MATCH_NOT_ENOUGH_COIN_POPUP_RECHARGE_CLICK, params)
                    }

                    override fun onCancel() {
                        finishMatching()
                        val params: MutableMap<String, String?> = HashMap<String, String?>()
                        params["record_id"] = mMatchInfo?.live_id
                        params["room_id"] = mMatchInfo?.room_id
                        params["enter_match_source"] = if (mIsSquare) Constants.ENTER_VIDEO_MATCH_SQUARE else Constants.ENTER_VIDEO_MATCH_NO_SQUARE
                        MultiTracker.onEvent(TrackConstants.B_VIDEO_MATCH_NOT_ENOUGH_COIN_POPUP_CLOSE_CLICK, params)
                    }
                }).create().show(supportFragmentManager, CommonDialogFragment::class.java.simpleName)
    }


    override fun onBackPressed() {
        showExitConfirmDialog()
    }

    /**
     * 结束速配
     */
    fun finishMatching() {
        trackFinishVideoMatch()
        releaseMatchingMedia()
        finish()
    }

//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
//        if (resultCode == Activity.RESULT_OK) {
//            when (requestCode) {
//                ENTRY_VIDEO_MATCH -> {
//                    mIsSquare = false
//                    if(!PorosApplication.getBackground()){
//                        finish()
//                    }
//                }
//            }
//
//        }
//    }

    override fun onDestroy() {
        super.onDestroy()
        if (mHandler != null) {
            mHandler!!.removeCallbacksAndMessages(null)
            mHandler = null
        }
        if (mTextSwitcherAnimation != null) {
            mTextSwitcherAnimation!!.stop()
            mTextSwitcherAnimation = null
        }
    }

    /**
     * 开始匹配曝光
     */
    private fun trackStartVideoMatch(){
        val params: MutableMap<String, String?> = HashMap<String, String?>()
        params["record_id"] = mMatchInfo?.live_id
        params["room_id"] = mMatchInfo?.room_id
        params["enter_match_source"] = if (mIsSquare) Constants.ENTER_VIDEO_MATCH_SQUARE else Constants.ENTER_VIDEO_MATCH_NO_SQUARE
        MultiTracker.onEvent(TrackConstants.B_VIDEO_MATCH_TRUE_EXPOSURE, params)
    }

    /**
     * 点击退出速配
     */
    private fun trackFinishVideoMatch(){
        val params: MutableMap<String, String?> = HashMap<String, String?>()
        params["record_id"] = mMatchInfo?.live_id
        params["room_id"] = mMatchInfo?.room_id
        params["enter_match_source"] = if (mIsSquare) Constants.ENTER_VIDEO_MATCH_SQUARE else Constants.ENTER_VIDEO_MATCH_NO_SQUARE
        MultiTracker.onEvent(TrackConstants.B_VIDEO_MATCH_PAGE_CLOSE_CLICK, params)
    }
}