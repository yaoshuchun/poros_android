package com.liquid.poros.ui.fragment.dialog.user;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.liquid.poros.R;
import com.liquid.poros.widgets.WheelView;

import java.util.Arrays;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * 选择年龄弹窗
 */
public class ChooseAgeDialogFragment extends BottomSheetDialogFragment {
    private WheelView rv_wheel_choose;
    private static final String[] AGE_LEVEL = new String[]{"10岁", "11岁", "12岁", "13岁", "14岁", "15岁", "16岁", "17岁", "18岁",
            "19岁", "20岁", "21岁", "22岁", "23岁", "24岁", "25岁", "26岁", "27岁", "28岁", "29岁", "30岁", "31岁", "32岁", "33岁", "34岁", "35岁", "36岁", "37岁", "38岁", "39岁", "40岁"};
    private String mSelectItem;

    public static ChooseAgeDialogFragment newInstance() {
        ChooseAgeDialogFragment fragment = new ChooseAgeDialogFragment();
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
        View view = View.inflate(getContext(), R.layout.fragment_choose_age_dialog, null);
        dialog.setContentView(view);
        //设置透明背景
        dialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);
        View root = dialog.getDelegate().findViewById(R.id.design_bottom_sheet);
        BottomSheetBehavior behavior = BottomSheetBehavior.from(root);
        behavior.setHideable(false);
        initializeView(view);
        return dialog;
    }

    private void initializeView(View view) {
        rv_wheel_choose = view.findViewById(R.id.rv_wheel_choose);
        rv_wheel_choose.setOffset(2);
        rv_wheel_choose.setItems(Arrays.asList(AGE_LEVEL));
        mSelectItem = AGE_LEVEL[5].replace("岁", "");
        rv_wheel_choose.setSeletion(5);
        rv_wheel_choose.setOnWheelViewListener(new WheelView.OnWheelViewListener() {
            @Override
            public void onSelected(int selectedIndex, String item) {
                mSelectItem = item.replace("岁", "");
            }
        });
        view.findViewById(R.id.tv_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissAllowingStateLoss();
            }
        });
        view.findViewById(R.id.tv_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onChoose(mSelectItem);
                }
                dismissAllowingStateLoss();
            }
        });
    }

    private ChooseAgeDialogFragment.WheelChooseListener listener;

    public ChooseAgeDialogFragment setChooseAgeListener(ChooseAgeDialogFragment.WheelChooseListener listener) {
        this.listener = listener;
        return this;
    }

    public interface WheelChooseListener {
        void onChoose(String selectItem);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }
}
