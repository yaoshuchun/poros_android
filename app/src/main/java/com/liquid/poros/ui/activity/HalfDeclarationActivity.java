package com.liquid.poros.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.liquid.base.tools.GT;
import com.liquid.base.tools.StringUtil;
import com.liquid.base.utils.ContextUtils;
import com.liquid.poros.R;
import com.liquid.poros.base.BaseActivity;
import com.liquid.poros.analytics.utils.MultiTracker;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.entity.CpLevelUpDetail;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.poros.utils.ViewUtils;
import com.liquid.poros.utils.network.HttpCallback;
import com.liquid.poros.utils.network.RetrofitHttpManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.OnClick;
import okhttp3.RequestBody;

public class HalfDeclarationActivity extends BaseActivity {

    @BindView(R.id.iv_back)
    ImageView iv_back;

    @BindView(R.id.target_declaration_iv)
    ImageView target_declaration_iv;
    @BindView(R.id.target_declaration_head)
    ImageView target_declaration_head;
    @BindView(R.id.target_user_name)
    TextView target_user_name;
    @BindView(R.id.target_declaration_desc)
    TextView target_declaration_desc;
    @BindView(R.id.level_desc)
    TextView level_desc;
    @BindView(R.id.target_user_gift_img)
    ImageView target_user_gift_img;

    private String female_user_id;
    private String female_user_head;
    private String name;
    private String rise_to;
    private String intimacy;

    @Override
    protected String getPageId() {
        return TrackConstants.PAGE_HALF_DECLARATION;
    }

    @Override
    protected void parseBundleExtras(Bundle extras) {
        female_user_id = extras.getString("female_user_id");
        female_user_head = extras.getString("female_user_head");
        name = extras.getString("female_user_name");

        rise_to = extras.getString("rise_to");
        intimacy = extras.getString("intimacy");
    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.acitivity_half_declaration;
    }
    @Override
    protected void initViewsAndEvents(Bundle savedInstanceState) {
        showLoading();
        getWindow().getDecorView().postDelayed(new Runnable() {
            @Override
            public void run() {
                checkCpLevelDetail();
            }
        }, 700);
        Glide.with(HalfDeclarationActivity.this).load(female_user_head).into(target_user_gift_img);

        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", AccountUtil.getInstance().getUserId());
        params.put("female_guest_id", female_user_id);
        params.put("event_time", String.valueOf(System.currentTimeMillis()));
        params.put("rise_to",rise_to);
        params.put("intimacy", intimacy);
        MultiTracker.onEvent("b_exposure_opposite_oath_page", params);
    }

    @OnClick({R.id.iv_back, R.id.cp_declaration_comfirm_tv})
    public void onClick(View view) {
        if (ViewUtils.isFastClick()) {
            return;
        }

        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.cp_declaration_comfirm_tv:

                Intent intent1 = new Intent(mContext, DeclarationActivity.class);
                Bundle bundle1 = new Bundle();
                bundle1.putString(Constants.SESSION_ID, female_user_id);
                bundle1.putString("target_user_head",female_user_head);
                bundle1.putString("rise_to", rise_to);
                bundle1.putString("intimacy",intimacy);
                intent1.putExtras(bundle1);
                startActivityForResult(intent1, 102);

                HashMap<String, String> params = new HashMap<>();
                params.put("user_id", AccountUtil.getInstance().getUserId());
                params.put("female_guest_id", female_user_id);
                params.put("event_time", String.valueOf(System.currentTimeMillis()));
                params.put("rise_to",rise_to);
                params.put("intimacy", intimacy);
                MultiTracker.onEvent("b_button_opposite_oath_write_oath", params);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 102 && resultCode == RESULT_OK){
            finish();
        }
    }

    private void checkCpLevelDetail (){
        if(ViewUtils.isFastClick()){
            return;
        }
        try {
            JSONObject object = new JSONObject();
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("female_user_id",female_user_id);
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.checkCpLevelDetail(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    if (isDestroyed() || isFinishing()){
                        return;
                    }
                    closeLoading();
                    CpLevelUpDetail cpLevelUpDetail = GT.fromJson(result, CpLevelUpDetail.class);
                    if(cpLevelUpDetail != null){
                        CpLevelUpDetail.DataBean dataBean = cpLevelUpDetail.getData();

                        if(dataBean != null){
                            CpLevelUpDetail.DataBean.AdvanceDetailBean advanceDetailBean = dataBean.getAdvance_detail();
                            if(advanceDetailBean != null){
                                CpLevelUpDetail.DataBean.AdvanceDetailBean.GiftBean giftBean = advanceDetailBean.getGift();
                                if(giftBean != null){
                                    String gift_pic_url = giftBean.getGift_pic_url();
                                    Glide.with(HalfDeclarationActivity.this).load(gift_pic_url).into(target_declaration_iv);
                                    Glide.with(HalfDeclarationActivity.this).load(female_user_head).into(target_declaration_head);
                                    target_user_name.setText(name);

                                    String swear_words = dataBean.getTarget_swear_words();
                                    target_declaration_desc.setText(swear_words);
                                }
                            }

                            CpLevelUpDetail.DataBean.GroupBean groupBean = dataBean.getGroup();
                            if(groupBean != null){
                                String name = groupBean.getName();
                                level_desc.setText("TA想和你晋级为【" + name + "】");
                            }
                        }
                    }

                    int code = cpLevelUpDetail.getCode();
                    if(code != 0){
                        String msg = cpLevelUpDetail.getMsg();
                        if(StringUtil.isNotEmpty(msg)){
                            Toast.makeText(ContextUtils.getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                        }
                    }

                }

                @Override
                public void OnFailed(int ret, String result) {
                    closeLoading();
                }
            });
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

}
