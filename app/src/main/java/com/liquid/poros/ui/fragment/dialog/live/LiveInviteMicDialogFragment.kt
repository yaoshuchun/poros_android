package com.liquid.poros.ui.fragment.dialog.live

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.TextUtils
import android.util.DisplayMetrics
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.request.RequestOptions
import com.liquid.base.utils.ToastUtil
import com.liquid.poros.R
import com.liquid.poros.analytics.utils.MultiTracker
import com.liquid.poros.base.BaseActivity
import com.liquid.poros.base.BaseDialogFragment
import com.liquid.poros.constant.TrackConstants
import com.liquid.poros.contract.live.LiveInviteMicContract
import com.liquid.poros.contract.live.LiveInviteMicPresenter
import com.liquid.poros.entity.BaseModel
import com.liquid.poros.entity.MicUserInfo
import com.liquid.poros.entity.UserCoinInfo
import com.liquid.poros.ui.activity.audiomatch.CPMatchingGlobal.Companion.getInstance
import com.liquid.poros.ui.activity.live.RoomLiveActivityV2
import com.liquid.poros.ui.dialog.RechargeCoinDialog
import com.liquid.poros.ui.floatball.GiftPackageFloatManager
import com.liquid.poros.utils.AccountUtil
import com.liquid.poros.utils.StringUtil
import com.liquid.poros.utils.ViewUtils
import kotlinx.android.synthetic.main.fragment_live_invite_mic_dialog.*
import java.math.BigDecimal
import java.util.*

/**
 * 直播房间邀请上麦
 */
class LiveInviteMicDialogFragment : BaseDialogFragment(), LiveInviteMicContract.View,
    View.OnClickListener {
    private var mInviteId: String? = null
    private var mSource: String? = null
    private var mRoomId: String? = null
    private var mAvatarUrl: String? = null
    private var mContent: String? = null
    private var mTextTip: String? = null
    private var mNeedCoin: String? = null
    private var mPresenter: LiveInviteMicPresenter? = null
    private var isEnoughCoin = false //金币是否足够
    private var isEnoughCard = false //视频上麦卡是否足够
    var isNewUserMale: Boolean? = false //是否是新用户
    var anchorId: String? = null
    var account: String? = null
    var boyUser: MicUserInfo? = null
    private val rechargeCoinDialog: RechargeCoinDialog by lazy {
        val dialog = RechargeCoinDialog()
        dialog.setRoomId(mRoomId)
        dialog
    }

    fun setUserInfo(
        roomId: String?,
        avatarUrl: String?,
        content: String?,
        text: String?,
        needCoin: String?
    ): LiveInviteMicDialogFragment {
        mRoomId = roomId
        mAvatarUrl = avatarUrl
        mContent = content
        mTextTip = text
        mNeedCoin = needCoin
        return this
    }

    override fun getPageId(): String {
        return TrackConstants.PAGE_LIVE_INVITE_MIC
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return LayoutInflater.from(activity)
            .inflate(R.layout.fragment_live_invite_mic_dialog, container)
    }

    override fun onStart() {
        super.onStart()
        val win = dialog!!.window
        // 一定要设置Background，如果不设置，window属性设置无效
        win.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val dm = DisplayMetrics()
        requireActivity().windowManager.defaultDisplay.getMetrics(dm)
        val params = win.attributes
        params.gravity = Gravity.CENTER
        // 使用ViewGroup.LayoutParams，以便Dialog 宽度充满整个屏幕
        params.width = (dm.widthPixels * 0.8).toInt()
        win.attributes = params
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val options = RequestOptions()
            .transform(CircleCrop())
            .placeholder(R.mipmap.img_default_avatar)
            .error(R.mipmap.img_default_avatar)
        Glide.with(requireActivity())
            .load(mAvatarUrl)
            .apply(options)
            .into(iv_invite_mic_head)
        tv_invite_mic_title.text = mContent
        tv_invite_mic_tip.text = mTextTip
        tv_invite_mic_coin.text = String.format("需%s金币", mNeedCoin)
        tv_cancel.setOnClickListener(this)
        tv_confirm.setOnClickListener(this)
        mPresenter = LiveInviteMicPresenter()
        mPresenter!!.attachView(this)
        updateUserCoin()
    }

    override fun onClick(v: View) {
        if (ViewUtils.isFastClick()) {
            return
        }
        when (v.id) {
            R.id.tv_cancel -> {
                dismissDialog()
                val params = HashMap<String?, String?>()
                params["user_id"] = AccountUtil.getInstance().userId
                params["room_id"] = mRoomId
                MultiTracker.onEvent(TrackConstants.B_REFUSE_ON_MIC_CLICK, params)
            }
            R.id.tv_confirm -> {
                if (GiftPackageFloatManager.getInstance().checkGiftPackage(4)) {
                    dismissDialog()
                    return
                }
                if (getInstance((activity as BaseActivity?)!!).isMatching) {
                    ToastUtil.show(getString(R.string.audio_match_float_ball_tip))
                    return
                }
                if (isEnoughCard || isEnoughCoin || StringUtil.isNotEmpty(AccountUtil.getInstance().goldMasterId)) {
                    if (boyUser != null) {
                        showMessage(getString(R.string.live_room_input_on_mic_tip))
                    } else {
                        if (activity is RoomLiveActivityV2) {
                            (activity as RoomLiveActivityV2).checkPermission(
                                isEnoughCard,
                                mSource,
                                mInviteId
                            )
                        }
                    }
                    trackMicClick(
                        if (isEnoughCard) "user_card" else "user_coin",
                        if (StringUtil.isEquals(
                                account,
                                "all"
                            )
                        ) "one_click_invite" else "directional_invite"
                    )
                    if (listener != null) {
                        listener!!.onJoinMic(isEnoughCard)
                    }
                } else {
                    val fragmentManager = (activity as RoomLiveActivityV2).supportFragmentManager
                    rechargeCoinDialog.show(fragmentManager, RechargeCoinDialog::class.java.name)
                    rechargeCoinDialog.setWindowFrom("accept_invite_to_mic")
                    trackMicClick(
                        "go_recharge",
                        if (StringUtil.isEquals(
                                account,
                                "all"
                            )
                        ) "one_click_invite" else "directional_invite"
                    )
                    if (listener != null) {
                        listener!!.goRecharge()
                    }
                }
                dismissDialog()
            }
        }
    }

    private fun trackMicClick(card: String, from: String) {
        val params = HashMap<String?, String?>()
        params["up_mic_type"] = card
        params["is_new_user"] =
            AccountUtil.getInstance().userInfo.isIs_live_unactive_user?.toString()
        params["room_id"] = mRoomId
        params["anchor_id"] = anchorId
        params["only_user_card"] = "go_recharge"
        params["invite_window_from"] =
            if (StringUtil.isEquals(account, "all")) "one_click_invite" else "directional_invite"
        MultiTracker.onEvent(TrackConstants.B_ACCEPT_ON_MIC_CLICK, params)
    }

    /**
     * 更新金币
     */
    fun updateUserCoin() {
        mPresenter!!.getUserCoin()
    }

    override fun onUserCoinSuccessFetch(userCoinInfo: UserCoinInfo) {
        if (userCoinInfo != null && userCoinInfo.code == 0 && userCoinInfo.data != null && !TextUtils.isEmpty(
                userCoinInfo.data.coin
            )
        ) {
            AccountUtil.getInstance().coin = userCoinInfo.data.coin
            val coin = AccountUtil.getInstance().coin
            val bigDecimal1 = BigDecimal(coin)
            val bigDecimal2 = BigDecimal(mNeedCoin)
            val coinCode = bigDecimal1.compareTo(bigDecimal2)
            isEnoughCoin = coinCode >= 0
            isEnoughCard = if (!TextUtils.isEmpty(userCoinInfo.data.mic_cards.count)) {
                val code = BigDecimal(userCoinInfo.data.mic_cards.count).compareTo(BigDecimal.ZERO)
                code > 0
            } else {
                false
            }
            tv_invite_mic_tip!!.visibility = if (isEnoughCard) View.VISIBLE else View.GONE
        }
    }

    override fun userMicCardFetch(baseModel: BaseModel, onlyUseCard: Boolean) {
        if (listener != null) {
            listener!!.onJoinMic(onlyUseCard)
        }
        dismissDialog()
    }

    override fun showLoading() {}
    override fun showSuccess() {}
    override fun closeLoading() {}
    override fun onError() {}
    override fun showMessage(message: String) {
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
    }

    override fun onSuccess(data: Any) {}
    private var listener: ApplyMicListener? = null
    fun setApplyMicListener(listener: ApplyMicListener?): LiveInviteMicDialogFragment {
        this.listener = listener
        return this
    }

    interface ApplyMicListener {
        //加入上麦
        fun onJoinMic(onlyUseCard: Boolean)

        //充值
        fun goRecharge()
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter!!.detachView()
    }

    fun setUserInfo(
        roomId: String?,
        avatarUrl: String?,
        content: String?,
        text: String?,
        needCoin: String,
        source: String?,
        inviteId: String?
    ): LiveInviteMicDialogFragment {
        mRoomId = roomId
        mAvatarUrl = avatarUrl
        mContent = content
        mTextTip = text
        mNeedCoin = needCoin
        mSource = source
        mInviteId = inviteId
        return this
    }

    companion object {
        @JvmStatic
        fun newInstance(): LiveInviteMicDialogFragment {
            val fragment = LiveInviteMicDialogFragment()
            val bundle = Bundle()
            fragment.arguments = bundle
            return fragment
        }
    }
}