package com.liquid.poros.ui.fragment.dialog.live.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.liquid.library.retrofitx.RetrofitX
import com.liquid.poros.entity.BoostLotteryInfo
import com.liquid.poros.entity.OpenBoostLotteryInfo
import com.liquid.poros.http.ApiUrl
import com.liquid.poros.http.observer.ObjectObserver
import com.liquid.poros.http.utils.postPoros

/**
 * 视频房礼物排行榜
 */
class BoostGiftRewardVM : ViewModel() {
    val boostLotteryInfoLiveData: MutableLiveData<BoostLotteryInfo> by lazy { MutableLiveData<BoostLotteryInfo>() }
    val boostLotteryInfoFailed: MutableLiveData<BoostLotteryInfo> by lazy { MutableLiveData<BoostLotteryInfo>() }
    val openBoostLotterySuccess: MutableLiveData<OpenBoostLotteryInfo> by lazy { MutableLiveData<OpenBoostLotteryInfo>() }
    val openBoostLotteryFailed: MutableLiveData<OpenBoostLotteryInfo> by lazy { MutableLiveData<OpenBoostLotteryInfo>() }


    /**
     * 获取助力抽奖详情
     */
    fun getBoostLotteryInfo() {
        RetrofitX.url(ApiUrl.BOOST_LOTTERY_INFO)
                .postPoros()
                .subscribe(object : ObjectObserver<BoostLotteryInfo>() {
                    override fun success(result: BoostLotteryInfo) {
                        boostLotteryInfoLiveData.postValue(result)
                    }

                    override fun failed(result: BoostLotteryInfo) {
                        boostLotteryInfoFailed.postValue(result)
                    }
                })
    }

    /**
     * 助力抽奖
     */
    fun openBoostLottery(roomId: String, boostLotteryType: String) {
        RetrofitX.url(ApiUrl.OPEN_BOOST_LOTTERY)
                .param("room_id", roomId)
                .param("boost_lottery_type", boostLotteryType)
                .postPoros()
                .subscribe(object : ObjectObserver<OpenBoostLotteryInfo>() {
                    override fun success(result: OpenBoostLotteryInfo) {
                        openBoostLotterySuccess.postValue(result)
                    }

                    override fun failed(result: OpenBoostLotteryInfo) {
                        openBoostLotteryFailed.postValue(result)
                    }
                })
    }
}