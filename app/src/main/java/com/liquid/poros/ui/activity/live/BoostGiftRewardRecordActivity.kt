package com.liquid.poros.ui.activity.live

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentPagerAdapter
import com.liquid.poros.R
import com.liquid.poros.base.BaseActivity
import com.liquid.poros.constant.TrackConstants
import com.liquid.poros.ui.fragment.live.BoostLotteryRecordFragment
import com.liquid.poros.utils.DensityUtil
import kotlinx.android.synthetic.main.activity_boost_gift_reward_record.*
import net.lucode.hackware.magicindicator.ViewPagerHelper
import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView
import net.lucode.hackware.magicindicator.buildins.commonnavigator.indicators.LinePagerIndicator
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.ClipPagerTitleView
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.badge.BadgePagerTitleView
import java.util.*

/**
 * 助力中奖记录界面
 */
@Suppress("DEPRECATION")
class BoostGiftRewardRecordActivity : BaseActivity() {

    private val viewPagerTitles = arrayListOf("初级宝箱", "高级宝箱")
    private val fragments: ArrayList<Fragment> = ArrayList()
    //初级宝箱
    private var TYPE_BOOST_COMMON: String? = "boost_cm"
    //高级宝箱
    private var TYPE_BOOST_ADVANCE: String? = "boost_ad"

    override fun parseBundleExtras(extras: Bundle?) {
    }

    override fun getPageId(): String {
        return TrackConstants.PAGE_BOOST_GIFT_REWARD_RECORD
    }

    override fun getContentViewLayoutID(): Int {
        return R.layout.activity_boost_gift_reward_record
    }

    override fun needSetTransparent(): Boolean {
        return false
    }

    override fun initViewsAndEvents(savedInstanceState: Bundle?) {
        val commonRecordFragment = BoostLotteryRecordFragment.newInstance(TYPE_BOOST_COMMON!!)
        commonRecordFragment?.let { fragments.add(it) }
        val advanceRecordFragment = BoostLotteryRecordFragment.newInstance(TYPE_BOOST_ADVANCE!!)
        advanceRecordFragment?.let { fragments.add(it) }
        vp_boost_record.adapter = object : FragmentPagerAdapter(supportFragmentManager) {
            override fun getCount(): Int {
                return fragments.size
            }

            override fun getItem(position: Int): Fragment {
                return fragments[position]
            }
        }
        initMagicIndicator()
    }

    private fun initMagicIndicator() {
        val commonNavigator = CommonNavigator(this)
        commonNavigator.isAdjustMode = true
        commonNavigator.adapter = object : CommonNavigatorAdapter() {
            override fun getCount(): Int {
                return viewPagerTitles?.size ?: 0
            }

            override fun getTitleView(context: Context, index: Int): IPagerTitleView {
                val badgePagerTitleView = BadgePagerTitleView(context)
                val clipPagerTitleView = ClipPagerTitleView(context)
                clipPagerTitleView.text = viewPagerTitles[index]
                clipPagerTitleView.textColor = Color.parseColor("#A4A9B3")
                clipPagerTitleView.clipColor = Color.parseColor("#10D8C8")
                clipPagerTitleView.textSize = DensityUtil.dp2px(12F).toFloat()
                clipPagerTitleView.setOnClickListener {
                    vp_boost_record.currentItem = index
                    Log.e("xxx", "index == ${index}")
                }
                badgePagerTitleView.innerPagerTitleView = clipPagerTitleView
                return badgePagerTitleView
            }

            override fun getIndicator(context: Context): IPagerIndicator {
                val indicator = LinePagerIndicator(context)
                indicator.lineHeight = DensityUtil.dp2px(28F).toFloat()
                indicator.roundRadius = DensityUtil.dp2px(90F).toFloat()
                indicator.setColors(Color.parseColor("#ffffff"))
                return indicator
            }
        }
        magic_indicator_boost_record.navigator = commonNavigator
        ViewPagerHelper.bind(magic_indicator_boost_record, vp_boost_record)
    }

}