package com.liquid.poros.ui.fragment.dialog.account;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import androidx.annotation.Nullable;

import com.faceunity.nama.NamaRenderer;
import com.faceunity.nama.fromapp.entity.BeautyParameterModel;
import com.faceunity.nama.ui.control.BeautyUnionView;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.liquid.poros.R;


public class UpdateMakeUpDialogFragment extends BottomSheetDialogFragment {


    private NamaRenderer mRender;

    public UpdateMakeUpDialogFragment setFuRender(NamaRenderer mRender) {
        this.mRender = mRender;
        return this;
    }

    public static UpdateMakeUpDialogFragment newInstance() {
        final UpdateMakeUpDialogFragment fragment = new UpdateMakeUpDialogFragment();
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_update_make_up_dialog, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        //setCancelable(false);
        Window window = getDialog().getWindow();
        WindowManager.LayoutParams layoutParams = window.getAttributes();
        layoutParams.dimAmount = 0.0f;
        window.setAttributes(layoutParams);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        BeautyUnionView fuView = view.findViewById(R.id.fu_union_view);
        fuView.setFURenderer(mRender);
        view.findViewById(com.faceunity.nama.R.id.tv_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissAllowingStateLoss();
            }
        });
        view.findViewById(com.faceunity.nama.R.id.tv_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BeautyParameterModel.saveParams();
                dismiss();
            }
        });
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
    }
}
