package com.liquid.poros.ui.fragment.dialog.pta;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.liquid.poros.R;
import com.liquid.poros.base.BaseDialogFragment;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.utils.ViewUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

/**
 * 个人默认形象弹窗
 */
public class UserDressDialogFragment extends BaseDialogFragment implements View.OnClickListener {
    private ImageView surface_view;
    private TextView tv_user_dress_sure;
    private String mCartoonImg;
    public UserDressDialogFragment setCartoonImg(String cartoonImg) {
        this.mCartoonImg = cartoonImg;
        return this;
    }

    public static UserDressDialogFragment newInstance() {
        UserDressDialogFragment fragment = new UserDressDialogFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    protected String getPageId() {
        return TrackConstants.PAGE_USER_DRESS_DIALOG;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onStart() {
        super.onStart();
        Window win = getDialog().getWindow();
        // 一定要设置Background，如果不设置，window属性设置无效
        win.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams params = win.getAttributes();
        params.gravity = Gravity.CENTER;
        setCancelable(false);
        // 使用ViewGroup.LayoutParams，以便Dialog 宽度充满整个屏幕
        params.width = (int) (dm.widthPixels * 0.9);
        win.setAttributes(params);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_user_dress_dialog, null);
        tv_user_dress_sure = view.findViewById(R.id.tv_user_dress_sure);
        surface_view = view.findViewById(R.id.surface_view);
        Glide.with(getActivity())
                .load(mCartoonImg)
                .into(surface_view);
        tv_user_dress_sure.setOnClickListener(this);
        builder.setView(view);
        return builder.create();
    }

    @Override
    public void onClick(View v) {
        if (ViewUtils.isFastClick()) {
            return;
        }
        switch (v.getId()) {
            case R.id.tv_user_dress_sure:
                dismissDialog();
                break;
        }
    }
}
