package com.liquid.poros.ui.activity.live.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.liquid.base.utils.LogOut
import com.liquid.library.retrofitx.RetrofitX
import com.liquid.poros.analytics.utils.MultiTracker
import com.liquid.poros.entity.*
import com.liquid.poros.http.ApiUrl
import com.liquid.poros.http.observer.ObjectObserver
import com.liquid.poros.http.utils.postPoros
import com.liquid.poros.utils.AccountUtil
import com.liquid.poros.utils.StringUtil
import java.util.*


/**
 * 视频房ViewModel
 */
class RoomLiveViewModel : ViewModel() {

    val cpRoomLiveData: MutableLiveData<CoupleRoom> by lazy { MutableLiveData() } //视频房信息
    val micListLiveData: MutableLiveData<MicListInfo> by lazy { MutableLiveData() } //上麦申请列表
    val micPrivateListLiveData: MutableLiveData<MicPrivateListInfo> by lazy { MutableLiveData() } //专属房申请列表
    val sendGiftSuccessLiveData: MutableLiveData<GiftInfo> by lazy { MutableLiveData() } //送礼成功
    val sendGiftFailedLiveData: MutableLiveData<GiftInfo> by lazy { MutableLiveData() } //送礼失败
    val cpSuccessLiveData: MutableLiveData<BaseBean> by lazy { MutableLiveData() } //申请组CP成功
    val cpFailedLiveData: MutableLiveData<BaseBean> by lazy { MutableLiveData() } //申请组CP失败
    val quitMicSuccessLiveData: MutableLiveData<BaseBean> by lazy { MutableLiveData() } //下麦成功
    val quitMicFailedLiveData: MutableLiveData<BaseBean> by lazy { MutableLiveData() } //下麦失败
    val joinMicSuccessLiveData: MutableLiveData<RoomOnMicV2> by lazy { MutableLiveData() } //上麦成功
    val joinMicFailedLiveData: MutableLiveData<RoomOnMicV2> by lazy { MutableLiveData() } //上麦失败
    val cpTeamSuccessLiveData: MutableLiveData<CPTeamResult> by lazy { MutableLiveData() } //CP组队成功
    val cpTeamFailedLiveData: MutableLiveData<CPTeamResult> by lazy { MutableLiveData() } //CP组队失败
    val joinPrivateMicSuccessLiveData: MutableLiveData<PrivateRoomMicInfo> by lazy { MutableLiveData() } //专属房上麦成功
    val joinPrivateMicFailedLiveData: MutableLiveData<PrivateRoomMicInfo> by lazy { MutableLiveData() } //专属房上麦失败
    val enterPrivateSuccessLiveData: MutableLiveData<BaseBean> by lazy { MutableLiveData() } //申请进入专属房成功
    val enterPrivateFailedLiveData: MutableLiveData<BaseBean> by lazy { MutableLiveData() } //申请进入专属房失败
    val privateJHeartBeatSuccessLiveData: MutableLiveData<PrivateRoomHeartBeatInfo> by lazy { MutableLiveData() } //专属房心跳成功
    val privateJHeartBeatFailedLiveData: MutableLiveData<PrivateRoomHeartBeatInfo> by lazy { MutableLiveData() } //专属房心跳失败
    val switchCameraSuccessLiveData: MutableLiveData<SwitchCameraResult> by lazy { MutableLiveData() } //切换摄像头成功
    val switchCameraFailedLiveData: MutableLiveData<SwitchCameraResult> by lazy { MutableLiveData() } //切换摄像头失败
    val joinCpGroupSuccessLiveData: MutableLiveData<BaseBean> by lazy { MutableLiveData() }//加入Cp团成功
    val joinCpGroupFailedLiveData: MutableLiveData<BaseBean> by lazy { MutableLiveData() }//加入Cp团失败

    var count = 0

    /**
     * 获取房间信息
     */
    fun getRoomInfo(roomId: String, invite_id: String? = null, source: String? = null,is_new: Boolean? = false) {
        RetrofitX.url(ApiUrl.LIVE_ROOM_INFO)
                .param("room_id", roomId)
                .param("is_new", is_new)
                .also {
                    if (StringUtil.isNotEmpty(invite_id)) {
                        it.param("invite_id", invite_id)
                        it.param("source", source)
                    }
                }.postPoros().subscribe(object : ObjectObserver<CoupleRoom>() {
                    override fun success(result: CoupleRoom) {
                        LogOut.debug("RoomLiveActivity", "当前请求房间信息接口:${++count}次")
                        cpRoomLiveData.postValue(result)
                        if (StringUtil.isNotEmpty(invite_id)) {
                            val msgParams2 = HashMap<String?, String?>()
                            msgParams2["user_id"] = AccountUtil.getInstance().userId
                            msgParams2["room_id"] = roomId
                            msgParams2["invite_id"] = invite_id + ""
                            MultiTracker.onEvent("b_video_room_invite_enter_success", msgParams2)
                        }
                    }

                    override fun failed(result: CoupleRoom) {
                    }

                })
    }

    /**
     * 获取连麦列表
     */
    fun getMicList(roomId: String, page: Int) {
        RetrofitX.url(ApiUrl.LIVE_ROOM_WAITING_LIST)
                .param("room_id", roomId)
                .param("page_idx", page)
                .postPoros()
                .subscribe(object : ObjectObserver<MicListInfo>() {
                    override fun success(result: MicListInfo) {
                        micListLiveData.postValue(result)
                    }

                    override fun failed(result: MicListInfo) {
                    }
                })
    }

    /**
     * 加入CP团请求
     */
    fun postJoinCpGroup(roomId: String) {
        RetrofitX.url(ApiUrl.LIVE_ROOM_JOIN_CP_REQUEST)
                .param("room_id", roomId)
                .postPoros()
                .subscribe(object : ObjectObserver<BaseBean>() {
                    override fun success(result: BaseBean) {
                        joinCpGroupSuccessLiveData.postValue(result)
                    }

                    override fun failed(result: BaseBean) {
                        joinCpGroupFailedLiveData.postValue(result)
                    }
                })
    }

    /**
     * 获取专属房申请列表
     */
    fun getPrivateMicList(roomId: String?, page: Int) {
        RetrofitX.url(ApiUrl.LIVE_ROOM_PRIVATE_WAITING_LIST)
                .param("room_id", roomId)
                .param("page_idx", page)
                .postPoros()
                .subscribe(object : ObjectObserver<MicPrivateListInfo>() {
                    override fun success(result: MicPrivateListInfo) {
                        micPrivateListLiveData.postValue(result)
                    }

                    override fun failed(result: MicPrivateListInfo) {
                    }

                })
    }

    /**
     * 发送礼物
     * @param roomId         //房间ID
     * @param target_user_id //女嘉宾ID
     * @param gift_id        //礼物ID
     * @param giftName       //礼物名称
     * @param num            //礼物个数
     * @param to_anchor      //是否对主持人送礼物
     * @param pos            //pk房类型传递,区分多女嘉宾麦位
     * @param isGiftBoost    //是否是助力礼物
     */
    fun sendGift(roomId: String?, target_user_id: String?, gift_id: Int, giftName: String, num: Int, to_anchor: Boolean, pos: Int = -1, isGiftBoost: Boolean) {
        RetrofitX.url(ApiUrl.SEND_GIFT)
                .param("to_anchor", to_anchor)
                .param("room_id", roomId)
                .param("token", AccountUtil.getInstance().account_token)
                .param("gift_id", gift_id)
                .param("target_user_id", target_user_id)
                .param("num", num)
                .param("is_gift_boost", isGiftBoost)
                .also {
                    if (pos != -1) {
                        it.param("pos", pos)
                    }
                }.postPoros().subscribe(object : ObjectObserver<GiftInfo>() {
                    override fun success(result: GiftInfo) {
                        sendGiftSuccessLiveData.postValue(result.also {
                            it.gift_id = gift_id
                            it.giftName = giftName
                            it.giftNum = num
                            it.gift_boost_success = isGiftBoost
                        })
                    }

                    override fun failed(result: GiftInfo) {
                        sendGiftFailedLiveData.postValue(result.also {
                            it.gift_id = gift_id
                            it.giftName = giftName
                            it.gift_boost_success = isGiftBoost
                        })
                    }

                })
    }

    /**
     * 申请组CP
     */
    fun requestCP(roomId: String?) {
        RetrofitX.url(ApiUrl.LIVE_ROOM_REQUEST_CP)
                .param("room_id", roomId)
                .postPoros()
                .subscribe(object : ObjectObserver<BaseBean>() {
                    override fun success(result: BaseBean) {
                        cpSuccessLiveData.postValue(result)
                    }

                    override fun failed(result: BaseBean) {
                        cpFailedLiveData.postValue(result)
                    }
                })
    }

    /**
     * 设置视频房ChannelId
     */
    fun updateRTMPTask(roomId: String?) {
        RetrofitX.url(ApiUrl.LIVE_ROOM_UPDATE_RTMP_TASK)
                .param("room_id", roomId)
                .postPoros()
                .subscribe()
    }

    /**
     * 下麦
     */
    fun quitMic(roomId: String?, invite_id: String? = null) {
        RetrofitX.url(ApiUrl.LIVE_ROOM_QUIT_ON_MIC)
                .param("room_id", roomId)
                .postPoros()
                .subscribe(object : ObjectObserver<BaseBean>() {
                    override fun success(result: BaseBean) {

                    }

                    override fun failed(result: BaseBean) {
//                        quitMicFailedLiveData.postValue(result)
                    }

                })
    }

    /**
     * 上麦
     */
    fun joinMic(isFreeMic: Boolean, roomId: String?, mcPrice: Int, onlyUseCard: Boolean, source: String?, invite_id: String?) {
        RetrofitX.url(ApiUrl.LIVE_ROOM_JOIN_MIC)
                .param("room_id", roomId)
                .param("source", source)
                .param("invite_id", invite_id)
                .also {
                    if (onlyUseCard) {
                        it.param("only_use_card", true)
                    } else {
                        if (!isFreeMic) {
                            it.param("mic_card_price", mcPrice)
                        }
                    }
                }.postPoros().subscribe(object : ObjectObserver<RoomOnMicV2>() {
                    override fun success(result: RoomOnMicV2) {
                        joinMicSuccessLiveData.postValue(result)
                    }

                    override fun failed(result: RoomOnMicV2) {
                        joinMicFailedLiveData.postValue(result)
                    }
                })
    }

    /**
     * 同意CP组队
     */
    fun agreeCoupleTeam(roomId: String?, targetId: String?, inviteId: String?) {
        RetrofitX.url(ApiUrl.LIVE_ROOM_AGREE_CP)
                .param("room_id", roomId)
                .param("invite_id", inviteId)
                .param("target_user_id", targetId)
                .postPoros().subscribe(object : ObjectObserver<CPTeamResult>() {
                    override fun success(result: CPTeamResult) {
                        cpTeamSuccessLiveData.postValue(result.also {
                            it.inviteId = inviteId
                            it.targetId = targetId
                        })
                    }

                    override fun failed(result: CPTeamResult) {
                        cpTeamFailedLiveData.postValue(result)
                    }

                })
    }

    /**
     * 拒绝组CP
     */
    fun refuseCoupleTeam(roomId: String?, targetId: String?, inviteId: String?) {
        RetrofitX.url(ApiUrl.LIVE_ROOM_REFUSE_CP)
                .param("room_id", roomId)
                .param("invite_id", inviteId)
                .param("target_user_id", targetId)
                .postPoros().subscribe()
    }

    /**
     * 足迹触发
     */
    fun fetchDispatchFemale(dispatchType: Int) {
        RetrofitX.url(ApiUrl.DISPATCH_BY_TYPE)
                .param("dispatch_type", dispatchType)
                .postPoros().subscribe()
    }

    /**
     * 专属房上麦
     */
    fun fetchPrivateRoomOnMic(roomId: String?) {
        RetrofitX.url(ApiUrl.LIVE_ROOM_PRIVATE_JOIN_MIC)
                .param("room_id", roomId)
                .postPoros().subscribe(object : ObjectObserver<PrivateRoomMicInfo>() {
                    override fun success(result: PrivateRoomMicInfo) {
                        joinPrivateMicSuccessLiveData.postValue(result)
                    }

                    override fun failed(result: PrivateRoomMicInfo) {
                        joinPrivateMicFailedLiveData.postValue(result)
                    }

                })
    }

    /**
     * 申请进入专属房
     */
    fun fetchApplyPrivateRoom(roomId: String?) {
        RetrofitX.url(ApiUrl.LIVE_ROOM_PRIVATE_REQUEST)
                .param("room_id", roomId)
                .postPoros()
                .subscribe(object : ObjectObserver<BaseBean>() {
                    override fun success(result: BaseBean) {
                        enterPrivateSuccessLiveData.postValue(result)
                    }

                    override fun failed(result: BaseBean) {
                        enterPrivateFailedLiveData.postValue(result)
                    }
                })
    }

    /**
     * 专属房心跳
     */
    fun fetchPrivateRoomHeartBeat(roomId: String?) {
        RetrofitX.url(ApiUrl.LIVE_ROOM_PRIVATE_HEART_BEAT).param("room_id", roomId)
                .postPoros().subscribe(object : ObjectObserver<PrivateRoomHeartBeatInfo>() {
                    override fun success(result: PrivateRoomHeartBeatInfo) {
                        privateJHeartBeatSuccessLiveData.postValue(result)
                    }

                    override fun failed(result: PrivateRoomHeartBeatInfo) {
                        privateJHeartBeatFailedLiveData.postValue(result)
                    }

                })
    }

    /**
     * 用户行为上报
     */
    @Deprecated("废弃")
    fun uploadBehavior(recordType: String?, roomId: String?, anchor: String?, targetId: String?) {
        RetrofitX.url(ApiUrl.UPLOAD_BEHAVIOR)
                .param("record_type", recordType)
                .param("room_id", roomId)
                .param("anchor", anchor)
                .param("target_user_id", targetId)
                .postPoros().subscribe()
    }

    @Deprecated("废弃")
    fun switchCamera(roomId: String?, camera_close: Boolean) {
        RetrofitX.url(ApiUrl.LIVE_ROOM_SWITCH_CAMERA)
                .param("token", AccountUtil.getInstance().account_token)
                .param("room_id", roomId)
                .param("camera_close", camera_close)
                .postPoros().subscribe(object : ObjectObserver<SwitchCameraResult>() {
                    override fun success(result: SwitchCameraResult) {
                        result.camera_close = camera_close
                        switchCameraSuccessLiveData.postValue(result)
                    }

                    override fun failed(result: SwitchCameraResult) {
                        switchCameraFailedLiveData.postValue(result)
                    }

                })
    }

    /**
     * 获取用户等级
     */
    fun getUserLevel() {
        RetrofitX.url(ApiUrl.USER_LEVEL)
                .param("user_id", AccountUtil.getInstance().userId)
                .postPoros().subscribe(object : ObjectObserver<UserLevelResult>() {
                    override fun success(result: UserLevelResult) {
                        AccountUtil.getInstance().userInfo.user_level = result.user_level
                    }

                    override fun failed(result: UserLevelResult) {
                    }

                })
    }

    /**
     * 踢下麦操作，用于异常下麦后踢自己下麦操作
     */
    fun kickMic(roomId: String?, user_id: String?) {
        RetrofitX.url(ApiUrl.ROOM_KICKOUT_MIC)
                .param("room_id", roomId)
                .param("user_id", user_id)
                .postPoros()
                .subscribe()
    }

    /**
     * CP组队返回结果
     */
    class CPTeamResult : BaseBean() {
        var inviteId: String? = null
        var targetId: String? = null
    }

    /**
     * 切换摄像头返回结果
     */
    class SwitchCameraResult : BaseBean() {
        var camera_close: Boolean = false
    }

    /**
     * 用户等级返回结果
     */
    class UserLevelResult : BaseBean() {
        var user_level: Int = 0
    }
}