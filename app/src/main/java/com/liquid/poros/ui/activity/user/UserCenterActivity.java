package com.liquid.poros.ui.activity.user;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.liquid.base.adapter.CommonAdapter;
import com.liquid.base.adapter.CommonViewHolder;
import com.liquid.base.event.EventCenter;
import com.liquid.poros.R;
import com.liquid.poros.adapter.GuardGiftAdapter;
import com.liquid.poros.adapter.PersonNewsAdapter;
import com.liquid.poros.adapter.UserPhotoAdapter;
import com.liquid.poros.analytics.utils.ExposureUtils;
import com.liquid.poros.analytics.utils.MultiTracker;
import com.liquid.poros.base.BaseActivity;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.constant.DataOptimize;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.contract.user.UserCenterContract;
import com.liquid.poros.contract.user.UserCenterPresenter;
import com.liquid.poros.entity.BaseModel;
import com.liquid.poros.entity.FemaleGuestCommentBean;
import com.liquid.poros.entity.NormalGame;
import com.liquid.poros.entity.RoomDetailsInfo;
import com.liquid.poros.entity.SettingEditGameBean;
import com.liquid.poros.entity.SingleCommentBean;
import com.liquid.poros.entity.UserCenterInfo;
import com.liquid.poros.entity.UserPhotoInfo;
import com.liquid.poros.helper.JinquanResourceHelper;
import com.liquid.poros.ui.RouterUtils;
import com.liquid.poros.ui.activity.AngelGuardNoticeActivity;
import com.liquid.poros.ui.activity.live.RoomLiveActivityV2;
import com.liquid.poros.ui.fragment.dialog.user.PictureSelectorDialogFragment;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.poros.utils.ScreenUtil;
import com.liquid.poros.utils.StringUtil;
import com.liquid.poros.utils.ViewUtils;
import com.liquid.poros.utils.glide.GlideEngine;
import com.liquid.poros.widgets.FlowLayout;
import com.liquid.poros.widgets.HorizontalItemDecoration;
import com.liquid.poros.widgets.ObservableScrollView;
import com.liquid.poros.widgets.WrapContentGridLayoutManager;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.style.PictureWindowAnimationStyle;
import com.luck.picture.lib.tools.ToastUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.OnClick;

import static com.liquid.poros.constant.Constants.GIFT_RANK_PATH;

public class UserCenterActivity extends BaseActivity implements UserCenterContract.View, ObservableScrollView.ScrollViewListener, UserPhotoAdapter.OnListener {
    @BindView(R.id.rl_content)
    RelativeLayout rl_content;
    @BindView(R.id.ll_title_layout)
    LinearLayout ll_title_layout;
    @BindView(R.id.rl_cover)
    RelativeLayout rl_cover;
    @BindView(R.id.iv_user_header_bg)
    ImageView iv_user_header_bg;
    @BindView(R.id.osv_scroll_view)
    ObservableScrollView osv_scroll_view;
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.tv_user_name)
    TextView tv_user_name;
    @BindView(R.id.tv_user_id)
    TextView tv_user_id;
    @BindView(R.id.tv_user_age_level)
    TextView tv_user_age_level;
    @BindView(R.id.tv_charm_value_level)
    TextView tv_charm_value_level;
    @BindView(R.id.ll_audio)
    LinearLayout ll_audio;
    @BindView(R.id.tv_user_cp_content)
    TextView tv_user_cp_content;
    @BindView(R.id.iv_audio_play_status)
    ImageView iv_audio_play_status;
    @BindView(R.id.tv_audio_timer)
    TextView tv_audio_timer;
    @BindView(R.id.tv_user_tag)
    TextView tv_user_tag;
    @BindView(R.id.fl_user_tag)
    FlowLayout fl_user_tag;
    @BindView(R.id.rv_pics)
    RecyclerView rv_pics;
    @BindView(R.id.rl_chat)
    RelativeLayout rl_chat;
    @BindView(R.id.tv_chat)
    TextView tv_chat;
    @BindView(R.id.iv_free_call)
    ImageView iv_free_call;
    @BindView(R.id.rl_edit_user)
    RelativeLayout rl_edit_user;
    @BindView(R.id.tv_user_tag_seat)
    TextView tv_user_tag_seat;
    @BindView(R.id.tv_user_pics_seat)
    TextView tv_user_pics_seat;
    @BindView(R.id.tv_game_want)
    TextView tv_game_want;
    @BindView(R.id.tv_game_want_empty)
    TextView tv_game_want_empty;
    @BindView(R.id.ll_game_want)
    LinearLayout ll_game_want;
    @BindView(R.id.iv_heping)
    ImageView iv_heping;
    @BindView(R.id.iv_wangzhe)
    ImageView iv_wangzhe;
    @BindView(R.id.rl_game_other)
    RelativeLayout rl_game_other;
    @BindView(R.id.rv_other_game)
    RecyclerView rv_other_game;
    @BindView(R.id.rl_game_heping)
    RelativeLayout rl_game_heping;
    @BindView(R.id.tv_game_wangzhe_platform)
    TextView tv_game_wangzhe_platform;
    @BindView(R.id.tv_game_wangzhe_area)
    TextView tv_game_wangzhe_area;
    @BindView(R.id.tv_game_wangzhe_rank)
    TextView tv_game_wangzhe_rank;
    @BindView(R.id.rl_game_wangzhe)
    RelativeLayout rl_game_wangzhe;
    @BindView(R.id.tv_game_heping_platform)
    TextView tv_game_heping_platform;
    @BindView(R.id.tv_game_heping_area)
    TextView tv_game_heping_area;
    @BindView(R.id.tv_game_heping_rank)
    TextView tv_game_heping_rank;
    @BindView(R.id.ll_gift_empty)
    LinearLayout llGiftEmpty;
    @BindView(R.id.rl_has_gift)
    RelativeLayout rlHasGift;
    @BindView(R.id.guard_angel_avatar)
    ImageView guardAngelAvatar;
    @BindView(R.id.guard_angel_name)
    TextView guardAngelName;
    @BindView(R.id.guard_gift_list)
    RecyclerView guardGiftList;
    @BindView(R.id.is_guard)
    ImageView isGuard;
    @BindView(R.id.my_gift_notice)
    TextView myGiftNotice;
    //展开&收起
    @BindView(R.id.ll_center_pic_tip)
    LinearLayout ll_center_pic_tip;
    @BindView(R.id.tv_center_pic_tip)
    TextView tv_center_pic_tip;
    @BindView(R.id.iv_center_pic_tip)
    ImageView iv_center_pic_tip;
    @BindView(R.id.tv_feed)
    TextView tv_feed;
    @BindView(R.id.rv_person_new)
    RecyclerView rv_person_new;
    @BindView(R.id.ll_feed_empty)
    LinearLayout ll_feed_empty;
    private PersonNewsAdapter adapter;
    private UserCenterPresenter mPresenter;
    private UserCenterInfo.Data.UserInfo mUserInfoBean;
    private String mUserId;
    private UserPhotoAdapter mAdapter;
    private UserCenterInfo.Data.UserInfo.RankInfoBean rankInfo;
    private List<UserPhotoInfo.Data.Photos> mImageList = new ArrayList<>();
    public static final int SAVE_INFO_SUCCESS = 104;//用户信息保存
    private int mPage = 0;
    private List<String> mTagStrList = new ArrayList<>();
    private MediaPlayer mMediaPlayer;
    private Timer mTimer2;
    private TimerTask mTask2;
    private int mAudioDuration;
    private int mAudioTimer;
    private int clientId;
    private boolean isPause = false;//是否暂停
    private boolean isUserBg = false; //是否是背景封面  false 否
    private List<String> pathLists = new ArrayList<>();//选择上传图片资源
    private int imageHeight = 800;
    private List<UserPhotoInfo.Data.Photos> mUserPhotoList = new ArrayList<>();
    private CommonAdapter<NormalGame> mOtherGameAdapter;
    private SettingEditGameBean mStaticGameBean;
    private ArrayList<SingleCommentBean> commentBeans = new ArrayList<>();
    private ExposureUtils<PersonNewsAdapter, SingleCommentBean> personNewsExposureUtils;
    private String giftRankSource;
    private RoundedCorners roundedCorners;
    private RequestOptions options;
    private boolean isWangZheGame = false;
    private boolean isHePingGame = false;
    private boolean isOtherGame = false;
    private boolean isPicOpen = false;
    private boolean isUpdatePic = false;
//    private PTALauncher ptaLauncher;
    private PictureWindowAnimationStyle pictureWindowAnimationStyle =  new PictureWindowAnimationStyle();
    @Override
    protected void parseBundleExtras(Bundle extras) {
        mUserId = extras.getString(Constants.USER_ID);
        giftRankSource = extras.getString(GIFT_RANK_PATH);
    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.activity_user_center;
    }

    @Override
    protected void initViewsAndEvents(Bundle savedInstanceState) {
        roundedCorners = new RoundedCorners(100);
        options = new RequestOptions()
                .placeholder(R.mipmap.icon_default)
                .error(R.mipmap.icon_default)
                .transform(new CenterCrop(), roundedCorners);
        rv_pics.setLayoutManager(new WrapContentGridLayoutManager(this, 4));
        rv_pics.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                outRect.top = ScreenUtil.dip2px(8);
            }
        });
        mAdapter = new UserPhotoAdapter(UserCenterActivity.this, mImageList, mUserId);
        mOtherGameAdapter = new CommonAdapter<NormalGame>(UserCenterActivity.this, R.layout.item_other_game) {
            @Override
            public void convert(CommonViewHolder holder, NormalGame normalGame, int pos) {
                ImageView iv_image = holder.getView(R.id.iv_image);
                RequestOptions options = new RequestOptions()
                        .placeholder(R.mipmap.icon_default)
                        .error(R.mipmap.icon_default)
                        .transform(new CenterCrop(), new RoundedCorners(ScreenUtil.dip2px(6)));
                Glide.with(UserCenterActivity.this)
                        .load(normalGame.getIcon())
                        .override(ScreenUtil.dip2px(56), ScreenUtil.dip2px(56))
                        .apply(options)
                        .into(iv_image);
            }
        };
        adapter = new PersonNewsAdapter(this);
        adapter.setSessionId(mUserId);
        adapter.setFeedDetailsType(Constants.SOURCE_FEED_DETAIL);
//        adapter.initData(commentBeans);
        adapter.setListener(new PersonNewsAdapter.OnPersonNewsClickListener() {
            @Override
            public void like(String feedId, boolean isLike) {
                mPresenter.feedLike(feedId, isLike ? 1 : 0, Constants.SOURCE_FEED_LIST);
            }

            @Override
            public void voiceAction(String feedId) {
                mPresenter.feedActionReport(feedId, Constants.FEED_ACTION_VOICE, Constants.SOURCE_FEED_LIST);
            }

            @Override
            public void pictureAction(String feedId) {
                mPresenter.feedActionReport(feedId, Constants.FEED_ACTION_PICTURE, Constants.SOURCE_FEED_LIST);
            }
        });
        rv_person_new.setLayoutManager(new LinearLayoutManager(this));
        rv_person_new.setAdapter(adapter);
        rv_other_game.setLayoutManager(new LinearLayoutManager(mContext, RecyclerView.HORIZONTAL, false));
        rv_other_game.setAdapter(mOtherGameAdapter);
        rv_other_game.addItemDecoration(new HorizontalItemDecoration(ScreenUtil.dip2px(12)));
        osv_scroll_view.setNestedScrollingEnabled(false);
        rv_pics.setNestedScrollingEnabled(false);
        rv_pics.setHasFixedSize(true);
        rv_pics.setAdapter(mAdapter);
        mAdapter.setOnItemListener(UserCenterActivity.this);
        osv_scroll_view.setScrollViewListener(this);
        mPresenter = new UserCenterPresenter();
        mPresenter.attachView(this);
        mMediaPlayer = new MediaPlayer();
        tv_center_pic_tip.setText(getString(R.string.session_guard_game_open));
        JinquanResourceHelper.getInstance(this).setImageResourceByAttr(iv_center_pic_tip, R.attr.custom_bg_user_photo_open);
        mPresenter.getUserPhotos(mPage, mUserId);
//        ptaLauncher = PTALauncher.Companion.init(mGLSurfaceView, this);
//        AvatarPTA male = ptaLauncher.initAvatar(true);
//        male.setPosition(new double[]{0.0, -80.0, -800.0});
//        ptaLauncher.drawAvatar(male, null);
        mPresenter.getCommentList(mUserId, null);
    }

    @Override
    protected String getPageId() {
        return TrackConstants.PAGE_USER_CENTER;
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.getUserInfo(mUserId);
        if (personNewsExposureUtils == null) {
            personNewsExposureUtils = new ExposureUtils(adapter, commentBeans);
            personNewsExposureUtils.setmRecyclerView(rv_person_new);
        }
        personNewsExposureUtils.setFeedFrom(Constants.TYPE_FEED_PERSONAL);
        personNewsExposureUtils.startFeedExposureStatistics();
//        ptaLauncher.resume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (personNewsExposureUtils != null) {
            personNewsExposureUtils.calExplosure();
            personNewsExposureUtils.stopFeedExposureStatistics();
        }
//        ptaLauncher.pause();
    }

    @Override
    public void onUserInfoFetch(UserCenterInfo userCenterInfo) {
        if (userCenterInfo == null || userCenterInfo.getData() == null || userCenterInfo.getData().getUser_info() == null) {
            return;
        }
        mUserInfoBean = userCenterInfo.getData().getUser_info();
        clientId = mUserInfoBean.getClient();
        if (TextUtils.isEmpty(mUserInfoBean.getCover_img()) || mUserInfoBean.getCover_img().equals("null")) {
            RequestOptions roomOptions = new RequestOptions()
                    .placeholder(R.mipmap.icon_default)
                    .error(R.mipmap.icon_default);
            Glide.with(UserCenterActivity.this)
                    .load(mUserInfoBean.getAvatar_url())
                    .apply(roomOptions)
                    .into(iv_user_header_bg);
        } else {
            Glide.with(UserCenterActivity.this)
                    .load(mUserInfoBean.getCover_img())
                    .into(iv_user_header_bg);
        }
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) rl_cover.getLayoutParams();
        params.height = ScreenUtil.adjustWidgetWidth();
        //昵称
        tv_user_name.setText(!TextUtils.isEmpty(mUserInfoBean.getNick_name()) ? mUserInfoBean.getNick_name() : "");
        //id
        tv_user_id.setText("ID " + mUserInfoBean.getUser_id());
        //年龄
        tv_user_age_level.setBackgroundResource("1".equals(mUserInfoBean.getGender()) ? R.drawable.bg_color_00aaff_16 : R.drawable.bg_color_ff75a3_16);
        Drawable ageDrawable = getResources().getDrawable("1".equals(mUserInfoBean.getGender()) ? R.mipmap.icon_male : R.mipmap.icon_female);
        ageDrawable.setBounds(0, 0, ageDrawable.getMinimumWidth(), ageDrawable.getMinimumHeight());
        tv_user_age_level.setCompoundDrawables(ageDrawable, null, null, null);
        tv_user_age_level.setCompoundDrawablePadding(3);
        tv_user_age_level.setText(mUserInfoBean.getAge());

        //魅力值
        int[] charmValuesDrawable = {R.drawable.ic_charm_00,R.drawable.ic_charm_20,R.drawable.ic_charm_40,R.drawable.ic_charm_60,R.drawable.ic_charm_80,R.drawable.ic_charm_100};
        tv_charm_value_level.setBackgroundResource(charmValuesDrawable[mUserInfoBean.getUser_charm()/20]);
        tv_charm_value_level.setText(Html.fromHtml(String.format("魅 • <b>%d</b>", mUserInfoBean.getUser_charm())));

        //录音
        ll_audio.setVisibility(TextUtils.isEmpty(mUserInfoBean.getVoice_intro()) ? View.GONE : View.VISIBLE);
        if (!TextUtils.isEmpty(mUserInfoBean.getVoice_intro())) {
            try {
                mMediaPlayer.reset();
                mMediaPlayer.setDataSource(mUserInfoBean.getVoice_intro());
                mMediaPlayer.prepare();
                mAudioDuration = mMediaPlayer.getDuration() / 1000;
                mAudioTimer = mAudioDuration;
                setAudioDuration();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        //礼物榜
        rankInfo = mUserInfoBean.getRank_info();
        if (rankInfo != null) {
            llGiftEmpty.setVisibility(View.GONE);
            rlHasGift.setVisibility(View.VISIBLE);
            if (rankInfo.is_guard) {
                isGuard.setVisibility(View.VISIBLE);
            } else {
                isGuard.setVisibility(View.GONE);
            }
            GuardGiftAdapter adapter = new GuardGiftAdapter(this, rankInfo.gift_list);
            LinearLayoutManager manager = new LinearLayoutManager(this);
            manager.setOrientation(RecyclerView.HORIZONTAL);
            guardGiftList.setLayoutManager(manager);
            guardGiftList.setAdapter(adapter);
            guardAngelName.setText(rankInfo.nick_name);
            Glide.with(this).applyDefaultRequestOptions(options).load(rankInfo.avatar_url).into(guardAngelAvatar);
        } else {
            llGiftEmpty.setVisibility(View.VISIBLE);
            rlHasGift.setVisibility(View.GONE);
        }
        //理想CP
        if (!TextUtils.isEmpty(mUserInfoBean.getCouple_desc())) {
            tv_user_cp_content.setVisibility(View.VISIBLE);
            tv_user_cp_content.setText(StringUtil.replaceBlank(mUserInfoBean.getCouple_desc()));
        } else {
            tv_user_cp_content.setVisibility(View.GONE);
        }
        rl_chat.setVisibility(StringUtil.isEquals(mUserInfoBean.getUser_id(), AccountUtil.getInstance().getUserId()) ? View.GONE : View.VISIBLE);
        rl_edit_user.setVisibility(StringUtil.isEquals(mUserInfoBean.getUser_id(), AccountUtil.getInstance().getUserId()) ? View.VISIBLE : View.GONE);
        tv_chat.setText(mUserInfoBean.isIs_on_mic() ? getString(R.string.user_center_go_live_room) : getString(R.string.user_center_go_p2p));

        boolean isFreeChat = mUserInfoBean.isNew_relation_free_chat();
        if (isFreeChat) {
            iv_free_call.setVisibility(View.VISIBLE);
        } else {
            iv_free_call.setVisibility(View.GONE);
        }

        mStaticGameBean = userCenterInfo.getStaticGameBean();
        //和平精英
        setHpjyParams(userCenterInfo);
        //王者荣耀
        setWzryParams(userCenterInfo);
        UserCenterInfo.Data.UserInfo.GameInfoBean gameInfoBean = userCenterInfo.getData().getUser_info().getGame_info();
        //其他游戏
        if (gameInfoBean != null && gameInfoBean.getOther() != null && gameInfoBean.getOther().size() > 0) {
            rl_game_other.setVisibility(View.VISIBLE);
            List<NormalGame> list = gameInfoBean.getOther();
            mOtherGameAdapter.update(list);
            isOtherGame = false;
        } else {
            rl_game_other.setVisibility(View.GONE);
            isOtherGame = true;
        }
        if (mUserInfoBean.isMatch_control()) {
            tv_game_want.setVisibility(View.GONE);
            ll_game_want.setVisibility(View.GONE);
            tv_user_tag.setVisibility(View.VISIBLE);
            //标签
            if (mUserInfoBean.getGames() != null && mUserInfoBean.getGames().size() > 0) {
                fl_user_tag.setVisibility(View.VISIBLE);
                tv_user_tag_seat.setVisibility(View.GONE);
                mTagStrList.clear();
                mTagStrList.addAll(mUserInfoBean.getGames());
                initLeaderTag(mTagStrList);
            } else {
                fl_user_tag.setVisibility(View.GONE);
                tv_user_tag_seat.setVisibility(View.VISIBLE);
            }
        } else {
            tv_game_want.setVisibility(View.VISIBLE);
            ll_game_want.setVisibility(View.VISIBLE);
            tv_user_tag.setVisibility(View.GONE);
            fl_user_tag.setVisibility(View.GONE);
            tv_user_tag_seat.setVisibility(View.GONE);
            if (isHePingGame && isWangZheGame && isOtherGame) {
                tv_game_want_empty.setVisibility(View.VISIBLE);
            } else {
                tv_game_want_empty.setVisibility(View.GONE);
            }
        }
    }

    private void setWzryParams(UserCenterInfo userCenterInfo) {
        UserCenterInfo.Data.UserInfo.GameInfoBean userInfo = userCenterInfo.getData().getUser_info().getGame_info();
        UserCenterInfo.WZRYDATA wzrydata = userCenterInfo.getWzrydata();
        if (userInfo != null && wzrydata != null) {
            UserCenterInfo.Data.UserInfo.GameInfoBean.WzryBean wzryBean = userInfo.getWzry();
            if (wzryBean != null) {
                if (wzryBean.getPlatform().size() == 0 && wzryBean.getRank().size() == 0 && wzryBean.getZone().size() == 0) {
                    rl_game_wangzhe.setVisibility(View.GONE);
                    isWangZheGame = true;
                } else {
                    rl_game_wangzhe.setVisibility(View.VISIBLE);
                    isWangZheGame = false;
                }
                StringBuilder wzry_platfrom = new StringBuilder();
                StringBuilder wzry_rank = new StringBuilder();
                StringBuilder wzry_zone = new StringBuilder();
                //平台
                List<Integer> platforms = wzryBean.getPlatform();
                ArrayList<Map<String, String>> wzry_platfrom_list = wzrydata.getWzry_platform_list();
                for (int i = 0; i < platforms.size(); i++) {
                    int num = platforms.get(i);

                    for (int j = 0; j < wzry_platfrom_list.size(); j++) {
                        Map<String, String> map = wzry_platfrom_list.get(j);
                        if (map.containsKey(num + "")) {
                            String s = map.get(num + "");
                            if (com.liquid.base.tools.StringUtil.isEmpty(wzry_platfrom.toString())) {
                                wzry_platfrom.append(s);
                            } else {
                                wzry_platfrom.append(" | ").append(s);
                            }
                        }
                    }
                }
                //排行
                List<Integer> ranks = wzryBean.getRank();
                ArrayList<Map<String, String>> wzry_rank_list = wzrydata.getWzry_rank_list();
                for (int i = 0; i < ranks.size(); i++) {
                    int num = ranks.get(i);

                    for (int j = 0; j < wzry_rank_list.size(); j++) {
                        Map<String, String> map = wzry_rank_list.get(j);
                        if (map.containsKey(num + "")) {
                            String s = map.get(num + "");
                            if (com.liquid.base.tools.StringUtil.isEmpty(wzry_rank.toString())) {
                                wzry_rank.append(s);
                            } else {
                                wzry_rank.append(" | ").append(s);
                            }
                        }
                    }
                }
                //大区
                List<Integer> zones = wzryBean.getZone();
                ArrayList<Map<String, String>> wzry_area_list = wzrydata.getWzry_area_list();
                for (int i = 0; i < zones.size(); i++) {
                    int num = zones.get(i);

                    for (int j = 0; j < wzry_area_list.size(); j++) {
                        Map<String, String> map = wzry_area_list.get(j);
                        if (map.containsKey(num + "")) {
                            String s = map.get(num + "");
                            if (com.liquid.base.tools.StringUtil.isEmpty(wzry_zone.toString())) {
                                wzry_zone.append(s);
                            } else {
                                wzry_zone.append(" | ").append(s);
                            }
                        }
                    }
                }

                tv_game_wangzhe_platform.setText(wzry_platfrom.toString());
                tv_game_wangzhe_rank.setText(wzry_rank.toString());
                tv_game_wangzhe_area.setText(wzry_zone.toString());
                RequestOptions options = new RequestOptions()
                        .placeholder(R.mipmap.icon_wangzherongyao)
                        .error(R.mipmap.icon_wangzherongyao);
                Glide.with(UserCenterActivity.this).load(wzrydata.getIcon()).apply(options).apply(RequestOptions.bitmapTransform(new RoundedCorners(12))).into(iv_wangzhe);
            } else {
                rl_game_wangzhe.setVisibility(View.GONE);
            }
        }
    }

    private void setHpjyParams(UserCenterInfo userCenterInfo) {
        UserCenterInfo.Data.UserInfo.GameInfoBean userInfo = userCenterInfo.getData().getUser_info().getGame_info();
        UserCenterInfo.HPJYDATA hpjydata = userCenterInfo.getHpjydata();
        if (userInfo != null && hpjydata != null) {
            UserCenterInfo.Data.UserInfo.GameInfoBean.HpjyBean hpjyBean = userInfo.getHpjy();
            if (hpjyBean != null) {
                if (hpjyBean.getPlatform().size() == 0 && hpjyBean.getRank().size() == 0 && hpjyBean.getZone().size() == 0) {
                    rl_game_heping.setVisibility(View.GONE);
                    isHePingGame = true;
                } else {
                    rl_game_heping.setVisibility(View.VISIBLE);
                    isHePingGame = false;
                }
                StringBuilder hpjy_platfrom = new StringBuilder();
                StringBuilder hpjy_rank = new StringBuilder();
                StringBuilder hpjy_zone = new StringBuilder();
                //平台
                List<Integer> platforms = hpjyBean.getPlatform();
                ArrayList<Map<String, String>> Hpjy_platfrom_list = hpjydata.getHpjy_platform_list();
                for (int i = 0; i < platforms.size(); i++) {
                    int num = platforms.get(i);

                    for (int j = 0; j < Hpjy_platfrom_list.size(); j++) {
                        Map<String, String> map = Hpjy_platfrom_list.get(j);
                        if (map.containsKey(num + "")) {
                            String s = map.get(num + "");
                            if (com.liquid.base.tools.StringUtil.isEmpty(hpjy_platfrom.toString())) {
                                hpjy_platfrom.append(s);
                            } else {
                                hpjy_platfrom.append(" | ").append(s);
                            }
                        }
                    }
                }

                //排行
                List<Integer> ranks = hpjyBean.getRank();
                ArrayList<Map<String, String>> hpjy_rank_list = hpjydata.getHpjy_rank_list();
                for (int i = 0; i < ranks.size(); i++) {
                    int num = ranks.get(i);

                    for (int j = 0; j < hpjy_rank_list.size(); j++) {
                        Map<String, String> map = hpjy_rank_list.get(j);
                        if (map.containsKey(num + "")) {
                            String s = map.get(num + "");
                            if (com.liquid.base.tools.StringUtil.isEmpty(hpjy_rank.toString())) {
                                hpjy_rank.append(s);
                            } else {
                                hpjy_rank.append(" | ").append(s);
                            }
                        }
                    }
                }

                //大区
                List<Integer> zones = hpjyBean.getZone();
                ArrayList<Map<String, String>> wzry_area_list = hpjydata.getHpjy_area_list();
                for (int i = 0; i < zones.size(); i++) {
                    int num = zones.get(i);

                    for (int j = 0; j < wzry_area_list.size(); j++) {
                        Map<String, String> map = wzry_area_list.get(j);
                        if (map.containsKey(num + "")) {
                            String s = map.get(num + "");
                            if (com.liquid.base.tools.StringUtil.isEmpty(hpjy_zone.toString())) {
                                hpjy_zone.append(s);
                            } else {
                                hpjy_zone.append(" | ").append(s);
                            }
                        }
                    }
                }

                tv_game_heping_platform.setText(hpjy_platfrom.toString());
                tv_game_heping_rank.setText(hpjy_rank.toString());
                tv_game_heping_area.setText(hpjy_zone.toString());
                RequestOptions options = new RequestOptions()
                        .placeholder(R.mipmap.icon_hepingjingying)
                        .error(R.mipmap.icon_hepingjingying);
                Glide.with(UserCenterActivity.this).load(hpjydata.getIcon()).apply(options).apply(RequestOptions.bitmapTransform(new RoundedCorners(12))).into(iv_heping);
            } else {
                rl_game_heping.setVisibility(View.GONE);
            }

        }
    }

    @Override
    public void onUserPhotosFetch(UserPhotoInfo userPhotoInfo) {
        //个人相册
        if (userPhotoInfo.getData().getPhotos().size() == 0) {
            rv_pics.setVisibility(StringUtil.isEquals(mUserId, AccountUtil.getInstance().getUserId()) ? View.VISIBLE : View.GONE);
            tv_user_pics_seat.setVisibility(StringUtil.isEquals(mUserId, AccountUtil.getInstance().getUserId()) ? View.GONE : View.VISIBLE);
        }
        mUserPhotoList = userPhotoInfo.getData().getPhotos();
        mImageList.clear();
        if (isUpdatePic) {
            mImageList.addAll(mUserPhotoList);
            isPicOpen = mUserPhotoList.size() + 1 > 4;
            tv_center_pic_tip.setText(mUserPhotoList.size() + 1 > 4 ? getString(R.string.session_guard_game_close) : getString(R.string.session_guard_game_open));
            JinquanResourceHelper.getInstance(this).setImageResourceByAttr(iv_center_pic_tip, mUserPhotoList.size() + 1 > 4 ? R.attr.custom_bg_user_photo_close : R.attr.custom_bg_user_photo_open);
        } else {
            mImageList.addAll(mUserPhotoList.size() > 4 ? mUserPhotoList.subList(0, 4) : mUserPhotoList);
        }
        mAdapter.notifyDataSetChanged();
        if (StringUtil.isEquals(mUserId, AccountUtil.getInstance().getUserId())) {
            ll_center_pic_tip.setVisibility(mUserPhotoList.size() + 1 > 4 ? View.VISIBLE : View.GONE);
            if (isUpdatePic) {
                mAdapter.notifyPicOpen(false);
            } else {
                mAdapter.notifyPicOpen(mUserPhotoList.size() + 1 > 4);
            }
        } else {
            ll_center_pic_tip.setVisibility(mUserPhotoList.size() > 4 ? View.VISIBLE : View.GONE);
            mAdapter.notifyPicOpen(false);
        }
    }

    @Override
    public void uploadImageOssSuccess(String path) {
        if (isUserBg) {
            Glide.with(UserCenterActivity.this)
                    .load(path)
                    .into(iv_user_header_bg);
        } else {
            mPresenter.getUserPhotos(mPage, mUserId);
            isUpdatePic = true;
        }
    }

    @Override
    public void onDeletePhotosFetch() {
        mPresenter.getUserPhotos(mPage, mUserId);
        isUpdatePic = true;
    }

    @Override
    public void blockUserSuccessFetch(BaseModel baseModel) {

    }

    @Override
    public void onRoomDetailsFetched(RoomDetailsInfo roomDetailsInfo) {
        if (roomDetailsInfo.getData() == null) {
            return;
        }
        if (roomDetailsInfo.getData().getRoom_type() == RoomLiveActivityV2.ROOM_TYPE_PRIVATE) {
            //专属房
            if (!TextUtils.isEmpty(roomDetailsInfo.getData().getHint())) {
                showMessage(roomDetailsInfo.getData().getHint());
            }
        } else {
            //Cp房 直播中
            Bundle bundle = new Bundle();
            bundle.putString(Constants.ROOM_ID, roomDetailsInfo.getData().getRoom_id());
            bundle.putString(Constants.IM_ROOM_ID, roomDetailsInfo.getData().getIm_room_id());
            bundle.putInt(Constants.IM_ROOM_TYPE, roomDetailsInfo.getData().getRoom_type());
            bundle.putString(Constants.AUDIENCE_FROM, DataOptimize.PERSON_INFO_PAGE);
            RouterUtils.goToRoomLiveActivity(this,bundle);
//            readyGo(RoomLiveActivityV2.class, bundle);
            HashMap<String, String> params = new HashMap<>();
            params.put(Constants.ROOM_ID, roomDetailsInfo.getData().getRoom_id());
            params.put("user_id", AccountUtil.getInstance().getUserId());
            params.put("female_guest_id", mUserId);
            MultiTracker.onEvent(TrackConstants.B_USER_CENTER_TO_LIVE_ROOM, params);
        }
    }

    @Override
    public void updateCommentList(FemaleGuestCommentBean data) {
        if (adapter != null) {
            commentBeans.clear();
            commentBeans.addAll(data.feed_list);

            adapter.update(data.feed_list);
            tv_feed.setVisibility(Constants.show_feed == 1 ? View.VISIBLE : View.GONE);
            if (data.feed_list != null && data.feed_list.size() > 0) {
                rv_person_new.setVisibility(Constants.show_feed == 1 ? View.VISIBLE : View.GONE);
                ll_feed_empty.setVisibility(View.GONE);
            } else {
                rv_person_new.setVisibility(View.GONE);
                ll_feed_empty.setVisibility(Constants.show_feed == 1 ? View.VISIBLE : View.GONE);
            }
        }
    }

    @Override
    public void updateLikeResult() {
    }

    @Override
    public void onSelectImg() {
        //选择图片
        isUserBg = false;
        choosePictureDialog(9);
    }

    private void choosePictureDialog(int maxSelectNum) {
        pictureWindowAnimationStyle.ofAllAnimation(R.anim.live_bottom_in,R.anim.live_bottom_out);
        PictureSelectorDialogFragment.newInstance().choosePictureListener(new PictureSelectorDialogFragment.PictureSelectorListener() {
            @Override
            public void onShotChoose() {
                PictureSelector.create(UserCenterActivity.this)
                        .openCamera(PictureMimeType.ofImage())
                        .maxSelectNum(1)
                        .isAndroidQTransform(true)
                        .imageEngine(GlideEngine.createGlideEngine())
                        .setPictureWindowAnimationStyle(pictureWindowAnimationStyle)
                        .forResult(PictureConfig.REQUEST_CAMERA);
            }

            @Override
            public void onAlbumChoose() {
                PictureSelector.create(UserCenterActivity.this)
                        .openGallery(PictureMimeType.ofImage())
                        .maxSelectNum(maxSelectNum)
                        .isAndroidQTransform(true)
                        .imageEngine(GlideEngine.createGlideEngine())
                        .setPictureWindowAnimationStyle(pictureWindowAnimationStyle)
                        .forResult(PictureConfig.CHOOSE_REQUEST);
            }
        }).show(getSupportFragmentManager(), PictureSelectorDialogFragment.class.getSimpleName());
    }

    @Override
    public void onItemClick(int pos, List<UserPhotoInfo.Data.Photos> data) {
        if (data.get(pos).getType() == 2) {
            PictureSelector.create(this).externalPictureVideo(data.get(pos).getPath());
        } else {
            //点击图片预览
            PictureSelector.create((Activity) mContext)
                    .themeStyle(R.style.picture_default_style)
                    .isNotPreviewDownload(true)
                    .isPreviewVideo(true)
                    .imageEngine(GlideEngine.createGlideEngine())
                    .openExternalPreview(pos, getSelectList(data));
        }
        HashMap<String, String> params = new HashMap<>();
        params.put("female_guest_id", mUserId);
        MultiTracker.onEvent(TrackConstants.B_PHOTO, params);


    }

    private List<LocalMedia> getSelectList(List<UserPhotoInfo.Data.Photos> data) {
        List<LocalMedia> localMedia = new ArrayList<>();
        for (UserPhotoInfo.Data.Photos photos : data) {
            if (photos.getType() != 2) {
                localMedia.add(new LocalMedia(photos.getPath(), 0, 0, ""));
            }
        }
        return localMedia;
    }

    @Override
    public void onDeleteImg(int pos, String photoId) {
        //删除图片
        mPresenter.deleteUserPhoto(photoId);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case PictureConfig.CHOOSE_REQUEST:
                case PictureConfig.REQUEST_CAMERA:
                    List<LocalMedia> selectList = PictureSelector.obtainMultipleResult(data);
                    pathLists.clear();
                    for (int i = 0; i < selectList.size(); i++) {
                        String path = selectList.get(i).getPath();
                        if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.P) {
                            path = selectList.get(i).getAndroidQToPath();
                        }
                        pathLists.add(path);
                    }
                    if (isUserBg) {
                        mPresenter.uploadImageOss(pathLists, "user_cover_img");
                    } else {
                        mPresenter.uploadImageOss(pathLists, "user_photo");
                    }
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    protected void onEventComing(EventCenter eventCenter) {
        super.onEventComing(eventCenter);
        switch (eventCenter.getEventCode()) {
            case Constants.EVENT_UPDATE_PERSON_NEWS:
                if (mPresenter != null) {
                    mPresenter.getCommentList(mUserId, null);
                }
                break;
            case Constants.EVENT_UPDATE_LIST_POSITION:
                if (osv_scroll_view != null) {
                    osv_scroll_view.fullScroll(ScrollView.FOCUS_UP);
                }
                break;
        }
    }

    private void initLeaderTag(List<String> tagStrList) {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(0, ScreenUtil.dip2px(5), ScreenUtil.dip2px(10), ScreenUtil.dip2px(5));
        fl_user_tag.removeAllViews();
        for (final String item : tagStrList) {
            final TextView tv = new TextView(this);
            tv.setGravity(Gravity.CENTER);
            tv.setText(item);
            tv.setMaxEms(10);
            tv.setTextSize(12);
            tv.setPadding(ScreenUtil.dip2px(5), ScreenUtil.dip2px(5), ScreenUtil.dip2px(5), ScreenUtil.dip2px(5));
            tv.setTextColor(Color.parseColor("#191E24"));
            tv.setSingleLine();
            tv.setLayoutParams(layoutParams);
            JinquanResourceHelper.getInstance(this).setTextColorByAttr(tv, R.attr.custom_attr_flow_item_text_color);
            JinquanResourceHelper.getInstance(this).setBackgroundResourceByAttr(tv, R.attr.custom_attr_bg_flow_item);
            fl_user_tag.addView(tv);
        }
    }

    @OnClick({R.id.ll_center_pic_tip, R.id.tv_chat, R.id.iv_back, R.id.iv_user_header_bg, R.id.ll_audio,
            R.id.rl_chat, R.id.rl_edit_user, R.id.my_gift_notice, R.id.ll_gift_empty, R.id.click_to_rank_list})
    public void onClick(View view) {
        if (ViewUtils.isFastClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.ll_center_pic_tip:
                mImageList.clear();
                if (isPicOpen) {
                    mImageList.addAll(mUserPhotoList.size() > 4 ? mUserPhotoList.subList(0, 4) : mUserPhotoList);
                } else {
                    mImageList.addAll(mUserPhotoList);
                }
                mAdapter.notifyPicOpen(isPicOpen);
                mAdapter.notifyDataSetChanged();
                tv_center_pic_tip.setText(isPicOpen ? getString(R.string.session_guard_game_open) : getString(R.string.session_guard_game_close));
                JinquanResourceHelper.getInstance(this).setImageResourceByAttr(iv_center_pic_tip, isPicOpen ? R.attr.custom_bg_user_photo_open : R.attr.custom_bg_user_photo_close);
                isPicOpen = !isPicOpen;
                break;
            case R.id.tv_chat:
                if (mUserInfoBean != null && mUserInfoBean.isIs_on_mic()) {
                    mPresenter.fetchRoomDetails(mUserInfoBean.getRoom_id());
                } else {
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.SESSION_ID, mUserId);
                    if (clientId == 2) {
                        readyGo(SessionActivityV2.class, bundle);
                    }else {
                        readyGo(SessionActivity.class, bundle);
                    }
                    HashMap<String, String> params = new HashMap<>();
                    params.put("user_id", AccountUtil.getInstance().getUserId());
                    params.put("female_guest_id", mUserId);
                    MultiTracker.onEvent(TrackConstants.B_P2P_CHAT, params);
                }
                break;
            case R.id.iv_back:
                onBackPressed();
                break;
            case R.id.iv_user_header_bg:
                //背景
                if (StringUtil.isEquals(mUserId, AccountUtil.getInstance().getUserId())) {
                    isUserBg = true;
                    choosePictureDialog(1);
                }
                break;
            case R.id.ll_audio:
                //语音
                if (mMediaPlayer.isPlaying() && !isPause) {
                    isPause = true;
                    endAudio();
                } else {
                    isPause = false;
                    startAudio();
                }
                HashMap<String, String> params = new HashMap<>();
                params.put("user_id", AccountUtil.getInstance().getUserId());
                params.put("female_guest_id", mUserId);
                MultiTracker.onEvent(TrackConstants.B_VOICE, params);
                break;
            case R.id.rl_chat:
                //聊天
                break;
            case R.id.rl_edit_user:
                //编辑资料
                Intent intent = new Intent(UserCenterActivity.this, UserInfoSettingActivity.class);
                intent.putExtra(Constants.USER_ID, mUserInfoBean.getUser_id());
                intent.putExtra(Constants.USER_STATIC_GAME, new Gson().toJson(mStaticGameBean));
                intent.putExtra(Constants.USER_GAME, new Gson().toJson(mUserInfoBean.getGame_info()));
                intent.putExtra(Constants.USER_GAME_MATCH_CONTROL, mUserInfoBean.isMatch_control());
                startActivityForResult(intent, SAVE_INFO_SUCCESS);
                break;
            case R.id.my_gift_notice:
                if (rankInfo != null) {
                    Bundle bundle = new Bundle();
                    bundle.putString(AngelGuardNoticeActivity.FEMALE_USER_ID, mUserId);
                    bundle.putString(GIFT_RANK_PATH, giftRankSource);
                    readyGo(AngelGuardNoticeActivity.class, bundle);
                } else {
                    ToastUtils.s(this, "暂未收到礼物");
                }
                break;
            case R.id.ll_gift_empty:
                ToastUtils.s(this, "暂未收到礼物");
                break;
            case R.id.click_to_rank_list:
                Bundle bundle = new Bundle();
                bundle.putString(AngelGuardNoticeActivity.FEMALE_USER_ID, mUserId);
                bundle.putString(GIFT_RANK_PATH, giftRankSource);
                readyGo(AngelGuardNoticeActivity.class, bundle);
                break;
        }
    }

    @Override
    public void onBackPressed() {
//        ptaLauncher.release();
        super.onBackPressed();
    }

    /**
     * 播放语音
     */
    private void startAudio() {
        if (isDestroyed() || isFinishing()) {
            return;
        }
        iv_audio_play_status.setBackgroundResource(R.drawable.anim_user_audio_list);
        AnimationDrawable animation = (AnimationDrawable) iv_audio_play_status.getBackground();
        animation.start();
        if (mUserInfoBean != null && !TextUtils.isEmpty(mUserInfoBean.getVoice_intro()) && mMediaPlayer != null) {
            try {
                mMediaPlayer.reset();
                mMediaPlayer.setDataSource(mUserInfoBean.getVoice_intro());
                mMediaPlayer.prepare();
                mMediaPlayer.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (mTimer2 == null && mTask2 == null) {
            mTimer2 = new Timer();
            mTask2 = new TimerTask() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mAudioTimer--;
                            if (mAudioTimer <= 0) {
                                mAudioTimer = mAudioDuration;
                                endAudio();
                            } else {
                                setAudioDuration();
                            }
                        }
                    });
                }
            };
            mTimer2.schedule(mTask2, 0, 1000);
        }
    }

    private void setAudioDuration() {
        if (isDestroyed() || isFinishing()) {
            return;
        }
        tv_audio_timer.setText(mAudioTimer + "s");
    }

    private void releaseTimer() {
        if (mTask2 != null) {
            mTask2.cancel();
            mTask2 = null;
        }
        if (mTimer2 != null) {
            mTimer2.cancel();
            mTimer2 = null;
        }
    }

    /**
     * 暂停语音
     */
    private void endAudio() {
        if (isDestroyed() || isFinishing()) {
            return;
        }
        AnimationDrawable animation = (AnimationDrawable) iv_audio_play_status.getBackground();
        animation.selectDrawable(0);
        animation.stop();
        mAudioTimer = mAudioDuration;
        setAudioDuration();
        releaseTimer();
    }

    @Override
    public void onScrollChanged(ObservableScrollView scrollView, int x, int y, int oldx, int oldy) {
        if (y > imageHeight) {
            JinquanResourceHelper.getInstance(UserCenterActivity.this).setImageResourceByAttr(iv_back, R.attr.custom_attr_back);
            JinquanResourceHelper.getInstance(UserCenterActivity.this).setBackgroundResourceByAttr(ll_title_layout, R.attr.custom_attr_app_bg);
        } else {
            iv_back.setImageResource(R.mipmap.icon_user_center_back);
            ll_title_layout.setBackgroundColor(getResources().getColor(R.color.transparent));
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
        if (mMediaPlayer != null) {
            mMediaPlayer.stop();
            mMediaPlayer.reset();
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
        releaseTimer();
    }
}
