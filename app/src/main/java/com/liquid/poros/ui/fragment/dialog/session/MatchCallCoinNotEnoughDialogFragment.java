package com.liquid.poros.ui.fragment.dialog.session;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.aliyun.sls.android.sdk.LOGClient;
import com.bumptech.glide.Glide;
import com.liquid.base.event.EventCenter;
import com.liquid.poros.R;
import com.liquid.poros.base.BaseDialogFragment;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.helper.TimeUtil;
import com.liquid.poros.ui.fragment.dialog.common.CommonActionListener;
import com.liquid.poros.utils.ViewUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import de.greenrobot.event.EventBus;

/**
 * 匹配通话页面金币不足
 */
public class MatchCallCoinNotEnoughDialogFragment extends BaseDialogFragment implements View.OnClickListener {
    private int mCount = 30;

    public MatchCallCoinNotEnoughDialogFragment setmCount(int mCount) {
        this.mCount = mCount;
        return this;
    }

    public MatchCallCoinNotEnoughDialogFragment setTime(int time) {
        this.time = time;
        return this;
    }

    private int time = 2;
    private CountDownTimer mCountDownTimer;
    private TextView tv_desc;

    public static MatchCallCoinNotEnoughDialogFragment newInstance() {
        MatchCallCoinNotEnoughDialogFragment fragment = new MatchCallCoinNotEnoughDialogFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_confirm:
                if (listener != null) {
                    listener.onConfirm();
                }
                break;
            case R.id.tv_cancel:
                if (listener != null) {
                    listener.onCancel();
                }
                break;
        }
        dismissDialog();

    }

    private CommonActionListener listener;

    public MatchCallCoinNotEnoughDialogFragment setCommonActionListener(CommonActionListener listener) {
        this.listener = listener;
        return this;
    }

    @Override
    protected String getPageId() {
        return TrackConstants.PAGE_MATCH_CALL_COIN_NOT_ENOUGH_DIALOG;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        Window win = getDialog().getWindow();
        // 一定要设置Background，如果不设置，window属性设置无效
        win.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams params = win.getAttributes();
        params.gravity = Gravity.CENTER;
        // 使用ViewGroup.LayoutParams，以便Dialog 宽度充满整个屏幕
        params.width = (int) (dm.widthPixels * 0.78);
        win.setAttributes(params);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_match_call_coin_not_enough, null);

        TextView tv_confirm = view.findViewById(R.id.tv_confirm);
        TextView tv_cancel = view.findViewById(R.id.tv_cancel);
        tv_desc = view.findViewById(R.id.tv_desc);

        tv_confirm.setOnClickListener(this);
        tv_cancel.setOnClickListener(this);

        startCountTime();
        builder.setView(view);
        return builder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    private void startCountTime() {
        if (mCountDownTimer == null) {
            mCountDownTimer = new CountDownTimer(Integer.MAX_VALUE, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    if (isDetached() || isRemoving()) {
                        return;
                    }
                    if (mCount < 0){
                        dismissDialog();
                        return;
                    }
                    tv_desc.setText(getCountText(mCount));
                    mCount--;

                }

                @Override
                public void onFinish() {

                }
            };
            mCountDownTimer.start();
        }
    }

    private void endCountTime() {
        if (isDetached() || isRemoving()) {
            return;
        }

        if (mCountDownTimer != null) {
            mCountDownTimer.cancel();
            mCountDownTimer = null;
        }
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);
        endCountTime();
    }

    private SpannableStringBuilder getCountText(int count) {

        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(String.format("您的通话不足%s分钟\n" +
                "为了保持通话，别忘了充值呦\n", time));
        SpannableString spannableString = new SpannableString(String.format("%ss", count));
        spannableString.setSpan(new ForegroundColorSpan(Color.parseColor("#FFD13D")), 0, spannableString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableStringBuilder.append(spannableString);

        return spannableStringBuilder;
    }
}
