package com.liquid.poros.ui.fragment.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.liquid.library.retrofitx.RetrofitX
import com.liquid.poros.entity.BuyCoupleCardInfo
import com.liquid.poros.entity.CoupleCardListInfo.Data
import com.liquid.poros.http.ApiUrl
import com.liquid.poros.http.observer.ObjectObserver
import com.liquid.poros.http.utils.postPoros

/**
 * 女嘉宾召集抢单
 */
class BuyCpCardViewModel : ViewModel() {
    val buyCpCardDataSuccess : MutableLiveData<Data> by lazy { MutableLiveData<Data>() }
    val buyCpCardDataFail : MutableLiveData<Data> by lazy { MutableLiveData<Data>() }
    val buyCpCardSuccess : MutableLiveData<BuyCoupleCardInfo.Data> by lazy { MutableLiveData<BuyCoupleCardInfo.Data>() }
    val buyCpCardFail : MutableLiveData<BuyCoupleCardInfo.Data> by lazy { MutableLiveData<BuyCoupleCardInfo.Data>() }

    fun buyCpCard(targetUserId : String?,cardCategory : String,isRoom : Boolean) {
        RetrofitX.url(ApiUrl.SALE_CARD_LIST)
                .param("target_user_id",targetUserId)
                .param("card_category",cardCategory)
                .param("is_room",isRoom)
                .postPoros()
                .subscribe(object : ObjectObserver<Data>(){
                    override fun success(result: Data) {
                        buyCpCardDataSuccess.postValue(result)
                    }

                    override fun failed(result: Data) {
                        buyCpCardDataFail.postValue(result)
                    }

                })
    }

    fun buyCouple(cardType: Int?, cardPrice: String?, targetId: String?, cp_order_id: String?) {
        RetrofitX.url(ApiUrl.BUY_CP_CARD)
                .param("card_type", cardType)
                .param("card_price", cardPrice)
                .param("target_user_id", targetId)
                .param("cp_order_id", cp_order_id)
                .postPoros()
                .subscribe(object : ObjectObserver<BuyCoupleCardInfo.Data>(){
                    override fun success(result: BuyCoupleCardInfo.Data) {
                        buyCpCardSuccess.postValue(result)
                    }

                    override fun failed(result: BuyCoupleCardInfo.Data) {
                        buyCpCardFail.postValue(result)
                    }

                })

    }
}