package com.liquid.poros.ui.fragment.dialog.videomatch

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import com.liquid.poros.R
import kotlinx.android.synthetic.main.dialog_video_match_tip.*

class VideoMatchTipDialog(context:Context) : Dialog(context){
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.decorView.setBackgroundResource(R.color.transparent)
        setContentView(R.layout.dialog_video_match_tip)
        quick_match_ensure.setOnClickListener {
            dismiss()
        }
    }

    fun setQuickMatchContent(content:String) {
        quick_match_content?.text = content
    }
}