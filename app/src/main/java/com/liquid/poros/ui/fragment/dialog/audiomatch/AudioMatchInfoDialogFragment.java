package com.liquid.poros.ui.fragment.dialog.audiomatch;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.liquid.base.adapter.CommonAdapter;
import com.liquid.base.adapter.CommonViewHolder;
import com.liquid.base.event.EventCenter;
import com.liquid.poros.R;
import com.liquid.poros.base.BaseDialogFragment;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.entity.AudioMatchGameBean;
import com.liquid.poros.entity.AudioMatchInfo;
import com.liquid.poros.helper.JinquanResourceHelper;
import com.liquid.poros.utils.ScreenUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import de.greenrobot.event.EventBus;

/**
 * 语音速配信息设置弹窗
 */
public class AudioMatchInfoDialogFragment extends BaseDialogFragment {
    private CommonAdapter<AudioMatchGameBean> mAdapter;
    private List<AudioMatchGameBean> editGameBeans = new ArrayList<>();
    private List<AudioMatchGameBean> editGameBeansTemp = new ArrayList<>();
    private AudioMatchInfo.Data.UserGameInfo honorBean;
    private AudioMatchInfo.Data.UserGameInfo honorBeanTemp;

    @Override
    protected String getPageId() {
        return TrackConstants.PAGE_TEST_EDIT_GAME;
    }

    public AudioMatchInfoDialogFragment setHonorBean(AudioMatchInfo.Data.UserGameInfo gameInfo) {
        this.honorBean = gameInfo;
        Gson gson = new Gson();
        honorBeanTemp = gson.fromJson(gson.toJson(honorBean), AudioMatchInfo.Data.UserGameInfo.class);
        return this;
    }

    public static AudioMatchInfoDialogFragment newInstance() {
        AudioMatchInfoDialogFragment fragment = new AudioMatchInfoDialogFragment();
        return fragment;
    }

    public AudioMatchInfoDialogFragment setEditGameBeans(List<AudioMatchGameBean> editGameBeans) {
        this.editGameBeans = editGameBeans;
        Gson gson = new Gson();
        this.editGameBeansTemp = gson.fromJson(gson.toJson(editGameBeans), new TypeToken<List<AudioMatchGameBean>>() {
        }.getType());
        return this;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        Window win = getDialog().getWindow();
        // 一定要设置Background，如果不设置，window属性设置无效
        win.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams params = win.getAttributes();
        params.width = ScreenUtil.getDisplayWidth();
        params.gravity = Gravity.BOTTOM;
        win.setAttributes(params);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_audio_match_info_dialog, null);
        view.findViewById(R.id.save).setOnClickListener(v -> {
            HashMap<String, Object> map = new HashMap<>();
            if (honorBeanTemp != null) {
                if (honorBeanTemp.getZone() == 0) {
                    Toast.makeText(getContext(), getString(R.string.test_dialog_game_area_tip), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (honorBeanTemp.getRank() == 0) {
                    Toast.makeText(getContext(), getString(R.string.test_dialog_game_rank_tip), Toast.LENGTH_SHORT).show();
                    return;
                }
                honorBean = honorBeanTemp;
                map.put("static", editGameBeansTemp);
                map.put("data", honorBean);
                EventBus.getDefault().post(new EventCenter(Constants.EVENT_CODE_UPDATE_AUDIO_MATCH_GAME_INFO, map));
            }
            dismissDialog();
        });
        RecyclerView rv = view.findViewById(R.id.rv_game);
        rv.setLayoutManager(new LinearLayoutManager(getContext()));
        mAdapter = new CommonAdapter<AudioMatchGameBean>(getContext(), R.layout.item_edit_game) {
            @Override
            public void convert(CommonViewHolder holder, final AudioMatchGameBean data, int position) {
                holder.setText(R.id.item_game_title, data.des);
                RecyclerView item_rv = holder.getView(R.id.item_rv);
                item_rv.setLayoutManager(new GridLayoutManager(getContext(), 3));
                CommonAdapter<AudioMatchGameBean.Item> mItemAdapter = new CommonAdapter<AudioMatchGameBean.Item>(getContext(), R.layout.item_edit_game_item) {
                    @Override
                    public void convert(CommonViewHolder holderItem, AudioMatchGameBean.Item item, int pos) {
                        holderItem.setText(R.id.item_game_content, item.name);
                        if (item.select) {
                            holderItem.setTextColor(R.id.item_game_content, getResources().getColor(R.color.white));
                            holderItem.setBackgroundResource(R.id.item_game_content, R.drawable.bg_gradual_14d4d6_5);
                        } else {
                            JinquanResourceHelper.getInstance(getActivity()).setTextColorByAttr(holderItem.getView(R.id.item_game_content), R.attr.custom_attr_unknown_text_color);
                            JinquanResourceHelper.getInstance(getActivity()).setBackgroundResourceByAttr(holderItem.getView(R.id.item_game_content), R.attr.bg_test_dialog_game_item);
                        }
                        holderItem.setOnClickListener(R.id.item_game_content, v -> {
                            if (position == 0) {
                                if (honorBeanTemp != null) {
                                    for (int i = 0; i < editGameBeansTemp.get(0).items.size(); i++) {
                                        if (pos == i) {
                                            honorBeanTemp.setZone(Integer.valueOf(item.id));
                                            editGameBeansTemp.get(0).items.get(i).select = true;
                                        } else {
                                            editGameBeansTemp.get(0).items.get(i).select = false;
                                        }
                                    }
                                }
                            } else if (position == 1) {
                                if (honorBeanTemp != null) {
                                    for (int i = 0; i < editGameBeansTemp.get(1).items.size(); i++) {
                                        if (pos == i) {
                                            honorBeanTemp.setRank(Integer.valueOf(item.id));
                                            editGameBeansTemp.get(1).items.get(i).select = true;
                                        } else {
                                            editGameBeansTemp.get(1).items.get(i).select = false;
                                        }
                                    }
                                }
                            }
                            notifyDataSetChanged();
                        });
                    }
                };
                mItemAdapter.update(data.items);
                item_rv.setAdapter(mItemAdapter);
                item_rv.addItemDecoration(new RecyclerView.ItemDecoration() {
                    @Override
                    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
                        super.getItemOffsets(outRect, view, parent, state);
                        GridLayoutManager.LayoutParams layoutParams = (GridLayoutManager.LayoutParams) view.getLayoutParams();
                        int spanIndex = layoutParams.getSpanIndex();
                        outRect.top = ScreenUtil.dip2px(8);
                        if (spanIndex == 0) {
                            // left
                            outRect.left = 0;
                            outRect.right = ScreenUtil.dip2px(4);
                        } else if (spanIndex == 1) {
                            outRect.left = ScreenUtil.dip2px(4);
                            outRect.right = ScreenUtil.dip2px(4);
                        } else {
                            outRect.left = ScreenUtil.dip2px(4);
                            outRect.right = 0;
                        }
                    }
                });
            }
        };
        mAdapter.update(editGameBeansTemp);
        rv.setAdapter(mAdapter);
        builder.setView(view);
        AlertDialog alertDialog = builder.create();
        return alertDialog;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
