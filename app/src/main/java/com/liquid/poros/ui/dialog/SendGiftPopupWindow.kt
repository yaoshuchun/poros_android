package com.liquid.poros.ui.dialog

import android.content.Context
import android.view.Gravity
import android.view.View
import com.blankj.utilcode.util.SizeUtils
import com.liquid.poros.entity.MicUserInfo
import com.liquid.poros.entity.MsgUserModel
import razerdp.basepopup.BasePopupWindow

/**
 * 送礼对话框弹框效果
 */
class SendGiftPopupWindow(context: Context) : BasePopupWindow(context) {

    lateinit var giftView: SendGiftView

    override fun onCreateContentView(): View {
        popupGravity = Gravity.END or Gravity.BOTTOM
        setPopupGravityMode(GravityMode.ALIGN_TO_ANCHOR_SIDE, GravityMode.RELATIVE_TO_ANCHOR)
        offsetX = SizeUtils.dp2px(5.0f)
        return SendGiftView(context).also { giftView = it }
    }

    override fun dismiss() {
        if (isShowingGiftNum()) {
            giftView.selectGiftNumPanel?.visibility = View.GONE
        } else {
            super.dismiss()
        }
    }

    /**
     * 是否正在显示送礼对话框的选择礼物数量界面
     */
    private fun isShowingGiftNum(): Boolean {
        return giftView.selectGiftNumPanel?.visibility == View.VISIBLE
    }


    fun setRoomId(roomId: String) = giftView.setRoomId(roomId)

    fun setSessionId(sessionId: String?) = giftView.setSessionId(sessionId)

    /**
     * 设置金币数量
     */
    fun setCoinCount(coinCountStr: Long) = giftView.setCoinCount(coinCountStr)

    /**
     * 是否是LiveRoom
     */
    fun setIsLiveRoom(isLiveRoom: Boolean) = giftView.setIsLiveRoom(isLiveRoom)

    /**
     * 设置来源
     */
    fun setFrom(from: String?) = giftView.setFrom(from)
    /**
     * 设置房间入口，现在只有系统导流弹窗
     */
    fun setSource(source: String?) = giftView.setSource(source)
    /**
     * 设置用户信息
     */
    fun setUserInfo(micUserInfos: Array<MicUserInfo?>, toAnchor: Boolean, showAddFriendButton: Boolean, pos: Int, mRoomType: Int) = giftView.setUserInfo(micUserInfos, toAnchor, showAddFriendButton, pos, mRoomType)

    fun setAddFriendTipVisible(spendCoin: Int) = giftView.setAddFriendTipVisible(spendCoin)

    fun setCoinBalance(coin: String) = giftView.setCoinBalance(coin)

    fun setListener(onSendGiftClickListener: SendGiftView.OnSendGiftClickListener) = giftView.setListener(onSendGiftClickListener)
    fun setData(giftList: List<MsgUserModel.GiftBean>, giftNumList: List<MsgUserModel.GiftNums>) = giftView.setData(giftList, giftNumList)
}