package com.liquid.poros.ui.fragment.blindbox

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.liquid.base.adapter.CommonAdapter
import com.liquid.base.adapter.CommonViewHolder
import com.liquid.poros.R
import com.liquid.poros.base.BaseActivity
import com.liquid.poros.base.BaseFragment
import com.liquid.poros.constant.Constants
import com.liquid.poros.entity.BlindBoxRecordInfo
import com.liquid.poros.ui.fragment.blindbox.viewmodel.BlindBoxRecordVM
import com.liquid.poros.utils.ScreenUtil
import com.liquid.poros.widgets.pulltorefresh.BaseRefreshListener
import com.liquid.poros.widgets.pulltorefresh.ViewStatus
import kotlinx.android.synthetic.main.activity_withdrawal_history.container
import kotlinx.android.synthetic.main.fragment_blind_box_record.*

/**
 * 初级&中级&高级盲盒中奖记录
 */
class BlindBoxRecordFragment : BaseFragment(), BaseRefreshListener {
    private var mCommonAdapter: CommonAdapter<BlindBoxRecordInfo.BlindBoxBean>? = null
    private val blindBoxRecordVM: BlindBoxRecordVM by lazy { getFragmentViewModel(BlindBoxRecordVM::class.java) }
    private var blindRecordType: Int = 0 //初级盲盒

    companion object {
        fun newInstance(blindBoxRecordType: Int): BlindBoxRecordFragment? {
            val fragment: BlindBoxRecordFragment = BlindBoxRecordFragment()
            val bundle = Bundle()
            bundle.putInt(Constants.BLIND_BOX_RECORD_TYPE, blindBoxRecordType)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun getPageId(): String {
        return ""
    }

    override fun getContentViewLayoutID(): Int {
        return R.layout.fragment_blind_box_record
    }

    override fun notifyNetworkChanged(isConnected: Boolean) {

    }

    override fun initViewsAndEvents(savedInstanceState: Bundle?) {
        blindRecordType = arguments?.get(Constants.BLIND_BOX_RECORD_TYPE) as Int
        rv_blind_box_record!!.layoutManager = LinearLayoutManager(activity)
        mCommonAdapter = object : CommonAdapter<BlindBoxRecordInfo.BlindBoxBean>(activity, R.layout.item_blind_box_record) {
            override fun convert(holder: CommonViewHolder, blindBoxBean: BlindBoxRecordInfo.BlindBoxBean, pos: Int) {
                val options = RequestOptions()
                        .placeholder(R.mipmap.img_default_avatar)
                        .override(ScreenUtil.dip2px(48f), ScreenUtil.dip2px(48f))
                        .error(R.mipmap.img_default_avatar)
                Glide.with(context as BaseActivity)
                        .load(blindBoxBean.icon)
                        .apply(options)
                        .into((holder.getView<View>(R.id.item_iv_blind_box_record) as ImageView))
                holder.getView<TextView>(R.id.item_tv_blind_box_record_name).text =  "送给*${blindBoxBean.nick_name}*"
                holder.getView<TextView>(R.id.item_tv_blind_box_content).text = "[${blindBoxBean.name}]"
                holder.getView<TextView>(R.id.item_tv_blind_box_record_time).text = blindBoxBean.create_time
            }
        }
        rv_blind_box_record!!.adapter = mCommonAdapter
        container!!.setRefreshListener(this)
        container!!.setCanRefresh(true)
        container!!.setCanLoadMore(false)
        initObserver()
    }

    private fun initObserver() {
        blindBoxRecordVM.blindBoxRecord(blindRecordType)
        blindBoxRecordVM.blindBoxRecordLiveData.observe(this, Observer {
            container.finishRefresh()
            container.finishLoadMore()
            if (it?.record_list.isNullOrEmpty()) {
                container.showView(ViewStatus.EMPTY_STATUS)
                container.setCanLoadMore(false)
                return@Observer
            }
            it?.record_list?.let { it1 ->
                mCommonAdapter?.update(it1) }
            container.showView(ViewStatus.CONTENT_STATUS)
        })
    }

    override fun loadMore() {
    }

    override fun refresh() {
        blindBoxRecordVM.blindBoxRecord(blindRecordType)
    }

    override fun onUserInvisible() {
    }


    override fun onFirstUserVisible() {
    }

    override fun notifyByThemeChanged() {
    }

    override fun onUserVisible() {
    }

    override fun getLoadingTargetView() = null


}