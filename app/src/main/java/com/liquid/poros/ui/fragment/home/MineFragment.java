package com.liquid.poros.ui.fragment.home;

import android.content.Intent;
import android.graphics.Rect;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.liquid.base.adapter.CommonAdapter;
import com.liquid.base.adapter.CommonViewHolder;
import com.liquid.base.event.EventCenter;
import com.liquid.poros.PorosApplication;
import com.liquid.poros.R;
import com.liquid.poros.analytics.utils.MultiTracker;
import com.liquid.poros.base.BaseFragment;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.contract.mine.MineContract;
import com.liquid.poros.contract.mine.MinePresenter;
import com.liquid.poros.entity.EditGameBean;
import com.liquid.poros.entity.NormalGame;
import com.liquid.poros.entity.PorosUserInfo;
import com.liquid.poros.entity.SettingEditGameBean;
import com.liquid.poros.entity.UserCenterInfo;
import com.liquid.poros.entity.UserCoinInfo;
import com.liquid.poros.entity.UserLevelInfo;
import com.liquid.poros.entity.UserPhotoInfo;
import com.liquid.poros.helper.JinquanResourceHelper;
import com.liquid.poros.ui.activity.account.RechargeActivity;
import com.liquid.poros.ui.activity.user.EditOtherGameActivity;
import com.liquid.poros.ui.activity.user.PhotoManageActivity;
import com.liquid.poros.ui.activity.user.UserInfoSettingActivity;
import com.liquid.poros.ui.fragment.dialog.mine.UserLevelDescDialogFragment;
import com.liquid.poros.ui.fragment.dialog.user.EditGameDialogFragment;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.poros.utils.ScreenUtil;
import com.liquid.poros.utils.StringUtil;
import com.liquid.poros.utils.ViewUtils;
import com.liquid.poros.utils.glide.GlideEngine;
import com.liquid.poros.widgets.ObservableScrollView;
import com.liquid.poros.widgets.WrapContentGridLayoutManager;
import com.liquid.porostwo.business.router.Router;
import com.liquid.porostwo.business.setting.SettingActivity;
import com.liquid.stat.boxtracker.util.LtNetworkUtil;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.entity.LocalMedia;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.OnClick;

import static com.liquid.poros.ui.activity.user.UserCenterActivity.SAVE_INFO_SUCCESS;

/**
 * 4Tab之一 我的页面
 */
public class MineFragment extends BaseFragment implements MineContract.View, ObservableScrollView.ScrollViewListener {
    @BindView(R.id.ll_content)
    LinearLayout ll_content;
    @BindView(R.id.cb_switch_theme_mode)
    CheckBox cb_switch_theme_mode;
    @BindView(R.id.iv_user_head)
    ImageView iv_user_head;
    @BindView(R.id.tv_user_name)
    TextView tv_user_name;
    @BindView(R.id.tv_user_id)
    TextView tv_user_id;
    @BindView(R.id.tv_user_age)
    TextView tv_user_age;
    @BindView(R.id.tv_user_charm)
    TextView tv_user_charm;
    @BindView(R.id.tv_recharge_coin)
    TextView tv_recharge_coin;
    //设置主题
    @BindView(R.id.rl_container)
    View rl_container;
    @BindView(R.id.rl_head)
    View rl_head;
    @BindView(R.id.iv_user_bg)
    ImageView iv_user_bg;
    @BindView(R.id.iv_recharge_arrow)
    ImageView iv_recharge_arrow;
    @BindView(R.id.tv_want_game)
    TextView tv_want_game;
    @BindView(R.id.tv_user_pic)
    TextView tv_user_pic;
    //理想Cp
    @BindView(R.id.tv_user_cp_content)
    TextView tv_user_cp_content;
    @BindView(R.id.tv_cp_want_tip)
    TextView tv_cp_want_tip;
    //录音
    @BindView(R.id.ll_audio)
    LinearLayout ll_audio;
    @BindView(R.id.iv_audio_play_status)
    ImageView iv_audio_play_status;
    @BindView(R.id.tv_audio_timer)
    TextView tv_audio_timer;
    @BindView(R.id.tv_audio_timer_tip)
    TextView tv_audio_timer_tip;

    //游戏信息
    @BindView(R.id.tv_game_want)
    TextView tv_game_want;
    @BindView(R.id.iv_game_want_arrow)
    ImageView iv_game_want_arrow;
    @BindView(R.id.ll_game_heping_content)
    LinearLayout ll_game_heping_content;
    @BindView(R.id.ll_game_wangzhe_content)
    LinearLayout ll_game_wangzhe_content;
    @BindView(R.id.tv_game_wangzhe_platform)
    TextView tv_game_wangzhe_platform;
    @BindView(R.id.tv_game_wangzhe_area)
    TextView tv_game_wangzhe_area;
    @BindView(R.id.tv_game_wangzhe_rank)
    TextView tv_game_wangzhe_rank;
    @BindView(R.id.tv_game_heping_platform)
    TextView tv_game_heping_platform;
    @BindView(R.id.tv_game_heping_area)
    TextView tv_game_heping_area;
    @BindView(R.id.tv_game_heping_rank)
    TextView tv_game_heping_rank;
    @BindView(R.id.iv_heping)
    ImageView iv_heping;
    @BindView(R.id.iv_wangzhe)
    ImageView iv_wangzhe;
    @BindView(R.id.iv_edit_heping)
    ImageView iv_edit_heping;
    @BindView(R.id.iv_edit_wangzhe)
    ImageView iv_edit_wangzhe;
    @BindView(R.id.tv_game_wangzhe_platform_title)
    TextView tv_game_wangzhe_platform_title;
    @BindView(R.id.tv_game_wangzhe_area_title)
    TextView tv_game_wangzhe_area_title;
    @BindView(R.id.tv_game_wangzhe_rank_title)
    TextView tv_game_wangzhe_rank_title;
    @BindView(R.id.tv_game_heping_platform_title)
    TextView tv_game_heping_platform_title;
    @BindView(R.id.tv_game_heping_area_title)
    TextView tv_game_heping_area_title;
    @BindView(R.id.tv_game_heping_rank_title)
    TextView tv_game_heping_rank_title;
    @BindView(R.id.tv_game_wangzhe_tip)
    TextView tv_game_wangzhe_tip;
    @BindView(R.id.tv_game_heping_tip)
    TextView tv_game_heping_tip;
    //个人相册
    @BindView(R.id.tv_edit_pic)
    TextView tv_edit_pic;
    @BindView(R.id.iv_edit_pic_arrow)
    ImageView iv_edit_pic_arrow;
    @BindView(R.id.tv_user_pics_seat)
    TextView tv_user_pics_seat;
    @BindView(R.id.rv_pics)
    RecyclerView rv_pics;
    @BindView(R.id.rl_user_pic_tip)
    RelativeLayout rl_user_pic_tip;
    @BindView(R.id.ll_title_layout)
    LinearLayout ll_title_layout;
    @BindView(R.id.osv_scroll_view)
    ObservableScrollView osv_scroll_view;
    @BindView(R.id.iv_setting)
    ImageView iv_setting;
    @BindView(R.id.iv_vip)
    ImageView iv_vip;
    @BindView(R.id.user_level_tv)
    TextView user_level_tv;
    private MediaPlayer mMediaPlayer;
    private Timer mTimer2;
    private TimerTask mTask2;
    private int mAudioDuration;
    private int mAudioTimer;
    private SettingEditGameBean mStaticGameBean;
    private String honor_platform;
    private String honor_zoom;
    private String honor_rank;
    private String peace_platform;
    private String peace_zoom;
    private String peace_rank;
    private boolean isReverseRank = false;//是否倒序
    private boolean isPause = false;//是否暂停
    private int imageHeight = 800;
    private MinePresenter mPresenter;
    private UserCoinInfo.Data userCoinInfos;
    private UserCenterInfo.Data.UserInfo mUserInfoBean;
    private UserCenterInfo.Data.UserInfo.GameInfoBean mGameBean;
    private CommonAdapter<UserPhotoInfo.Data.Photos> mCommonAdapter;
    private List<UserPhotoInfo.Data.Photos> mUserPhotoList = new ArrayList<>();

    public static MineFragment newInstance() {
        return new MineFragment();
    }

    @Override
    protected String getPageId() {
        return TrackConstants.PAGE_MINE;
    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.fragment_mine;
    }

    @Override
    protected void onFirstUserVisible() {

    }

    @Override
    protected void onUserVisible() {

    }

    @Override
    protected void onUserInvisible() {

    }

    @Override
    protected View getLoadingTargetView() {
        return null;
    }

    @Override
    protected void initViewsAndEvents(Bundle savedInstanceState) {
        cb_switch_theme_mode.setChecked(getThemeTag() == 1);
        cb_switch_theme_mode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                setThemeTag(0 - getThemeTag());
                loadingCurrentTheme();
                ((PorosApplication) getActivity().getApplication()).notifyByThemeChanged();
            }
        });
        mCommonAdapter = new CommonAdapter<UserPhotoInfo.Data.Photos>(getActivity(), R.layout.item_user_img) {
            @Override
            public void convert(CommonViewHolder holder, UserPhotoInfo.Data.Photos photos, int pos) {
                ImageView iv_image = holder.getView(R.id.iv_select);
                RequestOptions options = new RequestOptions()
                        .placeholder(R.mipmap.icon_default)
                        .error(R.mipmap.icon_default)
                        .transform(new CenterCrop(), new RoundedCorners(ScreenUtil.dip2px(6)));
                Glide.with(getActivity())
                        .load(photos.getPath())
                        .override(ScreenUtil.dip2px(78), ScreenUtil.dip2px(78))
                        .apply(options)
                        .into(iv_image);
                holder.getView(R.id.iv_video_cover).setVisibility(photos.getType() == 2 ? View.VISIBLE : View.GONE);
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (ViewUtils.isFastClick()) {
                            return;
                        }
                        if (mUserPhotoList.get(pos).getType() == 2) {
                            PictureSelector.create(getActivity()).themeStyle(R.style.picture_default_style).externalPictureVideo(mUserPhotoList.get(pos).getPath());
                        } else {
                            //点击图片预览
                            PictureSelector.create(getActivity())
                                    .themeStyle(R.style.picture_default_style)
                                    .isNotPreviewDownload(true)
                                    .isPreviewVideo(true)
                                    .imageEngine(GlideEngine.createGlideEngine())
                                    .openExternalPreview(pos, getSelectList(mUserPhotoList));
                        }
                        TrackPoint.photoManageClick();
                    }
                });
            }
        };
        rv_pics.setLayoutManager(new WrapContentGridLayoutManager(getActivity(), 4));
        rv_pics.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                outRect.bottom = ScreenUtil.dip2px(8);
            }
        });
        rv_pics.setNestedScrollingEnabled(false);
        rv_pics.setHasFixedSize(true);
        rv_pics.setFocusable(false);
        rv_pics.setAdapter(mCommonAdapter);
        osv_scroll_view.setScrollViewListener(this);
        mMediaPlayer = new MediaPlayer();
        mPresenter = new MinePresenter();
        mPresenter.attachView(this);
        mPresenter.getUserInfo(AccountUtil.getInstance().getUserId());
        mPresenter.getUserPhotos(AccountUtil.getInstance().getUserId());

        if(AccountUtil.getInstance().getUserInfo() != null){
            iv_vip.setVisibility(Constants.is_sounds_vip?View.VISIBLE:View.GONE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.getUserCoin();
    }

    @Override
    public void fetchUserCoinSuccess(UserCoinInfo userCoinInfo) {
        if (userCoinInfo == null || userCoinInfo.getData() == null) {
            return;
        }
        userCoinInfos = userCoinInfo.getData();
        tv_recharge_coin.setText(userCoinInfos.getCoin());
    }

    @Override
    public void onUserInfoFetch(UserCenterInfo userCenterInfo) {
        if (userCenterInfo == null || userCenterInfo.getData() == null || userCenterInfo.getData().getUser_info() == null) {
            return;
        }
        mUserInfoBean = userCenterInfo.getData().getUser_info();
        RequestOptions options = new RequestOptions()
                .placeholder(R.mipmap.img_default_avatar)
                .override(ScreenUtil.dip2px(98), ScreenUtil.dip2px(98))
                .error(R.mipmap.img_default_avatar);
        Glide.with(this)
                .load(mUserInfoBean.getAvatar_url())
                .apply(options)
                .into(iv_user_head);
        if (TextUtils.isEmpty(mUserInfoBean.getCover_img()) || mUserInfoBean.getCover_img().equals("null")) {
            RequestOptions roomOptions = new RequestOptions()
                    .placeholder(R.mipmap.icon_default)
                    .error(R.mipmap.icon_default);
            Glide.with(getActivity())
                    .load(mUserInfoBean.getAvatar_url())
                    .apply(roomOptions)
                    .into(iv_user_bg);
        } else {
            Glide.with(getActivity())
                    .load(mUserInfoBean.getCover_img())
                    .into(iv_user_bg);
        }
        //昵称
        tv_user_name.setText(!TextUtils.isEmpty(mUserInfoBean.getNick_name()) ? mUserInfoBean.getNick_name() : "");
        //id
        tv_user_id.setText(mUserInfoBean.getUser_id());
        //年龄
        tv_user_age.setBackgroundResource("1".equals(mUserInfoBean.getGender()) ? R.drawable.bg_color_00aaff_16 : R.drawable.bg_color_ff75a3_16);
        Drawable ageDrawable = getResources().getDrawable("1".equals(mUserInfoBean.getGender()) ? R.mipmap.icon_male : R.mipmap.icon_female);
        ageDrawable.setBounds(0, 0, ageDrawable.getMinimumWidth(), ageDrawable.getMinimumHeight());
        tv_user_age.setCompoundDrawables(ageDrawable, null, null, null);
        tv_user_age.setCompoundDrawablePadding(3);
        tv_user_age.setText(mUserInfoBean.getAge());
        //魅力值
        int[] charmValuesDrawable = {R.drawable.ic_charm_00,R.drawable.ic_charm_20,R.drawable.ic_charm_40,R.drawable.ic_charm_60,R.drawable.ic_charm_80,R.drawable.ic_charm_100};
        tv_user_charm.setBackgroundResource(charmValuesDrawable[mUserInfoBean.getUser_charm()/20]);
        tv_user_charm.setText(Html.fromHtml(String.format("魅 • <b>%d</b>", mUserInfoBean.getUser_charm())));

        //录音
        if (TextUtils.isEmpty(mUserInfoBean.getVoice_intro())) {
            tv_audio_timer_tip.setVisibility(View.VISIBLE);
            tv_audio_timer.setVisibility(View.GONE);
            iv_audio_play_status.setVisibility(View.GONE);
        } else {
            tv_audio_timer_tip.setVisibility(View.GONE);
            tv_audio_timer.setVisibility(View.VISIBLE);
            iv_audio_play_status.setVisibility(View.VISIBLE);
        }
        if (!TextUtils.isEmpty(mUserInfoBean.getVoice_intro())) {
            try {
                mMediaPlayer.reset();
                mMediaPlayer.setDataSource(mUserInfoBean.getVoice_intro());
                mMediaPlayer.prepare();
                mAudioDuration = mMediaPlayer.getDuration() / 1000;
                mAudioTimer = mAudioDuration;
                setAudioDuration();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        //理想CP
        if (!TextUtils.isEmpty(mUserInfoBean.getCouple_desc())) {
            tv_user_cp_content.setVisibility(View.VISIBLE);
            tv_cp_want_tip.setVisibility(View.GONE);
            tv_user_cp_content.setText(StringUtil.replaceBlank(mUserInfoBean.getCouple_desc()));
        } else {
            tv_user_cp_content.setVisibility(View.GONE);
            tv_cp_want_tip.setVisibility(View.VISIBLE);
        }
        mStaticGameBean = userCenterInfo.getStaticGameBean();
        mGameBean = new Gson().fromJson(new Gson().toJson(mUserInfoBean.getGame_info()), UserCenterInfo.Data.UserInfo.GameInfoBean.class);
        checkGameData();
        //和平精英
        setHpjyParams(userCenterInfo);
        //王者荣耀
        setWzryParams(userCenterInfo);

        if(userCenterInfo.getData() != null && userCenterInfo.getData().getUser_info() != null){
            int user_level = userCenterInfo.getData().getUser_info().getUser_level();
            user_level_tv.setVisibility(View.VISIBLE);
            user_level_tv.setText("Lv." + user_level);
            AccountUtil.getInstance().getUserInfo().setUser_level(user_level);
        }
    }

    @Override
    public void onUserPhotosFetch(UserPhotoInfo userPhotoInfo) {
        mUserPhotoList.clear();
        mUserPhotoList.addAll(userPhotoInfo.getData().getPhotos());
        rv_pics.setVisibility(mUserPhotoList.size() != 0 ? View.VISIBLE : View.GONE);
        rl_user_pic_tip.setVisibility(mUserPhotoList.size() != 0 ? View.GONE : View.VISIBLE);
        mCommonAdapter.update(mUserPhotoList);
    }

    @Override
    public void uploadUserInfoSuccess(PorosUserInfo userInfo) {

    }

    @Override
    public void onUserLevelFetch(UserLevelInfo userLevelInfo) {
        if (userLevelInfo.getData() == null) {
            return;
        }
        AccountUtil.getInstance().getUserInfo().setUser_level(userLevelInfo.getData().getUser_level());
        user_level_tv.setText("Lv." + userLevelInfo.getData().getUser_level());
    }

    private List<LocalMedia> getSelectList(List<UserPhotoInfo.Data.Photos> data) {
        List<LocalMedia> localMedia = new ArrayList<>();
        for (UserPhotoInfo.Data.Photos photos : data) {
            localMedia.add(new LocalMedia(photos.getPath(), 0, 0, String.valueOf(photos.getType())));
        }
        return localMedia;
    }

    @OnClick({R.id.tv_edit_user_info, R.id.tv_audio_timer_tip, R.id.rl_recharge, R.id.iv_setting, R.id.ll_audio, R.id.iv_edit_heping, R.id.iv_edit_wangzhe, R.id.tv_game_want, R.id.tv_edit_pic, R.id.user_level_container})
    public void onClick(View view) {
        if (ViewUtils.isFastClick() || !LtNetworkUtil.isNetworkAvailable(getActivity())) {
            if (!LtNetworkUtil.isNetworkAvailable(getActivity())) {
                showMessage(getResources().getString(R.string.net_error));
            }
            return;
        }
        switch (view.getId()) {
            case R.id.tv_audio_timer_tip:
            case R.id.tv_edit_user_info:
                if (mUserInfoBean != null && mStaticGameBean != null) {
                    Intent intent = new Intent(getActivity(), UserInfoSettingActivity.class);
                    intent.putExtra(Constants.USER_ID, mUserInfoBean.getUser_id());
                    intent.putExtra(Constants.USER_STATIC_GAME, new Gson().toJson(mStaticGameBean));
                    intent.putExtra(Constants.USER_GAME, new Gson().toJson(mUserInfoBean.getGame_info()));
                    intent.putExtra(Constants.USER_GAME_MATCH_CONTROL, mUserInfoBean.isMatch_control());
                    startActivityForResult(intent, SAVE_INFO_SUCCESS);
                }
                break;
            case R.id.rl_recharge:
                //充值
                readyGo(RechargeActivity.class);
                TrackPoint.rechargeClick(AccountUtil.getInstance().getUserId(), userCoinInfos == null ? "" : userCoinInfos.getCoin());

                break;
            case R.id.iv_setting:
//                readyGo(SettingActivity.class);
                Router.INSTANCE.toSettingActivity();
                break;
            case R.id.ll_audio:
                //语音
                if (mMediaPlayer.isPlaying() && !isPause) {
                    isPause = true;
                    endAudio();
                } else {
                    isPause = false;
                    startAudio();
                }
                TrackPoint.mineVoiceClick();

                break;
            case R.id.iv_edit_heping:
                if (mStaticGameBean != null && mStaticGameBean.peace_elite != null) {
                    ArrayList<EditGameBean> peace_elite = new ArrayList<>();
                    peace_elite.add(mStaticGameBean.peace_elite.platform);
                    peace_elite.add(mStaticGameBean.peace_elite.zone);
                    peace_elite.add(mStaticGameBean.peace_elite.rank);
                    EditGameDialogFragment.newInstance()
                            .setTitle(getString(R.string.test_submit_info_game_hepingjingying))
                            .setEditGameBeans(peace_elite)
                            .setPeaceBean(mGameBean.getHpjy())
                            .show(getSupportFragmentManager(), EditGameDialogFragment.class.getSimpleName());
                }
                break;
            case R.id.iv_edit_wangzhe:
                if (mStaticGameBean != null && mStaticGameBean.honor_of_kings != null) {
                    ArrayList<EditGameBean> honor_of_kings = new ArrayList<>();
                    honor_of_kings.add(mStaticGameBean.honor_of_kings.platform);
                    honor_of_kings.add(mStaticGameBean.honor_of_kings.zone);
                    honor_of_kings.add(mStaticGameBean.honor_of_kings.rank);
                    EditGameDialogFragment.newInstance()
                            .setTitle(getString(R.string.test_submit_info_game_wangzhe))
                            .setEditGameBeans(honor_of_kings)
                            .setHonorBean(mGameBean.getWzry())
                            .show(getSupportFragmentManager(), EditGameDialogFragment.class.getSimpleName());
                }
                break;
            case R.id.tv_game_want:
                if (mStaticGameBean != null && mStaticGameBean.other != null) {
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.USER_OTHER_GAME, new Gson().toJson(mStaticGameBean.other.list));
                    readyGo(EditOtherGameActivity.class, bundle);
                }
                break;
            case R.id.tv_edit_pic:
                //相册管理
                readyGo(PhotoManageActivity.class);
                break;
            case R.id.user_level_container:
                UserLevelDescDialogFragment.newInstance("s").show(getSupportFragmentManager(), UserLevelDescDialogFragment.class.getSimpleName());
                break;
        }
    }

    @Override
    protected void onEventComing(EventCenter eventCenter) {
        super.onEventComing(eventCenter);
        switch (eventCenter.getEventCode()) {
            case Constants.EVENT_CODE_COIN_CHANGE:
                if (mPresenter != null) {
                    mPresenter.getUserCoin();
                    mPresenter.getUserLevel(AccountUtil.getInstance().getUserId());
                }
                break;
            case Constants.EVENT_CODE_UPDATE_PHOTO:
                if (mPresenter != null) {
                    mPresenter.getUserPhotos(AccountUtil.getInstance().getUserId());
                }
                break;
            case Constants.EVENT_CODE_UPDATE_USER_INFO:
                if (mPresenter != null) {
                    mPresenter.getUserInfo(AccountUtil.getInstance().getUserId());
                }
                break;
            case Constants.EVENT_CODE_UPDATE_GAME_INFO_HONOR:
                HashMap<String, Object> honorMap = (HashMap<String, Object>) eventCenter.getData();
                List<EditGameBean> honorData = (List<EditGameBean>) honorMap.get("static");
                mStaticGameBean.honor_of_kings.platform = honorData.get(0);
                mStaticGameBean.honor_of_kings.zone = honorData.get(1);
                mStaticGameBean.honor_of_kings.rank = honorData.get(2);
                UserCenterInfo.Data.UserInfo.GameInfoBean.WzryBean data = (UserCenterInfo.Data.UserInfo.GameInfoBean.WzryBean) honorMap.get("data");
                mGameBean.setWzry(data);
                isReverseRank = true;
                checkGameData();
                setGameData();
                mPresenter.updateUserInfo(mGameBean);
                break;
            case Constants.EVENT_CODE_UPDATE_GAME_INFO_PEACE:
                HashMap<String, Object> peaceMap = (HashMap<String, Object>) eventCenter.getData();
                List<EditGameBean> peaceData = (List<EditGameBean>) peaceMap.get("static");
                mStaticGameBean.peace_elite.platform = peaceData.get(0);
                mStaticGameBean.peace_elite.zone = peaceData.get(1);
                mStaticGameBean.peace_elite.rank = peaceData.get(2);
                UserCenterInfo.Data.UserInfo.GameInfoBean.HpjyBean peaceBean = (UserCenterInfo.Data.UserInfo.GameInfoBean.HpjyBean) peaceMap.get("data");
                mGameBean.setHpjy(peaceBean);
                isReverseRank = true;
                checkGameData();
                setGameData();
                mPresenter.updateUserInfo(mGameBean);
                break;
            case Constants.EVENT_CODE_UPDATE_GAME_INFO_OTHER:
                List<EditGameBean.Item> otherData = (List<EditGameBean.Item>) eventCenter.getData();
                mStaticGameBean.other.list = (ArrayList<EditGameBean.Item>) otherData;
                mGameBean.getOther().clear();
                for (EditGameBean.Item item : mStaticGameBean.other.list) {
                    if (item.select) {
                        NormalGame normalGame = new NormalGame();
                        normalGame.set_id(Integer.parseInt(item.id));
                        normalGame.setIcon(item.image_url);
                        normalGame.setName(item.name);
                        mGameBean.getOther().add(normalGame);
                    }
                }
                mPresenter.updateUserInfo(mGameBean);
                break;
            case Constants.EVENT_CODE_BUY_GIFT_PACKAGE_SUCCESS:
            case Constants.EVENT_CODE_UPDATE_GIFT_PACKAGE_STATUS:
                AccountUtil.getInstance().getUserInfo().setIs_vip(true);
                iv_vip.setVisibility(View.VISIBLE);
                break;
            case Constants.EVENT_CODE_BUY_ONE_CENT_VIP:
                Constants.is_sounds_vip = true;
                iv_vip.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void setWzryParams(UserCenterInfo userCenterInfo) {
        UserCenterInfo.Data.UserInfo.GameInfoBean userInfo = userCenterInfo.getData().getUser_info().getGame_info();
        UserCenterInfo.WZRYDATA wzrydata = userCenterInfo.getWzrydata();
        if (userInfo != null && wzrydata != null) {
            UserCenterInfo.Data.UserInfo.GameInfoBean.WzryBean wzryBean = userInfo.getWzry();
            if (wzryBean != null) {
                StringBuilder wzry_platfrom = new StringBuilder();
                StringBuilder wzry_rank = new StringBuilder();
                StringBuilder wzry_zone = new StringBuilder();
                //平台
                List<Integer> platforms = wzryBean.getPlatform();
                ArrayList<Map<String, String>> wzry_platfrom_list = wzrydata.getWzry_platform_list();
                for (int i = 0; i < platforms.size(); i++) {
                    int num = platforms.get(i);

                    for (int j = 0; j < wzry_platfrom_list.size(); j++) {
                        Map<String, String> map = wzry_platfrom_list.get(j);
                        if (map.containsKey(num + "")) {
                            String s = map.get(num + "");
                            if (com.liquid.base.tools.StringUtil.isEmpty(wzry_platfrom.toString())) {
                                wzry_platfrom.append(s);
                            } else {
                                wzry_platfrom.append(" | ").append(s);
                            }
                        }
                    }
                }
                //排行
                List<Integer> ranks = wzryBean.getRank();
                ArrayList<Map<String, String>> wzry_rank_list = wzrydata.getWzry_rank_list();
                for (int i = 0; i < ranks.size(); i++) {
                    int num = ranks.get(i);

                    for (int j = 0; j < wzry_rank_list.size(); j++) {
                        Map<String, String> map = wzry_rank_list.get(j);
                        if (map.containsKey(num + "")) {
                            String s = map.get(num + "");
                            if (com.liquid.base.tools.StringUtil.isEmpty(wzry_rank.toString())) {
                                wzry_rank.append(s);
                            } else {
                                wzry_rank.append(" | ").append(s);
                            }
                        }
                    }
                }
                //大区
                List<Integer> zones = wzryBean.getZone();
                ArrayList<Map<String, String>> wzry_area_list = wzrydata.getWzry_area_list();
                for (int i = 0; i < zones.size(); i++) {
                    int num = zones.get(i);

                    for (int j = 0; j < wzry_area_list.size(); j++) {
                        Map<String, String> map = wzry_area_list.get(j);
                        if (map.containsKey(num + "")) {
                            String s = map.get(num + "");
                            if (com.liquid.base.tools.StringUtil.isEmpty(wzry_zone.toString())) {
                                wzry_zone.append(s);
                            } else {
                                wzry_zone.append(" | ").append(s);
                            }
                        }
                    }
                }

                tv_game_wangzhe_platform.setText(wzry_platfrom.toString());
                tv_game_wangzhe_rank.setText(wzry_rank.toString());
                tv_game_wangzhe_area.setText(wzry_zone.toString());
                if (TextUtils.isEmpty(wzry_platfrom.toString()) && TextUtils.isEmpty(wzry_rank.toString()) && TextUtils.isEmpty(wzry_zone.toString())) {
                    ll_game_wangzhe_content.setVisibility(View.GONE);
                    tv_game_wangzhe_tip.setVisibility(View.VISIBLE);
                } else {
                    ll_game_wangzhe_content.setVisibility(View.VISIBLE);
                    tv_game_wangzhe_tip.setVisibility(View.GONE);
                }
                RequestOptions options = new RequestOptions()
                        .placeholder(R.mipmap.icon_wangzherongyao)
                        .error(R.mipmap.icon_wangzherongyao);
                Glide.with(getActivity()).load(wzrydata.getIcon()).apply(options).apply(RequestOptions.bitmapTransform(new RoundedCorners(12))).into(iv_wangzhe);
            }
        }
    }

    private void setHpjyParams(UserCenterInfo userCenterInfo) {
        UserCenterInfo.Data.UserInfo.GameInfoBean userInfo = userCenterInfo.getData().getUser_info().getGame_info();
        UserCenterInfo.HPJYDATA hpjydata = userCenterInfo.getHpjydata();
        if (userInfo != null && hpjydata != null) {
            UserCenterInfo.Data.UserInfo.GameInfoBean.HpjyBean hpjyBean = userInfo.getHpjy();
            if (hpjyBean != null) {
                StringBuilder hpjy_platfrom = new StringBuilder();
                StringBuilder hpjy_rank = new StringBuilder();
                StringBuilder hpjy_zone = new StringBuilder();
                //平台
                List<Integer> platforms = hpjyBean.getPlatform();
                ArrayList<Map<String, String>> Hpjy_platfrom_list = hpjydata.getHpjy_platform_list();
                for (int i = 0; i < platforms.size(); i++) {
                    int num = platforms.get(i);

                    for (int j = 0; j < Hpjy_platfrom_list.size(); j++) {
                        Map<String, String> map = Hpjy_platfrom_list.get(j);
                        if (map.containsKey(num + "")) {
                            String s = map.get(num + "");
                            if (com.liquid.base.tools.StringUtil.isEmpty(hpjy_platfrom.toString())) {
                                hpjy_platfrom.append(s);
                            } else {
                                hpjy_platfrom.append(" | ").append(s);
                            }
                        }
                    }
                }

                //排行
                List<Integer> ranks = hpjyBean.getRank();
                ArrayList<Map<String, String>> hpjy_rank_list = hpjydata.getHpjy_rank_list();
                for (int i = 0; i < ranks.size(); i++) {
                    int num = ranks.get(i);

                    for (int j = 0; j < hpjy_rank_list.size(); j++) {
                        Map<String, String> map = hpjy_rank_list.get(j);
                        if (map.containsKey(num + "")) {
                            String s = map.get(num + "");
                            if (com.liquid.base.tools.StringUtil.isEmpty(hpjy_rank.toString())) {
                                hpjy_rank.append(s);
                            } else {
                                hpjy_rank.append(" | ").append(s);
                            }
                        }
                    }
                }

                //大区
                List<Integer> zones = hpjyBean.getZone();
                ArrayList<Map<String, String>> wzry_area_list = hpjydata.getHpjy_area_list();
                for (int i = 0; i < zones.size(); i++) {
                    int num = zones.get(i);

                    for (int j = 0; j < wzry_area_list.size(); j++) {
                        Map<String, String> map = wzry_area_list.get(j);
                        if (map.containsKey(num + "")) {
                            String s = map.get(num + "");
                            if (com.liquid.base.tools.StringUtil.isEmpty(hpjy_zone.toString())) {
                                hpjy_zone.append(s);
                            } else {
                                hpjy_zone.append(" | ").append(s);
                            }
                        }
                    }
                }
                tv_game_heping_platform.setText(hpjy_platfrom.toString());
                tv_game_heping_rank.setText(hpjy_rank.toString());
                tv_game_heping_area.setText(hpjy_zone.toString());
                if (TextUtils.isEmpty(hpjy_platfrom.toString()) && TextUtils.isEmpty(hpjy_rank.toString()) && TextUtils.isEmpty(hpjy_zone.toString())) {
                    ll_game_heping_content.setVisibility(View.GONE);
                    tv_game_heping_tip.setVisibility(View.VISIBLE);
                } else {
                    ll_game_heping_content.setVisibility(View.VISIBLE);
                    tv_game_heping_tip.setVisibility(View.GONE);
                }
                RequestOptions options = new RequestOptions()
                        .placeholder(R.mipmap.icon_hepingjingying)
                        .error(R.mipmap.icon_hepingjingying);
                Glide.with(getActivity()).load(hpjydata.getIcon()).apply(options).apply(RequestOptions.bitmapTransform(new RoundedCorners(12))).into(iv_heping);
            }

        }
    }

    private void checkGameData() {
        if (mGameBean != null && mStaticGameBean != null) {
            peace_platform = setSelect(mGameBean.getHpjy().getPlatform(), mStaticGameBean.peace_elite.platform);
            peace_zoom = setSelect(mGameBean.getHpjy().getZone(), mStaticGameBean.peace_elite.zone);
            List<Integer> hpRank = mGameBean.getHpjy().getRank();
            if (isReverseRank) {
                Collections.reverse(hpRank);
            }
            peace_rank = setSelect(hpRank, mStaticGameBean.peace_elite.rank);

            honor_platform = setSelect(mGameBean.getWzry().getPlatform(), mStaticGameBean.honor_of_kings.platform);
            honor_zoom = setSelect(mGameBean.getWzry().getZone(), mStaticGameBean.honor_of_kings.zone);
            List<Integer> wzRank = mGameBean.getWzry().getRank();
            if (isReverseRank) {
                Collections.reverse(wzRank);
            }
            honor_rank = setSelect(wzRank, mStaticGameBean.honor_of_kings.rank);
            List<NormalGame> other = mGameBean.getOther();
            if (other != null && other.size() > 0) {
                for (int i = 0; i < other.size(); i++) {
                    try {
                        mStaticGameBean.other.list.get(other.get(i).get_id() - 1).select = true;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private String setSelect(List<Integer> item, EditGameBean data) {
        StringBuilder stringBuilder = new StringBuilder();
        if (item != null && item.size() > 0) {
            for (int i = 0; i < item.size(); i++) {
                try {
                    data.items.get(item.get(i) - 1).select = true;
                    stringBuilder.append(data.items.get(item.get(i) - 1).name);
                    if (i != item.size() - 1) {
                        stringBuilder.append(" | ");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return stringBuilder.toString();
    }

    private void setGameData() {
        if (mGameBean.getHpjy() != null && mGameBean.getHpjy().getPlatform().size() > 0) {
            ll_game_heping_content.setVisibility(View.VISIBLE);
            tv_game_heping_tip.setVisibility(View.GONE);
            tv_game_heping_platform.setText(peace_platform);
            tv_game_heping_area.setText(peace_zoom);
            tv_game_heping_rank.setText(peace_rank);
        } else {
            ll_game_heping_content.setVisibility(View.GONE);
            tv_game_heping_tip.setVisibility(View.VISIBLE);
        }
        if (mStaticGameBean.peace_elite != null) {
            RequestOptions options = new RequestOptions()
                    .placeholder(R.mipmap.icon_hepingjingying)
                    .error(R.mipmap.icon_hepingjingying);
            Glide.with(this).load(mStaticGameBean.peace_elite.icon).apply(options).apply(RequestOptions.bitmapTransform(new RoundedCorners(12))).into(iv_heping);
        }
        if (mGameBean.getWzry() != null && mGameBean.getWzry().getPlatform().size() > 0) {
            ll_game_wangzhe_content.setVisibility(View.VISIBLE);
            tv_game_wangzhe_tip.setVisibility(View.GONE);
            tv_game_wangzhe_platform.setText(honor_platform);
            tv_game_wangzhe_area.setText(honor_zoom);
            tv_game_wangzhe_rank.setText(honor_rank);
        } else {
            ll_game_wangzhe_content.setVisibility(View.GONE);
            tv_game_wangzhe_tip.setVisibility(View.VISIBLE);
        }
        if (mStaticGameBean.honor_of_kings != null) {
            RequestOptions options = new RequestOptions()
                    .placeholder(R.mipmap.icon_wangzherongyao)
                    .error(R.mipmap.icon_wangzherongyao);
            Glide.with(this).load(mStaticGameBean.honor_of_kings.icon).apply(options).apply(RequestOptions.bitmapTransform(new RoundedCorners(12))).into(iv_wangzhe);
        }
    }

    /**
     * 播放语音
     */
    private void startAudio() {
        if (getActivity().isDestroyed() || getActivity().isFinishing()) {
            return;
        }
        iv_audio_play_status.setBackgroundResource(R.drawable.anim_user_audio_list);
        AnimationDrawable animation = (AnimationDrawable) iv_audio_play_status.getBackground();
        animation.start();
        if (mUserInfoBean != null && !TextUtils.isEmpty(mUserInfoBean.getVoice_intro()) && mMediaPlayer != null) {
            try {
                mMediaPlayer.reset();
                mMediaPlayer.setDataSource(mUserInfoBean.getVoice_intro());
                mMediaPlayer.prepare();
                mMediaPlayer.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (mTimer2 == null && mTask2 == null) {
            mTimer2 = new Timer();
            mTask2 = new TimerTask() {
                @Override
                public void run() {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mAudioTimer--;
                            if (mAudioTimer <= 0) {
                                mAudioTimer = mAudioDuration;
                                endAudio();
                            } else {
                                setAudioDuration();
                            }
                        }
                    });
                }
            };
            mTimer2.schedule(mTask2, 0, 1000);
        }
    }

    private void releaseTimer() {
        if (mTask2 != null) {
            mTask2.cancel();
            mTask2 = null;
        }
        if (mTimer2 != null) {
            mTimer2.cancel();
            mTimer2 = null;
        }
    }

    /**
     * 暂停语音
     */
    private void endAudio() {
        if (getActivity().isDestroyed() || getActivity().isFinishing()) {
            return;
        }
        AnimationDrawable animation = (AnimationDrawable) iv_audio_play_status.getBackground();
        animation.selectDrawable(0);
        animation.stop();
        mAudioTimer = mAudioDuration;
        setAudioDuration();
        releaseTimer();
    }

    private void setAudioDuration() {
        if (getActivity().isDestroyed() || getActivity().isFinishing()) {
            return;
        }
        tv_audio_timer.setText(mAudioTimer + "s");
    }

    @Override
    public void notifyByThemeChanged() {
        if (isDetached()) return;
        JinquanResourceHelper.getInstance(getActivity()).setBackgroundColorByAttr(rl_container, R.attr.custom_attr_app_bg);
        JinquanResourceHelper.getInstance(getActivity()).setBackgroundResourceByAttr(ll_content, R.attr.bg_mine);
        JinquanResourceHelper.getInstance(getActivity()).setTextColorByAttr(tv_want_game, R.attr.custom_attr_main_text_color);
        JinquanResourceHelper.getInstance(getActivity()).setTextColorByAttr(tv_user_pic, R.attr.custom_attr_main_text_color);
        JinquanResourceHelper.getInstance(getActivity()).setBackgroundResourceByAttr(iv_edit_heping, R.attr.bg_mine_edit_game);
        JinquanResourceHelper.getInstance(getActivity()).setBackgroundResourceByAttr(iv_edit_wangzhe, R.attr.bg_mine_edit_game);
        JinquanResourceHelper.getInstance(getActivity()).setTextColorByAttr(tv_game_heping_platform, R.attr.color_test_game_content);
        JinquanResourceHelper.getInstance(getActivity()).setTextColorByAttr(tv_game_heping_area, R.attr.color_test_game_content);
        JinquanResourceHelper.getInstance(getActivity()).setTextColorByAttr(tv_game_heping_rank, R.attr.color_test_game_content);
        JinquanResourceHelper.getInstance(getActivity()).setTextColorByAttr(tv_game_wangzhe_platform, R.attr.color_test_game_content);
        JinquanResourceHelper.getInstance(getActivity()).setTextColorByAttr(tv_game_wangzhe_area, R.attr.color_test_game_content);
        JinquanResourceHelper.getInstance(getActivity()).setTextColorByAttr(tv_game_wangzhe_rank, R.attr.color_test_game_content);
        JinquanResourceHelper.getInstance(getActivity()).setTextColorByAttr(tv_game_heping_platform_title, R.attr.color_test_game_title);
        JinquanResourceHelper.getInstance(getActivity()).setTextColorByAttr(tv_game_heping_area_title, R.attr.color_test_game_title);
        JinquanResourceHelper.getInstance(getActivity()).setTextColorByAttr(tv_game_heping_rank_title, R.attr.color_test_game_title);
        JinquanResourceHelper.getInstance(getActivity()).setTextColorByAttr(tv_game_wangzhe_platform_title, R.attr.color_test_game_title);
        JinquanResourceHelper.getInstance(getActivity()).setTextColorByAttr(tv_game_wangzhe_area_title, R.attr.color_test_game_title);
        JinquanResourceHelper.getInstance(getActivity()).setTextColorByAttr(tv_game_wangzhe_rank_title, R.attr.color_test_game_title);
        JinquanResourceHelper.getInstance(getActivity()).setTextColorByAttr(tv_game_heping_tip, R.attr.color_test_game_title);
        JinquanResourceHelper.getInstance(getActivity()).setTextColorByAttr(tv_game_wangzhe_tip, R.attr.color_test_game_title);
        JinquanResourceHelper.getInstance(getActivity()).setTextColorByAttr(tv_game_want, R.attr.custom_attr_flow_item_text_color);
        JinquanResourceHelper.getInstance(getActivity()).setBackgroundResourceByAttr(iv_game_want_arrow, R.attr.bg_right_more);
        JinquanResourceHelper.getInstance(getActivity()).setTextColorByAttr(tv_edit_pic, R.attr.custom_attr_flow_item_text_color);
        JinquanResourceHelper.getInstance(getActivity()).setBackgroundResourceByAttr(iv_edit_pic_arrow, R.attr.bg_right_more);
        JinquanResourceHelper.getInstance(getActivity()).setTextColorByAttr(tv_user_pics_seat, R.attr.custom_attr_flow_item_text_color);
        if (mIsScroll) {
            JinquanResourceHelper.getInstance(getActivity()).setBackgroundResourceByAttr(ll_title_layout, R.attr.custom_attr_app_bg);
            JinquanResourceHelper.getInstance(getActivity()).setImageResourceByAttr(iv_setting, R.attr.bg_mine_setting);
        } else {
            if (ll_title_layout != null && iv_setting != null) {
                ll_title_layout.setBackgroundColor(getResources().getColor(R.color.transparent));
                iv_setting.setImageResource(R.mipmap.icon_mine_setting_night);
            }
        }

    }

    @Override
    public void notifyNetworkChanged(boolean isConnected) {

    }

    private boolean mIsScroll = false;

    @Override
    public void onScrollChanged(ObservableScrollView scrollView, int x, int y, int oldx, int oldy) {
        if (y > imageHeight) {
            JinquanResourceHelper.getInstance(getActivity()).setBackgroundResourceByAttr(ll_title_layout, R.attr.custom_attr_app_bg);
            JinquanResourceHelper.getInstance(getActivity()).setImageResourceByAttr(iv_setting, R.attr.bg_mine_setting);
            mIsScroll = true;
        } else {
            iv_setting.setImageResource(R.mipmap.icon_mine_setting_night);
            ll_title_layout.setBackgroundColor(getResources().getColor(R.color.transparent));
            mIsScroll = false;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
        releaseTimer();
    }
     static class TrackPoint{
         /**
          * 我的钱包点击
          * @param user_id
          * @param coin
          */
        public static void  rechargeClick(String user_id, String coin){
            HashMap<String, String> params = new HashMap<>();
            params.put("user_id", user_id);
            params.put("wallet_amount", coin);
            MultiTracker.onEvent(TrackConstants.B_MY_WALLET, params);
        }

         /**
          * 个人相册管理点击
          */
         public static void photoManageClick() {
             HashMap<String, String> params = new HashMap<>();
             params.put("type", "mine");
             MultiTracker.onEvent(TrackConstants.B_PHOTO_MANAGE, params);
         }

         /**
          * 未录制声音 点击
          */
         public static void mineVoiceClick() {
             MultiTracker.onEvent(TrackConstants.B_MINE_VOICE);
         }
     }


}
