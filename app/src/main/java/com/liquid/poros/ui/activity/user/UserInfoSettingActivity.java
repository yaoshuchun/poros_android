package com.liquid.poros.ui.activity.user;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.liquid.base.event.EventCenter;
import com.liquid.poros.R;
import com.liquid.poros.base.BaseActivity;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.contract.user.UserInfoSettingContract;
import com.liquid.poros.contract.user.UserInfoSettingPresenter;
import com.liquid.poros.entity.PorosUserInfo;
import com.liquid.poros.entity.UserCenterInfo;
import com.liquid.poros.ui.fragment.dialog.user.ChooseAgeDialogFragment;
import com.liquid.poros.ui.fragment.dialog.user.ChooseSexDialogFragment;
import com.liquid.poros.ui.fragment.dialog.user.PictureSelectorDialogFragment;
import com.liquid.poros.utils.GenerateJsonUtil;
import com.liquid.poros.utils.ScreenUtil;
import com.liquid.poros.utils.ViewUtils;
import com.liquid.poros.utils.glide.GlideEngine;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.style.PictureWindowAnimationStyle;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;

import static com.liquid.poros.ui.activity.user.UserCenterActivity.SAVE_INFO_SUCCESS;

public class UserInfoSettingActivity extends BaseActivity implements UserInfoSettingContract.View {
    @BindView(R.id.iv_user_head)
    public ImageView iv_user_head;
    @BindView(R.id.tv_name)
    public TextView tv_name;
    @BindView(R.id.tv_age)
    public TextView tv_age;
    @BindView(R.id.tv_sex)
    public TextView tv_sex;
    @BindView(R.id.tv_cp_want)
    public TextView tv_cp_want;
    @BindView(R.id.rl_user_game_want)
    RelativeLayout rl_user_game_want;
    @BindView(R.id.rl_user_tag)
    RelativeLayout rl_user_tag;
    @BindView(R.id.iv_user_cover)
    ImageView iv_user_cover;
    private UserInfoSettingPresenter mPresenter;
    private String mUserId;
    private String mStaticGame;
    private String mGame;
    private boolean mGameMatchControl;
    public static final int TYPE_EDIT_NICKNAME = 0; //昵称
    public static final int TYPE_EDIT_TAG = 1;      //个人标签
    public static final int TYPE_EDIT_CP_WANT = 2;  //理想CP
    public static final int TYPE_EDIT_AUDIO = 3;    //语音标签
    public static final int TYPE_EDIT_GAME_WANT = 4;//想玩游戏
    private String mAge = null;
    private String mSex = null;
    private UserCenterInfo.Data.UserInfo mUserInfoBean;
    private boolean isUserBg = false; //是否是背景封面  false 否
    private PictureWindowAnimationStyle pictureWindowAnimationStyle =  new PictureWindowAnimationStyle();
    @Override
    protected void parseBundleExtras(Bundle extras) {
        mUserId = extras.getString(Constants.USER_ID);
        mStaticGame = extras.getString(Constants.USER_STATIC_GAME);
        mGame = extras.getString(Constants.USER_GAME);
        mGameMatchControl = extras.getBoolean(Constants.USER_GAME_MATCH_CONTROL, false);
    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.activity_user_info_setting;
    }

    @Override
    protected void initViewsAndEvents(Bundle savedInstanceState) {
        mPresenter = new UserInfoSettingPresenter();
        mPresenter.attachView(this);
        mPresenter.getUserInfo(mUserId);
        rl_user_game_want.setVisibility(mGameMatchControl ? View.GONE : View.VISIBLE);
        rl_user_tag.setVisibility(mGameMatchControl ? View.VISIBLE : View.GONE);
    }

    @Override
    protected String getPageId() {
        return TrackConstants.PAGE_USER_CENTER_SETTING;
    }

    @OnClick({R.id.rl_user_head, R.id.rl_user_name, R.id.rl_user_age, R.id.rl_user_sex, R.id.rl_user_cp_want, R.id.rl_user_cover, R.id.rl_user_game_want, R.id.rl_user_tag, R.id.rl_user_audio})
    public void onClick(View view) {
        if (ViewUtils.isFastClick()) {
            return;
        }
        switch (view.getId()) {
            //头像
            case R.id.rl_user_head:
                choosePictureDialog(1);
                isUserBg = false;
                break;
            //昵称
            case R.id.rl_user_name:
                goUpdateUserInfoActivity(TYPE_EDIT_NICKNAME);
                break;
            //年龄
            case R.id.rl_user_age:
                if (mAge != null) {
                    Toast.makeText(UserInfoSettingActivity.this, getString(R.string.user_info_tip), Toast.LENGTH_SHORT).show();
                    return;
                }
                ChooseAgeDialogFragment.newInstance().setChooseAgeListener(new ChooseAgeDialogFragment.WheelChooseListener() {
                    @Override
                    public void onChoose(String selectItem) {
                        mAge = selectItem;
                        try {
                            mPresenter.updateUserInfo(null, null, null, mAge, null, GenerateJsonUtil.jsonArray(new ArrayList<>()));
                        } catch (Throwable e) {
                            e.printStackTrace();
                        }
                    }
                }).show(getSupportFragmentManager(), ChooseAgeDialogFragment.class.getName());
                break;
            //性别
            case R.id.rl_user_sex:
                if (mSex != null) {
                    Toast.makeText(UserInfoSettingActivity.this, getString(R.string.user_info_tip), Toast.LENGTH_SHORT).show();
                    return;
                }
                ChooseSexDialogFragment.newInstance().setChooseSexListener(new ChooseSexDialogFragment.ChooseSexListener() {
                    @Override
                    public void chooseSexSuccess(String sex) {
                        mSex = sex;
                        try {
                            mPresenter.updateUserInfo(null, null, mSex, null, null, GenerateJsonUtil.jsonArray(new ArrayList<>()));
                        } catch (Throwable e) {
                            e.printStackTrace();
                        }
                    }
                }).show(getSupportFragmentManager(), ChooseSexDialogFragment.class.getSimpleName());
                break;
            //个性封面
            case R.id.rl_user_cover:
                choosePictureDialog(1);
                isUserBg = true;
                break;
            //理想CP
            case R.id.rl_user_cp_want:
                goUpdateUserInfoActivity(TYPE_EDIT_CP_WANT);
                break;
            //想玩游戏
            case R.id.rl_user_game_want:
                goUpdateUserInfoActivity(TYPE_EDIT_GAME_WANT);
                break;
            //个人标签
            case R.id.rl_user_tag:
                goUpdateUserInfoActivity(TYPE_EDIT_TAG);
                break;
            //语音标签
            case R.id.rl_user_audio:
                goUpdateUserInfoActivity(TYPE_EDIT_AUDIO);
                break;
        }
    }

    private void goUpdateUserInfoActivity(int type) {
        if (mUserInfoBean == null) {
            return;
        }
        Bundle bundle = new Bundle();
        bundle.putInt(Constants.PARAMS_TYPE, type);
        switch (type) {
            case TYPE_EDIT_NICKNAME:
                bundle.putString(Constants.USER_NICKNAME, mUserInfoBean.getNick_name());
                break;
            case TYPE_EDIT_CP_WANT:
                bundle.putString(Constants.USER_CP_DESC, mUserInfoBean.getCouple_desc());
                break;
            case TYPE_EDIT_TAG:
                bundle.putStringArrayList(Constants.USER_TAGS, (ArrayList<String>) mUserInfoBean.getGames());
                break;
            case TYPE_EDIT_GAME_WANT:
                bundle.putString(Constants.USER_STATIC_GAME, mStaticGame);
                bundle.putString(Constants.USER_GAME, mGame);
                break;
        }
        bundle.putBoolean(Constants.USER_GAME_MATCH_CONTROL, mUserInfoBean.isMatch_control());
        readyGoForResult(UpdateUserInfoActivity.class, SAVE_INFO_SUCCESS, bundle);
    }

    private void choosePictureDialog(int maxSelectNum) {
        pictureWindowAnimationStyle.ofAllAnimation(R.anim.live_bottom_in,R.anim.live_bottom_out);
        PictureSelectorDialogFragment.newInstance().choosePictureListener(new PictureSelectorDialogFragment.PictureSelectorListener() {
            @Override
            public void onShotChoose() {
                PictureSelector.create(UserInfoSettingActivity.this)
                        .openCamera(PictureMimeType.ofImage())
                        .maxSelectNum(1)
                        .isAndroidQTransform(true)
                        .isEnableCrop(true)
                        .isAndroidQTransform(true)
                        .imageEngine(GlideEngine.createGlideEngine())
                        .setPictureWindowAnimationStyle(pictureWindowAnimationStyle)
                        .forResult(PictureConfig.REQUEST_CAMERA);
            }

            @Override
            public void onAlbumChoose() {
                PictureSelector.create(UserInfoSettingActivity.this)
                        .openGallery(PictureMimeType.ofImage())
                        .maxSelectNum(maxSelectNum)
                        .isAndroidQTransform(true)
                        .isEnableCrop(true)
                        .isAndroidQTransform(true)
                        .imageEngine(GlideEngine.createGlideEngine())
                        .setPictureWindowAnimationStyle(pictureWindowAnimationStyle)
                        .forResult(PictureConfig.CHOOSE_REQUEST);
            }
        }).show(getSupportFragmentManager(), PictureSelectorDialogFragment.class.getSimpleName());
    }

    @Override
    public void uploadImageSuccess(String path, String image_url) {
        mPresenter.updateUserHead(image_url);
        RequestOptions options = new RequestOptions()
                .apply(RequestOptions.bitmapTransform(new CircleCrop()))
                .override(ScreenUtil.dip2px(40), ScreenUtil.dip2px(40))
                .placeholder(R.mipmap.img_default_avatar)
                .error(R.mipmap.img_default_avatar);
        Glide.with(this)
                .load(image_url)
                .apply(options)
                .into(iv_user_head);
        EventBus.getDefault().post(new EventCenter<>(Constants.EVENT_CODE_UPDATE_USER_INFO));
    }

    @Override
    public void uploadImageOssSuccess(String path) {
        RequestOptions options = new RequestOptions()
                .placeholder(R.mipmap.icon_default)
                .error(R.mipmap.icon_default)
                .transform(new CenterCrop(), new RoundedCorners(ScreenUtil.dip2px(8)));
        Glide.with(UserInfoSettingActivity.this)
                .load(path)
                .override(ScreenUtil.dip2px(48), ScreenUtil.dip2px(48))
                .apply(options)
                .into(iv_user_cover);
        EventBus.getDefault().post(new EventCenter<>(Constants.EVENT_CODE_UPDATE_USER_INFO));
    }

    @Override
    public void onUserInfoFetch(UserCenterInfo userCenterInfo) {
        if (userCenterInfo == null || userCenterInfo.getData() == null || userCenterInfo.getData().getUser_info() == null) {
            return;
        }
        mUserInfoBean = userCenterInfo.getData().getUser_info();
        //头像
        RequestOptions options = new RequestOptions()
                .apply(RequestOptions.bitmapTransform(new CircleCrop()))
                .override(ScreenUtil.dip2px(40), ScreenUtil.dip2px(40))
                .placeholder(R.mipmap.img_default_avatar)
                .error(R.mipmap.img_default_avatar);
        Glide.with(this)
                .load(mUserInfoBean.getAvatar_url())
                .apply(options)
                .into(iv_user_head);
        if (!TextUtils.isEmpty(mUserInfoBean.getNick_name())) {
            tv_name.setText(mUserInfoBean.getNick_name());
        }
        //年龄
        if (!TextUtils.isEmpty(mUserInfoBean.getAge())) {
            tv_age.setText(mUserInfoBean.getAge() + "岁");
        } else {
            tv_age.setText(getString(R.string.user_info_tip));
        }
        mAge = mUserInfoBean.getAge();
        //性别
        if (!TextUtils.isEmpty(mUserInfoBean.getGender())) {
            mSex = mUserInfoBean.getGender();
            tv_sex.setText(mUserInfoBean.getGender().equals("1") ? "男" : "女");
        } else {
            tv_sex.setText(getString(R.string.user_info_tip));
        }
        //理想CP
        if (!TextUtils.isEmpty(mUserInfoBean.getCouple_desc())) {
            tv_cp_want.setText(mUserInfoBean.getCouple_desc());
        } else {
            tv_cp_want.setText(getString(R.string.user_info_cp_tip));
        }
        RequestOptions coverOptions = new RequestOptions()
                .placeholder(R.mipmap.icon_default)
                .error(R.mipmap.icon_default)
                .transform(new CenterCrop(), new RoundedCorners(ScreenUtil.dip2px(8)));
        if (TextUtils.isEmpty(mUserInfoBean.getCover_img()) || mUserInfoBean.getCover_img().equals("null")) {
            Glide.with(UserInfoSettingActivity.this)
                    .load(mUserInfoBean.getAvatar_url())
                    .override(ScreenUtil.dip2px(48), ScreenUtil.dip2px(48))
                    .apply(coverOptions)
                    .into(iv_user_cover);
        } else {
            Glide.with(UserInfoSettingActivity.this)
                    .load(mUserInfoBean.getCover_img())
                    .override(ScreenUtil.dip2px(48), ScreenUtil.dip2px(48))
                    .apply(coverOptions)
                    .into(iv_user_cover);
        }
        mGame = new Gson().toJson(mUserInfoBean.getGame_info());
    }

    @Override
    public void uploadUserInfoSuccess(PorosUserInfo userInfo) {
        mPresenter.getUserInfo(mUserId);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case SAVE_INFO_SUCCESS:
                    if (mPresenter != null) {
                        mPresenter.getUserInfo(mUserId);
                    }
                    EventBus.getDefault().post(new EventCenter<>(Constants.EVENT_CODE_UPDATE_USER_INFO));
                    break;
                case PictureConfig.CHOOSE_REQUEST:
                case PictureConfig.REQUEST_CAMERA:
                    List<LocalMedia> selectList = PictureSelector.obtainMultipleResult(data);
                    String path = selectList.get(0).getPath();
                    if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.P) {
                        path = selectList.get(0).getAndroidQToPath();
                    }
                    if (isUserBg) {
                        mPresenter.uploadImageOss(path, "user_cover_img");
                    } else {
                        mPresenter.uploadImage(path);
                    }
                    break;
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }
}
