package com.liquid.poros.ui.fragment.home;

import android.annotation.SuppressLint;
import android.graphics.Rect;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.liquid.base.adapter.CommonAdapter;
import com.liquid.base.adapter.CommonViewHolder;
import com.liquid.base.event.EventCenter;
import com.liquid.poros.R;
import com.liquid.poros.base.BaseFragment;
import com.liquid.poros.analytics.utils.MultiTracker;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.constant.DataOptimize;
import com.liquid.poros.constant.DispatchFemaleEnum;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.contract.couple.DiscoverContract;
import com.liquid.poros.contract.couple.DiscoverPresenter;
import com.liquid.poros.entity.CoupleRoomListInfo;
import com.liquid.poros.entity.MicUserInfo;
import com.liquid.poros.entity.RoomDetailsInfo;
import com.liquid.poros.helper.JinquanResourceHelper;
import com.liquid.poros.ui.RouterUtils;
import com.liquid.poros.ui.activity.live.RoomLiveActivityV2;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.poros.analytics.utils.ExposureUtils;
import com.liquid.poros.utils.ScreenUtil;
import com.liquid.poros.utils.ViewUtils;
import com.liquid.poros.widgets.WrapContentGridLayoutManager;
import com.liquid.poros.widgets.pulltorefresh.BaseRefreshListener;
import com.liquid.poros.widgets.pulltorefresh.PullToRefreshLayout;
import com.liquid.poros.widgets.pulltorefresh.ViewStatus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;

public class DiscoverFragment extends BaseFragment implements DiscoverContract.View, BaseRefreshListener {
    @BindView(R.id.rv_group)
    RecyclerView rvGroup;
    @BindView(R.id.container)
    PullToRefreshLayout container;

    @BindView(R.id.ll_container)
    View ll_container;

    private CommonAdapter<CoupleRoomListInfo.CoupleRoomInfo> mCommonAdapter;
    private ArrayList<CoupleRoomListInfo.CoupleRoomInfo> roomInfos = new ArrayList<>();
    private ExposureUtils<CommonAdapter, CoupleRoomListInfo.CoupleRoomInfo> disCoverExposureUtils;
    private DiscoverPresenter mPresenter;
    private int mPage = 0;
    private int mStartPos = 0;

    public static DiscoverFragment newInstance() {
        return new DiscoverFragment();
    }

    @Override
    protected String getPageId() {
        return TrackConstants.PAGE_HOME_COUPLE;
    }

    @Override
    protected void onFirstUserVisible() {

    }

    @Override
    protected void onUserVisible() {

    }

    @Override
    protected void onUserInvisible() {

    }

    @Override
    protected View getLoadingTargetView() {
        return null;
    }

    @Override
    protected void initViewsAndEvents(Bundle savedInstanceState) {
        rvGroup.setLayoutManager(new WrapContentGridLayoutManager(getActivity(), 2));
        rvGroup.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                WrapContentGridLayoutManager.LayoutParams layoutParams = (WrapContentGridLayoutManager.LayoutParams) view.getLayoutParams();
                int spanIndex = layoutParams.getSpanIndex();
                outRect.top = ScreenUtil.dip2px(7);
                if (spanIndex == 0) {
                    // left
                    outRect.left = ScreenUtil.dip2px(12);
                    outRect.right = ScreenUtil.dip2px(12) / 4;
                } else {
                    outRect.right = ScreenUtil.dip2px(12);
                    outRect.left = ScreenUtil.dip2px(12) / 4;
                }
            }
        });
        mCommonAdapter = new CommonAdapter<CoupleRoomListInfo.CoupleRoomInfo>(getActivity(), R.layout.item_couple_room) {
            @SuppressLint("StringFormatMatches")
            @Override
            public void convert(final CommonViewHolder holder, final CoupleRoomListInfo.CoupleRoomInfo coupleRoomInfo, final int pos) {
                View ll_room_info = holder.getView(R.id.ll_room_info);
                View ll_content = holder.getView(R.id.ll_content);
                TextView tv_anchor = holder.getView(R.id.tv_anchor);
                TextView tv_room_anchor_nickname = holder.getView(R.id.tv_room_anchor_nickname);
                JinquanResourceHelper.getInstance(getActivity()).setBackgroundColorByAttr(ll_room_info, R.attr.custom_attr_app_bg);
                JinquanResourceHelper.getInstance(getActivity()).setBackgroundColorByAttr(ll_content, R.attr.custom_attr_app_bg);
                JinquanResourceHelper.getInstance(getActivity()).setTextColorByAttr(tv_anchor, R.attr.custom_attr_remark_text_color);
                JinquanResourceHelper.getInstance(getActivity()).setTextColorByAttr(tv_room_anchor_nickname, R.attr.custom_attr_main_title_color);

                holder.itemView.setTag(coupleRoomInfo.getAnchor().getUser_id());
                ImageView iv_room_cover = holder.getView(R.id.iv_room_cover);
                final int itemHeight = (((ScreenUtil.adjustWidgetWidth() - ScreenUtil.dip2px(20)))) / 2;
                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) holder.getView(R.id.rl_container).getLayoutParams();
                params.height = itemHeight;
                //上麦用户信息
                if (coupleRoomInfo.getMic_user() != null && !TextUtils.isEmpty(coupleRoomInfo.getMic_user().getUser_id()) && coupleRoomInfo.getRoom_type() != RoomLiveActivityV2.ROOM_TYPE_PK) {
                    MicUserInfo micUserInfo = coupleRoomInfo.getMic_user();
                    RequestOptions roomOptions = new RequestOptions()
                            .placeholder(R.mipmap.icon_default)
                            .error(R.mipmap.icon_default)
                            .transform(new CenterCrop(), new RoundedCorners(ScreenUtil.dip2px(8)));
                    Glide.with(getActivity())
                            .load(micUserInfo.getAvatar_url())
                            .apply(roomOptions)
                            .override(ScreenUtil.dip2px(100))
                            .into(iv_room_cover);
                    //直播中状态
                    if (coupleRoomInfo.getRoom_type() == RoomLiveActivityV2.ROOM_TYPE_NORMAL) {
                        holder.getView(R.id.ll_room_live_tag).setVisibility(micUserInfo.isCamera_close() || coupleRoomInfo.getVoice_chatting() != 0 ? View.GONE : View.VISIBLE);
                        ImageView iv_anim = holder.getView(R.id.iv_anim);
                        if (iv_anim.getVisibility() == View.VISIBLE) {
                            ((Animatable) iv_anim.getDrawable()).start();
                        } else {
                            ((Animatable) iv_anim.getDrawable()).stop();
                        }
                    }
                    holder.setVisibility(R.id.ll_anchor_container, View.VISIBLE);
                    holder.setVisibility(R.id.tv_room_mic_nickname, TextUtils.isEmpty(micUserInfo.getNick_name()) ? View.GONE : View.VISIBLE);
                    holder.setText(R.id.tv_room_mic_nickname, micUserInfo.getNick_name());
                    //年龄
                    TextView tv_room_mic_age = holder.getView(R.id.tv_room_mic_age);
                    tv_room_mic_age.setBackgroundResource(micUserInfo.getGender() == 1 ? R.drawable.bg_color_00aaff_16 : R.drawable.bg_color_ff75a3_16);
                    tv_room_mic_age.setVisibility(TextUtils.isEmpty(micUserInfo.getAge()) ? View.GONE : View.VISIBLE);
                    Drawable ageDrawable = getActivity().getResources().getDrawable(micUserInfo.getGender() == 1 ? R.mipmap.icon_male : R.mipmap.icon_female);
                    ageDrawable.setBounds(0, 0, ageDrawable.getMinimumWidth(), ageDrawable.getMinimumHeight());
                    tv_room_mic_age.setCompoundDrawables(ageDrawable, null, null, null);
                    tv_room_mic_age.setCompoundDrawablePadding(3);
                    tv_room_mic_age.setText(micUserInfo.getAge());
                    //想玩 标签
                    TextView tv_want_game = holder.getView(R.id.tv_want_game);
                    if (micUserInfo.getGames() != null && micUserInfo.getGames().size() > 0) {
                        SpannableStringBuilder spannableString = new SpannableStringBuilder(mContext.getString(R.string.cp_want_play));
                        for (int i = 0; i < micUserInfo.getGames().size(); i++) {
                            spannableString.append(" ");
                            if (i == micUserInfo.getGames().size() - 1) {
                                spannableString.append(micUserInfo.getGames().get(i));
                            } else {
                                spannableString.append(micUserInfo.getGames().get(i) + " |");
                            }
                        }
                        tv_want_game.setVisibility(View.VISIBLE);
                        tv_want_game.setBackgroundResource(R.drawable.bg_color_4d000000_9);
                        tv_want_game.setText(spannableString);
                    } else {
                        tv_want_game.setVisibility(View.GONE);
                    }
                } else {
                    holder.setVisibility(R.id.ll_anchor_container, View.GONE);
                    holder.setVisibility(R.id.ll_room_live_tag, View.GONE);
                    RequestOptions roomOptions = new RequestOptions()
                            .placeholder(R.mipmap.icon_default)
                            .error(R.mipmap.icon_default)
                            .transform(new CenterCrop(), new RoundedCorners(ScreenUtil.dip2px(8)));
                    Glide.with(getActivity())
                            .load(coupleRoomInfo.getRoom_cover())
                            .apply(roomOptions)
                            .into(iv_room_cover);
                }
                holder.setVisibility(R.id.tv_private_room_tip, coupleRoomInfo.getRoom_type() == RoomLiveActivityV2.ROOM_TYPE_PRIVATE ? View.VISIBLE : View.GONE);
                holder.setVisibility(R.id.pk_tag_iv, coupleRoomInfo.getRoom_type() == RoomLiveActivityV2.ROOM_TYPE_PK ? View.VISIBLE : View.GONE);
                holder.setVisibility(R.id.iv_voice_chat_playing, coupleRoomInfo.getVoice_chatting() != 0 ? View.VISIBLE : View.GONE);
                //房间主持人信息
                holder.setText(R.id.tv_room_anchor_nickname, coupleRoomInfo.getAnchor().getNick_name());
                ImageView iv_room_anchor_head = holder.getView(R.id.iv_room_anchor_head);
                RequestOptions options = new RequestOptions()
                        .placeholder(R.mipmap.img_default_avatar)
                        .apply(RequestOptions.bitmapTransform(new CircleCrop()))
                        .override(ScreenUtil.dip2px(24), ScreenUtil.dip2px(24))
                        .error(R.mipmap.img_default_avatar);
                Glide.with(getActivity())
                        .load(coupleRoomInfo.getAnchor().getAvatar_url())
                        .apply(options)
                        .into(iv_room_anchor_head);
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (ViewUtils.isFastClick()) {
                            return;
                        }
                        mPresenter.fetchRoomDetails(coupleRoomInfo.getRoom_id());
                    }
                });
            }
        };
//        mCommonAdapter.initData(roomInfos);
        rvGroup.setAdapter(mCommonAdapter);
        mPresenter = new DiscoverPresenter();
        mPresenter.attachView(this);
        mPresenter.fetchCoupleRoom(mPage);
        container.setRefreshListener(this);
        container.setCanLoadMore(true);
    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.fragment_discover;
    }

    @Override
    protected void onEventComing(EventCenter eventCenter) {
        switch (eventCenter.getEventCode()) {
            case Constants.EVENT_CODE_DISPATCH_FEMALE:
                //足迹直播间关闭触发
                mPresenter.fetchDispatchFemale(DispatchFemaleEnum.DIS_OUT_LIVE.getCode());
                HashMap<String, String> params = new HashMap<>();
                params.put("dispatch_female_type", "active_out_room");
                MultiTracker.onEvent(TrackConstants.B_DISPATCH_FEMALE, params);
                break;
            case Constants.EVENT_CODE_CANCEL_APPLY_MIC:
                //直播间关闭 取消申请上麦
                String roomId = (String) eventCenter.getData();
                mPresenter.fetchCancelApplyMic(roomId);
                HashMap<String, String> micParams = new HashMap<>();
                micParams.put("room_id", roomId);
                MultiTracker.onEvent(TrackConstants.B_CANCEL_APPLY_MIC, micParams);
                break;
        }
    }

    @Override
    public void onCoupleRoomListFetched(CoupleRoomListInfo coupleRoomListInfo) {
        container.finishRefresh();
        container.finishLoadMore();
        if ((mPage == 0) && (coupleRoomListInfo == null || coupleRoomListInfo.getData() == null || coupleRoomListInfo.getData().size() == 0)) {
            container.showView(ViewStatus.EMPTY_STATUS);
            container.setCanLoadMore(false);
            return;
        }
        container.showView(ViewStatus.CONTENT_STATUS);

        if (mPage == 0) {
            roomInfos.clear();
            roomInfos.addAll(coupleRoomListInfo.getData());
            mCommonAdapter.update(coupleRoomListInfo.getData());
        } else {
            roomInfos.addAll(coupleRoomListInfo.getData());
            mStartPos = mCommonAdapter.getItemCount();
            mCommonAdapter.addWithoutNotify(getAddCouples(coupleRoomListInfo.getData()));
            mCommonAdapter.notifyItemRangeChanged(mStartPos, coupleRoomListInfo.getData().size());
        }
    }

    @Override
    public void onRoomDetailsFetched(RoomDetailsInfo roomDetailsInfo) {
        if (roomDetailsInfo.getData() == null) {
            return;
        }
        if (roomDetailsInfo.getData().getRoom_type() == RoomLiveActivityV2.ROOM_TYPE_PRIVATE) {
            //专属房
            if (!TextUtils.isEmpty(roomDetailsInfo.getData().getHint())) {
                showMessage(roomDetailsInfo.getData().getHint());
            }
        } else {
            //Cp房
            Bundle bundle = new Bundle();
            bundle.putString(Constants.ROOM_ID, roomDetailsInfo.getData().getRoom_id());
            bundle.putString(Constants.IM_ROOM_ID, roomDetailsInfo.getData().getIm_room_id());
            bundle.putInt(Constants.IM_ROOM_TYPE, roomDetailsInfo.getData().getRoom_type());
            bundle.putString(Constants.AUDIENCE_FROM, DataOptimize.VIDEO_LIST);
            RouterUtils.goToRoomLiveActivity(getContext(),bundle);
//            readyGo(RoomLiveActivityV2.class, bundle);
            HashMap<String, String> params = new HashMap<>();
            params.put("user_id", AccountUtil.getInstance().getUserId());
            params.put("live_room_id", roomDetailsInfo.getData().getRoom_id());
            if (!TextUtils.isEmpty(roomDetailsInfo.getData().getGirl_id())) {
                params.put("female_guest_id", roomDetailsInfo.getData().getGirl_id());
            }
            if (!TextUtils.isEmpty(roomDetailsInfo.getData().getAnchor_id())) {
                params.put("anchor_id", roomDetailsInfo.getData().getAnchor_id());
            }
            if (roomDetailsInfo.getData().getRoom_type() == RoomLiveActivityV2.ROOM_TYPE_PK){
                params.put("room_type","pk_room");
            }
            params.put("video_room_click_from", "video_list");
            MultiTracker.onEvent(TrackConstants.B_GO_TEAM, params);
        }
    }

    /**
     * 获取列表增量
     *
     * @param temps
     * @return
     */
    private List getAddCouples(List<CoupleRoomListInfo.CoupleRoomInfo> temps) {
        List<CoupleRoomListInfo.CoupleRoomInfo> adds = new ArrayList<>();
        List<CoupleRoomListInfo.CoupleRoomInfo> coupleRoomInfos = mCommonAdapter.getDatas();
        for (CoupleRoomListInfo.CoupleRoomInfo t : temps) {
            if (!coupleRoomInfos.contains(t)) {
                adds.add(t);
            }
        }
        return adds;
    }

    @Override
    public void refresh() {
        mPage = 0;
        mPresenter.fetchCoupleRoom(mPage);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mPresenter != null) {
            rvGroup.scrollToPosition(0);
            mPresenter.fetchCoupleRoom(mPage);
        }
        if (disCoverExposureUtils == null) {
            disCoverExposureUtils = new ExposureUtils(mCommonAdapter, roomInfos);
            disCoverExposureUtils.setmRecyclerView(rvGroup);
        }
        disCoverExposureUtils.startFeedExposureStatistics();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (disCoverExposureUtils != null) {
            disCoverExposureUtils.calDiscoverExplosure();
            disCoverExposureUtils.stopFeedExposureStatistics();
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisible) {
        super.setUserVisibleHint(isVisible);
        if (isResumed()) {
            if (isVisible) {
                startExpore();
            } else {
                if (disCoverExposureUtils != null) {
                    disCoverExposureUtils.calDiscoverExplosure();
                    disCoverExposureUtils.stopFeedExposureStatistics();
                }
            }
        }
    }

    private void startExpore() {
        if (disCoverExposureUtils == null) {
            disCoverExposureUtils = new ExposureUtils<>(mCommonAdapter, roomInfos);
            disCoverExposureUtils.setmRecyclerView(rvGroup);
        }
        disCoverExposureUtils.startFeedExposureStatistics();
    }

    @Override
    public void loadMore() {
        ++mPage;
        mPresenter.fetchCoupleRoom(mPage);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }

    @Override
    public void notifyByThemeChanged() {
        JinquanResourceHelper.getInstance(getActivity()).setBackgroundColorByAttr(ll_container, R.attr.custom_attr_app_bg);
        if (mCommonAdapter != null) {
            mCommonAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void notifyNetworkChanged(boolean isConnected) {
        container.showView(isConnected ? ViewStatus.CONTENT_STATUS : ViewStatus.ERROR_STATUS);
        container.setCanRefresh(isConnected);
    }
}
