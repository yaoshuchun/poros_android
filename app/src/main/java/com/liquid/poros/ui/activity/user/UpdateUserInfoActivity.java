package com.liquid.poros.ui.activity.user;

import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.liquid.base.adapter.CommonAdapter;
import com.liquid.base.adapter.CommonViewHolder;
import com.liquid.base.event.EventCenter;
import com.liquid.im.kit.permission.MPermission;
import com.liquid.poros.R;
import com.liquid.poros.base.BaseActivity;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.contract.user.UpdateUserInfoContract;
import com.liquid.poros.contract.user.UpdateUserInfoPresenter;
import com.liquid.poros.entity.EditGameBean;
import com.liquid.poros.entity.NormalGame;
import com.liquid.poros.entity.PorosUserInfo;
import com.liquid.poros.entity.SettingEditGameBean;
import com.liquid.poros.entity.UserCenterInfo;
import com.liquid.poros.helper.AudioPlayerHelper;
import com.liquid.poros.helper.JinquanResourceHelper;
import com.liquid.poros.ui.fragment.dialog.common.CommonActionListener;
import com.liquid.poros.ui.fragment.dialog.common.CommonDialogBuilder;
import com.liquid.poros.ui.fragment.dialog.common.CommonDialogFragment;
import com.liquid.poros.ui.fragment.dialog.user.EditGameDialogFragment;
import com.liquid.poros.ui.fragment.dialog.user.EditOtherGameDialogFragment;
import com.liquid.poros.utils.GenerateJsonUtil;
import com.liquid.poros.utils.ScreenUtil;
import com.liquid.poros.utils.SharePreferenceUtil;
import com.liquid.poros.utils.ViewUtils;
import com.liquid.poros.widgets.FlowLayout;
import com.liquid.poros.widgets.HorizontalItemDecoration;
import com.netease.lava.nertc.sdk.NERtcEx;
import com.netease.nimlib.sdk.media.record.AudioRecorder;
import com.netease.nimlib.sdk.media.record.IAudioRecordCallback;
import com.netease.nimlib.sdk.media.record.RecordType;

import org.json.JSONException;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;


public class UpdateUserInfoActivity extends BaseActivity implements UpdateUserInfoContract.View {
    @BindView(R.id.tv_update_title)
    TextView tv_update_title;
    @BindView(R.id.tv_update_tip)
    TextView tv_update_tip;
    @BindView(R.id.rl_nick_name)
    RelativeLayout rl_nick_name;
    @BindView(R.id.et_nick_name)
    EditText et_nick_name;
    @BindView(R.id.tv_nickname_count)
    TextView tv_nickname_count;
    @BindView(R.id.et_cp_want)
    EditText et_cp_want;
    @BindView(R.id.tv_cp_want_count)
    TextView tv_cp_want_count;
    @BindView(R.id.rl_cp_want)
    RelativeLayout rl_cp_want;
    @BindView(R.id.fl_user_tag)
    FlowLayout fl_user_tag;
    @BindView(R.id.ll_game)
    LinearLayout ll_game;
    @BindView(R.id.ll_audio)
    LinearLayout ll_audio;
    @BindView(R.id.iv_audio_record)
    ImageView iv_audio_record;
    @BindView(R.id.rl_audio_prepare)
    RelativeLayout rl_audio_prepare;
    @BindView(R.id.tv_audio_timer)
    TextView tv_audio_timer;
    @BindView(R.id.tv_audio_status)
    TextView tv_audio_status;
    @BindView(R.id.fl_audio)
    FrameLayout fl_audio;
    @BindView(R.id.rl_audio_play)
    RelativeLayout rl_audio_play;
    @BindView(R.id.iv_audio_play_status)
    ImageView iv_audio_play_status;
    @BindView(R.id.tv_audio_play_timer)
    TextView tv_audio_play_timer;
    @BindView(R.id.iv_audio_record_anim)
    ImageView iv_audio_record_anim;
    //游戏信息
    @BindView(R.id.tv_submit_game_title)
    TextView tv_submit_game_title;
    @BindView(R.id.tv_submit_game_tip)
    TextView tv_submit_game_tip;
    @BindView(R.id.ll_game_wangzhe_content)
    LinearLayout ll_game_wangzhe_content;
    @BindView(R.id.tv_game_wangzhe_platform)
    TextView tv_game_wangzhe_platform;
    @BindView(R.id.tv_game_wangzhe_area)
    TextView tv_game_wangzhe_area;
    @BindView(R.id.tv_game_wangzhe_rank)
    TextView tv_game_wangzhe_rank;
    @BindView(R.id.tv_game_wangzhe_info)
    TextView tv_game_wangzhe_info;
    @BindView(R.id.ll_game_heping_content)
    LinearLayout ll_game_heping_content;
    @BindView(R.id.tv_game_heping_platform)
    TextView tv_game_heping_platform;
    @BindView(R.id.tv_game_heping_area)
    TextView tv_game_heping_area;
    @BindView(R.id.tv_game_heping_rank)
    TextView tv_game_heping_rank;
    @BindView(R.id.tv_game_heping_info)
    TextView tv_game_heping_info;
    @BindView(R.id.ll_game_other_content)
    LinearLayout ll_game_other_content;
    @BindView(R.id.rv_other_game)
    RecyclerView rv_other_game;
    @BindView(R.id.tv_game_other_info)
    TextView tv_game_other_info;
    @BindView(R.id.iv_heping)
    ImageView iv_heping;
    @BindView(R.id.iv_wangzhe)
    ImageView iv_wangzhe;
    @BindView(R.id.iv_other)
    ImageView iv_other;
    private UpdateUserInfoPresenter mPresenter;
    private int mUpdateType;
    private String mNickName;
    private String mCpDesc;
    private ArrayList<String> mUserTags;    //个人标签
    private String mStaticGame;
    private String mGame;
    private boolean mIsMatchControl;
    private List<String> mTags = new ArrayList<>();
    private List<String> mTagStrList = new ArrayList<>();
    private long downTime;//按下时间
    private long upTime;  //抬起时间
    private Timer mTimer2;
    private TimerTask mTask2;
    private int mAudioTimer = 0;
    public int audioMaxTime = 60;//语音介绍最大录音时长
    private int endTime = 0;//暂停时间计时
    private boolean started = false;
    private boolean touched = false; // 是否按着
    protected AudioRecorder audioMessageHelper;
    private File mAudioFile;//语音文件
    private AudioPlayerHelper mAudioPlayerHelper;
    private SettingEditGameBean mStaticGameBean;
    private UserCenterInfo.Data.UserInfo.GameInfoBean mGameBean;
    private CommonAdapter<NormalGame> mOtherGameAdapter;
    private String honor_platform;
    private String honor_zoom;
    private String honor_rank;
    private String peace_platform;
    private String peace_zoom;
    private String peace_rank;
    private String other_games;
    private boolean isReverseRank = false;//是否倒序

    @Override
    protected void parseBundleExtras(Bundle extras) {
        mUpdateType = extras.getInt(Constants.PARAMS_TYPE);
        mNickName = extras.getString(Constants.USER_NICKNAME);
        mCpDesc = extras.getString(Constants.USER_CP_DESC);
        mUserTags = extras.getStringArrayList(Constants.USER_TAGS);
        mStaticGame = extras.getString(Constants.USER_STATIC_GAME);
        mGame = extras.getString(Constants.USER_GAME);
        mIsMatchControl = extras.getBoolean(Constants.USER_GAME_MATCH_CONTROL);
    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.activity_update_user_info;
    }

    @Override
    protected String getPageId() {
        return TrackConstants.PAGE_USER_INFO_UPDATE;
    }

    @Override
    protected void initViewsAndEvents(Bundle savedInstanceState) {
        mPresenter = new UpdateUserInfoPresenter();
        mPresenter.attachView(this);
        et_nick_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int content = et_nick_name.getText().length();
                tv_nickname_count.setText(String.format(getString(R.string.game_room_name_count), content));
            }

            @Override
            public void afterTextChanged(Editable s) {
                mNickName = s.toString().trim();
            }
        });
        et_cp_want.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int content = et_cp_want.getText().length();
                tv_cp_want_count.setText(String.format(getString(R.string.cp_want_count), content));
            }

            @Override
            public void afterTextChanged(Editable s) {
                mCpDesc = s.toString().trim();
            }
        });
        switchShowView();
        String[] infoGames = SharePreferenceUtil.getStringArray(SharePreferenceUtil.FILE_INFO_GAMES_DATA, SharePreferenceUtil.KEY_INFO_GAMES);
        if (infoGames != null && infoGames.length > 0) {
            initTags(Arrays.asList(infoGames));
        }
        if (mUpdateType == UserInfoSettingActivity.TYPE_EDIT_GAME_WANT) {
            mStaticGameBean = new Gson().fromJson(mStaticGame, SettingEditGameBean.class);
            mGameBean = new Gson().fromJson(mGame, UserCenterInfo.Data.UserInfo.GameInfoBean.class);
            isReverseRank = false;
            checkGameData();
            setGameData();
        }
        tv_submit_game_title.setVisibility(View.GONE);
        tv_submit_game_tip.setVisibility(View.GONE);
    }


    @OnClick({R.id.iv_left, R.id.iv_delete_audio, R.id.rl_audio_play, R.id.rl_game_heping, R.id.rl_game_wangzhe, R.id.rl_game_other, R.id.tv_save})
    public void onClick(View view) {
        if (ViewUtils.isFastClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_left:
                back();
                break;
            case R.id.iv_delete_audio:
                rl_audio_prepare.setVisibility(View.GONE);
                tv_audio_timer.setVisibility(View.GONE);
                tv_audio_status.setVisibility(View.VISIBLE);
                fl_audio.setVisibility(View.VISIBLE);
                mAudioFile = null;
                break;
            case R.id.rl_audio_play:
                if (isDestroyed() || isFinishing() || mAudioFile == null) {
                    return;
                }
                mAudioPlayerHelper = new AudioPlayerHelper();
                mAudioPlayerHelper.startPlay(iv_audio_play_status,mAudioFile.getPath(), false, false);
                mAudioPlayerHelper.startTimer(endTime);
                mAudioPlayerHelper.setAudioListener(new AudioPlayerHelper.AudioPlayListener() {
                    @Override
                    public void startTime(int time) {
                        if (isDestroyed() || isFinishing()) {
                            return;
                        }
                        tv_audio_play_timer.setText((time == 0 ? endTime : time) + "s");
                        iv_audio_play_status.setBackgroundResource(R.mipmap.icon_audio_stop);
                    }

                    @Override
                    public void finishTime() {
                        if (isDestroyed() || isFinishing()) {
                            return;
                        }
                        iv_audio_play_status.setBackgroundResource(R.mipmap.icon_audio_play);
                    }
                });
                break;
            case R.id.rl_game_heping:
                ArrayList<EditGameBean> peace_elite = new ArrayList<>();
                peace_elite.add(mStaticGameBean.peace_elite.platform);
                peace_elite.add(mStaticGameBean.peace_elite.zone);
                peace_elite.add(mStaticGameBean.peace_elite.rank);
                EditGameDialogFragment.newInstance()
                        .setTitle(getString(R.string.test_submit_info_game_hepingjingying))
                        .setEditGameBeans(peace_elite)
                        .setPeaceBean(mGameBean.getHpjy())
                        .show(getSupportFragmentManager(), EditGameDialogFragment.class.getSimpleName());
                break;
            case R.id.rl_game_wangzhe:
                ArrayList<EditGameBean> honor_of_kings = new ArrayList<>();
                honor_of_kings.add(mStaticGameBean.honor_of_kings.platform);
                honor_of_kings.add(mStaticGameBean.honor_of_kings.zone);
                honor_of_kings.add(mStaticGameBean.honor_of_kings.rank);
                EditGameDialogFragment.newInstance()
                        .setTitle(getString(R.string.test_submit_info_game_wangzhe))
                        .setEditGameBeans(honor_of_kings)
                        .setHonorBean(mGameBean.getWzry())
                        .show(getSupportFragmentManager(), EditGameDialogFragment.class.getSimpleName());
                break;
            case R.id.rl_game_other:
                EditOtherGameDialogFragment.newInstance()
                        .setTitle(getString(R.string.test_submit_info_game_other))
                        .setEditGameBeans(mStaticGameBean.other.list)
                        .show(getSupportFragmentManager(), EditOtherGameDialogFragment.class.getSimpleName());
                break;
            case R.id.tv_save:
                saveUserInfo();
                break;

        }
    }

    private void saveUserInfo() {
        if (mUpdateType == UserInfoSettingActivity.TYPE_EDIT_AUDIO) {
            if (mAudioFile != null) {
                mPresenter.uploadImageOss(mAudioFile.getPath(), "user_voice");
            }else {
                finish();
            }
        } else {
            try {
                mPresenter.updateUserInfo(mIsMatchControl, mNickName, null, null, null, mCpDesc, GenerateJsonUtil.jsonArray(mTags), mGameBean);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void uploadImageOssSuccess(String path) {
        EventBus.getDefault().post(new EventCenter<>(Constants.EVENT_CODE_UPDATE_PHOTO));
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void uploadUserInfoSuccess(PorosUserInfo userInfo) {
        hideKeyBoard();
        setResult(RESULT_OK);
        finish();
    }

    /**
     * 根据type显示不同view
     */
    private void switchShowView() {
        switch (mUpdateType) {
            case UserInfoSettingActivity.TYPE_EDIT_NICKNAME:
                //昵称
                tv_update_title.setText(getString(R.string.update_user_info_nickname_title));
                tv_update_tip.setText(getString(R.string.update_user_info_nickname_tip));
                rl_nick_name.setVisibility(View.VISIBLE);
                rl_cp_want.setVisibility(View.GONE);
                ll_game.setVisibility(View.GONE);
                fl_user_tag.setVisibility(View.GONE);
                ll_audio.setVisibility(View.GONE);
                if (!TextUtils.isEmpty(mNickName)) {
                    et_nick_name.setText(mNickName);
                    et_nick_name.setSelection(mNickName.length());
                }
                break;
            case UserInfoSettingActivity.TYPE_EDIT_TAG:
                //个人标签
                tv_update_title.setText(getString(R.string.update_user_info_tag_title));
                tv_update_tip.setText(getString(R.string.update_user_info_tag_tip));
                rl_nick_name.setVisibility(View.GONE);
                fl_user_tag.setVisibility(View.VISIBLE);
                rl_cp_want.setVisibility(View.GONE);
                ll_game.setVisibility(View.GONE);
                ll_audio.setVisibility(View.GONE);
                break;
            case UserInfoSettingActivity.TYPE_EDIT_CP_WANT:
                //理想CP
                tv_update_title.setText(getString(R.string.update_user_info_cp_want_title));
                tv_update_tip.setText(getString(R.string.update_user_info_cp_want_tip));
                rl_nick_name.setVisibility(View.GONE);
                fl_user_tag.setVisibility(View.GONE);
                rl_cp_want.setVisibility(View.VISIBLE);
                ll_game.setVisibility(View.GONE);
                ll_audio.setVisibility(View.GONE);
                if (!TextUtils.isEmpty(mCpDesc)) {
                    et_cp_want.setText(mCpDesc);
                    et_cp_want.setSelection(mCpDesc.length());
                }
                break;
            case UserInfoSettingActivity.TYPE_EDIT_GAME_WANT:
                //想玩游戏
                tv_update_title.setText(getString(R.string.update_user_info_game_want_title));
                tv_update_tip.setText(getString(R.string.update_user_info_game_want_tip));
                rl_nick_name.setVisibility(View.GONE);
                fl_user_tag.setVisibility(View.GONE);
                rl_cp_want.setVisibility(View.GONE);
                ll_audio.setVisibility(View.GONE);
                ll_game.setVisibility(View.VISIBLE);
                break;
            case UserInfoSettingActivity.TYPE_EDIT_AUDIO:
                //语音标签
                tv_update_title.setText(getString(R.string.update_user_info_audio_title));
                tv_update_tip.setText(getString(R.string.update_user_info_audio_tip));
                rl_nick_name.setVisibility(View.GONE);
                fl_user_tag.setVisibility(View.GONE);
                rl_cp_want.setVisibility(View.GONE);
                ll_game.setVisibility(View.GONE);
                ll_audio.setVisibility(View.VISIBLE);
                iv_audio_record.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        switch (event.getAction()) {
                            case MotionEvent.ACTION_DOWN:
                                downTime = System.currentTimeMillis();
                                touched = true;
                                initAudioRecord();
                                break;
                            case MotionEvent.ACTION_CANCEL:
                            case MotionEvent.ACTION_UP:
                                upTime = System.currentTimeMillis();
                                if (upTime - downTime > 5000) {
                                    touched = false;
                                    onEndAudioRecord(false);
                                } else {
                                    touched = false;
                                    cancelAudioRecord();
                                    Toast.makeText(UpdateUserInfoActivity.this, getString(R.string.audio_timer_min_time), Toast.LENGTH_SHORT).show();
                                }
                                break;
                            case MotionEvent.ACTION_MOVE:
                                break;
                        }
                        return true;
                    }
                });
                break;
        }

    }

    private void initTags(final List<String> tags) {
        if (tags == null || tags.size() == 0) {
            return;
        }
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(15, 10, 15, 10);
        mTagStrList.clear();
        for (int i = 0; i < tags.size(); i++) {
            mTagStrList.add(tags.get(i));
        }
        fl_user_tag.removeAllViews();
        for (final String item : mTagStrList) {
            final TextView tv = new TextView(this);
            tv.setGravity(Gravity.CENTER);
            tv.setText(item);
            tv.setMaxEms(10);
            tv.setTextSize(14);
            tv.setPadding(ScreenUtil.dip2px(10), ScreenUtil.dip2px(10), ScreenUtil.dip2px(10), ScreenUtil.dip2px(10));
            JinquanResourceHelper.getInstance(UpdateUserInfoActivity.this).setTextColorByAttr(tv, R.attr.custom_attr_flow_item_text_color);
            JinquanResourceHelper.getInstance(UpdateUserInfoActivity.this).setBackgroundResourceByAttr(tv, R.attr.bg_feedback_cancel);
            tv.setSingleLine();
            tv.setLayoutParams(layoutParams);
            if (mUserTags != null && mUserTags.size() > 0 && mUserTags.contains(item)) {
                tv.setTextColor(Color.parseColor("#ffffff"));
                tv.setBackground(getResources().getDrawable(R.drawable.bg_common_button_save));
                mTags.add(item);
            } else {
                JinquanResourceHelper.getInstance(UpdateUserInfoActivity.this).setTextColorByAttr(tv, R.attr.custom_attr_flow_item_text_color);
                JinquanResourceHelper.getInstance(UpdateUserInfoActivity.this).setBackgroundResourceByAttr(tv, R.attr.bg_feedback_cancel);
                mTags.remove(item);
            }
            tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isSelected(item)) {
                        JinquanResourceHelper.getInstance(UpdateUserInfoActivity.this).setTextColorByAttr(tv, R.attr.custom_attr_flow_item_text_color);
                        JinquanResourceHelper.getInstance(UpdateUserInfoActivity.this).setBackgroundResourceByAttr(tv, R.attr.bg_feedback_cancel);
                        mTags.remove(item);
                    } else {
                        tv.setTextColor(Color.parseColor("#ffffff"));
                        tv.setBackground(getResources().getDrawable(R.drawable.bg_common_button_save));
                        mTags.add(item);
                    }
                }
            });
            fl_user_tag.addView(tv);
        }

    }

    @Override
    protected void onEventComing(EventCenter eventCenter) {
        super.onEventComing(eventCenter);
        switch (eventCenter.getEventCode()) {
            case Constants.EVENT_CODE_UPDATE_GAME_INFO_HONOR:
                HashMap<String, Object> honorMap = (HashMap<String, Object>) eventCenter.getData();
                List<EditGameBean> honorData = (List<EditGameBean>) honorMap.get("static");
                mStaticGameBean.honor_of_kings.platform = honorData.get(0);
                mStaticGameBean.honor_of_kings.zone = honorData.get(1);
                mStaticGameBean.honor_of_kings.rank = honorData.get(2);
                UserCenterInfo.Data.UserInfo.GameInfoBean.WzryBean data = (UserCenterInfo.Data.UserInfo.GameInfoBean.WzryBean) honorMap.get("data");
                mGameBean.setWzry(data);
                isReverseRank = true;
                checkGameData();
                setGameData();
                break;
            case Constants.EVENT_CODE_UPDATE_GAME_INFO_PEACE:
                HashMap<String, Object> peaceMap = (HashMap<String, Object>) eventCenter.getData();
                List<EditGameBean> peaceData = (List<EditGameBean>) peaceMap.get("static");
                mStaticGameBean.peace_elite.platform = peaceData.get(0);
                mStaticGameBean.peace_elite.zone = peaceData.get(1);
                mStaticGameBean.peace_elite.rank = peaceData.get(2);
                UserCenterInfo.Data.UserInfo.GameInfoBean.HpjyBean peaceBean = (UserCenterInfo.Data.UserInfo.GameInfoBean.HpjyBean) peaceMap.get("data");
                mGameBean.setHpjy(peaceBean);
                isReverseRank = true;
                checkGameData();
                setGameData();
                break;
            case Constants.EVENT_CODE_UPDATE_GAME_INFO_OTHER:
                List<EditGameBean.Item> otherData = (List<EditGameBean.Item>) eventCenter.getData();
                mStaticGameBean.other.list = (ArrayList<EditGameBean.Item>) otherData;
                mGameBean.getOther().clear();
                for (EditGameBean.Item item : mStaticGameBean.other.list) {
                    if (item.select) {
                        NormalGame normalGame = new NormalGame();
                        normalGame.set_id(Integer.parseInt(item.id));
                        normalGame.setIcon(item.image_url);
                        normalGame.setName(item.name);
                        mGameBean.getOther().add(normalGame);
                    }
                }
                checkGameData();
                setGameData();
                break;
        }

    }

    private void checkGameData() {
        if (mGameBean != null && mStaticGameBean != null) {
            peace_platform = setSelect(mGameBean.getHpjy().getPlatform(), mStaticGameBean.peace_elite.platform);
            peace_zoom = setSelect(mGameBean.getHpjy().getZone(), mStaticGameBean.peace_elite.zone);
            List<Integer> hpRank = mGameBean.getHpjy().getRank();
            if (isReverseRank) {
                Collections.reverse(hpRank);
            }
            peace_rank = setSelect(hpRank, mStaticGameBean.peace_elite.rank);

            honor_platform = setSelect(mGameBean.getWzry().getPlatform(), mStaticGameBean.honor_of_kings.platform);
            honor_zoom = setSelect(mGameBean.getWzry().getZone(), mStaticGameBean.honor_of_kings.zone);
            List<Integer> wzRank = mGameBean.getWzry().getRank();
            if (isReverseRank) {
                Collections.reverse(wzRank);
            }
            honor_rank = setSelect(wzRank, mStaticGameBean.honor_of_kings.rank);

            StringBuilder stringBuilder = new StringBuilder();
            List<NormalGame> other = mGameBean.getOther();
            if (other != null && other.size() > 0) {
                for (int i = 0; i < other.size(); i++) {
                    try {
                        mStaticGameBean.other.list.get(other.get(i).get_id() - 1).select = true;
                        stringBuilder.append(other.get(i).getName());
                        if (i != other.size() - 1) {
                            stringBuilder.append(" | ");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            other_games = stringBuilder.toString();
        }
    }

    private String setSelect(List<Integer> item, EditGameBean data) {
        StringBuilder stringBuilder = new StringBuilder();
        if (item != null && item.size() > 0) {
            for (int i = 0; i < item.size(); i++) {
                try {
                    data.items.get(item.get(i) - 1).select = true;
                    stringBuilder.append(data.items.get(item.get(i) - 1).name);
                    if (i != item.size() - 1) {
                        stringBuilder.append(" | ");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return stringBuilder.toString();
    }

    private void setGameData() {
        if (mGameBean.getHpjy() != null && mGameBean.getHpjy().getPlatform().size() > 0) {
            ll_game_heping_content.setVisibility(View.VISIBLE);
            tv_game_heping_info.setVisibility(View.GONE);
            tv_game_heping_platform.setText(peace_platform);
            tv_game_heping_area.setText(peace_zoom);
            tv_game_heping_rank.setText(peace_rank);
        } else {
            ll_game_heping_content.setVisibility(View.GONE);
            tv_game_heping_info.setVisibility(View.VISIBLE);
        }
        if (mStaticGameBean.peace_elite != null) {
            RequestOptions options = new RequestOptions()
                    .placeholder(R.mipmap.icon_hepingjingying)
                    .error(R.mipmap.icon_hepingjingying);
            Glide.with(this).load(mStaticGameBean.peace_elite.icon).apply(options).apply(RequestOptions.bitmapTransform(new RoundedCorners(12))).into(iv_heping);
        }
        if (mGameBean.getWzry() != null && mGameBean.getWzry().getPlatform().size() > 0) {
            ll_game_wangzhe_content.setVisibility(View.VISIBLE);
            tv_game_wangzhe_info.setVisibility(View.GONE);

            tv_game_wangzhe_platform.setText(honor_platform);
            tv_game_wangzhe_area.setText(honor_zoom);
            tv_game_wangzhe_rank.setText(honor_rank);
        } else {
            ll_game_wangzhe_content.setVisibility(View.GONE);
            tv_game_wangzhe_info.setVisibility(View.VISIBLE);
        }
        if (mStaticGameBean.honor_of_kings != null) {
            RequestOptions options = new RequestOptions()
                    .placeholder(R.mipmap.icon_wangzherongyao)
                    .error(R.mipmap.icon_wangzherongyao);
            Glide.with(this).load(mStaticGameBean.honor_of_kings.icon).apply(options).apply(RequestOptions.bitmapTransform(new RoundedCorners(12))).into(iv_wangzhe);
        }
        if (mGameBean.getOther() != null && mGameBean.getOther().size() > 0) {
            ll_game_other_content.setVisibility(View.VISIBLE);
            tv_game_other_info.setVisibility(View.GONE);
            mOtherGameAdapter = new CommonAdapter<NormalGame>(UpdateUserInfoActivity.this, R.layout.item_submit_other_game) {
                @Override
                public void convert(CommonViewHolder holder, NormalGame normalGame, int pos) {
                    ImageView iv_image = holder.getView(R.id.iv_image);
                    RequestOptions options = new RequestOptions()
                            .placeholder(R.mipmap.icon_default)
                            .error(R.mipmap.icon_default)
                            .transform(new CenterCrop(), new RoundedCorners(ScreenUtil.dip2px(6)));
                    Glide.with(UpdateUserInfoActivity.this)
                            .load(normalGame.getIcon())
                            .override(ScreenUtil.dip2px(40), ScreenUtil.dip2px(40))
                            .apply(options)
                            .into(iv_image);
                }
            };
            rv_other_game.setLayoutManager(new LinearLayoutManager(UpdateUserInfoActivity.this, RecyclerView.HORIZONTAL, false));
            mOtherGameAdapter.update(mGameBean.getOther());
            rv_other_game.setAdapter(mOtherGameAdapter);
            rv_other_game.addItemDecoration(new HorizontalItemDecoration(ScreenUtil.dip2px(8)));
        } else {
            ll_game_other_content.setVisibility(View.GONE);
            tv_game_other_info.setVisibility(View.VISIBLE);
        }
        if (mStaticGameBean.other != null) {
            RequestOptions options = new RequestOptions()
                    .transform(new CircleCrop())
                    .placeholder(R.mipmap.icon_wangzherongyao)
                    .error(R.mipmap.icon_wangzherongyao);
            Glide.with(this)
                    .load(mStaticGameBean.other.icon)
                    .apply(options)
                    .into(iv_other);
        }
    }


    private IAudioRecordCallback iAudioRecordCallback = new IAudioRecordCallback() {
        @Override
        public void onRecordReady() {
        }

        @Override
        public void onRecordStart(File audioFile, RecordType recordType) {
            started = true;
            if (!touched) {
                return;
            }
            playAudioRecordAnim();
        }

        @Override
        public void onRecordSuccess(File audioFile, long audioLength, RecordType recordType) {
            mAudioFile = audioFile;
            stopAudioRecordAnim();
        }

        @Override
        public void onRecordFail() {
        }

        @Override
        public void onRecordCancel() {

        }

        @Override
        public void onRecordReachedMaxTime(final int maxTime) {
            touched = false;
            if (audioMessageHelper != null) {
                audioMessageHelper.handleEndRecord(true, maxTime);
            }
        }
    };

    /**
     * 开始语音录制动画
     */
    private void playAudioRecordAnim() {
        rotateAnim();
        tv_audio_timer.setVisibility(View.VISIBLE);
        tv_audio_status.setVisibility(View.GONE);
        startTimer();
    }

    /**
     * 结束语音录制
     *
     * @param cancel
     */
    private void onEndAudioRecord(boolean cancel) {
        started = false;
        getWindow().setFlags(0, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        if (audioMessageHelper != null) {
            audioMessageHelper.completeRecord(cancel);
        }
        stopAudioRecordAnim();
    }

    /**
     * 结束语音录制动画
     */
    private void stopAudioRecordAnim() {
        if (isDestroyed() || isFinishing()) {
            return;
        }
        rl_audio_prepare.setVisibility(View.VISIBLE);
        fl_audio.setVisibility(View.GONE);
        tv_audio_timer.setVisibility(View.GONE);
        tv_audio_play_timer.setVisibility(View.VISIBLE);
        tv_audio_play_timer.setText(endTime + "s");
        iv_audio_play_status.setBackgroundResource(R.mipmap.icon_audio_play);
        iv_audio_record_anim.setVisibility(View.GONE);
        iv_audio_record_anim.clearAnimation();
        tv_audio_status.setVisibility(View.GONE);
        releaseTimer();
    }

    public void rotateAnim() {
        iv_audio_record_anim.setVisibility(View.VISIBLE);
        Animation anim = new RotateAnimation(0f, 360f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        anim.setDuration(1000);
        anim.setInterpolator(new LinearInterpolator());
        anim.setRepeatCount(Animation.INFINITE);
        anim.setRepeatMode(Animation.RESTART);
        iv_audio_record_anim.startAnimation(anim);
    }

    private void cancelAudioRecord() {
        if (!started) {
            return;
        }
        tv_audio_timer.setVisibility(View.GONE);
        iv_audio_record_anim.setVisibility(View.GONE);
        iv_audio_record_anim.clearAnimation();
        tv_audio_status.setVisibility(View.VISIBLE);
        if (audioMessageHelper != null) {
            audioMessageHelper.completeRecord(true);
        }
        releaseTimer();
    }

    private void startTimer() {
        if (mTimer2 == null && mTask2 == null) {
            mTimer2 = new Timer();
            mTask2 = new TimerTask() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mAudioTimer++;
                            if (mAudioTimer <= audioMaxTime) {
                                endTime = mAudioTimer;
                                if (tv_audio_timer != null){
                                    tv_audio_timer.setText(mAudioTimer + "s");
                                }
                            } else {
                                releaseTimer();
                            }
                        }
                    });
                }
            };
            mTimer2.schedule(mTask2, 0, 1000);
        }

    }

    private void releaseTimer() {
        mAudioTimer = 0;
        if (mTimer2 != null) {
            mTimer2.cancel();
            mTimer2 = null;
        }
        if (mTask2 != null) {
            mTask2.cancel();
            mTask2 = null;
        }
    }

    private void initAudioRecord() {
        checkAudioPermission();
        audioMessageHelper = new AudioRecorder(mContext, RecordType.AAC, audioMaxTime, iAudioRecordCallback);
        if (audioMessageHelper != null) {
            audioMessageHelper.startRecord();
        }
    }

    private void checkAudioPermission() {
        List<String> lackPermissions = NERtcEx.getInstance().checkPermission(mContext);
        if (!lackPermissions.isEmpty()) {
            String[] permissions = new String[lackPermissions.size()];
            for (int i = 0; i < lackPermissions.size(); i++) {
                permissions[i] = lackPermissions.get(i);
            }
            MPermission.with(UpdateUserInfoActivity.this).permissions(permissions).request();
        }
    }

    private boolean isSelected(String target) {
        for (String flag : mTags) {
            if (TextUtils.equals(target, flag)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        back();
    }

    private void back() {
        hideKeyBoard();
        new CommonDialogBuilder()
                .setTitle(getString(R.string.dialog_update_info_back_title))
                .setDesc(getString(R.string.dialog_update_info_back_tip))
                .setListener(new CommonActionListener() {
                    @Override
                    public void onConfirm() {
                        saveUserInfo();
                    }

                    @Override
                    public void onCancel() {
                        finish();
                    }
                }).create().show(getSupportFragmentManager(), CommonDialogFragment.class.getSimpleName());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }
}
