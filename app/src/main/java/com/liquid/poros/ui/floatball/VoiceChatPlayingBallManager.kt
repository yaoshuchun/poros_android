package com.liquid.poros.ui.floatball

import android.annotation.SuppressLint
import android.app.Activity
import android.app.ActivityManager
import android.content.ComponentName
import android.content.Context
import android.content.Context.ACTIVITY_SERVICE
import android.content.Intent
import android.content.ServiceConnection
import android.os.IBinder
import com.liquid.poros.service.AvCoreService
import com.liquid.poros.utils.FloatWindowViewManager
import com.liquid.poros.widgets.VoiceChatPlayingWindowView
import java.lang.ref.WeakReference


object VoiceChatPlayingBallManager {
    var voiceChatPlayingWindowView: WeakReference<VoiceChatPlayingWindowView>? = null
    var activity: WeakReference<Activity>? = null


    fun show(context: Activity){
        this.activity = WeakReference(context)
        activity?.get()?.let {
            if (voiceChatPlayingWindowView?.get() == null) {
                voiceChatPlayingWindowView = WeakReference(
                    VoiceChatPlayingWindowView(it)
                )
                voiceChatPlayingWindowView?.get()?.setOnViewClickListener {
                    moveToFront(context)
                }
            }

            FloatWindowViewManager.getInstance().saveSingleView(voiceChatPlayingWindowView?.get())
            FloatWindowViewManager.getInstance().showSingleView(
                it,
                VoiceChatPlayingWindowView::class.java,
                0,
                0,
                true
            )
            bindAvCoreService()
        }
    }

    @SuppressLint("ServiceCast")
    fun moveToFront(context: Activity){
        val activityManager: ActivityManager =
            context.getSystemService(ACTIVITY_SERVICE) as ActivityManager

        val taskInfoList: List<ActivityManager.RunningTaskInfo> =
            activityManager.getRunningTasks(100)

        for (taskInfo in taskInfoList) {
            if (taskInfo.topActivity.packageName.equals(context.packageName)) {
                activityManager.moveTaskToFront(taskInfo.id, 0)
                break
            }
        }

    }

    fun hide() {
        FloatWindowViewManager.getInstance().removeView(voiceChatPlayingWindowView?.get())
        unbindAvCoreService()
        voiceChatPlayingWindowView = null
        activity = null
    }

    fun unbindAvCoreService() {
        try {
            activity?.get()?.unbindService(mConnection)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun bindAvCoreService() {
        try {
            activity?.get()?.bindService(
                Intent(
                    activity?.get()!!,
                    AvCoreService::class.java
                ), mConnection, Context.BIND_AUTO_CREATE or Context.BIND_IMPORTANT
            )
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private val mConnection: ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName, service: IBinder) {}
        override fun onServiceDisconnected(name: ComponentName) {}
    }
}