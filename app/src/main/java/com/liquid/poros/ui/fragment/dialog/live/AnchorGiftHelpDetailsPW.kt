package com.liquid.poros.ui.fragment.dialog.live

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.blankj.utilcode.util.SizeUtils
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.request.RequestOptions
import com.faceunity.nama.ui.CircleImageView
import com.liquid.base.adapter.CommonAdapter
import com.liquid.base.adapter.CommonViewHolder
import com.liquid.base.event.EventCenter
import com.liquid.base.tools.StringUtil
import com.liquid.base.utils.ToastUtil
import com.liquid.poros.R
import com.liquid.poros.analytics.utils.MultiTracker
import com.liquid.poros.base.BaseActivity
import com.liquid.poros.constant.Constants
import com.liquid.poros.constant.TrackConstants
import com.liquid.poros.databinding.PopupAnchorGiftHelpDetailsBinding
import com.liquid.poros.entity.GiftBoostBean
import com.liquid.poros.entity.GiftBoostRankBean
import com.liquid.poros.utils.FileDownloadUtil.download
import com.liquid.poros.utils.FileDownloadUtil.parentPath
import com.liquid.poros.utils.LottieAnimationUtils
import com.liquid.poros.ui.activity.WebViewActivity
import com.liquid.poros.ui.activity.live.RoomLiveActivityV2
import com.liquid.poros.ui.activity.live.viewmodel.RoomLiveViewModel
import com.liquid.poros.ui.fragment.dialog.live.viewmodel.AnchorBoostGiftDetailsVM
import com.liquid.poros.utils.ScreenUtil
import com.liquid.poros.utils.ViewUtils
import de.greenrobot.event.EventBus
import de.greenrobot.event.Subscribe
import de.greenrobot.event.ThreadMode
import razerdp.basepopup.BasePopupWindow
import java.io.File
import java.util.*
import kotlin.collections.ArrayList

/**
 * 主持人礼物助力详情弹窗
 */
class AnchorGiftHelpDetailsPW(context: Context) : BasePopupWindow(context) {
    private lateinit var binding: PopupAnchorGiftHelpDetailsBinding
    private var mCommonAdapter: CommonAdapter<GiftBoostBean>? = null
    private lateinit var anchorBoostGiftDetailsVM: AnchorBoostGiftDetailsVM
    private var mRoomId: String? = null
    private var mHostId: String? = null
    private var mBoostGiftH5: String? = null//助力h5
    private var retryCount = 0
    private var audienceFrom:String? = null
    private var mGuestId: String? = null//女嘉宾id
    private var mAnchorId: String? = null//主持人id
    private var mHelpGiftName: String? = null//助力礼物名称
    private var mHelpGiftStatus: String? = null//助力礼物状态

    override fun onCreateContentView(): View {
        popupGravity = Gravity.END or Gravity.BOTTOM
        setPopupGravityMode(GravityMode.ALIGN_TO_ANCHOR_SIDE, GravityMode.RELATIVE_TO_ANCHOR)
        offsetX = SizeUtils.dp2px(5.0f)
        return createPopupById(R.layout.popup_anchor_gift_help_details)
    }

    override fun onViewCreated(contentView: View) {
        super.onViewCreated(contentView)
        binding = PopupAnchorGiftHelpDetailsBinding.bind(contentView)
        anchorBoostGiftDetailsVM = (context as BaseActivity).getActivityViewModel(AnchorBoostGiftDetailsVM::class.java)
        mCommonAdapter = object : CommonAdapter<GiftBoostBean>(context, R.layout.item_anchor_gift_help_details) {
            override fun convert(holder: CommonViewHolder, giftBoostBean: GiftBoostBean, pos: Int) {
                holder.getView<TextView>(R.id.item_tv_help_details_details_name).text = giftBoostBean.gift_name
                var rv_help_rank = holder.getView<RecyclerView>(R.id.item_rv_help_rank)
                var pb_help_details_progress = holder.getView<ProgressBar>(R.id.item_pb_help_details_progress)
                var tv_help_details_progress = holder.getView<TextView>(R.id.item_tv_help_details_progress)
                var tv_help_details_status = holder.getView<TextView>(R.id.item_tv_help_details_status)
                val options = RequestOptions()
                        .placeholder(R.mipmap.img_default_avatar)
                        .override(ScreenUtil.dip2px(44f), ScreenUtil.dip2px(44f))
                        .error(R.mipmap.img_default_avatar)
                Glide.with(context as BaseActivity)
                        .load(giftBoostBean.gift_icon)
                        .apply(options)
                        .into((holder.getView<View>(R.id.item_iv_anchor_help_details_gift) as ImageView))
                pb_help_details_progress.max = giftBoostBean.total_count
                pb_help_details_progress.progress = giftBoostBean.boost_count
                tv_help_details_progress.text = "${giftBoostBean.boost_count}/${giftBoostBean.total_count}"
                tv_help_details_status.text = if (giftBoostBean.total_count == giftBoostBean.boost_count) "继续助力" else "帮ta助力"
                tv_help_details_status.setOnClickListener {
                    val giftFileName = LottieAnimationUtils.getGiftFileName(giftBoostBean.gift_id?.toInt()!!)
                    if (!File(parentPath + File.separator + giftFileName).exists()) {
                        ToastUtil.show("请稍等，正在下载资源")
                        MultiTracker.onEvent(TrackConstants.GIFT_RESOURCE_UNDOWNLOAD, HashMap<String?, String?>())
                        retryCount++
                        if (retryCount >= 3) {
                            download(giftFileName, null)
                            retryCount = 0
                        }
                        return@setOnClickListener
                    }
                    (context as BaseActivity).getActivityViewModel(RoomLiveViewModel::class.java).sendGift(mRoomId, mHostId, giftBoostBean.gift_id!!.toInt(), giftBoostBean.gift_name!!, 1, true, -1,true)
                    mHelpGiftName = giftBoostBean.gift_name!!
                    mHelpGiftStatus = if (giftBoostBean.total_count == giftBoostBean.boost_count) "continue_assist" else "help_assist"
                    val params: MutableMap<String?, String?> = HashMap()
                    params["anchor_id"] = mAnchorId
                    params["guest_id"] = mGuestId
                    params["audience_from"] = audienceFrom
                    params["deliver_gift_name"] = giftBoostBean.gift_name
                    params["help_assist_type"] = if (giftBoostBean.total_count == giftBoostBean.boost_count) "continue_assist" else "help_assist"
                    MultiTracker.onEvent(TrackConstants.B_HELP_ASSIST_CLICK, params)
                }
                val commonAdapter: CommonAdapter<GiftBoostRankBean> = object : CommonAdapter<GiftBoostRankBean>(context, R.layout.item_cp_room_gift_rank) {
                    override fun convert(holder: CommonViewHolder, giftBoostRankBean: GiftBoostRankBean?, pos: Int) {
                        var iv_cp_room_gift_head = holder.getView<CircleImageView>(R.id.iv_cp_room_gift_head)
                        val options = RequestOptions()
                                .placeholder(R.mipmap.icon_boost_rank_seat)
                                .override(ScreenUtil.dip2px(26f), ScreenUtil.dip2px(26f))
                                .error(R.mipmap.icon_boost_rank_seat)
                        Glide.with(context as BaseActivity)
                                .load(giftBoostRankBean?.avatar_url)
                                .apply(options)
                                .into(iv_cp_room_gift_head)
                        iv_cp_room_gift_head.borderColor = Color.parseColor(if (StringUtil.isNotEmpty(giftBoostRankBean?.avatar_url)) "#10D8C8" else "#FFFFFF")
                    }
                }
                val linearLayoutManager = LinearLayoutManager(context)
                linearLayoutManager.orientation = LinearLayoutManager.HORIZONTAL
                rv_help_rank.layoutManager = linearLayoutManager
                rv_help_rank.adapter = commonAdapter
                commonAdapter.update(giftBoostBean.boost_male_guest_info_list)
                commonAdapter.notifyDataSetChanged()
            }
        }
        val linearLayoutManager = LinearLayoutManager(context)
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        binding.rvAnchorGiftHelpSetting.layoutManager = linearLayoutManager
        binding.rvAnchorGiftHelpSetting.adapter = mCommonAdapter
        initObserver()
        initListener()
        EventBus.getDefault().register(this)
    }

    private fun initListener() {
        binding.ivBack.setOnClickListener {
            dismiss()
            trackBackBoostDetails()
        }
        binding.ivAnchorHelpTip.setOnClickListener {
            if (ViewUtils.isFastClick()) {
                return@setOnClickListener
            }
            val bundle = Bundle()
            bundle.putString(Constants.TARGET_PATH, mBoostGiftH5)
            val intent = Intent(context, WebViewActivity::class.java)
            intent.putExtras(bundle)
            context.startActivity(intent)
            trackClickBoostDetailsTip()
        }
        binding.rlBoostGiftReward.setOnClickListener {
            (context as RoomLiveActivityV2).showBoostLotteryDialog()
        }
    }

    private fun initObserver() {
        anchorBoostGiftDetailsVM.boostGiftInfoLiveData.observe(context as BaseActivity, Observer {
            mCommonAdapter?.update(it.gift_boost_info_list?.let { it1 -> handBoostGiftDetails(it1) })
            mCommonAdapter?.notifyDataSetChanged()
            binding.tvAnchorHelpContent.text = "(${it?.tip})"
        })
    }

    override fun onShowing() {
        super.onShowing()
        trackPageBoostDetails()
    }

    /**
     * 获取助力详情
     */
    fun getBoostGiftDetails(roomId: String, hostId: String,lotteryNum:String) {
        mRoomId = roomId
        mHostId = hostId
        binding.tvOpenBoostCount.visibility = if (lotteryNum.toInt()>0) View.VISIBLE else View.GONE
        binding.tvOpenBoostCount.text = lotteryNum
        anchorBoostGiftDetailsVM.setBoostGift(mRoomId!!)
    }

    /**
     * 设置主持人头像
     */
    fun setHostHead(hostHead: String?, boostGiftH5: String?, guestId: String?, anchorId: String?) {
        mBoostGiftH5 = boostGiftH5
        mAnchorId = anchorId
        mGuestId = guestId
        val options = RequestOptions()
                .placeholder(R.mipmap.img_default_avatar)
                .override(ScreenUtil.dip2px(44f), ScreenUtil.dip2px(44f))
                .error(R.mipmap.img_default_avatar)
                .apply(RequestOptions.bitmapTransform(CircleCrop()))
        Glide.with(context as BaseActivity)
                .load(hostHead)
                .apply(options)
                .into((binding.ivAnchorHelpHead))
    }

    fun setAudienceFrom(audienceFrom:String?) {
        this.audienceFrom = audienceFrom
    }

    /**
     * 助力详情数据组装
     * (为了助力排行列表的组装)
     */
    private fun handBoostGiftDetails(giftBoostBean: List<GiftBoostBean>): List<GiftBoostBean>? {
        var giftBoostList = ArrayList<GiftBoostBean>()
        giftBoostBean.forEach {
            //创建总长度为总进度的集合
            var giftBoostRankList = ArrayList<GiftBoostRankBean>(it.total_count)
            if (!it.boost_male_guest_info_list.isNullOrEmpty()) {
                if (it.boost_count >= it.total_count) {
                    //进度完成 ,全部添加助力排行列表
                    giftBoostRankList.addAll(it.boost_male_guest_info_list!!)
                } else {
                    if (it.boost_male_guest_info_list!!.size <= it.total_count) {
                        //助力排行列表不为空,把服务端返回的数据加入集合
                        giftBoostRankList.addAll(it.boost_male_guest_info_list!!)
                        for (index in 0 until (it.total_count - it.boost_male_guest_info_list!!.size)) {
                            //少于总进度 - 助力列表长度的空缺,填充假数据
                            giftBoostRankList.add(GiftBoostRankBean(""))
                        }
                    }
                }
            } else {
                //助力排行列表为空,总长度为总进度的集合,全部填充假数据
                for (index in 0 until it.total_count) {
                    giftBoostRankList.add(GiftBoostRankBean(""))
                }
            }
            giftBoostList.add(GiftBoostBean(it.gift_name, it.gift_icon, it.gift_id, it.total_count, it.boost_count, giftBoostRankList))
        }
        return giftBoostList
    }

    @Subscribe(threadMode = ThreadMode.MainThread)
    fun onEventBus(eventCenter: EventCenter<*>) {
        when (eventCenter.eventCode) {
            Constants.EVENT_CODE_UPDATE_BOOST_COUNT -> {
                var boostCount = eventCenter.data as Int
                binding.tvOpenBoostCount.visibility = if (boostCount > 0) View.VISIBLE else View.GONE
                binding.tvOpenBoostCount.text = boostCount.toString()
            }
        }
    }

    /**
     * 助力礼物详情界面曝光
     */
    private fun trackPageBoostDetails(){
        val params: MutableMap<String, String?> = HashMap<String, String?>()
        params["anchor_id"] = mAnchorId
        params["guest_id"] = mGuestId
        MultiTracker.onEvent(TrackConstants.B_HEART_ASSIST_POPUP_EXPOSURE, params)
    }

    /**
     * 助力礼物详情点击退出
     */
    private fun trackBackBoostDetails(){
        val params: MutableMap<String, String?> = HashMap<String, String?>()
        params["anchor_id"] = mAnchorId
        params["guest_id"] = mGuestId
        MultiTracker.onEvent(TrackConstants.B_HEART_ASSIST_POPUP_CLOSE_CLICK, params)
    }

    /**
     * 助力礼物详情 - 点击助力规则
     */
    private fun trackClickBoostDetailsTip(){
        val params: MutableMap<String, String?> = HashMap<String, String?>()
        params["anchor_id"] = mAnchorId
        params["guest_id"] = mGuestId
        params["room_id"] = mRoomId
        MultiTracker.onEvent(TrackConstants.B_HEART_ASSIST_QUESTION_MARK_CLICK, params)
    }

    /**
     * 助力礼物结果事件
     */
    fun trackHelpBoost() {
        val params: MutableMap<String?, String?> = HashMap()
        params["anchor_id"] = mAnchorId
        params["guest_id"] = mGuestId
        params["deliver_gift_name"] = mHelpGiftName
        params["help_assist_type"] = mHelpGiftStatus
        params["audience_from"] = audienceFrom
        MultiTracker.onEvent(TrackConstants.B_HELP_ASSIST_SUCCESS_EXPOSURE, params)
    }

    override fun onDestroy() {
        super.onDestroy()
        EventBus.getDefault().unregister(this)
    }

}