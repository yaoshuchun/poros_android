package com.liquid.poros.ui.activity.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.liquid.library.retrofitx.RetrofitX
import com.liquid.poros.entity.MessageListBean
import com.liquid.poros.http.ApiUrl
import com.liquid.poros.http.observer.ObjectObserver
import com.liquid.poros.http.utils.postPoros

/**
 * 关于消息列表相关的网络请求
 * Created by yejiurui on 1/27/21.
 * Mail:yejiurui@liquidnetwork.com
 */
class MessageListModel : ViewModel() {
    private val NORMAL_MESSAGE = 0 //普通消息
    private val INTIMATE_MESSAGE = 1 //亲密消息

    private val SHOW_NORMAL_SCORE_SESSION = 0 //亲密消息入口隐藏
    private val SHOW_INTIMATE_SCORE_SESSION = 1 //亲密消息入口展示


    val messageListDataSuccess: MutableLiveData<MessageListBean> by lazy { MutableLiveData<MessageListBean>() }
    val messageListDataFail: MutableLiveData<MessageListBean> by lazy { MutableLiveData<MessageListBean>() }

    /**
     * 获取消息列表的数据
     */
    fun getIntimateListData(isExpire:Boolean) {
        var sessionType = if (isExpire) {
            NORMAL_MESSAGE
        }else {
            INTIMATE_MESSAGE
        }
        RetrofitX.url(ApiUrl.INTEMATE_LIST_URL)
                .param("is_expire_list",isExpire)
                .param("session_type",sessionType)
                .param("show_high_score_session",SHOW_NORMAL_SCORE_SESSION)
                .postPoros()
                .subscribe(object : ObjectObserver<MessageListBean>() {
                    override fun success(result: MessageListBean) {
                        messageListDataSuccess.postValue(result)
                    }

                    override fun failed(result: MessageListBean) {
                        messageListDataFail.postValue(result)
                    }
                })
    }
}