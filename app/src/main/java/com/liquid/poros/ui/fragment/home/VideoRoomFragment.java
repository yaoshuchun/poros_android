package com.liquid.poros.ui.fragment.home;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;
import com.liquid.poros.R;
import com.liquid.poros.adapter.ViewPagerAdapter;
import com.liquid.poros.base.BaseFragment;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.helper.JinquanResourceHelper;

import java.util.ArrayList;
import java.util.List;

import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;

/**
 * 视频房界面
 */
public class VideoRoomFragment extends BaseFragment {
    @BindView(R.id.tab_layout)
    public TabLayout tab_layout;
    @BindView(R.id.container)
    public ViewPager container;
    @BindView(R.id.ll_container)
    LinearLayout ll_container;
    @BindView(R.id.iv_title)
    ImageView iv_title;
    @BindView(R.id.fl_head)
    View fl_head;
    private List<String> mTabTitles = new ArrayList<>();

    public static VideoRoomFragment newInstance() {
        return new VideoRoomFragment();
    }

    @Override
    protected void onFirstUserVisible() {

    }

    @Override
    protected void onUserVisible() {

    }

    @Override
    protected void onUserInvisible() {

    }

    @Override
    protected View getLoadingTargetView() {
        return null;
    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.fragment_video_room;
    }

    @Override
    protected String getPageId() {
        return TrackConstants.PAGE_VIDEO_ROOM;
    }

    @Override
    protected void initViewsAndEvents(Bundle savedInstanceState) {
        mTabTitles.add(getString(R.string.discover_cp_live_title));
        mTabTitles.add(getString(R.string.discover_private_live_title));
        ViewPagerAdapter mAdapter = new ViewPagerAdapter(getChildFragmentManager());
        mAdapter.addFragment(DiscoverFragment.newInstance());
        mAdapter.addFragment(PrivateRoomFragment.newInstance());
        container.setAdapter(mAdapter);
        container.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tab_layout));
        tab_layout.addOnTabSelectedListener(new OnTabSelectedListener());
        setTabs(tab_layout, getLayoutInflater(), mTabTitles);
    }

    private void setTabs(TabLayout tabLayout, LayoutInflater inflater, List<String> tabTitles) {
        tabLayout.removeAllTabs();
        for (int i = 0; i < tabTitles.size(); i++) {
            TabLayout.Tab tab = tabLayout.newTab();
            View view = inflater.inflate(R.layout.item_video_room_tab, null);
            tab.setCustomView(view);
            ImageView iv_msg_tab = tab.getCustomView().findViewById(R.id.iv_msg_tab);
            TextView tv_msg_tab_title = view.findViewById(R.id.tv_msg_tab_title);
            RelativeLayout rl_tab = view.findViewById(R.id.rl_tab);
            tv_msg_tab_title.setText(tabTitles.get(i));
            if (i == 0) {
                tv_msg_tab_title.setTextColor(Color.parseColor("#1D1E20"));
                JinquanResourceHelper.getInstance(getActivity()).setBackgroundResourceByAttr(rl_tab, R.attr.bg_video_room_tab_item_select);
                iv_msg_tab.setBackgroundResource(R.mipmap.icon_cp_room_tab_select);
            } else {
                tv_msg_tab_title.setTextColor(Color.parseColor("#9EA4B0"));
                JinquanResourceHelper.getInstance(getActivity()).setBackgroundResourceByAttr(rl_tab, R.attr.bg_video_room_tab_item_normal);
                iv_msg_tab.setBackgroundResource(R.mipmap.icon_private_room_tab_normal_day);
            }
            tabLayout.addTab(tab);
        }
        JinquanResourceHelper.getInstance(getActivity()).setBackgroundResourceByAttr(tab_layout, R.attr.bg_video_room_tab);
    }

    public class OnTabSelectedListener implements TabLayout.OnTabSelectedListener {
        @Override
        public void onTabSelected(TabLayout.Tab tab) {
            container.setCurrentItem(tab.getPosition(), false);
            ImageView iv_msg_tab = tab.getCustomView().findViewById(R.id.iv_msg_tab);
            TextView tv_msg_tab_title = tab.getCustomView().findViewById(R.id.tv_msg_tab_title);
            rl_tab = tab.getCustomView().findViewById(R.id.rl_tab);
            JinquanResourceHelper.getInstance(getActivity()).setBackgroundResourceByAttr(rl_tab, R.attr.bg_video_room_tab_item_select);
            tv_msg_tab_title.setTextColor(Color.parseColor("#10D8C8"));
            if (tab.getPosition()==0){
                iv_msg_tab.setBackgroundResource(R.mipmap.icon_cp_room_tab_select);
            }else {
                iv_msg_tab.setBackgroundResource(R.mipmap.icon_private_room_tab_select);
            }
        }

        @Override
        public void onTabUnselected(TabLayout.Tab tab) {
            ImageView iv_msg_tab = tab.getCustomView().findViewById(R.id.iv_msg_tab);
            TextView tv_msg_tab_title = tab.getCustomView().findViewById(R.id.tv_msg_tab_title);
            rl_tab = tab.getCustomView().findViewById(R.id.rl_tab);
            JinquanResourceHelper.getInstance(getActivity()).setBackgroundResourceByAttr(rl_tab, R.attr.bg_video_room_tab_item_normal);
            JinquanResourceHelper.getInstance(getActivity()).setTextColorByAttr(tv_msg_tab_title, R.attr.custom_attr_nickname_text_color);
            if (tab.getPosition()==0){
                JinquanResourceHelper.getInstance(getActivity()).setBackgroundResourceByAttr(iv_msg_tab, R.attr.tab_cp_room);
            }else {
                JinquanResourceHelper.getInstance(getActivity()).setBackgroundResourceByAttr(iv_msg_tab, R.attr.tab_private_room);
            }
        }

        @Override
        public void onTabReselected(TabLayout.Tab tab) {

        }
    }

    RelativeLayout rl_tab;

    @Override
    public void notifyByThemeChanged() {
        JinquanResourceHelper.getInstance(getActivity()).setBackgroundColorByAttr(ll_container, R.attr.custom_attr_app_bg);
        JinquanResourceHelper.getInstance(getActivity()).setBackgroundColorByAttr(fl_head, R.attr.custom_attr_app_bg);
        JinquanResourceHelper.getInstance(getActivity()).setImageResourceByAttr(iv_title, R.attr.bg_discover_title);
        JinquanResourceHelper.getInstance(getActivity()).setBackgroundResourceByAttr(tab_layout, R.attr.bg_video_room_tab);

        JinquanResourceHelper.getInstance(getActivity()).setBackgroundResourceByAttr(rl_tab, R.attr.bg_video_room_tab_item_select);
        JinquanResourceHelper.getInstance(getActivity()).setBackgroundResourceByAttr(rl_tab, R.attr.bg_video_room_tab_item_normal);
    }

    @Override
    public void notifyNetworkChanged(boolean isConnected) {

    }
}
