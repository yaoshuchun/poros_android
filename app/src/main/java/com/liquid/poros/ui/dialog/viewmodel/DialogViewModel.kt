package com.liquid.poros.ui.dialog.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.liquid.library.retrofitx.RetrofitX
import com.liquid.poros.entity.RoomLiveInfo
import com.liquid.poros.http.ApiUrl
import com.liquid.poros.http.observer.ObjectObserver
import com.liquid.poros.http.utils.postPoros

/**
 * Created by yejiurui on 2021/1/21.
 * Mail:yejiurui@danke.com
 */
class DialogViewModel : ViewModel() {
    val roomInfoDataSuccess: MutableLiveData<RoomLiveInfo> by lazy { MutableLiveData<RoomLiveInfo>() }
    val roomInfoDataFail: MutableLiveData<RoomLiveInfo> by lazy { MutableLiveData<RoomLiveInfo>() }

    /**
     * 获取APP的全局变量信息
     */
    fun getLiveRoomInfo(roomId: String) {
        RetrofitX.url(ApiUrl.LIVEROOM_INFO_URL)
                .param("room_id", roomId)
                .postPoros()
                .subscribe(object : ObjectObserver<RoomLiveInfo>() {
                    override fun success(result: RoomLiveInfo) {
                        roomInfoDataSuccess.postValue(result)
                    }

                    override fun failed(result: RoomLiveInfo) {
                        roomInfoDataFail.postValue(result)
                    }
                })
    }
}