package com.liquid.poros.ui.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.liquid.poros.R;
import com.liquid.poros.adapter.GuarderRankAdapter;
import com.liquid.poros.base.BaseActivity;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.contract.user.GuardGiftRankContract;
import com.liquid.poros.contract.user.GuardGiftRankPresenter;
import com.liquid.poros.entity.GuardRankerBean;
import com.liquid.poros.entity.GuardRankerInfo;
import com.liquid.poros.entity.GuardUserInfo;
import com.liquid.poros.entity.SelfRankInfo;
import com.liquid.poros.ui.dialog.GuardRuleDialog;
import java.util.HashMap;
import java.util.List;
import static com.liquid.poros.constant.Constants.GIFT_RANK_PATH;

/**
 * 礼物榜页面
 */
public class AngelGuardNoticeActivity extends BaseActivity implements View.OnClickListener, GuardGiftRankContract.View{
    public static final String FEMALE_USER_ID = "female_user_id";
    private LinearLayout guardIntroduce;
    private ImageView giftAngelAvatar;
    private TextView giftAngelName;
    private RecyclerView rvGuardRankList;
    private GuarderRankAdapter adapter;
    private RoundedCorners roundedCorners;
    private GuardRuleDialog dialog;
    private GuardGiftRankContract.Presenter mPresent;
    private RequestOptions options;
    private TextView selftRank;
    private TextView tempNoCount;
    private ImageView selfAvatar;
    private ImageView back;
    private TextView selfName;
    private TextView coinCount;
    private String targetId;
    private String giftRankPath;

    @Override
    protected String getPageId() {
        return TrackConstants.GIFT_RANK_LIST_PAGE;
    }

    @Override
    protected void parseBundleExtras(Bundle extras) {
        targetId = extras.getString(FEMALE_USER_ID);
        giftRankPath = extras.getString(GIFT_RANK_PATH);
    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.activity_angel_guard;
    }

    protected boolean needSetTransparent() {
        return true;
    }

    @Override
    protected int getThemeTag() {
        return 1;
    }

    @Override
    protected HashMap<String, String> getParams() {
        HashMap<String,String> params = new HashMap<>();
        params.put("female_guest_id",targetId);
        params.put("event_time",String.valueOf(System.currentTimeMillis()));
        params.put("path",giftRankPath);
        return params;
    }

    @Override
    protected void initViewsAndEvents(Bundle savedInstanceState) {
        mPresent = new GuardGiftRankPresenter();
        dialog = new GuardRuleDialog(this);
        mPresent.attachView(this);
        roundedCorners= new RoundedCorners(100);
        options= new RequestOptions()
                .transform(new CenterCrop(),roundedCorners);
        guardIntroduce = findViewById(R.id.guard_introduce);
        giftAngelAvatar = findViewById(R.id.gift_angel_avatar);
        giftAngelName = findViewById(R.id.gift_angel_name);
        rvGuardRankList = findViewById(R.id.rv_guarder_rank_list);
        selftRank = findViewById(R.id.user_rank_num);
        selfAvatar = findViewById(R.id.rank_user_avatar);
        selfName = findViewById(R.id.rank_user_name);
        coinCount = findViewById(R.id.coin_count);
        tempNoCount = findViewById(R.id.temp_no_count);
        back = findViewById(R.id.back);
        adapter = new GuarderRankAdapter(this);
        rvGuardRankList.setLayoutManager(new LinearLayoutManager(this));
        rvGuardRankList.setAdapter(adapter);
        mPresent.getUserRankInfo(targetId);
        initListener();
    }

    private void initListener() {
        guardIntroduce.setOnClickListener(this);
        back.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.guard_introduce:
                dialog.show();
                break;
            case R.id.back:
                finish();
                break;
        }
    }

    @Override
    public void onRankInfo(GuardRankerBean bean) {
        if (bean != null) {
            GuardUserInfo guardInfo = bean.getGuard_info();
            List<GuardRankerInfo> rankList = bean.getRank_list();
            SelfRankInfo selfRankInfo = bean.getSelf_rank_info();
            giftAngelName.setVisibility(View.VISIBLE);
            if (guardInfo != null && guardInfo.getNick_name() != null) {
                giftAngelName.setText(guardInfo.getNick_name());
                Glide.with(this).applyDefaultRequestOptions(options).load(guardInfo.getAvatar_url())
                        .into(giftAngelAvatar);
                giftAngelAvatar.setVisibility(View.VISIBLE);
            }else {
                giftAngelAvatar.setVisibility(View.INVISIBLE);
                giftAngelName.setText("虚位以待");
            }
            adapter.setDatas(rankList);
            if (selfRankInfo != null) {
                if (selfRankInfo.getRank() > 9) {
                    selftRank.setText(String.valueOf(selfRankInfo.getRank()));
                }else {
                    selftRank.setText("0" + (selfRankInfo.getRank()));
                }
                if (selfRankInfo.getRank() == 0) {
                    tempNoCount.setVisibility(View.VISIBLE);
                }else {
                    tempNoCount.setVisibility(View.INVISIBLE);
                }
                selfName.setText(selfRankInfo.getNick_name());
                Glide.with(this)
                        .applyDefaultRequestOptions(options)
                        .load(selfRankInfo.getAvatar_url())
                        .into(selfAvatar);
                coinCount.setText(String.valueOf((int)selfRankInfo.getCoins()));
            }
        }
    }
}
