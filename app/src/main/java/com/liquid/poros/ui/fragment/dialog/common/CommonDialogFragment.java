package com.liquid.poros.ui.fragment.dialog.common;

import android.app.Dialog;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.liquid.poros.R;
import com.liquid.poros.base.BaseDialogFragment;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.constant.TrackConstants;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;


public class CommonDialogFragment extends BaseDialogFragment implements View.OnClickListener {

    private String desc;
    private String title;
    private String middleTitle;
    private int imageTitle;
    private int topImageTitle;
    private String confirm;
    private int confirmRes;
    private boolean withCancel;
    private boolean cancelAble;
    private String cancel;
    private int orientation;
    private CharSequence descSequence;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_confirm:
                if (listener != null) {
                    listener.onConfirm();
                }
                break;
            case R.id.tv_cancel:
                if (listener != null) {
                    listener.onCancel();
                }
                break;
        }
        dismissDialog();

    }

    private CommonActionListener listener;

    protected void setCommonActionListener(CommonActionListener listener) {
        this.listener = listener;
    }


    @Override
    protected String getPageId() {
        return TrackConstants.PAGE_COMMENT_DIALOG;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        desc = getArguments().getString(Constants.PARAMS_DESC);
        descSequence = getArguments().getCharSequence(Constants.PARAMS_DESC_SEQUENCE);
        title = getArguments().getString(Constants.PARAMS_TITLE);
        middleTitle = getArguments().getString(Constants.PARAMS_MIDDLE_TITLE);
        imageTitle = getArguments().getInt(Constants.PARAMS_IMAGE_TITLE);
        topImageTitle = getArguments().getInt(Constants.PARAMS_TOP_IMAGE_TITLE);
        confirm = getArguments().getString(Constants.PARAMS_CONFIRM);
        confirmRes = getArguments().getInt(Constants.PARAMS_CONFIRM_BACKGROUND);
        withCancel = getArguments().getBoolean(Constants.PARAMS_WITH_CANCEL);
        cancel = getArguments().getString(Constants.PARAMS_CANCEL);
        cancelAble = getArguments().getBoolean(Constants.PARAMS_CANCEL_ABLE);
        orientation = getArguments().getInt(Constants.PARAMS_ORIENTATION, ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    @Override
    public void onStart() {
        super.onStart();
        Window win = getDialog().getWindow();
        // 一定要设置Background，如果不设置，window属性设置无效
        win.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        setCancelable(cancelAble);
        WindowManager.LayoutParams params = win.getAttributes();
        params.gravity = Gravity.CENTER;
        // 使用ViewGroup.LayoutParams，以便Dialog 宽度充满整个屏幕
        if (orientation == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE) {
            params.width = (int) (dm.heightPixels * 0.78);
        } else {
            params.width = (int) (dm.widthPixels * 0.78);
        }
        win.setAttributes(params);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_common_dialog, null);

        TextView tv_confirm = view.findViewById(R.id.tv_confirm);
        TextView tv_cancel = view.findViewById(R.id.tv_cancel);
        TextView tv_title = view.findViewById(R.id.tv_title);
        ImageView iv_title = view.findViewById(R.id.iv_title);
        TextView tv_middle_title = view.findViewById(R.id.tv_middle_title);
        ImageView iv_top_title = view.findViewById(R.id.iv_top_title);
        TextView tv_desc = view.findViewById(R.id.tv_desc);

        tv_confirm.setOnClickListener(this);
        tv_cancel.setOnClickListener(this);

        if (TextUtils.isEmpty(descSequence) && TextUtils.isEmpty(desc)) {
            tv_desc.setVisibility(View.GONE);
        } else {
            tv_desc.setVisibility(View.VISIBLE);
            tv_desc.setText(TextUtils.isEmpty(descSequence) ? desc : descSequence);
        }
        if (TextUtils.isEmpty(middleTitle) && TextUtils.isEmpty(middleTitle)) {
            tv_middle_title.setVisibility(View.GONE);
        } else {
            tv_middle_title.setVisibility(View.VISIBLE);
            tv_middle_title.setText(middleTitle);
        }

        if (!TextUtils.isEmpty(confirm)) {
            tv_confirm.setText(confirm);
        }
        tv_confirm.setBackgroundResource(confirmRes != 0 ? confirmRes : R.drawable.bg_make_up_confirm);
        if (!TextUtils.isEmpty(cancel)) {
            tv_cancel.setText(cancel);
        }
        tv_cancel.setVisibility(withCancel ? View.VISIBLE : View.GONE);
        if (imageTitle == 0) {
            tv_title.setVisibility(TextUtils.isEmpty(title) ? View.GONE : View.VISIBLE);
            tv_title.setText(title);
            iv_title.setVisibility(View.GONE);
        } else {
            tv_title.setVisibility(View.GONE);
            iv_title.setVisibility(View.VISIBLE);
            iv_title.setBackgroundResource(imageTitle);
        }
        if (topImageTitle == 0) {
            iv_top_title.setVisibility(View.GONE);
        }else{
            iv_top_title.setVisibility(View.VISIBLE);
            iv_top_title.setImageResource(topImageTitle);
//            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
//            params.setMargins(20,8,20,0);
//            tv_desc.setLayoutParams(params);
        }
        builder.setView(view);
        return builder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }


}
