package com.liquid.poros.ui.fragment.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.liquid.base.utils.ToastUtil
import com.liquid.library.retrofitx.RetrofitX
import com.liquid.poros.entity.UserCenterInfoV2
import com.liquid.poros.entity.UserPhotoInfoV2
import com.liquid.poros.http.ApiUrl
import com.liquid.poros.http.observer.ObjectObserver
import com.liquid.poros.http.utils.postPoros

/**
 * 个人信息ViewModel
 */
class UserInfoViewModel : ViewModel() {

    val userInfoLiveData: MutableLiveData<UserCenterInfoV2> by lazy { MutableLiveData() }
    val userPhotoLiveData: MutableLiveData<UserPhotoInfoV2> by lazy { MutableLiveData() }

    /**
     * 获取用户信息
     */
    fun getUserInfo(target_user_id: String?) {
        RetrofitX.url(ApiUrl.USER_INFO_OTHER)
                .param("target_user_id", target_user_id)
                .postPoros()
                .subscribe(object : ObjectObserver<UserCenterInfoV2>() {
                    override fun success(result: UserCenterInfoV2) {
                        userInfoLiveData.postValue(result)
                    }

                    override fun failed(result: UserCenterInfoV2) {
                        ToastUtil.show(result.msg)
                    }

                })
    }

    /**
     * 获取用户照片
     */
    fun getUserPhotos(target_user_id: String?) {
        RetrofitX.url(ApiUrl.USER_PHOTOS)
                .param("target_user_id", target_user_id)
                .param("page", 0)
                .param("page_size", 20)
                .postPoros()
                .subscribe(object : ObjectObserver<UserPhotoInfoV2>() {
                    override fun success(result: UserPhotoInfoV2) {
                        userPhotoLiveData.postValue(result)
                    }

                    override fun failed(result: UserPhotoInfoV2) {
                        ToastUtil.show(result.msg)
                    }

                })
    }
}