package com.liquid.poros.ui.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.liquid.poros.R;
import com.liquid.poros.base.BaseActivity;
import com.liquid.poros.widgets.CustomHeader;

public class GiftBoxRuleActivity extends BaseActivity {

    @Override
    protected String getPageId() {
        return null;
    }

    @Override
    protected void parseBundleExtras(Bundle extras) {

    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.layout_gift_box_rule;
    }

    @Override
    protected void initViewsAndEvents(Bundle savedInstanceState) {
        TextView tv_title = findViewById(R.id.tv_title);
        tv_title.setText("玩法规则");

        findViewById(R.id.tv_recharge_details).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                readyGo(LotteryRecordActivity.class);
            }
        });
    }


}
