package com.liquid.poros.ui.fragment.live

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.liquid.base.adapter.CommonAdapter
import com.liquid.base.adapter.CommonViewHolder
import com.liquid.poros.R
import com.liquid.poros.base.BaseActivity
import com.liquid.poros.base.BaseFragment
import com.liquid.poros.constant.Constants
import com.liquid.poros.entity.BoostLotteryRecordInfo
import com.liquid.poros.ui.fragment.live.viewmodel.BoostLotteryRecordVM
import com.liquid.poros.utils.ScreenUtil
import com.liquid.poros.widgets.pulltorefresh.BaseRefreshListener
import com.liquid.poros.widgets.pulltorefresh.ViewStatus
import kotlinx.android.synthetic.main.activity_withdrawal_history.container
import kotlinx.android.synthetic.main.fragment_boost_lottery_record.*

/**
 * 初级&高级宝箱中奖记录
 */
class BoostLotteryRecordFragment : BaseFragment(), BaseRefreshListener {
    private var mCommonAdapter: CommonAdapter<BoostLotteryRecordInfo.BoostRecordBean>? = null
    private val boostLotteryRecordVM: BoostLotteryRecordVM by lazy { getFragmentViewModel(BoostLotteryRecordVM::class.java) }
    private var mBoostLotteryRecordType: String? = "boost_cm"
    private var mLastId: String? = null
    private val boostRecordList = ArrayList<BoostLotteryRecordInfo.BoostRecordBean>()

    companion object {
        fun newInstance(boostLotteryRecordType: String): BoostLotteryRecordFragment? {
            val fragment: BoostLotteryRecordFragment = BoostLotteryRecordFragment()
            val bundle = Bundle()
            bundle.putString(Constants.BOOST_LOTTERY_RECORD_TYPE, boostLotteryRecordType)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun getPageId(): String {
        return ""
    }

    override fun getContentViewLayoutID(): Int {
        return R.layout.fragment_boost_lottery_record
    }

    override fun notifyNetworkChanged(isConnected: Boolean) {

    }

    override fun initViewsAndEvents(savedInstanceState: Bundle?) {
        mBoostLotteryRecordType = arguments?.get(Constants.BOOST_LOTTERY_RECORD_TYPE) as String
        rv_boost_lottery_record!!.layoutManager = LinearLayoutManager(activity)
        mCommonAdapter = object : CommonAdapter<BoostLotteryRecordInfo.BoostRecordBean>(activity, R.layout.item_boost_lottery_record) {
            override fun convert(holder: CommonViewHolder, boostRecordBean: BoostLotteryRecordInfo.BoostRecordBean, pos: Int) {
                val options = RequestOptions()
                        .placeholder(R.mipmap.img_default_avatar)
                        .override(ScreenUtil.dip2px(44f), ScreenUtil.dip2px(44f))
                        .error(R.mipmap.img_default_avatar)
                Glide.with(context as BaseActivity)
                        .load(boostRecordBean.gift_image)
                        .apply(options)
                        .into((holder.getView<View>(R.id.item_iv_boost_lottery_record) as ImageView))
                holder.getView<TextView>(R.id.item_tv_boost_lottery_record_name).text = if (boostRecordBean.gift_num == 1) boostRecordBean.gift_name else "${boostRecordBean.gift_name}*${boostRecordBean.gift_num}"
                holder.getView<TextView>(R.id.item_tv_boost_lottery_record_time).text = boostRecordBean.create_time
            }
        }
        rv_boost_lottery_record!!.adapter = mCommonAdapter
        container!!.setRefreshListener(this)
        container!!.setCanLoadMore(true)
        initObserver()
    }

    private fun initObserver() {
        boostLotteryRecordVM.boostLotteryRecord(mBoostLotteryRecordType!!, "")
        boostLotteryRecordVM.boostLotteryRecordLiveData.observe(this, Observer {
            container.finishRefresh()
            container.finishLoadMore()
            if (mLastId == null) {
                if (it?.record_list.isNullOrEmpty()) {
                    container.showView(ViewStatus.EMPTY_STATUS)
                    container.setCanLoadMore(false)
                    return@Observer
                }
                boostRecordList?.clear()
                it?.record_list?.let { it1 -> boostRecordList?.addAll(it1) }
                mCommonAdapter?.update(boostRecordList)
            } else {
                mCommonAdapter?.add(it?.record_list)
            }
            mLastId = it?.last_id
            container.showView(ViewStatus.CONTENT_STATUS)
        })
    }

    override fun loadMore() {
        mLastId?.let { boostLotteryRecordVM.boostLotteryRecord(mBoostLotteryRecordType!!, it) }
    }

    override fun refresh() {
        mLastId = null
        boostLotteryRecordVM.boostLotteryRecord(mBoostLotteryRecordType!!, "")
    }

    override fun onUserInvisible() {
    }


    override fun onFirstUserVisible() {
    }

    override fun notifyByThemeChanged() {
    }

    override fun onUserVisible() {
    }

    override fun getLoadingTargetView() = null


}