package com.liquid.poros.ui.activity.live.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.liquid.library.retrofitx.RetrofitX
import com.liquid.library.retrofitx.utils.LogUtils
import com.liquid.poros.entity.*
import com.liquid.poros.http.ApiUrl
import com.liquid.poros.http.observer.ObjectObserver
import com.liquid.poros.http.utils.postPoros

/**
 * 视频速配ViewModel
 */
class VideoFastDatingViewModel :ViewModel() {
    val userCoinLiveData: MutableLiveData<UserCoin> by lazy { MutableLiveData() } //用户可用金币
    val videoMatchInfoLiveData: MutableLiveData<VideoMatchInfo> by lazy { MutableLiveData() } //视频速配信息
    val videoMatchCoinNotEnough: MutableLiveData<VideoMatchInfo> by lazy { MutableLiveData() } //视频速配,金币不足
    val cancelVideoMatchMicLiveData: MutableLiveData<BaseBean> by lazy { MutableLiveData() } //男用户下麦成功
    val checkVideoMatchMicLiveData: MutableLiveData<CheckInfo> by lazy { MutableLiveData() } //check速配成功
    val checkVideoMatchMicFailLiveData: MutableLiveData<CheckInfo> by lazy { MutableLiveData() } //check速配失败


    /**
     * check速配接口
     */
    fun checkVideoMatchMic(live_id: String, room_id: String,video_room_id:String) {
        RetrofitX.url(ApiUrl.CHECK_VIDEO_MATCH_MIC)
                .param("live_id", live_id)
                .param("close_reson", room_id)
                .param("close_reson", video_room_id)
                .postPoros().subscribe(object : ObjectObserver<CheckInfo>() {
                    override fun success(result: CheckInfo) {
                        checkVideoMatchMicLiveData.postValue(result)
                    }

                    override fun failed(result: CheckInfo) {
                        checkVideoMatchMicFailLiveData.postValue(result)
                    }

                })
    }


    /**
     * 获取金币信息
     */
    fun getUserCoin(){
        LogUtils.d("请求时间----"+System.currentTimeMillis())
        RetrofitX.url(ApiUrl.GET_USER_COIN)
                .postPoros().subscribe(object : ObjectObserver<UserCoin>() {
                    override fun success(result: UserCoin) {
                        userCoinLiveData.postValue(result)
                    }

                    override fun failed(result: UserCoin) {
                    }

                })
    }
    /**
     * 视频速配接口
     */
    fun getVideoMatch(){
        LogUtils.d("请求时间2----"+System.currentTimeMillis())
        RetrofitX.url(ApiUrl.GET_VIDEO_MATCH_INFO)
                .postPoros().subscribe(object : ObjectObserver<VideoMatchInfo>() {
                    override fun success(result: VideoMatchInfo) {
                        videoMatchInfoLiveData.postValue(result)
                    }

                    override fun failed(result: VideoMatchInfo) {
                            if(result.code == 20000){
                                videoMatchCoinNotEnough.postValue(result)
                            }
                    }

                })
    }


}