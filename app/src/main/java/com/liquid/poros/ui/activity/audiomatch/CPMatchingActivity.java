package com.liquid.poros.ui.activity.audiomatch;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.liquid.poros.R;
import com.liquid.poros.analytics.utils.MultiTracker;
import com.liquid.poros.base.BaseActivity;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.ui.fragment.dialog.common.CommonActionListener;
import com.liquid.poros.ui.fragment.dialog.common.CommonDialogBuilder;
import com.liquid.poros.ui.fragment.dialog.common.CommonDialogFragment;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.poros.utils.StatusBarUtil;

import java.util.HashMap;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Group;
import butterknife.BindView;
import butterknife.OnClick;

/**
 * 速配中页面
 */
public class CPMatchingActivity extends BaseActivity {

    public static final String CP_GAME_WZRY = "wzry"; //王者荣耀游戏速配
    public static final String CP_GAME_HPJY = "hpjy"; //和平精英游戏速配
    public static final String CP_AUDIO = "chat"; //语音速配

    public static final String REQUEST_IS_CHANGE = "IS_CHANGE"; //换一个重新请求速配
    public static final String REQUEST_MATCHING = "REQUEST_MATCHING"; //重新请求速配
    public static final String CP_TYPE = "CP_TYPE"; //速配类型
    public static final String GAME_AREA = "GAME_AREA"; //游戏大区
    public static final String GAME_LEVEL = "GAME_LEVEL"; //游戏等级
    public static final String MATCHING_TIME_MAX = "MATCHING_TIME_MAX"; //速配最大时间
    private String currentCPType = CP_AUDIO; //当前速配类型
    private boolean isRequest = false; //请求语音速配
    private boolean isChange = false; //换一个重新请求速配
    private MediaPlayer mMediaPlayer;
    @BindView(R.id.content_layout)
    ConstraintLayout contentLayout;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.group_game)
    Group groupGame;
    @BindView(R.id.tv_area)
    TextView tvArea;
    @BindView(R.id.tv_level)
    TextView tvLevel;
    @BindView(R.id.progress_matching)
    ImageView progressMatching;
    private String area;
    private String level;
    private int zoneId;
    private int rankId;

    @Override
    protected String getPageId() {
        return TrackConstants.PAGE_MATCHING;
    }

    @Override
    protected void parseBundleExtras(Bundle extras) {
        currentCPType = extras.getString(CP_TYPE, CP_AUDIO);
        area = extras.getString(GAME_AREA);
        level = extras.getString(GAME_LEVEL);
        isRequest = extras.getBoolean(REQUEST_MATCHING, false);
        zoneId = extras.getInt(Constants.AUDIO_MATCH_INFO_ZONE);
        rankId = extras.getInt(Constants.AUDIO_MATCH_INFO_RANK);
        long matchingMaxSeconds = extras.getLong(MATCHING_TIME_MAX);
        isChange = extras.getBoolean(REQUEST_IS_CHANGE, false);
        CPMatchingGlobal.getInstance(this).setMatchingMaxTime(matchingMaxSeconds * 1000);
        CPMatchingGlobal.getInstance(this).saveCurrentCPParams(currentCPType, zoneId, rankId, area, level);
    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.activity_cp_matching;
    }

    @Override
    protected void initViewsAndEvents(Bundle saveceState) {
        StatusBarUtil.setPaddingTop(this, contentLayout);
        initViewByCPType(area, level);
        //如果重新请求则当前并没有匹配，否则当前正在进行匹配
        if (isRequest) {
            CPMatchingGlobal.getInstance(this).requestMatching(isChange, "");
        } else {
            CPMatchingGlobal.getInstance(this).startCountDown();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        CPMatchingGlobal.getInstance(this).hideMatchingBall();
        playMatchingMedia();
    }

    private void initViewByCPType(String area, String level) {
        Glide.with(mContext).asGif().load(R.mipmap.matching_loading).into(progressMatching);
        String title = "聊天陪伴";
        boolean showGameGroup = false;
        if (CP_GAME_WZRY.equals(currentCPType)) {
            title = "王者荣耀语音开黑速配";
            showGameGroup = true;
        } else if (CP_GAME_HPJY.equals(currentCPType)) {
            title = "和平精英语音开黑速配";
            showGameGroup = true;
        }
        tvTitle.setText(title);
        groupGame.setVisibility(showGameGroup ? View.VISIBLE : View.GONE);
        if (showGameGroup) {
            tvArea.setText(area);
            tvLevel.setText(level);
        }
    }

    @OnClick({R.id.icon_back, R.id.tv_pack_up})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.icon_back:
                alertStopMatchDialog();
                break;
            case R.id.tv_pack_up:
                HashMap<String, String> params = new HashMap<>();
                params.put("event_time", String.valueOf(System.currentTimeMillis()));
                params.put("user_id", AccountUtil.getInstance().getUserId());
                MultiTracker.onEvent("b_close_wait_quick_team_join_click", params);
                releaseMatchingMedia();
                CPMatchingGlobal.getInstance(this).showMatchingBall();
                finish();
                break;
        }
    }

    @Override
    protected int getThemeTag() {
        return 0;
    }

    /**
     * 弹出停止速配对话框
     */
    private void alertStopMatchDialog() {
        try {
            new CommonDialogBuilder()
                    .setTitle("结束速配")
                    .setDesc("您确定要结束速配吗？")
                    .setCancel("残忍结束")
                    .setConfirm("再等一会儿")
                    .setListener(new CommonActionListener() {
                        @Override
                        public void onConfirm() {
                            HashMap<String, String> params = new HashMap<>();
                            params.put("event_time", String.valueOf(System.currentTimeMillis()));
                            params.put("user_id", AccountUtil.getInstance().getUserId());
                            params.put("result", "wait");
                            MultiTracker.onEvent("b_stop_wait_quick_team_join_click", params);
                        }

                        @Override
                        public void onCancel() {
                            try {
                                HashMap<String, String> params = new HashMap<>();
                                params.put("event_time", String.valueOf(System.currentTimeMillis()));
                                params.put("user_id", AccountUtil.getInstance().getUserId());
                                params.put("result", "end");
                                MultiTracker.onEvent("b_stop_wait_quick_team_join_click", params);
                                releaseMatchingMedia();
                                CPMatchingGlobal.getInstance(CPMatchingActivity.this).cancelMatching();
                                finish();
                            } catch (Throwable e) {
                                e.printStackTrace();
                            }
                        }
                    }).create().show(getSupportFragmentManager(), CommonDialogFragment.class.getSimpleName());

            HashMap<String, String> params = new HashMap<>();
            params.put("event_time", String.valueOf(System.currentTimeMillis()));
            params.put("user_id", AccountUtil.getInstance().getUserId());
            MultiTracker.onEvent("b_stop_wait_quick_team_join_exposure", params);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    /**
     * 速配等待语音提示
     */
    private void playMatchingMedia() {
        try {
            if (mMediaPlayer == null) {
                mMediaPlayer = MediaPlayer.create(this, R.raw.audio_match_waiting);
                mMediaPlayer.setLooping(true);
                mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            }
            mMediaPlayer.start();
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    private void releaseMatchingMedia() {
        if (mMediaPlayer != null) {
            mMediaPlayer.stop();
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        releaseMatchingMedia();
    }
}
