package com.liquid.poros.ui.fragment.dialog.live;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.liquid.poros.R;
import com.liquid.poros.base.BaseDialogFragment;
import com.liquid.poros.constant.TrackConstants;

import java.util.Timer;
import java.util.TimerTask;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

/**
 * 专属视频房充值弹窗
 */
public class PrivateRoomRechargeDialogFragment extends BaseDialogFragment implements View.OnClickListener {
    private TextView tv_title;
    private TextView tv_desc;
    private TextView tv_cancel;
    private TextView tv_confirm;
    private String mContent;
    private String mDesc;
    private int mCountDown = 120;
    private TimerTask mTask2;
    private Timer mTimer2;

    public PrivateRoomRechargeDialogFragment setDialogInfo(String content, String desc, int countDown) {
        this.mContent = content;
        this.mDesc = desc;
        this.mCountDown = countDown;
        return this;
    }

    public static PrivateRoomRechargeDialogFragment newInstance() {
        PrivateRoomRechargeDialogFragment fragment = new PrivateRoomRechargeDialogFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    protected String getPageId() {
        return TrackConstants.PAGE_PRIVATE_ROOM_RECHARGE;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onStart() {
        super.onStart();
        Window win = getDialog().getWindow();
        // 一定要设置Background，如果不设置，window属性设置无效
        win.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams params = win.getAttributes();
        params.gravity = Gravity.CENTER;
        setCancelable(false);
        // 使用ViewGroup.LayoutParams，以便Dialog 宽度充满整个屏幕
        params.width = (int) (dm.widthPixels * 0.8);
        win.setAttributes(params);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_room_recharge_dialog, null);
        tv_title = view.findViewById(R.id.tv_title);
        tv_desc = view.findViewById(R.id.tv_desc);
        tv_cancel = view.findViewById(R.id.tv_cancel);
        tv_confirm = view.findViewById(R.id.tv_confirm);
        tv_title.setText(mContent);
        tv_cancel.setOnClickListener(this);
        tv_confirm.setOnClickListener(this);
        if (mTimer2 == null && mTask2 == null) {
            mTimer2 = new Timer();
            mTask2 = new TimerTask() {
                @Override
                public void run() {
                    tv_desc.post(new Runnable() {
                        @Override
                        public void run() {
                            mCountDown--;
                            if (mCountDown <= 0) {
                                if (listener != null) {
                                    listener.onCancel();
                                }
                                releaseTimer();
                                dismissDialog();
                            } else {
                                tv_desc.setText("(" + mDesc + mCountDown + ")");
                            }
                        }
                    });

                }
            };
            mTimer2.schedule(mTask2, 0, 1000);
        }

        builder.setView(view);
        return builder.create();
    }

    private PrivateRoomRechargeDialogFragment.PrivateRoomRechargeListener listener;

    public PrivateRoomRechargeDialogFragment setListener(PrivateRoomRechargeDialogFragment.PrivateRoomRechargeListener listener) {
        this.listener = listener;
        return this;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_cancel:
                //取消
                dismissDialog();
                break;
            case R.id.tv_confirm:
                //立即充值
                if (listener != null) {
                    listener.onRecharge();
                }
                break;
        }
    }

    public interface PrivateRoomRechargeListener {
        //充值
        void onRecharge();

        //取消 执行下麦
        void onCancel();
    }

    private void releaseTimer() {
        if (mTimer2 != null) {
            mTimer2.cancel();
            mTask2.cancel();
            mTask2 = null;
            mTimer2 = null;
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        releaseTimer();
    }
}
