package com.liquid .poros.ui.fragment.dialog.live

import android.content.Context
import android.graphics.Color
import android.text.Spannable
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.style.AbsoluteSizeSpan
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.request.RequestOptions
import com.liquid.base.adapter.CommonAdapter
import com.liquid.base.adapter.CommonViewHolder
import com.liquid.poros.R
import com.liquid.poros.base.BaseActivity
import com.liquid.poros.databinding.PopCpGiftRankDialogBinding
import com.liquid.poros.entity.CpLiveGiftRankInfo
import com.liquid.poros.ui.fragment.dialog.live.viewmodel.LiveGiftRankViewModel
import com.liquid.poros.utils.ScreenUtil
import com.liquid.poros.widgets.VerticalCenterSpan
import razerdp.basepopup.BasePopupWindow

/**
 * cp房礼物排行榜
 */
class CpLiveGiftRankPopupWindow(context: Context) : BasePopupWindow(context) {
    private lateinit var binding: PopCpGiftRankDialogBinding
    private lateinit var liveGiftRankViewModel: LiveGiftRankViewModel
    private var mCommonAdapter: CommonAdapter<CpLiveGiftRankInfo.RankInfo>? = null
    private var mCoin: String? = "0"
    private var  anchorTip:String? =""
    private var  mMicName:String? =""

    override fun onCreateContentView(): View {
        return createPopupById(R.layout.pop_cp_gift_rank_dialog)
    }

    override fun onViewCreated(contentView: View) {
        super.onViewCreated(contentView)
        binding = PopCpGiftRankDialogBinding.bind(contentView)
        liveGiftRankViewModel = (context as BaseActivity).getActivityViewModel(LiveGiftRankViewModel::class.java)
        initObserver()
        mCommonAdapter = object : CommonAdapter<CpLiveGiftRankInfo.RankInfo>(context, R.layout.item_cp_live_gift_rank) {
            override fun convert(holder: CommonViewHolder, t: CpLiveGiftRankInfo.RankInfo, pos: Int) {
                var item_iv_cp_rank_point = holder.getView<ImageView>(R.id.item_iv_cp_rank_point)
                var item_tv_cp_rank_point = holder.getView<TextView>(R.id.item_tv_cp_rank_point)
                item_iv_cp_rank_point.visibility = if (pos > 2) View.GONE else View.VISIBLE
                item_tv_cp_rank_point.visibility = if (pos > 2) View.VISIBLE else View.GONE
                if (pos > 2) {
                    item_tv_cp_rank_point.text = (pos + 1).toString()
                } else {
                    when (pos) {
                        0 -> holder.setBackgroundResource(R.id.item_iv_cp_rank_point, R.mipmap.icon_dialog_cp_gift_rank_num_one)
                        1 -> holder.setBackgroundResource(R.id.item_iv_cp_rank_point, R.mipmap.icon_dialog_cp_gift_rank_num_two)
                        2 -> holder.setBackgroundResource(R.id.item_iv_cp_rank_point, R.mipmap.icon_dialog_cp_gift_rank_num_three)
                    }
                }
                val options = RequestOptions()
                        .placeholder(R.mipmap.img_default_avatar)
                        .override(ScreenUtil.dip2px(44f), ScreenUtil.dip2px(44f))
                        .transform(CircleCrop())
                        .error(R.mipmap.img_default_avatar)
                Glide.with(context as BaseActivity)
                        .load(t.avatar_url)
                        .apply(options)
                        .into((holder.getView<View>(R.id.item_iv_cp_rank_head) as ImageView))
                holder.getView<TextView>(R.id.item_tv_cp_rank_nickname).text = t.nick_name
                holder.getView<TextView>(R.id.item_tv_cp_rank_coin).text = t.coins
            }
        }
        val linearLayoutManager = LinearLayoutManager(context)
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        binding.rvCpGiftRank.layoutManager = linearLayoutManager
        binding.rvCpGiftRank.adapter = mCommonAdapter
    }

    private fun initObserver() {
        liveGiftRankViewModel.cpGiftRankListSuccess.observe(context as BaseActivity, Observer {
            if (it == null)
                return@Observer
            if (!it.items.isNullOrEmpty()) {
                mCommonAdapter?.update(it.items)
                mCommonAdapter?.notifyDataSetChanged()
            }
            mCoin = it.total_coins
            binding.tvCpGiftRankTotal.text = getSpan()
        })
    }

    /**
     * 获取cp房列表数据
     * @param roomId 房间id
     * @param position 0 主持人麦位,2 女嘉宾麦位
     */
    fun setCpGiftRankData(roomId: String?, position: Int, micName: String?) {
        liveGiftRankViewModel.getCpGiftRankList(roomId, position)
        anchorTip = if(position==0) "主持" else ""
        mMicName = if (micName != null && micName?.length >= 7) "${micName?.substring(0, 7)}..." else micName
        binding.tvCpGiftRankTitle.text = "${anchorTip}<${mMicName}>的爱慕榜"
    }

    private fun getSpan() = SpannableStringBuilder()
            .append(SpannableString("本场金币: ").apply {
                setSpan(AbsoluteSizeSpan(14, true), 0, length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            })
            .append(SpannableString("$mCoin ").apply {
                setSpan(VerticalCenterSpan(14, Color.parseColor("#FFD13D")), 0, length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            })
}