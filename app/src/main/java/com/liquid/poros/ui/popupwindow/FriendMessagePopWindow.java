package com.liquid.poros.ui.popupwindow;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.liquid.poros.R;
import com.liquid.poros.utils.ScreenUtil;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * 消息长按弹框界面
 */
public class FriendMessagePopWindow extends PopupWindow {
    private Context mContext;
    private List<String> mStringList = new ArrayList<>();
    private FriendMessagePopWindow.ItemAdapter mAdapter;
    private Listener mListener;
    private RecyclerView rl_message;
    private boolean mIsMessage;
    private boolean mIsPinPriority;

    public FriendMessagePopWindow(Context context) {
        super(context);
        this.mContext = context;
    }

    public void showPop(View parent, boolean isMessage,  boolean isPinPriority) {
        this.mIsMessage = isMessage;
        this.mIsPinPriority = isPinPriority;
        View view = LayoutInflater.from(mContext).inflate(R.layout.pop_friend_message, null);
        this.setContentView(view);
        //低版本/个别机型兼容,设置宽高
        this.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        this.setWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
        this.setAnimationStyle(android.R.style.Animation_Dialog);
        this.setFocusable(true);
        this.setTouchable(true);
        this.setOutsideTouchable(true);
        this.setBackgroundDrawable(new ColorDrawable(0));
        view.setFocusableInTouchMode(true);
        //显示弹框
        this.showAsDropDown(parent, Gravity.CENTER, ScreenUtil.px2dip(10));
        initView(view);
    }

    private void initView(View view) {
        mStringList.clear();
        if (mIsMessage) {
            mStringList.add(mIsPinPriority ? mContext.getString(R.string.message_pop_topping) : mContext.getString(R.string.message_pop_cancel_topping));
            mStringList.add(mContext.getString(R.string.message_pop_report));
            mStringList.add(mContext.getString(R.string.message_pop_delete));
        } else {
            mStringList.add(mContext.getString(R.string.message_pop_delete_friend));
            mStringList.add(mContext.getString(R.string.message_pop_report));
        }
        rl_message = view.findViewById(R.id.rl_message);
        rl_message.setLayoutManager(new LinearLayoutManager(mContext));
        mAdapter = new FriendMessagePopWindow.ItemAdapter();
        rl_message.setAdapter(mAdapter);
        this.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss() {
                if (mListener!=null){
                    mListener.onDismiss();
                }
            }
        });
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        final TextView text;

        ViewHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.friend_message_pop_layout, parent, false));
            text = itemView.findViewById(R.id.tv_message_item);
            text.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (getAdapterPosition() >= mStringList.size()) {
                        return;
                    }
                    if (mListener != null) {
                        mListener.onItemClicked(mStringList.get(getAdapterPosition()));
                    }
                    dismiss();
                }
            });
        }

    }

    public interface Listener {
        void onItemClicked(String text);
        void onDismiss();
    }

    public void setOnListening(Listener listener) {
        mListener = listener;
    }

    private class ItemAdapter extends RecyclerView.Adapter<ViewHolder> {

        @Override
        public FriendMessagePopWindow.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new FriendMessagePopWindow.ViewHolder(LayoutInflater.from(parent.getContext()), parent);
        }

        @Override
        public void onBindViewHolder(FriendMessagePopWindow.ViewHolder holder, int position) {
            holder.text.setText(mStringList.get(position));
            holder.text.setTextColor(mContext.getString(R.string.message_pop_delete).equals(mStringList.get(position)) ? Color.parseColor("#F94F52") : Color.parseColor("#1D1E20"));
        }

        @Override
        public int getItemCount() {
            return mStringList.size();
        }

    }
}
