package com.liquid.poros.ui.activity.user;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothProfile;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewStub;
import android.view.WindowManager;
import android.view.animation.DecelerateInterpolator;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.airbnb.lottie.LottieAnimationView;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.liquid.base.event.EventCenter;
import com.liquid.base.guideview.Component;
import com.liquid.base.guideview.Guide;
import com.liquid.base.guideview.GuideBuilder;
import com.liquid.base.tools.GT;
import com.liquid.base.utils.ContextUtils;
import com.liquid.base.utils.GlobalConfig;
import com.liquid.base.utils.LogOut;
import com.liquid.im.kit.custom.AskPTAGiftAttachment;
import com.liquid.im.kit.custom.CoinChangeAttachment;
import com.liquid.im.kit.custom.CoinNotEnoughAttachment;
import com.liquid.im.kit.custom.CpLevelUpInviteAttach;
import com.liquid.im.kit.custom.CpLevelUpSuccessAttach;
import com.liquid.im.kit.custom.CpLevelUpdateTestAttachment;
import com.liquid.im.kit.custom.GiftBoxChangeAttachment;
import com.liquid.im.kit.custom.GiveRewardAttachment;
import com.liquid.im.kit.custom.GuardCpAttachment;
import com.liquid.im.kit.custom.HideMsgAttachment;
import com.liquid.im.kit.custom.InviteCoupleAttachment;
import com.liquid.im.kit.custom.InviteCoupleGameAttachment;
import com.liquid.im.kit.custom.InviteGameAttachment;
import com.liquid.im.kit.custom.LevelChangeAttachment;
import com.liquid.im.kit.custom.P2pActionAttachment;
import com.liquid.im.kit.custom.RoomActionAttachment;
import com.liquid.im.kit.custom.ScoreChangeAttachment;
import com.liquid.im.kit.custom.SendGetKeyPopup;
import com.liquid.im.kit.custom.ShowGiftPackageAttachment;
import com.liquid.im.kit.custom.UpdateDressAttachment;
import com.liquid.im.kit.custom.VideoAdAttachment;
import com.liquid.im.kit.permission.MPermission;
import com.liquid.im.kit.permission.annotation.OnMPermissionDenied;
import com.liquid.im.kit.permission.annotation.OnMPermissionGranted;
import com.liquid.im.kit.permission.annotation.OnMPermissionNeverAskAgain;
import com.liquid.poros.PorosApplication;
import com.liquid.poros.R;
import com.liquid.poros.ad.RewardVideoActivity;
import com.liquid.poros.adapter.MessageClickListener;
import com.liquid.poros.adapter.SessionMessageAdapter;
import com.liquid.poros.analytics.utils.IMMessageExposureUtils;
import com.liquid.poros.analytics.utils.MultiTracker;
import com.liquid.poros.base.BaseActivity;
import com.liquid.poros.constant.CardCategoryEnum;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.constant.DataOptimize;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.contract.message.SessionContract;
import com.liquid.poros.contract.message.SessionPresenter;
import com.liquid.poros.entity.BoxKeyNumBean;
import com.liquid.poros.entity.CpLevelUpDetail;
import com.liquid.poros.entity.GameRoomInfo;
import com.liquid.poros.entity.GiftItemInfo;
import com.liquid.poros.entity.GiftOfBox;
import com.liquid.poros.entity.MessageAction;
import com.liquid.poros.entity.MsgUserModel;
import com.liquid.poros.entity.PlayloadInfo;
import com.liquid.poros.entity.RoomDetailsInfo;
import com.liquid.poros.entity.SessionDressBean;
import com.liquid.poros.entity.TipItem;
import com.liquid.poros.helper.user.UserInfoObserver;
import com.liquid.poros.ui.RouterUtils;
import com.liquid.poros.ui.activity.DeclarationDetailActivity;
import com.liquid.poros.ui.activity.HalfDeclarationActivity;
import com.liquid.poros.ui.activity.WebViewActivity;
import com.liquid.poros.ui.activity.audiomatch.AudioMatchInfoActivity;
import com.liquid.poros.ui.activity.audiomatch.CPMatchingGlobal;
import com.liquid.poros.ui.activity.live.GuardVoiceRoom;
import com.liquid.poros.ui.activity.live.RoomLiveActivityV2;
import com.liquid.poros.ui.activity.live.VoiceRoom;
import com.liquid.poros.ui.activity.live.view.GiftAnimationPopupWindow;
import com.liquid.poros.ui.dialog.GiftBoxDialog;
import com.liquid.poros.ui.dialog.GiftDetailDialog;
import com.liquid.poros.ui.dialog.GotKeyDialog;
import com.liquid.poros.ui.dialog.MultiGiftSendDialog;
import com.liquid.poros.ui.dialog.RechargeCoinDialog;
import com.liquid.poros.ui.dialog.SendGiftPopupWindow;
import com.liquid.poros.ui.dialog.SendGiftView;
import com.liquid.poros.ui.floatball.FloatManager;
import com.liquid.poros.ui.floatball.GiftPackageFloatManager;
import com.liquid.poros.ui.floatball.rom.HuaweiUtils;
import com.liquid.poros.ui.floatball.rom.MeizuUtils;
import com.liquid.poros.ui.floatball.rom.MiuiUtils;
import com.liquid.poros.ui.floatball.rom.OppoUtils;
import com.liquid.poros.ui.floatball.rom.QikuUtils;
import com.liquid.poros.ui.floatball.rom.RomUtils;
import com.liquid.poros.ui.fragment.dialog.common.CommonActionListener;
import com.liquid.poros.ui.fragment.dialog.common.CommonDialogBuilder;
import com.liquid.poros.ui.fragment.dialog.common.CommonDialogFragment;
import com.liquid.poros.ui.fragment.dialog.pta.UserDressDialogFragment;
import com.liquid.poros.ui.fragment.dialog.session.SendVirtualDialogFragment;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.poros.utils.KeyboardHelper;
import com.liquid.poros.utils.SharePreferenceUtil;
import com.liquid.poros.utils.StringUtil;
import com.liquid.poros.utils.UIUtils;
import com.liquid.poros.utils.ViewUtils;
import com.liquid.poros.utils.glide.GlideEngine;
import com.liquid.poros.utils.network.HttpCallback;
import com.liquid.poros.utils.network.RetrofitHttpManager;
import com.liquid.poros.widgets.ComponentView;
import com.liquid.poros.widgets.InputPanel;
import com.liquid.poros.widgets.MessageLinearLayoutManager;
import com.liquid.poros.widgets.TipView;
import com.liquid.porostwo.ApplicationViewModel;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.tools.ToastUtils;
import com.netease.lava.nertc.sdk.NERtcEx;
import com.netease.nim.uikit.business.session.custom.CupidSysAttachment;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.Observer;
import com.netease.nimlib.sdk.RequestCallback;
import com.netease.nimlib.sdk.msg.MessageBuilder;
import com.netease.nimlib.sdk.msg.MsgService;
import com.netease.nimlib.sdk.msg.MsgServiceObserve;
import com.netease.nimlib.sdk.msg.constant.MsgDirectionEnum;
import com.netease.nimlib.sdk.msg.constant.MsgStatusEnum;
import com.netease.nimlib.sdk.msg.constant.MsgTypeEnum;
import com.netease.nimlib.sdk.msg.constant.SessionTypeEnum;
import com.netease.nimlib.sdk.msg.model.IMMessage;
import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import butterknife.BindView;
import butterknife.OnClick;
import okhttp3.RequestBody;

/**
 * 消息私聊页面
 */
public class SessionActivityV2 extends BaseActivity implements SessionContract.View, MessageClickListener, InputPanel.OnInputCallback {

    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tv_status)
    TextView tv_status;
    @BindView(R.id.tv_add_friend)
    TextView tv_add_friend;
    @BindView(R.id.rv_messages)
    RecyclerView rv_messages;

    @BindView(R.id.ll_voice_room)
    ViewStub ll_voice_room;
    @BindView(R.id.close_tip)
    ImageView closeTip;
    @BindView(R.id.ll_guard_room)
    ViewStub ll_guard_room;

    @BindView(R.id.ll_container)
    RelativeLayout ll_container;

    @BindView(R.id.gift_tips)
    ImageView giftTips;

    @BindView(R.id.ll_normal_input)
    View ll_normal_input;

    @BindView(R.id.info_tv)
    TextView info_tv;

    @BindView(R.id.clear_info)
    TextView clear_info;
    @BindView(R.id.rl_guard_live)
    RelativeLayout rl_guard_live;
    @BindView(R.id.tv_guard_live_status)
    TextView tv_guard_live_status;
    @BindView(R.id.iv_guard_live_head)
    ImageView iv_guard_live_head;
    @BindView(R.id.cp_status_iv)
    ImageView cp_status_iv;
    @BindView(R.id.active_get_box_key)
    ImageView active_get_box_key;
    @BindView(R.id.guide_upgrade_cp_iv)
    public LinearLayout guide_upgrade_cp_iv;
    @BindView(R.id.chat_bg)
    ImageView chat_bg;
    @BindView(R.id.guide_gift_package_iv)
    public ImageView guide_gift_package_iv;
    @BindView(R.id.message_container_layout)
    FrameLayout mMessageContainerLayout;

    @BindView(R.id.message_layout)
    LinearLayout message_layout;
    @BindView(R.id.top_layout)
    View top_layout;

    @BindView(R.id.iv_enter_gift_box)
    ImageView iv_enter_gift_box;
    @BindView(R.id.tv_gift_box_count)
    TextView tv_gift_box_count;

    @BindView(R.id.lottie_add_gift_box)
    LottieAnimationView lottie_add_gift_box;

    private ArrayList<GotKeyDialog> dialogArrayList;

    private String sessionId;
    private String femaleGuestId;
    private String inviteId;
    private SessionPresenter mPresenter;
    private boolean isInHouse = false;
    private SessionMessageAdapter mAdapter;
    private int giftNum;
    private String giftJson;
    private String giftName;
    private String giftPic;
    private String giftAudio;
    private boolean isCPend;
    private VoiceRoom mVoiceRoom;
    private int count = 0;
    private MsgUserModel.UserInfo mMsgUserInfo;
    private boolean isNewIntent = false;
    private InputPanel mInputPanel;
    private MessageLinearLayoutManager mLayoutManager;
    private boolean mIsConnected = true;
    private AudioManager mAudioManager;
    private MediaPlayer mMediaPlayer;
    private boolean isOnMic;
    private boolean mIsInviteMic;
    private boolean mIsBoxKeyTrial;
    private boolean canShowCPLevelDialog = true;
    public boolean canShowCpUpgradeDialog = true;
    private String levelName;
    private String roomId;
    private String sourceFrom;
    private boolean showRecharge;
    private boolean showBuyDialog;
    private MultiGiftSendDialog buyCpFragment;
    private CpLevelUpdateTestAttachment attachment;
    private int clickCount = 0;
    private boolean isPause = false;
    private boolean needRefresh = false;
    private long currentCoins = 0;
    private RechargeCoinDialog mRechargeCoinDialogFragment;
    private IMMessageExposureUtils<SessionMessageAdapter, IMMessage> exposureUtils;
    private String buy_position;//开黑卡购买埋点参数
    private final String TAG_WANGZHE = "wzry";
    private final String TAG_HEPING = "hpjy";
    private String mAudioMatchTitle;
    private SendGiftPopupWindow sendGiftPanel;
    private boolean isOfficialAssistant;//是否是官方小助手
    private GiftAnimationPopupWindow giftAnimationPopupWindow;
    private GiftBoxDialog giftBoxDialog;
    private boolean mIsSendCupidSys;
    public SessionPresenter getPresenter() {
        return mPresenter;
    }

    @Override
    protected String getPageId() {
        return TrackConstants.P2P_PAGE;
    }

    @Override
    protected void parseBundleExtras(Bundle extras) {
        femaleGuestId = extras.getString(Constants.SESSION_ID);
        inviteId = extras.getString(Constants.INVITE_ID);
        mIsInviteMic = extras.getBoolean("is_invite_mic");
        showRecharge = extras.getBoolean(Constants.SHOW_RECHARGE);
        showBuyDialog = extras.getBoolean(Constants.SHOW_BUY_DIALOG);
        roomId = extras.getString(Constants.ROOM_ID);
        buy_position = extras.getString(Constants.BUY_POSITION);
        sourceFrom = extras.getString(Constants.FROM);
        uploadTrackEvent(showBuyDialog, showRecharge, extras.getString(Constants.FROM));
    }

    /**
     * 剩余3分钟消息提醒 和  用户未成功续卡的消息提醒，的消息点击事件埋点
     */
    private void uploadTrackEvent(boolean showBuyDialog, boolean showRecharge, String from) {
        if (TextUtils.isEmpty(from))
            return;
        String eventName = "";
        String position = "";
        if (showBuyDialog) {
            eventName = TrackConstants.B_REMIND_TEAM_JOIN_CARD_TIME_END_CLICK;
        }
        if (showRecharge) {
            eventName = TrackConstants.B_TEAM_JOIN_CARD_TIME_3MIN_NOTICE_CLICK;
        }

        if (Constants.INNER_NOTICE.equals(from)) {
            position = TrackConstants.POSI_APPLICATION;
        }

        if (Constants.OUTER_NOTICE.equals(from)) {
            position = TrackConstants.POSI_NOTICE;
        }

        HashMap<String, String> params = new HashMap<>();
        params.put(Constants.POSITION, position);
        MultiTracker.onEvent(eventName, params);
    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.activity_session;
    }


    @Override
    protected void onNewIntent(Intent intent) {
        isNewIntent = true;
        super.onNewIntent(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void initViewsAndEvents(Bundle savedInstanceState) {
        Bundle extras = getIntent().getExtras();
        if (null != extras) {
            parseBundleExtras(extras);
        }
        if (isNewIntent) {
            if (mVoiceRoom != null) {
                mVoiceRoom.initLink(femaleGuestId, roomId);
                mPresenter.getMessageList("", femaleGuestId, false,"");
                mPresenter.getP2PUserStatusInfo(femaleGuestId);
                mInputPanel = new InputPanel(this, ll_normal_input, femaleGuestId, this,InputPanel.NORMAL_CHAT);
                if (mIsInviteMic) {
                    mVoiceRoom.showMicGuide();
                    mIsInviteMic = false;
                }
            }
            return;
        }
        dialogArrayList = new ArrayList<>();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);   //应用运行时，保持屏幕高亮，不锁屏
        mPresenter = new SessionPresenter();
        mPresenter.attachView(this);
        mPresenter.getMessageList("", femaleGuestId, false,"");
        mPresenter.getP2PUserStatusInfo(femaleGuestId);
        mInputPanel = new InputPanel(this, ll_normal_input, femaleGuestId, this,InputPanel.GIRL_CHAT);
        mAdapter = new SessionMessageAdapter(this, this);
        mAdapter.setRoom_id(roomId);
        mLayoutManager = new MessageLinearLayoutManager(this);
        rv_messages.setLayoutManager(mLayoutManager);
        rv_messages.setAdapter(mAdapter);
        sendGiftPanel = new SendGiftPopupWindow(this);
        sendGiftPanel.setSessionId(femaleGuestId);
        sendGiftPanel.setIsLiveRoom(false);
        mRechargeCoinDialogFragment = new RechargeCoinDialog();
        if (showRecharge) {
            if (sourceFrom != null) {
                if (sourceFrom.equals(Constants.OUTER_NOTICE)) {
                    mRechargeCoinDialogFragment.setPositon(RechargeCoinDialog.POSITION_REMIND_CARD_NOTICE);
                } else {
                    mRechargeCoinDialogFragment.setPositon(RechargeCoinDialog.POSITION_REMIND_CARD_APP);
                }
            }
            mRechargeCoinDialogFragment.show(getSupportFragmentManager(), RechargeCoinDialog.class.getName());
        }
        registerObservers(true);
        rv_messages.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

            }

            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                    int firstCompletelyVisibleItemPosition = layoutManager.findFirstCompletelyVisibleItemPosition();
                    if (firstCompletelyVisibleItemPosition == 0) {
                        mPresenter.getMessageList(mAdapter.getOldMsgId(), femaleGuestId, true,"");
                    }
                }
            }
        });
        mAudioManager = (AudioManager) getSystemService(Service.AUDIO_SERVICE);
        if (mAudioManager.isWiredHeadsetOn()) {
            mAudioManager.setWiredHeadsetOn(true);
        } else if (isBluetoothHeadsetConnected()) {
            mAudioManager.setBluetoothScoOn(true);
        } else {
//            mAudioManager.setSpeakerphoneOn(true);
        }

        info_tv = findViewById(R.id.info_tv);
        clear_info = findViewById(R.id.clear_info);
        clear_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                info_tv.setText("");
            }
        });

        tv_title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickCount++;
                if (clickCount == 3 && Constants.getUseTestApi()) {
                    info_tv.setVisibility(View.VISIBLE);
                    clear_info.setVisibility(View.VISIBLE);
                    clickCount = 0;
                }
            }
        });
        PorosApplication.instance.getViewModel(ApplicationViewModel.class)
                .getUserInfoChange().observe(this,list -> {
            if (!list.contains(femaleGuestId)) {
                return;
            }
            mAdapter.notifyDataSetChanged();
        });
    }

    public void setOnMic(boolean isOnMic) {
        this.isOnMic = isOnMic;
    }

    private boolean isBluetoothHeadsetConnected() {
        BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
        if (BluetoothProfile.STATE_CONNECTED == adapter.getProfileConnectionState(BluetoothProfile.HEADSET)) {
            return true;
        }
        return false;
    }

    @Override
    public void onMessageFetched(List<IMMessage> messages, boolean isForward) {
        Collections.reverse(messages);
        mAdapter.updateShowTimeItem(messages, false, true);
        if (isForward) {
            List<IMMessage> imMessages = getAddItems(messages);
            if (imMessages.size() > 0) {
                mAdapter.addWithoutNotify(0, imMessages);
                mAdapter.notifyItemRangeInserted(0, imMessages.size());
            }
        } else {
            if (messages.size() > 5) {
                mLayoutManager.setStackFromEnd(true);
            }
            mAdapter.update(messages);
        }
    }

    private List<IMMessage> getAddItems(List<IMMessage> messages) {
        List<IMMessage> adds = new ArrayList<>();
        for (IMMessage message : messages) {
            if (!mAdapter.getDatas().contains(message)) {
                adds.add(message);
            }
        }
        return adds;
    }


    @Override
    public void hideEmoji(MotionEvent event, View view) {
        if (giftTips != null && giftTips.getVisibility() == View.VISIBLE) {
            SharePreferenceUtil.saveBoolean(SharePreferenceUtil.FILE_USER_ACCOUNT_DATA, SharePreferenceUtil.KEY_GIFT_EVER_CLICK, true);
            giftTips.setVisibility(View.GONE);
        }
        if (isShouldHideInput(view, event)) {
            KeyboardHelper.hintKeyBoard(SessionActivityV2.this);
            if (mInputPanel != null) {
                mInputPanel.hideInput();
            }
        }
    }

    private boolean isShouldHideSendGiftView(View v, MotionEvent event) {
        if (v != null && (v instanceof LinearLayout || v instanceof TextView)) {
            int[] l = {0, 0};
            v.getLocationInWindow(l);
            int top = l[1];
            return !(event.getY() > top);
        }
        return false;
    }

    private boolean isShouldHideSelectGiftPanel(View v, MotionEvent ev) {
        if (v != null && (v instanceof LinearLayout || v instanceof TextView)) {
            int[] l = {0, 0, 0, 0};
            v.getLocationInWindow(l);
            int x = l[0];
            int y = l[1];
            if (ev.getX() < x || ev.getX() > (x + v.getWidth()) || ev.getY() < y || ev.getY() > (y + v.getHeight())) {
                return true;
            }
            return false;
        }
        return false;
    }

    /**
     * 根据EditText所在坐标和用户点击的坐标相对比，来判断是否隐藏键盘
     *
     * @param v
     * @param event
     * @return
     */
    private boolean isShouldHideInput(View v, MotionEvent event) {
        if (v != null && (v instanceof EditText || v instanceof TextView)) {
            int[] l = {0, 0};
            v.getLocationInWindow(l);
            int top = l[1];
            return !(event.getY() > top);
        }
        return false;
    }

    @Override
    public void onSuccess() {

    }

    @Override
    protected HashMap<String, String> getParams() {
        HashMap<String, String> map = new HashMap<>();
        map.put("user_id", AccountUtil.getInstance().getUserId());
        map.put("female_guest_id", femaleGuestId);
        map.put("event_time", String.valueOf(System.currentTimeMillis()));
        return map;
    }

    private Observer<IMMessage> messageStatusObserver = new Observer<IMMessage>() {
        @Override
        public void onEvent(IMMessage message) {
            if (isMyMessage(message)) {
                if (mAdapter != null) {
                    mAdapter.notifyDataSetChanged();
                }
            }
        }
    };
    Observer<List<IMMessage>> incomingMessageObserver = new Observer<List<IMMessage>>() {
        @Override
        public void onEvent(List<IMMessage> messages) {
            onIncomingMessage(messages);
        }
    };

    private void registerObservers(boolean register) {
        MsgServiceObserve service = NIMClient.getService(MsgServiceObserve.class);
        service.observeReceiveMessage(incomingMessageObserver, register);
        service.observeMsgStatus(messageStatusObserver, register);
        sendGiftPanel.setListener(new SendGiftView.OnSendGiftClickListener() {
            @Override
            public void sendGift(int giftId, String icon, int num, String audio, String name, String json, int toPos,String source) {
                giftNum = num;
                giftName = name;
                giftPic = icon;
                giftAudio = audio;
                giftJson = json;
                mPresenter.sendGift(femaleGuestId, giftId, num);
                uploadSendGiftData(giftId);
            }

            @Override
            public void jumpRecharge() {

            }

            @Override
            public void jumpRecharge(String position) {
                if (mRechargeCoinDialogFragment.isAdded()) {
                    return;
                }
                mRechargeCoinDialogFragment.setPositon(position);
                mRechargeCoinDialogFragment.show(getSupportFragmentManager(), RechargeCoinDialog.class.getName());
            }
        });
    }

    public void sendGift(int giftId, @org.jetbrains.annotations.Nullable String icon, int num, @org.jetbrains.annotations.Nullable String audio, @NotNull String name,String json,int toPos) {
        giftNum = num;
        giftName = name;
        giftPic = icon;
        giftAudio = audio;
        giftJson = json;
        mPresenter.sendGift(femaleGuestId, giftId, num);
        uploadSendGiftData(giftId);
    }

    private void uploadSendGiftData(int giftId) {
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", AccountUtil.getInstance().getUserId());
        params.put("female_guest_id", femaleGuestId);
        params.put("gift_id", String.valueOf(giftId));
        params.put("event_time", String.valueOf(System.currentTimeMillis()));
        MultiTracker.onEvent(TrackConstants.B_PRESENT_GIFT, params);
    }


    private void uploadSendGiftSuccessData(int giftId) {
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", AccountUtil.getInstance().getUserId());
        params.put("female_guest_id", femaleGuestId);
        params.put("gift_id", String.valueOf(giftId));
        params.put("gift_num", String.valueOf(giftNum));
        params.put("event_time", String.valueOf(System.currentTimeMillis()));
        MultiTracker.onEvent(TrackConstants.B_PRESENT_GIFT_SUCCESS, params);
    }

    @Override
    protected void onEventComing(EventCenter eventCenter) {
        super.onEventComing(eventCenter);
        switch (eventCenter.getEventCode()) {
            case Constants.EVENT_CODE_REPORT_LOCALE_SPEAKER:
                break;
            case Constants.EVENT_CODE_REPORT_SPEAKER:
                Map<String, Integer> speakers = (Map<String, Integer>) eventCenter.getData();
                if (mVoiceRoom != null) {
                    mVoiceRoom.onAudioVolume(speakers);
                }
                break;
            case Constants.EVENT_CONNECT_INFO:
                String data = (String) eventCenter.getData();
                String tv = info_tv.getText().toString() + "\n" + data;
                info_tv.setText(tv);
                break;
            case Constants.EVENT_CODE_COIN_CHANGE:
                mPresenter.getP2PUserStatusInfo(femaleGuestId);
                break;
            case Constants.EVENT_CODE_UPDATE_GIFT_PACKAGE_STATUS:
                if (mVoiceRoom != null && mVoiceRoom instanceof GuardVoiceRoom) {
                    ((GuardVoiceRoom) mVoiceRoom).showGiftPackageGuide();
                }
                break;
            case Constants.EVENT_CODE_BUY_COIN_NOT_ENOUGH:
                showChargeDialog(RechargeCoinDialog.POSITION_GUEST_ASK_FOR_DRESS);
                break;
            case Constants.ACTION_P2P_JOIN_ROOM:
                break;
            case Constants.EVENT_CODE_FINISH_SESSION_ACTIVITY:
                if (isDestroyed() || isFinishing()) return;
                finish();
                break;
            case Constants.EVENT_CODE_BUY_ONE_CENT_VIP:
                Toast.makeText(ContextUtils.getApplicationContext(), "购买成功", Toast.LENGTH_SHORT).show();
                mAdapter.notifyDataSetChanged();
                break;
        }
    }

    private void showChargeDialog(String positon) {
        if (mRechargeCoinDialogFragment.isAdded()) {
            return;
        }
        mRechargeCoinDialogFragment.setPositon(positon);
        mRechargeCoinDialogFragment.show(getSupportFragmentManager(), RechargeCoinDialog.class.getName());
    }

    public void onIncomingMessage(List<IMMessage> messages) {
        if (isDestroyed() || isFinishing()) {
            return;
        }
        List<IMMessage> addItems = new ArrayList<>();
        for (IMMessage message : messages) {
            if (isMyMessage(message)) {
                Map<String, Object> remoteExt = message.getRemoteExtension();
                if (remoteExt != null) {
                    if (remoteExt.containsKey("show_uid")) {
                        String show_uid = (String) remoteExt.get("show_uid");
                        if (!StringUtil.isEquals(AccountUtil.getInstance().getUserId(), show_uid)) {
                            continue;
                        }
                    }
                }

                if (message.getAttachment() instanceof HideMsgAttachment) {
                    HideMsgAttachment attachment = (HideMsgAttachment) message.getAttachment();
                    String cToast = attachment.getcToast();
                    if (StringUtil.isNotEmpty(cToast)) {
                        ToastUtils.s(this, cToast);
                    }
                    continue;
                }

                //如果消息不是
                if (message.getAttachment() instanceof InviteGameAttachment) {
                    InviteGameAttachment invite = (InviteGameAttachment) message.getAttachment();
                    if (invite.getStatus() != 0) {
                        updateInviteStatus(invite);
                        continue;
                    }
                }
                if (message.getAttachment() instanceof CoinNotEnoughAttachment) {
                    CoinNotEnoughAttachment attachment = (CoinNotEnoughAttachment) message.getAttachment();
                    if (attachment.getType() == 1) {
                        HashMap<String, String> params = new HashMap<>();
                        params.put("user_id", AccountUtil.getInstance().getUserId());
                        params.put("female_guest_id", femaleGuestId);
                        params.put("tag", "connect_mic_ques");
                        MultiTracker.onEvent("coin_not_enough", params);
                        //mVoiceRoom.leaveRoom();
                        mPresenter.getP2PUserStatusInfo(femaleGuestId);
                        if (!FloatManager.getInstance().isWindowDismiss()) {
                            FloatManager.getInstance().micStatus(1);
                            try {
                                NERtcEx.getInstance().enableLocalAudio(false);
                            } catch (Exception e) {

                            }
                        }
                    } else {
                        if (!FloatManager.getInstance().isWindowDismiss()) {
                            FloatManager.getInstance().micStatus(0);
                        }
                    }
                }
                if (message.getAttachment() instanceof ScoreChangeAttachment) {
                    ScoreChangeAttachment attachment = (ScoreChangeAttachment) message.getAttachment();
                    updateScore(attachment);
                    continue;
                }
                if (message.getAttachment() instanceof LevelChangeAttachment) {
                    LevelChangeAttachment attachment = (LevelChangeAttachment) message.getAttachment();
                    updateLevel(attachment);
                    continue;
                }

                if (message.getAttachment() instanceof RoomActionAttachment) {
                    RoomActionAttachment attachment = (RoomActionAttachment) message.getAttachment();
                    if (attachment != null && StringUtil.isNotEmpty(attachment.getcToast())) {
                        ToastUtils.s(this, attachment.getcToast());
                    }
                }

                if (message.getAttachment() instanceof GiveRewardAttachment) {
                    GiveRewardAttachment attachment = (GiveRewardAttachment) message.getAttachment();
                    if (attachment != null && attachment.isShowBuyDialog() && mVoiceRoom != null) {
                        if (isFinishing() || isDestroyed()) return;
                        mVoiceRoom.showCpCardDialog(false, false, "", "", femaleGuestId, "", CardCategoryEnum.TEAM_CARD.getCode());
                    } else if (attachment != null && attachment.isShowRecharge()) {
                        try {
                            if (!mRechargeCoinDialogFragment.isAdded()) {
                                mRechargeCoinDialogFragment.show(getSupportFragmentManager(), RechargeCoinDialog.class.getName());
                            }
                        } catch (Throwable throwable) {

                        }
                    }
                }

                if (message.getAttachment() instanceof GuardCpAttachment) {
                    GuardCpAttachment guardCpAttachment = (GuardCpAttachment) message.getAttachment();
                    String gToast = guardCpAttachment.getcToast();
                    if (StringUtil.isNotEmpty(gToast)) {
                        ToastUtils.s(mContext, gToast);
                        if (mPresenter != null) {
                            mPresenter.getP2PUserStatusInfo(femaleGuestId);
                        }
                    }
                }

                if (message.getAttachment() instanceof CpLevelUpdateTestAttachment) {
                    CpLevelUpdateTestAttachment attachment = (CpLevelUpdateTestAttachment) message.getAttachment();
                    updateCpUpgradeLevel(attachment);
                    continue;
                }

                //发送视频广告的消息
                if (message.getAttachment() instanceof VideoAdAttachment) {
                    VideoAdAttachment attachment = (VideoAdAttachment) message.getAttachment();
                    showVideoAdDialog(attachment);
                    continue;
                }

                if (message.getAttachment() instanceof P2pActionAttachment) {
                    P2pActionAttachment actionAttachment = (P2pActionAttachment) message.getAttachment();
                    onIncomingAction(actionAttachment.getStatus(), actionAttachment.getStatus_str(), actionAttachment.isVoice_warning());
                    continue;
                }
                if (message.getAttachment() instanceof InviteCoupleAttachment) {
                    InviteCoupleAttachment invite = (InviteCoupleAttachment) message.getAttachment();
                    if (invite.getStatus() != 1) {
                        updateInviteStatus(invite);
                        continue;
                    }
                }
                if (message.getAttachment() instanceof CpLevelUpInviteAttach || message.getAttachment() instanceof CpLevelUpSuccessAttach) {
                    if (isPause) {
                        needRefresh = true;
                    } else {
                        if (mPresenter != null) {
                            mPresenter.getP2PUserStatusInfo(femaleGuestId);
                        }
                    }
                }

                if (message.getAttachment() instanceof UpdateDressAttachment) {
                    continue;
                }

                //礼盒数量发生变化
                if (message.getAttachment() instanceof GiftBoxChangeAttachment) {
                    GiftBoxChangeAttachment attachment = (GiftBoxChangeAttachment) message.getAttachment();
                    tv_gift_box_count.setText(String.valueOf(attachment.getCount()));
                    if (attachment.getAddCount() > 0){
                        //startAddGiftBoxAnim();
                    }
                    continue;
                }

                if(message.getAttachment() instanceof SendGetKeyPopup){
                    SendGetKeyPopup attachment = (SendGetKeyPopup) message.getAttachment();
                    LogOut.debug("showGotKeyFlat","custom");
                    showGotKeyDialog(attachment.getTitle(), attachment.getEvery_day());
                    continue;
                }

                addItems.add(message);

            }
            if (message.getAttachment() instanceof CoinChangeAttachment) {
                CoinChangeAttachment attachment = (CoinChangeAttachment) message.getAttachment();
                updateCoin(attachment);
                continue;
            }

            if (message.getAttachment() instanceof ShowGiftPackageAttachment) {
                ShowGiftPackageAttachment attachment = (ShowGiftPackageAttachment) message.getAttachment();
                GiftPackageFloatManager.getInstance().showGiftBuyDialog(attachment.getPay_amount(), 1);
                continue;
            }

            if (message.getAttachment() instanceof GuardCpAttachment || message.getAttachment() instanceof InviteCoupleGameAttachment) {
                if (message.getDirect() == MsgDirectionEnum.In && femaleGuestId != null && femaleGuestId.equals(message.getSessionId())) {
                    if (mVoiceRoom != null) {
                        mVoiceRoom.showMicGuide();
                    }
                }
            }


        }
        if (addItems.size() > 0) {
            mAdapter.add(addItems);
            mAdapter.updateShowTimeItem(addItems, false, true);
            rv_messages.smoothScrollToPosition(mAdapter.getItemCount() - 1);
        }
    }

    private void showGotKeyDialog(Integer title, Integer every_day) {
        if(ViewUtils.isFastClick()){
            return;
        }
        closeYourDialogFragment();
        getWindow().getDecorView().postDelayed(new Runnable() {
            @Override
            public void run() {
                GotKeyDialog gotKeyDialog = GotKeyDialog.newInstance("got_key_dialog");
                gotKeyDialog.
                        setFrom(GotKeyDialog.P2P_PAGE).
                        setTargetId(femaleGuestId).
                        setParams(title, every_day).
                        show(getSupportFragmentManager(), GotKeyDialog.class.getSimpleName());

                dialogArrayList.add(gotKeyDialog);
            }
        },1000);
    }

    private void startAddGiftBoxAnim() {
        UIUtils.startLottieAnimation(iv_enter_gift_box,lottie_add_gift_box,"addgiftbox", null);
    }

    private MediaPlayer mMediaPlayerOne;


    private void playVoiceTip(String voiceUrl) {
        try {
            if (mMediaPlayerOne == null) {
                mMediaPlayerOne = MediaPlayer.create(mContext, Uri.parse(voiceUrl));
                mMediaPlayerOne.setLooping(false);
                mMediaPlayerOne.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mMediaPlayerOne.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        if (mMediaPlayerOne != null) {
                            mMediaPlayerOne.release();
                            mMediaPlayerOne = null;
                        }
                    }
                });
            }
            mMediaPlayerOne.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateCpUpgradeLevel(CpLevelUpdateTestAttachment attachment) {
        this.attachment = attachment;
        if (mVoiceRoom != null && this.attachment != null) {
            if (canShowCpUpgradeDialog) {
                MsgUserModel.P2PRankUpgradeAwardInfo p2PRankUpgradeAwardInfo = new MsgUserModel.P2PRankUpgradeAwardInfo();
                p2PRankUpgradeAwardInfo.group_name = attachment.getGroup_name();
                p2PRankUpgradeAwardInfo.award_list = new Gson().fromJson(attachment.getAward_list(), new TypeToken<List<MsgUserModel.P2PRankUpgradeAwardInfo.Info>>() {
                }.getType());
                mVoiceRoom.updateCpUpgradeDialog(p2PRankUpgradeAwardInfo);
                this.attachment = null;
            }
        }

        if (mPresenter != null) {
            mPresenter.raiseGroupBack(femaleGuestId);
        }

        getHandler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (SessionActivityV2.this.isDestroyed() || SessionActivityV2.this.isFinishing()) {
                    return;
                }
                if (mPresenter != null) {
                    mPresenter.getP2PUserStatusInfo(femaleGuestId);
                }
            }
        }, 200);
    }

    //展示广告相关
    private void showVideoAdDialog(VideoAdAttachment attachment) {
        try {
            HashMap<String, String> params = new HashMap<>();
            params.put("female_guest_id", femaleGuestId);
            MultiTracker.onEvent("b_excitation_video_alert_exposure", params);

            new CommonDialogBuilder()
                    .setWithCancel(true)
                    .setImageTitleRes(R.mipmap.icon_no_coin)
                    .setCancel("充值金币")
                    .setConfirm("免费聊天")
                    .setDesc(attachment.getContent())
                    .setListener(new CommonActionListener() {
                        @Override
                        public void onConfirm() {
                            Bundle bundle = new Bundle();
                            bundle.putString(Constants.SHOW_AD_MSG, attachment.getWatch_ad_content());
                            bundle.putString(Constants.FEMALE_GUEST_ID, femaleGuestId);
                            readyGo(RewardVideoActivity.class, bundle);

                            HashMap<String, String> params = new HashMap<>();
                            params.put("female_guest_id", femaleGuestId);
                            MultiTracker.onEvent("b_go_excitation_video_click", params);
                        }

                        @Override
                        public void onCancel() {
                            if (mRechargeCoinDialogFragment.isAdded()) {
                                return;
                            }
                            mRechargeCoinDialogFragment.show(getSupportFragmentManager(), RechargeCoinDialog.class.getName());
                            HashMap<String, String> params = new HashMap<>();
                            params.put("female_guest_id", femaleGuestId);
                            MultiTracker.onEvent("b_go_recharge_button_click", params);
                        }
                    }).create().show(getSupportFragmentManager(), CommonDialogFragment.class.getSimpleName());
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    private void updateCoin(CoinChangeAttachment attachment) {
        if (sendGiftPanel != null) {
            sendGiftPanel.setCoinCount(attachment.getCoin());
            if (buyCpFragment != null) {
                buyCpFragment.updateCoinCount(String.valueOf(attachment.getCoin()));
            }
        }
        currentCoins = attachment.getCoin();
    }

    private void updateScore(ScoreChangeAttachment attachment) {
        if (mVoiceRoom != null) {
            mVoiceRoom.updateScore(attachment);
        }
    }

    private void updateLevel(LevelChangeAttachment attachment) {
        levelName = attachment.getName();
        if (mVoiceRoom != null) {
            mVoiceRoom.updateLevel(attachment);
            if (canShowCPLevelDialog) {
                mVoiceRoom.updateLevelDialog(levelName);
                levelName = null;
            }
        }
    }

    private void updateInviteStatus(InviteGameAttachment invite) {
        for (IMMessage message : mAdapter.getDatas()) {
            if (message.getUuid().equals(invite.getMsgId()) && message.getAttachment() instanceof InviteGameAttachment) {
                ((InviteGameAttachment) message.getAttachment()).setStatus(invite.getStatus());
            }
        }
        mAdapter.notifyDataSetChanged();
    }

    private void updateInviteStatus(InviteCoupleAttachment invite) {
        for (IMMessage message : mAdapter.getDatas()) {
            if (message.getUuid().equals(invite.getMsgId()) && message.getAttachment() instanceof InviteCoupleAttachment) {
                ((InviteCoupleAttachment) message.getAttachment()).setStatus(invite.getStatus());
                ((InviteCoupleAttachment) message.getAttachment()).setStatus_str(invite.getStatus_str());
            }
        }
        mAdapter.notifyDataSetChanged();
        mPresenter.getP2PUserStatusInfo(femaleGuestId);
    }

    /**
     * 发更新消息
     *
     * @param status
     * @param extra
     */
    public void sendUpdateMsg(int status, String extra) {
        P2pActionAttachment attachment = new P2pActionAttachment(status, extra);
        IMMessage inviteMessage = MessageBuilder.createCustomMessage(
                femaleGuestId, SessionTypeEnum.P2P, "邀请CP开黑", attachment
        );
        sendMessage(inviteMessage, false);
    }

    private void onIncomingAction(int status, String status_str, boolean isVoiceWarning) {
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", AccountUtil.getInstance().getUserId());
        params.put("female_guest_id", femaleGuestId);
        params.put("status", String.valueOf(status));
        params.put("tag", "connect_mic_ques");
        MultiTracker.onEvent("on_incoming_action", params);
        switch (status) {
            case 3://Cp卡到期
            case 6://1.开黑卡到期 2.房主下麦（用户触发关闭） 3.异常下麦（被动）
                //stop
                if (mVoiceRoom != null) {
                    mVoiceRoom.leaveRoom();
                }
                if (isVoiceWarning) {
                    playMicFailedTip();
                    MultiTracker.onEvent(TrackConstants.B_MIC_FAILED_AUDIO_TIP, params);
                }
                break;
            case 4:
                if (count < 1) {
                    sendUpdateMsg(4, "");
                    ++count;
                }
                isInHouse = true;
                if (mMsgUserInfo != null) {
                    tv_status.setText(isInHouse ? "[对方正在当前房间]" : mMsgUserInfo.getStatus_str());
                }
                break;
            case 5:
                count = 0;
                isInHouse = false;
                if (mMsgUserInfo != null) {
                    tv_status.setText(isInHouse ? "[对方正在当前房间]" : mMsgUserInfo.getStatus_str());
                }
                break;
        }
        mPresenter.getP2PUserStatusInfo(femaleGuestId);
    }


    /**
     * 连麦异常语音提示
     */
    private void playMicFailedTip() {
        try {
            if (mMediaPlayer == null) {
                mMediaPlayer = MediaPlayer.create(this, R.raw.mic_failed_tip);
                mMediaPlayer.setLooping(false);
                mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        if (mMediaPlayer != null) {
                            mMediaPlayer.release();
                            mMediaPlayer = null;
                        }
                    }
                });
            }
            mMediaPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean isMyMessage(IMMessage message) {
        return message.getSessionType() == SessionTypeEnum.P2P
                && message.getSessionId() != null
                && message.getSessionId().equals(femaleGuestId);
    }

    private boolean hasShow = false;
    private String target_url = "";

    @Override
    public void onUserInfoToGirlFetched(MsgUserModel.UserInfo userInfo) {
        this.mMsgUserInfo = userInfo;
        if (mMsgUserInfo == null)
            return;
        mIsSendCupidSys = mMsgUserInfo.isSend_cupid_system_msg();
        if (showBuyDialog) {
            showBuyDialog = false;
        }
        List<MsgUserModel.GiftBean> gifts = mMsgUserInfo.getGifts();
        isOfficialAssistant = "0".equals(femaleGuestId);
        if (gifts == null || gifts.size() == 0 || isOfficialAssistant) {
            mInputPanel.setSendGiftVisible(false);
        } else {
            if (SharePreferenceUtil.getBoolean(SharePreferenceUtil.FILE_USER_ACCOUNT_DATA, SharePreferenceUtil.KEY_GIFT_EVER_CLICK, false)) {
                giftTips.setVisibility(View.GONE);
            } else {
                giftTips.setVisibility(View.GONE);
            }
            mInputPanel.setSendGiftVisible(false);
        }
        currentCoins = mMsgUserInfo.getUser_coins();
        tv_status.setText(isInHouse ? "[对方正在当前房间]" : mMsgUserInfo.getStatus_str());
        tv_status.setTextColor(mMsgUserInfo.isIs_online() ? Color.parseColor("#47d1d1") : Color.parseColor("#CBCDD3"));
        tv_title.setText(mMsgUserInfo.getNick_name());
        tv_add_friend.setVisibility(mMsgUserInfo.isIs_friend() || mMsgUserInfo.isShow_couple() ? View.GONE : View.VISIBLE);
        mInputPanel.setQuickList(mMsgUserInfo.getMsg_templates());
        mIsBoxKeyTrial = mMsgUserInfo.isGift_box_key_test();
        isCPend = mMsgUserInfo.isIs_cp_end();
        rl_guard_live.setVisibility(View.GONE);
        if (Constants.hide_peking_user) {
            cp_status_iv.setVisibility(View.GONE);
            active_get_box_key.setVisibility(View.GONE);
        } else {
            if (isCPend) {//true 不是cp，false：cp中
                cp_status_iv.setImageResource(R.mipmap.ic_active_cp);
                cp_status_iv.setEnabled(true);
            } else {
                cp_status_iv.setImageResource(R.mipmap.ic_jiasu);
                cp_status_iv.setEnabled(true);
            }
            if (mMsgUserInfo.isGift_box_key_test() && mMsgUserInfo.getGift_box_key_count() == 0 &&
                    !SharePreferenceUtil.getBoolean(SharePreferenceUtil.FILE_APP_CONFIG,SharePreferenceUtil.KEY_SHOULD_SHOW_KEY_TIP,false) &&
                    cp_status_iv.getVisibility() == View.VISIBLE) {
                active_get_box_key.setVisibility(View.VISIBLE);
                trackUploadKeyExposure();
            }else {
                active_get_box_key.setVisibility(View.GONE);
            }
        }

        if (mMsgUserInfo.rank_info != null) {
            MsgUserModel.RankInfo rank_info = mMsgUserInfo.rank_info;
            if (StringUtil.isNotEmpty(rank_info.frame_url)) {
                chat_bg.setVisibility(View.VISIBLE);
                Glide.with(this).load(rank_info.frame_url).into(chat_bg);
                chat_bg.setVisibility(View.VISIBLE);
            } else {
                chat_bg.setVisibility(View.GONE);
            }
        }
        if (mMsgUserInfo.isShow_team_time_lack() && !hasShow) {
            hasShow = true;
            new CommonDialogBuilder()
                    .setDesc(StringUtil.matcherSearchText(false, Color.parseColor("#F54562"), mMsgUserInfo.getTeam_time_lack_content(), mMsgUserInfo.getTeam_time_lack_time()))
                    .setImageTitleRes(R.mipmap.icon_team_lack_time)
                    .setCancel("取消")
                    .setConfirm("立即购买")
                    .setListener(new CommonActionListener() {
                        @Override
                        public void onConfirm() {
                            //mVoiceRoom.showCpCardDialog(true, false, "", "", femaleGuestId, "", CardCategoryEnum.TEAM_CARD.getCode());
                            MultiTracker.onEvent("b_confirm_buy");
                        }

                        @Override
                        public void onCancel() {
                            MultiTracker.onEvent("b_cancel_buy");
                        }
                    })
                    .create()

                    .show(getSupportFragmentManager(), CommonDialogFragment.class.getSimpleName());
        }
        if (mMsgUserInfo.getCartoon_guide_popup() != null && mMsgUserInfo.getCartoon_guide_popup().getImg() != null) {
            UserDressDialogFragment.newInstance().setCartoonImg(mMsgUserInfo.getCartoon_guide_popup().getImg()).show(getSupportFragmentManager(), UserDressDialogFragment.class.getSimpleName());
            HashMap<String, String> params = new HashMap<>();
            params.put("female_guest_id", femaleGuestId);
            params.put("gender", String.valueOf(AccountUtil.getInstance().getUserInfo().getGender()));
            MultiTracker.onEvent(TrackConstants.B_MESSAGE_VIRTUAL_ALERT_EXPOSURE, params);
        }
        //handleGiftBoxView(mMsgUserModel);
        //控制引导
        MsgUserModel.GiftBox giftBox = mMsgUserInfo.getGift_box();
        if(giftBox != null){
            int session_gift_box_count = giftBox.count;
            boolean show_guide = giftBox.show_guide;   //todo 展示引导

            if(show_guide){
                getHandler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //showGuideViewForGiftBox(iv_enter_gift_box);
                    }
                },200);
            }
        }
    }

    @Override
    public void onUserInfoFetched(MsgUserModel mMsgUserModel) {

    }

    private GuideBuilder builder;
    public void showGuideViewForGiftBox(View anchor){
        if (anchor == null) {
            return;
        }
        anchor.setVisibility(View.VISIBLE);
        builder = new GuideBuilder();
        builder.setTargetView(anchor)
                .setOverlayTarget(true)
                .setAlpha(153)
                .setHighTargetGraphStyle(Component.ROUNDRECT);//高亮区域View的展示样式
        builder.setOnVisibilityChangedListener(new GuideBuilder.OnVisibilityChangedListener() {
            @Override
            public void onShown() {
                Bundle bundle = new Bundle();
                bundle.putString(Constants.USER_ID, AccountUtil.getInstance().getUserId());
                bundle.putString("guest_id", femaleGuestId);
                readyGo(UserCenterActivity.class, bundle);
                MultiTracker.onEvent(TrackConstants.B_GUIDE_GIFT_BOX_EXPOSURE);
            }

            @Override
            public void onDismiss() {
                builder = null;
            }
        });

        ComponentView componentView = new ComponentView(R.mipmap.yindao_icon, Component.ANCHOR_TOP, Component.FIT_END, 0, 0);

        builder.addComponent(componentView);
        Guide guide = builder.createGuide();
        guide.show(SessionActivityV2.this);
    }

    private void handleGiftBoxView(MsgUserModel msgUserModel){
        if (null == msgUserModel && null == msgUserModel.getData())
            return;
        MsgUserModel.GiftBox giftBox = msgUserModel.getData().getGift_box();
        if (null != giftBox && !isOfficialAssistant){
            iv_enter_gift_box.setVisibility(View.VISIBLE);
            if (giftBox.count > 0) {
                tv_gift_box_count.setVisibility(View.VISIBLE);
                tv_gift_box_count.setText(String.valueOf(giftBox.count));
            }else {
                tv_gift_box_count.setVisibility(View.GONE);
            }
            HashMap<String, String> map = new HashMap<>();
            map.put("guest_id", femaleGuestId);
            MultiTracker.onEvent(TrackConstants.B_GIFT_BOX_BUTTON_EXPOSURE, map);
        }else {
            iv_enter_gift_box.setVisibility(View.GONE);
            tv_gift_box_count.setVisibility(View.GONE);
        }
    }

    @Override
    public void onRoomCreate(GameRoomInfo info) {
        if (mVoiceRoom != null) {
            //设置悬浮球相关参数
            mVoiceRoom.onRoomCreate(info.getData().getTeam_info().getTeam_im_id(), info.getData().getTips(), info.getData().getTips_style(), info.getData().getIm_video_token());
            mVoiceRoom.setMicMute(false);
        }
        mPresenter.getP2PUserStatusInfo(femaleGuestId);
        sendUpdateMsg(1, "");
    }

    @Override
    public void onRoomClose(int msg) {
        sendUpdateMsg(msg, "");
        mPresenter.getP2PUserStatusInfo(femaleGuestId);
    }

    @Override
    public void onAddFriendFetched() {
        tv_add_friend.setVisibility(View.GONE);
    }

    @Override
    public void onMoneyNotEnough() {
        new CommonDialogBuilder()
                .setConfirm("立即充值")
                .setWithCancel(true)
                .setImageTitleRes(R.mipmap.icon_not_enough_money)
                .setDesc("金币余额不足    请先充值")
                .setListener(new CommonActionListener() {
                    @Override
                    public void onConfirm() {
                        if (mRechargeCoinDialogFragment.isAdded()) {
                            return;
                        }
                        mRechargeCoinDialogFragment.show(getSupportFragmentManager(), RechargeCoinDialog.class.getName());
                    }

                    @Override
                    public void onCancel() {

                    }
                }).create().show(getSupportFragmentManager(), CommonDialogFragment.class.getSimpleName());
    }

    @Override
    public void sendGiftSuccess(String msg, int code, GiftItemInfo giftInfo,String from) {
        if (giftInfo == null || giftInfo.getData() == null || giftInfo.getData().getGift_item() == null) {
            return;
        }
        GiftItemInfo.DataBean.GiftItemBean giftItemInfo = giftInfo.getData().getGift_item();
        String giftAudio = "";
        if (giftItemInfo.getEffect() != null) {
            giftAudio = giftItemInfo.getEffect().getAudio();
        }
        if (code == 0) {
            showGiftAnimBag(giftItemInfo.getId(), giftItemInfo.getNum(), giftItemInfo.getIcon(), giftAudio, giftItemInfo.getName(),giftItemInfo.getEffect_type());
        } else {
            ToastUtils.s(this, msg);
            if (code == 17039) {
                if (mRechargeCoinDialogFragment.isAdded()) {
                    return;
                }
                mRechargeCoinDialogFragment.setPositon(RechargeCoinDialog.POSITION_SEND_GIFT);
                mRechargeCoinDialogFragment.show(getSupportFragmentManager(), RechargeCoinDialog.class.getName());
            }
        }
    }

    /**
     * 礼物动画弹窗
     * @param giftId
     * @param giftNum
     * @param giftPic
     * @param giftAudio
     * @param giftName
     * @param giftEffectType  0-普通礼物,1-盲盒
     */
    public void showGiftAnimBag(int giftId, int giftNum, String giftPic, String giftAudio, String giftName,int giftEffectType) {
        if (giftAnimationPopupWindow == null) {
            giftAnimationPopupWindow = new GiftAnimationPopupWindow(this);
            giftAnimationPopupWindow.setOnAnimationEnd(() -> {
                canShowCPLevelDialog = true;
                canShowCpUpgradeDialog = true;
                if (canShowCPLevelDialog && mVoiceRoom != null && StringUtil.isNotEmpty(levelName)) {
                    mVoiceRoom.updateLevelDialog(levelName);
                    levelName = null;
                }

                if (canShowCpUpgradeDialog && mVoiceRoom != null && attachment != null) {
                    MsgUserModel.P2PRankUpgradeAwardInfo p2PRankUpgradeAwardInfo = new MsgUserModel.P2PRankUpgradeAwardInfo();
                    p2PRankUpgradeAwardInfo.group_name = attachment.getGroup_name();
                    p2PRankUpgradeAwardInfo.award_list = new Gson().fromJson(attachment.getAward_list(), new TypeToken<List<MsgUserModel.P2PRankUpgradeAwardInfo.Info>>() {
                    }.getType());
                    mVoiceRoom.updateCpUpgradeDialog(p2PRankUpgradeAwardInfo);
                    attachment = null;
                }
                return null;
            });
        }
        giftAnimationPopupWindow.showGiftAnimation(giftId, giftNum, giftPic, giftAudio, giftName, giftEffectType);
        canShowCPLevelDialog = false;
        canShowCpUpgradeDialog = false;
        uploadSendGiftSuccessData(giftId);
    }

    public void setCpStatus(boolean clicked, boolean clicked_team) {
        if (mPresenter != null) {
            mPresenter.setExptCpAvailable(femaleGuestId, clicked, clicked_team);
        }
    }

    @OnClick({R.id.iv_left, R.id.iv_user_center, R.id.tv_add_friend, R.id.rl_guard_live, R.id.cp_status_iv,R.id.iv_enter_gift_box,R.id.close_tip})
    public void onClick(View view) {
        if (ViewUtils.isFastClick()) {
            return;
        }

        switch (view.getId()) {
            case R.id.iv_user_center:
                Bundle bundle = new Bundle();
                bundle.putString(Constants.USER_ID, femaleGuestId);
                bundle.putString(Constants.GIFT_RANK_PATH, "private_chat");
                readyGo(UserCenterActivity.class, bundle);
                MultiTracker.onEvent(TrackConstants.B_P2P_USER_CENTER);
                break;
            case R.id.iv_left:
                if (showFloatDialog()) {
                    return;
                }
                onBackPressed();
                break;
            case R.id.close_tip:
                mPresenter.cpScoreHint(femaleGuestId);
                guide_upgrade_cp_iv.setVisibility(View.GONE);
                trackUploadCloseTipInfo();
                break;
            case R.id.tv_add_friend:
                mPresenter.addFriend(femaleGuestId);
                break;
            case R.id.rl_guard_live:
                if (mMsgUserInfo != null) {
                    if (!mMsgUserInfo.isIs_in_private_room()) {
                        mPresenter.fetchRoomDetails(mMsgUserInfo.couple_room_id);
                    } else {
                        showMessage(mMsgUserInfo.getNick_name() + "正在专属房间");
                    }
                }
                break;
            case R.id.cp_status_iv:
                if (mIsBoxKeyTrial) {
                    showBuyCpDialog(1,"");
                    SharePreferenceUtil.saveBoolean(SharePreferenceUtil.FILE_APP_CONFIG,SharePreferenceUtil.KEY_SHOULD_SHOW_KEY_TIP,true);
                }else {
                    if (mVoiceRoom != null) {
                        mVoiceRoom.showCpCardDialog(false, false, "", "", femaleGuestId, "", CardCategoryEnum.CP_CARD.getCode());
                    }
                }
                trackUploadActiveSuperCp();
                break;
            case R.id.iv_enter_gift_box:
                showGiftBoxDialog(femaleGuestId);
                HashMap<String, String> map = new HashMap<>();
                map.put("guest_id", femaleGuestId);
                MultiTracker.onEvent(TrackConstants.B_GIFT_BOX_BUTTON_CLICK, map);
                break;
        }
    }

    private void trackUploadCloseTipInfo() {
        HashMap<String,String> params = new HashMap<>();
        params.put(Constants.GUEST_ID,femaleGuestId);
        MultiTracker.onEvent(TrackConstants.B_CLICK_RECEIVE_AWARD_CLICK,params);
    }

    @Override
    public void onRoomDetailsFetched(RoomDetailsInfo roomDetailsInfo) {
        if (roomDetailsInfo != null && roomDetailsInfo.getData() != null) {
            if (roomDetailsInfo.getData().getRoom_type() == RoomLiveActivityV2.ROOM_TYPE_PRIVATE) {
                ToastUtils.s(this, "此房间是专属房");
            } else {
                Bundle liveBundle = new Bundle();
                liveBundle.putString(Constants.ROOM_ID, roomDetailsInfo.getData().getRoom_id());
                liveBundle.putString(Constants.IM_ROOM_ID, roomDetailsInfo.getData().getIm_room_id());
                liveBundle.putString(Constants.AUDIENCE_FROM, DataOptimize.MSG_DETAIL);
                RouterUtils.goToRoomLiveActivity(this,liveBundle);
//                readyGo(RoomLiveActivityV2.class, liveBundle);
                HashMap<String, String> params = new HashMap<>();
                params.put("user_id", AccountUtil.getInstance().getUserId());
                params.put("female_guest_id", femaleGuestId);
                params.put("video_room_click_from", "session");
                MultiTracker.onEvent(TrackConstants.B_GO_TEAM, params);
            }
        }
    }

    @Override
    public void cartoonHide() {

    }

    @Override
    public void showBoxKeyTip(BoxKeyNumBean.Data data) {

    }

    @Override
    public void onDressUpdate(SessionDressBean sessionDressBean) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case PictureConfig.CHOOSE_REQUEST:
                case PictureConfig.REQUEST_CAMERA:
                    // onResult Callback
                    List<LocalMedia> selectList = PictureSelector.obtainMultipleResult(data);
                    LocalMedia localMedia = selectList.get(0);
                    String path = localMedia.getPath();
                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.P) {
                        path = localMedia.getAndroidQToPath();
                    }
                    IMMessage imMessage = MessageBuilder.createImageMessage(femaleGuestId, SessionTypeEnum.P2P, new File(path), localMedia.getFileName());
                    imMessage.setContent(path);
                    sendMessage(imMessage, false);
                    break;

                default:
                    break;
            }
        }
    }

    public void updateP2PUi() {
        if (cp_status_iv != null) {
            cp_status_iv.setImageResource(R.mipmap.ic_jiasu);
        }
    }

    public void sendMessage(IMMessage message, boolean resend) {
        Map map = new HashMap();
        if (message.getAttachment() instanceof P2pActionAttachment || message.getAttachment() instanceof InviteCoupleAttachment) {
            map.put("system", true);
        }
        map.put("version_name", GlobalConfig.instance().getClientVersion());
        message.setRemoteExtension(map);

        if (Constants.getUseTestApi()) {
            message.setEnv("poros_test");
        } else {
            message.setEnv("poros_prod");
        }

        NIMClient.getService(MsgService.class).sendMessage(message, resend).setCallback(new RequestCallback<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
            }

            @Override
            public void onFailed(int i) {
            }

            @Override
            public void onException(Throwable throwable) {
            }
        });
        if (!(message.getAttachment() instanceof P2pActionAttachment)) {
            if (!resend) {
                mAdapter.add(message);
                mAdapter.updateShowTimeItem(message, false, true);
                mLayoutManager.scrollToPosition(mAdapter.getItemCount() - 1);
                if (mMsgUserInfo != null && mMsgUserInfo.isIs_mic_minute_cost()) {
                    mAdapter.setNeedShowAnim(true);
                    getHandler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mAdapter.setNeedShowAnim(false);
                        }
                    }, 2000);
                }
            }
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i("PTAGlobal", "onRestart");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i("PTAGlobal", "onStop");
    }

    @Override
    protected void onPause() {
        Log.i("PTAGlobal", "onPause");
        sendUpdateMsg(5, "");
        if (mAdapter != null) {
            mPresenter.readMessage(mAdapter.getLastestMsgTime(), femaleGuestId,false);
        }
        boolean result = FloatManager.getInstance().checkPermission(mContext);
        if (mVoiceRoom != null && result) {
            mVoiceRoom.onPause();
        }
        super.onPause();
        isPause = true;

        if (exposureUtils != null) {
            exposureUtils.calExplosure();
            exposureUtils.stopFeedExposureStatistics();
        }
    }

    @Override
    protected void onResume() {
        Log.i("PTAGlobal", "onResume");
        sendUpdateMsg(4, "");
        if (mVoiceRoom != null) {
            mVoiceRoom.onResume();
        }
        super.onResume();
        isPause = false;
        if (needRefresh) {
            if (mPresenter != null) {
                mPresenter.getP2PUserStatusInfo(femaleGuestId);
                needRefresh = false;
            }
        }

        if (exposureUtils == null) {
            exposureUtils = new IMMessageExposureUtils(mAdapter, mAdapter.getDatas());
            exposureUtils.setmRecyclerView(rv_messages);
            exposureUtils.setFemaleGuestId(femaleGuestId);
        }
        exposureUtils.startFeedExposureStatistics();
    }

    @Override
    protected void onDestroy() {
        Log.i("PTAGlobal", "onDestroy");
        mPresenter.detachView();
        boolean result = FloatManager.getInstance().checkPermission(mContext);
        if (mVoiceRoom != null) {
            if (((mVoiceRoom.isOnMic() && !result) || (mVoiceRoom.isAvRunning() && !mVoiceRoom.isOnMic())) && StringUtil.isEquals(FloatManager.getInstance().getSessionId(), femaleGuestId)) {
                HashMap<String, String> params = new HashMap<>();
                params.put("user_id", AccountUtil.getInstance().getUserId());
                params.put("female_guest_id", femaleGuestId);
                params.put("tag", "connect_mic_ques");
                MultiTracker.onEvent("session_activity_on_destroy", params);
                mVoiceRoom.leaveRoom();
                mVoiceRoom.closeConnection();
            }
            if (!mVoiceRoom.isAvRunning()) {
                mVoiceRoom.closeConnection();
            }
            mVoiceRoom.onDestroy();
        }

        if (mAdapter != null) {
            mAdapter.onDestroy();
        }
        mInputPanel.onDestroy();
        registerObservers(false);
        super.onDestroy();
    }

    @Override
    protected boolean isCurrent(PlayloadInfo info) {
        return StringUtil.isEquals(femaleGuestId, info.getAccount());
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (BASIC_PERMISSION_REQUEST_CODE == requestCode) {
            MPermission.onRequestPermissionsResult(this, requestCode, permissions, grantResults);
        }
    }

    @OnMPermissionGranted(BASIC_PERMISSION_REQUEST_CODE)
    public void onPermissionSuccess() {
        if (mVoiceRoom != null) {
            mPresenter.createCpRoom(mVoiceRoom.getCurrentTeamId(), femaleGuestId, inviteId, roomId);
        }
    }

    @OnMPermissionDenied(BASIC_PERMISSION_REQUEST_CODE)
    @OnMPermissionNeverAskAgain(BASIC_PERMISSION_REQUEST_CODE)
    public void onBasicPermissionFailed() {
        Toast.makeText(mContext, "音视频通话所需权限未全部授权，部分功能可能无法正常运行", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        if (sendGiftPanel.isShowing()) {
            sendGiftPanel.dismiss();
        } else {
            if (showFloatDialog()) {
                return;
            }
            super.onBackPressed();
        }
    }

    /**
     * 悬浮球弹窗
     */
    private boolean showFloatDialog() {
        boolean result = FloatManager.getInstance().checkPermission(mContext);
        if (!result && mVoiceRoom != null && mVoiceRoom.isOnMic()) {
            HashMap<String, String> params = new HashMap<>();
            params.put("user_id", AccountUtil.getInstance().getUserId());
            params.put("female_guest_id", femaleGuestId);
            params.put("type", "back_pressed");
            MultiTracker.onEvent(TrackConstants.P_FLOAT_SHOW, params);
            new CommonDialogBuilder()
                    .setDesc(getString(R.string.float_title))
                    .setImageTitleRes(R.mipmap.icon_open_float)
                    .setConfirm(getString(R.string.float_open))
                    .setCancel(getString(R.string.float_cancel))
                    .setListener(new CommonActionListener() {
                        @Override
                        public void onConfirm() {
                            try {
                                if (Build.VERSION.SDK_INT < 23) {
                                    if (RomUtils.checkIsMiuiRom()) {
                                        MiuiUtils.applyMiuiPermission(mContext);
                                    } else if (RomUtils.checkIsMeizuRom()) {
                                        MeizuUtils.applyPermission(mContext);
                                    } else if (RomUtils.checkIsHuaweiRom()) {
                                        HuaweiUtils.applyPermission(mContext);
                                    } else if (RomUtils.checkIs360Rom()) {
                                        QikuUtils.applyPermission(mContext);
                                    } else if (RomUtils.checkIsOppoRom()) {
                                        OppoUtils.applyOppoPermission(mContext);
                                    }
                                } else {
                                    if (RomUtils.checkIsMeizuRom()) {
                                        MeizuUtils.applyPermission(mContext);
                                    } else {
                                        FloatManager.commonROMPermissionApplyInternal(mContext);
                                    }
                                }
                            } catch (NoSuchFieldException e) {
                                e.printStackTrace();
                            } catch (IllegalAccessException e) {
                                e.printStackTrace();
                            }
                            HashMap<String, String> params = new HashMap<>();
                            params.put("user_id", AccountUtil.getInstance().getUserId());
                            params.put("female_guest_id", femaleGuestId);
                            params.put("type", "back_pressed");
                            MultiTracker.onEvent(TrackConstants.B_FLOAT_OPEN, params);
                        }

                        @Override
                        public void onCancel() {
                            HashMap<String, String> params = new HashMap<>();
                            params.put("user_id", AccountUtil.getInstance().getUserId());
                            params.put("female_guest_id", femaleGuestId);
                            params.put("type", "back_pressed");
                            MultiTracker.onEvent(TrackConstants.B_FLOAT_CANCEL, params);
                            finish();
                        }
                    }).create().show(getSupportFragmentManager(), CommonDialogFragment.class.getSimpleName());
            return true;
        }
        return false;
    }

    public void checkPermission(int code) {
        List<String> lackPermissions = NERtcEx.getInstance().checkPermission(mContext);
        if (lackPermissions.isEmpty()) {
            onPermissionSuccess();
        } else {
            String[] permissions = new String[lackPermissions.size()];
            for (int i = 0; i < lackPermissions.size(); i++) {
                permissions[i] = lackPermissions.get(i);
            }
            MPermission.with(this)
                    .setRequestCode(code)
                    .permissions(permissions)
                    .request();
        }
    }

    @Override
    public void toBeNormalFriend() {

    }

    @Override
    public void onInviteCoupleAction(int code, String cp_order_id, MsgDirectionEnum direction,boolean isTrial) {
        if (direction == MsgDirectionEnum.In) {
            if (code == -1) {
                //拒绝组cp
                mPresenter.refuseCoupleInvite(cp_order_id);
            } else {
                if (isTrial) {
                    showBuyCpDialog(1,cp_order_id);
                }else {
                    if (mVoiceRoom != null) {
                        mVoiceRoom.showCpCardDialog(true, false, "", "", femaleGuestId, cp_order_id, CardCategoryEnum.CP_CARD.getCode());
                    }
                }
            }
        }
    }

    @Override
    public void onInviteCoupleGameAction() {
        if (CPMatchingGlobal.getInstance(SessionActivityV2.this).isMatching) {
            Toast.makeText(SessionActivityV2.this, getString(R.string.audio_match_float_ball_tip), Toast.LENGTH_SHORT).show();
            return;
        }
        onInviteGameMic();
    }

    private void onInviteGameMic() {
        if (isOnMic) {
            ToastUtils.s(this, "您已经在麦上");
            return;
        }
        if (ViewUtils.isFastClick() || mVoiceRoom == null || mMsgUserInfo == null) {
            return;
        }
        if (mVoiceRoom.isOnMic()) {
            showMessage("您已上麦");
            return;
        }
        if (mMsgUserInfo.isIs_mic_minute_cost()) {
            if (mVoiceRoom.getCurrentCpStatus() == 5) {
                showLoading();
                checkPermission(BASIC_PERMISSION_REQUEST_CODE);
            } else {
                mVoiceRoom.showCpCardDialog(true, false, "", "", femaleGuestId, null, CardCategoryEnum.TEAM_CARD.getCode());
            }
        } else if (mVoiceRoom.getCurrentCpStatus() == 2 || mVoiceRoom.getCurrentCpStatus() == 5) {
            showLoading();
            checkPermission(BASIC_PERMISSION_REQUEST_CODE);
        } else {
            mVoiceRoom.showCpCardDialog(true, false, "", "", femaleGuestId, null, CardCategoryEnum.TEAM_CARD.getCode());
        }
    }

    @Override
    protected boolean needSetTransparent() {
        return false;
    }

    @Override
    public void onRecharge() {
        if (mRechargeCoinDialogFragment.isAdded()) {
            return;
        }
        mRechargeCoinDialogFragment.setPositon(RechargeCoinDialog.POSITION_SEND_MESSAGE);
        mRechargeCoinDialogFragment.show(getSupportFragmentManager(), RechargeCoinDialog.class.getName());
    }

    @Override
    public void onShowCpCard() {
        if (mIsBoxKeyTrial) {
            showBuyCpDialog(1,"");
        }else {
            if (mVoiceRoom != null) {
                mVoiceRoom.showCpCardDialog(true, false, "", "", femaleGuestId, null, CardCategoryEnum.CP_CARD.getCode());
            }
        }
    }

    @Override
    public void onLongClickMsg(View clickView, IMMessage item) {
        if (item.getAttachment() instanceof InviteCoupleGameAttachment || item.getAttachment() instanceof InviteCoupleAttachment) {
            return;
        }
        boolean isMaster = AccountUtil.getInstance().getUserInfo().getIm_account().equals(item.getFromAccount());
        List<TipItem> tipItems = new ArrayList<>();
        //复制
        tipItems.add(new TipItem(MessageAction.COPY.getDesc(), Color.WHITE, MessageAction.COPY));
        //发送端显示撤回
        if (isMaster) {
            //撤回
            tipItems.add(new TipItem(MessageAction.RECALL.getDesc(), Color.WHITE, MessageAction.RECALL));
        }
        int[] location = new int[2];
        clickView.getLocationOnScreen(location);
        float axisY = (float) location[1];
        float axisX = (float) location[0];
        new TipView.Builder(this, ll_container, (int) axisX + clickView.getWidth() / 2, (int) axisY, clickView.getMeasuredHeight())
                .addItems(tipItems)
                .setOnItemClickListener(new TipView.OnItemClickListener() {
                    @Override
                    public void onItemClick(String str, final MessageAction action) {
                        switch (action) {
                            case COPY:
                                if (item.getMsgType().equals(MsgTypeEnum.text)) {
                                    ClipboardManager myClipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                                    ClipData myClip = ClipData.newPlainText("text", item.getContent());
                                    myClipboard.setPrimaryClip(myClip);
                                    showMessage("已经复制到粘贴板");
                                } else {
                                    showMessage("不支持当前消息");
                                }
                                break;
                            case RECALL:
                                new CommonDialogBuilder().setTitle("确定撤回该信息？").setDesc("撤回后将无法恢复").setListener(new CommonActionListener() {
                                    @Override
                                    public void onConfirm() {
                                        mPresenter.recallMessage(item.getUuid(), femaleGuestId,false);
                                    }

                                    @Override
                                    public void onCancel() {

                                    }
                                }).create().show(getSupportFragmentManager(), CommonDialogFragment.class.getSimpleName());
                                break;
                        }
                    }

                    @Override
                    public void dismiss() {

                    }
                }).create();
    }

    @Override
    public void onAvatarClick() {
        Bundle bundle = new Bundle();
        bundle.putString(Constants.USER_ID, femaleGuestId);
        bundle.putString(Constants.GIFT_RANK_PATH, "private_chat");
        readyGo(UserCenterActivity.class, bundle);
        MultiTracker.onEvent(TrackConstants.B_P2P_USER_HEAD);
    }

    @Override
    public void onResetSendMessage(IMMessage message) {
        if (!mIsConnected) {
            Toast.makeText(SessionActivityV2.this, getString(R.string.message_send_not_connected), Toast.LENGTH_SHORT).show();
            return;
        }
        new CommonDialogBuilder()
                .setWithCancel(true)
                .setTitle(getString(R.string.message_resend_title))
                .setDesc(getString(R.string.message_resend_content))
                .setConfirm(getString(R.string.message_resend))
                .setListener(new CommonActionListener() {
                    @Override
                    public void onConfirm() {
                        sendMessage(message, true);
                    }

                    @Override
                    public void onCancel() {

                    }
                }).create().show(getSupportFragmentManager(), CommonDialogFragment.class.getSimpleName());
    }

    @Override
    public void onReadMessage(IMMessage message) {
        message.setStatus(MsgStatusEnum.read);
        NIMClient.getService(MsgService.class).updateIMMessageStatus(message);
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
        mPresenter.p2pAudioMsgRead(message.getUuid());
    }

    @Override
    public void onDeclarationInvite(CpLevelUpInviteAttach attach, IMMessage imMessage) {
        if (ViewUtils.isFastClick()) {
            return;
        }
        if (imMessage != null) {
            if (imMessage.getDirect() == MsgDirectionEnum.Out) {
                //代表我自己发出的消息，跳转晋级情况

                Bundle bundle = new Bundle();
                bundle.putString("female_user_id", femaleGuestId);
                if (mMsgUserInfo != null) {
                    bundle.putString("female_user_head", mMsgUserInfo.getAvatar_url());
                    bundle.putString("female_user_name", mMsgUserInfo.getNick_name());
                }
                bundle.putInt("level", attach.getLevel());
                if (mMsgUserInfo != null && mMsgUserInfo.rank_info != null) {
                    bundle.putString("intimacy", mMsgUserInfo.rank_info.cp_score + "");
                }
                readyGo(DeclarationDetailActivity.class, bundle);

            } else {
                //代表别人发出的消
                jumpToDeclaration(attach.getLevel());
            }
        }

        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", AccountUtil.getInstance().getUserId());
        params.put("female_guest_id", femaleGuestId);
        params.put("event_time", String.valueOf(System.currentTimeMillis()));
        params.put("rise_to", attach.getLevel() + "");
        if (mMsgUserInfo != null && mMsgUserInfo.rank_info != null) {
            params.put("intimacy", mMsgUserInfo.rank_info.cp_score + "");
        }
        MultiTracker.onEvent("b_click_card_cp_rise_invite", params);
    }

    @Override
    public void onDeclarationInviteSuccess(CpLevelUpSuccessAttach attach, IMMessage imMessage) {
        if (ViewUtils.isFastClick()) {
            return;
        }
        Bundle bundle = new Bundle();
        bundle.putString("female_user_id", femaleGuestId);
        if (mMsgUserInfo != null) {
            bundle.putString("female_user_head", mMsgUserInfo.getAvatar_url());
            bundle.putString("female_user_name", mMsgUserInfo.getNick_name());
        }
        bundle.putInt("level", attach.getLevel());
        if (mMsgUserInfo != null && mMsgUserInfo.rank_info != null) {
            bundle.putString("intimacy", mMsgUserInfo.rank_info.cp_score + "");
        }
        readyGo(DeclarationDetailActivity.class, bundle);
    }

    @Override
    public void onVirtualBuy(AskPTAGiftAttachment askPTAGiftAttachment) {
        SendVirtualDialogFragment sendVirtualDialogFragment = SendVirtualDialogFragment.Companion.newInstance();
        sendVirtualDialogFragment.setAskPTAGiftAttachment(askPTAGiftAttachment);
        sendVirtualDialogFragment.setCoin(currentCoins);
        sendVirtualDialogFragment.setFemaleGuestId(femaleGuestId);
        sendVirtualDialogFragment.show(getSupportFragmentManager(), SendVirtualDialogFragment.class.getSimpleName());

        HashMap<String, String> params = new HashMap<>();
        params.put("female_guest_id", femaleGuestId);
        params.put("dress_id", askPTAGiftAttachment.getSuit_id());
        MultiTracker.onEvent(TrackConstants.B_GIVE_TO_TA_ALERT_EXPOSURE, params);
    }

    @Override
    public void onAudioMatch(String audioMatchType) {
        if (CPMatchingGlobal.getInstance(this).isMatching) {
            showMessage(getString(R.string.audio_matching_tip));
            return;
        }
        //跳转语音速配
        matchInfo(audioMatchType);
    }

    @Override
    public void onCheckPool() {
        if(ViewUtils.isFastClick()){
            return;
        }
        Bundle bundle = new Bundle();
        bundle.putString(Constants.TARGET_PATH, Constants.GIFT_BOX_RULE_URL);
        bundle.putString(Constants.FROM, "open_gift");
        bundle.putString("guest_id",femaleGuestId);
        Intent intent = new Intent(SessionActivityV2.this, WebViewActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
//        startActivity(new Intent(this, GiftBoxRuleActivity.class));

        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", AccountUtil.getInstance().getUserId());
        params.put("guest_id", femaleGuestId);
        params.put("event_time", String.valueOf(System.currentTimeMillis()));
        MultiTracker.onEvent(TrackConstants.B_CARD_PRIZE_POOL_CLICK, params);
    }

    private void trackUploadKeyExposure() {
        HashMap<String,String> params = new HashMap<>();
        params.put(Constants.GUEST_ID,femaleGuestId);
        MultiTracker.onEvent(TrackConstants.B_ACTIVATION_KEY_EXPOSURE,params);
    }

    @Override
    public void onCheckGiftBox() {
        showGiftBoxDialog(femaleGuestId);

        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", AccountUtil.getInstance().getUserId());
        params.put("guest_id", femaleGuestId);
        params.put("event_time", String.valueOf(System.currentTimeMillis()));
        MultiTracker.onEvent(TrackConstants.B_CARD_GOTOSEE_CLICK, params);
    }

    private void matchInfo(String type) {
        if (TAG_WANGZHE.equals(type)) {
            mAudioMatchTitle = getString(R.string.audio_match_info_wangzhe_title);
        } else if (TAG_HEPING.equals(type)) {
            mAudioMatchTitle = getString(R.string.audio_match_info_heping_title);
        } else {
            mAudioMatchTitle = getString(R.string.audio_match_info_chat_title);
        }
        Bundle bundle = new Bundle();
        bundle.putString(Constants.AUDIO_MATCH_INFO_TITLE, mAudioMatchTitle);
        bundle.putString(Constants.AUDIO_MATCH_INFO_TYPE, type);
        bundle.putInt(Constants.AUDIO_MATCH_ENTRY_TYPE, Constants.ENTRY_MESSAGE);
        readyGo(AudioMatchInfoActivity.class, bundle);
        HashMap<String, String> params = new HashMap<>();
        params.put("event_time", String.valueOf(System.currentTimeMillis()));
        params.put("match_type", type);
        params.put("entry_type", "message");
        MultiTracker.onEvent(TrackConstants.B_TEAM_JOIN_START_BUTTON_CLICK, params);
    }

    private void jumpToDeclaration(int level) {
        checkCpLevelDetail(level);
        //我发过宣言，跳转到 宣言详情
    }

    private void checkCpLevelDetail(int level) {
        try {
            JSONObject object = new JSONObject();
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("female_user_id", femaleGuestId);
            object.put("level", level + "");
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.checkCpLevelDetail(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    CpLevelUpDetail cpLevelUpDetail = GT.fromJson(result, CpLevelUpDetail.class);
                    if (cpLevelUpDetail != null) {
                        if (cpLevelUpDetail.getCode() == 0) {
                            CpLevelUpDetail.DataBean dataBean = cpLevelUpDetail.getData();
                            boolean level_swear = dataBean.isLevel_swear();
                            if (level_swear) {
                                //我发过宣言，跳转到 宣言详情
                                Bundle bundle = new Bundle();
                                bundle.putString("female_user_id", femaleGuestId);
                                if (mMsgUserInfo != null) {
                                    bundle.putString("female_user_head", mMsgUserInfo.getAvatar_url());
                                    bundle.putString("female_user_name", mMsgUserInfo.getNick_name());
                                }
                                bundle.putInt("level", level);
                                if (mMsgUserInfo != null && mMsgUserInfo.rank_info != null) {
                                    bundle.putString("intimacy", mMsgUserInfo.rank_info.cp_score + "");
                                }
                                readyGo(DeclarationDetailActivity.class, bundle);
                            } else {
                                //我还没发过宣言，跳转到邀请页面
                                Bundle bundle = new Bundle();
                                bundle.putString("female_user_id", femaleGuestId);
                                if (mMsgUserInfo != null) {
                                    bundle.putString("female_user_head", mMsgUserInfo.getAvatar_url());
                                    bundle.putString("female_user_name", mMsgUserInfo.getNick_name());
                                }
                                bundle.putInt("level", level);
                                readyGo(HalfDeclarationActivity.class, bundle);
                            }
                        } else if (cpLevelUpDetail.getCode() == 20463) {
                            // 您已经晋级此等级
                            String msg = cpLevelUpDetail.getMsg();
                            if (StringUtil.isNotEmpty(msg)) {
                                Toast.makeText(ContextUtils.getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            String msg = cpLevelUpDetail.getMsg();
                            if (com.liquid.base.tools.StringUtil.isNotEmpty(msg)) {
                                Toast.makeText(ContextUtils.getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }

                @Override
                public void OnFailed(int ret, String result) {
                }
            });
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSendMsg(IMMessage message) {
        sendMessage(message, false);
    }

    @Override
    public void onInputPanelExpand() {
        getHandler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (rv_messages != null && mAdapter != null && mAdapter.getItemCount() > 0) {
                    rv_messages.scrollToPosition(mAdapter.getItemCount() - 1);
                }
            }
        },200);
    }

    @Override
    public void onSendMsgAndSys(IMMessage message) {
        if (mIsSendCupidSys) {
            CupidSysAttachment attachment = new CupidSysAttachment("SEND_CUPID_SYSTEM");
            attachment.setMsg("“丘比特”牵线成功，给对方发消息 可领取系统红包哦～");
            IMMessage cupidMsg = MessageBuilder.createCustomMessage(
                    femaleGuestId, SessionTypeEnum.P2P, "", attachment
            );
            HashMap<String,Object> ext = new HashMap<>();
            ext.put("show_uid",femaleGuestId);
            cupidMsg.setRemoteExtension(ext);
            if (Constants.getUseTestApi()) {
                cupidMsg.setEnv("poros_test");
            } else {
                cupidMsg.setEnv("poros_prod");
            }
            uploadReplayCupidData();
            NIMClient.getService(MsgService.class).sendMessage(cupidMsg,false).setCallback(new RequestCallback<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                }

                @Override
                public void onFailed(int i) {
                }

                @Override
                public void onException(Throwable throwable) {
                }
            });
            getHandler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    sendMessage(message, false);
                }
            }, 1000);
            mIsSendCupidSys = false;
        }else {
            sendMessage(message, false);
        }
    }

    //回复丘比特埋点上传
    private void uploadReplayCupidData() {
        HashMap<String,String> params = new HashMap<>();
        MultiTracker.onEvent(TrackConstants.B_USER_CUPID_REPLY_COUNT,params);
    }

    @Override
    public void notifyNetworkChanged(boolean isConnected) {
        super.notifyNetworkChanged(isConnected);
        mIsConnected = isConnected;
    }

    @Override
    public void onChoosePicture() {
        PictureSelector.create(this)
                .openGallery(PictureMimeType.ofImage())
                .maxSelectNum(1)
                .isAndroidQTransform(true)
                .imageEngine(GlideEngine.createGlideEngine())
                .forResult(PictureConfig.CHOOSE_REQUEST);
    }

    @Override
    public void onTakePicture() {
        PictureSelector.create(this)
                .openCamera(PictureMimeType.ofImage())
                .maxSelectNum(1)
                .isAndroidQTransform(true)
                .imageEngine(GlideEngine.createGlideEngine()) // Please refer to the Demo GlideEngine.java
                .forResult(PictureConfig.REQUEST_CAMERA);
    }

    @Override
    public void sendGift() {
        KeyboardHelper.hintKeyBoard(this);
        if (mIsBoxKeyTrial) {
            trackUploadSuperCpPanel();
            showBuyCpDialog(0,"");
        }else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    uploadGiftClickData();
                    sendGiftPanel.showPopupWindow();
                }
            }, 200);
        }
    }

    private void trackUploadSuperCpPanel() {
        HashMap<String, String> params = new HashMap<>();
        params.put(Constants.GUEST_ID, femaleGuestId);
        params.put(Constants.IS_SUPER_CP,String.valueOf(!isCPend));
        MultiTracker.onEvent(TrackConstants.B_GIFT_SUPER_CP_CLICK , params);
    }

    public void hideActiveBoxKey() {
        if (active_get_box_key.getVisibility() == View.VISIBLE) {
            active_get_box_key.setVisibility(View.GONE);
        }
    }

    public void showBuyCpDialog(int pos,String cpOrderId) {
        if (active_get_box_key.getVisibility() == View.VISIBLE && pos == 1) {
            active_get_box_key.setVisibility(View.GONE);
        }
        if (buyCpFragment == null) {
            buyCpFragment = new MultiGiftSendDialog(this);
            buyCpFragment.show(buyCpFragment, SessionActivityV2.this,null,mMsgUserInfo.getGifts(),mMsgUserInfo.getGift_nums(),femaleGuestId,cpOrderId,"");
        }else {
            buyCpFragment.setCpOrderId(cpOrderId);
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.remove(buyCpFragment);
            ft.commit();
            buyCpFragment.show(getSupportFragmentManager(),"");
        }
        buyCpFragment.setIsCp(!isCPend);
        getHandler().post(new Runnable() {
            @Override
            public void run() {
                buyCpFragment.setCurrentItem(pos);
            }
        });
    }

    private void trackUploadActiveSuperCp() {
        HashMap<String,String> params = new HashMap<>();
        params.put(Constants.GUEST_ID,femaleGuestId);
        params.put(Constants.IS_SUPER_CP,String.valueOf(!isCPend));
        MultiTracker.onEvent(TrackConstants.B_ACTIVATION_SUPER_CP_CLICK,params);
    }

    private void uploadGiftClickData() {
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", AccountUtil.getInstance().getUserId());
        params.put("female_guest_id", femaleGuestId);
        params.put("event_time", String.valueOf(System.currentTimeMillis()));
        MultiTracker.onEvent(TrackConstants.B_GIFT_BUTTON, params);
    }

    public void normalGiftAnim(View view) {
        AnimatorSet animatorSetsuofang = new AnimatorSet();//组合动画
        ObjectAnimator scaleX = ObjectAnimator.ofFloat(view, "translationX", -500, 0);
        ObjectAnimator anim = ObjectAnimator.ofFloat(view, "alpha", 0, 1f);
        animatorSetsuofang.setDuration(2000);
        animatorSetsuofang.setInterpolator(new DecelerateInterpolator());
        animatorSetsuofang.play(scaleX).with(anim);//两个动画同时开始
        animatorSetsuofang.start();
        animatorSetsuofang.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {

            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
    }

    @Override
    public void buyMicCard() {
        if (mVoiceRoom != null) {
            mVoiceRoom.showCpCardDialog(false, false, "", "", femaleGuestId, "", CardCategoryEnum.TEAM_CARD.getCode());
        }
    }

    public void showGiftBoxDialog(String fromUserId) {
        dismissGiftBoxDialog();
        giftBoxDialog = new GiftBoxDialog();
        Bundle bundle = new Bundle();
        bundle.putSerializable(GiftBoxDialog.FROM_USER_ID,fromUserId);
        giftBoxDialog.setArguments(bundle);
        giftBoxDialog.show(getSupportFragmentManager());
    }

    public void dismissGiftBoxDialog() {
        if (null != giftBoxDialog){
            giftBoxDialog.dismiss();
            giftBoxDialog = null;
        }
    }

    @Override
    public void showGiftDialog(GiftOfBox gift) {
        if (null == gift)
            return;
        gift.target_user_id = femaleGuestId;
        GiftDetailDialog giftBoxDialog = new GiftDetailDialog();
        Bundle bundle = new Bundle();
        bundle.putSerializable(GiftDetailDialog.INFO,gift);
        giftBoxDialog.setArguments(bundle);
        giftBoxDialog.show(getSupportFragmentManager());
    }

    @Override
    public void blindBoxIntroSuccess() {

    }

    private void closeYourDialogFragment() {
//        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//        Fragment fragmentToRemove = getSupportFragmentManager().findFragmentByTag(GotKeyDialog.class.getSimpleName());
//        if (fragmentToRemove != null) {
//            ft.remove(fragmentToRemove);
//        }
//        ft.commit(); // or ft.commitAllowingStateLoss()

        for (int i = 0; i < dialogArrayList.size(); i++) {
            GotKeyDialog gotKeyDialog = dialogArrayList.get(i);
            gotKeyDialog.dismissAllowingStateLoss();
        }
    }
}
