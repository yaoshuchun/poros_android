package com.liquid.poros.ui.activity

import android.animation.ObjectAnimator
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.*
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.request.RequestOptions
import com.liquid.base.adapter.CommonAdapter
import com.liquid.base.adapter.CommonViewHolder
import com.liquid.base.adapter.MultiItemCommonAdapter
import com.liquid.base.adapter.MultiItemTypeSupport
import com.liquid.base.utils.LogOut
import com.liquid.base.utils.ToastUtil
import com.liquid.library.retrofitx.utils.LogUtils
import com.liquid.poros.R
import com.liquid.poros.analytics.utils.MultiTracker
import com.liquid.poros.base.BaseActivity
import com.liquid.poros.constant.Constants
import com.liquid.poros.constant.TrackConstants
import com.liquid.poros.entity.SoloVideoInfo
import com.liquid.poros.entity.SolotVideoModel
import com.liquid.poros.ui.activity.live.VideoMatchChattingActivity
import com.liquid.poros.utils.ScreenUtil
import com.liquid.poros.utils.StringUtil
import com.liquid.poros.widgets.pulltorefresh.BaseRefreshListener
import com.liquid.poros.widgets.pulltorefresh.PullToRefreshLayout
import com.liquid.poros.widgets.pulltorefresh.ViewStatus
import kotlinx.android.synthetic.main.activity_video_match_chatting.*
import kotlinx.android.synthetic.main.layout_for_solo_video.*
import java.util.*

/**
 * 1v1视频列表页面
 */
class SoloVideoListActivity : BaseActivity(), BaseRefreshListener {

    private var clickPosition: Int = 0
    var recyclerView: RecyclerView? = null
    var commonAdapter: CommonAdapter<SoloVideoInfo>? = null
    var soloVideoModel: SolotVideoModel? = null
    var needExe : Boolean = false;
    var mHandler: Handler = Handler {
        when (it.what) {
            1 -> {
                LogOut.debug("wap", "1")
                if(needExe){
                    soloVideoModel?.getSoloCall()
                }
                sendMsg()
            }
        }
        false
    }

    var isFling: Boolean = false
    private var refresh_view: PullToRefreshLayout? = null

    var isshow = true
    var disy = 0
    var height: Int? = null
    var iv_left: ImageView? = null
    var record_list: MutableList<SoloVideoInfo>? = null

    private fun sendMsg() {
        mHandler.sendEmptyMessageDelayed(1, 60000)
    }

    override fun parseBundleExtras(extras: Bundle?) {
    }

    override fun getContentViewLayoutID(): Int {
        return R.layout.layout_for_solo_video
    }

    override fun initViewsAndEvents(savedInstanceState: Bundle?) {
        iv_left = findViewById(R.id.iv_left)
        iv_left?.setOnClickListener {
            finish()
        }
        recyclerView = findViewById(R.id.recyclerView)
        recyclerView?.layoutManager = LinearLayoutManager(this)
        commonAdapter = object : MultiItemCommonAdapter<SoloVideoInfo>(this@SoloVideoListActivity,
            object : MultiItemTypeSupport<SoloVideoInfo> {
                override fun getLayoutId(itemType: Int): Int {
                    return if (itemType == 1) {
                        R.layout.layout_for_solo_video_list_empty_item
                    } else R.layout.layout_for_solo_video_list_item
                }

                override fun getItemViewType(position: Int, t: SoloVideoInfo?): Int {
                    return t!!.view_type
                }
            }) {
            override fun convert(holder: CommonViewHolder?, soloinfo: SoloVideoInfo?, pos: Int) {
                val iv_cover: ImageView? = holder?.getView<ImageView>(R.id.iv_cover)
                val name: TextView? = holder?.getView<TextView>(R.id.name)
                val tv_dvf_age_gender: TextView? = holder?.getView<TextView>(R.id.tv_dvf_age_gender)
                val tv_busy_status: TextView? = holder?.getView<TextView>(R.id.tv_busy_status)
                val tv_free_status: TextView? = holder?.getView<TextView>(R.id.tv_free_status)
                val video_view: VideoView? = holder?.getView<VideoView>(R.id.video_view)
                val ll_with_she_video = holder?.getView<LinearLayout>(R.id.ll_with_she_video)
                val view_view_container = holder?.getView<FrameLayout>(R.id.view_view_container)

                val age: Int? = soloinfo?.age
                soloinfo?.nick_name.also { name?.text = it }
                tv_dvf_age_gender?.text = "$age"
                iv_cover?.let {
                    Glide.with(this@SoloVideoListActivity).applyDefaultRequestOptions(RequestOptions()
                            .placeholder(R.mipmap.solo_video_default_cover)
                            .error(R.mipmap.solo_video_default_cover)).load(soloinfo?.image_1v1_url).into(it)
                }
                LogOut.debug("test_isAvailable","" + soloinfo?.available)
                if(soloinfo?.available == true){
                    tv_free_status?.visibility = View.VISIBLE
                    tv_busy_status?.visibility = View.GONE
                }else{
                    tv_free_status?.visibility = View.GONE
                    tv_busy_status?.visibility = View.VISIBLE
                }

                if (StringUtil.isNotEmpty(soloinfo?.video_url)) {
                    video_view?.setVideoURI(Uri.parse(soloinfo?.video_url))
                    video_view?.requestFocus()
                }

                val onPreparedListener: MediaPlayer.OnPreparedListener =
                    MediaPlayer.OnPreparedListener {
                        it.start()
                        it.isLooping = true
                        it.setVolume(0f, 0f)
                        view_view_container?.visibility = View.VISIBLE
                    }
                video_view?.setOnPreparedListener(onPreparedListener);

                ll_with_she_video?.setOnClickListener() {
                    //跳转1v1视频页(定向)
                    soloVideoModel?.requestVideoMatchMic(soloinfo?.user_id,soloinfo?.nick_name,soloinfo?.avatar_url,soloinfo?.image_url)
                    clickPosition = pos
                    val params: MutableMap<String, String?> = HashMap()
                    params["guest_id"] = soloinfo?.user_id
                    MultiTracker.onEvent(TrackConstants.B_VIDEO_LIST_WITH_SHE_VIDEO_CLICK, params)
                }
            }
        }

        recyclerView?.adapter = commonAdapter
        recyclerView?.setHasFixedSize(true)
        recyclerView?.setItemViewCacheSize(10)
        recyclerView?.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val firstVisibleItem =
                        (recyclerView.layoutManager as LinearLayoutManager?)!!.findFirstVisibleItemPosition()
                if (firstVisibleItem == 0) {  //当前可见的item第一个是否是列表的第一个 如果是第一个应该显示
                    if (!isshow) {
                        isshow = true
                        showToolbar()
                    }
                } else { //不是第一个
                    if (disy > 25 && isshow) {    //滑动距离大于25切显示状态
                        isshow = false
                        hideToolbar()
                        disy = 0 //清0
                    }
                    if (disy < -25 && !isshow) {  //向上滑动且距离大于25且隐藏状态 就显示
                        isshow = true
                        showToolbar()
                        disy = 0 //请0
                    }
                }
                if (isshow && dy > 0 || !isshow && dy < 0) {  //增加滑动的距离， 只有再触发两种状态的时候才进行叠加
                    disy += dy
                }
            }

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                isFling = newState != RecyclerView.SCROLL_STATE_IDLE
            }
        })

        soloVideoModel = getActivityViewModel(SolotVideoModel::class.java)
        soloVideoModel?.getRoomList()
        soloVideoModel?.success?.observe(this, Observer {
            record_list = it.items
            record_list?.let{ list ->
                if (list.size <= 0) {
                    ToastUtil.show("暂无中奖数据")
                }
            }
            commonAdapter?.updateData(record_list);
            refresh_view?.finishRefresh()
            refresh_view?.finishLoadMore()
        })
        soloVideoModel?.fail?.observe(this, Observer { ToastUtil.show("数据获取异常，请稍后尝试") })

        soloVideoModel?.soloCallSucess?.observe(this, Observer {
            /**
             * 跳转到1v1视频连接--有数据则代表有女嘉宾返回，否则data的数据都为空
             */
            it.live_id?.let { _ ->
                LogUtils.d(it.live_id?.toString())
                var bundle = Bundle()
                bundle.putString(
                    Constants.ENTER_VIDEO_MATCH_SOURCE,
                    Constants.ENTER_VIDEO_MATCH_1V1
                )
                bundle.putSerializable(Constants.VIDEO_MATCH_DATA_PASS_KEY, it)
                readyGo(VideoMatchChattingActivity::class.java, bundle)
            }

        })
        soloVideoModel?.soloCallFail?.observe(this, Observer {

        })
        soloVideoModel?.requestVideoMatchMicLiveDataSuccess?.observe(this, Observer {
            var bundle = Bundle()
            bundle.putString(
                Constants.ENTER_VIDEO_MATCH_SOURCE,
                Constants.ENTER_VIDEO_MATCH_SINGLE_CLICK
            )
            bundle.putSerializable(Constants.VIDEO_MATCH_DATA_PASS_KEY, it)
            readyGo(VideoMatchChattingActivity::class.java, bundle)
        })
        soloVideoModel?.requestVideoMatchMicLiveDataFail?.observe(this, Observer {
            when (it.code) {
                //金币不足
                19001 -> {
                    var bundle = Bundle()
                    bundle.putString(
                        Constants.ENTER_VIDEO_MATCH_SOURCE,
                        Constants.ENTER_VIDEO_MATCH_SINGLE_CLICK
                    )
                    bundle.putSerializable(Constants.VIDEO_MATCH_DATA_PASS_KEY, it)
                    readyGo(VideoMatchChattingActivity::class.java, bundle)
                }
                19017 -> {
                    ToastUtil.show(it.msg)
                    updateItem()
                    val params: MutableMap<String, String?> = HashMap()
                    params["guest_id"] = it.live_info?.anchor_id
                    MultiTracker.onEvent(TrackConstants.B_VIDEO_LIST_BUSY_TOAST, params)
                }
                19018 -> {
                    ToastUtil.show(it.msg)
                    updateItem()
                }
                else -> {
                    ToastUtil.show(it.msg)
                    updateItem()
                }

            }
        })

        refresh_view = findViewById(R.id.refresh_view)
        refresh_view?.setRefreshListener(this)
        refresh_view?.setCanRefresh(true)
        refresh_view?.setCanLoadMore(false)
        mHandler.sendEmptyMessageDelayed(1, 5000)
    }
    override fun isShowMsgTip(): Boolean {
        return false
    }


    private fun updateItem() {
        val soloVideoInfo: SoloVideoInfo? = record_list?.get(clickPosition)
        soloVideoInfo?.let { 
             if(soloVideoInfo.available){
                 soloVideoInfo.available = false                                       
                 soloVideoInfo.let { it1 -> record_list?.set(clickPosition, it1) }     
                 commonAdapter?.notifyItemChanged(clickPosition)                       
             }                                                                         
        }
    }

    override fun getPageId(): String {
        return TrackConstants.P_SOLO_VIDEO_ACTIVITY
    }

    override fun onPause() {
        super.onPause()
        needExe = false
    }

    override fun onResume() {
        super.onResume()
        needExe = true
    }

    override fun onDestroy() {
        super.onDestroy()
        mHandler .removeCallbacksAndMessages(null)
    }

    override fun refresh() {
        soloVideoModel?.getRoomList()
    }

    override fun loadMore() {
    }

    override fun notifyNetworkChanged(isConnected: Boolean) {
        super.notifyNetworkChanged(isConnected)
        refresh_view?.showView(if (isConnected) ViewStatus.CONTENT_STATUS else ViewStatus.ERROR_STATUS)
        refresh_view?.setCanRefresh(isConnected)
    }

    /**
     * 隐藏toolbar
     * @param toolbar
     * @param toolHeight
     */
    private fun hideToolbar() {
        val animator = ObjectAnimator.ofFloat(
            toolbar,
            View.TRANSLATION_Y,
            0f,
            -ScreenUtil.dip2px(70f).toFloat()
        )
        animator.duration = 500
        animator.start()
    }

    /**
     * 展示toolbar
     * @param toolbar
     * @param toolHeight
     */
    fun showToolbar() {
        val animator = ObjectAnimator.ofFloat(
            toolbar,
            View.TRANSLATION_Y,
            -ScreenUtil.dip2px(70f).toFloat(),
            0f
        )
        animator.duration = 500
        animator.start()
    }

    override fun needSetTransparent(): Boolean {
        return false
    }
}
