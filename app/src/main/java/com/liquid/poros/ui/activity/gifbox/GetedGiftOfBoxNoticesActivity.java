package com.liquid.poros.ui.activity.gifbox;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.liquid.poros.R;
import com.liquid.poros.adapter.WinningPrizeNoticeAdapter;
import com.liquid.poros.base.BaseActivity;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.entity.GetedGiftOfBoxNoticeInfo;
import com.liquid.poros.viewmodel.GiftBoxVM;

import butterknife.BindView;

/**
 * 中奖通告页面（拆礼盒）：recordType 为null
 *
 * 助力抽奖也用此页面
 *
 */
public class GetedGiftOfBoxNoticesActivity extends BaseActivity  {
    @BindView(R.id.rv_notices)
    RecyclerView rv_notices;
    private GiftBoxVM giftBoxVM;

    public static void start(Context context,String recordType){
        Intent intent = new Intent(context,GetedGiftOfBoxNoticesActivity.class);
        intent.putExtra(GiftBoxVM.RECORD_TYPE,recordType);
        context.startActivity(intent);
    }

    @Override
    protected void parseBundleExtras(Bundle extras) {

    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.activity_geted_giftofbox_notices;
    }

    @Override
    protected String getPageId() {
        return TrackConstants.P_GETED_GIFTOFBOX_NOTICES;
    }

    @Override
    protected void initViewsAndEvents(Bundle savedInstanceState) {
        rv_notices.setLayoutManager(new LinearLayoutManager(this));
        giftBoxVM = getActivityViewModel(GiftBoxVM.class);
        giftBoxVM.getGetedGiftOfBoxNoticeInfoData().observe(this, new Observer<GetedGiftOfBoxNoticeInfo>() {
            @Override
            public void onChanged(GetedGiftOfBoxNoticeInfo getedGiftOfBoxNoticeInfo) {
                if (isDestroyed() || null == getedGiftOfBoxNoticeInfo.record_list) {
                    return;
                }

                rv_notices.setAdapter(new WinningPrizeNoticeAdapter(GetedGiftOfBoxNoticesActivity.this, getedGiftOfBoxNoticeInfo.record_list));
            }
        });

        giftBoxVM.getNotices(getIntent().getStringExtra(GiftBoxVM.RECORD_TYPE));
    }
}
