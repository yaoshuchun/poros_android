package com.liquid.poros.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.alipay.sdk.app.PayTask;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.liquid.base.event.EventCenter;
import com.liquid.poros.R;
import com.liquid.poros.adapter.RechargeCoinAmountAdapter;
import com.liquid.poros.alipay.PayResult;
import com.liquid.poros.analytics.utils.MultiTracker;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.contract.account.RechargeContract;
import com.liquid.poros.contract.account.RechargePresenter;
import com.liquid.poros.entity.AlipayInfo;
import com.liquid.poros.entity.RechargeInfo;
import com.liquid.poros.entity.RechargeListInfo;
import com.liquid.poros.entity.RechargeResultInfo;
import com.liquid.poros.entity.UserCoinInfo;
import com.liquid.poros.entity.WeChatPayInfo;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.poros.utils.StringUtil;
import com.liquid.poros.widgets.DrawableCenterTextView;
import com.liquid.poros.wxapi.WXUtils;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.greenrobot.event.EventBus;
import de.greenrobot.event.Subscribe;
import de.greenrobot.event.ThreadMode;

public class RechargeCoinDialog extends BottomSheetDialogFragment implements View.OnClickListener, RechargeContract.View{

    //金币不足时礼物发送触发
    public static final String POSITION_SEND_GIFT = "send_gift";
    //礼物弹窗充值入口触发
    public static final String POSITION_GIFT_ALERT = "gift_alert";
    //金币不足-发送文字消息时灰色提示触发
    public static final String POSITION_SEND_MESSAGE = "send_message";
    //装扮弹窗金币不足送装扮触发
    public static final String POSITION_DRESS_SEND = "dress_send";
    //装扮弹窗充值入口触发
    public static final String POSITION_DRESS_ALERT = "dress_alert";
    //嘉宾所要礼物-用户立即赠送触发
    public static final String POSITION_GUEST_ASK_FOR_DRESS = "guest_ask_for_dress";
    //开黑卡弹窗购买并使用按钮触发
    public static final String POSITION_BUY_AND_USE_TEAM_CARD = "buy_and_use_team_card";
    //开黑卡弹窗充值入口触发
    public static final String POSITION_TEAM_JOIN_CARD_ALERT = "team_join_card_alert";
    //点击未续卡成功消息通知 下拉框
    public static final String POSITION_REMIND_CARD_APP = "remind_card_app";
    //点击未续卡成功消息通知 通知栏
    public static final String POSITION_REMIND_CARD_NOTICE = "remind_card_notice";

    private final int SELECT_WE_CHAT_PAY = 0;   //微信支付
    private final int SELECT_ALI_PAY = 1;       //支付宝支付
    private static final int SDK_PAY_FLAG = 1;
    private Context context;
    private TextView coinCount;
    private ImageView closeRecharge;
    private RecyclerView rvRechargeList;
    private DrawableCenterTextView wechatPay;
    private DrawableCenterTextView aliPay;
    private LinearLayout llWechatPay;
    private LinearLayout llAliPay;
    private ImageView ivWechatPay;
    private ImageView ivAliPay;
    private TextView payQuick;
    private RechargePresenter mPresent;
    private RechargeCoinAmountAdapter adapter;
    private int payOption;
    private long cashAmount;
    private String rechargeType;
    private String mOrderId;
    private String windowFrom;
    private OnCoinUpdateListener listener;
    private String roomId;
    private String positon;
    private boolean match_room= false;
    private boolean video_match_room= false;

    public void setPositon(String positon) {
        this.positon = positon;
    }

    public void setMatch_room(boolean match_room) {
        this.match_room = match_room;
    }
    public void setVideoMatchRoom(boolean video_match_room) {
        this.video_match_room = video_match_room;
    }
    private Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case SDK_PAY_FLAG:
                    if (TextUtils.isEmpty(mOrderId)) {
                        return;
                    }
                    PayResult payResult = new PayResult((Map<String, String>) msg.obj);
                    String resultStatus = payResult.getResultStatus();
                    if (!StringUtil.isEquals(resultStatus, "9000")) {
                        //订单取消/失败
                        mPresent.closeRechargeOrder(mOrderId);
                    }
                    mPresent.rechargeResult(mOrderId);
                    break;
            }
        }
    };

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresent = new RechargePresenter();
        mPresent.attachView(this);
        mPresent.setMatch_room(match_room);
        mPresent.setVideo_match_room(video_match_room);
        EventBus.getDefault().register(this);
    }

    @Subscribe(threadMode = ThreadMode.MainThread)
    public void onEventBus(EventCenter eventCenter) {
        if (null != eventCenter) {
            if (eventCenter.getEventCode() == Constants.EVENT_CODE_PAY_RECHARGE) {
                int baseRespCode = (int) eventCenter.getData();
                if (baseRespCode != 0) {//订单取消
                    if (!TextUtils.isEmpty(mOrderId)) {
                        mPresent.closeRechargeOrder(mOrderId);
                    }
                }
                if (!TextUtils.isEmpty(mOrderId)) {
                    mPresent.rechargeResult(mOrderId);
                }
            }
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
        View view = View.inflate(getContext(), R.layout.view_recharge_coin, null);
        dialog.setContentView(view);
        //设置透明背景
        dialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(R.drawable.bg_recharge_panel);
        View root = dialog.getDelegate().findViewById(R.id.design_bottom_sheet);
        BottomSheetBehavior behavior = BottomSheetBehavior.from(root);
        behavior.setHideable(false);
        initializeView(view);
        return dialog;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public void setWindowFrom(String windowFrom) {
        this.windowFrom = windowFrom;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void initializeView(View view) {
        coinCount = view.findViewById(R.id.video_room_coin_count);
        closeRecharge = view.findViewById(R.id.video_room_recharge_close);
        rvRechargeList = view.findViewById(R.id.rv_recharge_amount_list);
        wechatPay = view.findViewById(R.id.we_chat_pay);
        aliPay = view.findViewById(R.id.ali_pay);
        llAliPay = view.findViewById(R.id.ll_ali_pay);
        llWechatPay = view.findViewById(R.id.ll_we_chat_pay);
        ivAliPay = view.findViewById(R.id.iv_ali_pay);
        ivWechatPay = view.findViewById(R.id.iv_we_chat_pay);
        payQuick = view.findViewById(R.id.pay_quick_for_coin);
        ivWechatPay.setImageResource(R.mipmap.icon_wechat_select);
        llWechatPay.setBackgroundResource(R.drawable.bg_ali_pay);
        wechatPay.setTextColor(context.getResources().getColor(R.color.color_ffffff));
        aliPay.setTextColor(context.getResources().getColor(R.color.color_181e25));
        llAliPay.setBackgroundResource(R.drawable.bg_wechat_pay);
        ivAliPay.setImageResource(R.mipmap.icon_alipay_unselect);
        adapter = new RechargeCoinAmountAdapter(context);
        rvRechargeList.setLayoutManager(new GridLayoutManager(context,2));
        rvRechargeList.setAdapter(adapter);
        closeRecharge.setOnClickListener(this);
        llAliPay.setOnClickListener(this);
        llWechatPay.setOnClickListener(this);
        payQuick.setOnClickListener(this);
        adapter.setListener(new RechargeCoinAmountAdapter.OnCoinNumSelectListener() {
            @Override
            public void coinNumSelect(long num, long cash, String type) {
                cashAmount = cash;
                rechargeType = type;
                if (payQuick != null) {
                    payQuick.setText("立即支付  (" + (cash / 100) + "元)");
                }
            }
        });
        mPresent.rechargeInfo(roomId);
    }

    public void setListener(OnCoinUpdateListener listener) {
        this.listener = listener;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.video_room_recharge_close:
                dismiss();
                break;
            case R.id.ll_ali_pay:
                payOption = SELECT_ALI_PAY;
                ivWechatPay.setImageResource(R.mipmap.icon_wechat_unselect);
                llWechatPay.setBackgroundResource(R.drawable.bg_wechat_pay);
                wechatPay.setTextColor(context.getResources().getColor(R.color.color_181e25));
                aliPay.setTextColor(context.getResources().getColor(R.color.color_ffffff));
                llAliPay.setBackgroundResource(R.drawable.bg_ali_pay);
                ivAliPay.setImageResource(R.mipmap.icon_alipay_select);
                break;
            case R.id.ll_we_chat_pay:
                ivWechatPay.setImageResource(R.mipmap.icon_wechat_select);
                llWechatPay.setBackgroundResource(R.drawable.bg_ali_pay);
                wechatPay.setTextColor(context.getResources().getColor(R.color.color_ffffff));
                aliPay.setTextColor(context.getResources().getColor(R.color.color_181e25));
                llAliPay.setBackgroundResource(R.drawable.bg_wechat_pay);
                ivAliPay.setImageResource(R.mipmap.icon_alipay_unselect);
                payOption = SELECT_WE_CHAT_PAY;
                break;
            case R.id.pay_quick_for_coin:
                if (payOption == SELECT_WE_CHAT_PAY) {
                    mPresent.wechatOrder(new BigDecimal(cashAmount).intValue(),rechargeType);
                }else {
                    mPresent.alipayOrder(new BigDecimal(cashAmount).intValue(),rechargeType);
                }
                HashMap<String, String> params = new HashMap<>();
                params.put("user_id", AccountUtil.getInstance().getUserId());
                params.put("recharge_window_from",windowFrom);
                params.put("recharge_amount",String.valueOf(cashAmount));
                MultiTracker.onEvent("b_recharge_button_click",params);
                break;
        }
    }

    @Override
    public void onRechargeInfoFetch(RechargeInfo rechargeInfo) {
        if (rechargeInfo != null) {
            RechargeInfo.Data data = rechargeInfo.getData();
            if (data != null) {
                if(coinCount != null) {
                    coinCount.setText(data.getRest_coin());
                }
                List<RechargeListInfo> rechargeList = data.getRecharge_list();
                if (rechargeList != null && rechargeList.size() != 0) {
                    cashAmount = rechargeList.get(0).getMoney();
                    rechargeType = rechargeList.get(0).getType();
                    adapter.setDatas(data.getRecharge_list());
                    payQuick.setText("立即支付  (" + (cashAmount / 100) + "元)");
                }
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        uploadBehavior(TrackConstants.B_RECHARGE_WINDOW_EXPOSURE);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onWechatSuccessFetch(WeChatPayInfo weChatPayInfo) {
        if (weChatPayInfo == null || weChatPayInfo.getData() == null || weChatPayInfo.getData().getRecharge_data() == null) {
            return;
        }
        WeChatPayInfo.Data.RechargeData rechargeData = weChatPayInfo.getData().getRecharge_data();
        mOrderId = weChatPayInfo.getData().getOrder_id();
        WXUtils.getInstance(context)
                .recharge(rechargeData.getAppid()
                        , rechargeData.getPartnerid()
                        , rechargeData.getPrepayid()
                        , rechargeData.getNoncestr()
                        , rechargeData.getTimestamp()
                        , rechargeData.getPackageValue()
                        , rechargeData.getSign());
    }

    @Override
    public void onAlipaySuccessFetch(AlipayInfo alipayInfo) {
        if (alipayInfo == null || alipayInfo.getData() == null || alipayInfo.getData().getRecharge_data() == null) {
            return;
        }
        mOrderId = alipayInfo.getData().getOrder_id();
        String order_string = alipayInfo.getData().getRecharge_data().getOrder_string();
        Runnable payRunnable = new Runnable() {
            @Override
            public void run() {
                if (getActivity() != null){
                    PayTask alipay = new PayTask(getActivity());
                    Map<String, String> result = alipay.payV2(order_string, true);
                    Message msg = new Message();
                    msg.what = SDK_PAY_FLAG;
                    msg.obj = result;
                    mHandler.sendMessage(msg);
                }
            }
        };
        // 必须异步调用
        Thread payThread = new Thread(payRunnable);
        payThread.start();
    }

    @Override
    public void onRechargeResultFetch(RechargeResultInfo rechargeResultInfo) {
        if (rechargeResultInfo == null) return;

        if (rechargeResultInfo.getCode() == 0) {
            if (listener != null && rechargeResultInfo.getData() != null) {
                listener.update(rechargeResultInfo.getData().getCoin());
            }
            EventBus.getDefault().post(new EventCenter(Constants.EVENT_CODE_COIN_CHANGE,(rechargeResultInfo.getData() != null)? rechargeResultInfo.getData().getCoin():"0"));
            try {
//            dismiss();
                dismissAllowingStateLoss();//fix,#754956 java.lang.IllegalStateException Can not perform this action after onSaveInstanceState
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        uploadBehavior(rechargeResultInfo.getCode() == 0 ? TrackConstants.B_RECHARGE_WINDOW_SUCCESS:TrackConstants.B_RECHARGE_WINDOW_FAIL);
    }

    private void uploadBehavior(String event){
        HashMap<String, String> params = new HashMap<>();
        params.put("recharge_window_from",windowFrom);
        params.put("recharge_amount",String.valueOf(cashAmount));
        params.put("position",positon);
        MultiTracker.onEvent(event,params);
    }

    @Override
    public void onUserCoinSuccessFetch(UserCoinInfo userCoinInfo) {

    }

    @Override
    public void onCloseRechargeOrderFetch() {

    }

    @Override
    public void showLoading() {

    }

    @Override
    public void showSuccess() {

    }

    @Override
    public void closeLoading() {

    }

    @Override
    public void onError() {

    }

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void onSuccess(Object data) {

    }

    public interface OnCoinUpdateListener{
        void update(String coin);
    }
}
