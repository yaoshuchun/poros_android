package com.liquid.poros.ui.dialog

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import butterknife.ButterKnife
import com.liquid.library.retrofitx.RetrofitX
import com.liquid.poros.R
import com.liquid.poros.analytics.utils.MultiTracker
import com.liquid.poros.base.BaseDialogFragment
import com.liquid.poros.constant.Constants
import com.liquid.poros.constant.TrackConstants
import com.liquid.poros.entity.BaseBean
import com.liquid.poros.http.ApiUrl
import com.liquid.poros.http.observer.ObjectObserver
import com.liquid.poros.utils.AccountUtil
import com.liquid.poros.utils.ScreenUtil
import com.liquid.poros.utils.StringUtil
import java.util.*

/**
 * 获得钥匙的提示研创
 * si: 每次登陆 接口请求 是否展示弹窗
 * s2: 进入私聊页面，获得针对与女嘉宾的钥匙数量
 */
@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class GotKeyDialog : BaseDialogFragment() {
    private var mFrom = ""
    var key_num: TextView? = null
    var desc_tv: TextView? = null
    var confirm_btn: TextView? = null
    var title_iv: ImageView? = null

    var mKeyNum = 0
    var mDesc = 0
    var mTargetUserId : String? = null
    fun setFrom(from: String): GotKeyDialog {
        mFrom = from
        return this
    }

    fun setParams(keyNum: Int, desc: Int): GotKeyDialog {
        mKeyNum = keyNum
        mDesc = desc
        return this
    }

    override fun getPageId(): String {
        return TrackConstants.P_GOT_KEY_DIALOG
    }

    fun setTargetId(target_user_id: String?) : GotKeyDialog {
        this.mTargetUserId = target_user_id
        return this
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dialog!!.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onStart() {
        super.onStart()
        if (dialog != null) {
            val win = dialog!!.window
            // 一定要设置Background，如果不设置，window属性设置无效
            if (win != null) {
                win.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                val dm = DisplayMetrics()
                if (activity != null) {
                    requireActivity().windowManager.defaultDisplay.getMetrics(dm)
                    val params = win.attributes
                    params.gravity = Gravity.CENTER
                    params.width = ScreenUtil.dip2px(280f)
                    params.width = ScreenUtil.dip2px(300f)
                    isCancelable = false
                    // 使用ViewGroup.LayoutParams，以便Dialog 宽度充满整个屏幕
                    win.attributes = params
                }
            }
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        if (activity != null) {
            val builder = AlertDialog.Builder(requireActivity())
            val view = LayoutInflater.from(activity).inflate(R.layout.fragment_got_key, null)
            key_num = view.findViewById(R.id.key_num_tv)
            desc_tv = view.findViewById(R.id.desc_everyday)
            confirm_btn = view.findViewById(R.id.confirm_tv)
            title_iv = view.findViewById(R.id.title_iv)

            key_num?.text = ("$mKeyNum").toString()
            if(StringUtil.isEquals(MAIN_PAGE, mFrom)){
                desc_tv?.text = ("明天登录可得钥匙*${mDesc}").toString()
            }else{
                desc_tv?.text = ("每日登录获得礼盒钥匙 +${mDesc}").toString()
            }
            confirm_btn?.setOnClickListener(View.OnClickListener { dismissAllowingStateLoss() })
            when {
                StringUtil.isEquals(mFrom, P2P_PAGE) -> {
                    title_iv?.setImageResource(R.mipmap.icon_p2p_key_title)
                }
                StringUtil.isEquals(mFrom, MAIN_PAGE) -> {
                    title_iv?.setImageResource(R.mipmap.login_key_icon)
                }
                else -> {
                    title_iv?.setImageResource(R.mipmap.icon_p2p_key_title)
                }
            }
            builder.setView(view)
            ButterKnife.bind(this, view)
            isCancelable = true
            val dialog: Dialog = builder.create()
            dialog.setCancelable(true)
            dialog.setCanceledOnTouchOutside(true)
            if (StringUtil.isEquals(P2P_PAGE, mFrom)) {
                updateDialogStatus()
            }
            if(StringUtil.isEquals(P2P_PAGE,mFrom)){
                val params = HashMap<String?, String?>()
                params["user_id"] = AccountUtil.getInstance().userId
                params["guest_id"] = mTargetUserId
                params["key_number"] = "$mKeyNum"
                MultiTracker.onEvent(TrackConstants.B_ACTIVATION_CP_GET_KEY_ALERT_EXPOSURE, params)
            }else{
                val params = HashMap<String?, String?>()
                params["user_id"] = AccountUtil.getInstance().userId
                params["guest_id"] = mTargetUserId
                params["key_number"] = "$mKeyNum"
                MultiTracker.onEvent(TrackConstants.B_DAY_LEVEL_KEY_ALERT_EXPOSURE, params)
            }
            return dialog
        }
        return super.onCreateDialog(savedInstanceState)
    }

    private fun updateDialogStatus() {
        RetrofitX.url(ApiUrl.P2P_SHOW_KEY_DIALOG).
                param("token", AccountUtil.getInstance().account_token).
                param("target_user_id", mTargetUserId).
                postJSON().
                subscribe(object : ObjectObserver<BaseBean>() {
                    override fun failed(result: BaseBean) {}
                    override fun success(result: BaseBean) {}
                })
    }

    companion object {
        const val MAIN_PAGE = "main_page"
        const val P2P_PAGE = "p2p_page"
        @JvmStatic
        fun newInstance(groupId: String?): GotKeyDialog {
            val fragment = GotKeyDialog()
            val bundle = Bundle()
            bundle.putString(Constants.GROUP_ID, groupId)
            fragment.arguments = bundle
            return fragment
        }
    }
}