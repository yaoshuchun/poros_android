package com.liquid.poros.ui.fragment.dialog.account;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.liquid.base.adapter.CommonAdapter;
import com.liquid.base.adapter.CommonViewHolder;
import com.liquid.poros.R;
import com.liquid.poros.analytics.utils.MultiTracker;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.entity.RechargeWayInfo;
import com.liquid.poros.utils.ScreenUtil;
import com.liquid.poros.utils.StringUtil;
import com.liquid.poros.utils.ViewUtils;
import com.liquid.poros.widgets.WrapContentLinearLayoutManager;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

/**
 * 充值选择支付方式弹窗
 */
public class PayWaysChooseDialogFragment extends BottomSheetDialogFragment implements View.OnClickListener {
    private TextView tv_pay_money;
    private TextView tv_pay_coin;
    private RecyclerView rv_recharge_way;
    private TextView tv_recharge_pay;
    private List<RechargeWayInfo> mRechargeWaysList;
    private String mOrderType;
    private String mCoin;
    private String mMoney;
    private long mPayMoney;
    private String mRechargeWay = "wechat";
    private ArrayList<Integer> mIsSelectList = new ArrayList<>();
    private CommonAdapter<RechargeWayInfo> mCommonAdapter;

    public static PayWaysChooseDialogFragment newInstance() {
        PayWaysChooseDialogFragment fragment = new PayWaysChooseDialogFragment();
        return fragment;
    }

    public PayWaysChooseDialogFragment setRechargeWays(String orderType, String coin, String money, long payMoney, List<RechargeWayInfo> rechargeWaysList) {
        this.mOrderType = orderType;
        this.mCoin = coin;
        this.mMoney = money;
        this.mPayMoney = payMoney;
        this.mRechargeWaysList = rechargeWaysList;
        return this;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
        View view = View.inflate(getContext(), R.layout.fragment_pay_ways_choose_dialog, null);
        dialog.setContentView(view);
        //设置透明背景
        dialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);
        View root = dialog.getDelegate().findViewById(R.id.design_bottom_sheet);
        BottomSheetBehavior behavior = BottomSheetBehavior.from(root);
        behavior.setHideable(false);
        initializeView(view);
        return dialog;
    }

    @SuppressLint("ResourceType")
    private void initializeView(View view) {
        tv_pay_money = view.findViewById(R.id.tv_pay_money);
        tv_pay_coin = view.findViewById(R.id.tv_pay_coin);
        rv_recharge_way = view.findViewById(R.id.rv_recharge_way);
        tv_recharge_pay = view.findViewById(R.id.tv_recharge_pay);
        tv_pay_money.setText(StringUtil.matcherSearchText(true, Color.parseColor("#10D8C8"), mMoney, "￥"));
        tv_pay_coin.setText("订单详情:购买" + mCoin + "金币");
        rv_recharge_way.setLayoutManager(new WrapContentLinearLayoutManager(getActivity()));
        mIsSelectList.add(0);
        mCommonAdapter = new CommonAdapter<RechargeWayInfo>(getActivity(), R.layout.item_recharge_way) {
            @Override
            public void convert(CommonViewHolder holder, RechargeWayInfo rechargeWayInfo, int pos) {
                holder.setText(R.id.tv_recharge_way_title, rechargeWayInfo.getTitle());
                Glide.with(mContext)
                        .load(rechargeWayInfo.getIcon())
                        .override(ScreenUtil.dip2px(24), ScreenUtil.dip2px(24))
                        .into((ImageView) holder.getView(R.id.iv_recharge_way));
                if (mIsSelectList.contains(pos)) {
                    holder.getView(R.id.iv_recharge_way_check).setBackgroundResource(R.mipmap.icon_recharge_way_select);
                } else {
                    holder.getView(R.id.iv_recharge_way_check).setBackgroundResource(R.mipmap.icon_recharge_way_normal);
                }
                holder.getView(R.id.rl_recharge_way).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!mIsSelectList.contains(pos)) {
                            mIsSelectList.clear();
                            mIsSelectList.add(pos);
                        }
                        mRechargeWay = mRechargeWaysList.get(mIsSelectList.get(0)).getType();
                        notifyDataSetChanged();
                    }
                });
            }
        };
        mCommonAdapter.update(mRechargeWaysList);
        rv_recharge_way.setAdapter(mCommonAdapter);
        tv_recharge_pay.setOnClickListener(this);
    }


    private PayWaysChooseListener listener;

    public PayWaysChooseDialogFragment setRechargeWaysListener(PayWaysChooseListener listener) {
        this.listener = listener;
        return this;
    }

    @Override
    public void onClick(View v) {
        if (ViewUtils.isFastClick()) {
            return;
        }
        HashMap params = new HashMap();
        if (listener != null) {
            if ("wechat".equals(mRechargeWay)) {
                params.put("pay_type", "wechat");
                listener.wechatOrder(new BigDecimal(mPayMoney).intValue(), mOrderType);
            } else if ("alipay".equals(mRechargeWay)) {
                params.put("pay_type", "alipay");
                listener.aliPayOrder(new BigDecimal(mPayMoney).intValue(), mOrderType);
            }
        }
        params.put("pay_amount", mMoney);
        MultiTracker.onEvent(TrackConstants.B_PAYMENT, params);
        dismissAllowingStateLoss();
    }

    public interface PayWaysChooseListener {
        void wechatOrder(int payMoney, String orderType);

        void aliPayOrder(int payMoney, String orderType);
    }
}
