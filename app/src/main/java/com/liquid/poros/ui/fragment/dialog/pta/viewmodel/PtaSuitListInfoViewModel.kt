package com.liquid.poros.ui.fragment.dialog.pta.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.liquid.library.retrofitx.RetrofitX
import com.liquid.poros.entity.PtaSuitListInfo
import com.liquid.poros.http.ApiUrl
import com.liquid.poros.http.observer.ObjectObserver
import com.liquid.poros.http.utils.getPoros
import com.liquid.poros.http.utils.postPoros
import com.liquid.poros.utils.AccountUtil

class PtaSuitListInfoViewModel : ViewModel() {
    val ptaSuitListLiveData : MutableLiveData<PtaSuitListInfo> by lazy{ MutableLiveData<PtaSuitListInfo>() }
    val failed : MutableLiveData<PtaSuitListInfo> by lazy{ MutableLiveData<PtaSuitListInfo>() }

    /**
     * 获去匹配信息
     */
    fun getPtaSuitList(){
        RetrofitX.url(ApiUrl.PTA_SUIT_LIST)
                .getPoros()
                .subscribe(object : ObjectObserver<PtaSuitListInfo>() {
                    override fun success(result: PtaSuitListInfo) {
                        ptaSuitListLiveData.postValue(result)
                    }

                    override fun failed(result: PtaSuitListInfo) {
                        failed.postValue(result)
                    }

                })
    }
}