package com.liquid.poros.ui.fragment

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.style.AbsoluteSizeSpan
import android.view.Gravity
import android.view.View
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import com.bumptech.glide.Glide
import com.liquid.base.adapter.CommonAdapter
import com.liquid.base.adapter.CommonViewHolder
import com.liquid.base.event.EventCenter
import com.liquid.base.utils.ContextUtils
import com.liquid.im.kit.emoji.ImageSpanAlignCenter
import com.liquid.poros.R
import com.liquid.poros.analytics.utils.MultiTracker
import com.liquid.poros.base.BaseFragment
import com.liquid.poros.constant.Constants
import com.liquid.poros.constant.TrackConstants
import com.liquid.poros.entity.Bag
import com.liquid.poros.entity.BagBean
import com.liquid.poros.entity.MicUserInfo
import com.liquid.poros.entity.MsgUserModel.GiftNums
import com.liquid.poros.ui.activity.account.WithdrawalActivity
import com.liquid.poros.ui.activity.user.SessionActivity
import com.liquid.poros.ui.dialog.LiveMultiGiftSendDialog
import com.liquid.poros.ui.dialog.MultiGiftSendDialog
import com.liquid.poros.ui.fragment.viewmodel.BagViewModel
import com.liquid.poros.utils.NumberFormatUtil
import com.liquid.poros.utils.ViewUtils
import com.liquid.poros.widgets.VerticalCenterSpan
import com.netease.neliveplayer.playerkit.common.net.NetworkUtil
import kotlinx.android.synthetic.main.fragment_bag.*
import kotlinx.android.synthetic.main.fragment_bag.view.*
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.indices
import kotlin.collections.set

/**
 * 背包fragment
 */
class BagFragment : BaseFragment() {

    companion object{
        fun newInstance(arguments: Bundle?): BagFragment? {
            val fragment = BagFragment()
            fragment.arguments = arguments
            return fragment
        }
        const val TYPE_UNKNOWN = 0
        const val TYPE_CASH = 1            //现金
        const val TYPE_CANDY = 2           //糖果
        const val TYPE_GIFT_FRAGMENTS = 3  //礼物碎片
        const val TYPE_NORMAL_GIFT = 4     //普通礼物
        const val TYPE_EXCLUSIVE_GIFT = 5  //专属礼物
    }

    private val bagViewModel:BagViewModel by lazy { getFragmentViewModel(BagViewModel::class.java) }
    private var micUserInfos:ArrayList<MicUserInfo>? = null
    private val viewPageData = ArrayList<Boolean>()
    private val bagData = ArrayList<BagBean>()
    private val indicators = ArrayList<Boolean>()
    private lateinit var viewPagerAdapter: CommonAdapter<Boolean>
    private lateinit var indicatorAdapter: CommonAdapter<Boolean>
    var count = 0
    private val countPerPage = 6
    private var selectPos = 0
    private var giftNum = 1
    private var targetUserId = ""
    private var currentPage = 0
    private var mIsContentGpVisibly = false
    private var mRoomId = ""
    private var audienceFrom:String? = null
    private var mListener:OnBagSelectListener? = null;
    override fun getPageId(): String = TrackConstants.PAGE_BAG_FRAGMENT
    override fun getLoadingTargetView(): View? = null

    override fun notifyNetworkChanged(isConnected: Boolean) {
        if (isConnected){
            bagViewModel.getBagData(mRoomId)
        }else{
            ll_error_container.visibility = View.VISIBLE
        }
    }

    override fun notifyByThemeChanged() {}

    override fun onUserVisible() {}

    override fun onUserInvisible() {}

    override fun getContentViewLayoutID(): Int = R.layout.fragment_bag

    override fun onFirstUserVisible() {
        if (NetworkUtil.isNetAvailable(context)){
            bagViewModel.getBagData(mRoomId)
        }else{
            ll_error_container.visibility = View.VISIBLE
        }
    }

    fun setRoomId(roomId:String) {
        mRoomId = roomId
    }

    fun setAudienceFrom(audienceFrom:String?) {
        this.audienceFrom = audienceFrom
    }

    override fun initViewsAndEvents(savedInstanceState: Bundle?) {

        val giftNums = requireArguments()[MultiGiftSendDialog.GIFT_NUMS] as ArrayList<GiftNums>

        targetUserId = requireArguments().getString(Constants.FEMALE_GUEST_ID,"")

        left_tv.setOnClickListener {
            if (ViewUtils.isFastClick()){
                return@setOnClickListener
            }
            if (bagData[selectPos].type == TYPE_NORMAL_GIFT || bagData[selectPos].type == TYPE_EXCLUSIVE_GIFT) {
                num_gp.visibility = if (num_gp.visibility == View.VISIBLE) View.GONE else View.VISIBLE
            }
        }

        num_bg.setOnClickListener {
            num_gp.visibility = View.GONE
        }

        right_tv.setOnClickListener {
            if (ViewUtils.isFastClick()){
                return@setOnClickListener
            }
            when (bagData[selectPos].type) {
                TYPE_NORMAL_GIFT,TYPE_EXCLUSIVE_GIFT ->{
                    bagViewModel.sendBagGift(bagData[selectPos]
                            ,targetUserId
                            ,giftNum,"")
                    trackGift()
                }
                TYPE_GIFT_FRAGMENTS -> {
                    bagViewModel.sendBagGift(bagData[selectPos]
                            ,targetUserId,1,"")
                   trackGiftFragments()
                }
                TYPE_CANDY,TYPE_CASH -> {
                    startActivity(Intent(activity, WithdrawalActivity::class.java).apply {
                        putExtra(Constants.START_WITHDRAWAL_TYPE, bagData[selectPos].type == TYPE_CASH)
                    })
                  trackCandyToCash()
                }
            }
        }
        if(parentFragment is LiveMultiGiftSendDialog) {
            (parentFragment as LiveMultiGiftSendDialog)?.giftRightLive.setOnClickListener {
                if (ViewUtils.isFastClick() || bagData.isEmpty()){
                    return@setOnClickListener
                }
                when (bagData[selectPos].type) {
                    TYPE_NORMAL_GIFT,TYPE_EXCLUSIVE_GIFT ->{
                        bagViewModel.sendBagGift(bagData[selectPos]
                                ,targetUserId
                                ,giftNum,mRoomId)
                        trackLiveRoomGift()
                    }
                    TYPE_GIFT_FRAGMENTS -> {
                        bagViewModel.sendBagGift(bagData[selectPos]
                                ,targetUserId,1,mRoomId)
                        trackLiveGiftFragments()
                    }
                    TYPE_CANDY,TYPE_CASH -> {
                        startActivity(Intent(activity, WithdrawalActivity::class.java).apply {
                            putExtra(Constants.START_WITHDRAWAL_TYPE, bagData[selectPos].type == TYPE_CASH)
                        })
                        trackLiveCandyToCash()
                    }
                }
            }
        }


        num_view.setListener {
            giftNum = it
            left_tv.text = getSpan()
        }

        num_view.setData(giftNums)

        observerData()
    }

    //视频房背包提现
    private fun trackLiveCandyToCash() {
        val params = HashMap<String?, String?>()
        params["anchor_id"] = micUserInfos?.get(0)?.user_id
        MultiTracker.onEvent(TrackConstants.B_BAG_PAGE_CASH_CLICK, params)
    }

    //视频房背包碎片合成
    private fun trackLiveGiftFragments() {
        val params = HashMap<String?, String?>()
        params["anchor_id"] = micUserInfos?.get(0)?.user_id
        params["compose_type"] = bagData[selectPos].type.toString()
        MultiTracker.onEvent(TrackConstants.B_BAG_PAGE_GIFT_COMPOSE_CLICK, params)
    }

    //视频房背包碎片合成成功
    private fun trackLiveGiftFragmentsSuccess() {
        val params = HashMap<String?, String?>()
        params["anchor_id"] = micUserInfos?.get(0)?.user_id
        params["compose_type"] = bagData[selectPos].type.toString()
        MultiTracker.onEvent(TrackConstants.B_BAG_PAGE_GIFT_COMPOSE_SUCCESS_EXPOSURE, params)
    }

    fun setMicUserInfos(micUserInfos:ArrayList<MicUserInfo>) {
        this.micUserInfos = micUserInfos
    }

    //视频房背包礼物点击赠送
    private fun trackLiveRoomGift() {
        val params = HashMap<String?, String?>()
        params["anchor_id"] = micUserInfos?.get(0)?.user_id
        params["guest_id"] = micUserInfos?.get(2)?.user_id
        params["gift_recipient"] = targetUserId
        params["audience_from"] = audienceFrom
        params["deliver_gift_name"] = bagData[selectPos].name
        params["deliver_gift_id"] = bagData[selectPos].record_id.toString()
        params["deliver_gift_num"] = bagData[selectPos].num.toString()
        MultiTracker.onEvent(TrackConstants.B_BAG_PAGE_GIVE_CLICK, params)
    }

    //视频房背包礼物点击赠送成功
    private fun trackLiveRoomGiftSuccess() {
        val params = HashMap<String?, String?>()
        params["anchor_id"] = micUserInfos?.get(0)?.user_id
        params["guest_id"] = micUserInfos?.get(2)?.user_id
        params["gift_recipient"] = targetUserId
        params["audience_from"] = audienceFrom
        params["deliver_gift_name"] = bagData[selectPos].name
        params["deliver_gift_id"] = bagData[selectPos].record_id.toString()
        params["deliver_gift_num"] = bagData[selectPos].num.toString()
        MultiTracker.onEvent(TrackConstants.B_BAG_PAGE_GIVE_SUCCESS_EXPOSURE, params)
    }

    fun setListener(listener:OnBagSelectListener) {
        mListener = listener
    }

    interface OnBagSelectListener{
        fun giftTypeSelect(type : Int)
        fun bagIsEmpty(isEmpty:Boolean)
    }

    /**
     * 赠送礼物埋点
     */
    private fun trackGift() {
        val params = HashMap<String?, String?>()
        params["guest_id"] = targetUserId
        params["gift_name"] = bagData[selectPos].name
        params["gift_id"] = bagData[selectPos].record_id.toString()
        params["gift_count"] = bagData[selectPos].num.toString()
        MultiTracker.onEvent(TrackConstants.B_BAG_SEND_GIFT_CLICK, params)
    }

    /**
     * 礼物碎片合成埋点
     */
    private fun trackGiftFragments() {
        val params = HashMap<String?, String?>()
        params["guest_id"] = targetUserId
        params["gift_name"] = bagData[selectPos].name
        params["gift_id"] = bagData[selectPos].record_id.toString()
        MultiTracker.onEvent(TrackConstants.B_BAG_SYNTHETIC_CLICK, params)
    }

    /**
     * 糖果提现埋点
     */
    private fun trackCandyToCash() {
        val params = HashMap<String?, String?>()
        params["guest_id"] = targetUserId
        params["candy_count"] = bagData[selectPos].info?.num
        MultiTracker.onEvent(TrackConstants.B_BAG_WITHDRAW_CLICK, params)
    }

    private fun observerData() {
        bagViewModel.bagData.observe(this, Observer {
            ll_error_container.visibility = View.GONE
            fillData(it)
        })

        bagViewModel.sendBagGiftData.observe(this, Observer {
            if (it.first.type in TYPE_NORMAL_GIFT..TYPE_EXCLUSIVE_GIFT){
                if (activity is SessionActivity){
                    (activity as SessionActivity).showGiftAnimBag(it.first.gift_id, it.second, it.first.image, it.first.effect?.audio, it.first.name, Constants.GIFT_NORMAL)
                }
                if (parentFragment is LiveMultiGiftSendDialog) {
                    trackLiveRoomGiftSuccess()
                }
            }else if (it.first.type == TYPE_GIFT_FRAGMENTS) {
                if (parentFragment is LiveMultiGiftSendDialog) {
                    trackLiveGiftFragmentsSuccess()
                }
            }

            bagViewModel.getBagData(mRoomId)
        })
    }

    fun setContentGpVisibility(isContentGpVisibly:Boolean) {
        mIsContentGpVisibly = isContentGpVisibly
    }

    private fun fillData(bag: Bag){
        bagData.clear()
        bagData.addAll(bag.gift_list)

        if (bagData.isEmpty()) {
            mainLayout.content_gp.visibility = View.GONE
            mainLayout.send_btn.visibility = View.GONE
            mainLayout.empty_iv.visibility = View.VISIBLE
            mListener?.bagIsEmpty(true)
            return
        }
        mListener?.bagIsEmpty(false)
        if (mIsContentGpVisibly) {
            mainLayout.send_btn.visibility = View.VISIBLE
        }else {
            mainLayout.send_btn.visibility = View.GONE
        }
        mainLayout.empty_iv.visibility = View.GONE

        count = bagData.size / countPerPage
        if (bagData.size % countPerPage > 0){
            count += 1
        }

        viewPageData.clear()
        indicators.clear()

        for (i in 0 until count) {
            viewPageData.add(false)
            indicators.add(false)
        }

        indicators[currentPage] = true

        initBottomLayout(bagData[selectPos])

        initViewPager()
        initIndicator()
    }

    fun getData(page: Int) = bagData.subList(countPerPage * (page - 1), if (countPerPage * page >= bagData.size) bagData.size else countPerPage * page)

    fun getSpan(): SpannableStringBuilder = SpannableStringBuilder()
            .append(SpannableString("数量   ").apply {
                setSpan(AbsoluteSizeSpan(12,true),0,length,Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            })
            .append(SpannableString("$giftNum ").apply {
                setSpan(VerticalCenterSpan(16,Color.parseColor("#ffffff")),0,length,Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            })
            .append(SpannableString(" ").apply {
                setSpan(ImageSpanAlignCenter(ContextCompat.getDrawable(ContextUtils.getApplicationContext(),R.mipmap.ic_more_select)),0,length,Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            })

    private fun initBottomLayout(bagBean: BagBean) {
        val constraintSet = ConstraintSet().apply { clone(mainLayout.root) }
        mListener?.giftTypeSelect(bagBean.type)
        when (bagBean.type) {
            TYPE_NORMAL_GIFT,TYPE_EXCLUSIVE_GIFT -> {
                left_tv.gravity = Gravity.CENTER
                left_tv.text = getSpan()
                right_tv.text = getString(R.string.bag_send_gift)
                constraintSet.setHorizontalWeight(left_tv.id, 1f)
                constraintSet.setHorizontalWeight(right_tv.id, 1f)
                if (parentFragment is LiveMultiGiftSendDialog) {
                    (parentFragment as LiveMultiGiftSendDialog)?.giftLeftLive.gravity = Gravity.CENTER
                    (parentFragment as LiveMultiGiftSendDialog)?.giftLeftLive.text = getSpan()
                    (parentFragment as LiveMultiGiftSendDialog)?.giftRightLive.text = getString(R.string.bag_send_gift)
                    constraintSet.setHorizontalWeight((parentFragment as LiveMultiGiftSendDialog)?.giftLeftLive.id, 1f)
                    constraintSet.setHorizontalWeight((parentFragment as LiveMultiGiftSendDialog)?.giftRightLive.id, 1f)
                }
            }
            TYPE_GIFT_FRAGMENTS -> {
                left_tv.gravity =  Gravity.START or Gravity.CENTER_VERTICAL
                left_tv.text = bagBean.use_description
                right_tv.text = getString(R.string.bag_combine)
                constraintSet.setHorizontalWeight(left_tv.id, 2.5f)
                constraintSet.setHorizontalWeight(right_tv.id, 1f)
                if (parentFragment is LiveMultiGiftSendDialog) {
                    (parentFragment as LiveMultiGiftSendDialog)?.giftLeftLive.gravity =  Gravity.START or Gravity.CENTER_VERTICAL
                    (parentFragment as LiveMultiGiftSendDialog)?.giftLeftLive.text = bagBean.use_description
                    (parentFragment as LiveMultiGiftSendDialog)?.giftRightLive.text = getString(R.string.bag_combine)
                    constraintSet.setHorizontalWeight((parentFragment as LiveMultiGiftSendDialog)?.giftLeftLive.id, 2.5f)
                    constraintSet.setHorizontalWeight((parentFragment as LiveMultiGiftSendDialog)?.giftRightLive.id, 1f)
                }
            }
            TYPE_CANDY,TYPE_CASH -> {
                left_tv.gravity =  Gravity.START or Gravity.CENTER_VERTICAL
                left_tv.text = bagBean.use_description
                right_tv.text = getString(R.string.bag_change)
                constraintSet.setHorizontalWeight(left_tv.id, 2.5f)
                constraintSet.setHorizontalWeight(right_tv.id, 1f)
                if (parentFragment is LiveMultiGiftSendDialog) {
                    (parentFragment as LiveMultiGiftSendDialog)?.giftLeftLive.gravity =  Gravity.START or Gravity.CENTER_VERTICAL
                    (parentFragment as LiveMultiGiftSendDialog)?.giftLeftLive.text = bagBean.use_description
                    (parentFragment as LiveMultiGiftSendDialog)?.giftRightLive.text = getString(R.string.bag_change)
                    constraintSet.setHorizontalWeight((parentFragment as LiveMultiGiftSendDialog)?.giftLeftLive.id, 2.5f)
                    constraintSet.setHorizontalWeight((parentFragment as LiveMultiGiftSendDialog)?.giftRightLive.id, 1f)
                }
            }
        }
        constraintSet.applyTo(mainLayout.root)
    }

    fun setGiftNum(giftNum : Int){
        this.giftNum = giftNum;
    }

    private fun initIndicator() {
        indicator_rv.apply {
            indicatorAdapter = object : CommonAdapter<Boolean>(context, R.layout.item_indicator) {
                override fun convert(holder: CommonViewHolder, t: Boolean, pos: Int) {
                    holder.setBackgroundResource(R.id.indicator, if (t) R.drawable.bg_indicator_dot_focus else R.drawable.bg_indicator_dot_unfocus)
                }
            }

            adapter = indicatorAdapter

            layoutManager = LinearLayoutManager(context).apply { orientation = LinearLayoutManager.HORIZONTAL }
        }

        indicatorAdapter.update(indicators)
    }

    private fun initViewPager() {
        viewPagerAdapter = object : CommonAdapter<Boolean>(this.context, R.layout.item_bag) {
            override fun convert(holder: CommonViewHolder, t: Boolean, pos: Int) {

                val adapterItem = object : CommonAdapter<BagBean>(context, R.layout.item_bag_item) {
                    override fun convert(holder: CommonViewHolder, item: BagBean, posItem: Int) {
                        Glide.with(this@BagFragment).load(item.image).into(holder.getView(R.id.icon_iv))
                        holder.setText(R.id.name_tv, item.name)
                        holder.setText(R.id.amount_tv, "x${item.num}")
                        if (item.type == TYPE_CANDY) {
                            holder.setText(R.id.value_tv, "总价值:${item.info?.cash?.toLong()?.let { NumberFormatUtil.formatCandyToCash(it)}}元")
                        } else {
                            holder.setText(R.id.value_tv, "总价值:${item.coins}金币")
                        }
                        holder.setBackgroundResource(R.id.bg, if (selectPos == (pos * countPerPage + posItem)) R.drawable.bg_gift_selector else context!!.resources.getColor(R.color.transparent))
                        holder.setOnClickListener(R.id.bg) {
                            selectPos = pos * countPerPage + posItem
                            initBottomLayout(item)
                            viewPagerAdapter.notifyDataSetChanged()
                        }
                    }
                }
                holder.getView<RecyclerView>(R.id.bag_rv).apply {
                    adapter = adapterItem
                    layoutManager = GridLayoutManager(context, countPerPage/2)
                }

                adapterItem.update(getData(pos + 1))
            }
        }

        bag_vp2.adapter = viewPagerAdapter

        bag_vp2.orientation = ViewPager2.ORIENTATION_HORIZONTAL

        bag_vp2.isUserInputEnabled = true

        bag_vp2.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                currentPage = position
                for (i in indicatorAdapter.datas.indices) {
                    indicatorAdapter.datas[i] = i == position
                }
                indicatorAdapter.notifyDataSetChanged()
            }
        })

        viewPagerAdapter.update(viewPageData)


        bag_vp2.setCurrentItem(currentPage,false)
    }
    override fun onEventComing(eventCenter: EventCenter<*>) {
        if (eventCenter.eventCode == Constants.EVENT_CODE_WITHDRAW_SUCCESS) {
            //提现成功,刷新糖果同步
            bagViewModel.getBagData(mRoomId)
            selectPos = 0
        }
    }
}