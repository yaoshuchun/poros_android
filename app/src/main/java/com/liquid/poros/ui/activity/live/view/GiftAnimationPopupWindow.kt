package com.liquid.poros.ui.activity.live.view

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.content.Context
import android.view.View
import android.view.animation.DecelerateInterpolator
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.airbnb.lottie.LottieAnimationView
import com.bumptech.glide.Glide
import com.liquid.base.utils.LogOut
import com.liquid.im.kit.custom.RoomActionAttachment
import com.liquid.poros.R
import com.liquid.poros.constant.Constants
import com.liquid.poros.constant.FileDownloadConstant
import com.liquid.poros.helper.AudioPlayerHelper
import com.liquid.poros.utils.LottieAnimationUtils
import com.liquid.poros.utils.StringUtil
import com.liquid.poros.utils.SvgaAnimationUtils
import com.opensource.svgaplayer.SVGACallback
import com.opensource.svgaplayer.SVGAImageView
import razerdp.basepopup.BasePopupWindow
import java.io.FileInputStream


/**
 * 送礼动画弹框
 */
class GiftAnimationPopupWindow(context: Context) : BasePopupWindow(context),SVGACallback {
    private lateinit var lottie_likeanim: LottieAnimationView
    private lateinit var gift_num_anim: ConstraintLayout
    private lateinit var anim_gift_name: TextView
    private lateinit var anim_gift_pic: ImageView
    private lateinit var anim_gift_num: TextView
    private lateinit var svga_anim: SVGAImageView
    private var fis: FileInputStream? = null
    private val audioPlayerHelper: AudioPlayerHelper by lazy { AudioPlayerHelper() }
    var onAnimationEnd: (() -> Unit)? = null

    override fun onCreateContentView(): View {
        setBackPressEnable(false)
        return createPopupById(R.layout.dialog_send_gift)
    }

    override fun onViewCreated(contentView: View) {
        super.onViewCreated(contentView)
        lottie_likeanim = findViewById(R.id.lottie_likeanim)
        gift_num_anim = findViewById(R.id.gift_num_anim)
        anim_gift_name = findViewById(R.id.anim_gift_name)
        anim_gift_pic = findViewById(R.id.anim_gift_pic)
        anim_gift_num = findViewById(R.id.anim_gift_num)
        svga_anim = findViewById(R.id.svga_anim)
    }

    override fun onDismiss() {
        super.onDismiss()
        lottie_likeanim.removeAnimatorListener(animatorListener)
    }

    override fun onPreShow(): Boolean {
        lottie_likeanim.addAnimatorListener(animatorListener)
        return super.onPreShow()
    }

    /**
     * 展示送礼动画
     */
    fun showGiftAnimation(actionAttachment: RoomActionAttachment) {
        var giftFileName = LottieAnimationUtils.getGiftFileName(actionAttachment.giftId)
        if (StringUtil.isEmpty(giftFileName)) {
            return
        }
        showPopupWindow()
        try {
            if (lottie_likeanim.isAnimating) {
                lottie_likeanim.cancelAnimation()
            }
            anim_gift_num.text = "x ${actionAttachment.num}"
            anim_gift_name.text = actionAttachment.giftName
            Glide.with(context).load(actionAttachment.giftIcon).into(anim_gift_pic)
            gift_num_anim.visibility = View.VISIBLE
            normalGiftAnim(gift_num_anim)
            val imagePath = "${FileDownloadConstant.rootPathAnimation}/$giftFileName/images"
            val jsonPath = "${FileDownloadConstant.rootPathAnimation}/$giftFileName/data.json"
            fis = LottieAnimationUtils.loadLottie(lottie_likeanim, imagePath, jsonPath, giftFileName)
            anim_gift_name.postDelayed(Runnable {
                lottie_likeanim.visibility = View.VISIBLE
            }, 200)
            if (!actionAttachment.giftAudio.isNullOrBlank()) {
                audioPlayerHelper.resetUrl(actionAttachment.giftAudio)
            }
        } catch (h: Throwable) {
        }
    }

    /**
     * 展示送礼动画
     * @param giftEffectType 0-普通礼物,1-盲盒
     */
    fun showGiftAnimation(giftId: Int, giftNum: Int, giftIcon: String, giftAudio: String? = null, giftName: String, giftEffectType: Int) {
        var giftFileName = LottieAnimationUtils.getGiftFileName(giftId)
        if (StringUtil.isEmpty(giftFileName)) {
            return
        }
        showPopupWindow()
        try {
            when (giftEffectType) {
                Constants.GIFT_NORMAL -> {
                    //普通礼物
                    if (lottie_likeanim.isAnimating) {
                        lottie_likeanim.cancelAnimation()
                    }
                    val imagePath = "${FileDownloadConstant.rootPathAnimation}/$giftFileName/images"
                    val jsonPath = "${FileDownloadConstant.rootPathAnimation}/$giftFileName/data.json"
                    fis = LottieAnimationUtils.loadLottie(lottie_likeanim, imagePath, jsonPath, giftFileName)
                    anim_gift_name.postDelayed(Runnable {
                        lottie_likeanim.visibility = View.VISIBLE
                        svga_anim.visibility = View.GONE
                    }, 200)
                }
                Constants.GIFT_BLIND -> {
                    //盲盒礼物
                    val imagePath = "${FileDownloadConstant.rootPathAnimation}/$giftFileName/$giftFileName.svga"
                    SvgaAnimationUtils.loadSvga(context, svga_anim, imagePath, giftName)
                    //礼物动画播放监听
                    anim_gift_name.postDelayed(Runnable {
                        lottie_likeanim.visibility = View.GONE
                        svga_anim.visibility = View.VISIBLE
                    }, 200)
                    //动画监听
                    svga_anim.callback = this
                }
            }
            anim_gift_num.text = "x ${giftNum}"
            anim_gift_name.text = giftName
            Glide.with(context).load(giftIcon).into(anim_gift_pic)
            gift_num_anim.visibility = View.VISIBLE
            normalGiftAnim(gift_num_anim)
            if (!giftAudio.isNullOrBlank()) {
                audioPlayerHelper.resetUrl(giftAudio)
            }
        } catch (h: Throwable) {
        }
    }

    private fun normalGiftAnim(view: View?) {
        val animatorSetsuofang = AnimatorSet() //组合动画
        val scaleX = ObjectAnimator.ofFloat(view, "translationX", -500f, 0f)
        val anim = ObjectAnimator.ofFloat(view, "alpha", 0f, 1f)
        animatorSetsuofang.duration = 2000
        animatorSetsuofang.interpolator = DecelerateInterpolator()
        animatorSetsuofang.play(scaleX).with(anim) //两个动画同时开始
        animatorSetsuofang.start()
    }

    private val animatorListener: Animator.AnimatorListener = object : Animator.AnimatorListener {
        override fun onAnimationStart(animator: Animator) {}
        override fun onAnimationEnd(animator: Animator) {
            onAnimationEnd?.invoke()
            lottie_likeanim.visibility = View.GONE
            dismiss()
            LottieAnimationUtils.inputStreamClose(fis)
        }

        override fun onAnimationCancel(animator: Animator) {}
        override fun onAnimationRepeat(animator: Animator) {}
    }

    override fun onFinished() {
        //播放完成,弹窗消失
        svga_anim.visibility = View.GONE
        dismiss()
    }

    override fun onPause() {
    }

    override fun onRepeat() {
    }

    override fun onStep(frame: Int, percentage: Double) {
    }
}
