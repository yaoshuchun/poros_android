package com.liquid.poros.ui.popupwindow

import android.app.Activity
import android.content.Context
import android.text.SpannableStringBuilder
import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.liquid.base.adapter.CommonAdapter
import com.liquid.base.adapter.CommonViewHolder
import com.liquid.base.adapter.MultiItemCommonAdapter
import com.liquid.base.adapter.MultiItemTypeSupport
import com.liquid.poros.R
import com.liquid.poros.base.BaseActivity
import com.liquid.poros.databinding.PopFemaleUserInfoDialogBinding
import com.liquid.poros.entity.UserPhotoInfoV2
import com.liquid.poros.ui.fragment.viewmodel.UserInfoViewModel
import com.liquid.poros.utils.ScreenUtil
import com.liquid.poros.utils.glide.GlideEngine
import com.liquid.poros.widgets.GridSpacingItemDecoration
import com.liquid.poros.widgets.WrapContentGridLayoutManager
import com.luck.picture.lib.PictureSelector
import com.luck.picture.lib.entity.LocalMedia
import razerdp.basepopup.BasePopupWindow
import androidx.lifecycle.Observer
import java.util.*

/**
 * cp直播间女嘉宾用户个人信息弹窗
 */
class FemaleUserInfoPopWindow(context: Context) : BasePopupWindow(context) {
    private var mAdapter: CommonAdapter<UserPhotoInfoV2.Photos>? = null

    private lateinit var binding: PopFemaleUserInfoDialogBinding
    private lateinit var userInfoViewModel: UserInfoViewModel

    override fun onCreateContentView(): View {
        return createPopupById(R.layout.pop_female_user_info_dialog)
    }

    override fun onViewCreated(contentView: View) {
        binding = PopFemaleUserInfoDialogBinding.bind(contentView)
        userInfoViewModel = (context as BaseActivity).getActivityViewModel(UserInfoViewModel::class.java)
        binding.rvPics.layoutManager = WrapContentGridLayoutManager(context, 4)
        binding.rvPics.addItemDecoration(GridSpacingItemDecoration(4, ScreenUtil.dip2px(8f), false))
        mAdapter = object : MultiItemCommonAdapter<UserPhotoInfoV2.Photos>(context, object : MultiItemTypeSupport<UserPhotoInfoV2.Photos> {
            override fun getLayoutId(itemType: Int): Int {
                return if (itemType == 1) {
                    R.layout.item_cp_game_ref_img
                } else R.layout.item_cp_game_ref_video
            }

            override fun getItemViewType(position: Int, photos: UserPhotoInfoV2.Photos): Int {
                return Integer.valueOf(photos.type)
            }
        }) {
            override fun convert(holder: CommonViewHolder, refItem: UserPhotoInfoV2.Photos, pos: Int) {
                val iv_image = holder.getView<ImageView>(R.id.iv_image)
                val itemHeight = (ScreenUtil.adjustWidgetWidth() - ScreenUtil.dip2px(55f)) / 4
                holder.itemView.layoutParams.height = itemHeight
                holder.itemView.layoutParams.width = itemHeight
                val headOptions = RequestOptions()
                        .transform(CenterCrop(), RoundedCorners(ScreenUtil.dip2px(6f)))
                Glide.with(mContext)
                        .load(refItem.path)
                        .apply(headOptions)
                        .into(iv_image)
                holder.itemView.setOnClickListener {
                    if (mAdapter?.datas != null && mAdapter?.datas?.size?:0 > 0) {
                        if (mAdapter!!.datas[pos].type == 2) {
                            PictureSelector.create(mContext as Activity).externalPictureVideo(mAdapter!!.datas[pos].path)
                        } else {
                            //点击图片预览
                            PictureSelector.create(mContext as Activity)
                                    .themeStyle(R.style.picture_default_style)
                                    .isNotPreviewDownload(true)
                                    .isPreviewVideo(true)
                                    .imageEngine(GlideEngine.createGlideEngine())
                                    .openExternalPreview(pos, getSelectList(mAdapter!!.datas))
                        }
                    }
                }
            }
        }
        binding.rvPics.isNestedScrollingEnabled = false
        binding.rvPics.setHasFixedSize(true)
        binding.rvPics.adapter = mAdapter
        initObserver()
    }

    private fun initObserver() {
        userInfoViewModel.userInfoLiveData.observe(context as BaseActivity, Observer{
            if (it.user_info == null) {
                return@Observer
            }
            if (!it.user_info!!.games.isNullOrEmpty()) {
                val spannableString = SpannableStringBuilder(context.getString(R.string.cp_want_play))
                for (i in it.user_info!!.games!!.indices) {
                    spannableString.append(" ")
                    if (i == it.user_info!!.games!!.size - 1) {
                        spannableString.append(it.user_info!!.games?.get(i))
                    } else {
                        spannableString.append(it.user_info!!.games?.get(i).toString() + " |")
                    }
                }
                binding.tvWantGame.visibility = View.VISIBLE
                binding.tvWantGame.setBackgroundResource(R.drawable.bg_color_2d295c)
                binding.tvWantGame.text = spannableString
            } else {
                binding.tvWantGame.visibility = View.GONE
            }
        })
        userInfoViewModel.userPhotoLiveData.observe(context as BaseActivity, Observer {
            if (!it.photos.isNullOrEmpty()) {
                binding.rvPics.visibility = View.VISIBLE
                binding.tvUserPicsSeat.visibility = View.GONE
                val photoLists: List<UserPhotoInfoV2.Photos> = it.photos!!.subList(0, if (it.photos!!.size >= 4) 4 else it.photos!!.size)
                mAdapter?.update(photoLists)
                mAdapter?.notifyDataSetChanged()
            } else {
                binding.rvPics.visibility = View.GONE
                binding.tvUserPicsSeat.visibility = View.VISIBLE
            }
        })
    }

    fun setData(userId: String) {
        userInfoViewModel.getUserInfo(userId)
        userInfoViewModel.getUserPhotos(userId)
    }

    private fun getSelectList(data: List<UserPhotoInfoV2.Photos>): List<LocalMedia> {
        val localMedia: MutableList<LocalMedia> = ArrayList()
        for (photos in data) {
            if (photos.type != 2) {
                localMedia.add(LocalMedia(photos.path, 0, 0, ""))
            }
        }
        return localMedia
    }
}