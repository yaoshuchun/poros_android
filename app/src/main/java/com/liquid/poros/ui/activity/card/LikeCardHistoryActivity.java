package com.liquid.poros.ui.activity.card;

import android.os.Bundle;
import android.widget.TextView;

import com.liquid.base.adapter.CommonAdapter;
import com.liquid.base.adapter.CommonViewHolder;
import com.liquid.poros.R;
import com.liquid.poros.base.BaseActivity;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.contract.likecard.LikeCardContract;
import com.liquid.poros.contract.likecard.LikeCardPresenter;
import com.liquid.poros.entity.LikeCardInfo;
import com.liquid.poros.widgets.pulltorefresh.BaseRefreshListener;
import com.liquid.poros.widgets.pulltorefresh.PullToRefreshLayout;
import com.liquid.poros.widgets.pulltorefresh.ViewStatus;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;

@Deprecated
public class LikeCardHistoryActivity extends BaseActivity implements LikeCardContract.View, BaseRefreshListener {
    @BindView(R.id.rv_like_card)
    RecyclerView rv_like_card;
    @BindView(R.id.refresh)
    PullToRefreshLayout container;
    @BindView(R.id.tv_my_like_card_count)
    TextView tv_my_like_card_count;
    private LikeCardPresenter mPresenter;
    private CommonAdapter<LikeCardInfo.Data.Record> mCommonAdapter;
    private int page = 0;

    @Override
    protected void parseBundleExtras(Bundle extras) {

    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.activity_like_card_history;
    }

    @Override
    protected void initViewsAndEvents(Bundle savedInstanceState) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
        rv_like_card.setLayoutManager(linearLayoutManager);
        mCommonAdapter = new CommonAdapter<LikeCardInfo.Data.Record>(this, R.layout.item_like_card) {
            @Override
            public void convert(CommonViewHolder holder, LikeCardInfo.Data.Record likeCardBean, int pos) {
                holder.setText(R.id.tv_name, likeCardBean.getDesc());
                holder.setText(R.id.tv_time, likeCardBean.getCreate_time());
                if (likeCardBean.getChange_cnt() > 0) {
                    holder.setText(R.id.tv_like_card_count, "+" + likeCardBean.getChange_cnt());
                } else {
                    holder.setText(R.id.tv_like_card_count, String.valueOf(likeCardBean.getChange_cnt()));
                }
            }
        };
        rv_like_card.setAdapter(mCommonAdapter);
        mPresenter = new LikeCardPresenter();
        mPresenter.attachView(this);
        container.setRefreshListener(this);
        container.setCanRefresh(true);
        mPresenter.fetchedLikeCardList(page);
        container.setCanLoadMore(true);
    }

    @Override
    protected String getPageId() {
        return TrackConstants.PAGE_LIKE_CARD_HISTORY;
    }

    @Override
    public void onLikeCardListFetched(LikeCardInfo likeCardInfo) {
        container.finishRefresh();
        container.finishLoadMore();
        if ((page == 0) && (likeCardInfo == null || likeCardInfo.getData() == null || likeCardInfo.getData().getRecord_list().size() == 0)) {
            container.showView(ViewStatus.EMPTY_STATUS);
            container.setCanLoadMore(false);
            return;
        }
        tv_my_like_card_count.setText(String.valueOf(likeCardInfo.getData().getGift_card_cnt()));
        container.showView(ViewStatus.CONTENT_STATUS);
        if (page == 0) {
            mCommonAdapter.update(likeCardInfo.getData().getRecord_list());
        } else {
            mCommonAdapter.add(likeCardInfo.getData().getRecord_list());
        }
    }

    @Override
    public void refresh() {
        //下拉刷新
        page = 0;
        mPresenter.fetchedLikeCardList(page);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }

    @Override
    public void loadMore() {
        //加载更多
        ++page;
        mPresenter.fetchedLikeCardList(page);
    }
}
