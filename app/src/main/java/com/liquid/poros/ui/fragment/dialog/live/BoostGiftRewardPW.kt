package com.liquid.poros.ui.fragment.dialog.live

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.TextView
import android.widget.ViewSwitcher
import androidx.lifecycle.Observer
import com.blankj.utilcode.util.SizeUtils
import com.liquid.base.event.EventCenter
import com.liquid.base.utils.ToastUtil
import com.liquid.poros.R
import com.liquid.poros.analytics.utils.MultiTracker
import com.liquid.poros.base.BaseActivity
import com.liquid.poros.constant.Constants
import com.liquid.poros.constant.TrackConstants
import com.liquid.poros.databinding.PopupBoostGiftRewardBinding
import com.liquid.poros.entity.BoostLotteryListBean
import com.liquid.poros.ui.activity.gifbox.GetedGiftOfBoxNoticesActivity
import com.liquid.poros.ui.activity.live.BoostGiftRewardRecordActivity
import com.liquid.poros.ui.fragment.dialog.live.viewmodel.BoostGiftRewardVM
import com.liquid.poros.utils.ViewUtils
import com.liquid.poros.viewmodel.GiftBoxVM
import com.liquid.poros.widgets.LotteryView
import com.liquid.poros.widgets.TextSwitcherAnimation
import de.greenrobot.event.EventBus
import de.greenrobot.event.Subscribe
import de.greenrobot.event.ThreadMode
import razerdp.basepopup.BasePopupWindow

/**
 * 助力抽奖详情
 */
class BoostGiftRewardPW(context: Context) : BasePopupWindow(context) {
    private lateinit var binding: PopupBoostGiftRewardBinding
    private lateinit var boostGiftRewardVM: BoostGiftRewardVM

    //是否是初级宝箱
    private var mIsBoostCommon: Boolean = true

    //初级宝箱奖品
    private val boostCommonList = ArrayList<BoostLotteryListBean>()

    //高级宝箱奖品
    private val boostAdvanceList = ArrayList<BoostLotteryListBean>()

    //房间id
    private var mRoomId: String? = null

    //初级宝箱
    private var TYPE_BOOST_COMMON: String? = "boost_cm"

    //高级宝箱
    private var TYPE_BOOST_ADVANCE: String? = "boost_ad"

    //中奖礼物图片
    private var mGiftImage: String? = null

    //中奖礼物名称
    private var mGiftName: String? = null

    //助力中奖轮播
    private var mTextSwitcherAnimation: TextSwitcherAnimation? = null
    private var mGuestId: String? = null//女嘉宾id
    private var mAnchorId: String? = null//主持人id
    private var mPos1Id: String? = null//一号麦id
    private var mBoostLotteryNum: Int? = 0 //奖品数
    //抽奖奖品弹窗
    private var lotteryGiftDetailsPW :LotteryGiftDetailsPW ?=null

    override fun onCreateContentView(): View {
        popupGravity = Gravity.END or Gravity.BOTTOM
        setPopupGravityMode(GravityMode.ALIGN_TO_ANCHOR_SIDE, GravityMode.RELATIVE_TO_ANCHOR)
        offsetX = SizeUtils.dp2px(5.0f)
        return createPopupById(R.layout.popup_boost_gift_reward)
    }

    override fun onViewCreated(contentView: View) {
        super.onViewCreated(contentView)
        binding = PopupBoostGiftRewardBinding.bind(contentView)
        boostGiftRewardVM = (context as BaseActivity).getActivityViewModel(BoostGiftRewardVM::class.java)
        EventBus.getDefault().register(this)
        initObserver()
        initListener()
    }

    override fun onShowing() {
        super.onShowing()
        updateBoostStatus(true)
        this.mTextSwitcherAnimation = TextSwitcherAnimation(binding.tsBoostGiftSwitch)
        boostGiftRewardVM.getBoostLotteryInfo()
    }
    /**
     * 获取助力抽奖详情
     */
    fun getBoostLotteryInfo(roomId: String, guestId: String?, anchorId: String?,pos1MicId: String?) {
        mRoomId = roomId
        mGuestId = guestId
        mAnchorId = anchorId
        mPos1Id = pos1MicId
    }


    private fun initObserver() {
        boostGiftRewardVM.boostLotteryInfoLiveData.observe(context as BaseActivity, Observer {
            if (it.lotterys == null) {
                return@Observer
            }
            it.lotterys?.common?.let { it1 -> boostCommonList?.addAll(it1) }
            it.lotterys?.advance?.let { it1 -> boostAdvanceList?.addAll(it1) }
            binding.lvBoostLuckPan.setLotteryData(mRoomId!!, if (mIsBoostCommon) TYPE_BOOST_COMMON!! else TYPE_BOOST_ADVANCE!!, boostCommonList, mGuestId, mAnchorId, mPos1Id)
            binding.tvBoostRewardCommon.text = "(剩余${it?.lotterys?.common_count}次宝箱机会)"
            binding.tvBoostRewardAdvance.text = "(剩余${it?.lotterys?.advance_count}次宝箱机会)"
            if (!it?.lotterys?.boost_reward_msg_list.isNullOrEmpty()) {
                binding.tsBoostGiftSwitch.visibility = View.VISIBLE
                if (binding.tsBoostGiftSwitch != null && binding.tsBoostGiftSwitch.childCount < 2) {
                    binding.tsBoostGiftSwitch.setFactory(ViewSwitcher.ViewFactory {
                        val textView = TextView(context)
                        textView.setTextColor(Color.parseColor("#ffffff"))
                        textView.textSize = 12f
                        textView.gravity = Gravity.CENTER
                        textView.setSingleLine(true)
                        textView.layoutParams = FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
                        textView.setOnClickListener {
                            //中奖轮播
                            GetedGiftOfBoxNoticesActivity.start(context, GiftBoxVM.TYPE_BOOST)
                        }
                        textView
                    })
                }
                mTextSwitcherAnimation!!.setTexts(it?.lotterys?.boost_reward_msg_list!!.toTypedArray()).create()
            } else {
                binding.tsBoostGiftSwitch.visibility = View.GONE
            }
        })
        boostGiftRewardVM.openBoostLotterySuccess.observe(context as BaseActivity, Observer {
            binding.lvBoostLuckPan.updateCurrentPosition(it?.prize?.prize_position!!)
            binding.tvBoostRewardCommon.text = "(剩余${it?.prize?.common_count}次宝箱机会)"
            binding.tvBoostRewardAdvance.text = "(剩余${it?.prize?.advance_count}次宝箱机会)"
            //更新抽奖券总数
            val boostTotalCount = it?.prize?.common_count!! + it?.prize?.advance_count!!
            EventBus.getDefault().post(EventCenter<Any?>(Constants.EVENT_CODE_UPDATE_BOOST_COUNT, boostTotalCount))
            mGiftImage = it?.prize?.image
            mGiftName = it?.prize?.prize_name
            mBoostLotteryNum = it?.prize.boost_lottery_num
            val params: MutableMap<String, String?> = HashMap<String, String?>()
            params["anchor_id"] = mAnchorId
            params["guest_id"] = mGuestId
            params["talk_user_id"] = mPos1Id
            params["get_prize_name"] = mGiftName
            if (mIsBoostCommon) {
                MultiTracker.onEvent(TrackConstants.B_PRIMARY_TREASURE_BOX_GET_EXPOSURE, params)
            } else {
                MultiTracker.onEvent(TrackConstants.B_HIGH_TREASURE_BOX_GET_EXPOSURE, params)
            }
        })
        boostGiftRewardVM.openBoostLotteryFailed.observe(context as BaseActivity, Observer {
            ToastUtil.show(it.msg)
        })
    }

    private fun initListener() {
        binding.ivBack.setOnClickListener {
            dismiss()
        }
        binding.ivBoostRewardRecord.setOnClickListener {
            //中奖记录
            if (ViewUtils.isFastClick()) {
                return@setOnClickListener
            }
            val bundle = Bundle()
            bundle.putString(Constants.TARGET_PATH, "")
            val intent = Intent(context, BoostGiftRewardRecordActivity::class.java)
            intent.putExtras(bundle)
            context.startActivity(intent)
            val params: MutableMap<String, String?> = HashMap<String, String?>()
            params["anchor_id"] = mAnchorId
            MultiTracker.onEvent(TrackConstants.B_WIN_REWARD_PAGE_EXPOSURE, params)
        }
        binding.flBoostRewardCommon.setOnClickListener {
            //初级宝箱
            updateBoostStatus(true)
        }
        binding.flBoostRewardAdvance.setOnClickListener {
            //高级宝箱
            updateBoostStatus(false)
        }
        binding.lvBoostLuckPan.setOnLotteryAnimationEndListener(object : LotteryView.OnLotteryAnimationEndListener {
            override fun OnAnimationEnd() {
                //抽奖奖品弹窗
                showLotteryGiftDialog()
            }
        })
    }

    /**
     * 初&高级宝箱状态切换
     * @param isBoostCommon true 初级宝箱,反之高级宝箱
     */
    private fun updateBoostStatus(isBoostCommon: Boolean) {
        mIsBoostCommon = isBoostCommon
        if (mIsBoostCommon) {
            binding.flBoostRewardCommon.setBackgroundResource(R.mipmap.icon_boost_reward_common_select)
            binding.flBoostRewardAdvance.setBackgroundResource(R.mipmap.icon_boost_reward_advance_normal)
            binding.ivContanier.setBackgroundResource(R.mipmap.bg_boost_reward_common)
            binding.tvBoostRewardTip.text = context.getString(R.string.dialog_boost_reward_common_tip)
            if (!boostCommonList.isNullOrEmpty()) {
                binding.lvBoostLuckPan.setLotteryData(mRoomId!!, if (mIsBoostCommon) TYPE_BOOST_COMMON!! else TYPE_BOOST_ADVANCE!!, boostCommonList, mGuestId, mAnchorId, mPos1Id)
            }
            val params: MutableMap<String, String?> = HashMap<String, String?>()
            params["anchor_id"] = mAnchorId
            params["guest_id"] = mGuestId
            params["talk_user_id"] = mPos1Id
            MultiTracker.onEvent(TrackConstants.B_PRIMARY_TREASURE_BOX_EXPOSURE, params)
        } else {
            binding.flBoostRewardCommon.setBackgroundResource(R.mipmap.icon_boost_reward_common_normal)
            binding.flBoostRewardAdvance.setBackgroundResource(R.mipmap.icon_boost_reward_advance_select)
            binding.ivContanier.setBackgroundResource(R.mipmap.bg_boost_reward_advance)
            binding.tvBoostRewardTip.text = context.getString(R.string.dialog_boost_reward_advance_tip)
            if (!boostAdvanceList.isNullOrEmpty()) {
                binding.lvBoostLuckPan.setLotteryData(mRoomId!!, if (mIsBoostCommon) TYPE_BOOST_COMMON!! else TYPE_BOOST_ADVANCE!!, boostAdvanceList, mGuestId, mAnchorId, mPos1Id)
            }
            val params: MutableMap<String, String?> = HashMap<String, String?>()
            params["anchor_id"] = mAnchorId
            params["guest_id"] = mGuestId
            params["talk_user_id"] = mPos1Id
            MultiTracker.onEvent(TrackConstants.B_HIGH_TREASURE_BOX_EXPOSURE, params)
        }
    }

    @Subscribe(threadMode = ThreadMode.MainThread)
    fun onEventBus(eventCenter: EventCenter<*>) {
        when (eventCenter.eventCode) {
            Constants.EVENT_CODE_OPEN_LOTTERY -> {
                //开始抽奖
                mRoomId?.let { boostGiftRewardVM.openBoostLottery(it, if (mIsBoostCommon) TYPE_BOOST_COMMON!! else TYPE_BOOST_ADVANCE!!) }
            }
        }
    }
    
    /**
     * 抽奖奖品
     */
    private fun showLotteryGiftDialog() {
        if (lotteryGiftDetailsPW == null) {
            lotteryGiftDetailsPW = LotteryGiftDetailsPW(context)
        }
        lotteryGiftDetailsPW?.setLotteryGiftData(mGiftImage, mGiftName, mBoostLotteryNum!!, mGuestId, mAnchorId, mPos1Id, if (mIsBoostCommon) "primary" else "high")
        lotteryGiftDetailsPW?.popupGravity = Gravity.CENTER
        lotteryGiftDetailsPW?.showPopupWindow()
    }

    override fun onDestroy() {
        super.onDestroy()
        EventBus.getDefault().unregister(this)
    }
}