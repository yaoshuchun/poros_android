package com.liquid.poros.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.ProgressBar;

import com.liquid.poros.R;
import com.liquid.poros.base.BaseActivity;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.utils.InJavaScriptLocalObjUtils;
import com.liquid.poros.utils.StringUtil;
import com.liquid.poros.widgets.CustomHeader;
import com.liquid.poros.widgets.GuardWebViewClient;
import com.tencent.smtt.export.external.interfaces.JsResult;
import com.tencent.smtt.export.external.interfaces.SslError;
import com.tencent.smtt.export.external.interfaces.SslErrorHandler;
import com.tencent.smtt.sdk.WebChromeClient;
import com.tencent.smtt.sdk.WebSettings;
import com.tencent.smtt.sdk.WebView;

import butterknife.BindView;
import butterknife.OnClick;


public class WebViewActivity extends BaseActivity {

    @BindView(R.id.ch_header)
    public CustomHeader mHeader;

    @BindView(R.id.webview)
    public com.tencent.smtt.sdk.WebView mWebView;

    private String mTargetPath;

    @BindView(R.id.progress_bar)
    public ProgressBar mProgressBar;

    private String firstName;
    private String mFrom;
    private String guest_id;
    private InJavaScriptLocalObjUtils inJavaScriptLocalObjUtils;
    @Override
    protected String getPageId() {
        return TrackConstants.PAGE_WEB_VIEW;
    }

    @Override
    protected void parseBundleExtras(Bundle extras) {
        mTargetPath = extras.getString(Constants.TARGET_PATH);
        mFrom = extras.getString(Constants.FROM);
        guest_id = extras.getString("guest_id");
    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.activity_web_view;
    }


    @Override
    protected void initViewsAndEvents(Bundle savedInstanceState) {
        WebSettings webSetting = mWebView.getSettings();
        webSetting.setAllowFileAccess(true);
        webSetting.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
        webSetting.setSupportZoom(false);
        webSetting.setBuiltInZoomControls(false);
        webSetting.setDisplayZoomControls(false);
        webSetting.setUseWideViewPort(true);
        webSetting.setSupportMultipleWindows(false);
        webSetting.setLoadWithOverviewMode(true);
        webSetting.setAppCacheEnabled(true);
        // webSetting.setDatabaseEnabled(true);
        webSetting.setDomStorageEnabled(true);
        webSetting.setJavaScriptEnabled(true);
        webSetting.setGeolocationEnabled(true);
        webSetting.setAppCacheMaxSize(Long.MAX_VALUE);
        webSetting.setAppCachePath(this.getDir("appcache", 0).getPath());
        webSetting.setDatabasePath(this.getDir("databases", 0).getPath());
        webSetting.setGeolocationDatabasePath(this.getDir("geolocation", 0)
                .getPath());
        // webSetting.setPageCacheCapacity(IX5WebSettings.DEFAULT_CACHE_CAPACITY);
        webSetting.setPluginState(WebSettings.PluginState.ON_DEMAND);
        webSetting.setCacheMode(WebSettings.LOAD_DEFAULT);

        mWebView.loadUrl(mTargetPath);
        mProgressBar.setMax(100);
        mProgressBar.setProgressDrawable(this.getResources().getDrawable(R.drawable.webview_progressbar_color));

        mWebView.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onJsConfirm(WebView webView, String s, String s1, JsResult jsResult) {
                return super.onJsConfirm(webView, s, s1, jsResult);
            }

            @Override
            public void onProgressChanged(WebView webView, int progress) {
                super.onProgressChanged(webView, progress);
                if (mProgressBar != null) {
                    mProgressBar.setProgress(progress);
                    if (progress == 100) {
                        mProgressBar.setVisibility(View.GONE);
                    } else {
                        mProgressBar.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onReceivedTitle(WebView webView, String title_name) {
                super.onReceivedTitle(webView, title_name);
                if (mHeader != null) {
                    mHeader.setTitile(title_name);
                }
                if (TextUtils.isEmpty(firstName)) {
                    firstName = title_name;
                }

            }
        });
        mWebView.setWebViewClient(webViewClient);
        inJavaScriptLocalObjUtils = new InJavaScriptLocalObjUtils(this, mWebView);
        mWebView.addJavascriptInterface(inJavaScriptLocalObjUtils, "JSObj");

        if(StringUtil.isEquals(mFrom, "open_gift")){
            mHeader.showEndTv("中奖记录");
            mHeader.setOnEndTvClicker(new CustomHeader.OnEndTvClicker() {
                @Override
                public void endTvClick() {
                    Intent intent = new Intent(WebViewActivity.this, LotteryRecordActivity.class);
                    intent.putExtra("guest_id",guest_id);
                    startActivity(intent);
                }
            });
        }
    }


    private GuardWebViewClient webViewClient = new GuardWebViewClient() {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            boolean override = false;
            if (url != null & !url.startsWith("http")) {
                override = true;
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                try {
                    intent = Intent.parseUri(url, 0);
                    if (url.startsWith("weixin:")) {
                        intent.setPackage("com.tencent.mm");
                        startActivity(intent);
                    } else if (url.startsWith("pubgmhd")) {
                        intent.setPackage("com.tencent.tmgp.pubgmhd");
                        startActivity(intent);
                        finish();
                    }
                } catch (Exception e) {
                }
            }
            return override;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
        }

        @Override
        public void onReceivedSslError(WebView webView, SslErrorHandler sslErrorHandler, SslError sslError) {
            sslErrorHandler.proceed();
        }
    };

    @OnClick({R.id.iv_left})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_left:
                if (null != mWebView && mWebView.canGoBack()) {
                    mWebView.goBack();
                } else {
                    finish();
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (null != mWebView && mWebView.canGoBack()) {
            mWebView.goBack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        if (mWebView != null) {
            // 如果先调用destroy()方法，则会命中if (isDestroyed()) return;这一行代码，需要先onDetachedFromWindow()，再
            // destory()
            ViewParent parent = mWebView.getParent();
            if (parent != null) {
                ((ViewGroup) parent).removeView(mWebView);
            }
            mWebView.stopLoading();
            // 退出时调用此方法，移除绑定的服务，否则某些特定系统会报错
            mWebView.getSettings().setJavaScriptEnabled(false);
            mWebView.clearHistory();
            mWebView.clearView();
            mWebView.removeAllViews();
            mWebView.destroy();
        }
        super.onDestroy();

    }
}
