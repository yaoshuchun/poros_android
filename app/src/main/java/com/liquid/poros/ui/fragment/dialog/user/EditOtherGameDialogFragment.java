package com.liquid.poros.ui.fragment.dialog.user;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.liquid.base.adapter.CommonAdapter;
import com.liquid.base.adapter.CommonViewHolder;
import com.liquid.base.event.EventCenter;
import com.liquid.poros.R;
import com.liquid.poros.base.BaseDialogFragment;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.entity.EditGameBean;
import com.liquid.poros.utils.ScreenUtil;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import de.greenrobot.event.EventBus;

/**
 * 女嘉宾任务描述弹框
 */
public class EditOtherGameDialogFragment extends BaseDialogFragment {
    private CommonAdapter<EditGameBean.Item> mAdapter;
    private List<EditGameBean.Item> otherGames = new ArrayList<>();
    private String title;

    @Override
    protected String getPageId() {
        return TrackConstants.PAGE_TEST_EDIT_OTHER_GAME;
    }

    public static EditOtherGameDialogFragment newInstance() {
        EditOtherGameDialogFragment fragment = new EditOtherGameDialogFragment();
        return fragment;
    }

    public EditOtherGameDialogFragment setEditGameBeans(List<EditGameBean.Item> otherGames) {
        Gson gson = new Gson();
        this.otherGames = gson.fromJson(gson.toJson(otherGames), new TypeToken<List<EditGameBean.Item>>() {
        }.getType());
        return this;
    }

    public EditOtherGameDialogFragment setTitle(String title) {
        this.title = title;
        return this;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        Window win = getDialog().getWindow();
        // 一定要设置Background，如果不设置，window属性设置无效
        win.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams params = win.getAttributes();
        params.width = ScreenUtil.getDisplayWidth();
        params.gravity = Gravity.BOTTOM;
        win.setAttributes(params);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_edit_other_game_dialog, null);
        TextView tv_game_name = view.findViewById(R.id.tv_game_name);
        tv_game_name.setText(title);
        view.findViewById(R.id.ll_close_game).setOnClickListener(v -> {
            dismissDialog();
        });
        view.findViewById(R.id.save).setOnClickListener(v -> {
            EventBus.getDefault().post(new EventCenter(Constants.EVENT_CODE_UPDATE_GAME_INFO_OTHER, otherGames));
            dismissDialog();
        });
        RecyclerView rv_other_game = view.findViewById(R.id.rv_other_game);
        rv_other_game.setLayoutManager(new GridLayoutManager(getContext(), 3));
        mAdapter = new CommonAdapter<EditGameBean.Item>(getContext(), R.layout.item_edit_other_game) {
            @Override
            public void convert(CommonViewHolder holder, final EditGameBean.Item data, int position) {
                Glide.with(getContext()).load(data.image_url).into((ImageView) holder.getView(R.id.iv_other_game));
                holder.getView(R.id.item_other_game_line).setBackground(getContext().getDrawable(data.select ? R.drawable.bg_color_10d8c8_line_20 : R.drawable.bg_color_transparent_line_20));
                holder.setVisibility(R.id.item_iv_other_game_check, data.select ? View.VISIBLE : View.GONE);
                holder.setOnClickListener(R.id.layout, v -> {
                    data.select = !data.select;
                    notifyDataSetChanged();
                });
            }
        };
        mAdapter.update(otherGames);
        rv_other_game.setAdapter(mAdapter);
        builder.setView(view);
        AlertDialog alertDialog = builder.create();
        return alertDialog;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
