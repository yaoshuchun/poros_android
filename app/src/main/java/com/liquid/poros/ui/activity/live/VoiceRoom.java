package com.liquid.poros.ui.activity.live;

import com.liquid.im.kit.custom.LevelChangeAttachment;
import com.liquid.im.kit.custom.ScoreChangeAttachment;
import com.liquid.poros.entity.MsgUserModel;

import java.util.Map;

public abstract class VoiceRoom {

    abstract void initView();

    public abstract void updateRoomStatus(MsgUserModel.UserInfo cpInfo);

    public abstract void initLink(String sessionId,String roomId);

    public abstract void leaveRoom();

    public abstract void showCpCardDialog(boolean isAgreeCp, boolean isCpRoom, String roomId, String roomAnchor, String targetId, String cpOrderId, String cardCategory);

    public abstract void onRoomCreate(String teamImId, String tips, int style,String imVideoToken);

    public abstract void setMicMute(boolean mute);

    public abstract void onPause();

    public abstract void onResume();

    public abstract void onDestroy();

    public abstract void setIsInviteMic(boolean isInviteMic);

    public abstract boolean isOnMic();

    public abstract boolean isAvRunning();

    public abstract void closeConnection();

    public abstract String getCurrentTeamId();

    public abstract int getCurrentCpStatus();

    public abstract void onAudioVolume(Map<String, Integer> speakers);

    public abstract void showMicGuide();

    public abstract void updateScore(ScoreChangeAttachment attachment);

    public abstract void updateLevel(LevelChangeAttachment level);

    public abstract void updateLevelDialog(String level);

    public abstract void updateCpUpgradeDialog(MsgUserModel.P2PRankUpgradeAwardInfo info);

    public abstract void doTask();

    public abstract void setRoomId(String roomId);
}
