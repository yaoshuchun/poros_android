package com.liquid.poros.ui.fragment

import android.app.Dialog
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import com.liquid.poros.R
import com.liquid.poros.analytics.utils.MultiTracker
import com.liquid.poros.base.BaseDialogFragment
import com.liquid.poros.constant.TrackConstants
import com.liquid.poros.utils.AccountUtil
import com.liquid.poros.utils.ScreenUtil
import com.liquid.poros.utils.StringUtil
import java.util.HashMap

class AppraiseAnchorDialogfragment : BaseDialogFragment(), View.OnClickListener {

    var mAnchorId: String? = null
    var mRoomId: String? = null
    var mGuestId: String? = null
    var mChose: String = ""

    override fun getPageId(): String {
        return TrackConstants.PRO_APPRAISE_ANCHOR_STEP_1
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dialog!!.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onStart() {
        super.onStart()
        val win = dialog!!.window
        // 一定要设置Background，如果不设置，window属性设置无效
        win.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val dm = DisplayMetrics()
        requireActivity().windowManager.defaultDisplay.getMetrics(dm)
        val params = win.attributes
        params.gravity = Gravity.BOTTOM
        params.width = ScreenUtil.getDisplayWidth()
        win.attributes = params
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(requireActivity())
        val view = LayoutInflater.from(activity).inflate(R.layout.dialog_appraise_anchor, null)
        val negetive_btn = view.findViewById<TextView>(R.id.negetive_btn)
        negetive_btn.setOnClickListener(this)
        val positive_btn = view.findViewById<TextView>(R.id.positive_btn)
        positive_btn.setOnClickListener(this)
        updateExp()
        builder.setView(view)
        return builder.create()
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.negetive_btn -> {
                mChose = "not_good"
                AppraiseAnchorDialogfragmentStep2.newInstance().setPos(1).setAnchor(mAnchorId, mRoomId, mGuestId, "not_good").show(requireFragmentManager(), AppraiseAnchorDialogfragmentStep2::class.java.simpleName)
            }
            R.id.positive_btn -> {
                mChose = "good"
                AppraiseAnchorDialogfragmentStep2.newInstance().setPos(2).setAnchor(mAnchorId,mRoomId,mGuestId, "good").show(requireFragmentManager(), AppraiseAnchorDialogfragmentStep2::class.java.simpleName)
            }
        }
        dismissAllowingStateLoss()
    }

    fun setAnchor(anchor_id : String?, room_id: String?, guest_id: String?) : AppraiseAnchorDialogfragment{
        this.mAnchorId = anchor_id
        this.mRoomId = room_id
        this.mGuestId = guest_id
        return this
    }

    companion object {
        @JvmStatic
        fun newInstance(): AppraiseAnchorDialogfragment {
            return AppraiseAnchorDialogfragment()
        }
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        if(StringUtil.isEmpty(mChose))
        {
            updateAction("close")
        }else{
            updateAction(mChose)
        }
    }

    private fun updateExp() {
        val params = HashMap<String?, String?>()
        params["anchor_id"] = mAnchorId
        params["user_id"] = AccountUtil.getInstance().userId
        params["guest_id"] = mGuestId
        MultiTracker.onEvent(TrackConstants.B_UP_DOWN_MIC_EVALUATION_POPUP_EXPOSURE, params)
    }

    private fun updateAction(action: String) {
        val params = HashMap<String?, String?>()
        params["anchor_id"] = mAnchorId
        params["user_id"] = AccountUtil.getInstance().userId
        params["guest_id"] = mGuestId
        params["on_click_type"] = action
        MultiTracker.onEvent(TrackConstants.B_UP_DOWN_MIC_EVALUATION_POPUP_ONCLICK, params)
    }
}