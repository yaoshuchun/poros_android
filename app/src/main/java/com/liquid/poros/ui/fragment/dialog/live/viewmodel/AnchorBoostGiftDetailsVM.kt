package com.liquid.poros.ui.fragment.dialog.live.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.liquid.library.retrofitx.RetrofitX
import com.liquid.poros.entity.AnchorGiftHelpDetailsInfo
import com.liquid.poros.http.ApiUrl
import com.liquid.poros.http.observer.ObjectObserver
import com.liquid.poros.http.utils.postPoros

/**
 * 主持人助力 - 助力详情
 */
class AnchorBoostGiftDetailsVM : ViewModel() {
    val boostGiftInfoLiveData: MutableLiveData<AnchorGiftHelpDetailsInfo> by lazy { MutableLiveData<AnchorGiftHelpDetailsInfo>() }
    val boostGiftInfoFailed: MutableLiveData<AnchorGiftHelpDetailsInfo> by lazy { MutableLiveData<AnchorGiftHelpDetailsInfo>() }

    /**
     * 获取助力礼物详情
     */
    fun setBoostGift(roomId: String) {
        RetrofitX.url(ApiUrl.GIFT_BOOST_INFO)
                .param("room_id", roomId)
                .postPoros()
                .subscribe(object : ObjectObserver<AnchorGiftHelpDetailsInfo>() {
                    override fun success(result: AnchorGiftHelpDetailsInfo) {
                        boostGiftInfoLiveData.postValue(result)
                    }

                    override fun failed(result: AnchorGiftHelpDetailsInfo) {
                        boostGiftInfoFailed.postValue(result)
                    }
                })
    }
}