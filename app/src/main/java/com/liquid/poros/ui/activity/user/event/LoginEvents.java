package com.liquid.poros.ui.activity.user.event;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import androidx.annotation.StringDef;

/**
 * 登录相关
 *
 * @author yejiurui
 */
public class LoginEvents {

    @Retention(RetentionPolicy.SOURCE)
    @StringDef({LOGIN, LOGOUT, CANCEL})
    public @interface Event {
    }

    public static final String LOGIN = "login";
    public static final String LOGOUT = "logout";
    public static final String CANCEL = "cancel";

    @Event
    public String message;

    // h5页面跳转登录可能会带登录成功目标url
    public String h5TargetUrl;

    public LoginEvents(@Event String event) {
        this.message = event;
    }

    public LoginEvents(@Event String event, String h5TargetUrl) {
        this.message = event;
        this.h5TargetUrl = h5TargetUrl;
    }
}
