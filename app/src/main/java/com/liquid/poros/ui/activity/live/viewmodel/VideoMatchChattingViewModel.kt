package com.liquid.poros.ui.activity.live.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.liquid.library.retrofitx.RetrofitX
import com.liquid.poros.entity.BaseBean
import com.liquid.poros.entity.VideoMatchInfo
import com.liquid.poros.http.ApiUrl
import com.liquid.poros.http.observer.ObjectObserver
import com.liquid.poros.http.utils.postPoros

/**
 * 视频速配ViewModel
 */
class VideoMatchChattingViewModel : ViewModel() {
    val cancelVideoMatchMicLiveDataSuccess: MutableLiveData<BaseBean> by lazy { MutableLiveData<BaseBean>() } //男用户下麦成功
    val cancelVideoMatchMicLiveDataFail: MutableLiveData<BaseBean> by lazy { MutableLiveData<BaseBean>() } //男用户下麦失败
    val requestVideoMatchMicLiveDataSuccess: MutableLiveData<VideoMatchInfo> by lazy { MutableLiveData<VideoMatchInfo>() } //男用户上麦成功
    val requestVideoMatchMicLiveDataFail: MutableLiveData<VideoMatchInfo> by lazy { MutableLiveData<VideoMatchInfo>() } //男用户上麦失败
    val refuseVideoMatchMicLiveDataSuccess: MutableLiveData<BaseBean> by lazy { MutableLiveData<BaseBean>() } //男用户拒绝成功
    val refuseVideoMatchMicLiveDataFail: MutableLiveData<BaseBean> by lazy { MutableLiveData<BaseBean>() } //男用户拒绝失败
    /**
     * 视频速配接口
     */
    fun cancelVideoMatchMic(liveId: String, close_reason: String) {
        RetrofitX.url(ApiUrl.CANCEL_VIDEO_MATCH_MIC)
                .param("live_id", liveId)
                .param("close_reson", close_reason)
                .postPoros().subscribe(object : ObjectObserver<BaseBean>() {
                    override fun success(result: BaseBean) {
                        cancelVideoMatchMicLiveDataSuccess.postValue(result)
                    }

                    override fun failed(result: BaseBean) {
                        cancelVideoMatchMicLiveDataFail.postValue(result)
                    }

                })
    }
    /**
     * 请求上麦
     */
    fun requestVideoMatchMic(user_id: String?,live_id: String?, mic_id: String?) {
        RetrofitX.url(ApiUrl.REQUEST_VIDEO_MATCH_MIC)
                .param("user_id", user_id)
                .param("live_id", live_id)
                .param("mic_id", mic_id)
                .postPoros().subscribe(object : ObjectObserver<VideoMatchInfo>() {
                    override fun success(result: VideoMatchInfo) {

                        requestVideoMatchMicLiveDataSuccess.postValue(result)
                    }

                    override fun failed(result: VideoMatchInfo) {
                        requestVideoMatchMicLiveDataFail.postValue(result)
                    }

                })
    }
    /**
     * 拒绝上麦接口
     */
    fun refuseVideoMatchMic(live_id: String, mic_id: String) {
        RetrofitX.url(ApiUrl.REFUSE_VIDEO_MATCH_MIC)
                .param("live_id", live_id)
                .param("mic_id", mic_id)
                .postPoros().subscribe(object : ObjectObserver<BaseBean>() {
                    override fun success(result: BaseBean) {
                        refuseVideoMatchMicLiveDataSuccess.postValue(result)
                    }

                    override fun failed(result: BaseBean) {
                        refuseVideoMatchMicLiveDataFail.postValue(result)
                    }

                })
    }
}