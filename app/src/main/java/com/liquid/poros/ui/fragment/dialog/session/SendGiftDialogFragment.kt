package com.liquid.poros.ui.fragment.dialog.session

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RadioButton
import com.liquid.poros.R
import com.liquid.poros.base.BaseDialogFragment
import com.liquid.poros.constant.TrackConstants

/**
 * 私聊页面赠送礼物弹窗
 */
class SendGiftDialogFragment : BaseDialogFragment() {
    override fun getPageId(): String {
        return TrackConstants.PAGE_DIALOG_SEND_GIFT
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return View.inflate(context, R.layout.fragment_send_gift_dialog, container)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }

    /**
     * 创建单选按钮
     */
    private fun createRadio(content: String) {
        val radio = RadioButton(context);
        radio.text = content
        radio.setButtonDrawable(R.drawable.ic_send_gift_selector)
        radio.setTextColor(context?.resources?.getColorStateList(R.color.send_gift_radio_text_selector))
        radio.textSize = 14f;
    }

    /**
     * 创建分隔View
     */
    private fun createSeparate() {
        val view = View(context)
        val layoutParams = LinearLayout.LayoutParams(0, 1)
        layoutParams.weight = 1f
        view.layoutParams = layoutParams
    }
}