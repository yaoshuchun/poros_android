package com.liquid.poros.ui.floatball;

public interface onExpandClickListener {
        /**
         * 0 点击来自FloatMenu  1 点击来自FloatMessage
         *
         * @param form
         */
        void onExpandClick(int form);
    }