package com.liquid.poros.ui.activity.account

import android.annotation.SuppressLint
import android.graphics.Rect
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ItemDecoration
import com.liquid.base.adapter.CommonAdapter
import com.liquid.base.adapter.CommonViewHolder
import com.liquid.base.event.EventCenter
import com.liquid.base.utils.ToastUtil
import com.liquid.poros.R
import com.liquid.poros.analytics.utils.MultiTracker
import com.liquid.poros.base.BaseActivity
import com.liquid.poros.constant.Constants
import com.liquid.poros.constant.TrackConstants
import com.liquid.poros.entity.BaseBean
import com.liquid.poros.entity.WithdrawalInfo
import com.liquid.poros.entity.WithdrawalInfo.CandyInfoBean
import com.liquid.poros.entity.WithdrawalInfo.CashInfoBean
import com.liquid.poros.entity.WithdrawalInfo.CashInfoBean.WithdrawBalanceBean
import com.liquid.poros.helper.JinquanResourceHelper
import com.liquid.poros.ui.activity.account.viewmodel.WithdrawalViewModel
import com.liquid.poros.ui.fragment.dialog.account.SubmitWithdrawalInfoFragment
import com.liquid.poros.ui.fragment.dialog.account.SubmitWithdrawalInfoFragment.Companion.newInstance
import com.liquid.poros.ui.fragment.dialog.common.CommonActionListener
import com.liquid.poros.ui.fragment.dialog.common.CommonDialogBuilder
import com.liquid.poros.ui.fragment.dialog.common.CommonDialogFragment
import com.liquid.poros.utils.NumberFormatUtil
import com.liquid.poros.utils.ScreenUtil
import com.liquid.poros.utils.StringUtil
import com.liquid.poros.utils.ViewUtils
import com.liquid.poros.widgets.WrapContentGridLayoutManager
import com.liquid.poros.wxapi.WXUtils
import com.tencent.mm.opensdk.modelmsg.SendAuth
import de.greenrobot.event.EventBus
import kotlinx.android.synthetic.main.activity_withdrawal.*
import java.math.BigDecimal
import java.util.*

/**
 * 提现
 */
class WithdrawalActivity : BaseActivity(),View.OnClickListener{
    private var mCommonAdapter: CommonAdapter<WithdrawBalanceBean>? = null
    private var selectWithdrawalPos = 0

    //区分提现类型,true余额提现,反之糖果提现
    private var withdrawalType = false
    private var withdrawalViewModel: WithdrawalViewModel? = null

    //是否实名认证  true 认证
    private var isCertification = false

    //是否绑定微信 true 绑定
    private var isBindWeChat = false

    //余额提现
    var WITHDRAWAL_CASH = "cash"

    //糖果提现
    var WITHDRAWAL_CANDY = "candy"
    var LOGIN_FROM_WECHAT = "wechat"
    var LOGIN_FROM_QQ = "qq"

    //现金余额 = 0
    var myBalance = 0

    //糖果转化余额
    var myCandyToCash: String? = null

    override fun parseBundleExtras(extras: Bundle) {
        withdrawalType = extras.getBoolean(Constants.START_WITHDRAWAL_TYPE, true)
    }

    override fun getContentViewLayoutID(): Int {
        return R.layout.activity_withdrawal
    }

    override fun getPageId(): String {
        return TrackConstants.PAGE_WITHDRAWAL
    }

    override fun needSetTransparent(): Boolean {
        return false
    }

    override fun trackWithDrawType(): String {
        return if (withdrawalType) WITHDRAWAL_CASH else WITHDRAWAL_CANDY
    }

    @SuppressLint("ResourceType")
    override fun initViewsAndEvents(savedInstanceState: Bundle?) {
        group_balance!!.visibility = if (withdrawalType) View.VISIBLE else View.GONE
        cl_withdrawal_gift!!.visibility = if (withdrawalType) View.GONE else View.VISIBLE
        rv_withdrawal_balance!!.layoutManager = WrapContentGridLayoutManager(this, 3)
        rv_withdrawal_balance!!.addItemDecoration(object : ItemDecoration() {
            override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
                super.getItemOffsets(outRect, view, parent, state)
                val layoutParams = view.layoutParams as GridLayoutManager.LayoutParams
                val spanIndex = layoutParams.spanIndex
                outRect.top = ScreenUtil.dip2px(8f)
                when (spanIndex) {
                    0 -> {
                        outRect.left = ScreenUtil.dip2px(12f)
                        outRect.right = ScreenUtil.dip2px(4f)
                    }
                    1 -> {
                        outRect.left = ScreenUtil.dip2px(4f)
                        outRect.right = ScreenUtil.dip2px(4f)
                    }
                    else -> {
                        outRect.left = ScreenUtil.dip2px(4f)
                        outRect.right = ScreenUtil.dip2px(12f)
                    }
                }
            }
        })
        mCommonAdapter = object : CommonAdapter<WithdrawBalanceBean>(this, R.layout.item_withdrawal) {
            override fun convert(holder: CommonViewHolder, withdrawBalanceBean: WithdrawBalanceBean, pos: Int) {
                holder.setText(R.id.item_withdrawal, String.format(getString(R.string.withdrawal_balance_count), withdrawBalanceBean.cash))
                if (selectWithdrawalPos == pos) {
                    holder.setTextColor(R.id.item_withdrawal, resources.getColor(R.color.white))
                    holder.setBackgroundResource(R.id.item_withdrawal, R.drawable.bg_gradual_14d4d6_5)
                } else {
                    JinquanResourceHelper.getInstance(this@WithdrawalActivity).setTextColorByAttr(holder.getView(R.id.item_withdrawal), R.attr.custom_attr_unknown_text_color)
                    JinquanResourceHelper.getInstance(this@WithdrawalActivity).setBackgroundResourceByAttr(holder.getView(R.id.item_withdrawal), R.attr.bg_test_dialog_game_item)
                }
                holder.getView<View>(R.id.item_withdrawal).setOnClickListener(View.OnClickListener {
                    selectWithdrawalPos = pos
                    notifyDataSetChanged()
                })
            }
        }
        rv_withdrawal_balance!!.adapter = mCommonAdapter
        withdrawalViewModel = getActivityViewModel(WithdrawalViewModel::class.java)
        withdrawalViewModel?.getWithdrawalInfo(if (withdrawalType) WITHDRAWAL_CASH else WITHDRAWAL_CANDY)
        initClickListener()
        initObserveData()
    }

    private fun initObserveData(){
        withdrawalViewModel?.withdrawalInfoSuccess?.observe(this, Observer { withdrawalInfo: WithdrawalInfo ->
            //提现说明
            tv_withdrawal_tip_content!!.text = withdrawalInfo.explanation
            syncRealNameStatus(withdrawalInfo.certification)
            syncBindWechatStatus(withdrawalInfo.is_bind_wechat, withdrawalInfo.login_from)
            if (withdrawalInfo.cash_info != null) {
                myBalance = withdrawalInfo.cash_info!!.cash_balance
                setBalanceData(withdrawalInfo.cash_info)
            }
            if (withdrawalInfo.candy_info != null) {
                setCandyData(withdrawalInfo.candy_info)
            }
        })
        //提现
        withdrawalViewModel!!.userWithdrawalSuccess.observe(this, Observer { baseBean: BaseBean? ->
            //弹窗提示
            startWithdrawSuccess()
            //刷新余额/糖果,同步检验
            withdrawalViewModel!!.getWithdrawalInfo(if (withdrawalType) WITHDRAWAL_CASH else WITHDRAWAL_CANDY)
            EventBus.getDefault().post(EventCenter<Any?>(Constants.EVENT_CODE_WITHDRAW_SUCCESS))
        })
        withdrawalViewModel!!.userWithdrawalFail.observe(this, Observer { baseBean: BaseBean ->
            if ((baseBean.code == 2005) || (baseBean.code == 2035) || (baseBean.code == 2019)) {
                //2005:未绑定真实姓名&身份证号;
                //2035:未绑定手机号
                //2019:未绑定微信
                ToastUtil.show(getString(R.string.withdrawal_not_authentication_tip))
            } else {
                ToastUtil.show(baseBean.msg)
            }
        })
        //绑定微信
        withdrawalViewModel!!.withdrawalBingWechatSuccess.observe(this, Observer { baseBean: BaseBean ->
            syncBindWechatStatus(true, LOGIN_FROM_QQ)
            ToastUtil.show(baseBean.msg)
            val wechatParams: HashMap<String?, String?> = HashMap()
            wechatParams.put("type", if (withdrawalType) WITHDRAWAL_CASH else WITHDRAWAL_CANDY)
            MultiTracker.onEvent(TrackConstants.B_BIND_WECHAT_SUCCESS, wechatParams)
        })
        withdrawalViewModel!!.withdrawalBingWechatFail.observe(this, Observer { baseBean: BaseBean? -> syncBindWechatStatus(false, LOGIN_FROM_QQ) })
    }

    //余额提现
    private fun setBalanceData(cashInfoBean: CashInfoBean?) {
        tv_withdrawal_balance!!.text = StringUtil.matcherSearchText(true,"${NumberFormatUtil.balanceFormat(cashInfoBean!!.cash_balance.toLong())}元", "元")
        mCommonAdapter!!.update(cashInfoBean.withdraw_denomination_info_list)
    }

    //糖果提现
    private fun setCandyData(candyData: CandyInfoBean?) {
        if (StringUtil.isEmpty(candyData!!.candy_count) || StringUtil.isEmpty(candyData.candy_to_cash)) {
            return
        }
        myCandyToCash = candyData.candy_to_cash
        tv_withdrawal_gift_number!!.text = StringUtil.matcherSearchText(true, "${candyData.candy_count}个", "个")
        tv_withdrawal_balance_gift!!.text = StringUtil.matcherSearchText(true, "${candyData.candy_to_cash}元", "元")
    }

    //实名认证状态同步
    private fun syncRealNameStatus(isRealName: Boolean) {
        isCertification = isRealName
        tv_real_name!!.text = if (isCertification) "已认证" else "未认证"
    }

    /**
     * 微信绑定状态同步
     * @param loginFrom 登录来源
     */
    private fun syncBindWechatStatus(isBind: Boolean, loginFrom: String?) {
        isBindWeChat = isBind
        tv_bind_wechat!!.text = if (isBindWeChat) if ((LOGIN_FROM_WECHAT == loginFrom)) "已绑定当前登录微信" else "已绑定" else "未绑定"
    }

    private fun initClickListener() {
        iv_left.setOnClickListener(this)
        tv_recharge_details.setOnClickListener(this)
        tv_real_name.setOnClickListener(this)
        tv_bind_wechat.setOnClickListener(this)
        tv_withdrawal.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        if (ViewUtils.isFastClick()) {
            return
        }
        when (view.id) {
            R.id.iv_left -> finish()
            R.id.tv_recharge_details ->
                //提现记录
                readyGo(WithdrawalHistoryActivity::class.java)
            R.id.tv_real_name -> {
                //实名认证
                if (isCertification) {
                    return
                }
                newInstance().show(supportFragmentManager, SubmitWithdrawalInfoFragment::class.java.simpleName)
            }
            R.id.tv_bind_wechat -> {
                //绑定微信
                if (isBindWeChat) {
                    return
                }
                bindWechat()
                val wechatParams = HashMap<String?, String?>()
                wechatParams["type"] = if (withdrawalType) WITHDRAWAL_CASH else WITHDRAWAL_CANDY
                MultiTracker.onEvent(TrackConstants.B_BIND_WECHAT_ALERT_EXPOSURE, wechatParams)
            }
            R.id.tv_withdrawal -> {
                trackWithdraw()
                if (withdrawalType) {
                    val bigDecimal1 = BigDecimal(NumberFormatUtil.balanceFormat(myBalance.toLong()))
                    val bigDecimal2 = BigDecimal(mCommonAdapter!!.datas[selectWithdrawalPos].cash)
                    val coinCode = bigDecimal1.compareTo(bigDecimal2)
                    if (coinCode < 0) {
                        ToastUtil.show(getString(R.string.withdrawal_balance_not_wnough))
                        return
                    }
                }
                withdrawalViewModel!!.userWithDraw(if (withdrawalType) WITHDRAWAL_CASH else WITHDRAWAL_CANDY)
            }
        }
    }

    private fun trackWithdraw() {
        val params = HashMap<String?, String?>()
        params["type"] = if (withdrawalType) WITHDRAWAL_CASH else WITHDRAWAL_CANDY
        params["amount"] = if (withdrawalType) myBalance.toString() else myCandyToCash
        MultiTracker.onEvent(TrackConstants.B_WITHDRAW_BUTTON_CLICK, params)
    }

    /**
     * 发起提现成功,弹窗提示
     */
    private fun startWithdrawSuccess() {
        //立即提现
        CommonDialogBuilder()
                .setImageTitleRes(R.mipmap.icon_start_withdrawal)
                .setDesc(getString(R.string.withdrawal_start_tip))
                .setCancel(getString(R.string.cancel))
                .setConfirm(getString(R.string.withdrawal_keep))
                .setListener(object : CommonActionListener {
                    override fun onConfirm() {}
                    override fun onCancel() {}
                }).create().show(supportFragmentManager, CommonDialogFragment::class.java.simpleName)
        //埋点
        val params = HashMap<String?, String?>()
        params["type"] = if (withdrawalType) WITHDRAWAL_CASH else WITHDRAWAL_CANDY
        params["amount"] = if (withdrawalType) myBalance.toString() else myCandyToCash
        MultiTracker.onEvent(TrackConstants.B_WITHDRAW_SUCCESS_ALERT_EXPOSURE, params)
    }

    override fun onEventComing(eventCenter: EventCenter<*>) {
        super.onEventComing(eventCenter)
        when (eventCenter.eventCode) {
            Constants.EVENT_CODE_REAL_NAME_SUCCESS -> {
                //实名认证成功
                syncRealNameStatus(true)
                val params = HashMap<String?, String?>()
                params["type"] = if (withdrawalType) WITHDRAWAL_CASH else WITHDRAWAL_CANDY
                MultiTracker.onEvent(TrackConstants.B_VERIFIED_SUCCESS, params)
            }
            Constants.EVENT_CODE_WECHAT_AUTH_FINISH -> {
                //微信回传成功 - 绑定微信
                val code = eventCenter.data as String
                withdrawalViewModel!!.bindWechat(code)
            }
        }
    }

    //绑定微信
    private fun bindWechat() {
        CommonDialogBuilder()
                .setTitle(getString(R.string.withdrawal_bind_wechat_title))
                .setWithCancel(false)
                .setConfirm(getString(R.string.withdrawal_bind_wechat_tip))
                .setListener(object : CommonActionListener {
                    override fun onConfirm() {
                        val req = SendAuth.Req()
                        req.scope = "snsapi_userinfo"
                        req.state = "state" + System.currentTimeMillis()
                        if (WXUtils.getInstance(this@WithdrawalActivity).iwxApi.isWXAppInstalled) {
                            WXUtils.getInstance(this@WithdrawalActivity).iwxApi.sendReq(req)
                        } else {
                            ToastUtil.show(getString(R.string.wechat_not_install_tip))
                        }
                    }

                    override fun onCancel() {}
                }).create().show(supportFragmentManager, CommonDialogFragment::class.java.simpleName)
    }
}