package com.liquid.poros.ui.activity.live

import android.graphics.Color
import android.media.AudioManager
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import com.liquid.base.event.EventCenter
import com.liquid.base.utils.ToastUtil
import com.liquid.im.kit.custom.MinuteChangeAttachment
import com.liquid.im.kit.custom.RoomActionAttachment
import com.liquid.library.retrofitx.utils.LogUtils
import com.liquid.poros.R
import com.liquid.poros.analytics.utils.MultiTracker
import com.liquid.poros.base.BaseActivity
import com.liquid.poros.constant.Constants
import com.liquid.poros.constant.CoupleVoiceAction
import com.liquid.poros.constant.TrackConstants
import com.liquid.poros.entity.SoloCallInfo
import com.liquid.poros.entity.SoloVideoInfo
import com.liquid.poros.entity.VideoMatchInfo
import com.liquid.poros.helper.AvHelper
import com.liquid.poros.helper.LiveRoomListener
import com.liquid.poros.sdk.HeartBeatUtils
import com.liquid.poros.ui.activity.live.viewmodel.VideoMatchChattingViewModel
import com.liquid.poros.ui.dialog.RechargeCoinDialog
import com.liquid.poros.ui.fragment.dialog.common.CommonActionListener
import com.liquid.poros.ui.fragment.dialog.common.CommonDialogBuilder
import com.liquid.poros.ui.fragment.dialog.common.CommonDialogFragment
import com.liquid.poros.utils.AccountUtil
import com.liquid.poros.utils.ScreenUtil
import com.liquid.poros.utils.StringUtil
import com.netease.lava.nertc.sdk.NERtcEx
import com.netease.lava.nertc.sdk.video.NERtcRemoteVideoStreamType
import com.netease.lava.nertc.sdk.video.NERtcVideoView
import com.netease.nimlib.sdk.NIMClient
import com.netease.nimlib.sdk.RequestCallback
import com.netease.nimlib.sdk.chatroom.ChatRoomService
import com.netease.nimlib.sdk.chatroom.ChatRoomServiceObserver
import com.netease.nimlib.sdk.chatroom.model.ChatRoomInfo
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage
import com.netease.nimlib.sdk.chatroom.model.EnterChatRoomData
import com.netease.nimlib.sdk.chatroom.model.EnterChatRoomResultData
import de.greenrobot.event.Subscribe
import de.greenrobot.event.ThreadMode
import kotlinx.android.synthetic.main.activity_video_match_chatting.*
import kotlinx.android.synthetic.main.layout_video_chat_auto_call.*
import kotlinx.android.synthetic.main.layout_video_chat_auto_call.tv_vfd_chat_duration
import kotlinx.android.synthetic.main.layout_video_chat_bottom.*
import java.util.*

/**
 * 视频速配聊天中
 */
class VideoMatchChattingActivity : BaseActivity() {
    private var mMediaPlayer: MediaPlayer? = null
    private var videoChannel: String? = null
    private var imageUrl: String? = null

    //fast_match : 速配
    //single_click : 定向视频（列表点击跳转）
    //1v1 : 1v1视频（定时自动触发）
    private var fromPage = Constants.ENTER_VIDEO_MATCH_FAST_DATING
    private var dialog: CommonDialogFragment? = null
    private var hint: String? = null
    private var liveId: String? = null
    private var micId: String? = null
    private var avatarUrl: String? = null
    private var videoUrl: String? = null
    private var nickName: String? = null
    private var femaleGuestId: String? = null
    private var liveInfo: VideoMatchInfo.LiveFemaleGuestInfo? = null
    private var matchInfo: VideoMatchInfo? = null
    private var femaleGuestInfo: SoloVideoInfo? = null
    private var callInfo: SoloCallInfo? = null
    private val videoRoomListener: LiveRoomListener by lazy { VideoRoomListener() }
    private var videoMatchChattingViewModel: VideoMatchChattingViewModel? = null
    private var roomInfo: ChatRoomInfo? = null
    private var roomId: String? = null
    private var mTask: TimerTask? = null
    private var mTimer: Timer? = null
    private var startTime: Long = 0
    private var isConnecting: Boolean = true
    var mAvHelper: AvHelper? = null
    private val mIncomingMsgObserver: MsgObserver by lazy { MsgObserver() }

    //速配来源
    companion object {
        private const val MSG: Int = 1
        private const val CLOSE_REASON_JOIN_ROOM_FAIL: Int = 0//加入房间失败
        private const val CLOSE_REASON_MALE_USER_CLOSE: Int = 2//男用户挂断视频
        private const val CLOSE_REASON_FEMALE_USER_CLOSE: Int = 3//女嘉宾挂断视频
        private const val CLOSE_REASON_STOP_ORDER: Int = 11//停止接单
        private const val CLOSE_REASON_COIN_NOT_ENOUGH: Int = 14//金币不足
        private const val CLOSE_REASON_FEMALE_DISCONNECTED: Int = 16//女方断网
    }

    override fun parseBundleExtras(extras: Bundle) {
        fromPage = extras.getString(Constants.ENTER_VIDEO_MATCH_SOURCE)
        when (fromPage) {
            Constants.ENTER_VIDEO_MATCH_FAST_DATING -> {
                matchInfo =
                    extras.getSerializable(Constants.VIDEO_MATCH_DATA_PASS_KEY) as VideoMatchInfo
                roomId = matchInfo?.room_id
                liveId = matchInfo?.live_id
                liveInfo = matchInfo?.live_info
                avatarUrl = liveInfo?.avatar_url
                nickName = liveInfo?.nick_name
                videoChannel = "random_match"
            }
            Constants.ENTER_VIDEO_MATCH_1V1 -> {
                callInfo =
                    extras.getSerializable(Constants.VIDEO_MATCH_DATA_PASS_KEY) as SoloCallInfo
                avatarUrl = callInfo?.avatar_url
                nickName = callInfo?.nick_name
                liveId = callInfo?.live_id
                micId = callInfo?.mic_id
                videoUrl = callInfo?.video_url
                var games: Array<String> = emptyArray()
                liveInfo = VideoMatchInfo.LiveFemaleGuestInfo(
                    callInfo?.user_id,
                    callInfo?.avatar_url,
                    callInfo?.nick_name,
                    0,
                    28,
                    games.asList(),
                    callInfo?.image_url
                )
                imageUrl = callInfo?.image_url
                videoChannel = "video_call"
            }
            Constants.ENTER_VIDEO_MATCH_SINGLE_CLICK -> {
                matchInfo =
                    extras.getSerializable(Constants.VIDEO_MATCH_DATA_PASS_KEY) as VideoMatchInfo
                if (matchInfo?.code == 19001) {//金币不足
                    hint = matchInfo?.hint
                }
                roomId = matchInfo?.room_id
                liveId = matchInfo?.live_id
                liveInfo = matchInfo?.live_info
                avatarUrl = liveInfo?.avatar_url
                nickName = liveInfo?.nick_name
                imageUrl = liveInfo?.image_url
                femaleGuestId = liveInfo?.anchor_id

                videoChannel = "user_active_call"
            }
            else -> {

            }
        }

    }

    override fun getPageId(): String {
        return TrackConstants.PAGE_VIDEO_MATCH_CHATTING
    }

    override fun getContentViewLayoutID(): Int {
        return R.layout.activity_video_match_chatting
    }

    override fun initViewsAndEvents(savedInstanceState: Bundle?) {
        videoMatchChattingViewModel = getActivityViewModel(VideoMatchChattingViewModel::class.java)
        //视频预览
        when (fromPage) {
            Constants.ENTER_VIDEO_MATCH_1V1 -> {
                tv_connecting.text = "想和你视频聊天"
                view_stub_bottom?.layoutResource = R.layout.layout_video_chat_auto_call
                startCountTime()
                trackVideoCallExposure()
                playMatchingMedia()
            }
            Constants.ENTER_VIDEO_MATCH_SINGLE_CLICK -> {
                view_stub_bottom?.layoutResource = R.layout.layout_video_chat_bottom
                playMatchingMedia()
            }
            else -> {
                view_stub_bottom?.layoutResource = R.layout.layout_video_chat_bottom
            }
        }
        if (StringUtil.isEquals(fromPage, Constants.ENTER_VIDEO_MATCH_1V1)) {
            tv_connecting.text = "想和你视频聊天"
            view_stub_bottom?.layoutResource = R.layout.layout_video_chat_auto_call
            startCountTime()
        } else {
            view_stub_bottom?.layoutResource = R.layout.layout_video_chat_bottom
        }
        try {
            group_video_chat.visibility = View.VISIBLE

        } catch (th: Throwable) {
            th.printStackTrace()
        }

        avatarUrl?.let { it ->
            Glide.with(this)
                .load(it)
                .circleCrop()
                .into(iv_vdf_avatar)
        }
        tv_dvf_nickname.text = nickName
        if (StringUtil.isNotEmpty(imageUrl)) {
            Glide.with(this)
                .load(imageUrl)
                .placeholder(R.drawable.iv_video_chat)
                .into(iv_female_guest)
        }
        iv_female_guest.visibility = View.VISIBLE
        vv_connecting.visibility = View.GONE

        if (StringUtil.isNotEmpty(videoUrl)) {
            iv_female_guest.visibility = View.GONE
            vv_connecting?.visibility = View.VISIBLE
            vv_connecting?.setVideoURI(Uri.parse(videoUrl))
            vv_connecting?.requestFocus()
            val onPreparedListener: MediaPlayer.OnPreparedListener =
                MediaPlayer.OnPreparedListener {
                    it.start()
                    it.isLooping = true
                    it.setVolume(0f, 0f)
                }
            vv_connecting?.setOnPreparedListener(onPreparedListener)
            vv_connecting?.start()
        }
        roomId?.let { startPreview(it) }
        NIMClient.getService(ChatRoomServiceObserver::class.java)
            .observeReceiveMessage(mIncomingMsgObserver, false)
        NIMClient.getService(ChatRoomServiceObserver::class.java)
            .observeReceiveMessage(mIncomingMsgObserver, true)
        NIMClient.getService(ChatRoomService::class.java)
            .enterChatRoomEx(EnterChatRoomData(roomId), 10)
            .setCallback(object : RequestCallback<EnterChatRoomResultData> {
                override fun onSuccess(result: EnterChatRoomResultData) {
                    if (isDestroyed || isFinishing) {
                        return
                    }
                    roomInfo = result.roomInfo
                    if (roomInfo == null) {
                        finish()
                        return
                    }
                }

                override fun onFailed(code: Int) {}
                override fun onException(exception: Throwable) {}
            })
        initObserver()
        initListener()
        trackVideoMatchLoading()
        video_render_myself.setZOrderOnTop(true)
        hint?.let {
            showChargeDialogV2(it)
        }
    }

    override fun onPause() {
        super.onPause()
        if (StringUtil.isEquals(fromPage, Constants.ENTER_VIDEO_MATCH_SINGLE_CLICK)) return
        if (StringUtil.isEquals(fromPage, Constants.ENTER_VIDEO_MATCH_1V1)) return
        if (isConnecting) {
            leaveRoom(CLOSE_REASON_MALE_USER_CLOSE)
        }
    }

    override fun isShowMsgTip(): Boolean {
        return false
    }

    private fun initObserver() {
        videoMatchChattingViewModel?.cancelVideoMatchMicLiveDataSuccess?.observe(
            this,
            androidx.lifecycle.Observer {
                HeartBeatUtils.addHeartBeatParams(mapOf("is_video_chatting" to false))
                HeartBeatUtils.removeHeartBeatParams("live_id")
                LogUtils.d("下麦成功")
                roomInfo = null
            })
        videoMatchChattingViewModel?.cancelVideoMatchMicLiveDataFail?.observe(
            this,
            androidx.lifecycle.Observer {
                LogUtils.d("取消匹配接口请求失败，下麦操作")
                roomInfo = null
            })
        videoMatchChattingViewModel?.requestVideoMatchMicLiveDataSuccess?.observe(
            this,
            androidx.lifecycle.Observer { videoMatchInfo ->
                stopCountTime()
                liveInfo = videoMatchInfo?.live_info
                liveId = videoMatchInfo?.live_id
                videoMatchInfo?.room_id?.let {
                    try {
                        startPreview(it)
                        NIMClient.getService(ChatRoomServiceObserver::class.java)
                            .observeReceiveMessage(mIncomingMsgObserver, true)
                        NIMClient.getService(ChatRoomService::class.java)
                            .enterChatRoomEx(EnterChatRoomData(it), 10)
                            .setCallback(object : RequestCallback<EnterChatRoomResultData> {
                                override fun onSuccess(result: EnterChatRoomResultData) {
                                    if (isDestroyed || isFinishing) {
                                        return
                                    }
                                    roomInfo = result.roomInfo
                                    if (roomInfo == null) {
                                        finish()
                                        return
                                    }
                                }

                                override fun onFailed(code: Int) {}
                                override fun onException(exception: Throwable) {}
                            })
                    } catch (t: Throwable) {
                        t.printStackTrace()
                    }
                }
            })
        videoMatchChattingViewModel?.requestVideoMatchMicLiveDataFail?.observe(
            this,
            androidx.lifecycle.Observer {
                when (it.code) {
                    //金币不足
                    19001 -> {
                        ToastUtil.show("金币不足")
                        showChargeDialogV2(it.hint)
                        tv_accept?.isClickable = true
                    }
                    19017 -> showToastAndFinish(it.msg)
                    19018 -> showToastAndFinish(it.msg)
                    else -> showToastAndFinish(it.msg)

                }
            })
        videoMatchChattingViewModel?.refuseVideoMatchMicLiveDataSuccess?.observe(
            this,
            androidx.lifecycle.Observer {
                finish()
            })
        videoMatchChattingViewModel?.refuseVideoMatchMicLiveDataFail?.observe(
            this,
            androidx.lifecycle.Observer {
                finish()
            })
    }

    private fun showToastAndFinish(msg: String?) {
        ToastUtil.show(msg)
        finish()
    }

    private fun initListener() {
        tv_vfd_cancel?.setOnClickListener {
            showHangUpDialog(CLOSE_REASON_MALE_USER_CLOSE, true)
            if (fromPage == Constants.ENTER_VIDEO_MATCH_SINGLE_CLICK) {
                val params: MutableMap<String, String?> = HashMap()
                params["guest_id"] = liveInfo?.anchor_id
                params["video_channel"] = videoChannel
                MultiTracker.onEvent(TrackConstants.B_VIDEO_CALLING_HANG_UP_CLICK, params)
            } else {
                trackVideoMatchHangUpExposure(true)
            }
        }

        if (StringUtil.isEquals(fromPage, Constants.ENTER_VIDEO_MATCH_1V1)) {
            tv_accept.setOnClickListener {
                videoMatchChattingViewModel?.requestVideoMatchMic(
                    user_id = null,
                    live_id = liveId,
                    mic_id = micId
                )
                tv_accept.isClickable = false
                trackVideoCallAnswerClick()
            }
            tv_hang_up.setOnClickListener {
                liveId?.let {
                    //挂断
                    videoMatchChattingViewModel?.refuseVideoMatchMic(live_id = it, mic_id = "")
                    trackVideoCallHangUpClick(Constants.VIDEO_CALL_ACTIVE_HANG_UP)
                }
            }
        }

        tv_chatting_cancel.setOnClickListener {
            showHangUpDialog(CLOSE_REASON_MALE_USER_CLOSE, false)
            trackVideoMatchHangUpExposure(false)
        }
        cl_container.setOnClickListener {
            if (!isConnecting) {
                hindTwoSecondView()
                group_chatting.visibility = View.VISIBLE
            }
        }
    }

    override fun onBackPressed() {
        showHangUpDialog(CLOSE_REASON_MALE_USER_CLOSE, isConnecting)
    }

    private fun startPreview(im_room_id: String) {
        //1.导包，2.init
        AccountUtil.getNERtcToken { rtcToken ->
            mAvHelper = AvHelper(im_room_id, rtcToken, false, videoRoomListener)
//            NERtcEx.getInstance().enableLocalVideo(true)
//            video_render_other.visibility = View.VISIBLE
            mAvHelper!!.joinAvRoom(this, video_render_other)
        }
    }

    var countDownTimer: CountDownTimer? = null

    /**
     * 开始倒计时
     */
    private fun startCountTime() {
        if (countDownTimer == null) {
            countDownTimer = object : CountDownTimer(15 * 1000, 1000) {
                override fun onTick(millisUntilFinished: Long) {
                    if (tv_vfd_chat_duration != null) {
                        tv_vfd_chat_duration.text =
                            getString(R.string.video_call_tips, millisUntilFinished / 1000)
                    }
                }

                override fun onFinish() {
                    if (isConnecting) {
                        trackVideoCallHangUpClick(Constants.VIDEO_CALL_SYSTEM)
                        leaveRoom(CLOSE_REASON_STOP_ORDER)
                    }
                }
            }
            countDownTimer!!.start()
        }
    }

    /**
     * 停止倒计时
     */
    private fun stopCountTime() {
        if (countDownTimer != null) {
            countDownTimer!!.cancel()
            countDownTimer = null
        }
    }

    private fun showHangUpDialog(reason: Int, isVideoMatchLoading: Boolean) {
        CommonDialogBuilder()
            .setImageTitleRes(R.mipmap.icon_close_video_match)
            .setDesc(getString(R.string.video_match_close))
            .setCancel(getString(R.string.cancel))
            .setConfirm(getString(R.string.sure))
            .setListener(object : CommonActionListener {
                override fun onConfirm() {
                    leaveRoom(reason)
                    trackVideoMatchHangUp(isVideoMatchLoading, true)
                }

                override fun onCancel() {
                    trackVideoMatchHangUp(isVideoMatchLoading, false)
                }
            }).create().show(supportFragmentManager, CommonDialogFragment::class.java.simpleName)
    }

    private fun onRoomActionReceived(message: ChatRoomMessage) {
        val code = (message.attachment as RoomActionAttachment).action
        val action = CoupleVoiceAction.typeOfValue(code)
        when (action) {
            CoupleVoiceAction.MINUTES_CHANGE_VIDEO -> {
                var mMessage = message.attachment as RoomActionAttachment
                showMessage(mMessage.reason_text)
                when (mMessage.minutes) {
                    0 -> showChargeDialog("1分钟后通话将自动挂断")
                    else -> {
                    }
                }

            }
            CoupleVoiceAction.FEMALE_GUEST_CLOSE -> {
                leaveRoom(CLOSE_REASON_FEMALE_USER_CLOSE)
            }
            CoupleVoiceAction.MALE_USER_CLOSE -> {//自己下麦或被踢下麦
                ToastUtil.show("对方已退出,您可匹配其他有缘人")
                leaveRoom(CLOSE_REASON_COIN_NOT_ENOUGH)
            }
            CoupleVoiceAction.FEMALE_DISCONNECTED -> {
                leaveRoom(CLOSE_REASON_FEMALE_DISCONNECTED)
            }
            CoupleVoiceAction.MINUTES_CHANGE -> {
                val attachment = message.attachment as RoomActionAttachment
                val minuteChangeAttachment = MinuteChangeAttachment()
                minuteChangeAttachment.coins = attachment.coins
                minuteChangeAttachment.free_minutes = attachment.free_minutes
                minuteChangeAttachment.minutes = attachment.minutes
//                updateRemainTime(minuteChangeAttachment)
            }
        }
    }


    var mHandler: Handler = Handler {
        when (it.what) {
            1 -> {
                val millis: Long = System.currentTimeMillis() - startTime
                var seconds = (millis / 1000).toInt()
                val minutes = seconds / 60
                seconds %= 60
                tv_chatting_duration.text = String.format("%02d:%02d", minutes, seconds)
            }

        }
        false

    }

    fun startTimer() {
        if (mTimer == null && mTask == null) {
            mTimer = Timer()
            mTask = object : TimerTask() {
                override fun run() {
                    mHandler.sendMessage(mHandler.obtainMessage(MSG))
                }
            }
            mTimer!!.schedule(mTask, 0, 1000)
        }
    }

    /**
     * 2秒消失 用户信息view
     */
    private fun hindTwoSecondView() {
        handler?.removeCallbacks(twoSecondRunnable)
        handler?.postDelayed(twoSecondRunnable, 3000)
    }

    private var twoSecondRunnable = Runnable {
        if (null != group_chatting) {//tv_vfd_cancel
            group_chatting.visibility = View.INVISIBLE
            cl_connecting_single_click?.visibility = View.INVISIBLE
            cl_connecting_1v1?.visibility = View.INVISIBLE
        }
    }

    private fun showChargeDialog(description: String) {
        dialog?.dismissAllowingStateLoss()
        trackVideoMatchRecharge()
        dialog = CommonDialogBuilder()
            .setImageTitleRes(R.mipmap.ic_yuebuzu)
            .setTitle(getString(R.string.balance_not_enough))
            .setDesc(description)
            .setMiddleTitle(getString(R.string.balance_not_enough))
            .setConfirm(getString(R.string.go_recharge))
            .setCancelAble(false)
            .setWithCancel(false)
            .setListener(object : CommonActionListener {
                override fun onConfirm() {
                    var fragment = RechargeCoinDialog()
                    fragment.setVideoMatchRoom(true)
                    fragment.show(supportFragmentManager, RechargeCoinDialog::class.java.name)
                    trackVideoMatchGoRecharge()
                }

                override fun onCancel() {

                }
            }).create()
        dialog?.show(supportFragmentManager, CommonDialogFragment::class.java.simpleName)
    }

    private fun showChargeDialogV2(description: String?) {
        dialog?.dismissAllowingStateLoss()
//        trackVideoMatchRecharge()
        dialog = CommonDialogBuilder()
            .setImageTitleRes(R.drawable.ic_yuebuzu_v2)
            .setTitle(getString(R.string.balance_not_enough_v2))
            .setMiddleTitle(getString(R.string.balance_not_enough_v2))
            .setDesc(description)
            .setConfirm(getString(R.string.go_recharge))
            .setCancel(getString(R.string.recharge_leave))
            .setListener(object : CommonActionListener {
                override fun onConfirm() {
                    var fragment = RechargeCoinDialog()
                    fragment.setVideoMatchRoom(true)
                    fragment.show(supportFragmentManager, RechargeCoinDialog::class.java.name)
                    val params: MutableMap<String, String?> = HashMap()
                    params["guest_id"] = liveInfo?.anchor_id
                    params["video_channel"] = videoChannel
                    params["button_click"] = "recharge"
                    MultiTracker.onEvent(TrackConstants.B_VIDEO_CALLING_CLICK, params)
                }

                override fun onCancel() {
                    val params: MutableMap<String, String?> = HashMap()
                    params["guest_id"] = liveInfo?.anchor_id
                    params["video_channel"] = videoChannel
                    params["button_click"] = "refuse"
                    MultiTracker.onEvent(TrackConstants.B_VIDEO_CALLING_CLICK, params)
                    finish()
                }
            }).create()
        dialog?.show(supportFragmentManager, CommonDialogFragment::class.java.simpleName)
        val params: MutableMap<String, String?> = HashMap()
        params["guest_id"] = liveInfo?.anchor_id
        params["video_channel"] = videoChannel
        MultiTracker.onEvent(TrackConstants.B_VIDEO_CALL_COIN_NOT_ENOUGH_EXPOSURE, params)
    }

    private inner class MsgObserver : com.netease.nimlib.sdk.Observer<List<ChatRoomMessage>> {
        override fun onEvent(messages: List<ChatRoomMessage>?) {
            LogUtils.d("有新消息")
            if (messages == null || messages.isEmpty() || isFinishing || isDestroyed) {
                return
            }
            for (message in messages) {
                if (message.attachment is RoomActionAttachment) {
                    onRoomActionReceived(message)
                    continue
                }
            }

        }
    }

    @Subscribe(threadMode = ThreadMode.MainThread)
    override fun onEventBus(eventCenter: EventCenter<*>?) {
        if (null != eventCenter) {
            when (eventCenter.eventCode) {
                Constants.EVENT_CODE_COIN_CHANGE -> {
                    if (isConnecting) {
                        val params: MutableMap<String, String?> = HashMap()
                        params["video_channel"] = videoChannel
                        MultiTracker.onEvent(
                            TrackConstants.B_VIDEO_CALLING_RECHARGE_SUCCESS,
                            params
                        )
                        if (StringUtil.isEquals(
                                fromPage,
                                Constants.ENTER_VIDEO_MATCH_SINGLE_CLICK
                            )
                        ) {
                            videoMatchChattingViewModel?.requestVideoMatchMic(
                                user_id = femaleGuestId,
                                live_id = liveId,
                                mic_id = micId
                            )
                        }
                    } else {
                        trackVideoMatchRechargeSuccess()
                    }
                }
            }
        }
    }

    inner class VideoRoomListener : LiveRoomListener {
        override fun onUserLeave(account: String) {
        }

        override fun onUserJoined(account: String) {
            group_video_chat.visibility = View.GONE
            cl_connecting_single_click?.visibility = View.GONE
            cl_connecting_1v1?.visibility = View.GONE
            iv_female_guest.visibility = View.GONE
            vv_connecting.visibility = View.GONE
            group_chatting.visibility = View.VISIBLE

            //用户加入
            NERtcEx.getInstance().setupLocalVideoCanvas(video_render_myself)
            video_render_myself.visibility = View.VISIBLE
            hindTwoSecondView()
            //显示远端视频流
            NERtcEx.getInstance().setupRemoteVideoCanvas(video_render_other, account.toLong())
            NERtcEx.getInstance().subscribeRemoteVideoStream(
                account.toLong(),
                NERtcRemoteVideoStreamType.kNERtcRemoteVideoStreamTypeHigh,
                true
            )
        }

        override fun onJoinRoomSuccess(channelId: Long) {
            HeartBeatUtils.addHeartBeatParams(mapOf("is_video_chatting" to true))
            liveId?.let {
                HeartBeatUtils.addHeartBeatParams(mapOf("live_id" to it))
            }
            tv_vfd_chat_duration.text = ""
            isConnecting = false
            stopCountTime()
            releaseMatchingMedia()
            startTimer()
            startTime = System.currentTimeMillis()
            trackVideoMatching()
        }

        override fun onJoinRoomFailed(resultCode: Int) {
            LogUtils.d("onJoinRoomFailed")
            //加入失败
            ToastUtil.show("加入房间失败，请重试")
            joinRoomFailed(resultCode)
            liveId?.let {
                videoMatchChattingViewModel?.cancelVideoMatchMic(
                    it,
                    CLOSE_REASON_JOIN_ROOM_FAIL.toString()
                )
            }
        }

        override fun onVideoLeave() {
            LogUtils.d("onVideoLeave")
        }

        override fun onMatchLeave() {
            LogUtils.d("onMatchLeave")
        }

        override fun onUserVideoStart(uid: Long, maxProfile: Int) {
            LogUtils.d("onUserVideoStart")

            group_video_chat.visibility = View.GONE
            group_chatting.visibility = View.VISIBLE
            tv_chatting_age_gender.visibility = View.GONE
            liveInfo?.age?.let {
                tv_chatting_age_gender.text = it.toString()
                tv_chatting_age_gender.visibility = View.VISIBLE
            }

            val games = liveInfo?.games
            if (!games.isNullOrEmpty()) {
                val layoutParams = LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
                )
                layoutParams.setMargins(0, 0, ScreenUtil.dip2px(4f), ScreenUtil.dip2px(4f))
                fl_video_match_game.removeAllViews()
                for (item in games) {
                    val tv = TextView(this@VideoMatchChattingActivity)
                    tv.setPadding(
                        ScreenUtil.dip2px(8f),
                        ScreenUtil.dip2px(2f),
                        ScreenUtil.dip2px(8f),
                        ScreenUtil.dip2px(2f)
                    )
                    tv.text = item
                    tv.maxEms = 10
                    tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 10f)
                    tv.gravity = Gravity.CENTER
                    tv.setSingleLine()
                    tv.setTextColor(Color.parseColor("#FFFFFF"))
                    tv.background = resources.getDrawable(R.drawable.bg_game)
                    tv.layoutParams = layoutParams
                    fl_video_match_game.addView(tv, layoutParams)
                }
            } else {
                fl_video_match_game.visibility = View.GONE
            }
        }

        override fun onDisconnect() {
            ToastUtil.show("对方已退出，请重新匹配")
            leaveRoom(CLOSE_REASON_STOP_ORDER)
            LogUtils.d("onDisconnect")

        }
    }

    /**
     * 速配等待语音提示
     */
    private fun playMatchingMedia() {
        try {
            if (mMediaPlayer != null) {
                releaseMatchingMedia()
            }
            mMediaPlayer = MediaPlayer.create(this, R.raw.audio_connect_waiting)
            mMediaPlayer!!.isLooping = true
            mMediaPlayer!!.setAudioStreamType(AudioManager.STREAM_MUSIC)
            mMediaPlayer?.start()
        } catch (e: Throwable) {
            e.printStackTrace()
        }
    }

    private fun releaseMatchingMedia() {
        mMediaPlayer?.stop()
        mMediaPlayer?.reset()
        mMediaPlayer?.release()
        mMediaPlayer = null
    }

    /**
     * 结束通话
     */
    fun leaveRoom(reason: Int) {
        liveId?.let {
            if (isConnecting && StringUtil.isEquals(fromPage, "1v1")) {//挂断
                videoMatchChattingViewModel?.refuseVideoMatchMic(live_id = it, mic_id = "")
            } else {
                videoMatchChattingViewModel?.cancelVideoMatchMic(it, reason.toString())
            }
        }
        mTimer?.cancel()
        mTimer = null
        mTask = null
        mAvHelper?.releaseAvRoom()
        finish()
    }


    fun getMySelfRtcVideoView(): NERtcVideoView? {
        return video_render_myself
    }

    /**
     * 视频链接中曝光
     */
    private fun trackVideoMatchLoading() {
        val params: MutableMap<String, String?> = HashMap<String, String?>()
        params["guest_id"] = liveInfo?.anchor_id
        params["record_id"] = liveId
        params["room_id"] = roomId
        params["enter_match_source"] = Constants.ENTER_VIDEO_MATCH_SQUARE
        MultiTracker.onEvent(TrackConstants.B_TEAM_VIDEO_PAGE_LOAD_EXPOSURE, params)
    }

    /**
     * 视频通话中曝光
     */
    private fun trackVideoMatching() {
        val params: MutableMap<String, String?> = HashMap<String, String?>()
        params["guest_id"] = liveInfo?.anchor_id
        params["record_id"] = liveId
        params["room_id"] = roomId
        params["video_channel"] = videoChannel
        params["enter_match_source"] = Constants.ENTER_VIDEO_MATCH_SQUARE
        MultiTracker.onEvent(TrackConstants.B_TEAM_VIDEO_PAGE_EXPOSURE, params)
    }

    /**
     * 加入房间失败原因上报
     */
    private fun joinRoomFailed(result: Int) {
        val params: MutableMap<String, String?> = HashMap()
        params["guest_id"] = liveInfo?.anchor_id
        params["record_id"] = liveId
        params["room_id"] = roomId
        params["result_code"] = result.toString()
        MultiTracker.onEvent(TrackConstants.B_TEAM_VIDEO_PAGE_JOIN_ROOM_FAILED, params)
    }


    /**
     * 视频通话中充值弹窗曝光
     */
    private fun trackVideoMatchRecharge() {
        val params: MutableMap<String, String?> = HashMap<String, String?>()
        params["guest_id"] = liveInfo?.anchor_id
        params["record_id"] = liveId
        params["room_id"] = roomId
        params["video_channel"] = videoChannel
        params["enter_match_source"] = Constants.ENTER_VIDEO_MATCH_SQUARE
        MultiTracker.onEvent(TrackConstants.B_VIDEO_NOT_ENOUGH_COIN_RECHARGE_POPUP_EXPOSURE, params)
    }

    /**
     * 视频通话中充值弹窗 - 去充值
     */
    private fun trackVideoMatchGoRecharge() {
        val params: MutableMap<String, String?> = HashMap<String, String?>()
        params["guest_id"] = liveInfo?.anchor_id
        params["record_id"] = liveId
        params["room_id"] = roomId
        params["video_channel"] = videoChannel
        params["enter_match_source"] = Constants.ENTER_VIDEO_MATCH_SQUARE
        MultiTracker.onEvent(TrackConstants.B_VIDEO_COIN_RECHARGE_BUTTON_CLICK, params)
    }

    /**
     * 挂断弹窗曝光
     */
    private fun trackVideoMatchHangUpExposure(isVideoMatchLoading: Boolean) {
        val params: MutableMap<String, String?> = HashMap<String, String?>()
        params["guest_id"] = liveInfo?.anchor_id
        params["record_id"] = liveId
        params["room_id"] = roomId
        params["page_name"] = if (isVideoMatchLoading) "load" else "chat"
        MultiTracker.onEvent(TrackConstants.B_TEAM_VIDEO_HANG_UP_POPUP_EXPOSURE, params)
    }

    /**
     * 挂断弹窗点击
     */
    private fun trackVideoMatchHangUp(isVideoMatchLoading: Boolean, isClickBtn: Boolean) {
        val params: MutableMap<String, String?> = HashMap<String, String?>()
        params["guest_id"] = liveInfo?.anchor_id
        params["record_id"] = liveId
        params["room_id"] = roomId
        params["page_name"] = if (isVideoMatchLoading) "load" else "chat"
        params["click"] = if (isClickBtn) "confirm" else "cancel"
        MultiTracker.onEvent(TrackConstants.B_TEAM_VIDEO_HANG_UP_POPUP_CLICK, params)
    }

    /**
     * 曝光视频call
     */
    private fun trackVideoCallExposure() {
        val params: MutableMap<String, String?> = HashMap()
        params["guest_id"] = liveInfo?.anchor_id
        MultiTracker.onEvent(TrackConstants.B_VIDEO_CALL_POPUP_EXPOSURE, params)
    }

    /**
     * 接收视频call
     */
    private fun trackVideoCallAnswerClick() {
        val params: MutableMap<String, String?> = HashMap()
        params["guest_id"] = liveInfo?.anchor_id
        params["call_button_click"] = "answer"
        MultiTracker.onEvent(TrackConstants.B_VIDEO_CALL_ANSWER_CLICK, params)
    }

    /**
     * 挂断视频call
     */
    private fun trackVideoCallHangUpClick(refuse_type: String) {
        val params: MutableMap<String, String?> = HashMap()
        params["guest_id"] = liveInfo?.anchor_id
        params["refuse_type"] = refuse_type
        MultiTracker.onEvent(TrackConstants.B_VIDEO_CALL_HANG_UP_CLICK, params)
    }

    override fun onDestroy() {
        super.onDestroy()
        HeartBeatUtils.addHeartBeatParams(mapOf("is_video_chatting" to false))
        HeartBeatUtils.removeHeartBeatParams("live_id")
        stopCountTime()
        releaseMatchingMedia()
        NIMClient.getService(ChatRoomServiceObserver::class.java)
            .observeReceiveMessage(mIncomingMsgObserver, false)
        NIMClient.getService(ChatRoomService::class.java).exitChatRoom(roomId)
        if (mHandler != null) {
            mHandler!!.removeCallbacksAndMessages(null)
        }
    }

    /**
     * 充值成功
     */
    private fun trackVideoMatchRechargeSuccess() {
        val params: MutableMap<String, String?> = HashMap<String, String?>()
        params["video_channel"] = videoChannel
        params["guest_id"] = liveInfo?.anchor_id
        params["record_id"] = liveId
        params["room_id"] = roomId
        params["enter_match_source"] = Constants.ENTER_VIDEO_MATCH_SQUARE
        MultiTracker.onEvent(TrackConstants.B_VIDEO_COIN_RECHARGE_SUCCESS_EXPOSURE, params)
    }
}