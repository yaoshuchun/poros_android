package com.liquid.poros.ui.fragment.dialog.session;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.request.RequestOptions;
import com.liquid.poros.R;
import com.liquid.poros.base.BaseDialogFragment;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.utils.ViewUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

/**
 * CP等级弹窗
 */
public class CoupleLevelDialogFragment extends BaseDialogFragment {
    private String mLeftAvatarUrl;
    private String mRightAvatarUrl;
    private String mCpLevel;

    public CoupleLevelDialogFragment setUserInfo(String leftAvatarUrl, String rightAvatarUrl, String cpLevel) {
        this.mLeftAvatarUrl = leftAvatarUrl;
        this.mRightAvatarUrl = rightAvatarUrl;
        this.mCpLevel = cpLevel;
        return this;
    }

    public static CoupleLevelDialogFragment newInstance() {
        CoupleLevelDialogFragment fragment = new CoupleLevelDialogFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    protected String getPageId() {
        return TrackConstants.PAGE_CP_LEVEL_DIALOG;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onStart() {
        super.onStart();
        Window win = getDialog().getWindow();
        // 一定要设置Background，如果不设置，window属性设置无效
        win.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams params = win.getAttributes();
        params.gravity = Gravity.CENTER;
        setCancelable(false);
        // 使用ViewGroup.LayoutParams，以便Dialog 宽度充满整个屏幕
        params.width = (int) (dm.widthPixels * 0.8);
        win.setAttributes(params);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_cp_level_dialog, null);
        ImageView iv_user_head_left = view.findViewById(R.id.iv_user_head_left);
        ImageView iv_user_head_right = view.findViewById(R.id.iv_user_head_right);
        TextView tv_cp_level_title = view.findViewById(R.id.tv_cp_level_title);
        TextView tv_cp_level_tip = view.findViewById(R.id.tv_cp_level_tip);
        RequestOptions leftOptions = new RequestOptions()
                .transform(new CircleCrop())
                .placeholder(R.mipmap.img_default_avatar)
                .error(R.mipmap.img_default_avatar);
        Glide.with(getActivity())
                .load(mLeftAvatarUrl)
                .apply(leftOptions)
                .into(iv_user_head_left);
        RequestOptions rightOptions = new RequestOptions()
                .transform(new CircleCrop())
                .placeholder(R.mipmap.img_default_avatar)
                .error(R.mipmap.img_default_avatar);
        Glide.with(getActivity())
                .load(mRightAvatarUrl)
                .apply(rightOptions)
                .into(iv_user_head_right);
        tv_cp_level_title.setText(String.format("恭喜你们升级为【 %s 】 ", mCpLevel));
        tv_cp_level_tip.setText(getActivity().getString(R.string.dialog_level_tip));
        view.findViewById(R.id.tv_confirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ViewUtils.isFastClick()) {
                    return;
                }
                dismissDialog();
            }
        });
        builder.setView(view);
        return builder.create();
    }
}
