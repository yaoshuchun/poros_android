package com.liquid.poros.ui.activity.account;

import android.graphics.Color;
import android.os.Bundle;
import android.widget.TextView;

import com.liquid.base.adapter.CommonAdapter;
import com.liquid.base.adapter.CommonViewHolder;
import com.liquid.poros.R;
import com.liquid.poros.base.BaseActivity;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.contract.account.RechargeDetailsContract;
import com.liquid.poros.contract.account.RechargeDetailsPresenter;
import com.liquid.poros.entity.RechargeRecordInfo;
import com.liquid.poros.utils.StringUtil;
import com.liquid.poros.widgets.pulltorefresh.BaseRefreshListener;
import com.liquid.poros.widgets.pulltorefresh.PullToRefreshLayout;
import com.liquid.poros.widgets.pulltorefresh.ViewStatus;


import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;

public class RechargeDetailsActivity extends BaseActivity implements RechargeDetailsContract.View, BaseRefreshListener {
    @BindView(R.id.container)
    PullToRefreshLayout container;
    @BindView(R.id.rv_group)
    RecyclerView rv_group;
    private RechargeDetailsPresenter mPresenter;
    private CommonAdapter<RechargeRecordInfo.Data.Records> mCommonAdapter;
    private int page = 0;

    @Override
    protected void parseBundleExtras(Bundle extras) {

    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.activity_recharge_details;
    }

    @Override
    protected void initViewsAndEvents(Bundle savedInstanceState) {
        rv_group.setLayoutManager(new LinearLayoutManager(this));
        mCommonAdapter = new CommonAdapter<RechargeRecordInfo.Data.Records>(this, R.layout.item_recharge_details) {
            @Override
            public void convert(CommonViewHolder holder, final RechargeRecordInfo.Data.Records data, int pos) {
                holder.setText(R.id.tv_name, data.getDesc());
                holder.setText(R.id.tv_time, data.getCreate_time());
                TextView tv_coin_count = holder.getView(R.id.tv_coin_count);
                tv_coin_count.setText(StringUtil.matcherSearchText(true, Color.parseColor("#FFD13D"), data.getChange_str(), data.getType_str() + ":"));
            }
        };
        rv_group.setAdapter(mCommonAdapter);
        mPresenter = new RechargeDetailsPresenter();
        mPresenter.attachView(this);
        mPresenter.getRechargeRecord(page);
        container.setRefreshListener(this);
        container.setCanLoadMore(true);
    }

    @Override
    protected String getPageId() {
        return TrackConstants.PAGE_RECHARGE_DETAILS;
    }

    @Override
    public void rechargeRecordFetch(RechargeRecordInfo info) {
        container.finishRefresh();
        container.finishLoadMore();
        if ((page == 0) && (info == null || info.getCode() != 0 || info.getData() == null || info.getData().getRecords().size() == 0)) {
            container.showView(ViewStatus.EMPTY_STATUS);
            container.setCanLoadMore(false);
            return;
        }
        container.showView(ViewStatus.CONTENT_STATUS);
        if (page == 0) {
            mCommonAdapter.update(info.getData().getRecords());
        } else {
            mCommonAdapter.add(info.getData().getRecords());
        }
    }

    @Override
    public void refresh() {
        page = 0;
        mPresenter.getRechargeRecord(page);
    }

    @Override
    public void loadMore() {
        ++page;
        mPresenter.getRechargeRecord(page);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }
}
