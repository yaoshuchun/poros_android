package com.liquid.poros.ui.activity.user;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

import com.liquid.base.event.EventCenter;
import com.liquid.poros.R;
import com.liquid.poros.adapter.PhotoManageAdapter;
import com.liquid.poros.base.BaseActivity;
import com.liquid.poros.analytics.utils.MultiTracker;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.contract.user.PhotoManageContract;
import com.liquid.poros.contract.user.PhotoManagePresenter;
import com.liquid.poros.entity.UserPhotoInfo;
import com.liquid.poros.ui.fragment.dialog.user.PictureSelectorDialogFragment;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.poros.utils.ScreenUtil;
import com.liquid.poros.utils.glide.GlideEngine;
import com.liquid.poros.widgets.WrapContentGridLayoutManager;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.style.PictureWindowAnimationStyle;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import de.greenrobot.event.EventBus;

public class PhotoManageActivity extends BaseActivity implements PhotoManageContract.View, PhotoManageAdapter.OnListener {
    @BindView(R.id.rv_pics)
    RecyclerView rv_pics;
    private PhotoManageAdapter mAdapter;
    private PhotoManagePresenter mPresenter;
    private List<UserPhotoInfo.Data.Photos> mUserPhotoList = new ArrayList<>();
    //选择上传图片资源
    private List<String> pathLists = new ArrayList<>();
    private boolean mIsUpdate = false;
    private PictureWindowAnimationStyle pictureWindowAnimationStyle =  new PictureWindowAnimationStyle();
    @Override
    protected void parseBundleExtras(Bundle extras) {

    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.activity_photo_manage;
    }

    @Override
    protected String getPageId() {
        return TrackConstants.PAGE_PHOTO_MANAGE;
    }

    @Override
    protected void initViewsAndEvents(Bundle savedInstanceState) {
        mAdapter = new PhotoManageAdapter(this, mUserPhotoList);
        rv_pics.setLayoutManager(new WrapContentGridLayoutManager(this, 4));
        rv_pics.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                outRect.top = ScreenUtil.dip2px(8);
            }
        });
        rv_pics.setAdapter(mAdapter);
        mAdapter.setOnItemListener(PhotoManageActivity.this);
        mPresenter = new PhotoManagePresenter();
        mPresenter.attachView(this);
        mPresenter.getUserPhotos(AccountUtil.getInstance().getUserId());
    }

    @Override
    public void uploadImageOssSuccess(String path) {
        mPresenter.getUserPhotos(AccountUtil.getInstance().getUserId());
        mIsUpdate = true;
    }

    @Override
    public void onUserPhotosFetch(UserPhotoInfo userPhotoInfo) {
        if (userPhotoInfo == null || userPhotoInfo.getData() == null) {
            return;
        }
        mUserPhotoList.clear();
        mUserPhotoList.addAll(userPhotoInfo.getData().getPhotos());
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onDeletePhotosFetch() {
        mPresenter.getUserPhotos(AccountUtil.getInstance().getUserId());
        mIsUpdate = true;
    }

    @Override
    public void onSelectImg() {
        choosePictureDialog(9);
    }

    @Override
    public void onItemClick(int pos, List<UserPhotoInfo.Data.Photos> data) {
        if (data.get(pos).getType() == 2) {
            PictureSelector.create(this).themeStyle(R.style.picture_default_style).externalPictureVideo(data.get(pos).getPath());
        } else {
            //点击图片预览
            PictureSelector.create((Activity) mContext)
                    .themeStyle(R.style.picture_default_style)
                    .isNotPreviewDownload(true)
                    .isPreviewVideo(true)
                    .imageEngine(GlideEngine.createGlideEngine())
                    .openExternalPreview(pos, getSelectList(data));
        }
        HashMap<String, String> params = new HashMap<>();
        params.put("type", "photo_manage");
        MultiTracker.onEvent(TrackConstants.B_PHOTO_MANAGE, params);
    }

    @Override
    public void onDeleteImg(int pos, String photoId) {
        //删除图片
        mPresenter.deleteUserPhoto(photoId);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case PictureConfig.CHOOSE_REQUEST:
                case PictureConfig.REQUEST_CAMERA:
                    List<LocalMedia> selectList = PictureSelector.obtainMultipleResult(data);
                    pathLists.clear();
                    for (int i = 0; i < selectList.size(); i++) {
                        String path = selectList.get(i).getPath();
                        if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.P) {
                            path = selectList.get(i).getAndroidQToPath();
                        }
                        pathLists.add(path);
                    }
                    mPresenter.uploadImageOss(pathLists, "user_photo");
                    break;
                default:
                    break;
            }
        }
    }

    private void choosePictureDialog(int maxSelectNum) {
        pictureWindowAnimationStyle.ofAllAnimation(R.anim.live_bottom_in,R.anim.live_bottom_out);
        PictureSelectorDialogFragment.newInstance().choosePictureListener(new PictureSelectorDialogFragment.PictureSelectorListener() {
            @Override
            public void onShotChoose() {
                PictureSelector.create(PhotoManageActivity.this)
                        .openCamera(PictureMimeType.ofImage())
                        .maxSelectNum(1)
                        .isAndroidQTransform(true)
                        .imageEngine(GlideEngine.createGlideEngine())
                        .setPictureWindowAnimationStyle(pictureWindowAnimationStyle)
                        .forResult(PictureConfig.REQUEST_CAMERA);
            }

            @Override
            public void onAlbumChoose() {
                PictureSelector.create(PhotoManageActivity.this)
                        .openGallery(PictureMimeType.ofAll())
                        .maxSelectNum(maxSelectNum)
                        .isAndroidQTransform(true)
                        .imageEngine(GlideEngine.createGlideEngine())
                        .maxVideoSelectNum(1)
                        .setPictureWindowAnimationStyle(pictureWindowAnimationStyle)
                        .forResult(PictureConfig.CHOOSE_REQUEST);
            }
        }).show(getSupportFragmentManager(), PictureSelectorDialogFragment.class.getSimpleName());
    }

    private List<LocalMedia> getSelectList(List<UserPhotoInfo.Data.Photos> data) {
        List<LocalMedia> localMedia = new ArrayList<>();
        for (UserPhotoInfo.Data.Photos photos : data) {
            localMedia.add(new LocalMedia(photos.getPath(), 0, 0, String.valueOf(photos.getType())));
        }
        return localMedia;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mIsUpdate) {
            EventBus.getDefault().post(new EventCenter<>(Constants.EVENT_CODE_UPDATE_PHOTO));
        }
        mPresenter.detachView();
    }


}
