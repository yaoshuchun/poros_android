package com.liquid.poros.ui.fragment.dialog.live;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.liquid.base.adapter.CommonAdapter;
import com.liquid.base.adapter.CommonViewHolder;
import com.liquid.poros.R;
import com.liquid.poros.base.BaseDialogFragment;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.entity.LiveGiftRankInfo;
import com.liquid.poros.ui.fragment.dialog.live.viewmodel.LiveGiftRankViewModel;
import com.liquid.poros.utils.ScreenUtil;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * 视频房礼物排行榜
 */
public class LiveGiftRankListDialogFragment extends BaseDialogFragment {
    RecyclerView rv_live_gift_rank;
    TextView tv_gift_rank_number;
    ImageView iv_gift_rank_user_avatar;
    TextView tv_gift_rank_user_name;
    TextView tv_gift_rank_tip;
    TextView tv_gift_rank_coin;
    private LiveGiftRankViewModel mLiveGiftRankViewModel;
    private List<LiveGiftRankInfo.RankInfo> mRankInfoList = new ArrayList<>();
    private CommonAdapter<LiveGiftRankInfo.RankInfo> mCommonAdapter;
    private String mRoomId;
    private int mMicPosition;

    @Override
    protected String getPageId() {
        return TrackConstants.PAGE_LIVE_GIFT_RANK_LIST;
    }

    public LiveGiftRankListDialogFragment setMicPositionRank(String roomId, int micPosition) {
        this.mRoomId = roomId;
        this.mMicPosition = micPosition;
        return this;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        Window win = getDialog().getWindow();
        win.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams params = win.getAttributes();
        params.width = ScreenUtil.getDisplayWidth();
        params.height = ScreenUtil.getDisplayWidth();
        params.gravity = Gravity.BOTTOM;
        win.setAttributes(params);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_live_gift_rank_dialog, null);
        initView(view);
        builder.setView(view);
        AlertDialog alertDialog = builder.create();
        return alertDialog;
    }

    private void initView(View view) {
        rv_live_gift_rank = view.findViewById(R.id.rv_live_gift_rank);
        tv_gift_rank_number = view.findViewById(R.id.tv_gift_rank_number);
        iv_gift_rank_user_avatar = view.findViewById(R.id.iv_gift_rank_user_avatar);
        tv_gift_rank_user_name = view.findViewById(R.id.tv_gift_rank_user_name);
        tv_gift_rank_tip = view.findViewById(R.id.tv_gift_rank_tip);
        tv_gift_rank_coin = view.findViewById(R.id.tv_gift_rank_coin);
        mCommonAdapter = new CommonAdapter<LiveGiftRankInfo.RankInfo>(getContext(), R.layout.item_live_gift_rank_list) {
            @Override
            public void convert(CommonViewHolder holder, LiveGiftRankInfo.RankInfo rankInfo, int position) {
                holder.setText(R.id.tv_gift_rank_coin, String.valueOf((int) rankInfo.getCoins()));
                holder.setText(R.id.tv_gift_rank_nickname, rankInfo.getNick_name());
                RecyclerView rv_gift_list = holder.getView(R.id.rv_gift_list);
                RequestOptions options = new RequestOptions()
                        .placeholder(R.mipmap.img_default_avatar)
                        .error(R.mipmap.img_default_avatar)
                        .transform(new CircleCrop());
                Glide.with(mContext).load(rankInfo.getAvatar_url()).apply(options).into((ImageView) holder.getView(R.id.iv_rank_user_avatar));
                if (position > 2) {
                    holder.setText(R.id.tv_rank_num, position > 8 ? String.valueOf(position + 1) : "0" + (position + 1));
                    holder.setVisibility(R.id.iv_rank_num, View.GONE);
                    holder.setVisibility(R.id.tv_rank_num, View.VISIBLE);
                } else {
                    holder.setVisibility(R.id.iv_rank_num, View.VISIBLE);
                    holder.setVisibility(R.id.tv_rank_num, View.GONE);
                    if (position == 0) {
                        holder.setBackgroundResource(R.id.iv_rank_num, R.mipmap.icon_pk_rank_first);
                    } else if (position == 1) {
                        holder.setBackgroundResource(R.id.iv_rank_num, R.mipmap.icon_pk_rank_second);
                    } else if (position == 2) {
                        holder.setBackgroundResource(R.id.iv_rank_num, R.mipmap.icon_pk_rank_third);
                    }

                }
                CommonAdapter<String> rankGiftAdapter = new CommonAdapter<String>(getContext(), R.layout.item_gift_list) {
                    @Override
                    public void convert(CommonViewHolder holder, String string, int pos) {
                        RoundedCorners roundedCorners= new RoundedCorners(1);
                        RequestOptions  options = new RequestOptions()
                                .placeholder(R.mipmap.img_default_avatar)
                                .error(R.mipmap.img_default_avatar)
                                .transform(new CenterCrop(),roundedCorners);
                        Glide.with(mContext).load(string).apply(options).into((ImageView) holder.itemView);
                    }
                };
                rv_gift_list.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false));
                rv_gift_list.setAdapter(rankGiftAdapter);
                rankGiftAdapter.update(rankInfo.getGift_list());
            }
        };
        mLiveGiftRankViewModel = getDialogFragmentViewModel(LiveGiftRankViewModel.class);
        mLiveGiftRankViewModel.getPkGiftRankList(mRoomId, mMicPosition, 0, 20);
        mLiveGiftRankViewModel.getPtaSuitListLiveData().observe(this, liveGiftRankInfo -> {
            if (liveGiftRankInfo == null || liveGiftRankInfo.getRank_list() == null || liveGiftRankInfo.getRank_list().size() == 0) {
                return;
            }
            if (liveGiftRankInfo.getSelf_rank_info() != null) {
                LiveGiftRankInfo.SelfRankInfo selfRankInfo = liveGiftRankInfo.getSelf_rank_info();
                tv_gift_rank_number.setText(selfRankInfo.getRank() > 9 ? String.valueOf(selfRankInfo.getRank()) : "0" + (selfRankInfo.getRank()));
                tv_gift_rank_tip.setVisibility(selfRankInfo.getRank() == 0 ? View.VISIBLE : View.INVISIBLE);
                tv_gift_rank_user_name.setText(selfRankInfo.getNick_name());
                RequestOptions options = new RequestOptions()
                        .placeholder(R.mipmap.img_default_avatar)
                        .error(R.mipmap.img_default_avatar)
                        .transform(new CircleCrop());
                Glide.with(this)
                        .load(selfRankInfo.getAvatar_url())
                        .apply(options)
                        .into(iv_gift_rank_user_avatar);
                tv_gift_rank_coin.setText(String.valueOf((int) selfRankInfo.getCoins()));
            }
            mRankInfoList.clear();
            mRankInfoList.addAll(liveGiftRankInfo.getRank_list());
            mCommonAdapter.update(mRankInfoList);
        });
        rv_live_gift_rank.setLayoutManager(new LinearLayoutManager(getActivity()));
        rv_live_gift_rank.setAdapter(mCommonAdapter);
    }
}
