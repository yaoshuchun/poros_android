package com.liquid.poros.ui.fragment.home;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.liquid.base.event.EventCenter;
import com.liquid.poros.R;
import com.liquid.poros.adapter.PersonNewsAdapter;
import com.liquid.poros.analytics.utils.ExposureUtils;
import com.liquid.poros.base.BaseFragment;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.contract.feed.FeedContract;
import com.liquid.poros.contract.feed.FeedPresenter;
import com.liquid.poros.entity.FemaleGuestCommentBean;
import com.liquid.poros.entity.SingleCommentBean;
import com.liquid.poros.helper.JinquanResourceHelper;
import com.liquid.poros.widgets.pulltorefresh.BaseRefreshListener;
import com.liquid.poros.widgets.pulltorefresh.PullToRefreshLayout;
import com.liquid.poros.widgets.pulltorefresh.ViewStatus;

import java.util.ArrayList;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;

/**
 * 动态广场
 */
public class FeedFragment extends BaseFragment implements FeedContract.View, BaseRefreshListener {
    @BindView(R.id.rv_group)
    RecyclerView rvGroup;
    @BindView(R.id.container)
    PullToRefreshLayout container;
    @BindView(R.id.rl_container)
    View rl_container;
    @BindView(R.id.iv_title)
    ImageView iv_title;
    @BindView(R.id.fl_head)
    View fl_head;
    private int page = 0;
    private ExposureUtils<PersonNewsAdapter, SingleCommentBean> personNewsExposureUtils;
    private FeedPresenter mPresenter;
    private PersonNewsAdapter adapter;
    private ArrayList<SingleCommentBean> commentBeans = new ArrayList<>();

    public static FeedFragment newInstance() {
        return new FeedFragment();
    }

    @Override
    protected void onFirstUserVisible() {

    }

    @Override
    protected void onUserVisible() {

    }

    @Override
    protected void onUserInvisible() {

    }

    @Override
    protected View getLoadingTargetView() {
        return null;
    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.fragment_feed;
    }

    @Override
    protected String getPageId() {
        return TrackConstants.PAGE_FEED;
    }

    @Override
    protected void initViewsAndEvents(Bundle savedInstanceState) {
        container.setRefreshListener(this);
        mPresenter = new FeedPresenter();
        mPresenter.attachView(this);
        mPresenter.fetchFeedList(page);
        adapter = new PersonNewsAdapter(getActivity());
        adapter.setListener(new PersonNewsAdapter.OnPersonNewsClickListener() {
            @Override
            public void like(String feedLike, boolean isLike) {
                mPresenter.feedLike(feedLike, isLike ? 1 : 0, Constants.SOURCE_FEED_SQUARE_LIST);
            }

            @Override
            public void voiceAction(String feedId) {
                mPresenter.feedActionReport(feedId, Constants.FEED_ACTION_VOICE, Constants.SOURCE_FEED_SQUARE_LIST);
            }

            @Override
            public void pictureAction(String feedId) {
                mPresenter.feedActionReport(feedId, Constants.FEED_ACTION_PICTURE, Constants.SOURCE_FEED_SQUARE_LIST);
            }
        });
        adapter.setFeedDetailsType(Constants.SOURCE_FEED_SQUARE_DETAIL);
//        adapter.initData(commentBeans);
        rvGroup.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvGroup.setAdapter(adapter);
    }

    @Override
    public void onFeedListFetched(FemaleGuestCommentBean femaleGuestCommentBean) {
        container.finishRefresh();
        container.finishLoadMore();
        if ((page == 0) && (adapter == null || femaleGuestCommentBean == null || femaleGuestCommentBean.feed_list == null || femaleGuestCommentBean.feed_list.size() == 0)) {
            container.showView(ViewStatus.EMPTY_STATUS);
            container.setCanLoadMore(false);
            return;
        }
        container.showView(ViewStatus.CONTENT_STATUS);
        container.setCanLoadMore(true);
        if (page == 0) {
            commentBeans.clear();
            commentBeans.addAll(femaleGuestCommentBean.feed_list);
            adapter.update(femaleGuestCommentBean.feed_list);
        } else {
            commentBeans.addAll(femaleGuestCommentBean.feed_list);
            adapter.setDatas(femaleGuestCommentBean.feed_list);
        }
    }

    @Override
    public void updateLikeResult() {
        //mPresenter.fetchFeedList(page);
    }

    @Override
    public void refresh() {
        page = 0;
        mPresenter.fetchFeedList(page);
    }

    @Override
    public void loadMore() {
        ++page;
        mPresenter.fetchFeedList(page);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (personNewsExposureUtils == null) {
            personNewsExposureUtils = new ExposureUtils(adapter, commentBeans);
            personNewsExposureUtils.setmRecyclerView(rvGroup);
        }
        personNewsExposureUtils.setFeedFrom(Constants.TYPE_FEED_SQUARE);
        personNewsExposureUtils.startFeedExposureStatistics();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (personNewsExposureUtils != null) {
            personNewsExposureUtils.calExplosure();
            personNewsExposureUtils.stopFeedExposureStatistics();
        }
    }

    @Override
    protected void onEventComing(EventCenter eventCenter) {
        super.onEventComing(eventCenter);
        switch (eventCenter.getEventCode()) {
            case Constants.EVENT_UPDATE_PERSON_NEWS:
                if (mPresenter != null) {
                    mPresenter.fetchFeedList(page);
                }
                break;
        }
    }

    @Override
    public void notifyByThemeChanged() {
        JinquanResourceHelper.getInstance(getActivity()).setBackgroundColorByAttr(rl_container, R.attr.custom_attr_app_bg);
        JinquanResourceHelper.getInstance(getActivity()).setBackgroundColorByAttr(fl_head, R.attr.custom_attr_app_bg);
        JinquanResourceHelper.getInstance(getActivity()).setImageResourceByAttr(iv_title, R.attr.bg_feed_title);
        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void notifyNetworkChanged(boolean isConnected) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }
}