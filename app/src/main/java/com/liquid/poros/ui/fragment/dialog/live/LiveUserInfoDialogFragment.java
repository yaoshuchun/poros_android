package com.liquid.poros.ui.fragment.dialog.live;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.faceunity.nama.ui.CircleImageView;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.liquid.poros.R;
import com.liquid.poros.adapter.GuardGiftAdapter;
import com.liquid.poros.analytics.utils.MultiTracker;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.contract.live.VideoRoomUserInfoContract;
import com.liquid.poros.contract.live.VideoRoomUserInfoPresenter;
import com.liquid.poros.entity.VideoRoomUserInfo;
import com.liquid.poros.ui.activity.AngelGuardNoticeActivity;
import com.liquid.poros.utils.StringUtil;
import com.luck.picture.lib.tools.ToastUtils;
import java.util.HashMap;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * 视频房用户个人信息弹窗
 */
public class LiveUserInfoDialogFragment extends BottomSheetDialogFragment implements VideoRoomUserInfoContract.View, View.OnClickListener {
    CircleImageView iv_user_head;
    TextView tv_user_cp_nickname;
    TextView tv_charm_value_level;
    TextView tv_user_cp_content;
    TextView tv_cp_want_tip;
    TextView tv_send_gift;
    LinearLayout llGiftEmpty;
    RelativeLayout rlHasGift;
    ImageView guardAngelAvatar;
    TextView guardAngelName;
    RecyclerView guardGiftList;
    ImageView isGuard;
    TextView myGiftNotice;
    View click_to_rank_list;
    private RoundedCorners roundedCorners;
    private RequestOptions options;
    private VideoRoomUserInfo.Data.RankInfoBean rankInfo;
    private VideoRoomUserInfoPresenter mPresenter;
    private VideoRoomUserInfo.Data.UserInfo mUserInfoBean;
    private String mTargetId;
    private String roomId;
    private int roomType;

    public static LiveUserInfoDialogFragment newInstance(String roomId,int roomType) {
        LiveUserInfoDialogFragment fragment = new LiveUserInfoDialogFragment(roomId,roomType);
        return fragment;
    }

    public LiveUserInfoDialogFragment(String roomId,int roomType) {
        this.roomId = roomId;
        this.roomType = roomType;
    }

    public LiveUserInfoDialogFragment setTargetId(String targetId) {
        this.mTargetId = targetId;
        return this;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        roundedCorners= new RoundedCorners(100);
        options= new RequestOptions()
                .transform(new CenterCrop(),roundedCorners);
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        try {
            super.show(manager, tag);
        } catch (Exception e) {

        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
        View view = View.inflate(getContext(), R.layout.fragment_live_user_info_dialog, null);
        dialog.setContentView(view);
        //设置透明背景
        dialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);
        initView(view);
        mPresenter = new VideoRoomUserInfoPresenter();
        mPresenter.attachView(this);
        mPresenter.getUserInfo(mTargetId);
        return dialog;
    }

    private void initView(View view) {
        iv_user_head = view.findViewById(R.id.iv_user_head);
        tv_user_cp_nickname = view.findViewById(R.id.tv_user_cp_nickname);
        tv_charm_value_level = view.findViewById(R.id.tv_charm_value_level);
        tv_user_cp_content = view.findViewById(R.id.tv_user_cp_content);
        tv_cp_want_tip = view.findViewById(R.id.tv_cp_want_tip);
        tv_send_gift = view.findViewById(R.id.tv_send_gift);
        llGiftEmpty = view.findViewById(R.id.ll_gift_empty);
        rlHasGift = view.findViewById(R.id.rl_has_gift);
        click_to_rank_list = view.findViewById(R.id.click_to_rank_list);
        guardAngelAvatar = view.findViewById(R.id.guard_angel_avatar);
        guardAngelName = view.findViewById(R.id.guard_angel_name);
        guardGiftList = view.findViewById(R.id.guard_gift_list);
        isGuard = view.findViewById(R.id.is_guard);
        myGiftNotice = view.findViewById(R.id.my_gift_notice);
        tv_send_gift.setOnClickListener(this);
        myGiftNotice.setOnClickListener(this);
        click_to_rank_list.setOnClickListener(this);

    }

    @Override
    public void onUserInfoFetch(VideoRoomUserInfo videoRoomUserInfo) {
        if (videoRoomUserInfo == null || videoRoomUserInfo.getData() == null || videoRoomUserInfo.getData().getUser_info() == null) {
            return;
        }
        mUserInfoBean = videoRoomUserInfo.getData().getUser_info();
        Glide.with(getActivity())
                .load(mUserInfoBean.getAvatar_url())
                .into(iv_user_head);
        tv_user_cp_nickname.setText(mUserInfoBean.getNick_name());
        //魅力值
        int[] charmValuesDrawable = {R.drawable.ic_charm_00,R.drawable.ic_charm_20,R.drawable.ic_charm_40,R.drawable.ic_charm_60,R.drawable.ic_charm_80,R.drawable.ic_charm_100};
        tv_charm_value_level.setBackgroundResource(charmValuesDrawable[mUserInfoBean.getUser_charm()/20]);
        tv_charm_value_level.setText(Html.fromHtml(String.format("魅 • <b>%d</b>", mUserInfoBean.getUser_charm())));
        //礼物榜
        rankInfo = videoRoomUserInfo.getData().getRank_info();
        if (rankInfo != null) {
            llGiftEmpty.setVisibility(View.GONE);
            rlHasGift.setVisibility(View.VISIBLE);
            if (rankInfo.is_guard) {
                isGuard.setVisibility(View.VISIBLE);
            }else {
                isGuard.setVisibility(View.GONE);
            }
            GuardGiftAdapter adapter = new GuardGiftAdapter(getContext(),rankInfo.gift_list);
            LinearLayoutManager manager = new LinearLayoutManager(getContext());
            manager.setOrientation(RecyclerView.HORIZONTAL);
            guardGiftList.setLayoutManager(manager);
            guardGiftList.setAdapter(adapter);
            guardAngelName.setText(rankInfo.nick_name);
            Glide.with(this).applyDefaultRequestOptions(options).load(rankInfo.avatar_url).into(guardAngelAvatar);
        }else {
            llGiftEmpty.setVisibility(View.VISIBLE);
            rlHasGift.setVisibility(View.GONE);
        }

        //理想CP
        if (!TextUtils.isEmpty(mUserInfoBean.getCouple_desc())) {
            tv_user_cp_content.setVisibility(View.VISIBLE);
            tv_cp_want_tip.setVisibility(View.GONE);
            tv_user_cp_content.setText(StringUtil.replaceBlank(mUserInfoBean.getCouple_desc()));
        } else {
            tv_user_cp_content.setVisibility(View.GONE);
            tv_cp_want_tip.setVisibility(View.VISIBLE);
        }
        tv_send_gift.setText(videoRoomUserInfo.getData().getButton_content());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_send_gift:
                if (mUserInfoBean == null){
                    return;
                }
                if (listener != null) {
                    listener.onSendGift(mUserInfoBean.getUser_id());
                }
                HashMap<String, String> params = new HashMap<>();
                params.put("female_guest_id", mUserInfoBean.getUser_id());
                params.put("live_room_id",roomId);
                MultiTracker.onEvent(TrackConstants.B_GUEST_DETAILS_ADD_FRIEND_CLICK,params);
                dismiss();
                break;
            case R.id.my_gift_notice:
                if (rankInfo != null) {
                    Bundle bundle = new Bundle();
                    bundle.putString(AngelGuardNoticeActivity.FEMALE_USER_ID,mTargetId);
                    if (roomType == 0) {
                        bundle.putString(Constants.GIFT_RANK_PATH,"video_room");
                    }else {
                        bundle.putString(Constants.GIFT_RANK_PATH,"exclusive_room");
                    }
                    Intent intent = new Intent(getContext(),AngelGuardNoticeActivity.class);
                    if (null != bundle) {
                        intent.putExtras(bundle);
                    }
                    startActivity(intent);
                }else {
                    ToastUtils.s(getContext(),"暂未收到礼物");
                }
                break;
            case R.id.ll_gift_empty:
                ToastUtils.s(getContext(),"暂未收到礼物");
                break;
            case R.id.click_to_rank_list:
                Bundle bundle = new Bundle();
                bundle.putString(AngelGuardNoticeActivity.FEMALE_USER_ID,mTargetId);
                if (roomType == 0) {
                    bundle.putString(Constants.GIFT_RANK_PATH,"video_room");
                }else {
                    bundle.putString(Constants.GIFT_RANK_PATH,"exclusive_room");
                }
                Intent intent = new Intent(getContext(),AngelGuardNoticeActivity.class);
                if (null != bundle) {
                    intent.putExtras(bundle);
                }
                startActivity(intent);
                break;
        }
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void showSuccess() {

    }

    @Override
    public void closeLoading() {

    }

    @Override
    public void onError() {

    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSuccess(Object data) {

    }

    private LiveUserInfoDialogFragment.Listener listener;

    public LiveUserInfoDialogFragment setListener(LiveUserInfoDialogFragment.Listener listener) {
        this.listener = listener;
        return this;
    }


    public interface Listener {
        //送礼物
        void onSendGift(String targetId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }


}
