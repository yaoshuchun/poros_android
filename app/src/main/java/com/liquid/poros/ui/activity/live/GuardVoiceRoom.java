package com.liquid.poros.ui.activity.live;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.faceunity.nama.ui.CircleImageView;
import com.liquid.base.tools.GT;
import com.liquid.base.utils.ContextUtils;
import com.liquid.im.kit.custom.InviteCoupleAttachment;
import com.liquid.im.kit.custom.LevelChangeAttachment;
import com.liquid.im.kit.custom.ScoreChangeAttachment;
import com.liquid.poros.AvInterface;
import com.liquid.poros.PorosApplication;
import com.liquid.poros.R;
import com.liquid.poros.base.BaseActivity;
import com.liquid.poros.analytics.utils.MultiTracker;
import com.liquid.poros.constant.CardCategoryEnum;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.constant.ValidTalk;
import com.liquid.poros.entity.BuyCoupleCardInfo;
import com.liquid.poros.entity.CpLevelUpDetail;
import com.liquid.poros.entity.MsgUserModel;
import com.liquid.poros.helper.JinquanResourceHelper;
import com.liquid.poros.helper.TimeUtil;
import com.liquid.poros.service.AvCoreService;
import com.liquid.poros.ui.activity.DeclarationActivity;
import com.liquid.poros.ui.activity.DeclarationDetailActivity;
import com.liquid.poros.ui.activity.account.RechargeActivity;
import com.liquid.poros.ui.activity.audiomatch.CPMatchingGlobal;
import com.liquid.poros.ui.activity.user.SessionActivity;
import com.liquid.poros.ui.activity.user.UserCenterActivity;
import com.liquid.poros.ui.dialog.RechargeCoinDialog;
import com.liquid.poros.ui.floatball.FloatManager;
import com.liquid.poros.ui.floatball.GiftPackageFloatManager;
import com.liquid.poros.ui.floatball.rom.HuaweiUtils;
import com.liquid.poros.ui.floatball.rom.MeizuUtils;
import com.liquid.poros.ui.floatball.rom.MiuiUtils;
import com.liquid.poros.ui.floatball.rom.OppoUtils;
import com.liquid.poros.ui.floatball.rom.QikuUtils;
import com.liquid.poros.ui.floatball.rom.RomUtils;
import com.liquid.poros.ui.fragment.dialog.account.BuyCoupleCardDialogFragment;
import com.liquid.poros.ui.fragment.dialog.account.BuyCoupleCardDialogFragmentTwo;
import com.liquid.poros.ui.fragment.dialog.common.CommonActionListener;
import com.liquid.poros.ui.fragment.dialog.common.CommonDialogBuilder;
import com.liquid.poros.ui.fragment.dialog.common.CommonDialogFragment;
import com.liquid.poros.ui.fragment.dialog.session.CoupleLevelDialogFragment;
import com.liquid.poros.ui.fragment.dialog.session.CoupleLevelTestDialogFragment;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.base.utils.LogOut;
import com.liquid.poros.utils.ScreenUtil;
import com.liquid.poros.utils.SharePreferenceUtil;
import com.liquid.poros.utils.StringUtil;
import com.liquid.poros.utils.ViewUtils;
import com.liquid.poros.utils.network.HttpCallback;
import com.liquid.poros.utils.network.RetrofitHttpManager;
import com.liquid.poros.widgets.RippleView;
import com.liquid.porostwo.business.router.Router;
import com.netease.nimlib.sdk.msg.MessageBuilder;
import com.netease.nimlib.sdk.msg.constant.SessionTypeEnum;
import com.netease.nimlib.sdk.msg.model.IMMessage;
import com.orhanobut.logger.Logger;

import org.json.JSONObject;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.RequestBody;


public class GuardVoiceRoom extends VoiceRoom implements View.OnClickListener {
    private ViewStub stubParent;
    private View container;
    private SessionActivity mContext;
    private String sessionId;
    private String avatarUrl;
    private String name;
    private String micPrice;

    private CircleImageView iv_cp_user_head;   //登录用户头像
    private CircleImageView iv_cp_target_head; //目标用户头像

    private TextView tv_cp_target_user_head;

    private RippleView anim_cp_target_user_head;
    private RippleView anim_cp_user_head;

    private TextView tv_enter_room;            //进入房间

    private TextView level_tv;
    private ImageView level_icon;
    private ImageView upgrade_cp_iv;

    private TextView intimacy_value;
    private ImageView iv_cp_desc;
    private TextView tv_price;
    private TextView tv_tips;
    private int currentCpStatus;
    private boolean isHost;
    private String imVideoToken;
    private String currentTeamImId;
    private String currentTeamId;
    private AvInterface mAvManager;
    private PopupWindow mCpGuideView;
    private PopupWindow mMicGuideView;
    //游戏信息展示
    private LinearLayout ll_game_guard_info;
    private TextView tv_game_content;
    private LinearLayout ll_game_tip;
    private TextView tv_game_tip;
    private ImageView iv_game_tip;
    private View bg_click;
    private boolean mIsInviteMic;
    private String leftHead;
    private String nextRankName;
    private String roomId;
    private RechargeCoinDialog fragment;
    private String buy_position;//开黑卡购买埋点参数
    private HashMap<String, Long> volumeMap = new HashMap<>();
    private ViewStub.OnInflateListener onInflateListener;

    public void setOnInflateListener(ViewStub.OnInflateListener onInflateListener){
        this.onInflateListener = onInflateListener;
    }

    public TextView getLevel_tv() {
        return level_tv;
    }

    public ImageView getUpgrade_cp_iv() {
        return upgrade_cp_iv;
    }

    public void setBuy_position(String buy_position) {
        this.buy_position = buy_position;
    }

    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mAvManager = AvInterface.Stub.asInterface(service);
            initLink(sessionId,roomId);
            joinRoom(currentTeamImId,imVideoToken);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            HashMap<String, String> map = new HashMap<>();
            map.put("user_id", AccountUtil.getInstance().getUserId());
            map.put("tag", "connect_mic_ques");
            map.put("female_guest_id", sessionId);
            map.put("event_time", String.valueOf(System.currentTimeMillis()));
            MultiTracker.onEvent("on_service_disconnect", map);
        }
    };

    private Timer mTimer1;
    private TimerTask mTask1;

    private CoupleLevelDialogFragment coupleLevelDialogFragment;
    private CoupleLevelTestDialogFragment coupleLevelTestDialogFragment;

    public void initLink(String sessionId,String roomId) {
        this.sessionId = sessionId;
        this.roomId = roomId;
        try {
            if (mAvManager != null && mAvManager.isAvRunning()) {
                if (StringUtil.isEquals(sessionId, FloatManager.getInstance().getSessionId())) {
                    FloatManager.getInstance().hideMenuWindow();
                    tv_enter_room.setEnabled(true);
                    mContext.setOnMic(false);
                    tv_tips.setVisibility(View.GONE);

                    if (memberList != null && !isLeftInRoom(memberList)) {
                        if (!isRightInRoom(memberList)) {

                        } else {
                            setMicMute(true);
                        }
                    }
                } else {
                    tv_enter_room.setEnabled(false);
                    mContext.setOnMic(true);
                    showCpMicStatus(0, mContext.getString(R.string.cp_unavaliable_mic));
                }
            } else {
                if (StringUtil.isNotEmpty(roomId)) {
                    return;
                }
                if (isLeftInRoom(memberList) && isRightInRoom(memberList)) {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("user_id", AccountUtil.getInstance().getUserId());
                    map.put("tag", "connect_mic_ques");
                    map.put("female_guest_id", sessionId);
                    map.put("is_mute", "false");
                    map.put("event_time", String.valueOf(System.currentTimeMillis()));
                    MultiTracker.onEvent("init_link_auto_join_room", map);
                    autoJoinRoom(false);
                } else if (!isLeftInRoom(memberList) && isRightInRoom(memberList)) {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("user_id", AccountUtil.getInstance().getUserId());
                    map.put("tag", "connect_mic_ques");
                    map.put("female_guest_id", sessionId);
                    map.put("is_mute", "true");
                    map.put("event_time", String.valueOf(System.currentTimeMillis()));
                    MultiTracker.onEvent("init_link_auto_join_room", map);
                    autoJoinRoom(true);
                } else if (isLeftInRoom(memberList) && !isRightInRoom(memberList)) {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("user_id", AccountUtil.getInstance().getUserId());
                    map.put("tag", "connect_mic_ques");
                    map.put("female_guest_id", sessionId);
                    map.put("event_time", String.valueOf(System.currentTimeMillis()));
                    MultiTracker.onEvent("init_link_close_or_leave_team", map);
                    closeOrLeaveTeam(true);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setIsInviteMic(boolean mIsInviteMic) {
        this.mIsInviteMic = mIsInviteMic;
    }

    /**
     * 自动连麦  只听声  不说话
     */
    private void autoJoinRoom(boolean isMute) {
        //开始自动连麦
        joinRoom(currentTeamImId,imVideoToken);
        setMicMute(isMute);
    }


    private List<MsgUserModel.Member> memberList;


    public GuardVoiceRoom(ViewStub container, SessionActivity context, String sessionId, String micPrice) {
        this.stubParent = container;
        this.mContext = context;
        this.sessionId = sessionId;
        this.micPrice = micPrice;
        fragment = new RechargeCoinDialog();
        //启动连麦服务
        context.bindService(new Intent(context, AvCoreService.class), mConnection, Context.BIND_AUTO_CREATE);
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    @Override
    public void initView() {
        stubParent.setOnInflateListener(new ViewStub.OnInflateListener() {
            @Override
            public void onInflate(ViewStub stub, View inflated) {
                if (null != onInflateListener){
                    onInflateListener.onInflate(stub,inflated);
                }
            }
        });
        container = stubParent.inflate();
        iv_cp_user_head = container.findViewById(R.id.iv_cp_user_head);
        iv_cp_target_head = container.findViewById(R.id.iv_cp_target_user_head);

        iv_cp_target_head.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString(Constants.USER_ID, sessionId);
                bundle.putString(Constants.GIFT_RANK_PATH, "private_chat");

                Intent intent = new Intent(mContext, UserCenterActivity.class);
                intent.putExtras(bundle);
                mContext.startActivity(intent);
            }
        });
        tv_tips = container.findViewById(R.id.tv_tips);
        tv_price = container.findViewById(R.id.tv_price);
        tv_enter_room = container.findViewById(R.id.tv_enter_room);
        tv_cp_target_user_head = container.findViewById(R.id.tv_cp_target_user_head);
        anim_cp_target_user_head = container.findViewById(R.id.anim_cp_target_user_head);
        anim_cp_user_head = container.findViewById(R.id.anim_cp_user_head);

        iv_cp_desc = container.findViewById(R.id.iv_cp_desc);
        level_tv = container.findViewById(R.id.level_tv);
        level_icon = container.findViewById(R.id.level_icon);
        intimacy_value = container.findViewById(R.id.intimacy_value);
        upgrade_cp_iv = container.findViewById(R.id.upgrade_cp_iv);
        //游戏信息展示
        ll_game_guard_info = container.findViewById(R.id.ll_game_guard_info);
        tv_game_content = container.findViewById(R.id.tv_game_content);
        ll_game_tip = container.findViewById(R.id.ll_game_tip);
        tv_game_tip = container.findViewById(R.id.tv_game_tip);
        iv_game_tip = container.findViewById(R.id.iv_game_tip);
        bg_click = container.findViewById(R.id.bg_click);
        if (mIsInviteMic) {
            showMicGuide();
        }

        upgrade_cp_iv.setOnClickListener(this);
        iv_cp_desc.setOnClickListener(this);
        ll_game_tip.setOnClickListener(this);
        tv_enter_room.setOnClickListener(this);
        bg_click.setOnClickListener(this);

        if (AccountUtil.getInstance().isOpen()){
            container.post(new Runnable() {
                @Override
                public void run() {
                    if (mContext == null) {
                        return;
                    }
                    if (mContext.isFinishing() || mContext.isDestroyed()) {
                        return;
                    }
                    showGiftPackageGuide();
                }
            });
        }
    }

    public void showGiftPackageGuide() {
        if (AccountUtil.getInstance().isOpen()){
            int[] location = new int[2];
            tv_enter_room.getLocationInWindow(location);
            ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) mContext.guide_gift_package_iv.getLayoutParams();
            layoutParams.topMargin = location[1];
            layoutParams.rightMargin = ScreenUtil.getDisplayWidth() - (location[0] + tv_enter_room.getWidth());
            mContext.guide_gift_package_iv.setLayoutParams(layoutParams);
            mContext.guide_gift_package_iv.setVisibility(View.VISIBLE);
        }else {
            mContext.guide_gift_package_iv.setVisibility(View.GONE);
        }
    }

    private long joinMicTime;
    private int MIN_DURATION = 63;

    protected int getThemeTag() {
        return SharePreferenceUtil.getInt(SharePreferenceUtil.FILE_USER_ACCOUNT_DATA, Constants.KEY_CACHE_THEME_TAG, 1);
    }

    /**
     * 更新房间状态
     */
    @Override
    public void updateRoomStatus(MsgUserModel.UserInfo cpInfo) {
        if (!cpInfo.isShow_couple()) {
            if (container != null) {
                container.setVisibility(View.GONE);
            }
            return;
        } else {
            if (container == null) {
                initView();
            }
            if (container != null) {
                container.setVisibility(View.VISIBLE);
            }
        }
        if (cpInfo.getTeam_info() != null) {
            memberList = cpInfo.getTeam_info().getMember_group();
            isHost = StringUtil.isEquals(AccountUtil.getInstance().getUserId(), cpInfo.getTeam_info().getLeader_id());
            currentTeamImId = cpInfo.getTeam_info().getTeam_im_id();
            imVideoToken = cpInfo.im_video_token;
            currentTeamId = cpInfo.getTeam_info().getTeam_id();
        } else {
            memberList = null;
            currentTeamImId = null;
            currentTeamId = null;
            imVideoToken = null;
        }
        avatarUrl = cpInfo.getAvatar_url();
        name = cpInfo.getNick_name();
        if (isRightInRoom(memberList) && !isLeftInRoom(memberList)) {
            if (mAvManager != null) {
                initLink(sessionId,roomId);
            }
        }

        if (isRightInRoom(memberList) && isLeftInRoom(memberList)) {
            tv_tips.setVisibility(View.GONE);
        } else {

        }

        leftHead = cpInfo.getAvatar_url();
        RequestOptions options = new RequestOptions()
                .placeholder(R.mipmap.img_default_avatar)
                .error(R.mipmap.img_default_avatar);
        Glide.with(mContext)
                .load(AccountUtil.getInstance().getUserInfo().getAvatar_url())
                .apply(options)
                .into(iv_cp_user_head);

        Glide.with(mContext)
                .load(cpInfo.getAvatar_url())
                .apply(options)
                .into(iv_cp_target_head);

        currentCpStatus = cpInfo.getCp_status();
        Logger.d("cpInfo.getCp_status()    " + cpInfo.getCp_status());
        tv_enter_room.setTag(cpInfo.getCp_status());
        //共同游戏信息
        showGameInfo(cpInfo.getCommon_game_hint(), cpInfo.getCommon_games_str());
        updateLevel(cpInfo.rank_info);
        updateMicStatus(cpInfo);
        if (cpInfo.raise_group_info != null) {
            if (cpInfo.raise_group_info.award_list != null && cpInfo.raise_group_info.award_list.size() > 0) {
                if (mContext != null && mContext.canShowCpUpgradeDialog) {
                    showUpgradeDialog(cpInfo.raise_group_info);
                    mContext.getPresenter().raiseGroupBack(sessionId);
                }
            }
        }
    }
    private void updateMicStatus(MsgUserModel.UserInfo cpInfo){
        if (isRightInRoom(memberList) || isLeftInRoom(memberList)) {
            if (isRightInRoom(memberList)) {
                tv_cp_target_user_head.setSelected(true);
                tv_cp_target_user_head.setText("TA已上麦");
                iv_cp_target_head.setBorderColor(Color.parseColor("#FF598C"));
            } else {
                tv_cp_target_user_head.setSelected(false);
                tv_cp_target_user_head.setText("TA未上麦");
                iv_cp_target_head.setBorderColor(Color.parseColor(getThemeTag() == 1 ? "#EBEFF5" : "#303047"));
                anim_cp_target_user_head.setVisibility(View.GONE);
            }
            if (isLeftInRoom(memberList)) {
                tv_enter_room.setSelected(true);
                tv_enter_room.setText("点击下麦");

                if (tv_enter_room.isEnabled()) {
                    switch (currentCpStatus) {
                        case 5://已购买专属语音房
                        case 3:
                            if (cpInfo.getVoice_end_time() > cpInfo.getServer_timestamp()) {
                                tv_price.setVisibility(View.VISIBLE);
                                if (mTimer1 == null && mTask1 == null) {
                                    mTimer1 = new Timer();
                                    mTask1 = new MicCardTask();
                                    ((MicCardTask) mTask1).cpInfo = cpInfo;
                                    mTimer1.schedule(mTask1, 0, 1000);
                                } else {
                                    ((MicCardTask) mTask1).cpInfo = cpInfo;
                                }
                            }
                    }
                }
                iv_cp_user_head.setBorderColor(Color.parseColor("#FF598C"));
            } else {
                tv_enter_room.setSelected(false);
                tv_enter_room.setText("点击上麦");
                iv_cp_user_head.setBorderColor(Color.parseColor(getThemeTag() == 1 ? "#EBEFF5" : "#303047"));
                anim_cp_user_head.setVisibility(View.GONE);
            }
        } else {
            tv_price.setVisibility(View.GONE);
            releaseTimer();
            tv_tips.setVisibility(View.GONE);
            tv_enter_room.setSelected(false);
            tv_enter_room.setText("点击上麦");
            tv_cp_target_user_head.setText("TA未上麦");
            tv_cp_target_user_head.setSelected(false);
            iv_cp_user_head.setBorderColor(Color.parseColor(getThemeTag() == 1 ? "#EBEFF5" : "#303047"));
            iv_cp_target_head.setBorderColor(Color.parseColor(getThemeTag() == 1 ? "#EBEFF5" : "#303047"));
            anim_cp_user_head.setVisibility(View.GONE);
            anim_cp_target_user_head.setVisibility(View.GONE);
        }
    }
    private boolean canUpdateUI() {
        return container != null && container.getVisibility() == View.VISIBLE;
    }

    private void showUpgradeDialog(MsgUserModel.P2PRankUpgradeAwardInfo upgradeAwardInfo) {
        if (!canUpdateUI()) {
            return;
        }

        String intimacy = intimacy_value.getText().toString();
        try {
            if (coupleLevelTestDialogFragment == null) {
                coupleLevelTestDialogFragment = CoupleLevelTestDialogFragment.newInstance();
            }
            coupleLevelTestDialogFragment.setInfo(upgradeAwardInfo, sessionId, avatarUrl, intimacy).show(mContext.getSupportFragmentManager(), CoupleLevelTestDialogFragment.class.getSimpleName());

        } catch (Exception e) {
            e.printStackTrace();
        }

        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", AccountUtil.getInstance().getUserId());
        params.put("female_guest_id", sessionId);
        params.put("event_time", String.valueOf(System.currentTimeMillis()));
        params.put("rise_to", upgradeAwardInfo.group_name);
        params.put("intimacy", intimacy);
        MultiTracker.onEvent(TrackConstants.B_EXPOSURE_POPUP_RISE_CP_LEVEL, params);
    }

    private void showUpgradeCpGuide() {
        int[] location = new int[2];
        upgrade_cp_iv.getLocationInWindow(location);
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) mContext.guide_upgrade_cp_iv.getLayoutParams();
        layoutParams.topMargin = location[1];
        layoutParams.rightMargin = ScreenUtil.getDisplayWidth() - (location[0] + upgrade_cp_iv.getWidth());
        mContext.guide_upgrade_cp_iv.setLayoutParams(layoutParams);
        mContext.guide_upgrade_cp_iv.setVisibility(View.VISIBLE);
    }

    private void updateLevel(MsgUserModel.RankInfo rankInfo) {
        if (rankInfo != null) {
            nextRankName = rankInfo.next_rank_name;
            Glide.with(mContext)
                    .load(rankInfo.rank_icon)
                    .into(level_icon);
            level_tv.setText(rankInfo.name);
            intimacy_value.setText(getFloatData(rankInfo.cp_score) + "");
            if (rankInfo.has_rank_rise) {
                updateLevelDialog(rankInfo.name);
            }

            if (rankInfo.level_pack_test && rankInfo.need_advance) {
                if (upgrade_cp_iv.getVisibility() != View.VISIBLE) {
                    level_tv.setVisibility(View.INVISIBLE);
                    upgrade_cp_iv.setVisibility(View.VISIBLE);
                    upgrade_cp_iv.setEnabled(true);
                    upgrade_cp_iv.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (mContext == null) {
                                return;
                            }
                            if (mContext.isFinishing() || mContext.isDestroyed()) {
                                return;
                            }
                            if (rankInfo.cp_hint) {
                                showUpgradeCpGuide();
                            }
                        }
                    }, 200);
                }
            } else {
                upgrade_cp_iv.setVisibility(View.INVISIBLE);
                level_tv.setVisibility(View.VISIBLE);
                upgrade_cp_iv.setEnabled(false);
                mContext.guide_upgrade_cp_iv.setVisibility(View.INVISIBLE);
            }
        }
    }

    private class MicCardTask extends TimerTask {
        public MsgUserModel.UserInfo cpInfo;

        @Override
        public void run() {
            Message message = Message.obtain();
            message.obj = cpInfo;
            mHandler.sendMessage(message);
        }
    }

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            MsgUserModel.UserInfo cpInfo = (MsgUserModel.UserInfo) msg.obj;
            if (mContext.isFinishing() || mContext.isDestroyed()) {
                return;
            }
            long duration = cpInfo.getVoice_end_time() - System.currentTimeMillis();
            if (duration > 0) {
                tv_price.setVisibility(View.VISIBLE);
                tv_price.setText(String.format("剩余时长 %s", TimeUtil.getTime1((int) (duration / 1000))));
            } else {
                tv_price.setVisibility(View.GONE);
                if (cpInfo.getCp_status() != 2 && isLeftInRoom(memberList)) {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("user_id", AccountUtil.getInstance().getUserId());
                    params.put("tag", "connect_mic_ques");
                    params.put("female_guest_id", sessionId);
                    params.put("event_time", String.valueOf(System.currentTimeMillis()));
                    MultiTracker.onEvent("time_out_leave_room", params);
                }
                mContext.getPresenter().getUserStatusInfo(sessionId);
                releaseTimer();
            }
        }
    };

    private void showGameInfo(String gameHint, String gamesString) {
        if (!TextUtils.isEmpty(gameHint) && !TextUtils.isEmpty(gamesString)) {
            ll_game_guard_info.setVisibility(View.VISIBLE);
            tv_game_tip.setText(mContext.getString(R.string.session_guard_game_close));
            JinquanResourceHelper.getInstance(mContext).setImageResourceByAttr(iv_game_tip, R.attr.custom_bg_game_tip_close);
            SpannableString spannableString = new SpannableString(gameHint + gamesString);
            spannableString.setSpan(new ForegroundColorSpan(getThemeTag() == 1 ? Color.parseColor("#A4A9B3") : Color.parseColor("#898996")), 0, gameHint.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            spannableString.setSpan(new ForegroundColorSpan(getThemeTag() == 1 ? Color.parseColor("#181E25") : Color.parseColor("#ffffff")), gameHint.length(), gameHint.length() + gamesString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            tv_game_content.setText(spannableString);
            tv_game_content.post(new Runnable() {
                @Override
                public void run() {
                    int line = tv_game_content.getLineCount();
                    ll_game_tip.setVisibility(line > 1 ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            ll_game_guard_info.setVisibility(View.GONE);
        }
    }

    /**
     * 对方在麦上
     *
     * @param members
     * @return
     */
    private boolean isRightInRoom(List<MsgUserModel.Member> members) {
        if (members == null) {
            return false;
        }
        for (MsgUserModel.Member member : members) {
            if (StringUtil.isEquals(sessionId, member.getUser_id())) {
                return true;
            }
        }
        return false;
    }

    /**
     * 语音CP在线状态提示
     *
     * @param status 状态码
     * @param micTip 状态描述
     */
    private void showCpMicStatus(int status, String micTip) {
        tv_tips.setVisibility(View.VISIBLE);
        tv_tips.setText(micTip);
        switch (status) {
            case 0://忙碌
                tv_tips.setTextColor(Color.parseColor("#ffffff"));
                break;
            case 1://离线
                tv_tips.setTextColor(Color.parseColor("#ffffff"));
                break;
            case 2://在线
                tv_tips.setTextColor(Color.parseColor("#ffffff"));
                break;
            default:
                tv_tips.setVisibility(View.GONE);
                break;
        }
    }

    /**
     * 用户在麦上
     *
     * @param members
     * @return
     */
    private boolean isLeftInRoom(List<MsgUserModel.Member> members) {
        if (members == null) {
            return false;
        }
        for (MsgUserModel.Member member : members) {
            if (StringUtil.isEquals(AccountUtil.getInstance().getUserId(), member.getUser_id())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean isOnMic() {
        return isLeftInRoom(memberList);
    }

    @Override
    public boolean isAvRunning() {
        boolean isRunning = false;
        try {
            isRunning = mAvManager != null && mAvManager.isAvRunning();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return isRunning;
    }

    @Override
    public int getCurrentCpStatus() {
        return currentCpStatus;
    }

    @Override
    public void showCpCardDialog(boolean isAgreeCp, boolean isCpRoom, String roomId, String roomAnchor, String targetId, String cpOrderId, String cardCategory) {
        boolean result = FloatManager.getInstance().checkPermission(mContext);
        if (!result && !isLeftInRoom(memberList)) {
            HashMap<String, String> params = new HashMap<>();
            params.put("user_id", AccountUtil.getInstance().getUserId());
            params.put("female_guest_id", sessionId);
            params.put("type", "cp_card");
            MultiTracker.onEvent(TrackConstants.P_FLOAT_SHOW, params);
            new CommonDialogBuilder()
                    .setDesc(mContext.getString(R.string.float_title))
                    .setImageTitleRes(R.mipmap.icon_open_float)
                    .setConfirm(mContext.getString(R.string.float_open))
                    .setCancel(mContext.getString(R.string.float_cancel))
                    .setListener(new CommonActionListener() {
                        @Override
                        public void onConfirm() {
                            try {
                                if (Build.VERSION.SDK_INT < 23) {
                                    if (RomUtils.checkIsMiuiRom()) {
                                        MiuiUtils.applyMiuiPermission(mContext);
                                    } else if (RomUtils.checkIsMeizuRom()) {
                                        MeizuUtils.applyPermission(mContext);
                                    } else if (RomUtils.checkIsHuaweiRom()) {
                                        HuaweiUtils.applyPermission(mContext);
                                    } else if (RomUtils.checkIs360Rom()) {
                                        QikuUtils.applyPermission(mContext);
                                    } else if (RomUtils.checkIsOppoRom()) {
                                        OppoUtils.applyOppoPermission(mContext);
                                    }
                                } else {
                                    if (RomUtils.checkIsMeizuRom()) {
                                        MeizuUtils.applyPermission(mContext);
                                    } else {
                                        FloatManager.commonROMPermissionApplyInternal(mContext);
                                    }
                                }
                            } catch (NoSuchFieldException e) {
                                e.printStackTrace();
                            } catch (IllegalAccessException e) {
                                e.printStackTrace();
                            }
                            HashMap<String, String> params = new HashMap<>();
                            params.put("user_id", AccountUtil.getInstance().getUserId());
                            params.put("female_guest_id", sessionId);
                            params.put("type", "cp_card");
                            MultiTracker.onEvent(TrackConstants.B_FLOAT_OPEN, params);
                        }

                        @Override
                        public void onCancel() {
                            showDialog(isAgreeCp, isCpRoom, roomId, roomAnchor, targetId, cpOrderId, cardCategory);
                            HashMap<String, String> params = new HashMap<>();
                            params.put("female_guest_id", sessionId);
                            params.put("user_id", AccountUtil.getInstance().getUserId());
                            params.put("type", "cp_card");
                            MultiTracker.onEvent(TrackConstants.B_FLOAT_CANCEL, params);
                        }
                    }).create().show(mContext.getSupportFragmentManager(), CommonDialogFragment.class.getSimpleName());
        } else {
            showDialog(isAgreeCp, isCpRoom, roomId, roomAnchor, targetId, cpOrderId, cardCategory);
            HashMap<String, String> params = new HashMap<>();
            params.put("user_id", AccountUtil.getInstance().getUserId());
            params.put("female_guest_id", sessionId);
            params.put("type", "cp_card");
            MultiTracker.onEvent(TrackConstants.P_FLOAT_SHOW_SUCCESS, params);
        }

    }

    @Override
    public void onRoomCreate(String teamImId, String tips, int style, String imVideoToken) {
        this.imVideoToken = imVideoToken;
        joinRoom(teamImId,imVideoToken);
        showCpMicStatus(style, tips);
    }

    private void showDialog(boolean isAgreeCp, boolean isCpRoom, String roomId, String roomAnchor, String targetId, String cpOrderId, String cardCategory) {
        if (Constants.hide_peking_user) {
            BuyCoupleCardDialogFragmentTwo.newInstance()
                    .setCpCardInfo(isAgreeCp, isCpRoom, roomId, roomAnchor, targetId, cpOrderId, cardCategory)
                    .setBuyListener(new BuyCoupleCardDialogFragmentTwo.BuyCardListener() {
                        @Override
                        public void goRecharge() {
                            //跳转充值界面
                            mContext.startActivity(new Intent(mContext, RechargeActivity.class));
                        }

                        @Override
                        public void buyCardSuccess(boolean isAgree, String targetId, BuyCoupleCardInfo buyCoupleCardInfo) {
                            if (buyCoupleCardInfo != null && buyCoupleCardInfo.getData() != null && !TextUtils.isEmpty(buyCoupleCardInfo.getData().getCp_order_id())) {
                                InviteCoupleAttachment attachment = new InviteCoupleAttachment(buyCoupleCardInfo.getData().getCp_order_id(), "待确认", buyCoupleCardInfo.getData().getCp_card_desc(), 1,false);
                                IMMessage inviteMessage = MessageBuilder.createCustomMessage(
                                        sessionId, SessionTypeEnum.P2P, "激活超级cp", attachment
                                );
                                mContext.sendMessage(inviteMessage, false);
                            } else {
                                mContext.sendUpdateMsg(2, "");
                            }
                        }

                        @Override
                        public void buyCardMsg(String msg) {
                            mContext.showMessage(msg);
                        }

                        @Override
                        public void userTeamCardSuccess() {
                            if (!isOnMic()) {
                                mContext.showLoading();
                                mContext.checkPermission(BaseActivity.BASIC_PERMISSION_REQUEST_CODE);
                            }
                        }

                        @Override
                        public void userMicCardSuccess() {
                            if (!isOnMic()) {
                                mContext.showLoading();
                                mContext.checkPermission(BaseActivity.BASIC_PERMISSION_REQUEST_CODE);
                            }
                        }

                        @Override
                        public void onDismiss() {

                        }
                    }).show(mContext.getSupportFragmentManager(), BuyCoupleCardDialogFragmentTwo.class.getName());
        } else {
            BuyCoupleCardDialogFragment.newInstance()
                    .setBuy_position(buy_position)
                    .setCpCardInfo(isAgreeCp, isCpRoom, roomId, roomAnchor, targetId, cpOrderId, cardCategory)
                    .setBuyListener(new BuyCoupleCardDialogFragment.BuyCardListener() {
                        @Override
                        public void goRecharge() {
                            //跳转充值界面
                        }

                        @Override
                        public void buyCardSuccess(boolean isAgree, String targetId, BuyCoupleCardInfo buyCoupleCardInfo) {
                            if (buyCoupleCardInfo != null && buyCoupleCardInfo.getData() != null && !TextUtils.isEmpty(buyCoupleCardInfo.getData().getCp_order_id())) {
                                InviteCoupleAttachment attachment = new InviteCoupleAttachment(buyCoupleCardInfo.getData().getCp_order_id(), "待确认", buyCoupleCardInfo.getData().getCp_card_desc(), 1,false);
                                IMMessage inviteMessage = MessageBuilder.createCustomMessage(
                                        sessionId, SessionTypeEnum.P2P, "激活超级cp", attachment
                                );
                                mContext.sendMessage(inviteMessage, false);
                            } else {
                                mContext.sendUpdateMsg(2, "");
                            }
                        }

                        @Override
                        public void buyCardMsg(String msg) {
                            mContext.showMessage(msg);
                        }

                        @Override
                        public void userTeamCardSuccess() {
                            if (!isOnMic()) {
                                mContext.showLoading();
                                mContext.checkPermission(BaseActivity.BASIC_PERMISSION_REQUEST_CODE);
                            }
                        }

                        @Override
                        public void userMicCardSuccess() {
                            if (!isOnMic()) {
                                mContext.showLoading();
                                mContext.checkPermission(BaseActivity.BASIC_PERMISSION_REQUEST_CODE);
                            }
                        }

                        @Override
                        public void onDismiss() {

                        }

                        @Override
                        public void goRecharge(String position) {
                            if (fragment != null){
                                fragment.setPositon(position);
                                fragment.show(mContext.getSupportFragmentManager(), RechargeCoinDialog.class.getName());
                            }
                        }
                    }).show(mContext.getSupportFragmentManager(), BuyCoupleCardDialogFragment.class.getName());
        }
        MultiTracker.onEvent(TrackConstants.P2P_PAGE_WINDOW);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_cp_desc:
            case R.id.bg_click:
//                Intent intent = new Intent(mContext, CPLevelActivity.class);
//                intent.putExtra(Constants.USER_ID, sessionId);
//                intent.putExtra("user_head", avatarUrl);
//                intent.putExtra("name", name);
//                mContext.startActivity(intent);
                Router.INSTANCE.toCPLevelActivity(sessionId,avatarUrl,name);
                break;
            case R.id.tv_enter_room:
                if (CPMatchingGlobal.getInstance((SessionActivity) mContext).isMatching) {
                    Toast.makeText(mContext, mContext.getString(R.string.audio_match_float_ball_tip), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (mContext.mIsExpireFriPesrson()) {
                    return;
                }

                if (GiftPackageFloatManager.getInstance().checkGiftPackage(2)){
                    return;
                }

                HashMap<String, String> enterRooParams = new HashMap<>();
                enterRooParams.put("user_id", AccountUtil.getInstance().getUserId());
                enterRooParams.put("female_guest_id", sessionId);
                enterRooParams.put("tag", "connect_mic_ques");
                enterRooParams.put("event_time", String.valueOf(System.currentTimeMillis()));
                MultiTracker.onEvent(TrackConstants.B_P2P_MICROPHONE, enterRooParams);
                boolean result = FloatManager.getInstance().checkPermission(mContext);
                int status = (int) tv_enter_room.getTag();
                if (!result && !isLeftInRoom(memberList) && status == 5) {
                    new CommonDialogBuilder()
                            .setDesc(mContext.getString(R.string.float_title))
                            .setImageTitleRes(R.mipmap.icon_open_float)
                            .setConfirm(mContext.getString(R.string.float_open))
                            .setCancel(mContext.getString(R.string.float_cancel))
                            .setListener(new CommonActionListener() {
                                @Override
                                public void onConfirm() {
                                    try {
                                        if (Build.VERSION.SDK_INT < 23) {
                                            if (RomUtils.checkIsMiuiRom()) {
                                                MiuiUtils.applyMiuiPermission(mContext);
                                            } else if (RomUtils.checkIsMeizuRom()) {
                                                MeizuUtils.applyPermission(mContext);
                                            } else if (RomUtils.checkIsHuaweiRom()) {
                                                HuaweiUtils.applyPermission(mContext);
                                            } else if (RomUtils.checkIs360Rom()) {
                                                QikuUtils.applyPermission(mContext);
                                            } else if (RomUtils.checkIsOppoRom()) {
                                                OppoUtils.applyOppoPermission(mContext);
                                            }
                                        } else {
                                            if (RomUtils.checkIsMeizuRom()) {
                                                MeizuUtils.applyPermission(mContext);
                                            } else {
                                                FloatManager.commonROMPermissionApplyInternal(mContext);
                                            }
                                        }
                                    } catch (Throwable e) {
                                        e.printStackTrace();
                                    }
                                    HashMap<String, String> params = new HashMap<>();
                                    params.put("user_id", AccountUtil.getInstance().getUserId());
                                    params.put("female_guest_id", sessionId);
                                    params.put("type", "click_microphone");
                                    params.put("tag", "connect_mic_ques");
                                    MultiTracker.onEvent(TrackConstants.B_FLOAT_OPEN, params);
                                }

                                @Override
                                public void onCancel() {
                                    try {
                                        handEnterRoom();
                                        HashMap<String, String> params = new HashMap<>();
                                        params.put("user_id", AccountUtil.getInstance().getUserId());
                                        params.put("female_guest_id", sessionId);
                                        params.put("type", "click_microphone");
                                        params.put("tag", "connect_mic_ques");
                                        MultiTracker.onEvent(TrackConstants.B_FLOAT_CANCEL, params);
                                    } catch (Throwable e) {
                                        e.printStackTrace();
                                    }
                                }
                            }).create().show(mContext.getSupportFragmentManager(), CommonDialogFragment.class.getSimpleName());

                } else {
                    handEnterRoom();
                    if (mMicGuideView != null) {
                        mMicGuideView.dismiss();
                    }
                    mContext.setCpStatus(false, true);
                }
                break;
            case R.id.ll_game_tip:
                ll_game_tip.post(new Runnable() {
                    @Override
                    public void run() {
                        int maxLines = tv_game_content.getMaxLines();
                        tv_game_content.setMaxLines(maxLines == 1 ? 3 : 1);
                        tv_game_tip.setText(maxLines == 1 ? mContext.getString(R.string.session_guard_game_close) : mContext.getString(R.string.session_guard_game_open));
                        JinquanResourceHelper.getInstance(mContext).setImageResourceByAttr(iv_game_tip, maxLines == 1 ? R.attr.custom_bg_game_tip_close : R.attr.custom_bg_game_tip_open);
                    }
                });
                break;
            case R.id.upgrade_cp_iv:
                if (ViewUtils.isFastClick()) {
                    return;
                }
                jumpToDeclaration();
                break;
        }
    }

    private void jumpToDeclaration() {
        checkCpLevelDetail();
    }

    private void checkCpLevelDetail() {
        try {
            JSONObject object = new JSONObject();
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("female_user_id", sessionId);
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.checkCpLevelDetail(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    CpLevelUpDetail cpLevelUpDetail = GT.fromJson(result, CpLevelUpDetail.class);
                    if (cpLevelUpDetail != null) {
                        if (cpLevelUpDetail.getCode() == 0) {
                            CpLevelUpDetail.DataBean dataBean = cpLevelUpDetail.getData();
                            if (dataBean != null) {
                                boolean level_swear = dataBean.isLevel_swear();
                                if (level_swear) {
                                    //代表自己发送过  跳转到 详情
                                    Bundle bundle = new Bundle();
                                    bundle.putString("female_user_id", sessionId);
                                    bundle.putString("female_user_head", avatarUrl);
                                    bundle.putString("female_user_name", name);
                                    bundle.putString("rise_to", nextRankName);
                                    bundle.putString("intimacy", intimacy_value.getText().toString());
                                    Intent intent1 = new Intent(mContext, DeclarationDetailActivity.class);
                                    intent1.putExtras(bundle);
                                    mContext.startActivity(intent1);


                                } else {
                                    Intent intent1 = new Intent(mContext, DeclarationActivity.class);
                                    Bundle bundle1 = new Bundle();
                                    bundle1.putString(Constants.SESSION_ID, sessionId);
                                    bundle1.putString("target_user_head", avatarUrl);
                                    bundle1.putString("rise_to", nextRankName);
                                    bundle1.putString("intimacy", intimacy_value.getText().toString());
                                    intent1.putExtras(bundle1);
                                    mContext.startActivity(intent1);
                                }

                                HashMap<String, String> params = new HashMap<>();
                                params.put("user_id", AccountUtil.getInstance().getUserId());
                                params.put("female_guest_id", sessionId);
                                params.put("event_time", String.valueOf(System.currentTimeMillis()));
                                params.put("rise_to", nextRankName);
                                params.put("intimacy", intimacy_value.getText().toString());
                                MultiTracker.onEvent(TrackConstants.B_BUTTON_RISE_CP_LEVEL, params);
                            }
                        } else if (cpLevelUpDetail.getCode() == 20463) {
                            // 您已经晋级此等级
                            String msg = cpLevelUpDetail.getMsg();
                            if (StringUtil.isNotEmpty(msg)) {
                                Toast.makeText(ContextUtils.getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            String msg = cpLevelUpDetail.getMsg();
                            if (com.liquid.base.tools.StringUtil.isNotEmpty(msg)) {
                                Toast.makeText(ContextUtils.getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }

                @Override
                public void OnFailed(int ret, String result) {
                }
            });
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    /**
     * 离开语音房间
     */
    @Override
    public void leaveRoom() {
        try {
            if (mAvManager != null) {
                HashMap<String, String> params = new HashMap<>();
                params.put("user_id", AccountUtil.getInstance().getUserId());
                params.put("female_guest_id", sessionId);
                params.put("tag", "connect_mic_ques");
                params.put("event_time", String.valueOf(System.currentTimeMillis()));
                MultiTracker.onEvent("leave_room_stop_audio_link", params);
                mAvManager.stopAudioLink();
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        closeOrLeaveTeam(false);
    }

    private void handEnterRoom() {
        int status = (int) tv_enter_room.getTag();
        Logger.e("handEnterRoom  " + status);
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", AccountUtil.getInstance().getUserId());
        params.put("female_guest_id", sessionId);
        params.put("tag", "connect_mic_ques");
        params.put("status", String.valueOf(status));
        params.put("event_time", String.valueOf(System.currentTimeMillis()));
        MultiTracker.onEvent("hand_enter_room", params);
        switch (status) {
            case 1://未组CP
            case 2:
            case 3://CP已结束
            case 4://未体验专属语音房间
                showCpCardDialog(false, false, "", "", sessionId, "", CardCategoryEnum.TEAM_CARD.getCode());
                break;
            case 5://已购买专属语音房
                if (isLeftInRoom(memberList)) {
                    if (isRightInRoom(memberList) && !isHost) {
                        try {
                            if (mAvManager != null) {
                                mAvManager.setMicrophoneMute(true);
                            }
                        } catch (Throwable e) {
                            e.printStackTrace();
                        }
                        closeOrLeaveTeam(false);
                    } else {
                        HashMap<String, String> leftParams = new HashMap<>();
                        leftParams.put("user_id", AccountUtil.getInstance().getUserId());
                        leftParams.put("female_guest_id", sessionId);
                        leftParams.put("tag", "connect_mic_ques");
                        leftParams.put("event_time", String.valueOf(System.currentTimeMillis()));
                        MultiTracker.onEvent("click_leave_room", leftParams);
                        leaveRoom();
                    }
                } else {
                    //检测连麦权限
                    mContext.showLoading();
                    mContext.checkPermission(BaseActivity.BASIC_PERMISSION_REQUEST_CODE);
                }
                break;
        }
    }

    /**
     * 关闭或者离开聊天室
     *
     * @param error 异常掉麦，重新进来，重置状态 true 为重置  false 为正常关闭
     */
    private void closeOrLeaveTeam(boolean error) {
        if (isHost) {
            mContext.getPresenter().closeTeam(currentTeamId, error);
            mContext.sendUpdateMsg(3, "");
        } else {
            mContext.getPresenter().quitTeam(currentTeamId, error);
            mContext.sendUpdateMsg(1, "");
        }
    }

    private boolean isClose = false;

    @Override
    public void closeConnection() {
        if (!isClose) {
            mContext.unbindService(mConnection);
            isClose = true;
        }
    }

    @Override
    public String getCurrentTeamId() {
        return currentTeamId;
    }

    /**
     * 加入语音房间
     *
     * @param teamImId
     */
    private void joinRoom(String teamImId,String imVideoToken) {
        try {
            if (mAvManager != null && !mAvManager.isAvRunning()
                    && StringUtil.isNotEmpty(teamImId) && StringUtil.isNotEmpty(imVideoToken)) {
                mAvManager.startAudioLink(teamImId,imVideoToken,sessionId, avatarUrl, isHost);
                joinMicTime = System.currentTimeMillis();
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        MultiTracker.onEvent(TrackConstants.B_P2P_MICROPHONE_SUCCESS);

    }

    @Override
    public void setMicMute(boolean mute) {
        try {
            if (mAvManager != null) {
                HashMap<String, String> params = new HashMap<>();
                params.put("user_id", AccountUtil.getInstance().getUserId());
                params.put("female_guest_id", sessionId);
                params.put("tag", "connect_mic_ques");
                params.put("mute", mute ? "true" : "false");
                MultiTracker.onEvent("set_microphone_mute", params);
                mAvManager.setMicrophoneMute(mute);
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    /**
     * 更新音量水波纹
     *
     * @param speakers
     */
    @Override
    public void onAudioVolume(Map<String, Integer> speakers) {
        if (speakers == null){
            return;
        }
        if (speakers.get(AccountUtil.getInstance().getUserId()) != null && isLeftInRoom(memberList)) {
            Long time = volumeMap.get(AccountUtil.getInstance().getUserId());
            int volume = speakers.get(AccountUtil.getInstance().getUserId());
            if (time == null || System.currentTimeMillis() - time > 2000){
                if (volume == 0) {
                    anim_cp_user_head.setVisibility(View.GONE);
                    anim_cp_user_head.stopWaveAnimation();
                } else {
                    if (anim_cp_user_head.getVisibility() == View.GONE) {
                        anim_cp_user_head.setVisibility(View.VISIBLE);
                    }
                    anim_cp_user_head.startWaveAnimation();
                }
                volumeMap.put(AccountUtil.getInstance().getUserId(),System.currentTimeMillis());
            }
        }

        if (speakers.get(sessionId) != null) {
            Long time = volumeMap.get(sessionId);
            if (time == null || System.currentTimeMillis() - time > 2000){
                int volume = speakers.get(sessionId);
                if (volume == 0) {
                    anim_cp_target_user_head.setVisibility(View.GONE);
                    anim_cp_target_user_head.stopWaveAnimation();
                } else {
                    if (anim_cp_target_user_head.getVisibility() == View.GONE) {
                        anim_cp_target_user_head.setVisibility(View.VISIBLE);
                    }
                    anim_cp_target_user_head.startWaveAnimation();
                }
                volumeMap.put(sessionId,System.currentTimeMillis());
            }
        }
    }

    @Override
    public void showMicGuide() {
        if (mContext.isFinishing() || mContext.isDestroyed()) return;
        if (!tv_enter_room.isSelected()) {
            if (mMicGuideView == null) {
                View contentView = LayoutInflater.from(mContext).inflate(R.layout.pop_guard_cp_tip_dialog, null);
                mMicGuideView = new PopupWindow(contentView,
                        ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);
                mMicGuideView.setOutsideTouchable(false);
                mMicGuideView.setFocusable(false);
                mMicGuideView.setContentView(contentView);
                mMicGuideView.getContentView().measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
                ImageView iv_guard_cp_tip = contentView.findViewById(R.id.iv_guard_cp_tip);
                View iv_sure = contentView.findViewById(R.id.iv_sure);
                iv_guard_cp_tip.setBackgroundResource(R.mipmap.icon_invite_mic_tip);
                iv_sure.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mContext.setCpStatus(false, true);
                        mMicGuideView.dismiss();
                    }
                });
            }
            mContext.getHandler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (mContext.isDestroyed() || mContext.isFinishing()) {
                        return;
                    }
                    mMicGuideView.showAsDropDown(tv_enter_room, -mMicGuideView.getContentView().getMeasuredWidth() + tv_enter_room.getWidth() + ScreenUtil.dip2px(20), 0);
                }
            }, 500);
        }
    }

    @Override
    public void updateScore(ScoreChangeAttachment attachment) {
        if (!canUpdateUI()) {
            return;
        }
        String cpScore = attachment.getScore();
        if (intimacy_value != null) {
            intimacy_value.setText(getFloatData(cpScore) + "");
        }
    }


    @Override
    public void updateLevel(LevelChangeAttachment level) {
        if (!canUpdateUI()) {
            return;
        }
        Glide.with(mContext)
                .load(level.getRank_icon())
                .into(level_icon);
        level_tv.setText(level.getName());

        try {
            intimacy_value.setText(getFloatData(level.getCp_score()) + "");
        } catch (Exception e) {
            e.printStackTrace();
        }

        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", AccountUtil.getInstance().getUserId());
        params.put("female_guest_id", sessionId);
        params.put("event_time", String.valueOf(System.currentTimeMillis()));
        params.put("dan", level.getName());
        params.put("level", level.getRank());
        params.put("intimacy", level.getCp_score());
        MultiTracker.onEvent(TrackConstants.B_EXPOSURE_POPUP_LOW_LEVEL_UP, params);
    }

    @Override
    public void updateLevelDialog(String level) {
        if (!canUpdateUI()) {
            return;
        }
        try {
            if (coupleLevelDialogFragment == null) {
                coupleLevelDialogFragment = CoupleLevelDialogFragment.newInstance();
            }
            coupleLevelDialogFragment.setUserInfo(leftHead, AccountUtil.getInstance().getUserInfo().getAvatar_url(), level).show(mContext.getSupportFragmentManager(), CoupleLevelDialogFragment.class.getSimpleName());
            mContext.getPresenter().checkRankRaise(sessionId);
        } catch (Exception e) {
            e.printStackTrace();
        }

        mContext.getPresenter().getUserStatusInfo(sessionId);
    }

    @Override
    public void updateCpUpgradeDialog(MsgUserModel.P2PRankUpgradeAwardInfo info) {
        showUpgradeDialog(info);
    }

    @Override
    public void doTask() {

    }

    //绑定生命周期
    @Override
    public void onPause() {
        try {
            boolean isRunning = mAvManager != null && mAvManager.isAvRunning();
            if (isRunning && isOnMic() && StringUtil.isEquals(sessionId, FloatManager.getInstance().getSessionId())) {
                FloatManager.getInstance().showMenuWindow();
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        if (StringUtil.isEquals(sessionId, FloatManager.getInstance().getSessionId())) {
            FloatManager.getInstance().hideMenuWindow();
        }
    }

    private void releaseTimer() {
        if (mTimer1 != null) {
            if (mTask1 != null) {
                mTask1.cancel();
                mTask1 = null;
            }
            mTimer1.cancel();
            mTimer1 = null;
        }
    }

    @Override
    public void onDestroy() {
        releaseTimer();
    }

    private float getFloatData(String value) {
        float data;
        try {
            data = new BigDecimal(value).setScale(1, BigDecimal.ROUND_DOWN).floatValue();
        } catch (Exception e) {
            data = 0f;
            e.printStackTrace();
        }
        return data;
    }

}
