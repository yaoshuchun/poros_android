package com.liquid.poros.ui.fragment.dialog.pta;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.liquid.base.adapter.CommonAdapter;
import com.liquid.base.adapter.CommonViewHolder;
import com.liquid.poros.R;
import com.liquid.poros.base.BaseDialogFragment;
import com.liquid.poros.entity.PtaShopCartInfo;
import com.liquid.poros.utils.ScreenUtil;
import com.liquid.poros.utils.ViewUtils;
import java.util.ArrayList;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * 个人形象购物车
 */
public class PtaShopCartDialogFragment extends BaseDialogFragment implements View.OnClickListener {
    private TextView tv_user_coin;
    private RecyclerView rv_pta;
    private TextView tv_pta_pay_coin;
    private TextView tv_pta_pay;
    private CommonAdapter<PtaShopCartInfo.Data.PtaShopItem> mCommonAdapter;
    private CommonAdapter<PtaShopCartInfo.Data.PtaShopItem.PtaDurationItem> mPtaDurationItemAdapter;
    private ArrayList<PtaShopCartInfo.Data.PtaShopItem> mPtaShopItemList = new ArrayList<>();
    //装扮选中集合
    private ArrayList<PtaShopCartInfo.Data.PtaShopItem> mPtaSelectItemList = new ArrayList<>();

    public static PtaShopCartDialogFragment newInstance() {
        PtaShopCartDialogFragment fragment = new PtaShopCartDialogFragment();
        return fragment;
    }

    public PtaShopCartDialogFragment setPtaShopList(ArrayList<PtaShopCartInfo.Data.PtaShopItem> ptaShopItemList) {
        mPtaShopItemList.clear();
        mPtaShopItemList.addAll(ptaShopItemList);
        return this;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        Window win = getDialog().getWindow();
        // 一定要设置Background，如果不设置，window属性设置无效
        win.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams params = win.getAttributes();
        params.width = ScreenUtil.getDisplayWidth();
        params.height = ScreenUtil.getDisplayWidth() * 3 / 2;
        params.gravity = Gravity.BOTTOM;
        win.setAttributes(params);
    }

    @Override
    protected String getPageId() {
        return null;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_pta_shop_cart_dialog, null);
        initView(view);
        builder.setView(view);
        AlertDialog alertDialog = builder.create();
        return alertDialog;
    }

    private void initView(View view) {
        tv_user_coin = view.findViewById(R.id.tv_user_coin);
        rv_pta = view.findViewById(R.id.rv_pta);
        tv_pta_pay_coin = view.findViewById(R.id.tv_pta_pay_coin);
        tv_pta_pay = view.findViewById(R.id.tv_pta_pay);
        tv_pta_pay.setOnClickListener(this);
        mPtaSelectItemList.clear();
        for (int i = 0; i < mPtaShopItemList.size(); i++) {
            if (mPtaShopItemList.get(i).isSelect()) {
                mPtaSelectItemList.add(mPtaShopItemList.get(i));
            }
        }
        updatePtaPayCount();
        mCommonAdapter = new CommonAdapter<PtaShopCartInfo.Data.PtaShopItem>(getContext(), R.layout.item_pta_shop) {
            @Override
            public void convert(CommonViewHolder holder, final PtaShopCartInfo.Data.PtaShopItem data, int position) {
                ImageView item_iv_pta_check = holder.getView(R.id.item_iv_pta_check);
                holder.setText(R.id.item_tv_pta_name, data.getPta_title());
                holder.setText(R.id.tv_pta_pay_coin, data.getPta_coin());
                RecyclerView rv_pta_time = holder.getView(R.id.rv_pta_time);
                if (data.isSelect()) {
                    item_iv_pta_check.setBackgroundResource(R.mipmap.icon_pta_shop_select);
                } else {
                    item_iv_pta_check.setBackgroundResource(R.mipmap.icon_pta_shop_normal);
                }
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (data.isSelect()) {
                            data.setSelect(false);
                            mPtaSelectItemList.remove(data);
                            //装扮条目取消选中后，默认天数取消选中状态
                            for (int i = 0; i < data.getPtaDuration().size(); i++) {
                                data.getPtaDuration().get(i).setPtaDurationSelect(false);
                            }
                        } else {
                            data.setSelect(true);
                            if (!mPtaSelectItemList.contains(data)) {
                                mPtaSelectItemList.add(data);
                            }
                            //装扮条目选中后，默认天数选中第一项
                            for (int i = 0; i < data.getPtaDuration().size(); i++) {
                                data.getPtaDuration().get(0).setPtaDurationSelect(true);
                            }
                        }
                        updatePtaPayCount();
                        notifyDataSetChanged();
                        mPtaDurationItemAdapter.notifyDataSetChanged();
                    }
                });
                mPtaDurationItemAdapter = new CommonAdapter<PtaShopCartInfo.Data.PtaShopItem.PtaDurationItem>(getContext(), R.layout.item_pta_shop_duration) {
                    @Override
                    public void convert(CommonViewHolder commonViewHolder, PtaShopCartInfo.Data.PtaShopItem.PtaDurationItem ptaDurationItem, int pos) {
                        LinearLayout item_ll_pta_duration = commonViewHolder.getView(R.id.item_ll_pta_duration);
                        TextView item_tv_pta_duration = commonViewHolder.getView(R.id.item_tv_pta_duration);
                        item_tv_pta_duration.setText(ptaDurationItem.getPtaDay() + "天");
                        if (ptaDurationItem.isPtaDurationSelect()) {
                            item_ll_pta_duration.setBackgroundResource(R.drawable.bg_gradual_fc6573_22);
                            item_tv_pta_duration.setTextColor(Color.parseColor("#ffffff"));
                        } else {
                            item_ll_pta_duration.setBackgroundResource(R.drawable.bg_color_f5f7fa_22);
                            item_tv_pta_duration.setTextColor(Color.parseColor("#CBCDD3"));
                        }
                        item_ll_pta_duration.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                for (int i = 0; i < data.getPtaDuration().size(); i++) {
                                    if (pos == i) {
                                        data.getPtaDuration().get(i).setPtaDurationSelect(true);
                                        mCommonAdapter.notifyDataSetChanged();
                                        //选中天数时，默认选中当前装扮条目
                                        data.setSelect(true);
                                        if (!mPtaSelectItemList.contains(data)) {
                                            mPtaSelectItemList.add(data);
                                        }
                                        updatePtaPayCount();
                                    } else {
                                        data.getPtaDuration().get(i).setPtaDurationSelect(false);
                                    }
                                }
                                notifyDataSetChanged();
                            }
                        });
                    }
                };
                rv_pta_time.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL, false));
                mPtaDurationItemAdapter.update(data.getPtaDuration());
                rv_pta_time.setAdapter(mPtaDurationItemAdapter);
            }
        };
        rv_pta.setLayoutManager(new LinearLayoutManager(getActivity()));
        mCommonAdapter.update(mPtaShopItemList);
        rv_pta.setAdapter(mCommonAdapter);
    }
    private void updatePtaPayCount() {
        tv_pta_pay.setText(mPtaSelectItemList.size() == 0 ? getString(R.string.pta_shop_pay_count_zero) : String.format(getString(R.string.pta_shop_pay_count), mPtaSelectItemList.size()));
    }

    @Override
    public void onClick(View v) {
        if (ViewUtils.isFastClick()) {
            return;
        }
        switch (v.getId()) {
            case R.id.tv_pta_pay:
                if (mPtaSelectItemList.size() == 0) {
                    Toast.makeText(getActivity(), getString(R.string.pta_shop_select_empty), Toast.LENGTH_SHORT).show();
                    return;
                }
                break;
        }
    }
}
