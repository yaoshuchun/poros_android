package com.liquid.poros.ui.activity.live.view

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import android.widget.Toast
import com.liquid.base.utils.ToastUtil
import com.liquid.poros.R
import com.liquid.poros.analytics.utils.MultiTracker
import com.liquid.poros.constant.TrackConstants
import com.liquid.poros.entity.CoupleRoom
import com.liquid.poros.entity.MicUserInfo
import com.liquid.poros.ui.activity.audiomatch.CPMatchingGlobal
import com.liquid.poros.ui.activity.live.RoomLiveActivityV2
import com.liquid.poros.ui.activity.live.RoomLiveActivityV2.Companion.ROOM_TYPE_PK
import com.liquid.poros.ui.activity.live.viewmodel.RoomLiveViewModel
import com.liquid.poros.ui.floatball.GiftPackageFloatManager
import com.liquid.poros.utils.ViewUtils
import kotlinx.android.synthetic.main.activity_room_live_new.*
import kotlinx.android.synthetic.main.live_room_apply_mic.view.*
import java.util.*

/**
 * 视频房申请上麦View
 */
class LiveRoomApplyMicView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    private var mRoomInfo: CoupleRoom? = null
    private var mRoomId: String? = null //视频房ID
    private var micUserInfos: Array<MicUserInfo?>? = null
    private val roomLiveViewModel: RoomLiveViewModel by lazy {
        (context as RoomLiveActivityV2).getActivityViewModel(RoomLiveViewModel::class.java)
    }

    init {
        inflate(context, R.layout.live_room_apply_mic, this)
        setBackgroundResource(R.drawable.bg_gradual_14d4d6_24)
        setOnClickListener {
            if (ViewUtils.isFastClick()){
                return@setOnClickListener
            }

            if (CPMatchingGlobal.getInstance(context as RoomLiveActivityV2)!!.isMatching) {
                Toast.makeText(context, context.getString(R.string.audio_match_float_ball_tip), Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (micUserInfos?.get(1) != null) {
                ToastUtil.show(context.getString(R.string.live_room_input_on_mic_tip))
            }
            if (GiftPackageFloatManager.getInstance().checkGiftPackage(3)) {
                return@setOnClickListener
            }
            if (mRoomInfo?.request_onmic == true) {
                roomLiveViewModel.requestCP(mRoomId)
            } else {
                setBackgroundResource(R.drawable.bg_gradual_ff8080_24)
                iv_apply_mic!!.visibility = View.GONE
                tv_apply_mic_title!!.text = context.getString(R.string.live_room_apply_mic_success)
            }
            val applyMicParams = HashMap<String?, String?>()
            applyMicParams["female_guest_id"] = micUserInfos?.get(2)?.user_id ?: ""
            applyMicParams["live_room_id"] = mRoomId
            MultiTracker.onEvent(TrackConstants.B_APPLY_ON_MIC_CLICK, applyMicParams)
        }
        roomLiveViewModel.cpSuccessLiveData.observe(context as RoomLiveActivityV2) {
            context.closeLoading()
            onApplyMicSuccess()
            ToastUtil.show(it.msg)
        }
    }

    /**
     * 设置数据
     */
    fun setData(roomInfo: CoupleRoom, roomId: String, micUsers: Array<MicUserInfo?>) {
        mRoomInfo = roomInfo
        mRoomId = roomId
        micUserInfos = micUsers
        tv_apply_mic_tip!!.text = if (roomInfo.has_mic_card) context.getString(R.string.live_room_apply_mic_free) else String.format("(上麦 %s 金币)", roomInfo.mic_card.ori_price)
        if (roomInfo.request_onmic) {
            tv_apply_mic_title!!.text = context.getString(R.string.live_room_apply_mic)
            setBackgroundResource(R.drawable.bg_gradual_14d4d6_24)
        } else {
            tv_apply_mic_title!!.text = context.getString(R.string.live_room_apply_mic_success)
            setBackgroundResource(R.drawable.bg_gradual_ff8080_24)
        }
        if (roomInfo.room_type != ROOM_TYPE_PK) {
            iv_apply_mic!!.visibility = if (roomInfo.request_onmic) View.VISIBLE else View.GONE
        }
    }

    private fun onApplyMicSuccess() {
        //申请上麦成功
        setBackgroundResource(R.drawable.bg_gradual_ff8080_24)
        iv_apply_mic!!.visibility = View.GONE
        tv_apply_mic_title!!.text = context.getString(R.string.live_room_apply_mic_success)
    }
}