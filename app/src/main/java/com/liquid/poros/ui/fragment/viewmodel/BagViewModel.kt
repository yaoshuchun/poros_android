package com.liquid.poros.ui.fragment.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.liquid.base.utils.ToastUtil
import com.liquid.library.retrofitx.RetrofitX
import com.liquid.poros.analytics.utils.MultiTracker
import com.liquid.poros.entity.*
import com.liquid.poros.http.ApiUrl
import com.liquid.poros.http.observer.ListObjectObserver
import com.liquid.poros.http.observer.ObjectObserver
import com.liquid.poros.http.utils.postPoros
import com.liquid.poros.utils.AccountUtil
import com.liquid.poros.utils.StringUtil
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import org.json.JSONException
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList


/**
 * 视频房ViewModel
 */
class BagViewModel : ViewModel() {
    val bagData: MutableLiveData<Bag> = MutableLiveData()
    val sendBagGiftData: MutableLiveData<Pair<BagBean,Int>> = MutableLiveData()
    val fastSendBagGiftData: MutableLiveData<BaseBean> by lazy { MutableLiveData() }

    fun getBagData(roomId: String){
        RetrofitX.url(ApiUrl.GIFT_BAG)
                .param("room_id",roomId)
                .postPoros()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : ObjectObserver<Bag>(){

                    override fun success(result: Bag) {
                        bagData.postValue(result)
                    }

                    override fun failed(result: Bag) {
                        ToastUtil.show(result.msg)
                    }
                })
    }

    fun sendBagGift(bagBean: BagBean,targetUserId:String,num:Int = 1,roomId:String){
        RetrofitX.url(ApiUrl.GIFT_BAG_SEND_GIFT)
                .param("record_id",bagBean.record_id)
                .param("num",num)
                .param("target_user_id",targetUserId)
                .param("room_id",roomId)
                .postPoros()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : ObjectObserver<BaseBean>(){

                    override fun success(result: BaseBean) {
                        sendBagGiftData.postValue(Pair(bagBean,num))
                    }

                    override fun failed(result: BaseBean) {
                        ToastUtil.show(result.msg)
                    }
                })
    }

    /**
     * 一键送出背包礼物
     */
    fun fastSendBagGift(record_id: Int,targetUserId:String,num:Int = 1){
        RetrofitX.url(ApiUrl.GIFT_BAG_SEND_GIFT_FAST)
                .param("record_id",record_id)
                .param("target_user_id",targetUserId)
                .postPoros()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : ObjectObserver<BaseBean>(){

                    override fun success(result: BaseBean) {
                        fastSendBagGiftData.postValue(result)
                    }

                    override fun failed(result: BaseBean) {
                        ToastUtil.show(result.msg)
                    }
                })
    }
}