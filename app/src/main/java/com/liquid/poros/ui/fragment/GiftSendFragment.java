package com.liquid.poros.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import de.greenrobot.event.EventBus;
import de.greenrobot.event.Subscribe;
import de.greenrobot.event.ThreadMode;

import com.liquid.base.event.EventCenter;
import com.liquid.poros.R;
import com.liquid.poros.base.BaseFragment;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.entity.MicUserInfo;
import com.liquid.poros.entity.MsgUserModel;
import com.liquid.poros.ui.activity.user.event.LoginEvents;
import com.liquid.poros.ui.dialog.MultiGiftSendDialog;
import com.liquid.poros.ui.dialog.SendGiftView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class GiftSendFragment extends BaseFragment {
    private SendGiftView giftView;
    private Context context;
    private ArrayList<MsgUserModel.GiftBean> giftBeans;
    private ArrayList<MsgUserModel.GiftNums> giftNums;
    private boolean mIsGiftPresenterVisible;
    private String from;
    private String giftSource;
    private boolean toAnchor;
    private boolean showAddFriendButton;
    private MicUserInfo[] micUserInfos;
    private int pos;
    private OnTouchListener mListener;
    private String mRoomId;
    private int mRoomType;
    private String femaleGuestId;
    private String giftChannel;
    public static GiftSendFragment newInstance(Bundle arguments) {
        GiftSendFragment fragment = new GiftSendFragment();
        fragment.setArguments(arguments);
        return fragment;
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    protected String getPageId() {
        return TrackConstants.B_CHAT_GIFT_BAR_EXPOSURE;
    }

    @Override
    protected void onFirstUserVisible() {

    }

    @Override
    protected void onUserVisible() {

    }

    @Override
    protected void onUserInvisible() {

    }

    @Override
    protected View getLoadingTargetView() {
        return null;
    }

    @Override
    protected void initViewsAndEvents(@Nullable Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        if (bundle != null) {
            giftBeans = (ArrayList<MsgUserModel.GiftBean>) bundle.getSerializable(MultiGiftSendDialog.GIFT_BEANS);
            giftNums = (ArrayList<MsgUserModel.GiftNums>) bundle.getSerializable(MultiGiftSendDialog.GIFT_NUMS);
            femaleGuestId =  bundle.getString(Constants.FEMALE_GUEST_ID);
            giftChannel =  bundle.getString(MultiGiftSendDialog.GIFT_CHANNEL);
            giftView = mainLayout.findViewById(R.id.send_gift);
            giftView.setListener(new SendGiftView.OnSendGiftClickListener() {
                @Override
                public void sendGift(int giftId, @org.jetbrains.annotations.Nullable String icon, int num, @org.jetbrains.annotations.Nullable String audio, @NotNull String name, @org.jetbrains.annotations.Nullable String json, int toPos,String source) {
                    if (getParentFragment() != null) {
                        ((MultiGiftSendDialog)getParentFragment()).sendGift(giftId,icon,num,audio,name,json,toPos);
                    }
                }

                @Override
                public void jumpRecharge() {

                }

                @Override
                public void jumpRecharge(@org.jetbrains.annotations.Nullable String position) {

                }
            });
            giftView.setSessionId(femaleGuestId);
            giftView.setSource(giftChannel);
            giftView.setGiftPresenterBtnVisibly(mIsGiftPresenterVisible);
            giftView.setUserInfo(micUserInfos, toAnchor, showAddFriendButton, pos, mRoomType);
            giftView.setData(giftBeans,giftNums);
            giftView.setRoomId(mRoomId);
            giftView.setCustomListener(new SendGiftView.CustomTouchListener() {
                @Override
                public void giftSelect(int select) {
                    if (mListener != null) {
                        mListener.giftSelect(select);
                    }
                }

                @Override
                public void onTouch() {
                    if(mListener != null) {
                        mListener.onTouch();
                    }
                }
            });
            giftView.setIsLiveRoom(false);
            giftView.setIsTrial(true);
        }
    }

    @Subscribe(threadMode = ThreadMode.MainThread)
    public void onEventBus(EventCenter eventCenter) {
      switch (eventCenter.getEventCode()){
          case  Constants.EVENT_CODE_ENTRY_BLIND_BOX:
              int blindBoxType = (int) eventCenter.getData();
              if (blindBoxType == 0) {
                  giftView.setBlindBoxPosition(2);
              } else if (blindBoxType == 1) {
                  giftView.setBlindBoxPosition(3);
              } else {
                  giftView.setBlindBoxPosition(4);
              }
              giftView.setSource("blind_box_introduce");//盲盒礼物点击渠道 - 盲盒介绍弹窗
              break;
      }
    }

    public void setData(List<MsgUserModel.GiftBean> giftBeans, List<MsgUserModel.GiftNums> giftNums) {
        giftView.setData(giftBeans,giftNums);
    }

    public void setGiftPresenterVisibly(boolean isVisible) {
        mIsGiftPresenterVisible = isVisible;
        if (giftView != null)
        giftView.setGiftPresenterBtnVisibly(isVisible);
    }

    public void setRoomId(String roomId) {
        mRoomId = roomId;
        if (giftView != null) {
            giftView.setRoomId(roomId);
        }
    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.fragment_send_gift;
    }

    @Override
    public void notifyByThemeChanged() {

    }

    @Override
    public void notifyNetworkChanged(boolean isConnected) {

    }

    public void setListener(OnTouchListener listener) {
        mListener = listener;
    }

    public interface OnTouchListener {
        void onTouch();
        void giftSelect(int selectPos);
    }

    public void setUserInfo(MicUserInfo[] micUserInfos, boolean toAnchor,boolean showAddFriendButton,int pos,int mRoomType) {
        this.micUserInfos = micUserInfos;
        this.toAnchor = toAnchor;
        this.showAddFriendButton = showAddFriendButton;
        this.pos = pos;
        this.mRoomType = mRoomType;
        if (giftView != null) {
            giftView.setUserInfo(micUserInfos, toAnchor, showAddFriendButton, pos, mRoomType);
        }
    }
}
