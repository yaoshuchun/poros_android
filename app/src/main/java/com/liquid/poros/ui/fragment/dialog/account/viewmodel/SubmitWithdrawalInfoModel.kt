package com.liquid.poros.ui.fragment.dialog.account.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.liquid.library.retrofitx.RetrofitX
import com.liquid.poros.entity.BaseBean
import com.liquid.poros.http.ApiUrl
import com.liquid.poros.http.observer.ObjectObserver
import com.liquid.poros.http.utils.postPoros

class SubmitWithdrawalInfoModel : ViewModel() {
    val submitWithdrawalInfoSuccess: MutableLiveData<BaseBean> by lazy { MutableLiveData<BaseBean>() }
    val submitWithdrawalInfoFail: MutableLiveData<BaseBean> by lazy { MutableLiveData<BaseBean>() }

    /**
     * 实名认证
     */
    fun submitRealNameData(real_name: String, id_card: String, phone: String) {
        RetrofitX.url(ApiUrl.REAL_NAME_ID_CARD)
                .param("real_name", real_name)
                .param("id_card", id_card)
                .param("phone", phone)
                .postPoros()
                .subscribe(object : ObjectObserver<BaseBean>() {
                    override fun success(result: BaseBean) {
                        submitWithdrawalInfoSuccess.postValue(result)
                    }

                    override fun failed(result: BaseBean) {
                        submitWithdrawalInfoFail.postValue(result)
                    }
                })
    }
}