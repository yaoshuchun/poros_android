package com.liquid.poros.ui.activity.account

import android.os.Bundle
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.liquid.base.adapter.CommonAdapter
import com.liquid.base.adapter.CommonViewHolder
import com.liquid.poros.R
import com.liquid.poros.base.BaseActivity
import com.liquid.poros.constant.TrackConstants
import com.liquid.poros.entity.WithdrawalHistoryInfo
import com.liquid.poros.entity.WithdrawalHistoryInfo.withdrawalBean
import com.liquid.poros.ui.activity.account.viewmodel.WithdrawalHistoryViewModel
import com.liquid.poros.utils.StringUtil
import com.liquid.poros.utils.TimeUtils
import com.liquid.poros.widgets.pulltorefresh.BaseRefreshListener
import com.liquid.poros.widgets.pulltorefresh.ViewStatus
import kotlinx.android.synthetic.main.activity_withdrawal_history.*

/**
 * 提现历史界面
 */
class WithdrawalHistoryActivity : BaseActivity(), BaseRefreshListener {
    private var mCommonAdapter: CommonAdapter<withdrawalBean>? = null
    private var page = 0
    private var withdrawalHistoryViewModel: WithdrawalHistoryViewModel? = null
    override fun parseBundleExtras(extras: Bundle) {}
    override fun getContentViewLayoutID(): Int {
        return R.layout.activity_withdrawal_history
    }

    override fun getPageId(): String {
        return TrackConstants.PAGE_WITHDRAWAL_HISTORY
    }

    override fun needSetTransparent(): Boolean {
        return false
    }

    override fun initViewsAndEvents(savedInstanceState: Bundle?) {
        rv_withdrawal_history!!.layoutManager = LinearLayoutManager(this)
        mCommonAdapter = object : CommonAdapter<withdrawalBean>(this, R.layout.item_recharge_details) {
            override fun convert(holder: CommonViewHolder, withdrawalBean: withdrawalBean, pos: Int) {
                holder.setText(R.id.tv_name, withdrawalBean.desc)
                holder.setText(R.id.tv_time, withdrawalBean.time_stamp?.toLong()?.let { TimeUtils.getTimeShowString(it, false) })
                val tv_coin_count = holder.getView<TextView>(R.id.tv_coin_count)
                if (withdrawalBean.withdraw_status==1){
                    tv_coin_count.setTextColor(resources.getColor(R.color.cyan))
                    tv_coin_count.text = StringUtil.matcherSearchText(true, resources.getColor(R.color.cyan),"${withdrawalBean.count}${withdrawalBean.unit}", withdrawalBean.unit)
                }else{
                    tv_coin_count.setTextColor(resources.getColor(R.color.tv_status_busy))
                    tv_coin_count.text = StringUtil.matcherSearchText(true, resources.getColor(R.color.tv_status_busy),"${withdrawalBean.count}${withdrawalBean.unit}", withdrawalBean.unit)
                }
            }
        }
        rv_withdrawal_history!!.adapter = mCommonAdapter
        container!!.setRefreshListener(this)
        container!!.setCanLoadMore(true)
        withdrawalHistoryViewModel = getActivityViewModel(WithdrawalHistoryViewModel::class.java)
        withdrawalHistoryViewModel?.getWithdrawalHistoryListData(page)
        withdrawalHistoryViewModel?.withdrawalHistorySuccess?.observe(this, Observer { withdrawalHistoryInfo: WithdrawalHistoryInfo? ->
            container!!.finishRefresh()
            container!!.finishLoadMore()
            if (page == 0 && (withdrawalHistoryInfo?.user_balance_record_list == null || withdrawalHistoryInfo.user_balance_record_list!!.isEmpty())) {
                container!!.showView(ViewStatus.EMPTY_STATUS)
                container!!.setCanLoadMore(false)
                return@Observer
            }
            container!!.showView(ViewStatus.CONTENT_STATUS)
            if (page == 0) {
                mCommonAdapter!!.update(withdrawalHistoryInfo!!.user_balance_record_list)
            } else {
                mCommonAdapter!!.add(withdrawalHistoryInfo!!.user_balance_record_list)
            }
        })
        withdrawalHistoryViewModel?.withdrawalHistoryFail?.observe(this, Observer {
            container!!.finishRefresh()
            container!!.finishLoadMore()
            container!!.showView(ViewStatus.EMPTY_STATUS)
        })
    }

    override fun refresh() {
        page = 0
        withdrawalHistoryViewModel!!.getWithdrawalHistoryListData(page)
    }

    override fun loadMore() {
        page++
        withdrawalHistoryViewModel!!.getWithdrawalHistoryListData(page)
    }
}