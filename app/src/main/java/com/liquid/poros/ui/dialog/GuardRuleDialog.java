package com.liquid.poros.ui.dialog;


import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.liquid.poros.R;

public class GuardRuleDialog extends Dialog {
    private TextView close;
    public GuardRuleDialog(@NonNull Context context) {
        super(context);
        initView(context);
    }

    public GuardRuleDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        initView(context);
    }

    protected GuardRuleDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        initView(context);
    }

    private void initView(Context context) {
        getWindow().getDecorView().setBackgroundResource(R.color.transparent);
        setContentView(R.layout.dialog_guard_rule);
        close = findViewById(R.id.i_know);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
    }
}
