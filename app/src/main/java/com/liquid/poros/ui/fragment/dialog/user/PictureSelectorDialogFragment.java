package com.liquid.poros.ui.fragment.dialog.user;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.liquid.poros.R;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * 选择图片弹窗
 */
public class PictureSelectorDialogFragment extends BottomSheetDialogFragment implements View.OnClickListener {
    private TextView tv_shot_choose;
    private TextView tv_album_choose;
    private TextView tv_cancel;

    public static PictureSelectorDialogFragment newInstance() {
        PictureSelectorDialogFragment fragment = new PictureSelectorDialogFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
        View view = View.inflate(getContext(), R.layout.fragment_picture_selector_dialog, null);
        dialog.setContentView(view);
        //设置透明背景
        dialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);
        View root = dialog.getDelegate().findViewById(R.id.design_bottom_sheet);
        BottomSheetBehavior behavior = BottomSheetBehavior.from(root);
        behavior.setHideable(false);
        initializeView(view);
        return dialog;
    }

    @SuppressLint("ResourceType")
    private void initializeView(View view) {
        tv_shot_choose = view.findViewById(R.id.tv_shot_choose);
        tv_album_choose = view.findViewById(R.id.tv_album_choose);
        tv_cancel = view.findViewById(R.id.tv_cancel);
        tv_shot_choose.setOnClickListener(this);
        tv_album_choose.setOnClickListener(this);
        tv_cancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_shot_choose:
                if (listener != null) {
                    listener.onShotChoose();
                }
                dismissAllowingStateLoss();
                break;
            case R.id.tv_album_choose:
                if (listener != null) {
                    listener.onAlbumChoose();
                }
                dismissAllowingStateLoss();
                break;
            case R.id.tv_cancel:
                dismissAllowingStateLoss();
                break;
        }
    }

    private PictureSelectorDialogFragment.PictureSelectorListener listener;

    public PictureSelectorDialogFragment choosePictureListener(PictureSelectorDialogFragment.PictureSelectorListener listener) {
        this.listener = listener;
        return this;
    }

    public interface PictureSelectorListener {
        //拍摄
        void onShotChoose();

        //相册中选择
        void onAlbumChoose();
    }
}
