package com.liquid.poros.ui.fragment.dialog.common;

public interface CommonActionListener {
    void onConfirm();

    void onCancel();
}
