package com.liquid.poros.ui.fragment.dialog.session;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.liquid.base.event.EventCenter;
import com.liquid.poros.R;
import com.liquid.poros.base.BaseDialogFragment;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.utils.ViewUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import de.greenrobot.event.EventBus;

/**
 * 匹配通话页面邀请上麦
 */
public class MatchInvitePlayDialogFragment extends BaseDialogFragment implements View.OnClickListener {
    private String mRoomId;
    private String mAvatarUrl;
    private String lineCost;
    private String needCoin;
    private String title;
    private ImageView iv_invite_mic_head;
    private TextView tv_cost_with_line;
    private TextView tv_cost;
    private TextView tv_cancel;
    private TextView tv_confirm;
    private TextView tv_invite_mic_title;


    public MatchInvitePlayDialogFragment setUserInfo( String avatarUrl, String lineCost, String needCoin,String title) {
        this.mAvatarUrl = avatarUrl;
        this.lineCost = lineCost;
        this.needCoin = needCoin;
        this.title = title;
        return this;
    }

    public static MatchInvitePlayDialogFragment newInstance() {
        MatchInvitePlayDialogFragment fragment = new MatchInvitePlayDialogFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    protected String getPageId() {
        return TrackConstants.PAGE_MATCHED_INVITE_MIC;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onStart() {
        super.onStart();
        Window win = getDialog().getWindow();
        // 一定要设置Background，如果不设置，window属性设置无效
        win.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams params = win.getAttributes();
        params.gravity = Gravity.CENTER;
        setCancelable(false);
        // 使用ViewGroup.LayoutParams，以便Dialog 宽度充满整个屏幕
        params.width = (int) (dm.widthPixels * 0.8);
        params.height = (int) (dm.widthPixels * 0.8);
        win.setAttributes(params);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_match_invite_play_dialog, null);
        iv_invite_mic_head = view.findViewById(R.id.iv_invite_mic_head);

        tv_invite_mic_title = view.findViewById(R.id.tv_invite_mic_title);

        tv_invite_mic_title.setText(title);

        tv_cost_with_line = view.findViewById(R.id.tv_cost_with_line);
        tv_cost_with_line.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        tv_cost_with_line.setText(lineCost);

        tv_cost = view.findViewById(R.id.tv_cost);
        tv_cost.setText(needCoin);

        tv_cancel = view.findViewById(R.id.tv_cancel);
        tv_confirm = view.findViewById(R.id.tv_confirm);

        Glide.with(getActivity())
                .load(mAvatarUrl)
                .into(iv_invite_mic_head);

        tv_cancel.setOnClickListener(this);
        tv_confirm.setOnClickListener(this);
        builder.setView(view);
        return builder.create();
    }

    @Override
    public void onClick(View v) {
        if (ViewUtils.isFastClick()) {
            return;
        }
        switch (v.getId()) {
            case R.id.tv_cancel:
                EventBus.getDefault().post(new EventCenter(Constants.EVENT_CODE_REFUSE_PLAY_IN_MATCH_CALL));
                dismissDialog();
                break;
            case R.id.tv_confirm:
                EventBus.getDefault().post(new EventCenter(Constants.EVENT_CODE_ACCEPT_PLAY_IN_MATCH_CALL));
                dismissDialog();
                break;
        }
    }
}
