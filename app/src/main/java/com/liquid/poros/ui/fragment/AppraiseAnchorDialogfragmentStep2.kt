package com.liquid.poros.ui.fragment

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.TextUtils
import android.util.DisplayMetrics
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import com.liquid.base.utils.ToastUtil
import com.liquid.library.retrofitx.RetrofitX.Companion.url
import com.liquid.poros.R
import com.liquid.poros.analytics.utils.MultiTracker
import com.liquid.poros.base.BaseDialogFragment
import com.liquid.poros.constant.TrackConstants
import com.liquid.poros.entity.AppraiseInfo
import com.liquid.poros.entity.BaseBean
import com.liquid.poros.http.ApiUrl
import com.liquid.poros.http.observer.ObjectObserver
import com.liquid.poros.utils.AccountUtil
import com.liquid.poros.utils.ScreenUtil
import com.liquid.poros.utils.StringUtil
import com.liquid.poros.utils.ViewUtils
import com.liquid.poros.widgets.FlowLayout
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import java.util.*

class AppraiseAnchorDialogfragmentStep2 : BaseDialogFragment(), View.OnClickListener {
    private val selects: MutableList<String> = ArrayList()
    private var negetive_btn: TextView? = null
    private var positive_btn: TextView? = null
    private var edittext: EditText? = null
    private var flowLayout: FlowLayout? = null
    private var mAnchorId: String? = null
    private var mGuestId: String? = null
    private var mRoomId: String? = null
    private var mPopupType: String? = null
    private var boy_satisfaction: List<String>? = null
    private var boy_un_satisfy: List<String>? = null
    private var degree_of_satisfaction_iv: ImageView? = null
    private var degree_of_satisfaction_tv: TextView? = null
    private var commit_btn: TextView? = null
    private var mChose: String = ""
    override fun getPageId(): String {
        return TrackConstants.PRO_APPRAISE_ANCHOR_STEP_2
    }

    fun setPos(pos: Int): AppraiseAnchorDialogfragmentStep2 {
        choosePosition = pos
        return this
    }

    fun setAnchor(anchor_id: String?, room_id: String?, guest_id: String?, popup_type: String?): AppraiseAnchorDialogfragmentStep2 {
        mAnchorId = anchor_id
        mRoomId = room_id
        mGuestId = guest_id
        mPopupType = popup_type
        return this
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dialog!!.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        if(StringUtil.isEmpty(mChose)){
            updateAction("close")
        }else{
            updateAction(mChose)
        }
    }

    override fun onStart() {
        super.onStart()
        val win = dialog!!.window
        // 一定要设置Background，如果不设置，window属性设置无效
        win.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val dm = DisplayMetrics()
        requireActivity().windowManager.defaultDisplay.getMetrics(dm)
        val params = win.attributes
        params.gravity = Gravity.BOTTOM
        win.attributes = params
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(requireActivity())
        val view = LayoutInflater.from(activity).inflate(R.layout.dialog_appraise_anchor_step2, null)
        negetive_btn = view.findViewById(R.id.negetive_btn)
        negetive_btn?.setOnClickListener(this)
        positive_btn = view.findViewById(R.id.positive_btn)
        positive_btn?.setOnClickListener(this)
        edittext = view.findViewById(R.id.edittext)
        flowLayout = view.findViewById(R.id.flowlayout)
        degree_of_satisfaction_iv = view.findViewById(R.id.degree_of_satisfaction_iv)
        degree_of_satisfaction_tv = view.findViewById(R.id.degree_of_satisfaction_tv)
        commit_btn = view.findViewById(R.id.commit_btn)
        commit_btn?.setOnClickListener(this)
        chooseSatisifaction(choosePosition)
        data
        updateExp()
        builder.setView(view)
        return builder.create()
    }

    private fun initFlowLayout(fLowList: List<String>?) {
        if (fLowList == null) {
            return
        }
        clearItems()
        if (flowLayout != null) {
            flowLayout!!.removeAllViews()
        }
        val layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        layoutParams.setMargins(ScreenUtil.dip2px(6f), ScreenUtil.dip2px(6f), ScreenUtil.dip2px(6f), ScreenUtil.dip2px(6f))
        for (i in fLowList.indices) {
            val item = fLowList[i]
            val tv = TextView(context)
            tv.gravity = Gravity.LEFT
            tv.text = item
//            tv.maxEms = 50
            tv.textSize = 12f
            tv.setTextColor(Color.parseColor("#1D1E20"))
            tv.background = resources.getDrawable(R.drawable.bg_for_flowlayout_item)
//            tv.maxLines = 2
            tv.setPadding(ScreenUtil.dip2px(8f), ScreenUtil.dip2px(8f), ScreenUtil.dip2px(8f), ScreenUtil.dip2px(8f))
            tv.setOnClickListener { v: View ->
                val target = (v as TextView).text.toString()
                if (selects.size > 0) {
                    if (isSelected(item)) {
                        selects.remove(target)
                        tv.setTextColor(Color.parseColor("#1D1E20"))
                        tv.background = resources.getDrawable(R.drawable.bg_for_flowlayout_item)
                    } else {
                        selects.add(target)
                        tv.setTextColor(Color.parseColor("#ffffff"))
                        tv.background = resources.getDrawable(R.drawable.bg_appraise_anchor_commit)
                    }
                } else {
                    selects.add(target)
                    tv.setTextColor(Color.parseColor("#ffffff"))
                    tv.background = resources.getDrawable(R.drawable.bg_appraise_anchor_commit)
                }
            }
            flowLayout!!.addView(tv, layoutParams)
        }
    }

    private fun isSelected(target: String): Boolean {
        for (flag in selects) {
            if (TextUtils.equals(target, flag)) {
                return true
            }
        }
        return false
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.negetive_btn -> chooseSatisifaction(1)
            R.id.positive_btn -> chooseSatisifaction(2)
            R.id.commit_btn -> updateItems()
        }
    }

    /**
     * 1 = 不满意
     * 2 = 满意
     * @param pos
     */
    private var choosePosition = 0
    @SuppressLint("UseCompatLoadingForDrawables")
    private fun chooseSatisifaction(pos: Int) {
        if (pos == 1) {
            //step 1 single chose
            negetive_btn!!.setTextColor(Color.parseColor("#ffffff"))
            negetive_btn!!.background = resources.getDrawable(R.drawable.bg_appraise_anchor_negetive_btn, null)
            positive_btn!!.setTextColor(Color.parseColor("#1D1E20"))
            positive_btn!!.background = resources.getDrawable(R.drawable.bg_for_flowlayout_item)
            //step 2 clear options
            clearItems()
            initFlowLayout(boy_un_satisfy)
            //step 3
            edittext!!.setText("")
            //step 4
            degree_of_satisfaction_iv!!.setImageResource(R.mipmap.ic_bumanyi)
            degree_of_satisfaction_tv!!.text = "不满意，各方面都不好"
            degree_of_satisfaction_tv!!.setTextColor(Color.parseColor("#F54562"))
            //step 5
            choosePosition = pos
            //step 6
            mChose = "not_good"
        } else if (pos == 2) {
            //step 1 single chose
            negetive_btn!!.setTextColor(Color.parseColor("#1D1E20"))
            negetive_btn!!.background = resources.getDrawable(R.drawable.bg_for_flowlayout_item)
            positive_btn!!.setTextColor(Color.parseColor("#ffffff"))
            positive_btn!!.background = resources.getDrawable(R.drawable.bg_appraise_anchor_commit)
            //step 2 clear options
            clearItems()
            initFlowLayout(boy_satisfaction)
            //step 3
            edittext!!.setText("")
            //step 4
            degree_of_satisfaction_iv!!.setImageResource(R.mipmap.ic_manyi)
            degree_of_satisfaction_tv!!.text = "满意，无可挑剔"
            degree_of_satisfaction_tv!!.setTextColor(Color.parseColor("#10D8C8"))
            //step 5
            choosePosition = pos
            //step 6
            mChose = "good"
        }
    }

    private fun clearItems() {
        selects.clear()
    }

    private val data: Unit
        get() {
            url(ApiUrl.GET_APPRAISE_ANCHOR_ITEMS)
                    .param("target_user_id", mAnchorId)
                    .param("token", AccountUtil.getInstance().account_token)
                    .postJSON()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(object : ObjectObserver<AppraiseInfo>() {
                        override fun failed(result: AppraiseInfo) {}
                        override fun success(result: AppraiseInfo) {
                            if (result.code == 0) {
                                val appraiseDataBean = result.appraise_data
                                boy_satisfaction = appraiseDataBean!!.boy_satisfaction
                                boy_un_satisfy = appraiseDataBean.boy_un_satisfaction
                                if (choosePosition == 1) {
                                    initFlowLayout(boy_un_satisfy)
                                } else if (choosePosition == 2) {
                                    initFlowLayout(boy_satisfaction)
                                }
                            }
                        }
                    })
        }

    private fun updateItems() {
        if(ViewUtils.isFastClick()){
            return
        }
        var updatePos = -1
        updatePos = if (choosePosition == 1) 0 else 1
        val edittextString = edittext!!.text.toString()
        if (StringUtil.isNotEmpty(edittextString)) {
            selects.add(edittextString)
        }
        if (choosePosition == 1) {
            //此处代表不满意
            //不满意必须要有选项
            if (selects.size <= 0) {
                ToastUtil.showInsToast("不满意需要选择填写理由")
                return
            }
        } else if (choosePosition == 2) {
            //此处代表满意
            //do nothing
        }
        url(ApiUrl.UPDATE_APPRAISE_ANCHOR_ITEMS)
                .param("target_user_id", mAnchorId)
                .param("room_id", mRoomId)
                .param("is_satisfaction", updatePos)
                .param("appraise_content", selects.toTypedArray())
                .param("token", AccountUtil.getInstance().account_token)
                .postJSON()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : ObjectObserver<BaseBean>() {
                    override fun failed(result: BaseBean) {
                        ToastUtil.showInsToast("发布评价失败，请稍后尝试")
                    }

                    override fun success(result: BaseBean) {
                        if (result.code == 0) {
                            ToastUtil.showInsToast("感谢你的评价")
                            dismissAllowingStateLoss()
                        }
                    }
                })
    }

    companion object {
        fun newInstance(): AppraiseAnchorDialogfragmentStep2 {
            return AppraiseAnchorDialogfragmentStep2()
        }
    }

    private fun updateAction(action: String) {
        val params = HashMap<String?, String?>()
        params["anchor_id"] = mAnchorId
        params["user_id"] = AccountUtil.getInstance().userId
        params["guest_id"] = mGuestId
        params[action] = selects.toString()
        MultiTracker.onEvent(TrackConstants.B_EVALUATION_POPUP_ANONYMOUS_SUBMIT_ONCLICK, params)
    }

    private fun updateExp() {
        val params = HashMap<String?, String?>()
        params["anchor_id"] = mAnchorId
        params["user_id"] = AccountUtil.getInstance().userId
        params["guest_id"] = mGuestId
        params["popup_type"] = mPopupType
        MultiTracker.onEvent(TrackConstants.B_EVALUATION_POPUP_EXPOSURE, params)
    }
}