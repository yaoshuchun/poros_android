package com.liquid.poros.ui.activity.user;

import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.blankj.utilcode.util.ToastUtils;
import com.liquid.poros.R;
import com.liquid.poros.base.BaseActivity;
import com.liquid.poros.analytics.utils.MultiTracker;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.contract.user.SubmitUserInfoContract;
import com.liquid.poros.contract.user.SubmitUserInfoPresenter;
import com.liquid.poros.entity.PorosUserInfo;
import com.liquid.poros.ui.MainActivity;
import com.liquid.poros.utils.GenerateJsonUtil;
import com.liquid.poros.utils.ScreenUtil;
import com.liquid.poros.utils.SharePreferenceUtil;
import com.liquid.poros.utils.ViewUtils;
import com.liquid.poros.widgets.FlowLayout;
import com.liquid.poros.widgets.WheelView;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class SubmitUserInfoActivity extends BaseActivity implements SubmitUserInfoContract.View {
    @BindView(R.id.iv_submit_info_pos1)
    ImageView iv_submit_info_pos1;
    @BindView(R.id.iv_submit_info_pos2)
    ImageView iv_submit_info_pos2;
    @BindView(R.id.iv_submit_info_pos3)
    ImageView iv_submit_info_pos3;
    @BindView(R.id.tv_submit_info_title)
    TextView tv_submit_info_title;
    @BindView(R.id.tv_submit_info_tip)
    TextView tv_submit_info_tip;
    @BindView(R.id.ll_sex)
    LinearLayout ll_sex;
    @BindView(R.id.rl_choose_male)
    RelativeLayout rl_choose_male;
    @BindView(R.id.iv_male)
    ImageView iv_male;
    @BindView(R.id.iv_male_check)
    ImageView iv_male_check;
    @BindView(R.id.iv_female)
    ImageView iv_female;
    @BindView(R.id.iv_female_check)
    ImageView iv_female_check;
    @BindView(R.id.rl_choose_female)
    RelativeLayout rl_choose_female;
    @BindView(R.id.ll_choose_age)
    LinearLayout ll_choose_age;
    @BindView(R.id.rv_choose_age)
    WheelView rv_wheel_choose;
    @BindView(R.id.fl_user_tag)
    FlowLayout fl_user_tag;
    @BindView(R.id.tv_save)
    TextView tv_save;
    private ArrayList<String> mTargetGame = new ArrayList<>();
    private List<String> mGameStrList = new ArrayList<>();
    private int mGender = 1;
    private SubmitUserInfoPresenter mPresenter;
    private static final int TYPE_SEX = 1;
    private static final int TYPE_AGE = 2;
    private static final int TYPE_GAME = 3;
    private static final String[] AGE_LEVEL = new String[]{"10岁", "11岁", "12岁", "13岁", "14岁", "15岁", "16岁", "17岁", "18岁",
            "19岁", "20岁", "21岁", "22岁", "23岁", "24岁", "25岁", "26岁", "27岁", "28岁", "29岁", "30岁", "31岁", "32岁", "33岁", "34岁", "35岁", "36岁", "37岁", "38岁", "39岁", "40岁"};
    private static final String[] GAME_INFO = new String[]{ "和平精英", "王者荣耀", "迷你世界", "欢乐斗地主", "我的世界", "QQ飞车", "跑跑卡丁车", "穿越火线", "第五人格"};
    private String mSelectItem = null;

    @Override
    protected void parseBundleExtras(Bundle extras) {

    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.activity_submit_user_info;
    }

    @Override
    protected void initViewsAndEvents(Bundle savedInstanceState) {
        String[] infoGames = SharePreferenceUtil.getStringArray(SharePreferenceUtil.FILE_INFO_GAMES_DATA, SharePreferenceUtil.KEY_INFO_GAMES);
        if (infoGames != null && infoGames.length > 0) {
            initGameFlow(Arrays.asList(infoGames));
        }else {
            initGameFlow(Arrays.asList(GAME_INFO));
        }
        mPresenter = new SubmitUserInfoPresenter();
        mPresenter.attachView(this);
        switchView(TYPE_SEX);
    }

    @Override
    protected String getPageId() {
        return TrackConstants.PAGE_USER_INFO;
    }

    @OnClick({R.id.tv_save, R.id.rl_choose_male, R.id.rl_choose_female})
    public void onClick(View view) {
        if (ViewUtils.isFastClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.rl_choose_male:
                switchChooseSex(true);
                break;
            case R.id.rl_choose_female:
                switchChooseSex(false);
                break;
            case R.id.tv_save:
                if (tv_save.getText().equals(getString(R.string.submit_user_info_next_tip))) {
                    if (ll_sex.getVisibility() == View.VISIBLE) {
                        switchView(TYPE_AGE);
                        HashMap<String, String> map = new HashMap<>();
                        map.put("gender", String.valueOf(mGender));
                        MultiTracker.onEvent(TrackConstants.B_USER_FIRST_STEP,map);
                    } else {
                        if (mSelectItem == null){
                            ToastUtils.showShort("请先选择年龄");
                            return;
                        }
                        switchView(TYPE_GAME);
                        HashMap<String, String> map = new HashMap<>();
                        map.put("age", mSelectItem);
                        MultiTracker.onEvent(TrackConstants.B_USER_SECOND_STEP,map);
                    }
                    return;
                }
                if (mSelectItem == null || mGender == 0 || mTargetGame.size() == 0) {
                    Toast.makeText(SubmitUserInfoActivity.this, getString(R.string.submit_user_info_tip), Toast.LENGTH_SHORT).show();
                    return;
                }

                try {
                    mPresenter.updateUserInfo(mGender, mSelectItem, GenerateJsonUtil.jsonArray(mTargetGame));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    /**
     * 选择性别
     *
     * @param isMale
     */
    private void switchChooseSex(boolean isMale) {
        iv_female.setBackgroundResource(isMale ? R.mipmap.icon_female_normal : R.mipmap.icon_female_select);
        iv_female_check.setVisibility(isMale ? View.GONE : View.VISIBLE);
        iv_male.setBackgroundResource(isMale ? R.mipmap.icon_male_select : R.mipmap.icon_male_normal);
        iv_male_check.setVisibility(isMale ? View.VISIBLE : View.GONE);
        mGender = isMale ? 1 : 2;
    }

    private void switchView(int type) {
        tv_save.setVisibility(View.VISIBLE);
        switch (type) {
            case TYPE_SEX:
                iv_submit_info_pos1.setBackgroundResource(R.drawable.bg_common_button_save);
                iv_submit_info_pos2.setBackgroundResource(R.drawable.bg_color_f5f7fa_22);
                iv_submit_info_pos3.setBackgroundResource(R.drawable.bg_color_f5f7fa_22);
                tv_submit_info_title.setText(getString(R.string.submit_user_info_sex_title));
                tv_submit_info_tip.setText(getString(R.string.submit_user_info_sex_tip));
                ll_sex.setVisibility(View.VISIBLE);
                ll_choose_age.setVisibility(View.GONE);
                fl_user_tag.setVisibility(View.GONE);
                tv_save.setText(getString(R.string.submit_user_info_next_tip));
                tv_save.setEnabled(true);
                tv_save.setTextColor(getResources().getColor(R.color.tv_status_online));
                tv_save.setBackgroundResource(R.drawable.bg_common_button_save);
                break;
            case TYPE_AGE:
                iv_submit_info_pos1.setBackgroundResource(R.drawable.bg_color_f5f7fa_22);
                iv_submit_info_pos2.setBackgroundResource(R.drawable.bg_common_button_save);
                iv_submit_info_pos3.setBackgroundResource(R.drawable.bg_color_f5f7fa_22);
                tv_submit_info_title.setText(getString(R.string.submit_user_info_age_title));
                tv_submit_info_tip.setText(getString(R.string.submit_user_info_age_tip));
                ll_sex.setVisibility(View.GONE);
                ll_choose_age.setVisibility(View.VISIBLE);
                fl_user_tag.setVisibility(View.GONE);
                tv_save.setText(getString(R.string.submit_user_info_next_tip));
                tv_save.setEnabled(true);
                tv_save.setTextColor(getResources().getColor(R.color.tv_status_offline));
                tv_save.setBackgroundResource(R.drawable.bg_color_f5f7fa_22);
                rv_wheel_choose.setItems(Arrays.asList(AGE_LEVEL));
                rv_wheel_choose.setSeletion(8);
                rv_wheel_choose.setOnWheelViewListener(new WheelView.OnWheelViewListener() {
                    @Override
                    public void onSelected(int selectedIndex, String item) {
                        mSelectItem = item.replace("岁", "");
                        tv_save.setTextColor(getResources().getColor(R.color.tv_status_online));
                        tv_save.setBackgroundResource(R.drawable.bg_common_button_save);
                    }
                });
                break;
            case TYPE_GAME:
                iv_submit_info_pos1.setBackgroundResource(R.drawable.bg_color_f5f7fa_22);
                iv_submit_info_pos2.setBackgroundResource(R.drawable.bg_color_f5f7fa_22);
                iv_submit_info_pos3.setBackgroundResource(R.drawable.bg_common_button_save);
                tv_submit_info_title.setText(getString(R.string.submit_user_info_game_title));
                tv_submit_info_tip.setText(getString(R.string.submit_user_info_game_tip));
                ll_sex.setVisibility(View.GONE);
                ll_choose_age.setVisibility(View.GONE);
                fl_user_tag.setVisibility(View.VISIBLE);
                tv_save.setText(getString(R.string.submit_user_info_well_done));
                tv_save.setTextColor(getResources().getColor(R.color.tv_status_offline));
                tv_save.setBackgroundResource(R.drawable.bg_color_f5f7fa_22);
                tv_save.setEnabled(false);
                break;
        }

    }


    @Override
    public void uploadUserInfoSuccess(PorosUserInfo userInfo) {
        HashMap<String, String> map = new HashMap<>();
        map.put("submit_type", "user_control_group");
        map.put("game", mTargetGame.toString());
        MultiTracker.onEvent(TrackConstants.B_USER_SUCCESS,map);
        readyGo(MainActivity.class);
        finish();
    }

    @Override
    public void uploadUserInfoFailed() {
        HashMap<String, String> map = new HashMap<>();
        map.put("submit_type", "user_control_group");
        map.put("game", mTargetGame.toString());
        MultiTracker.onEvent(TrackConstants.B_USER_FAILED,map);
        readyGo(MainActivity.class);
        finish();
    }

    private void initGameFlow(final List<String> games) {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(ScreenUtil.dip2px(10), ScreenUtil.dip2px(5), ScreenUtil.dip2px(10), ScreenUtil.dip2px(5));
        mGameStrList.clear();
        for (int i = 0; i < games.size(); i++) {
            mGameStrList.add(games.get(i));
        }
        mTargetGame.clear();
        fl_user_tag.removeAllViews();
        for (final String item : mGameStrList) {
            final TextView tv = new TextView(SubmitUserInfoActivity.this);
            tv.setGravity(Gravity.CENTER);
            tv.setText(item);
            tv.setMaxEms(10);
            tv.setTextSize(14);
            tv.setPadding(ScreenUtil.dip2px(15), ScreenUtil.dip2px(10), ScreenUtil.dip2px(15), ScreenUtil.dip2px(10));
            tv.setTextColor(Color.parseColor("#A4A9B3"));
            tv.setBackground(getResources().getDrawable(R.drawable.bg_color_f5f7fa_22));
            tv.setSingleLine();
            tv.setLayoutParams(layoutParams);
            tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isSelected(item)) {
                        tv.setTextColor(Color.parseColor("#A4A9B3"));
                        tv.setBackground(getResources().getDrawable(R.drawable.bg_color_f5f7fa_22));
                        mTargetGame.remove(item);
                    } else {
                        tv.setTextColor(Color.parseColor("#ffffff"));
                        tv.setBackground(getResources().getDrawable(R.drawable.bg_color_00aaff_22));
                        mTargetGame.add(item);
                    }
                    if (mTargetGame == null || mTargetGame.size() == 0) {
                        tv_save.setTextColor(getResources().getColor(R.color.tv_status_offline));
                        tv_save.setBackgroundResource(R.drawable.bg_color_f5f7fa_22);
                        tv_save.setEnabled(false);
                    } else {
                        tv_save.setTextColor(getResources().getColor(R.color.tv_status_online));
                        tv_save.setBackgroundResource(R.drawable.bg_common_button_save);
                        tv_save.setEnabled(true);
                    }
                }
            });
            fl_user_tag.addView(tv);
        }

    }

    private boolean isSelected(String target) {
        for (String flag : mTargetGame) {
            if (TextUtils.equals(target, flag)) {
                return true;
            }
        }
        return false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }
}
