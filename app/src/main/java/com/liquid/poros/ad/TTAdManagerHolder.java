package com.liquid.poros.ad;

import android.content.Context;

import com.bytedance.sdk.openadsdk.TTAdConfig;
import com.bytedance.sdk.openadsdk.TTAdConstant;
import com.bytedance.sdk.openadsdk.TTAdManager;
import com.bytedance.sdk.openadsdk.TTAdSdk;
import com.liquid.poros.BuildConfig;
import com.liquid.poros.utils.AppConfigUtil;
import com.liquid.poros.utils.SharePreferenceUtil;
import com.liquid.poros.utils.StringUtil;

/**
 * 可以用一个单例来保存TTAdManager实例，在需要初始化sdk的时候调用
 */
public class TTAdManagerHolder {

    private static boolean sInit;


    public static TTAdManager get() {
        if (!sInit) {
            throw new RuntimeException("TTAdSdk is not init, please check.");
        }
        return TTAdSdk.getAdManager();
    }

    public static void init(Context context) {
        doInit(context);
    }

    //step1:接入网盟广告sdk的初始化操作，详情见接入文档和穿山甲平台说明
    private static void doInit(Context context) {
        if (!sInit) {
            TTAdSdk.init(context, buildConfig(context));
            sInit = true;
        }
    }

    private static TTAdConfig buildConfig(Context context) {
        String appid = TTConstants.APP_ID;
        if (StringUtil.isNotEmpty(AppConfigUtil.getAd_csj_appid()) && StringUtil.isNotEmpty(AppConfigUtil.getAd_csj_codeid())) {
            appid = AppConfigUtil.getAd_csj_appid();
        } else {
            AppConfigUtil.setAd_csj_appid(TTConstants.APP_ID);
            AppConfigUtil.setAd_csj_codeid(TTConstants.CODE_ID);
        }
        SharePreferenceUtil.saveString(SharePreferenceUtil.FILE_APP_CONFIG, SharePreferenceUtil.KEY_FIRST_USED_APPID, appid);
        return new TTAdConfig.Builder()
                .appId(appid)
                .useTextureView(true) //使用TextureView控件播放视频,默认为SurfaceView,当有SurfaceView冲突的场景，可以使用TextureView
                .allowShowNotify(true) //是否允许sdk展示通知栏提示
                .debug(BuildConfig.DEBUG) //测试阶段打开，可以通过日志排查问题，上线时去除该调用
                .directDownloadNetworkType(TTAdConstant.NETWORK_STATE_WIFI, TTAdConstant.NETWORK_STATE_3G) //允许直接下载的网络状态集合
                .supportMultiProcess(true)//是否支持多进程
                .needClearTaskReset()
                .httpStack(new MyOkStack3())//自定义网络库，demo中给出了okhttp3版本的样例，其余请自行开发或者咨询工作人员。
                .build();
    }
}
