package com.liquid.poros.ad;

import com.liquid.poros.BuildConfig;

/**
 * Created by yejiurui on 2020/12/11.
 * Mail:yejiurui@liqiudnetwork.com
 */
public class TTConstants {

    //测试
    public static final String APP_ID_TEST = "5125716";
    public static final String CODE_ID_TEST = "945677475";
    //正式
    public static final String APP_ID_PRO = "5125716";
    public static final String CODE_ID_PRO = "945677475";

    public static final String APP_ID = BuildConfig.DEBUG ? APP_ID_TEST : APP_ID_PRO;
    public static final String CODE_ID = BuildConfig.DEBUG ? CODE_ID_TEST : CODE_ID_PRO;
}
