package com.liquid.poros.faceunity

import com.liquid.lib.faceunity_base.data.BundleInfo
import com.liquid.poros.entity.BundleBean
import com.liquid.poros.utils.bundle.VirtualBundleDownloadUtil

/**
 * 虚拟形象资源转换器
 */
object FaceUnityResAdapter {
    /**
     * 服务端返回的BundleBean转换为FaceUnity需要的信息
     */
    @JvmStatic
    fun bundleBeanToInfo(bundleBean: List<BundleBean>): List<BundleInfo> {
        return bundleBean.map {
            var res = it.ext.toString()
            if (it.ext.has("bundle")) {
                res = VirtualBundleDownloadUtil.parentPath + it.ext.get("bundle").asString
            }
            BundleInfo(it.cate_2, res, 0, it.ext)
        }
    }
}