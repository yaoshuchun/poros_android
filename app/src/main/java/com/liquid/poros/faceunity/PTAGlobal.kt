package com.liquid.poros.faceunity

import android.util.Log
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import com.liquid.poros.entity.SessionDressBean

///**
// * PTA全局类
// */
class PTAGlobal private constructor(var activity: AppCompatActivity, var parentView: ViewGroup) {
//
//    private lateinit var heAvatarPTA: AvatarPTA
//    private lateinit var myAvatarPTA: AvatarPTA
//    private lateinit var glTextureView: GLTextureView
//    private lateinit var ptaLauncher: PTALauncher
//    private lateinit var ptaAvatarManager: PTAAvatarManager
//    private var isRelease: Boolean = false
//    var ptaHandler = Handler(Looper.getMainLooper())
//
//    init {
//        ptaHandler.postDelayed({
//            glTextureView = GLTextureView(activity)
//            val layoutParams = FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT)
//            layoutParams.gravity = Gravity.BOTTOM
//            glTextureView.layoutParams = layoutParams
//            parentView.addView(glTextureView)
//            ptaLauncher = PTALauncher.PTABuilder(activity)
//                    .addAvatarView(glTextureView)
//                    .isSwitchAnimation(true)
//                    .initExtensionCachePath(parentPath)
//                    .build();
//            ptaAvatarManager = ptaLauncher.ptaAvatarManager
//            myAvatarPTA = ptaAvatarManager.initAvatar(true)
//            myAvatarPTA.roleId = 0
//            myAvatarPTA.position = doubleArrayOf(40.0, -50.0, -200.0)
//            heAvatarPTA = ptaAvatarManager.initAvatar(false)
//            heAvatarPTA.roleId = 1
//            heAvatarPTA.position = doubleArrayOf(-40.0, -50.0, -200.0)
//            Log.i("PTAGlobal", "PTALauncher初始化完成")
//        }, 200)
//    }
//
    companion object {
        private var ptaGlobal: PTAGlobal? = null

        fun getInstance(activity: AppCompatActivity, viewGroup: ViewGroup): PTAGlobal {
            if (ptaGlobal == null) {
                ptaGlobal = PTAGlobal(activity, viewGroup)
                Log.i("PTAGlobal", "PTAGlobal初始化完成")
            }
            return ptaGlobal!!
        }
    }
//
    fun updateAvatar(sessionDressBean: SessionDressBean) {
//        ptaHandler.postDelayed({
//            val heUpdateInfo = bundleBeanToInfo(sessionDressBean.female_suit_list)
//            val myUpdateInfo = bundleBeanToInfo(sessionDressBean.male_suit_list)
//            if (::ptaLauncher.isInitialized) {
//                Log.i("PTAGlobal", "开始更新形象")
//                ptaAvatarManager.updateAvatar(myAvatarPTA, myUpdateInfo) {
////                    ptaLauncher.updateFaceShape(myAvatarPTA)
//                    ptaAvatarManager.updateAvatar(heAvatarPTA, heUpdateInfo) {
////                        ptaLauncher.updateFaceShape(heAvatarPTA)
//                    }
//                }
//            }
//        }, 350)
    }
//
    fun drawAvatar() {
//        ptaHandler.postDelayed({
//            if (::ptaLauncher.isInitialized) {
//                Log.i("PTAGlobal", "开始绘制形象")
//                ptaAvatarManager.updateAvatar(myAvatarPTA) { ptaAvatarManager.updateAvatar(heAvatarPTA) }
//            }
//        }, 350)
    }
//
//    /**
//     * 聚焦某一虚拟形象
//     */
//    fun focusAvatar(isMale: Boolean) {
//        ptaLauncher.loadHalfBodyCamera(if (isMale) myAvatarPTA else heAvatarPTA)
//    }
//
    fun releaseData() {
//        //调用过释放资源后不再调用,无需二次释放
//        if (isRelease) {
//            return
//        }
//        isRelease = true
//        Log.i("PTAGlobal", "开始释放资源")
//        if (::ptaLauncher.isInitialized) {
//            ptaLauncher.release()
//        }
//        removeSurfaceView()
//        ptaGlobal = null
    }
//
//    private fun removeSurfaceView() {
//        parentView.removeAllViews()
//    }
}