package com.liquid.poros.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.os.RemoteException;

import com.liquid.poros.AvInterface;
import com.liquid.poros.R;
import com.liquid.poros.helper.AvHelper;
import com.liquid.poros.ui.MainActivity;
import com.liquid.poros.ui.floatball.FloatManager;

import androidx.core.app.NotificationCompat;

//连麦统一管理服务
public class AvCoreService extends Service {
    public AvCoreService() {

    }

    private static AvHelper mAvHelper;

    //是否启动了音视频通话
    private static boolean isStartAv = false;

    class AvBinder extends AvInterface.Stub {

        @Override
        public void startAudioLink(String roomId, String imVideoToken, String sessionId, String avatarUrl, boolean isHost) throws RemoteException {
            if (mAvHelper == null) {
                mAvHelper = new AvHelper(roomId, imVideoToken, true);
                FloatManager.getInstance().setUpParams(sessionId, avatarUrl, isHost, AvCoreService.this);
            }
            isStartAv = true;
            mAvHelper.joinAudioRoom();
        }

        @Override
        public void startAvLink() throws RemoteException {
            isStartAv = true;
        }

        @Override
        public boolean isAvRunning() throws RemoteException {
            return isStartAv;
        }

        @Override
        public void stopAudioLink() throws RemoteException {
            if (mAvHelper != null && isStartAv) {
                isStartAv = false;
                mAvHelper.releaseAvRoom();
                mAvHelper = null;
                FloatManager.getInstance().setUpParams("", "", false, AvCoreService.this);
                FloatManager.getInstance().hideMenuWindow();
            }
        }

        @Override
        public void stopAvLink() throws RemoteException {
            isStartAv = false;
        }

        @Override
        public void setMicrophoneMute(boolean mute) throws RemoteException {
            if (mAvHelper != null) {
                mAvHelper.setMicrophoneMute(mute);
            }
        }

    }

    @Override
    public IBinder onBind(Intent intent) {
        return new AvBinder();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotification();
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void createNotification() {
        //设置点击跳转
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);

        String id = "1";
        String name = "进圈云消息";
        NotificationManager notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification = null;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(id, name, NotificationManager.IMPORTANCE_DEFAULT);
            mChannel.setSound(null, null);
            notificationManager.createNotificationChannel(mChannel);
            notification = new Notification.Builder(this)
                    .setChannelId(id)
                    .setContentTitle(this.getResources().getString(R.string.notice_title))
                    .setContentIntent(pendingIntent)
                    .setAutoCancel(false)// 设置这个标志当用户单击面板就可以让通知将自动取消
                    .setOngoing(true)// true，设置他为一个正在进行的通知。他们通常是用来表示一个后台任务,用户积极参与(如播放音乐)或以某种方式正在等待,因此占用设备(如一个文件下载,同步操作,主动网络连接)
                    .setSmallIcon(R.mipmap.ic_jinquan).build();
        } else {
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setContentTitle(this.getResources().getString(R.string.notice_title))
                    .setContentIntent(pendingIntent)
                    .setPriority(Notification.PRIORITY_DEFAULT)// 设置该通知优先级
                    .setAutoCancel(false)// 设置这个标志当用户单击面板就可以让通知将自动取消
                    .setOngoing(true)// true，设置他为一个正在进行的通知。他们通常是用来表示一个后台任务,用户积极参与(如播放音乐)或以某种方式正在等待,因此占用设备(如一个文件下载,同步操作,主动网络连接)
                    .setSmallIcon(R.mipmap.ic_jinquan);
            notification = notificationBuilder.build();
        }
        startForeground(1, notification);
    }

}
