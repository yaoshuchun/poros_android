package com.liquid.poros.service;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.text.TextUtils;
import android.util.Log;

import com.igexin.sdk.GTIntentService;
import com.igexin.sdk.PushManager;
import com.igexin.sdk.message.GTCmdMessage;
import com.igexin.sdk.message.GTNotificationMessage;
import com.igexin.sdk.message.GTTransmitMessage;
import com.liquid.base.event.EventCenter;
import com.liquid.base.tools.GT;
import com.liquid.poros.PorosApplication;
import com.liquid.poros.R;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.entity.PlayloadInfo;
import com.liquid.poros.ui.fragment.home.MessageFragment;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.base.utils.LogOut;
import com.liquid.poros.utils.network.HttpCallback;
import com.liquid.poros.utils.network.RetrofitHttpManager;
import com.liquid.porostwo.ApplicationViewModel;

import org.json.JSONObject;

import de.greenrobot.event.EventBus;
import okhttp3.RequestBody;

public class JQIntentService extends GTIntentService {

    private MediaPlayer mSuffixPlayer;

    @Override
    public void onReceiveServicePid(Context context, int i) {

    }

    @Override
    public void onReceiveClientId(Context context, String s) {
        // 接收 cid
        LogOut.debug(TAG, "onReceiveClientId :" + s);
        try {
            JSONObject object = new JSONObject();
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("cid", s);

            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.set_gt_cid(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    LogOut.e("SSSSS", "--------------");
                }

                @Override
                public void OnFailed(int ret, String result) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();

        }
    }


    @Override
    public void onReceiveMessageData(Context context, GTTransmitMessage msg) {
        // 透传消息的处理方式
        String appid = msg.getAppid();
        String taskid = msg.getTaskId();
        String messageid = msg.getMessageId();
        byte[] payload = msg.getPayload();
        String pkg = msg.getPkgName();
        String cid = msg.getClientId();
        // 第三方回执调用接口，actionid范围为90000-90999，可根据业务场景执行
        boolean result = PushManager.getInstance().sendFeedbackMessage(context, taskid, messageid, 90001);
        if (payload == null) return;
        String data = new String(payload);
        PlayloadInfo baseModel = GT.fromJson(data, PlayloadInfo.class);
        if (baseModel == null || TextUtils.isEmpty(baseModel.getTo_user_id()) || !baseModel.getTo_user_id().equals(AccountUtil.getInstance().getUserId())) {
            return;
        }
        switch (baseModel.getType()) {
            case 1:
                if (!MessageFragment.isPause) {
                    EventBus.getDefault().post(new EventCenter(Constants.EVENT_CODE_REFRESH_MESSAGE));
                }
                playAudioNotification(baseModel.getType());
                EventBus.getDefault().post(new EventCenter(Constants.EVENT_CODE_TOAST_MSG, baseModel));
                break;
            case 8:
                if (!MessageFragment.isPause) {
                    EventBus.getDefault().post(new EventCenter(Constants.EVENT_CODE_REFRESH_MESSAGE));
                }
                playAudioNotification(baseModel.getType());
                break;
            case 21:
                PorosApplication.instance.getViewModel(ApplicationViewModel.class).getMessageNotification()
                        .postValue(baseModel);
                EventBus.getDefault().post(new EventCenter(Constants.EVENT_CODE_TOAST_MSG, baseModel));
                break;
            case 3:
                EventBus.getDefault().post(new EventCenter(Constants.EVENT_CODE_INVITE_GAME, baseModel));
                break;
            case 106:
                EventBus.getDefault().post(new EventCenter(Constants.EVENT_MATCHING_SUCCESS, baseModel));
                break;
            case 107:
                EventBus.getDefault().post(new EventCenter(Constants.EVENT_CODE_MATCH_INVITE_PLAY, baseModel));
                break;
            case 108:
                EventBus.getDefault().post(new EventCenter(Constants.EVENT_CODE_REFUSE_BY_REMOTE, baseModel));
                break;
            case 109:
                EventBus.getDefault().post(new EventCenter(Constants.EVENT_CODE_ACCEPT_BY_REMOTE, baseModel));
                break;
            case 110:
                EventBus.getDefault().post(new EventCenter(Constants.EVENT_CODE_TIMEOUT_BY_REMOTE, baseModel));
                break;
            default:
                playAudioNotification(baseModel.getType());
                break;
        }
    }

    @Override
    public void onReceiveOnlineState(Context context, boolean b) {
        // cid 离线上线通知
    }

    @Override
    public void onReceiveCommandResult(Context context, GTCmdMessage gtCmdMessage) {
        // 各种事件处理回执
    }

    @Override
    public void onNotificationMessageArrived(Context context, GTNotificationMessage message) {
        // 通知到达，只有个推通道下发的通知会回调此方法

        Log.d(TAG, "onNotificationMessageArrived -> " + "appid = " + message.getAppid() + "\ntaskid = " + message.getTaskId() + "\nmessageid = "
                + message.getMessageId() + "\npkg = " + message.getPkgName() + "\ncid = " + message.getClientId() + "\ntitle = "
                + message.getTitle() + "\ncontent = " + message.getContent());
    }

    @Override
    public void onNotificationMessageClicked(Context context, GTNotificationMessage message) {
        // 通知点击，只有个推通道下发的通知会回调此方法
        Log.d(TAG, "onNotificationMessageClicked -> " + "appid = " + message.getAppid() + "\ntaskid = " + message.getTaskId() + "\nmessageid = "
                + message.getMessageId() + "\npkg = " + message.getPkgName() + "\ncid = " + message.getClientId() + "\ntitle = "
                + message.getTitle() + "\ncontent = " + message.getContent());

    }


    private void playAudioNotification(int type) {
        try {
            if (mSuffixPlayer == null) {
                mSuffixPlayer = MediaPlayer.create(this, type == 2 ? R.raw.on_free_lead_tip : R.raw.on_mic_tip);
                mSuffixPlayer.setLooping(false);
                mSuffixPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mSuffixPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        if (mSuffixPlayer != null) {
                            mSuffixPlayer.release();
                            mSuffixPlayer = null;
                        }
                    }
                });
                mSuffixPlayer.start();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
