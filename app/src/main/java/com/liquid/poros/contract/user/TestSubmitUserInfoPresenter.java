package com.liquid.poros.contract.user;


import com.google.gson.Gson;
import com.liquid.base.tools.GT;
import com.liquid.poros.entity.EditGameBean;
import com.liquid.poros.entity.NormalGame;
import com.liquid.poros.entity.PorosUserInfo;
import com.liquid.poros.entity.SettingEditGameBean;
import com.liquid.poros.entity.TestSubmitUserInfo;
import com.liquid.poros.entity.UserCenterInfo;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.base.utils.LogOut;
import com.liquid.poros.utils.network.HttpCallback;
import com.liquid.poros.utils.network.RetrofitHttpManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;

public class TestSubmitUserInfoPresenter extends TestSubmitUserInfoContract.Presenter {
    @Override
    public void getUserGame() {
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.user_game(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    LogOut.debug("Update UserInfo" + result);
                    if (!isViewAttached()) {
                        return;
                    }
                    TestSubmitUserInfo testSubmitUserInfo = GT.fromJson(result, TestSubmitUserInfo.class);
                    if (testSubmitUserInfo == null) {
                        return;
                    }
                    if (testSubmitUserInfo.getCode() == 0) {
                        try {
                            JSONObject resultObj = new JSONObject(result);
                            ArrayList<Map<String, String>> wzry_platform_list = new ArrayList<>();
                            ArrayList<Map<String, String>> wzry_rank_list = new ArrayList<>();
                            ArrayList<Map<String, String>> wzry_area_list = new ArrayList<>();
                            JSONObject dataObj = resultObj.optJSONObject("data");
                            if (dataObj != null) {
                                SettingEditGameBean staticGameBean = new SettingEditGameBean();
                                //game_info_static
                                JSONObject wzryObj = dataObj.optJSONObject("wzry");
                                SettingEditGameBean.SettingGameBean honor_of_kings = new SettingEditGameBean.SettingGameBean();
                                honor_of_kings.platform = new EditGameBean();
                                honor_of_kings.platform.des = "游戏平台";

                                JSONObject platformObj = wzryObj.optJSONObject("platform");
                                Iterator<String> iterator = platformObj.keys();
                                while (iterator.hasNext()) {
                                    Map<String, String> map2 = new HashMap<String, String>();
                                    String key = iterator.next();
                                    String value = platformObj.getString(key);
                                    map2.put(key, value);
                                    wzry_platform_list.add(map2);
                                    EditGameBean.Item item = new EditGameBean.Item();
                                    item.id = key;
                                    item.name = value;
                                    honor_of_kings.platform.items.add(item);
                                }

                                JSONObject rankObj = wzryObj.optJSONObject("rank");
                                honor_of_kings.rank = new EditGameBean();
                                honor_of_kings.rank.des = "段位";
                                Iterator<String> rankIterator = rankObj.keys();
                                while (rankIterator.hasNext()) {
                                    Map<String, String> map2 = new HashMap<String, String>();
                                    String key = rankIterator.next();
                                    String value = rankObj.getString(key);
                                    map2.put(key, value);
                                    wzry_rank_list.add(map2);
                                    EditGameBean.Item item = new EditGameBean.Item();
                                    item.id = key;
                                    item.name = value;
                                    honor_of_kings.rank.items.add(item);
                                }

                                JSONObject zoneObj = wzryObj.optJSONObject("zone");
                                honor_of_kings.zone = new EditGameBean();
                                honor_of_kings.zone.des = "游戏大区";
                                Iterator<String> zoneIterator = zoneObj.keys();
                                while (zoneIterator.hasNext()) {
                                    Map<String, String> map2 = new HashMap<String, String>();
                                    String key = zoneIterator.next();
                                    String value = zoneObj.getString(key);
                                    map2.put(key, value);
                                    wzry_area_list.add(map2);
                                    EditGameBean.Item item = new EditGameBean.Item();
                                    item.id = key;
                                    item.name = value;
                                    honor_of_kings.zone.items.add(item);
                                }
                                String wzryIconString = wzryObj.optString("icon");
                                honor_of_kings.icon = wzryIconString;
                                staticGameBean.honor_of_kings = honor_of_kings;

                                UserCenterInfo.WZRYDATA wzrydata = new UserCenterInfo.WZRYDATA();
                                wzrydata.setWzry_platform_list(wzry_platform_list);
                                wzrydata.setWzry_area_list(wzry_area_list);
                                wzrydata.setWzry_rank_list(wzry_rank_list);
                                wzrydata.setIcon(wzryIconString);

                                testSubmitUserInfo.setWzrydata(wzrydata);

                                //和平精英
                                ArrayList<Map<String, String>> hpjy_platform_list = new ArrayList<>();
                                ArrayList<Map<String, String>> hpjy_rank_list = new ArrayList<>();
                                ArrayList<Map<String, String>> hpjy_area_list = new ArrayList<>();
                                JSONObject hpjyObj = dataObj.optJSONObject("hpjy");

                                SettingEditGameBean.SettingGameBean peace_elite = new SettingEditGameBean.SettingGameBean();
                                peace_elite.platform = new EditGameBean();
                                peace_elite.platform.des = "游戏平台";

                                JSONObject hpjyplatformObj = hpjyObj.optJSONObject("platform");
                                Iterator<String> hpjyiterator = hpjyplatformObj.keys();
                                while (hpjyiterator.hasNext()) {
                                    Map<String, String> map2 = new HashMap<String, String>();
                                    String key = hpjyiterator.next();
                                    String value = hpjyplatformObj.getString(key);
                                    map2.put(key, value);
                                    hpjy_platform_list.add(map2);
                                    EditGameBean.Item item = new EditGameBean.Item();
                                    item.id = key;
                                    item.name = value;
                                    peace_elite.platform.items.add(item);
                                }

                                JSONObject hpjyrankObj = hpjyObj.optJSONObject("rank");
                                peace_elite.rank = new EditGameBean();
                                peace_elite.rank.des = "段位";

                                Iterator<String> hpjyrankIterator = hpjyrankObj.keys();
                                while (hpjyrankIterator.hasNext()) {
                                    Map<String, String> map2 = new HashMap<String, String>();
                                    String key = hpjyrankIterator.next();
                                    String value = hpjyrankObj.getString(key);
                                    map2.put(key, value);
                                    hpjy_rank_list.add(map2);
                                    EditGameBean.Item item = new EditGameBean.Item();
                                    item.id = key;
                                    item.name = value;
                                    peace_elite.rank.items.add(item);
                                }

                                JSONObject hpjyzoneObj = hpjyObj.optJSONObject("zone");
                                peace_elite.zone = new EditGameBean();
                                peace_elite.zone.des = "游戏大区";
                                Iterator<String> hpjyzoneIterator = hpjyzoneObj.keys();
                                while (hpjyzoneIterator.hasNext()) {
                                    Map<String, String> map2 = new HashMap<String, String>();
                                    String key = hpjyzoneIterator.next();
                                    String value = hpjyzoneObj.getString(key);
                                    map2.put(key, value);
                                    hpjy_area_list.add(map2);
                                    EditGameBean.Item item = new EditGameBean.Item();
                                    item.id = key;
                                    item.name = value;
                                    peace_elite.zone.items.add(item);
                                }
                                String hpjyIconString = hpjyObj.optString("icon");
                                peace_elite.icon = hpjyIconString;
                                staticGameBean.peace_elite = peace_elite;

                                SettingEditGameBean.SettingOtherGameBean others = new SettingEditGameBean.SettingOtherGameBean();
                                JSONArray otherArray = dataObj.optJSONArray("other");
                                for (int i = 0; i < otherArray.length(); i++) {
                                    EditGameBean.Item item = new EditGameBean.Item();
                                    JSONObject object1 = otherArray.getJSONObject(i);
                                    item.id = object1.getString("_id");
                                    item.name = object1.getString("name");
                                    item.image_url = object1.getString("icon");
                                    others.list.add(item);
                                }
                                others.icon = dataObj.optString("other_game_icon");
                                staticGameBean.other = others;
                                testSubmitUserInfo.setStaticGameBean(staticGameBean);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        getView().fetchUserGame(testSubmitUserInfo);
                    } else {
                        getView().showMessage(testSubmitUserInfo.getMsg());
                    }
                }

                @Override
                public void OnFailed(int ret, String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    getView().closeLoading();
                }
            });
        } catch (Throwable e) {
            e.printStackTrace();
        }

    }

    @Override
    public void updateUserInfo(int gender, int age, int zodiac, UserCenterInfo.Data.UserInfo.GameInfoBean gameInfoBean) {
        getView().showLoading();
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("gender", gender);
            object.put("age", age);
            object.put("zodiac", zodiac);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("hpjy", new JSONObject(new Gson().toJson(gameInfoBean.getHpjy())));
            jsonObject.put("wzry", new JSONObject(new Gson().toJson(gameInfoBean.getWzry())));
            JSONArray jsonArray = new JSONArray();
            for (NormalGame normalGame : gameInfoBean.getOther()) {
                jsonArray.put(normalGame.get_id());
            }
            jsonObject.put("other", jsonArray);
            object.put("game_info", jsonObject);
        } catch (Throwable e) {
            e.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
        RetrofitHttpManager.getInstance().httpInterface.user_update_info(body).enqueue(new HttpCallback() {
            @Override
            public void OnSucceed(String result) {
                LogOut.debug("Update UserInfo" + result);
                if (!isViewAttached()) {
                    return;
                }
                getView().closeLoading();
                PorosUserInfo porosUserInfo = GT.fromJson(result, PorosUserInfo.class);
                if (AccountUtil.handleUserInfo(result)) {
                    getView().uploadUserInfoSuccess(porosUserInfo);
                } else {
                    getView().uploadUserInfoFailed();
                }
            }

            @Override
            public void OnFailed(int ret, String result) {
                if (!isViewAttached()) {
                    return;
                }
                getView().closeLoading();
            }
        });
    }
}
