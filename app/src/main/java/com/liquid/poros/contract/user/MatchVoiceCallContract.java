package com.liquid.poros.contract.user;

import com.liquid.base.mvp.BaseContract;
import com.liquid.poros.base.BPresenter;
import com.liquid.poros.entity.BaseModel;
import com.liquid.poros.entity.FemaleGuestCommentBean;
import com.liquid.poros.entity.MatchVoiceCallInfo;
import com.liquid.poros.entity.RoomDetailsInfo;
import com.liquid.poros.entity.UserCenterInfo;
import com.liquid.poros.entity.UserPhotoInfo;

import java.util.List;

public interface MatchVoiceCallContract {
    interface View extends BaseContract.BaseView {
        void onMatchedInfo(MatchVoiceCallInfo info);
        void onQuitRoom();
        void onHasTeamCard(int code);
        void onMatchLeave();
    }

    abstract class Presenter extends BPresenter<View> {

        /**
         * 获取用户信息
         *
         * @param target_user_id 用户id
         */
        public abstract void getMatchedInfo(String target_user_id);

        /**
         * 退出匹配房间
         *
         * @param on_startup
         */
        public abstract void cancelMatchRoom(boolean on_startup);


        /**
         * 拒绝开黑邀请
         *
         */
        public abstract void matchRoomRefuse(String roomId);

        /**
         * 检测是否有开黑时长
         *
         */
        public abstract void hasTeamCard(String roomId,String target_user_id);

        /**
         * 设置速配channelId
         * @param room_id
         * @param channel_id
         */
        public abstract void setChannelId(String room_id,String channel_id);
    }

}
