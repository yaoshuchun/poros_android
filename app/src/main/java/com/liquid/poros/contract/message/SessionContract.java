package com.liquid.poros.contract.message;

import com.liquid.base.mvp.BaseContract;
import com.liquid.poros.base.BPresenter;
import com.liquid.poros.entity.BoxKeyNumBean;
import com.liquid.poros.entity.GameRoomInfo;
import com.liquid.poros.entity.GiftItemInfo;
import com.liquid.poros.entity.GiftOfBox;
import com.liquid.poros.entity.MsgUserModel;
import com.liquid.poros.entity.RoomDetailsInfo;
import com.liquid.poros.entity.SessionDressBean;
import com.netease.nimlib.sdk.msg.model.IMMessage;

import java.util.List;

public interface SessionContract {
    interface View extends BaseContract.BaseView {
        /**
         * @param messages
         */
        void onMessageFetched(List<IMMessage> messages, boolean isForward);

        /**
         * 执行成功
         */
        void onSuccess();

        /**
         * 获取用户信息
         *
         * @param mMsgUserModel
         */
        void onUserInfoFetched(MsgUserModel mMsgUserModel);

        void onUserInfoToGirlFetched(MsgUserModel.UserInfo userInfo);

        void onRoomCreate(GameRoomInfo info);

        void onRoomClose(int msg);

        void onAddFriendFetched();

        void onMoneyNotEnough();

        void sendGiftSuccess(String msg, int code, GiftItemInfo giftInfo,String from);

        void onRoomDetailsFetched(RoomDetailsInfo roomDetailsInfo);

        void onDressUpdate(SessionDressBean mMsgUserModel);

        void cartoonHide();

        void showBoxKeyTip(BoxKeyNumBean.Data data);

        void buyMicCard();

        //拆礼盒获得某个奖品详情弹窗
        void showGiftDialog(GiftOfBox gift);

        //盲盒点击上报成功
        void blindBoxIntroSuccess();
    }

    abstract class Presenter extends BPresenter<SessionContract.View> {
        /**
         * 获取消息列表
         *
         * @param msgId
         */
        public abstract void getMessageList(String msgId, String target_user_id, boolean isForward,String from);

        /**
         * 添加好友
         *
         * @param target_user_id 目标用户id
         */
        public abstract void addFriend(String target_user_id);

        /**
         * 获取私聊用户状态信息
         *
         * @param target_user_id
         */
        public abstract void getUserStatusInfo(String target_user_id);

        public abstract void getP2PUserStatusInfo(String target_user_id);

        public abstract void cpScoreHint(String sessionId);

        public abstract void readMessage(long msg_time, String sessionId,boolean mIsExpireFri);

        public abstract void closeTeam(String teamId,boolean error);

        public abstract void quitTeam(String teamId,boolean error);

        public abstract void createCpRoom(String teamId, String target_user_id, String live_invite_id,String room_id);

        public abstract void setExptCpAvailable(String target_user_id, boolean clicked, boolean clicked_team);

        public abstract void sendGift(String target_user_id,int gift_id,int num,String from);
        public abstract void sendGift(String target_user_id,int gift_id,int num);

        public abstract void checkRankRaise(String target_user_id);

        public abstract void raiseGroupBack(String target_user_id);

        public abstract void fetchRoomDetails(String roomId);

        public abstract void getKeyBoxTip(String femaleId);

        public abstract void updateCurrentDress(String target_user_id);

        public abstract void blindBoxIntroClick(String target_user_id);
    }
}
