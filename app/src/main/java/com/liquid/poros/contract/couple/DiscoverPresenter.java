package com.liquid.poros.contract.couple;

import com.liquid.base.tools.GT;
import com.liquid.poros.entity.CoupleRoomListInfo;
import com.liquid.poros.entity.RoomDetailsInfo;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.poros.utils.network.HttpCallback;
import com.liquid.poros.utils.network.RetrofitHttpManager;

import org.json.JSONObject;

import okhttp3.RequestBody;

public class DiscoverPresenter extends DiscoverContract.Presenter {
    @Override
    public void fetchCoupleRoom(int page) {
        try {
            JSONObject object = new JSONObject();
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("page", page);

            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.couple_room_list(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    CoupleRoomListInfo coupleRoomListInfo = GT.fromJson(result, CoupleRoomListInfo.class);
                    if (coupleRoomListInfo != null && coupleRoomListInfo.getCode() == 0) {
                        getView().onCoupleRoomListFetched(coupleRoomListInfo);
                    }

                }

                @Override
                public void OnFailed(int ret, String result) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void fetchDispatchFemale(int dispatchType) {
        try {
            JSONObject object = new JSONObject();
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("dispatch_type", dispatchType);
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.user_dispatch_female(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                }

                @Override
                public void OnFailed(int ret, String result) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void fetchCancelApplyMic(String roomId) {
        try {
            JSONObject object = new JSONObject();
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("room_id", roomId);
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.couple_mic_leave(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                }

                @Override
                public void OnFailed(int ret, String result) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void fetchRoomDetails(String roomId) {
        try {
            JSONObject object = new JSONObject();
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("room_id", roomId);
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.couple_video_room_details(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    RoomDetailsInfo roomDetailsInfo = GT.fromJson(result, RoomDetailsInfo.class);
                    if (roomDetailsInfo != null && roomDetailsInfo.getCode() == 0) {
                        getView().onRoomDetailsFetched(roomDetailsInfo);
                    }
                }

                @Override
                public void OnFailed(int ret, String result) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
