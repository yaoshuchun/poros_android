package com.liquid.poros.contract.live;

import android.util.Log;

import com.liquid.base.tools.GT;
import com.liquid.base.utils.LogOut;
import com.liquid.poros.analytics.utils.MultiTracker;
import com.liquid.poros.entity.BaseModel;
import com.liquid.poros.entity.CoupleRoomInfo;
import com.liquid.poros.entity.ExclusiveApplyListInfo;
import com.liquid.poros.entity.GiftInfo;
import com.liquid.poros.entity.IsOnMic;
import com.liquid.poros.entity.MicListBean;
import com.liquid.poros.entity.PrivateRoomHeartBeatInfo;
import com.liquid.poros.entity.PrivateRoomMicInfo;
import com.liquid.poros.entity.UserRole;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.poros.utils.StringUtil;
import com.liquid.poros.utils.network.HttpCallback;
import com.liquid.poros.utils.network.RetrofitHttpManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.RequestBody;

public class RoomLivePresenter extends RoomLiveContract.Presenter {


    private void getroomInfo(String roomId, String invite_id, String source) {
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("room_id", roomId);
            if (StringUtil.isNotEmpty(invite_id)) {
                object.put("invite_id", invite_id);
                object.put("source", source);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
        RetrofitHttpManager.getInstance().httpInterface.couple_room_info(body).enqueue(new HttpCallback() {
            @Override
            public void OnSucceed(String result) {
                if (!isViewAttached()) {
                    return;
                }
                getView().closeLoading();
                CoupleRoomInfo roomInfo = GT.fromJson(result, CoupleRoomInfo.class);
                if (roomInfo != null && roomInfo.getCode() == 0 && roomInfo.getData() != null) {
                    getView().updateRoomInfo(roomInfo);
                }

                if (StringUtil.isNotEmpty(invite_id)) {
                    HashMap<String, String> msgParams2 = new HashMap<>();
                    msgParams2.put("user_id", AccountUtil.getInstance().getUserId());
                    msgParams2.put("room_id", roomId);
                    msgParams2.put("invite_id", invite_id + "");
                    MultiTracker.onEvent("b_video_room_invite_enter_success", msgParams2);
                }
            }

            @Override
            public void OnFailed(int ret, String result) {
            }
        });
    }

    @Override
    public void getRoomInfo(String roomId) {
        getroomInfo(roomId, "", "");
    }

    @Override
    public void getRoomInfo(String roomId, String invitee_id, String source) {
        getroomInfo(roomId, invitee_id, source);
    }

    @Override
    public void sendGift(String roomId, String target_user_id, int gift_id, int num, boolean to_anchor) {
        sendGift(roomId, target_user_id, gift_id, num, to_anchor, -1);
    }

    public void sendGift(String roomId, String target_user_id, int gift_id, int num, boolean to_anchor, int pos) {
        org.json.JSONObject object = new org.json.JSONObject();
        try {
            object.put("to_anchor", to_anchor);
            object.put("room_id", roomId);
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("gift_id", gift_id);
            object.put("target_user_id", target_user_id);
            object.put("num", num);
            if (pos != -1) {
                object.put("pos", pos);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
        RetrofitHttpManager.getInstance().httpInterface.send_gift(body).enqueue(new HttpCallback() {
            @Override
            public void OnSucceed(String result) {
                try {
                    JSONObject json = new JSONObject(result);
                    GiftInfo baseModel = GT.fromJson(json.getString("data"), GiftInfo.class);
                    if (baseModel != null) {
                        int spendCoin = baseModel.getSpend_coins();
                        getView().sendGiftSuccess(json.getString("msg"), json.getInt("code"), gift_id, spendCoin);
                    }
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }

            @Override
            public void OnFailed(int ret, String result) {
                getView().sendGiftFailed(gift_id, result);
            }
        });
    }


    @Override
    public void fetchApplyPrivateRoom(String roomId) {
        getView().showLoading();
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("room_id", roomId);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
        RetrofitHttpManager.getInstance().httpInterface.couple_private_room(body).enqueue(new HttpCallback() {
            @Override
            public void OnSucceed(String result) {
                if (!isViewAttached()) {
                    return;
                }
                getView().closeLoading();
                BaseModel baseModel = GT.fromJson(result, BaseModel.class);
                if (baseModel != null) {
                    if (baseModel.getCode() == 0) {
                        getView().applyPrivateRoomSuccess();
                    } else if (baseModel.getCode() == 17044) {
                        getView().onNotEnoughCoin(true);
                        getView().applyPrivateRoomFailed();
                    } else {
                        getView().applyPrivateRoomFailed();
                    }
                    getView().showMessage(baseModel.getMsg());
                }
            }

            @Override
            public void OnFailed(int ret, String result) {
                if (!isViewAttached()) {
                    return;
                }
                getView().applyPrivateRoomFailed();
                getView().closeLoading();
            }
        });
    }

    @Override
    public void fetchPrivateRoomHeartBeat(String roomId) {
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("room_id", roomId);
            RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.couple_private_room_heart_beat(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    getView().closeLoading();
                    try {
                        JSONObject json = new JSONObject(result);
                        PrivateRoomHeartBeatInfo privateRoomHeartBeatInfo = GT.fromJson(json.getString("data"), PrivateRoomHeartBeatInfo.class);
                        if (privateRoomHeartBeatInfo != null) {
                            privateRoomHeartBeatInfo.setCode(json.getInt("code"));
                            privateRoomHeartBeatInfo.setMsg(json.getString("msg"));
                            if (privateRoomHeartBeatInfo.getCode() == 0) {
                                LogOut.e("xxx", "计时心跳 == " + result);
                                getView().onPrivateRoomHeartBeat(privateRoomHeartBeatInfo);
                            } else {
                                getView().showMessage(privateRoomHeartBeatInfo.getMsg());
                            }
                        }
                    } catch (JSONException exception) {
                        exception.printStackTrace();
                    }
                }

                @Override
                public void OnFailed(int ret, String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    getView().closeLoading();
                    getView().onPrivateRoomJoinMicFailed();
                    getView().showMessage(result);
                }
            });
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @Override
    public void fetchPrivateRoomOnMic(String roomId) {
        getView().showLoading();
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("room_id", roomId);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
        RetrofitHttpManager.getInstance().httpInterface.couple_private_room_on_mic(body).enqueue(new HttpCallback() {
            @Override
            public void OnSucceed(String result) {
                if (!isViewAttached()) {
                    return;
                }
                getView().closeLoading();
                try {
                    JSONObject json = new JSONObject(result);
                    PrivateRoomMicInfo privateRoomMicInfo = GT.fromJson(json.getString("data"), PrivateRoomMicInfo.class);
                    if (privateRoomMicInfo != null) {
                        privateRoomMicInfo.setCode(json.getInt("code"));
                        privateRoomMicInfo.setMsg(json.getString("msg"));
                        if (privateRoomMicInfo.getCode() == 0) {
                            getView().onPrivateRoomMicSuccess(privateRoomMicInfo);
                        } else if (privateRoomMicInfo.getCode() == 17044) {
                            getView().onNotEnoughCoin(false);
                        } else if (privateRoomMicInfo.getCode() == 17004 || privateRoomMicInfo.getCode() == 17042) {
                            //麦上有用户 或已是专属房
                            getView().onPrivateRoomJoinMicFailed();
                        } else {
                            getView().onPrivateRoomJoinMicFailed();
                        }
                        getView().showMessage(privateRoomMicInfo.getMsg());
                    }
                } catch (JSONException exception) {
                    exception.printStackTrace();
                }
            }

            @Override
            public void OnFailed(int ret, String result) {
                if (!isViewAttached()) {
                    return;
                }
                getView().onPrivateRoomJoinMicFailed();
                getView().closeLoading();
            }
        });
    }

    @Deprecated
    public void isOnMic(String roomId) {
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("room_id", roomId);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
        RetrofitHttpManager.getInstance().httpInterface.is_on_mic(body).enqueue(new HttpCallback() {
            @Override
            public void OnSucceed(String result) {
                if (!isViewAttached()) {
                    return;
                }
                IsOnMic roomInfo = GT.fromJson(result, IsOnMic.class);
                if (roomInfo.getCode() == 0) {
                    getView().updateMic(roomInfo);
                }

            }

            @Override
            public void OnFailed(int ret, String result) {
            }
        });
    }

    @Override
    public void closeRoom(String roomId) {
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("room_id", roomId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
        RetrofitHttpManager.getInstance().httpInterface.couple_close_room(body).enqueue(new HttpCallback() {
            @Override
            public void OnSucceed(String result) {

            }

            @Override
            public void OnFailed(int ret, String result) {
                if (!isViewAttached()) {
                    return;
                }
                getView().closeLoading();
            }
        });
    }

    public void switchCamera(String roomId, boolean camera_close) {
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("room_id", roomId);
            object.put("camera_close", camera_close);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
        RetrofitHttpManager.getInstance().httpInterface.room_switch_camera(body).enqueue(new HttpCallback() {
            @Override
            public void OnSucceed(String result) {
                if (!isViewAttached()) {
                    return;
                }
                getView().closeLoading();
                BaseModel model = GT.fromJson(result, BaseModel.class);
                if (model != null) {
                    if (model.getCode() == 0) {
                        getView().onSwitchBack(camera_close);
                    } else {
                        getView().showMessage(model.getMsg());
                    }
                }

            }

            @Override
            public void OnFailed(int ret, String result) {
                if (!isViewAttached()) {
                    return;
                }
                getView().closeLoading();
            }
        });
    }

    @Deprecated
    public void kickMic(String roomId, String user_id) {
        getView().showLoading();
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("room_id", roomId);
            object.put("user_id", user_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
        RetrofitHttpManager.getInstance().httpInterface.room_kickout_mic(body).enqueue(new HttpCallback() {
            @Override
            public void OnSucceed(String result) {
                if (!isViewAttached()) {
                    return;
                }
                getView().closeLoading();
                BaseModel baseModel = GT.fromJson(result, BaseModel.class);
                if (baseModel != null) {
                    if (baseModel.getCode() == 0) {
                        getRoomInfo(roomId);
                    } else {
                        getView().showMessage(baseModel.getMsg());
                    }
                }

            }

            @Override
            public void OnFailed(int ret, String result) {
                if (!isViewAttached()) {
                    return;
                }
                getView().closeLoading();
            }
        });
    }

    public void banUser(String roomId, String user_id) {
        getView().showLoading();
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("room_id", roomId);
            object.put("target_user_id", user_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
        RetrofitHttpManager.getInstance().httpInterface.room_ban_user(body).enqueue(new HttpCallback() {
            @Override
            public void OnSucceed(String result) {
                if (!isViewAttached()) {
                    return;
                }
                getView().closeLoading();
                BaseModel baseModel = GT.fromJson(result, BaseModel.class);
                if (baseModel.getCode() == 0) {
                    getRoomInfo(roomId);
                } else {
                    getView().showMessage(baseModel.getMsg());
                }
            }

            @Override
            public void OnFailed(int ret, String result) {
                if (!isViewAttached()) {
                    return;
                }
                getView().closeLoading();
            }
        });
    }

    @Override
    public void kickOutMic(String roomId, String userId) {

    }

    @Override
    public void joinMic(boolean isFreeMic, String roomId, int mcPrice, boolean onlyUseCard) {
        getView().showLoading();

        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("room_id", roomId);
            if (onlyUseCard) {
                object.put("only_use_card", true);
            } else {
                if (!isFreeMic) {
                    object.put("mic_card_price", mcPrice);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
        RetrofitHttpManager.getInstance().httpInterface.room_on_mic(body).enqueue(new HttpCallback() {
            @Override
            public void OnSucceed(String result) {
                if (!isViewAttached()) {
                    return;
                }
                getView().closeLoading();
                try {
                    JSONObject json = new JSONObject(result);
                    if (json.getInt("code") == 0) {
                        getView().onJoinMicBack(json.getString("session_id"));
                    } else {
                        getView().showMessage(json.getString("msg"));
                    }
                } catch (JSONException exception) {
                    exception.printStackTrace();
                }

            }

            @Override
            public void OnFailed(int ret, String result) {
                if (!isViewAttached()) {
                    return;
                }
                getView().closeLoading();
            }
        });
    }

    @Override
    public void continueOnMic(boolean isFreeMic, String roomId, int mcPrice) {
        getView().showLoading();

        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("room_id", roomId);
            if (!isFreeMic) {
                object.put("mic_card_price", mcPrice);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
        RetrofitHttpManager.getInstance().httpInterface.room_continue_on_mic(body).enqueue(new HttpCallback() {
            @Override
            public void OnSucceed(String result) {
                if (!isViewAttached()) {
                    return;
                }
                getView().closeLoading();
                try {
                    JSONObject json = new JSONObject(result);
                    if (json.getInt("code") == 0) {
                        getView().onJoinMicBack(json.getString("session_id"));
                    } else {
                        getView().showMessage(json.getString("msg"));
                    }
                } catch (JSONException exception) {
                    exception.printStackTrace();
                }
            }

            @Override
            public void OnFailed(int ret, String result) {
                if (!isViewAttached()) {
                    return;
                }
                getView().closeLoading();
            }
        });
    }

    @Override
    public void quitMic(String roomId, boolean isPrivateRoom) {

        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("room_id", roomId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
        RetrofitHttpManager.getInstance().httpInterface.room_quit_on_mic(body).enqueue(new HttpCallback() {
            @Override
            public void OnSucceed(String result) {
                if (!isViewAttached()) {
                    return;
                }
                BaseModel baseModel = GT.fromJson(result, BaseModel.class);
                if (baseModel != null && baseModel.getCode() == 0) {
                    if (isPrivateRoom) {
                        getView().onPrivateRoomQuitMic();
                    } else {
                        getRoomInfo(roomId);
                    }
                } else {
                    getView().showMessage(baseModel.getMsg());
                }
            }

            @Override
            public void OnFailed(int ret, String result) {

            }
        });
    }

    public void quitMic(String roomId, String invite_id) {

        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("room_id", roomId);
            object.put("invite_id", invite_id);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
        RetrofitHttpManager.getInstance().httpInterface.room_quit_on_mic(body).enqueue(new HttpCallback() {
            @Override
            public void OnSucceed(String result) {

            }

            @Override
            public void OnFailed(int ret, String result) {

            }
        });
    }

    @Override
    public void setChannelId(String roomId, String channelId) {
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("room_id", roomId);
            object.put("channel_id", channelId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
        RetrofitHttpManager.getInstance().httpInterface.room_set_channel_id(body).enqueue(new HttpCallback() {
            @Override
            public void OnSucceed(String result) {

            }

            @Override
            public void OnFailed(int ret, String result) {
            }
        });
    }

    @Override
    public void uploadBehavior(String recordType, String roomId, String anchor, String targetId) {
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("record_type", recordType);
            object.put("room_id", roomId);
            object.put("anchor", anchor);
            object.put("target_user_id", targetId);
            RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.upload_behavior(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    Log.e("zzz", "点击上麦上报" + result);
                }

                @Override
                public void OnFailed(int ret, String result) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void inviteCoupleTeam(String roomId) {
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("room_id", roomId);
            RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.inviteCoupleTeam(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    BaseModel baseModel = GT.fromJson(result, BaseModel.class);
                    if (baseModel != null && baseModel.getCode() == 0) {
                        getView().onInviteCpTeamSuccess();
                    }
                }

                @Override
                public void OnFailed(int ret, String result) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void agreeCoupleTeam(String roomId, String targetId, String inviteId) {
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("room_id", roomId);
            object.put("invite_id", inviteId);
            object.put("target_user_id", targetId);
            RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.agreeCoupleTeam(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    BaseModel baseModel = GT.fromJson(result, BaseModel.class);
                    getView().onAgreeBack(baseModel, inviteId, targetId);
                }

                @Override
                public void OnFailed(int ret, String result) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void refuseCoupleTeam(String roomId, String targetId, String inviteId) {
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("room_id", roomId);
            object.put("invite_id", inviteId);
            object.put("target_user_id", targetId);
            RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.refuseCoupleTeam(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {

                }

                @Override
                public void OnFailed(int ret, String result) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addFriend(String user_id) {
        getView().showLoading();
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("target_user_id", user_id);

            RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.add_friend(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    getView().closeLoading();
                    BaseModel baseModel = GT.fromJson(result, BaseModel.class);
                    if (baseModel != null) {
                        if (baseModel.getCode() == 0) {
                            getView().addFriendSuccessFetch();
                        } else {
                            getView().showMessage(baseModel.getMsg());
                        }
                    }
                }

                @Override
                public void OnFailed(int ret, String result) {

                }
            });
        } catch (Exception e) {

            e.printStackTrace();
        }
    }


    public void queryUserRole(String groupId, String mRoomID, String account) {
        try {
            JSONObject object = new JSONObject();
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("group_id", groupId);
            object.put("user_id", account);
            object.put("team_id", mRoomID);

            RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.query_user_role(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    UserRole bm = GT.fromJson(result, UserRole.class);
                    if (!isViewAttached()) {
                        return;
                    }
                    if (bm != null && bm.getCode() == 0) {
                        getView().onQueryRoleSuccess(bm, account);
                    }
                }

                @Override
                public void OnFailed(int ret, String result) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void chatRoomBanUser(String mRoomID, int duration, String fromAccount) {
        getView().showLoading();
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("target_user_id", fromAccount);
            object.put("room_id", mRoomID);
            object.put("duration", duration);

            RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.couple_room_ban_user_speak(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    getView().closeLoading();
                    BaseModel baseModel = GT.fromJson(result, BaseModel.class);
                    if (baseModel.getCode() == 0) {
                        getView().showMessage(baseModel.getMsg());
                    }
                }

                @Override
                public void OnFailed(int ret, String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    getView().closeLoading();
                }
            });
        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    @Override
    public void fetchDispatchFemale(int dispatchType) {
        try {
            JSONObject object = new JSONObject();
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("dispatch_type", dispatchType);
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.user_dispatch_female(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                }

                @Override
                public void OnFailed(int ret, String result) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void fetchApplyMic(String roomId) {
        try {
            JSONObject object = new JSONObject();
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("room_id", roomId);
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.couple_waiting_list(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    getView().closeLoading();
                    BaseModel baseModel = GT.fromJson(result, BaseModel.class);
                    if (baseModel != null) {
                        if (baseModel.getCode() == 0) {
                            getView().onApplyMicSuccess();
                            getView().showMessage(baseModel.getMsg());
                        } else if (baseModel.getCode() == 17006) {
                            getView().applyMicCoinNotEnough();
                            getView().showMessage(baseModel.getMsg());
                        }
                    }
                }

                @Override
                public void OnFailed(int ret, String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    getView().closeLoading();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void pullMicList(String roomId, int page) {
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("room_id", roomId);
            object.put("page_idx", page);
            RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.pull_mic_list(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    MicListBean micListBean = GT.fromJson(result, MicListBean.class);
                    if (micListBean != null && micListBean.getCode() == 0 && micListBean.getData() != null) {
                        getView().roomMicListFetch(micListBean.getData());
                    }
                }

                @Override
                public void OnFailed(int ret, String result) {
                }
            });
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @Override
    public void pullPrivateMicList(String roomId, int page) {
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("room_id", roomId);
            object.put("page_idx", page);

            RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.private_room_request_list(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    ExclusiveApplyListInfo info = GT.fromJson(result, ExclusiveApplyListInfo.class);
                    if (info != null && info.getCode() == 0 && info.getData() != null && info.getData().getItems() != null) {
                        getView().privateRoomMicListFetch(info.getData());
                    }
                }

                @Override
                public void OnFailed(int ret, String result) {
                }
            });
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
}
