package com.liquid.poros.contract.account;

import com.liquid.base.tools.GT;
import com.liquid.poros.entity.RechargeRecordInfo;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.poros.utils.network.HttpCallback;
import com.liquid.poros.utils.network.RetrofitHttpManager;

import org.json.JSONObject;

import okhttp3.RequestBody;

/**
 * Created by sundroid on 29/07/2019.
 */

public class RechargeDetailsPresenter extends RechargeDetailsContract.Presenter {
    @Override
    public void getRechargeRecord(int page) {
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("page", page);
            object.put("page_size", 20);
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.coin_change_record(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    RechargeRecordInfo rechargeRecordInfo = GT.fromJson(result, RechargeRecordInfo.class);
                    if (rechargeRecordInfo != null && rechargeRecordInfo.getCode() == 0) {
                        getView().rechargeRecordFetch(rechargeRecordInfo);
                    }
                }

                @Override
                public void OnFailed(int ret, String result) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
