package com.liquid.poros.contract.message;

import android.os.CountDownTimer;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.liquid.base.adapter.CommonAdapter;
import com.liquid.base.event.EventCenter;
import com.liquid.base.tools.GT;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.entity.BaseModel;
import com.liquid.poros.entity.FriendMessageInfo;
import com.liquid.poros.entity.FriendMessageListInfo;
import com.liquid.poros.ui.activity.viewmodel.MessageListModel;
import com.liquid.poros.ui.fragment.home.MessageFragment;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.base.utils.LogOut;
import com.liquid.poros.utils.network.HttpCallback;
import com.liquid.poros.utils.network.RetrofitHttpManager;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;
import okhttp3.RequestBody;

public class MessagePresenter extends MessageContract.Presenter {
    private int NORMAL_MESSAGE = 0; //普通消息
    private int INTIMATE_MESSAGE = 1;  //亲密消息

    private int SHOW_NORMAL_SCORE_SESSION = 0;  //亲密消息入口隐藏
    private int SHOW_INTIMATE_SCORE_SESSION = 1;  //亲密消息入口展示
    private MessageFragment messageFragment;

    public void setMessageFragment(MessageFragment messageFragment) {
        this.messageFragment = messageFragment;
    }

    private CountDownTimer msgListTimer;
    private long pastSecond = 0;//过去了多少秒
    private StringBuffer stringBuffer = new StringBuffer();
    @Override
    public void getFriendMessageList() {
        try {
            JSONObject object = new JSONObject();
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("session_type", NORMAL_MESSAGE);
            object.put("show_high_score_session", SHOW_INTIMATE_SCORE_SESSION);
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.get_msg_session_list_v3(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    FriendMessageListInfo friendMessageInfo = GT.fromJson(result, FriendMessageListInfo.class);
                    getView().onFriendMessageListFetched(friendMessageInfo);

                }

                @Override
                public void OnFailed(int ret, String result) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteMessage(final int position, String target_user_id,boolean is_expire_list) {
        try {
            JSONObject object = new JSONObject();
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("target_user_id", target_user_id);
            object.put("is_expire_list", is_expire_list);

            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.delete_msg(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    getView().deleteMessageFetched(position);
                }

                @Override
                public void OnFailed(int ret, String result) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void topMessage(final String target_user_id) {
        try {
            JSONObject object = new JSONObject();
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("target_user_id", target_user_id);
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.top_msg(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    BaseModel baseModel = GT.fromJson(result, BaseModel.class);
                    if (baseModel != null && baseModel.getCode() == 0) {
                        getView().showMessage("已置顶");
                        getView().topOrUnTopMsgFetched();
                    }
                }

                @Override
                public void OnFailed(int ret, String result) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void unTopMessage(final String target_user_id) {
        try {
            JSONObject object = new JSONObject();
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("target_user_id", target_user_id);
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.untop_msg(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    BaseModel baseModel = GT.fromJson(result, BaseModel.class);
                    if (baseModel != null && baseModel.getCode() == 0) {
                        getView().showMessage("已取消置顶");
                        getView().topOrUnTopMsgFetched();
                    }
                }

                @Override
                public void OnFailed(int ret, String result) {
                    LogOut.debug("zzz", "取消置顶失败" + result);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addFriend(String target_user_id) {
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getUserInfo().getAccount_token());
            object.put("target_user_id", target_user_id);

            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.add_friend(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    LogOut.debug(result);
                    BaseModel baseModel = GT.fromJson(result, BaseModel.class);
                    if (baseModel != null) {
                        if (baseModel.getCode() == 0) {
                            EventBus.getDefault().post(new EventCenter(Constants.EVENT_CODE_REFRESH_MESSAGE));
                            getView().showMessage("添加成功");
                        } else {
                            getView().showMessage(baseModel.getMsg());
                        }
                    }
                }

                @Override
                public void OnFailed(int ret, String result) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 列表里是否有需要计时的item，如临时好友的24小时有效期 倒计时
     * @param friendMessageInfos
     * @return
     */
    public boolean hasTimerItem(List<FriendMessageInfo> friendMessageInfos){
        boolean flag = false;
        for (FriendMessageInfo friendMessageInfo : friendMessageInfos) {
            if (friendMessageInfo.getTemp_remain_time() > 0){
                friendMessageInfo.setCurr_temp_friend_remain_time(friendMessageInfo.getTemp_remain_time());
                flag = true;
            }
        }
        return flag;
    }

    /**
     * 如果有需要计时的item，开始计时
     */
    public void startListTimer(RecyclerView recyclerView, RecyclerView.Adapter<?> adapter,ArrayList<FriendMessageInfo> friendMessageInfos){
        stopListTimer();
        pastSecond = 0;
        LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        List<Integer> toDeleteArray = new ArrayList<>();
        msgListTimer = new CountDownTimer(Integer.MAX_VALUE, 1000){
            @Override
            public void onTick(long millisUntilFinished) {
                //更新已过去的秒数
                pastSecond ++;
                toDeleteArray.clear();
                //只更新界面可看到的,如果 recyclerView没有数据， firstVisibleItemPosition 和 lastVisibleItemPosition会返回 -1
                int firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition();
                int lastVisibleItemPosition = linearLayoutManager.findLastVisibleItemPosition();
                lastVisibleItemPosition = lastVisibleItemPosition >= friendMessageInfos.size() ? friendMessageInfos.size() - 1 : lastVisibleItemPosition;

                if (-1 == firstVisibleItemPosition || -1 == lastVisibleItemPosition){
                    stopListTimer();
                    return;
                }


                FriendMessageInfo friendMessageInfo;
                for (int i = firstVisibleItemPosition; i <= lastVisibleItemPosition; i++) {
                    friendMessageInfo = friendMessageInfos.get(i);//friendMessageInfos size为 0 时，会报：length=10 ？？？ ：java.lang.ArrayIndexOutOfBoundsException: length=10; index=-1
                    //如果是临时好友
                    if (friendMessageInfo.getTemp_remain_time() > 0){
                        //计时结束，放入列表，待删除该item
                        if (friendMessageInfo.getTemp_remain_time() - pastSecond <= 0){
                            toDeleteArray.add(i);
                        }else {
                            friendMessageInfo.setCurr_temp_friend_remain_time(friendMessageInfo.getTemp_remain_time() - pastSecond);
                            adapter.notifyItemChanged(i);
                        }
                    }
                }
                boolean isDeleted = false;
                //需倒着删除，正着删除会导致索引不正确
                for (int i = toDeleteArray.size() - 1; i >= 0; i--) {
                    friendMessageInfos.remove((int)toDeleteArray.get(i));
                    adapter.notifyItemRemoved((int)toDeleteArray.get(i));
                    isDeleted = true;
                }

                if (isDeleted && null != messageFragment){
                    messageFragment.tempFriendExpired();
                }

            }
            @Override
            public void onFinish() {
            }
        };
        msgListTimer.start();
    }

    /**
     * 如果 有需要计时的item，开始计时
     */
    public void stopListTimer(){
        if (null != msgListTimer){
            msgListTimer.cancel();
            msgListTimer = null;
        }
    }

}
