package com.liquid.poros.contract.audiomatch;

import com.liquid.base.mvp.BaseContract;
import com.liquid.poros.base.BPresenter;
import com.liquid.poros.entity.AudioMatchInfo;
import com.liquid.poros.entity.StartAudioMatchInfo;

public interface AudioMatchInfoContract {
    interface View extends BaseContract.BaseView {
        void fetchAudioMatchInfo(AudioMatchInfo audioMatchInfo);

        void startAudioMatchFetch(StartAudioMatchInfo startAudioMatchInfo, int zone, int rank);

        void notEnoughCoinFetch(String content);
    }

    abstract class Presenter extends BPresenter<View> {
        /**
         * 语音速配信息设置
         * matchType              Y  //匹配类型
         */
        public abstract void getAudioMatchInfo(String matchType);

        /**
         * 保存语音速配信息设置
         * matchType              Y  //匹配类型
         * zone                   Y  //游戏大区
         * rank                   Y  //游戏段位
         * isForce                N  //强制匹配，免费时长不足5分钟也进行匹配
         * isForce                Y  //入口类型  1 广场  2消息
         */
        public abstract void startAudioMatch(String matchType, int zone, int rank,boolean isForce,int entryType);
    }
}
