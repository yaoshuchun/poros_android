package com.liquid.poros.contract.user;

import com.liquid.base.mvp.BaseContract;
import com.liquid.poros.base.BPresenter;
import com.liquid.poros.entity.PorosUserInfo;
import com.liquid.poros.entity.UserCenterInfo;

import org.json.JSONArray;

public interface UserInfoSettingContract {
    interface View extends BaseContract.BaseView {
        void uploadImageSuccess(String path, String image_url);

        void uploadImageOssSuccess(String path);

        void onUserInfoFetch(UserCenterInfo userCenterInfo);

        void uploadUserInfoSuccess(PorosUserInfo userInfo);
    }

    abstract class Presenter extends BPresenter<View> {
        /**
         * 上传头像
         *
         * @param path 图片资源
         */
        public abstract void uploadImage(String path);

        /**
         * 上传
         *
         * @param file_dir 上传路径区分 user_cover_img 个人中心背景图，user_photo 个人相册
         */
        public abstract void uploadImageOss(String path, String file_dir);

        /**
         * 修改用户信息
         *
         * @param path 图片URL
         */
        public abstract void updateUserHead(String path);

        /**
         * 获取用户信息
         *
         * @param target_user_id 用户id
         */
        public abstract void getUserInfo(String target_user_id);

        /**
         * 更新用户信息
         */
        public abstract void updateUserInfo(String nickName, String avatarUrl, String gender, String age, String couple_desc, JSONArray games);
    }
}
