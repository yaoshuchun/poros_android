package com.liquid.poros.contract.report;

import com.liquid.base.tools.GT;
import com.liquid.poros.PorosApplication;
import com.liquid.poros.R;
import com.liquid.poros.entity.BaseModel;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.poros.utils.network.HttpCallback;
import com.liquid.poros.utils.network.RetrofitHttpManager;

import org.json.JSONObject;

import okhttp3.RequestBody;

/**
 * Created by sundroid on 20/03/2019.
 */

public class ReportPresenter extends ReportConstract.Presenter {
    @Override
    public void reportChatRoom(String room_id, String reason, String target_user_id) {
        getView().showLoading();
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("room_id", room_id);
            object.put("reason", reason);
            object.put("target_user_id", target_user_id);
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.report_chat_room(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    getView().closeLoading();
                    BaseModel baseModel = GT.fromJson(result, BaseModel.class);
                    if (baseModel!=null&&baseModel.getCode()==0){
                        getView().showMessage(PorosApplication.context.getString(R.string.report_success_tip));
                        getView().onReportSucceed();
                    }else {
                        getView().showMessage(baseModel.getMsg());
                    }
                }

                @Override
                public void OnFailed(int ret, String result) {
                    getView().closeLoading();
                }
            });
        } catch (Throwable e) {
            if (!isViewAttached()) {
                return;
            }
            getView().closeLoading();
            e.printStackTrace();
        }
    }

    @Override
    public void reportUser(String reason, String target_user_id) {
        getView().showLoading();
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("reason", reason);
            object.put("target_user_id", target_user_id);
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.report_user(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    getView().closeLoading();
                    BaseModel baseModel = GT.fromJson(result, BaseModel.class);
                    if (baseModel!=null&&baseModel.getCode()==0){
                        getView().showMessage(PorosApplication.context.getString(R.string.report_success_tip));
                        getView().onReportSucceed();
                    }else {
                        getView().showMessage(baseModel.getMsg());
                    }
                }

                @Override
                public void OnFailed(int ret, String result) {
                    getView().closeLoading();
                }
            });
        } catch (Throwable e) {
            if (!isViewAttached()) {
                return;
            }
            getView().closeLoading();
            e.printStackTrace();
        }
    }
}
