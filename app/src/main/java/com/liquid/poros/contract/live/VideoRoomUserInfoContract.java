package com.liquid.poros.contract.live;

import com.liquid.base.mvp.BaseContract;
import com.liquid.poros.base.BPresenter;
import com.liquid.poros.entity.VideoRoomUserInfo;

public interface VideoRoomUserInfoContract {
    interface View extends BaseContract.BaseView {
        void onUserInfoFetch(VideoRoomUserInfo userCenterInfo);
    }

    abstract class Presenter extends BPresenter<View> {
        /**
         * 获取用户信息
         *
         * @param target_user_id 用户id
         */
        public abstract void getUserInfo(String target_user_id);



    }
}
