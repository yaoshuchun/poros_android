package com.liquid.poros.contract.user;

import com.liquid.base.mvp.BaseContract;
import com.liquid.poros.base.BPresenter;
import com.liquid.poros.entity.CommentBean;
import com.liquid.poros.entity.FeedDetailBean;

public interface FeedDetailContract {
    interface View extends BaseContract.BaseView {
        /**
         * 刷新评论详情
         * @param bean
         */
        void updateFeedInfo(FeedDetailBean bean);

        void updateComment(CommentBean data);

        void updateCommentFail(String msg);

        void updateLikeResult();
    }

    abstract class Presenter extends BPresenter<View> {
        /**
         * 获取评论详情
         * @param feed_id 评论id
         * @param need_feed_info
         * @param source   来源
         */
        public abstract void getFeedDetail(String feed_id,int need_feed_info,String source);

        /**
         * 发布评论
         * @param text
         * @param feedId
         */
        public abstract void uploadComment(String text,String feedId);

        public abstract void feedLike(String feed_id,int is_like,String source);
        /**
         * 动态操作上报
         * @param feed_id
         * @param action
         * @param source
         */
        public abstract void feedActionReport(String feed_id,int action,String source);
    }

}
