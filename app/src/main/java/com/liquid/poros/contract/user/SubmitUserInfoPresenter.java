package com.liquid.poros.contract.user;


import com.liquid.base.tools.GT;
import com.liquid.poros.entity.PorosUserInfo;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.base.utils.LogOut;
import com.liquid.poros.utils.network.HttpCallback;
import com.liquid.poros.utils.network.RetrofitHttpManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import okhttp3.MediaType;
import okhttp3.RequestBody;

public class SubmitUserInfoPresenter extends SubmitUserInfoContract.Presenter {
    @Override
    public void updateUserInfo(int gender, String age, JSONArray games) {
        getView().showLoading();
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("gender", gender);
            object.put("age", age);
            object.put("games", games);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
        RetrofitHttpManager.getInstance().httpInterface.user_update_info(body).enqueue(new HttpCallback() {
            @Override
            public void OnSucceed(String result) {
                LogOut.debug("Update UserInfo" + result);
                if (!isViewAttached()) {
                    return;
                }
                getView().closeLoading();
                PorosUserInfo porosUserInfo = GT.fromJson(result, PorosUserInfo.class);
                if (AccountUtil.handleUserInfo(result)) {
                    getView().uploadUserInfoSuccess(porosUserInfo);
                } else {
                    getView().uploadUserInfoFailed();
                }
            }

            @Override
            public void OnFailed(int ret, String result) {
                if (!isViewAttached()) {
                    return;
                }
                getView().closeLoading();
            }
        });
    }
}
