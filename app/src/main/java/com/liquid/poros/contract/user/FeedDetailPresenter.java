package com.liquid.poros.contract.user;

import com.liquid.base.tools.GT;
import com.liquid.poros.entity.BaseModel;
import com.liquid.poros.entity.CommentData;
import com.liquid.poros.entity.FeedDetailData;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.poros.utils.network.HttpCallback;
import com.liquid.poros.utils.network.RetrofitHttpManager;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;

public class FeedDetailPresenter extends FeedDetailContract.Presenter {
    @Override
    public void getFeedDetail(String feed_id, int need_feed_info, String source) {
        RetrofitHttpManager.getInstance().httpInterface.get_comment_list(AccountUtil.getInstance().getAccount_token(), feed_id, need_feed_info, source).enqueue(new HttpCallback() {
            @Override
            public void OnSucceed(String result) {
                if (!isViewAttached()) {
                    return;
                }
                FeedDetailData feedDetailData = GT.fromJson(result, FeedDetailData.class);
                if (feedDetailData != null && feedDetailData.getCode() == 0) {
                    getView().updateFeedInfo(feedDetailData.data);
                }
            }

            @Override
            public void OnFailed(int ret, String result) {

            }
        });
    }

    @Override
    public void uploadComment(String text, String feedId) {
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("text", text);
            object.put("feed_id", feedId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
        RetrofitHttpManager.getInstance().httpInterface.upload_comment(body).enqueue(new HttpCallback() {
            @Override
            public void OnSucceed(String result) {
                if (!isViewAttached()) {
                    return;
                }
                CommentData commentData
                        = GT.fromJson(result, CommentData.class);
                if (commentData == null) return;
                if (commentData.getCode() == 0) {
                    getView().updateComment(commentData.data);
                } else if (commentData.getCode() == 20000) {
                    getView().updateCommentFail(commentData.getMsg());
                }
            }

            @Override
            public void OnFailed(int ret, String result) {

            }
        });
    }

    @Override
    public void feedLike(String feed_id, int is_like, String source) {
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("feed_id", feed_id);
            object.put("is_like", is_like);
            object.put("source", source);
            RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.feed_like(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    BaseModel baseModel = GT.fromJson(result, BaseModel.class);
                    if (baseModel != null) {
                        if (baseModel.getCode() == 0) {
                            getView().updateLikeResult();
                        } else {
                            getView().showMessage(baseModel.getMsg());
                        }
                    }

                }

                @Override
                public void OnFailed(int ret, String result) {

                }
            });
        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    @Override
    public void feedActionReport(String feed_id, int action, String source) {
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("feed_id", feed_id);
            object.put("action", action);
            object.put("source", source);
            RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.feedActionReport(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {

                }

                @Override
                public void OnFailed(int ret, String result) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
