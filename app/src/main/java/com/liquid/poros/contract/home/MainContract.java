package com.liquid.poros.contract.home;

import com.liquid.base.mvp.BaseContract;
import com.liquid.poros.base.BPresenter;
import com.liquid.poros.entity.KeyAdd;
import com.liquid.poros.entity.PrizeCoinInfo;
import com.liquid.poros.entity.UpdateInfo;


/**
 * Created by sundroid on 19/03/2019.
 */

public interface MainContract {
    interface View extends BaseContract.BaseView {

        void onUpdateFetch(UpdateInfo updateInfo);

        void onUpdateFail();

        void onFetchCoin(PrizeCoinInfo baseModel);

        void onShowKeyStatus(KeyAdd keyAdd);
    }

    abstract class Presenter extends BPresenter<View> {

        /**
         * 检查版本
         *
         * @param cur_version
         */
        public abstract void checkUpdate(String cur_version);
        /**
         * 重置在线状态
         *
         */
        public abstract void resetOnlineStatus();
        /**
         * 签到获取奖励金币
         */
        public abstract void getDailyPrizeCoin();

        /**
         * 首页请求接口查看是否有 获得钥匙
         */
        public abstract void checkKeyStatus();

    }
}
