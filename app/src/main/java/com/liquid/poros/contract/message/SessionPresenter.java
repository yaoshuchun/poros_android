package com.liquid.poros.contract.message;

import android.text.TextUtils;

import com.alibaba.fastjson.JSONObject;
import com.liquid.base.tools.GT;
import com.liquid.library.retrofitx.RetrofitX;
import com.liquid.poros.entity.BaseModel;
import com.liquid.poros.entity.BoxKeyNumBean;
import com.liquid.poros.entity.GameRoomInfo;
import com.liquid.poros.entity.GiftItemInfo;
import com.liquid.poros.entity.IMRoomMessageParser;
import com.liquid.poros.entity.MsgUserModel;
import com.liquid.poros.entity.RoomCreate;
import com.liquid.poros.entity.RoomDetailsInfo;
import com.liquid.poros.entity.SessionDressBean;
import com.liquid.poros.http.ApiUrl;
import com.liquid.poros.http.observer.ObjectObserver;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.poros.utils.network.HttpCallback;
import com.liquid.poros.utils.network.RetrofitHttpManager;
import com.netease.nimlib.sdk.msg.model.IMMessage;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import okhttp3.MediaType;
import okhttp3.RequestBody;

public class SessionPresenter extends SessionContract.Presenter {
    @Override
    public void getMessageList(String msgId, String target_user_id, boolean isForward,String from) {
        try {
            JSONObject object = new JSONObject();
            object.put("token", AccountUtil.getInstance().getUserInfo().getAccount_token());
            object.put("target_user_id", target_user_id);
            object.put("limit", 50);
            if ("temp".equals(from)) {
                object.put("is_expire_list",true);
            }else if ("init".equals(from)) {
                object.put("is_expire_list",false);
            }
            if (!TextUtils.isEmpty(msgId)) {
                object.put("msg_id", msgId);
            }
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.get_msg_list(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    List<IMMessage> messages = IMRoomMessageParser.parseJson(result);
                    getView().onMessageFetched(messages, isForward);
                }

                @Override
                public void OnFailed(int ret, String result) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    @Override
    public void fetchRoomDetails(String roomId) {
        try {
            JSONObject object = new JSONObject();
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("room_id", roomId);
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.couple_video_room_details(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    RoomDetailsInfo roomDetailsInfo = GT.fromJson(result, RoomDetailsInfo.class);
                    if (roomDetailsInfo != null && roomDetailsInfo.getCode() == 0) {
                        getView().onRoomDetailsFetched(roomDetailsInfo);
                    }
                }

                @Override
                public void OnFailed(int ret, String result) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getKeyBoxTip(String femaleId) {
        try {
            RetrofitX.Companion.url(ApiUrl.P2P_CHAT_KEY_TIP)
                    .param("target_user_id",femaleId)
                    .param("token", AccountUtil.getInstance().getAccount_token())
                    .postJSON()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new ObjectObserver<BoxKeyNumBean.Data>() {
                        @Override
                        public void failed(@NotNull BoxKeyNumBean.Data result) {

                        }

                        @Override
                        public void success(@NotNull BoxKeyNumBean.Data result) {
                            if (!isViewAttached()) {
                                return;
                            }
                            getView().showBoxKeyTip(result);
                        }
                    });
        }catch (Exception e) {

        }
    }

    @Override
    public void updateCurrentDress(String target_user_id) {
        try {
            RetrofitX.Companion.url(ApiUrl.CARTOON_SUIT)
                    .param("other_user_id", target_user_id)
                    .param("token", AccountUtil.getInstance().getAccount_token())
                    .get()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new ObjectObserver<SessionDressBean>() {
                        @Override
                        public void failed(@NotNull SessionDressBean result) {
                            if (!isViewAttached()) {
                                return;
                            }
                            if (result.getCode() == 20471) {
                                getView().cartoonHide();
                            }
                        }

                        @Override
                        public void success(@NotNull SessionDressBean result) {
                            if (!isViewAttached()) {
                                return;
                            }
                            getView().onDressUpdate(result);
                        }
                    });
        } catch (Exception e) {

        }
    }


    @Override
    public void addFriend(String target_user_id) {
       JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getUserInfo().getAccount_token());
            object.put("target_user_id", target_user_id);

            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.add_friend(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    BaseModel baseModel = GT.fromJson(result, BaseModel.class);
                    if (baseModel != null) {
                        if (baseModel.getCode() == 0) {
                            getView().onAddFriendFetched();
                            getView().showMessage("添加成功");
                        } else {
                            getView().showMessage(baseModel.getMsg());
                        }
                    }

                }

                @Override
                public void OnFailed(int ret, String result) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void cpScoreHint(String sessionId) {
        try {
            JSONObject object = new JSONObject();
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("target_user_id",sessionId);
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.cp_score_hint(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {

                }

                @Override
                public void OnFailed(int ret, String result) {

                }
            });
        } catch (Exception e) {

        }
    }

    @Override
    public void getP2PUserStatusInfo(String target_user_id) {
        try {
            RetrofitX.Companion.url(ApiUrl.GET_MSG_USER_INFO)
                    .param("target_user_id", target_user_id)
                    .param("token", AccountUtil.getInstance().getAccount_token())
                    .postJSON()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new ObjectObserver<MsgUserModel.UserInfo>() {

                        @Override
                        public void failed(@NotNull MsgUserModel.UserInfo result) {

                        }

                        @Override
                        public void success(@NotNull MsgUserModel.UserInfo result) {
                            if (result != null) {
                                getView().onUserInfoToGirlFetched(result);
                            }
                        }
                    });
        }catch (Exception e) {

        }
    }

    @Override
    public void getUserStatusInfo(String target_user_id) {
        try {
            JSONObject object = new JSONObject();
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("target_user_id", target_user_id);

            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.get_msg_user_info(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    MsgUserModel mMsgUserModel = GT.fromJson(result, MsgUserModel.class);
                    if (mMsgUserModel != null && mMsgUserModel.getCode() == 0 && mMsgUserModel.getData() != null) {
                        getView().onUserInfoFetched(mMsgUserModel);
                    }
                }

                @Override
                public void OnFailed(int ret, String result) {

                }
            });
        } catch (Exception e) {

        }
    }

    @Override
    public void readMessage(long msg_time, String sessionId,boolean mIsExpireFri) {
        try {
            //上报已读消息
            JSONObject object = new JSONObject();
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("target_user_id", sessionId);
            object.put("msg_time", msg_time);
            object.put("is_expire_list",mIsExpireFri);
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.read_msg(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {

                }

                @Override
                public void OnFailed(int ret, String result) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void recallMessage(String msg_id, String sessionId,boolean mIsExpireFri) {
        try {
            //上报已读消息
            JSONObject object = new JSONObject();
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("target_user_id", sessionId);
            object.put("is_expire_list",mIsExpireFri);
            object.put("msg_id", msg_id);
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.p2p_recall_msg(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    BaseModel baseModel = GT.fromJson(result, BaseModel.class);
                    if (baseModel != null) {
                        if (baseModel.getCode() == 0) {
                            getView().showMessage("消息撤回成功");
                        } else {
                            getView().showMessage(baseModel.getMsg());
                        }
                    }
                }

                @Override
                public void OnFailed(int ret, String result) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void closeTeam(String teamId, boolean error) {
        try {
            //上报已读消息
            JSONObject object = new JSONObject();
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("team_id", teamId);
            object.put("error", error);
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.close_team(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    BaseModel model = GT.fromJson(result, BaseModel.class);
                    if (model != null && model.getCode() == 0) {
                        getView().onRoomClose(3);
                    }
                }

                @Override
                public void OnFailed(int ret, String result) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void quitTeam(String teamId, boolean error) {
        try {
            //上报已读消息
            JSONObject object = new JSONObject();
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("team_id", teamId);
            object.put("error", error);
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.quit_team(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    BaseModel model = GT.fromJson(result, BaseModel.class);
                    if (model != null && model.getCode() == 0) {
                        getView().onRoomClose(1);
                    }
                }

                @Override
                public void OnFailed(int ret, String result) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void createCpRoom(String teamId, String target_user_id, String live_invite_id, String room_id) {
        try {
            JSONObject object = new JSONObject();
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("team_id", teamId);
            object.put("target_user_id", target_user_id);
            object.put("live_invite_id", live_invite_id);
            object.put("room_id", room_id);
            RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.join_cp_room(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    getView().closeLoading();
                    GameRoomInfo info = GT.fromJson(result, GameRoomInfo.class);
                    if (info != null && info.getCode() == 0) {
                        getView().onRoomCreate(info);
                    } else {
                        RoomCreate roomCreate = GT.fromJson(result, RoomCreate.class);
                        if (roomCreate.getCode() == 12005) {
                            closeTeam(roomCreate.getData().getTeam_id(), false);
                        } else if (roomCreate.getCode() == 17006) {
                            getView().onMoneyNotEnough();
                        } else if (roomCreate.getCode() == 17024) {
                            getView().buyMicCard();
                        } else {
                            getView().showMessage(info.getMsg());
                        }
                    }

                }

                @Override
                public void OnFailed(int ret, String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    getView().closeLoading();
                }
            });
        } catch (Throwable e) {
            e.printStackTrace();
        }

    }

    @Override
    public void setExptCpAvailable(String target_user_id, boolean clicked, boolean clicked_team) {
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("target_user_id", target_user_id);
            if (clicked) {
                object.put("clicked", clicked);
            }
            if (clicked_team) {
                object.put("clicked_team", clicked_team);
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
        RetrofitHttpManager.getInstance().httpInterface.expt_cp_available(body).enqueue(new HttpCallback() {
            @Override
            public void OnSucceed(String result) {

            }

            @Override
            public void OnFailed(int ret, String result) {

            }
        });
    }

    @Override
    public void sendGift(String target_user_id, int gift_id, int num) {
        sendGift(target_user_id, gift_id, num,"");
    }

    @Override
    public void sendGift(String target_user_id, int gift_id, int num,String from) {
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("gift_id", gift_id);
            object.put("target_user_id", target_user_id);
            object.put("num", num);
        } catch (Throwable e) {
            e.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
        RetrofitHttpManager.getInstance().httpInterface.send_gift(body).enqueue(new HttpCallback() {
            @Override
            public void OnSucceed(String result) {
                GiftItemInfo giftInfo = GT.fromJson(result, GiftItemInfo.class);
                if (giftInfo != null) {
                    getView().sendGiftSuccess(giftInfo.getMsg(), giftInfo.getCode(), giftInfo,from);
                }
            }

            @Override
            public void OnFailed(int ret, String result) {

            }
        });
    }

    @Override
    public void checkRankRaise(String target_user_id) {
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("target_user_id", target_user_id);
        } catch (Throwable e) {
            e.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
        RetrofitHttpManager.getInstance().httpInterface.check_rank_raise(body).enqueue(new HttpCallback() {
            @Override
            public void OnSucceed(String result) {

            }

            @Override
            public void OnFailed(int ret, String result) {

            }
        });
    }

    @Override
    public void raiseGroupBack(String target_user_id) {
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("female_user_id", target_user_id);
        } catch (Throwable e) {
            e.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
        RetrofitHttpManager.getInstance().httpInterface.raise_group_back(body).enqueue(new HttpCallback() {
            @Override
            public void OnSucceed(String result) {

            }

            @Override
            public void OnFailed(int ret, String result) {

            }
        });
    }


    /**
     * 拒绝组cp邀请
     *
     * @param cp_order_id 邀请ID
     */
    public void refuseCoupleInvite(String cp_order_id) {
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("cp_order_id", cp_order_id);
        } catch (Throwable e) {
            e.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
        RetrofitHttpManager.getInstance().httpInterface.refuse_couple_invite(body).enqueue(new HttpCallback() {
            @Override
            public void OnSucceed(String result) {

            }

            @Override
            public void OnFailed(int ret, String result) {

            }
        });
    }

    /**
     * 语音消息标记为已读
     *
     * @param msg_id 语音消息ID
     */
    public void p2pAudioMsgRead(String msg_id) {
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("msg_id", msg_id);
        } catch (Throwable e) {
            e.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
        RetrofitHttpManager.getInstance().httpInterface.p2p_audio_msg_read(body).enqueue(new HttpCallback() {
            @Override
            public void OnSucceed(String result) {

            }

            @Override
            public void OnFailed(int ret, String result) {

            }
        });
    }

    /**
     * 盲盒介绍点击上报
     */
    public void blindBoxIntroClick(String target_user_id) {
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("target_user_id", target_user_id);
            RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.blindBoxIntroClick(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    BaseModel baseModel = GT.fromJson(result, BaseModel.class);
                    if (baseModel != null && baseModel.getCode() == 0) {
                        getView().blindBoxIntroSuccess();
                    }
                }

                @Override
                public void OnFailed(int ret, String result) {

                }
            });
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
}
