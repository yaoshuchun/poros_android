package com.liquid.poros.contract.user;

import com.liquid.base.mvp.BaseContract;
import com.liquid.poros.base.BPresenter;
import com.liquid.poros.entity.BaseModel;
import com.liquid.poros.entity.FemaleGuestCommentBean;
import com.liquid.poros.entity.RoomDetailsInfo;
import com.liquid.poros.entity.UserCenterInfo;
import com.liquid.poros.entity.UserPhotoInfo;

import java.util.List;

public interface UserCenterContract {
    interface View extends BaseContract.BaseView {
        void uploadImageOssSuccess(String path);

        void onUserInfoFetch(UserCenterInfo userCenterInfo);

        void onUserPhotosFetch(UserPhotoInfo userPhotoInfo);

        void onDeletePhotosFetch();

        void blockUserSuccessFetch(BaseModel baseModel);

        void onRoomDetailsFetched(RoomDetailsInfo roomDetailsInfo);

        void updateCommentList(FemaleGuestCommentBean data);

        void updateLikeResult();
    }

    abstract class Presenter extends BPresenter<View> {
        /**
         * 上传
         *
         * @param file_dir 上传路径区分 user_cover_img 个人中心背景图，user_photo 个人相册
         */
        public abstract void uploadImageOss(List<String> path, String file_dir);

        /**
         * 获取用户信息
         *
         * @param target_user_id 用户id
         */
        public abstract void getUserInfo(String target_user_id);

        /**
         * 获取用户相册
         *
         * @param target_user_id 用户id
         */
        public abstract void getUserPhotos(int page, String target_user_id);

        /***
         * 获取动态列表
         * @param female_guest_id 用户id
         * @param last_id 翻页使用
         */
        public abstract void getCommentList(String female_guest_id,String last_id);

        /**
         * 删除相册
         *
         * @param photoId 相册id
         */
        public abstract void deleteUserPhoto(String photoId);

        /**
         * 拉黑用户
         *
         * @param target_user_id
         */
        public abstract void unblock_user(String target_user_id);

        public abstract void block_user(String target_user_id);

        /**
         * 获取房间类型
         *
         * @param roomId 房间id
         */
        public abstract void fetchRoomDetails(String roomId);

        public abstract void feedLike(String feed_id,int is_like,String source);
        /**
         * 动态操作上报
         * @param feed_id
         * @param action
         * @param source
         */
        public abstract void feedActionReport(String feed_id,int action,String source);
    }

}
