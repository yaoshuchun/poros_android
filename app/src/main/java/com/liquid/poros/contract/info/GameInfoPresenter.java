package com.liquid.poros.contract.info;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.poros.utils.network.HttpCallback;
import com.liquid.poros.utils.network.RetrofitHttpManager;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;

public class GameInfoPresenter extends GameInfoContract.Presenter {
    @Override
    public void setAccountInfo(String nickName, String gameZone) {
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("hpjy_game_nick", nickName);
            object.put("hpjy_game_area", gameZone);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
        RetrofitHttpManager.getInstance().httpInterface.set_hpjy_account_info(body).enqueue(new HttpCallback() {
            @Override
            public void OnSucceed(String result) {
                if (!isViewAttached()) {
                    return;
                }
                if (AccountUtil.handleUserInfo1(result)) {
                    getView().onAccountInfoFetched();
                }else {
                    getView().onAccountInfoFailFetched();
                }
            }

            @Override
            public void OnFailed(int ret, String result) {
            }
        });
    }
}
