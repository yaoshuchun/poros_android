package com.liquid.poros.contract.likecard;

import com.liquid.base.tools.GT;
import com.liquid.poros.entity.LikeCardInfo;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.poros.utils.network.HttpCallback;
import com.liquid.poros.utils.network.RetrofitHttpManager;

import org.json.JSONObject;

import okhttp3.RequestBody;

public class LikeCardPresenter extends LikeCardContract.Presenter {
    @Override
    public void fetchedLikeCardList(int page) {
        try {
            JSONObject object = new JSONObject();
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("page", page);
            object.put("page_size", 20);
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.gift_card_change_record(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    LikeCardInfo likeCardInfo = GT.fromJson(result, LikeCardInfo.class);
                    if (likeCardInfo != null) {
                        if (likeCardInfo.getCode() == 0) {
                            getView().onLikeCardListFetched(likeCardInfo);
                        } else {
                            getView().showMessage(likeCardInfo.getMsg());
                        }
                    }
                }

                @Override
                public void OnFailed(int ret, String result) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
