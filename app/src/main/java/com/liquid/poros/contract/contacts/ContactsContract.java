package com.liquid.poros.contract.contacts;

import com.liquid.base.mvp.BaseContract;
import com.liquid.poros.base.BPresenter;
import com.liquid.poros.entity.ContactsInfo;

/**
 * Created by sundroid on 29/07/2019.
 */

public interface ContactsContract {
    interface View extends BaseContract.BaseView {

        void onContactFetch(ContactsInfo contactListInfo);

        void onDeleteFriendFetch();
    }

    abstract class Presenter extends BPresenter<View> {
        /**
         * 获取好友列表
         */
        public abstract void getFriendList();

        /**
         * 删除好友
         *
         * @param target_user_id 目标用户id
         */
        public abstract void deleteFriend(String target_user_id);
    }
}
