package com.liquid.poros.contract.live;

import com.liquid.base.mvp.BaseContract;
import com.liquid.poros.base.BPresenter;
import com.liquid.poros.entity.BaseModel;
import com.liquid.poros.entity.CoupleRoomInfo;
import com.liquid.poros.entity.ExclusiveApplyListInfo;
import com.liquid.poros.entity.IsOnMic;
import com.liquid.poros.entity.MicListBean;
import com.liquid.poros.entity.PrivateRoomHeartBeatInfo;
import com.liquid.poros.entity.PrivateRoomMicInfo;
import com.liquid.poros.entity.RoomOnMicV2;
import com.liquid.poros.entity.UserRole;

public interface RoomLiveContract {
    interface View extends BaseContract.BaseView {

        void updateRoomInfo(CoupleRoomInfo roomInfo);

        void addFriendSuccessFetch();

        void onJoinMicBack(String sessionId);

        void onQueryRoleSuccess(UserRole bm, String account);

        void updateMic(IsOnMic roomInfo);

        void onAgreeBack(BaseModel baseModel, String liveInviteId, String targetId);

        void onInviteCpTeamSuccess();

        void onSwitchBack(boolean camera_close);

        void onApplyMicSuccess();

        void applyMicCoinNotEnough();

        void sendGiftSuccess(String msg, int code, int giftId, int spendCoin);

        void sendGiftFailed(int giftId, String result);

        void applyPrivateRoomSuccess();

        void applyPrivateRoomFailed();

        void onNotEnoughCoin(boolean isApply);

        void onPrivateRoomHeartBeat(PrivateRoomHeartBeatInfo privateRoomHeartBeatInfo);

        void onPrivateRoomMicSuccess(PrivateRoomMicInfo privateRoomMicInfo);

        void onPrivateRoomQuitMic();

        void onPrivateRoomJoinMicFailed();

        void roomMicListFetch(MicListBean.DataBean micListBean);

        void privateRoomMicListFetch(ExclusiveApplyListInfo.Data exclusiveApplyListInfo);
    }

    abstract class Presenter extends BPresenter<View> {
        /**
         * 获取组队房间信息
         *
         * @param roomId
         */
        public abstract void getRoomInfo(String roomId);

        /**
         * 获取组队房间信息
         *
         * @param roomId
         */
        public abstract void getRoomInfo(String roomId, String invitee_id, String source);

        /**
         * 关闭组队房间
         *
         * @param roomId
         */
        public abstract void closeRoom(String roomId);

        /**
         * 踢下麦
         *
         * @param roomId
         * @param userId
         */
        public abstract void kickOutMic(String roomId, String userId);

        /**
         * 上麦接口
         *
         * @param roomId
         * @param isFreeMic 是否免费
         * @param mcPrice   上麦卡价格
         */
        public abstract void joinMic(boolean isFreeMic, String roomId, int mcPrice, boolean joinMic);

        /**
         * 继续上麦
         *
         * @param roomId
         * @param isFreeMic 是否免费
         * @param mcPrice   上麦卡价格
         */
        public abstract void continueOnMic(boolean isFreeMic, String roomId, int mcPrice);

        /**
         * 下麦接口
         *
         * @param roomId
         * @param isPrivateRoom 专属房
         */
        public abstract void quitMic(String roomId, boolean isPrivateRoom);

        /**
         * 组队房间设置通道id
         *
         * @param roomId
         * @param channelId
         */
        public abstract void setChannelId(String roomId, String channelId);

        /**
         * 用户行为上报
         *
         * @param recordType 用户行为
         * @param roomId     房间ID
         * @param anchor     主播ID
         * @param targetId   目标用户ID
         */
        public abstract void uploadBehavior(String recordType, String roomId, String anchor, String targetId);


        /**
         * 主持人邀请加入专属开黑房间
         *
         * @param roomId
         */
        public abstract void inviteCoupleTeam(String roomId);

        /**
         * 用户接受主持人邀请加入专属开黑房间
         *
         * @param roomId
         * @param targetId
         */
        public abstract void agreeCoupleTeam(String roomId, String targetId, String inviteId);

        /**
         * 用户拒绝主持人邀请加入专属开黑房间
         *
         * @param roomId
         * @param targetId
         * @param inviteId
         */
        public abstract void refuseCoupleTeam(String roomId, String targetId, String inviteId);

        /**
         * 足迹触发
         */
        public abstract void fetchDispatchFemale(int dispatchType);

        /**
         * 申请上麦
         */
        public abstract void fetchApplyMic(String roomId);

        /**
         * 送礼物
         */
        public abstract void sendGift(String roomId, String target_user_id, int gift_id, int num, boolean to_anchor);

        /**
         * 申请进入专属房
         */
        public abstract void fetchApplyPrivateRoom(String roomId);

        /**
         * 专属房心跳
         */
        public abstract void fetchPrivateRoomHeartBeat(String roomId);

        /**
         * 专属房上麦
         */
        public abstract void fetchPrivateRoomOnMic(String roomId);

        /**
         * 视频房申请上麦列表
         *
         * @param roomId
         * @param page
         */
        public abstract void pullMicList(String roomId, int page);

        /**
         * 专属房申请上麦列表
         *
         * @param roomId
         * @param page
         */
        public abstract void pullPrivateMicList(String roomId, int page);

    }
}
