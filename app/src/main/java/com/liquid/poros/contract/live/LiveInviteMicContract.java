package com.liquid.poros.contract.live;

import com.liquid.base.mvp.BaseContract;
import com.liquid.poros.base.BPresenter;
import com.liquid.poros.entity.BaseModel;
import com.liquid.poros.entity.UserCoinInfo;

public interface LiveInviteMicContract {
    interface View extends BaseContract.BaseView {

        void onUserCoinSuccessFetch(UserCoinInfo userCoinInfo);

        void userMicCardFetch(BaseModel baseModel,boolean onlyUseCard);
    }

    abstract class Presenter extends BPresenter<View> {
        /**
         * 获取用户余额
         *
         * @param
         */
        public abstract void getUserCoin();

        /**
         * 使用/购买上麦卡
         * target_user_id         目标用户ID
         * only_use_card          true是仅使用上麦卡 false为购买并使用上麦卡
         * price                  购买上麦卡花费金币
         */
        public abstract void userMicCard(String roomId, boolean onlyUseCard, String price);

    }
}
