package com.liquid.poros.contract.couple;

import com.liquid.base.mvp.BaseContract;
import com.liquid.poros.base.BPresenter;
import com.liquid.poros.entity.CoupleRoomListInfo;
import com.liquid.poros.entity.RoomDetailsInfo;

public interface DiscoverContract {
    interface View extends BaseContract.BaseView {
        void onCoupleRoomListFetched(CoupleRoomListInfo coupleRoomListInfo);

        void onRoomDetailsFetched(RoomDetailsInfo roomDetailsInfo);
    }

    abstract class Presenter extends BPresenter<View> {
        /**
         * 获取组cp房间列表
         *
         * @param page 页码
         */
        public abstract void fetchCoupleRoom(int page);

        /**
         * 足迹触发
         */
        public abstract void fetchDispatchFemale(int dispatchType);

        /**
         * 离开视频房 取消申请上麦
         */
        public abstract void fetchCancelApplyMic(String roomId);

        /**
         * 获取房间类型
         *
         * @param roomId 房间id
         */
        public abstract void fetchRoomDetails(String roomId);
    }
}
