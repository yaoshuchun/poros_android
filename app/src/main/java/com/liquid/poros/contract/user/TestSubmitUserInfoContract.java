package com.liquid.poros.contract.user;

import com.liquid.base.mvp.BaseContract;
import com.liquid.poros.base.BPresenter;
import com.liquid.poros.entity.PorosUserInfo;
import com.liquid.poros.entity.TestSubmitUserInfo;
import com.liquid.poros.entity.UserCenterInfo;

import org.json.JSONArray;
import org.json.JSONObject;

public interface TestSubmitUserInfoContract {
    interface View extends BaseContract.BaseView {
        void uploadUserInfoSuccess(PorosUserInfo userInfo);

        void uploadUserInfoFailed();

        void fetchUserGame(TestSubmitUserInfo testSubmitUserInfo);
    }

    abstract class Presenter extends BPresenter<TestSubmitUserInfoContract.View> {
        /**
         * 获取游戏静态信息
         */
        public abstract void getUserGame();

        /**
         * 更新用户信息
         * gender         性别
         * age            年龄
         * zodiac         属相
         * gameInfoBean   游戏标签
         */
        public abstract void updateUserInfo(int gender, int age,int zodiac,UserCenterInfo.Data.UserInfo.GameInfoBean gameInfoBean);
    }
}
