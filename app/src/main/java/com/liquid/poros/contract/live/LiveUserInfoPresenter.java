package com.liquid.poros.contract.live;

import com.liquid.base.tools.GT;
import com.liquid.poros.entity.EditGameBean;
import com.liquid.poros.entity.NormalGame;
import com.liquid.poros.entity.SettingEditGameBean;
import com.liquid.poros.entity.UserCenterInfo;
import com.liquid.poros.entity.UserPhotoInfo;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.poros.utils.network.HttpCallback;
import com.liquid.poros.utils.network.RetrofitHttpManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;

public class LiveUserInfoPresenter extends LiveUserInfoContract.Presenter {
    @Override
    public void getUserInfo(String target_user_id) {
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("target_user_id", target_user_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
        RetrofitHttpManager.getInstance().httpInterface.user_info_other(body).enqueue(new HttpCallback() {
            @Override
            public void OnSucceed(String result) {
                if (!isViewAttached()) {
                    return;
                }
                UserCenterInfo baseModel = GT.fromJson(result, UserCenterInfo.class);
                if (baseModel == null) {
                    return;
                }
                if (baseModel.getCode() == 0) {
                    try {
                        //王者荣耀
                        ArrayList<Map<String, String>> wzry_platform_list = new ArrayList<>();
                        ArrayList<Map<String, String>> wzry_rank_list = new ArrayList<>();
                        ArrayList<Map<String, String>> wzry_area_list = new ArrayList<>();

                        JSONObject resultObj = new JSONObject(result);
                        JSONObject dataObj = resultObj.optJSONObject("data");
                        if (dataObj != null) {
                            JSONObject user_infoObj = dataObj.optJSONObject("user_info");
                            JSONObject game_info_static = user_infoObj.optJSONObject("game_info_static");
                            SettingEditGameBean staticGameBean = new SettingEditGameBean();
                            //game_info_static
                            JSONObject wzryObj = game_info_static.optJSONObject("wzry");
                            SettingEditGameBean.SettingGameBean honor_of_kings = new SettingEditGameBean.SettingGameBean();
                            honor_of_kings.platform = new EditGameBean();
                            honor_of_kings.platform.des = "游戏平台";

                            JSONObject platformObj = wzryObj.optJSONObject("platform");
                            Iterator<String> iterator = platformObj.keys();
                            while (iterator.hasNext()) {
                                Map<String, String> map2 = new HashMap<String, String>();
                                String key = iterator.next();
                                String value = platformObj.getString(key);
                                map2.put(key, value);
                                wzry_platform_list.add(map2);
                                EditGameBean.Item item = new EditGameBean.Item();
                                item.id = key;
                                item.name = value;
                                honor_of_kings.platform.items.add(item);
                            }

                            JSONObject rankObj = wzryObj.optJSONObject("rank");
                            honor_of_kings.rank = new EditGameBean();
                            honor_of_kings.rank.des = "段位";
                            Iterator<String> rankIterator = rankObj.keys();
                            while (rankIterator.hasNext()) {
                                Map<String, String> map2 = new HashMap<String, String>();
                                String key = rankIterator.next();
                                String value = rankObj.getString(key);
                                map2.put(key, value);
                                wzry_rank_list.add(map2);
                                EditGameBean.Item item = new EditGameBean.Item();
                                item.id = key;
                                item.name = value;
                                honor_of_kings.rank.items.add(item);
                            }

                            JSONObject zoneObj = wzryObj.optJSONObject("zone");
                            honor_of_kings.zone = new EditGameBean();
                            honor_of_kings.zone.des = "游戏大区";
                            Iterator<String> zoneIterator = zoneObj.keys();
                            while (zoneIterator.hasNext()) {
                                Map<String, String> map2 = new HashMap<String, String>();
                                String key = zoneIterator.next();
                                String value = zoneObj.getString(key);
                                map2.put(key, value);
                                wzry_area_list.add(map2);
                                EditGameBean.Item item = new EditGameBean.Item();
                                item.id = key;
                                item.name = value;
                                honor_of_kings.zone.items.add(item);
                            }
                            String wzryIconString = wzryObj.optString("icon");
                            honor_of_kings.icon = wzryIconString;
                            staticGameBean.honor_of_kings = honor_of_kings;

                            UserCenterInfo.WZRYDATA wzrydata = new UserCenterInfo.WZRYDATA();
                            wzrydata.setWzry_platform_list(wzry_platform_list);
                            wzrydata.setWzry_area_list(wzry_area_list);
                            wzrydata.setWzry_rank_list(wzry_rank_list);
                            wzrydata.setIcon(wzryIconString);

                            baseModel.setWzrydata(wzrydata);

                            //和平精英
                            ArrayList<Map<String, String>> hpjy_platform_list = new ArrayList<>();
                            ArrayList<Map<String, String>> hpjy_rank_list = new ArrayList<>();
                            ArrayList<Map<String, String>> hpjy_area_list = new ArrayList<>();
                            JSONObject hpjyObj = game_info_static.optJSONObject("hpjy");

                            SettingEditGameBean.SettingGameBean peace_elite = new SettingEditGameBean.SettingGameBean();
                            peace_elite.platform = new EditGameBean();
                            peace_elite.platform.des = "游戏平台";

                            JSONObject hpjyplatformObj = hpjyObj.optJSONObject("platform");
                            Iterator<String> hpjyiterator = hpjyplatformObj.keys();
                            while (hpjyiterator.hasNext()) {
                                Map<String, String> map2 = new HashMap<String, String>();
                                String key = hpjyiterator.next();
                                String value = hpjyplatformObj.getString(key);
                                map2.put(key, value);
                                hpjy_platform_list.add(map2);
                                EditGameBean.Item item = new EditGameBean.Item();
                                item.id = key;
                                item.name = value;
                                peace_elite.platform.items.add(item);
                            }

                            JSONObject hpjyrankObj = hpjyObj.optJSONObject("rank");
                            peace_elite.rank = new EditGameBean();
                            peace_elite.rank.des = "段位";

                            Iterator<String> hpjyrankIterator = hpjyrankObj.keys();
                            while (hpjyrankIterator.hasNext()) {
                                Map<String, String> map2 = new HashMap<String, String>();
                                String key = hpjyrankIterator.next();
                                String value = hpjyrankObj.getString(key);
                                map2.put(key, value);
                                hpjy_rank_list.add(map2);
                                EditGameBean.Item item = new EditGameBean.Item();
                                item.id = key;
                                item.name = value;
                                peace_elite.rank.items.add(item);
                            }

                            JSONObject hpjyzoneObj = hpjyObj.optJSONObject("zone");
                            peace_elite.zone = new EditGameBean();
                            peace_elite.zone.des = "游戏大区";
                            Iterator<String> hpjyzoneIterator = hpjyzoneObj.keys();
                            while (hpjyzoneIterator.hasNext()) {
                                Map<String, String> map2 = new HashMap<String, String>();
                                String key = hpjyzoneIterator.next();
                                String value = hpjyzoneObj.getString(key);
                                map2.put(key, value);
                                hpjy_area_list.add(map2);
                                EditGameBean.Item item = new EditGameBean.Item();
                                item.id = key;
                                item.name = value;
                                peace_elite.zone.items.add(item);
                            }
                            String hpjyIconString = hpjyObj.optString("icon");
                            peace_elite.icon = hpjyIconString;
                            staticGameBean.peace_elite = peace_elite;

                            SettingEditGameBean.SettingOtherGameBean others = new SettingEditGameBean.SettingOtherGameBean();
                            List<NormalGame> other = baseModel.getData().getUser_info().getGame_info_static().getOther();
                            for (NormalGame normalGame : other) {
                                EditGameBean.Item item = new EditGameBean.Item();
                                item.id = normalGame.get_id() + "";
                                item.name = normalGame.getName();
                                item.image_url = normalGame.getIcon();
                                others.list.add(item);
                            }
                            others.icon = game_info_static.optString("other_game_icon");
                            staticGameBean.other = others;
                            baseModel.setStaticGameBean(staticGameBean);

                            UserCenterInfo.HPJYDATA hpjydata = new UserCenterInfo.HPJYDATA();
                            hpjydata.setHpjy_platform_list(hpjy_platform_list);
                            hpjydata.setHpjy_area_list(hpjy_area_list);
                            hpjydata.setHpjy_rank_list(hpjy_rank_list);
                            hpjydata.setIcon(hpjyIconString);

                            baseModel.setHpjydata(hpjydata);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    getView().onUserInfoFetch(baseModel);
                } else {
                    getView().showMessage(baseModel.getMsg());
                }
            }

            @Override
            public void OnFailed(int ret, String result) {
            }
        });
    }

    @Override
    public void getUserPhotos(String target_user_id) {
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("target_user_id", target_user_id);
            object.put("page", 0);
            object.put("page_size", 20);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
        RetrofitHttpManager.getInstance().httpInterface.user_photos(body).enqueue(new HttpCallback() {
            @Override
            public void OnSucceed(String result) {
                if (!isViewAttached()) {
                    return;
                }
                UserPhotoInfo baseModel = GT.fromJson(result, UserPhotoInfo.class);
                if (baseModel!=null){
                    if (baseModel.getCode() == 0) {
                        getView().onUserPhotosFetch(baseModel);
                    } else {
                        getView().showMessage(baseModel.getMsg());
                    }
                }

            }

            @Override
            public void OnFailed(int ret, String result) {
            }
        });
    }

}
