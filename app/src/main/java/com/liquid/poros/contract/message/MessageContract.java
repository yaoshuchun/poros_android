package com.liquid.poros.contract.message;

import com.liquid.base.mvp.BaseContract;
import com.liquid.poros.base.BPresenter;
import com.liquid.poros.entity.FriendMessageListInfo;


public interface MessageContract {
    interface View extends BaseContract.BaseView {
        void onFriendMessageListFetched(FriendMessageListInfo friendMessageListInfo);

        void deleteMessageFetched(int position);

        void topOrUnTopMsgFetched();

    }

    abstract class Presenter extends BPresenter<View> {
        /**
         * 获取消息&粉丝列表
         */
        public abstract void getFriendMessageList();

        /**
         * 删除消息
         *
         * @param position       消息位置
         * @param target_user_id 目标用户id
         */
        public abstract void deleteMessage(int position, String target_user_id,boolean is_expire_list);

        /**
         * 置顶消息
         *
         * @param target_user_id 目标用户id
         */
        public abstract void topMessage(String target_user_id);

        /**
         * 取消置顶
         *
         * @param target_user_id 目标用户id
         */
        public abstract void unTopMessage(String target_user_id);

        /**
         * 添加好友
         *
         * @param target_user_id 目标用户id
         */
        public abstract void addFriend(String target_user_id);

    }
}
