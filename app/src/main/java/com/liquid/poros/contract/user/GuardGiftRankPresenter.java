package com.liquid.poros.contract.user;

import com.liquid.base.tools.GT;
import com.liquid.poros.entity.GuardRankerData;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.poros.utils.network.HttpCallback;
import com.liquid.poros.utils.network.RetrofitHttpManager;
import org.json.JSONObject;
import okhttp3.RequestBody;

public class GuardGiftRankPresenter extends GuardGiftRankContract.Presenter{

    @Override
    public void getUserRankInfo(String male_user_id) {
        JSONObject object = new JSONObject();
        try {
            object.put("target_user_id", male_user_id);
            object.put("token", AccountUtil.getInstance().getAccount_token());
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.get_rank_list(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    try {
                        GuardRankerData data = GT.fromJson(result, GuardRankerData.class);
                        if (data != null){
                            if (data.getCode() == 0) {
                                getView().onRankInfo(data.data);
                            } else {
                                getView().showMessage(data.getMsg());
                            }
                        }
                    }catch (Exception e) {
                    }
                }

                @Override
                public void OnFailed(int ret, String result) {

                }
            });
        } catch (Exception e) {
            getView().closeLoading();
            e.printStackTrace();
        }
    }
}
