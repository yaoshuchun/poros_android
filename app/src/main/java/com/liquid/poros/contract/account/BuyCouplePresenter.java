package com.liquid.poros.contract.account;

import com.liquid.base.tools.GT;
import com.liquid.poros.entity.BaseModel;
import com.liquid.poros.entity.BuyCoupleCardInfo;
import com.liquid.poros.entity.CoupleCardListInfo;
import com.liquid.poros.entity.UserCoinInfo;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.poros.utils.network.HttpCallback;
import com.liquid.poros.utils.network.RetrofitHttpManager;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;

/**
 * Created by sundroid on 25/03/2019.
 */

public class BuyCouplePresenter extends BuyCoupleContract.Presenter {
    @Override
    public void buyCouple(boolean isAgreeCp, String roomId, String roomAnchor, int cardType, String cardPrice, String targetId, String cp_order_id) {
        JSONObject object = new JSONObject();
        getView().showLoading();

        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("room_id", roomId);
            object.put("room_anchor", roomAnchor);
            object.put("card_type", cardType);
            object.put("card_price", cardPrice);
            object.put("target_user_id", targetId);
            if (isAgreeCp) {
                object.put("cp_order_id", cp_order_id);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
        RetrofitHttpManager.getInstance().httpInterface.buy_couple_card(body).enqueue(new HttpCallback() {
            @Override
            public void OnSucceed(String result) {
                if (!isViewAttached()) {
                    return;
                }
                getView().closeLoading();
                BuyCoupleCardInfo buyCoupleCardInfo = GT.fromJson(result, BuyCoupleCardInfo.class);
                //购买成功 发送邀请
                if (buyCoupleCardInfo != null) {
                    if (buyCoupleCardInfo.getCode() == 0) {
                        getView().buyCpSuccessFetch(targetId, buyCoupleCardInfo);
                    } else {
                        getView().buyCardMsg(buyCoupleCardInfo.getMsg());
                    }
                }
            }

            @Override
            public void OnFailed(int ret, String result) {
            }
        });
    }

    @Override
    public void getCpCardList(boolean isRoom, String targetId, String card_category) {
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("target_user_id", targetId);
            object.put("card_category", card_category);
            object.put("is_room", isRoom);//是否在房间内  true cp房间
            RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.couple_sale_card_list(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    CoupleCardListInfo coupleCardListInfo = GT.fromJson(result, CoupleCardListInfo.class);
                    if (coupleCardListInfo != null && coupleCardListInfo.getCode() == 0) {
                        getView().cpCardListFetch(targetId, coupleCardListInfo);
                    } else if (coupleCardListInfo != null && coupleCardListInfo.getCode() == 17008) {
                        getView().notCoupleMsg();
                    }
                }

                @Override
                public void OnFailed(int ret, String result) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getUserCoin() {
        try {
            JSONObject object = new JSONObject();
            object.put("token", AccountUtil.getInstance().getAccount_token());
            RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.user_rest_coin(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    UserCoinInfo userCoinInfo = GT.fromJson(result, UserCoinInfo.class);
                    if (userCoinInfo != null && userCoinInfo.getCode() == 0) {
                        getView().onUserCoinFetch(userCoinInfo);
                    }
                }

                @Override
                public void OnFailed(int ret, String result) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void userTeamCard(String targetId, String roomId, boolean onlyUseCard, String price) {
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("target_user_id", targetId);
            object.put("room_id", roomId);
            object.put("only_use_card", onlyUseCard);
            object.put("price", price);
            getView().showLoading();

            RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.couple_team_card_user(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    getView().closeLoading();
                    BaseModel baseModel = GT.fromJson(result, BaseModel.class);
                    if (baseModel != null) {
                        if (baseModel.getCode() != 0) {
                            getView().showMessage(baseModel.getMsg());
                        }
                    }
                    getView().userTeamCardFetch(baseModel);
                }

                @Override
                public void OnFailed(int ret, String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    getView().userMicCardFetch(null);
                }
            });
        } catch (Exception e) {

        }
    }

    @Override
    public void userMicCard(String roomId, boolean onlyUseCard, String price) {
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("room_id", roomId);
            object.put("only_use_card", onlyUseCard);
            object.put("mic_card_price", price);
            getView().showLoading();
            RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.couple_room_on_mic_v2(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    getView().closeLoading();
                    BaseModel baseModel = GT.fromJson(result, BaseModel.class);
                    if (baseModel!=null&&baseModel.getCode() == 0) {
                        getView().userMicCardFetch(baseModel);
                    } else {
                        getView().showMessage(baseModel.getMsg());
                    }
                }

                @Override
                public void OnFailed(int ret, String result) {

                }
            });
        } catch (Exception e) {

        }
    }


}
