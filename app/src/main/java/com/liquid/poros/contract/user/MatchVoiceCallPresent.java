package com.liquid.poros.contract.user;


import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import com.liquid.base.tools.GT;
import com.liquid.poros.AvInterface;
import com.liquid.poros.entity.BaseModel;
import com.liquid.poros.entity.MatchVoiceCallInfo;
import com.liquid.poros.service.AvCoreService;
import com.liquid.poros.ui.activity.user.MatchVoiceCallActivity;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.poros.utils.StringUtil;
import com.liquid.poros.utils.network.HttpCallback;
import com.liquid.poros.utils.network.RetrofitHttpManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.RequestBody;

public class MatchVoiceCallPresent extends MatchVoiceCallContract.Presenter {

    private AvInterface mAvManager;
    private String sessionId;
    private String currentTeamImId;
    private String imVideoToken;
    private String avatarUrl;
    private long joinMicTime;
    private MatchVoiceCallActivity mContext;
    private boolean hasLink = false;
    private boolean isBind = false;

    public void init(MatchVoiceCallActivity context) {
        this.mContext = context;
        //启动连麦服务
        isBind = context.bindService(new Intent(context, AvCoreService.class), mConnection, Context.BIND_AUTO_CREATE);
    }

    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mAvManager = AvInterface.Stub.asInterface(service);
            autoJoinRoom();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }
    };

    public void initLink(String sessionId) {
        if (StringUtil.isNotEmpty(sessionId) && !hasLink){
            hasLink = true;
            this.sessionId = sessionId;
            try {
                autoJoinRoom();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 连麦
     */
    private void autoJoinRoom() {
        //开始自动连麦
        try {
            if (mAvManager != null && !mAvManager.isAvRunning() && StringUtil.isNotEmpty(sessionId)) {
                mAvManager.startAudioLink(currentTeamImId,imVideoToken,sessionId, avatarUrl, false);
                mAvManager.setMicrophoneMute(false);
                joinMicTime = System.currentTimeMillis();
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public boolean isAvRunning() {
        boolean isRunning = false;
        try {
            isRunning = mAvManager != null && mAvManager.isAvRunning();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return isRunning;
    }

    public void leaveRoom() {
        hasLink = false;
        try {
            if (mAvManager != null) {
                HashMap<String, String> params = new HashMap<>();
                mAvManager.stopAudioLink();
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void closeConnection() {
        if(isBind){
            mContext.unbindService(mConnection);
            isBind = false;
        }
    }

    @Override
    public void getMatchedInfo(String roomId) {
        getView().showLoading();
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("room_id", roomId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
        RetrofitHttpManager.getInstance().httpInterface.matchRoomInfo(body).enqueue(new HttpCallback() {
            @Override
            public void OnSucceed(String result) {
                if (!isViewAttached()) {
                    return;
                }
                getView().closeLoading();
                MatchVoiceCallInfo info = GT.fromJson(result, MatchVoiceCallInfo.class);
                if (info != null) {
                    if (info.getCode() == 0) {
                        if (info.data != null){
                            currentTeamImId = info.data.im_room_id;
                            imVideoToken = info.data.im_video_token;
                            if (info.data.girl_info != null){
                                avatarUrl = info.data.girl_info.avatar_url;
                            }
                        }
                        getView().onMatchedInfo(info);
                    } else {
                        getView().showMessage(info.getMsg());
                    }
                }

            }

            @Override
            public void OnFailed(int ret, String result) {
                if (!isViewAttached()) {
                    return;
                }
                getView().closeLoading();
            }
        });
    }

    @Override
    public void cancelMatchRoom(boolean on_startup) {
        getView().showLoading();
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("on_startup", on_startup);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
        RetrofitHttpManager.getInstance().httpInterface.cancelMatchRoom(body).enqueue(new HttpCallback() {
            @Override
            public void OnSucceed(String result) {
                if (!isViewAttached()) {
                    return;
                }
                getView().closeLoading();
                getView().onQuitRoom();

            }

            @Override
            public void OnFailed(int ret, String result) {
                if (!isViewAttached()) {
                    return;
                }
                getView().closeLoading();
            }
        });
    }

    @Override
    public void setChannelId(String room_id,String channel_id) {
        getView().showLoading();
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("room_id", room_id);
            object.put("channel_id", channel_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
        RetrofitHttpManager.getInstance().httpInterface.setChannelId(body).enqueue(new HttpCallback() {
            @Override
            public void OnSucceed(String result) {
                if (!isViewAttached()) {
                    return;
                }
                getView().closeLoading();
            }

            @Override
            public void OnFailed(int ret, String result) {
                if (!isViewAttached()) {
                    return;
                }
                getView().closeLoading();
            }
        });
    }

    @Override
    public void matchRoomRefuse(String roomId) {
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("room_id", roomId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
        RetrofitHttpManager.getInstance().httpInterface.matchRoomRefuse(body).enqueue(new HttpCallback() {
            @Override
            public void OnSucceed(String result) {

            }

            @Override
            public void OnFailed(int ret, String result) {

            }
        });
    }

    @Override
    public void hasTeamCard(String roomId,String target_user_id) {
        getView().showLoading();
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("target_user_id", target_user_id);
            object.put("room_id", roomId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
        RetrofitHttpManager.getInstance().httpInterface.hasTeamCard(body).enqueue(new HttpCallback() {
            @Override
            public void OnSucceed(String result) {
                if (!isViewAttached()) {
                    return;
                }
                getView().closeLoading();
                BaseModel info = GT.fromJson(result, BaseModel.class);
                if (info != null) {
                    if (info.getCode() == 0 || info.getCode() == 17024) {
                        getView().onHasTeamCard(info.getCode());
                    } else {
                        getView().showMessage(info.getMsg());
                    }
                }
            }

            @Override
            public void OnFailed(int ret, String result) {
                if (!isViewAttached()) {
                    return;
                }
                getView().closeLoading();
            }
        });
    }
}
