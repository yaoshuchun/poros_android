package com.liquid.poros.contract.contacts;

import com.liquid.base.tools.GT;
import com.liquid.poros.entity.BaseModel;
import com.liquid.poros.entity.ContactsInfo;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.poros.utils.network.HttpCallback;
import com.liquid.poros.utils.network.RetrofitHttpManager;

import org.json.JSONObject;

import okhttp3.RequestBody;

/**
 * Created by sundroid on 29/07/2019.
 */

public class ContactsPresenter extends ContactsContract.Presenter {
    @Override
    public void getFriendList() {
        JSONObject object = new JSONObject();
        getView().showLoading();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.get_friend_list(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    getView().closeLoading();
                    ContactsInfo contactListInfo = GT.fromJson(result, ContactsInfo.class);
                    if (contactListInfo != null && contactListInfo.getCode() == 0) {
                        getView().onContactFetch(contactListInfo);
                    }
                }

                @Override
                public void OnFailed(int ret, String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    getView().closeLoading();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteFriend(String target_user_id) {
        JSONObject object = new JSONObject();
        getView().showLoading();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("target_user_id", target_user_id);
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.remove_friend(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    getView().closeLoading();
                    BaseModel baseModel = GT.fromJson(result, BaseModel.class);
                    if (baseModel!=null){
                        if (baseModel.getCode() == 0) {
                            getView().onDeleteFriendFetch();
                        }
                        getView().showMessage(baseModel.getMsg());
                    }
                }

                @Override
                public void OnFailed(int ret, String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    getView().closeLoading();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
