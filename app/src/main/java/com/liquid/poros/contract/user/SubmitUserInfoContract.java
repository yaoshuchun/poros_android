package com.liquid.poros.contract.user;

import com.liquid.base.mvp.BaseContract;
import com.liquid.poros.base.BPresenter;
import com.liquid.poros.entity.PorosUserInfo;

import org.json.JSONArray;

public interface SubmitUserInfoContract {
    interface View extends BaseContract.BaseView {
        void uploadUserInfoSuccess(PorosUserInfo userInfo);

        void uploadUserInfoFailed();

    }

    abstract class Presenter extends BPresenter<SubmitUserInfoContract.View> {

        /**
         * 更新用户信息
         */
        public abstract void updateUserInfo(int gender, String age, JSONArray games);
    }
}
