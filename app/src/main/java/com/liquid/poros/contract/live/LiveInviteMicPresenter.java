package com.liquid.poros.contract.live;

import android.util.Log;

import com.liquid.base.tools.GT;
import com.liquid.poros.entity.BaseModel;
import com.liquid.poros.entity.UserCoinInfo;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.poros.utils.network.HttpCallback;
import com.liquid.poros.utils.network.RetrofitHttpManager;

import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;

public class LiveInviteMicPresenter extends LiveInviteMicContract.Presenter {

    @Override
    public void getUserCoin() {
        try {
            JSONObject object = new JSONObject();
            object.put("token", AccountUtil.getInstance().getAccount_token());
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.user_rest_coin(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    Log.e("zzz", "获取个人金币 == " + result);
                    UserCoinInfo userCoinInfo = GT.fromJson(result, UserCoinInfo.class);
                    if (userCoinInfo != null && userCoinInfo.getCode() == 0) {
                        getView().onUserCoinSuccessFetch(userCoinInfo);
                    }
                }

                @Override
                public void OnFailed(int ret, String result) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void userMicCard(String roomId, boolean onlyUseCard, String price) {
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("room_id", roomId);
            object.put("only_use_card", onlyUseCard);
            object.put("mic_card_price", price);
            getView().showLoading();
            RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.couple_room_on_mic_v2(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    getView().closeLoading();
                    BaseModel baseModel = GT.fromJson(result, BaseModel.class);
                    if (baseModel != null) {
                        if (baseModel.getCode() == 0) {
                            getView().userMicCardFetch(baseModel, onlyUseCard);
                        } else {
                            getView().showMessage(baseModel.getMsg());
                        }
                    }
                }

                @Override
                public void OnFailed(int ret, String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    getView().closeLoading();
                }
            });
        } catch (Exception e) {

        }
    }
}
