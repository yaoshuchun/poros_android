package com.liquid.poros.contract.info;

import com.liquid.base.mvp.BaseContract;
import com.liquid.poros.base.BPresenter;

public interface GameInfoContract {
    interface View extends BaseContract.BaseView {
        void onAccountInfoFetched();
        void onAccountInfoFailFetched();
    }

    abstract class Presenter extends BPresenter<View> {

        /**
         * 设置游戏信息
         *
         * @param nickName 昵称
         * @param gameZone 游戏分区
         */
        public abstract void setAccountInfo(String nickName, String gameZone);
    }
}
