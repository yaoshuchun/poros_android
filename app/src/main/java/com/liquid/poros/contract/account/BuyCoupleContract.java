package com.liquid.poros.contract.account;

import com.liquid.base.mvp.BaseContract;
import com.liquid.poros.base.BPresenter;
import com.liquid.poros.entity.BaseModel;
import com.liquid.poros.entity.BuyCoupleCardInfo;
import com.liquid.poros.entity.CoupleCardListInfo;
import com.liquid.poros.entity.UserCoinInfo;

/**
 * Created by sundroid on 19/03/2019.
 */

public interface BuyCoupleContract {
    interface View extends BaseContract.BaseView {
        void cpCardListFetch(String targetId, CoupleCardListInfo coupleCardListInfo);

        void buyCpSuccessFetch(String targetId, BuyCoupleCardInfo buyCoupleCardInfo);

        void onUserCoinFetch(UserCoinInfo userCoinInfo);

        void buyCardMsg(String msg);

        void notCoupleMsg();

        void userTeamCardFetch(BaseModel baseModel);

        void userMicCardFetch(BaseModel baseModel);

    }

    abstract class Presenter extends BPresenter<View> {
        /**
         * 购买CP卡/邀请组CP/同意组CP
         * isAgreeCp              N  //是否是邀请
         * card_type              Y  //cp卡类型
         * card_price             Y  //cp卡价格
         * room_id                N  //房间ID
         * room_anchor            N  //房间主持人ID
         * target_user_id         Y  //目标用户ID
         * cp_order_id            N  //同意组cp传递 邀请id
         */
        public abstract void buyCouple(boolean isAgreeCp, String roomId, String roomAnchor, int cardType, String cardPrice, String targetId, String cp_order_id);

        /**
         * 获取可购买cp卡列表
         *
         * @param isRoom        是否在cp直播房间  true cp直播房间
         * @param targetId      目标用户ID
         * @param card_category 卡类型  1.TEAM 开黑卡 2.MIC 上麦卡 3.CP仅CP卡 4.TEAM+CP 开黑和CP卡
         */
        public abstract void getCpCardList(boolean isRoom, String targetId, String card_category);

        /**
         * 获取用户金币
         */
        public abstract void getUserCoin();

        /**
         * 使用/购买开黑卡
         * target_user_id         目标用户ID
         * room_id                CP直播间id
         * only_use_card          true是仅使用开黑卡 false为购买并使用开黑卡
         * price                  购买开黑卡花费金币
         */
        public abstract void userTeamCard(String targetId, String roomId, boolean onlyUseCard, String price);

        /**
         * 使用/购买上麦卡
         * target_user_id         目标用户ID
         * only_use_card          true是仅使用上麦卡 false为购买并使用上麦卡
         * price                  购买上麦卡花费金币
         */
        public abstract void userMicCard(String roomId, boolean onlyUseCard, String price);
    }
}
