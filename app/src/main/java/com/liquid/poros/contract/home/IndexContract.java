package com.liquid.poros.contract.home;

import com.liquid.base.mvp.BaseContract;
import com.liquid.poros.base.BPresenter;
import com.liquid.poros.entity.CpListInfo;
import com.liquid.poros.entity.IndexCpInviteInfo;
import com.liquid.poros.entity.RoomDetailsInfo;

/**
 * Created by sundroid on 19/03/2019.
 */

public interface IndexContract {
    interface View extends BaseContract.BaseView {
        void onCPListFetched(CpListInfo hotGroupListInfo);

        void onCPInviteFetched(IndexCpInviteInfo indexCpInviteInfo);

        void onRoomDetailsFetched(RoomDetailsInfo roomDetailsInfo);
    }

    abstract class Presenter extends BPresenter<View> {
        /**
         * 获取CP用户列表
         */
        public abstract void fetchFetchCPList(int page);

        /**
         * 广场弹窗邀请
         */
        public abstract void fetchIndexCpInvite();

        /**
         * 足迹触发
         */
        public abstract void fetchDispatchFemale(int dispatchType, String femaleUserId);

        /**
         * 获取房间类型
         *
         * @param roomId 房间id
         */
        public abstract void fetchRoomDetails(String roomId);
    }
}
