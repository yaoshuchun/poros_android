package com.liquid.poros.contract.feed;

import com.liquid.base.tools.GT;
import com.liquid.poros.entity.BaseModel;
import com.liquid.poros.entity.FemaleGuestCommentData;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.poros.utils.network.HttpCallback;
import com.liquid.poros.utils.network.RetrofitHttpManager;

import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;

public class FeedPresenter extends FeedContract.Presenter {
    @Override
    public void fetchFeedList(int page) {
        RetrofitHttpManager.getInstance().httpInterface.feedSquareList(AccountUtil.getInstance().getAccount_token(),page).enqueue(new HttpCallback() {
            @Override
            public void OnSucceed(String result) {
                if (!isViewAttached()) {
                    return;
                }
                getView().closeLoading();
                FemaleGuestCommentData femaleGuestCommentData = GT.fromJson(result, FemaleGuestCommentData.class);
                if (femaleGuestCommentData != null && femaleGuestCommentData.getCode() == 0) {
                    getView().onFeedListFetched(femaleGuestCommentData.data);
                }
            }

            @Override
            public void OnFailed(int ret, String result) {
                if (!isViewAttached()) {
                    return;
                }
                getView().closeLoading();
            }
        });
    }
    @Override
    public void feedLike(String feed_id, int is_like,String source) {
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("feed_id", feed_id);
            object.put("is_like", is_like);
            object.put("source",source);
            RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.feed_like(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    BaseModel baseModel = GT.fromJson(result, BaseModel.class);
                    if (baseModel!=null){
                        if (baseModel.getCode() == 0) {
                            getView().updateLikeResult();
                        } else {
                            getView().showMessage(baseModel.getMsg());
                        }
                    }
                }

                @Override
                public void OnFailed(int ret, String result) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void feedActionReport(String feed_id, int action, String source) {
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("feed_id", feed_id);
            object.put("action", action);
            object.put("source",source);
            RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.feedActionReport(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {

                }

                @Override
                public void OnFailed(int ret, String result) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
