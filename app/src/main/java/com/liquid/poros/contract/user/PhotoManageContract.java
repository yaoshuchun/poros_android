package com.liquid.poros.contract.user;

import com.liquid.base.mvp.BaseContract;
import com.liquid.poros.base.BPresenter;
import com.liquid.poros.entity.UserPhotoInfo;

import java.util.List;

public interface PhotoManageContract {
    interface View extends BaseContract.BaseView {

        void uploadImageOssSuccess(String path);

        void onUserPhotosFetch(UserPhotoInfo userPhotoInfo);

        void onDeletePhotosFetch();

    }

    abstract class Presenter extends BPresenter<PhotoManageContract.View> {
        /**
         * 上传
         *
         * @param file_dir 上传路径区分 user_cover_img 个人中心背景图，user_photo 个人相册
         */
        public abstract void uploadImageOss(List<String> path, String file_dir);

        /**
         * 获取用户相册
         *
         * @param target_user_id 用户id
         */
        public abstract void getUserPhotos(String target_user_id);

        /**
         * 删除相册
         *
         * @param photoId 相册id
         */
        public abstract void deleteUserPhoto(String photoId);
    }
}
