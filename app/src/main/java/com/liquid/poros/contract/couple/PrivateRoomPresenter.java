package com.liquid.poros.contract.couple;

import com.liquid.base.tools.GT;
import com.liquid.poros.entity.PrivateRoomListInfo;
import com.liquid.poros.entity.RoomDetailsInfo;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.poros.utils.network.HttpCallback;
import com.liquid.poros.utils.network.RetrofitHttpManager;

import org.json.JSONObject;

import okhttp3.RequestBody;

public class PrivateRoomPresenter extends PrivateRoomContract.Presenter {

    @Override
    public void fetchPrivateRoom(int page) {
        try {
            JSONObject object = new JSONObject();
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("page", page);
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.couple_private_room_list(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    PrivateRoomListInfo privateRoomListInfo = GT.fromJson(result, PrivateRoomListInfo.class);
                    if (privateRoomListInfo != null && privateRoomListInfo.getCode() == 0) {
                        getView().onPrivateRoomListFetched(privateRoomListInfo);
                    }

                }

                @Override
                public void OnFailed(int ret, String result) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void fetchRoomDetails(String roomId) {
        try {
            JSONObject object = new JSONObject();
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("room_id", roomId);
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.couple_video_room_details(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    RoomDetailsInfo roomDetailsInfo = GT.fromJson(result, RoomDetailsInfo.class);
                    if (roomDetailsInfo != null && roomDetailsInfo.getCode() == 0) {
                        getView().onRoomDetailsFetched(roomDetailsInfo);
                    }
                }

                @Override
                public void OnFailed(int ret, String result) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
