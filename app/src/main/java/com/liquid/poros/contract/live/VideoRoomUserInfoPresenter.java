package com.liquid.poros.contract.live;

import com.liquid.base.tools.GT;
import com.liquid.poros.entity.VideoRoomUserInfo;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.poros.utils.network.HttpCallback;
import com.liquid.poros.utils.network.RetrofitHttpManager;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;

public class VideoRoomUserInfoPresenter extends VideoRoomUserInfoContract.Presenter {
    @Override
    public void getUserInfo(String target_user_id) {
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("target_user_id", target_user_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
        RetrofitHttpManager.getInstance().httpInterface.video_room_user_info(body).enqueue(new HttpCallback() {
            @Override
            public void OnSucceed(String result) {
                if (!isViewAttached()) {
                    return;
                }
                VideoRoomUserInfo baseModel = GT.fromJson(result, VideoRoomUserInfo.class);
                if (baseModel!=null){
                    if (baseModel.getCode() == 0) {
                        getView().onUserInfoFetch(baseModel);
                    } else {
                        getView().showMessage(baseModel.getMsg());
                    }
                }
            }

            @Override
            public void OnFailed(int ret, String result) {
            }
        });
    }

}
