package com.liquid.poros.contract.report;

import com.liquid.base.mvp.BaseContract;
import com.liquid.poros.base.BPresenter;

/**
 * Created by sundroid on 25/03/2019.
 */

public interface ReportConstract {
    interface View extends BaseContract.BaseView {
        void onReportSucceed();
    }

    abstract class Presenter extends BPresenter<View> {

        /**
         * 房间举报
         *
         * @param room_id        房间id
         * @param reason         举报原因
         * @param target_user_id 目标对象
         */
        public abstract void reportChatRoom(String room_id, String reason, String target_user_id);

        /**
         * 举报用户
         *
         * @param reason         举报原因
         * @param target_user_id 目标对象
         */
        public abstract void reportUser(String reason, String target_user_id);

    }
}
