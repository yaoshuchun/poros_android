package com.liquid.poros.contract.account;

import com.liquid.base.mvp.BaseContract;
import com.liquid.poros.base.BPresenter;
import com.liquid.poros.entity.AlipayInfo;
import com.liquid.poros.entity.RechargeInfo;
import com.liquid.poros.entity.RechargeResultInfo;
import com.liquid.poros.entity.UserCoinInfo;
import com.liquid.poros.entity.WeChatPayInfo;

/**
 * Created by sundroid on 29/07/2019.
 */

public interface RechargeContract {
    interface View extends BaseContract.BaseView {
        void onRechargeInfoFetch(RechargeInfo rechargeInfo);

        void onWechatSuccessFetch(WeChatPayInfo weChatPayInfo);

        void onAlipaySuccessFetch(AlipayInfo alipayInfo);

        void onRechargeResultFetch(RechargeResultInfo rechargeResultInfo);

        void onUserCoinSuccessFetch(UserCoinInfo userCoinInfo);

        void onCloseRechargeOrderFetch();
    }

    abstract class Presenter extends BPresenter<View> {
        /**
         * 获取充值页信息
         */
        public abstract void rechargeInfo(String roomId);

        /**
         * 微信充值
         *
         * @param amount        充值金额
         * @param recharge_type 充值类型
         */
        public abstract void wechatOrder(int amount, String recharge_type);

        /**
         * 支付宝充值
         *
         * @param amount        充值金额
         * @param recharge_type 充值类型
         */
        public abstract void alipayOrder(int amount, String recharge_type);

        /**
         * 充值结果查询
         *
         * @param order_id 订单ID
         */
        public abstract void rechargeResult(String order_id);

        /**
         * 获取用户余额
         *
         * @param
         */
        public abstract void getUserCoin();

        /**
         * 取消充值订单
         *
         * @param orderId 订单ID
         */
        public abstract void closeRechargeOrder(String orderId);

        /**
         * 小额礼包支付下单
         *
         * @param pay_amount 金额
         * @param pay_way 方式
         */
        public abstract void smallPacketPay(int pay_amount,int pay_way);

        /**
         * 1分钱付款
         *
         * @param pay_amount 金额
         * @param pay_way 方式
         */
        public abstract void payOneCent(int pay_amount,int pay_way);
    }
}
