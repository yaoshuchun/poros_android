package com.liquid.poros.contract.user;

import android.util.Log;

import com.google.gson.Gson;
import com.liquid.base.tools.GT;
import com.liquid.poros.entity.NormalGame;
import com.liquid.poros.entity.PorosUserInfo;
import com.liquid.poros.entity.UserCenterInfo;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.base.utils.LogOut;
import com.liquid.poros.utils.network.HttpCallback;
import com.liquid.poros.utils.network.RetrofitHttpManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class UpdateUserInfoPresenter extends UpdateUserInfoContract.Presenter {
    @Override
    public void uploadImageOss(String path, String file_dir) {
        try {
            MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);//表单类型
            File file = new File(path);//filePath 图片地址
            RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            builder.addFormDataPart("image_file", file.getName(), requestBody);
            builder.addFormDataPart("file_dir", file_dir);
            builder.addFormDataPart("token", AccountUtil.getInstance().getAccount_token());
            MultipartBody body = builder.build();
            RetrofitHttpManager.getInstance().httpInterface.upload_image_oss(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    Log.d("zzz", "上传成功" + result);
                    try {
                        JSONObject object = new JSONObject(result);
                        int code = object.getInt("code");
                        String message = object.getString("msg");
                        JSONObject data = object.getJSONObject("data");
                        String videoPath = data.getString("image_url");
                        if (code == 0) {
                            getView().uploadImageOssSuccess(videoPath);
                            getView().showMessage("上传成功");
                        }
                    } catch (Exception e) {

                    }
                }

                @Override
                public void OnFailed(int ret, String result) {
                    getView().closeLoading();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateUserInfo(boolean isMatchControl,String nickName, String avatarUrl, String gender, String age, String couple_desc, JSONArray games, UserCenterInfo.Data.UserInfo.GameInfoBean gameInfoBean) {
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("nick_name", nickName);
            object.put("avatar_url", avatarUrl);
            object.put("gender", gender);
            object.put("age", age);
            object.put("couple_desc", couple_desc);
            if (isMatchControl) {
                object.put("games", games);
            } else {
                if (gameInfoBean != null) {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("hpjy", new JSONObject(new Gson().toJson(gameInfoBean.getHpjy())));
                    jsonObject.put("wzry", new JSONObject(new Gson().toJson(gameInfoBean.getWzry())));
                    JSONArray jsonArray = new JSONArray();
                    for (NormalGame normalGame : gameInfoBean.getOther()) {
                        jsonArray.put(normalGame.get_id());
                    }
                    jsonObject.put("other", jsonArray);
                    object.put("game_info", jsonObject);
                }
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
        getView().showLoading();
        RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
        RetrofitHttpManager.getInstance().httpInterface.user_update_info(body).enqueue(new HttpCallback() {
            @Override
            public void OnSucceed(String result) {
                LogOut.debug("Update UserInfo" + result);
                if (!isViewAttached()) {
                    return;
                }
                getView().closeLoading();
                PorosUserInfo porosUserInfo = GT.fromJson(result, PorosUserInfo.class);
                if (AccountUtil.handleUserInfo(result)) {
                    getView().uploadUserInfoSuccess(porosUserInfo);
                }
            }

            @Override
            public void OnFailed(int ret, String result) {
                if (!isViewAttached()) {
                    return;
                }
                getView().closeLoading();
            }
        });
    }
}
