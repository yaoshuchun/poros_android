package com.liquid.poros.contract.feed;

import com.liquid.base.mvp.BaseContract;
import com.liquid.poros.base.BPresenter;
import com.liquid.poros.entity.FemaleGuestCommentBean;

public interface FeedContract {
    interface View extends BaseContract.BaseView {
        void onFeedListFetched(FemaleGuestCommentBean femaleGuestCommentBean);
        void updateLikeResult();
    }

    abstract class Presenter extends BPresenter<View> {
        /**
         * 获取动态列表
         */
        public abstract void fetchFeedList(int page);

        /**
         * 点赞
         * @param feed_id
         * @param is_like
         * @param source
         */
        public abstract void feedLike(String feed_id,int is_like,String source);
        /**
         * 动态操作上报
         * @param feed_id
         * @param action
         * @param source
         */
        public abstract void feedActionReport(String feed_id,int action,String source);
    }
}
