package com.liquid.poros.contract.account;

import com.liquid.base.mvp.BaseContract;
import com.liquid.poros.base.BPresenter;
import com.liquid.poros.entity.RechargeRecordInfo;

/**
 * Created by sundroid on 29/07/2019.
 */

public interface RechargeDetailsContract {
    interface View extends BaseContract.BaseView {
        void rechargeRecordFetch(RechargeRecordInfo rechargeRecordInfo);
    }

    abstract class Presenter extends BPresenter<View> {
        /**
         * 充值账单记录
         *
         * @param page
         */
        public abstract void getRechargeRecord(int page);
    }
}
