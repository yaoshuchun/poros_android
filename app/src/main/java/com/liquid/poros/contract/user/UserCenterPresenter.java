package com.liquid.poros.contract.user;

import com.liquid.base.tools.GT;
import com.liquid.poros.entity.BaseModel;
import com.liquid.poros.entity.EditGameBean;
import com.liquid.poros.entity.FemaleGuestCommentData;
import com.liquid.poros.entity.NormalGame;
import com.liquid.poros.entity.RoomDetailsInfo;
import com.liquid.poros.entity.SettingEditGameBean;
import com.liquid.poros.entity.UserCenterInfo;
import com.liquid.poros.entity.UserPhotoInfo;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.base.utils.LogOut;
import com.liquid.poros.utils.network.HttpCallback;
import com.liquid.poros.utils.network.RetrofitHttpManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class UserCenterPresenter extends UserCenterContract.Presenter {
    @Override
    public void uploadImageOss(List<String> path, String file_dir) {
        getView().showLoading();
        getView().showMessage("正在上传...");
        try {
            MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);//表单类型
            for (int i = 0; i < path.size(); i++) {
                File file = new File(path.get(i));//filePath 图片地址
                RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                builder.addFormDataPart("image_file", file.getName(), requestBody);
            }
            builder.addFormDataPart("file_dir", file_dir);
            builder.addFormDataPart("token", AccountUtil.getInstance().getAccount_token());
            MultipartBody body = builder.build();
            RetrofitHttpManager.getInstance().httpInterface.upload_image_oss(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    getView().closeLoading();
                    LogOut.e("zzzz", "上传图片" + result);
                    try {
                        JSONObject object = new JSONObject(result);
                        int code = object.getInt("code");
                        String message = object.getString("msg");
                        JSONObject data = object.getJSONObject("data");
                        String videoPath = data.getString("image_url");
                        if (code == 0) {
                            getView().uploadImageOssSuccess(videoPath);
                            getView().showMessage("上传成功");
                        } else {
                            getView().showMessage("上传失败，请重新上传");
                        }
                    } catch (Exception e) {

                    }
                }

                @Override
                public void OnFailed(int ret, String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    getView().closeLoading();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getUserInfo(String target_user_id) {
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("target_user_id", target_user_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
        RetrofitHttpManager.getInstance().httpInterface.user_info_other(body).enqueue(new HttpCallback() {
            @Override
            public void OnSucceed(String result) {
                if (!isViewAttached()) {
                    return;
                }
                UserCenterInfo baseModel = GT.fromJson(result, UserCenterInfo.class);
                if (baseModel == null) {
                    return;
                }
                if (baseModel.getCode() == 0) {
                    try {
                        //王者荣耀
                        ArrayList<Map<String, String>> wzry_platform_list = new ArrayList<>();
                        ArrayList<Map<String, String>> wzry_rank_list = new ArrayList<>();
                        ArrayList<Map<String, String>> wzry_area_list = new ArrayList<>();

                        JSONObject resultObj = new JSONObject(result);
                        JSONObject dataObj = resultObj.optJSONObject("data");
                        if (dataObj != null) {
                            JSONObject user_infoObj = dataObj.optJSONObject("user_info");
                            JSONObject game_info_static = user_infoObj.optJSONObject("game_info_static");

                            SettingEditGameBean staticGameBean = new SettingEditGameBean();

                            //game_info_static
                            JSONObject wzryObj = game_info_static.optJSONObject("wzry");

                            SettingEditGameBean.SettingGameBean honor_of_kings = new SettingEditGameBean.SettingGameBean();
                            honor_of_kings.platform = new EditGameBean();
                            honor_of_kings.platform.des = "游戏平台";

                            JSONObject platformObj = wzryObj.optJSONObject("platform");
                            Iterator<String> iterator = platformObj.keys();
                            while (iterator.hasNext()) {
                                Map<String, String> map2 = new HashMap<String, String>();
                                String key = iterator.next();
                                String value = platformObj.getString(key);
                                map2.put(key, value);
                                wzry_platform_list.add(map2);
                                EditGameBean.Item item = new EditGameBean.Item();
                                item.id = key;
                                item.name = value;
                                honor_of_kings.platform.items.add(item);
                            }

                            JSONObject rankObj = wzryObj.optJSONObject("rank");
                            honor_of_kings.rank = new EditGameBean();
                            honor_of_kings.rank.des = "段位";
                            Iterator<String> rankIterator = rankObj.keys();
                            while (rankIterator.hasNext()) {
                                Map<String, String> map2 = new HashMap<String, String>();
                                String key = rankIterator.next();
                                String value = rankObj.getString(key);
                                map2.put(key, value);
                                wzry_rank_list.add(map2);
                                EditGameBean.Item item = new EditGameBean.Item();
                                item.id = key;
                                item.name = value;
                                honor_of_kings.rank.items.add(item);
                            }

                            JSONObject zoneObj = wzryObj.optJSONObject("zone");
                            honor_of_kings.zone = new EditGameBean();
                            honor_of_kings.zone.des = "游戏大区";
                            Iterator<String> zoneIterator = zoneObj.keys();
                            while (zoneIterator.hasNext()) {
                                Map<String, String> map2 = new HashMap<String, String>();
                                String key = zoneIterator.next();
                                String value = zoneObj.getString(key);
                                map2.put(key, value);
                                wzry_area_list.add(map2);
                                EditGameBean.Item item = new EditGameBean.Item();
                                item.id = key;
                                item.name = value;
                                honor_of_kings.zone.items.add(item);
                            }
                            String wzryIconString = wzryObj.optString("icon");
                            honor_of_kings.icon = wzryIconString;
                            staticGameBean.honor_of_kings = honor_of_kings;

                            UserCenterInfo.WZRYDATA wzrydata = new UserCenterInfo.WZRYDATA();
                            wzrydata.setWzry_platform_list(wzry_platform_list);
                            wzrydata.setWzry_area_list(wzry_area_list);
                            wzrydata.setWzry_rank_list(wzry_rank_list);
                            wzrydata.setIcon(wzryIconString);

                            baseModel.setWzrydata(wzrydata);

                            //和平精英
                            ArrayList<Map<String, String>> hpjy_platform_list = new ArrayList<>();
                            ArrayList<Map<String, String>> hpjy_rank_list = new ArrayList<>();
                            ArrayList<Map<String, String>> hpjy_area_list = new ArrayList<>();
                            JSONObject hpjyObj = game_info_static.optJSONObject("hpjy");

                            SettingEditGameBean.SettingGameBean peace_elite = new SettingEditGameBean.SettingGameBean();
                            peace_elite.platform = new EditGameBean();
                            peace_elite.platform.des = "游戏平台";

                            JSONObject hpjyplatformObj = hpjyObj.optJSONObject("platform");
                            Iterator<String> hpjyiterator = hpjyplatformObj.keys();
                            while (hpjyiterator.hasNext()) {
                                Map<String, String> map2 = new HashMap<String, String>();
                                String key = hpjyiterator.next();
                                String value = hpjyplatformObj.getString(key);
                                map2.put(key, value);
                                hpjy_platform_list.add(map2);
                                EditGameBean.Item item = new EditGameBean.Item();
                                item.id = key;
                                item.name = value;
                                peace_elite.platform.items.add(item);
                            }

                            JSONObject hpjyrankObj = hpjyObj.optJSONObject("rank");
                            peace_elite.rank = new EditGameBean();
                            peace_elite.rank.des = "段位";

                            Iterator<String> hpjyrankIterator = hpjyrankObj.keys();
                            while (hpjyrankIterator.hasNext()) {
                                Map<String, String> map2 = new HashMap<String, String>();
                                String key = hpjyrankIterator.next();
                                String value = hpjyrankObj.getString(key);
                                map2.put(key, value);
                                hpjy_rank_list.add(map2);
                                EditGameBean.Item item = new EditGameBean.Item();
                                item.id = key;
                                item.name = value;
                                peace_elite.rank.items.add(item);
                            }

                            JSONObject hpjyzoneObj = hpjyObj.optJSONObject("zone");
                            peace_elite.zone = new EditGameBean();
                            peace_elite.zone.des = "游戏大区";
                            Iterator<String> hpjyzoneIterator = hpjyzoneObj.keys();
                            while (hpjyzoneIterator.hasNext()) {
                                Map<String, String> map2 = new HashMap<String, String>();
                                String key = hpjyzoneIterator.next();
                                String value = hpjyzoneObj.getString(key);
                                map2.put(key, value);
                                hpjy_area_list.add(map2);
                                EditGameBean.Item item = new EditGameBean.Item();
                                item.id = key;
                                item.name = value;
                                peace_elite.zone.items.add(item);
                            }
                            String hpjyIconString = hpjyObj.optString("icon");
                            peace_elite.icon = hpjyIconString;
                            staticGameBean.peace_elite = peace_elite;

                            SettingEditGameBean.SettingOtherGameBean others = new SettingEditGameBean.SettingOtherGameBean();
                            List<NormalGame> other = baseModel.getData().getUser_info().getGame_info_static().getOther();
                            for (NormalGame normalGame : other) {
                                EditGameBean.Item item = new EditGameBean.Item();
                                item.id = normalGame.get_id() + "";
                                item.name = normalGame.getName();
                                item.image_url = normalGame.getIcon();
                                others.list.add(item);
                            }
                            others.icon = game_info_static.optString("other_game_icon");
                            staticGameBean.other = others;
                            baseModel.setStaticGameBean(staticGameBean);

                            UserCenterInfo.HPJYDATA hpjydata = new UserCenterInfo.HPJYDATA();
                            hpjydata.setHpjy_platform_list(hpjy_platform_list);
                            hpjydata.setHpjy_area_list(hpjy_area_list);
                            hpjydata.setHpjy_rank_list(hpjy_rank_list);
                            hpjydata.setIcon(hpjyIconString);

                            baseModel.setHpjydata(hpjydata);

                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    getView().onUserInfoFetch(baseModel);
                } else {
                    getView().showMessage(baseModel.getMsg());
                }
            }

            @Override
            public void OnFailed(int ret, String result) {
            }
        });
    }

    @Override
    public void getUserPhotos(int page, String target_user_id) {
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("target_user_id", target_user_id);
            object.put("page", page);
            object.put("page_size", 20);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
        RetrofitHttpManager.getInstance().httpInterface.user_photos(body).enqueue(new HttpCallback() {
            @Override
            public void OnSucceed(String result) {
                if (!isViewAttached()) {
                    return;
                }
                UserPhotoInfo baseModel = GT.fromJson(result, UserPhotoInfo.class);
                if (baseModel != null) {
                    if (baseModel.getCode() == 0) {
                        getView().onUserPhotosFetch(baseModel);
                    } else {
                        getView().showMessage(baseModel.getMsg());
                    }
                }
            }

            @Override
            public void OnFailed(int ret, String result) {
            }
        });
    }

    @Override
    public void getCommentList(String female_guest_id, String last_id) {
        RetrofitHttpManager.getInstance().httpInterface.get_moment_list(AccountUtil.getInstance().getAccount_token(), female_guest_id, last_id).enqueue(new HttpCallback() {
            @Override
            public void OnSucceed(String result) {
                if (!isViewAttached()) {
                    return;
                }
                FemaleGuestCommentData commentData
                        = GT.fromJson(result, FemaleGuestCommentData.class);
                if (commentData != null && commentData.getCode() == 0) {
                    getView().updateCommentList(commentData.data);
                } else {

                }
            }

            @Override
            public void OnFailed(int ret, String result) {

            }
        });
    }

    @Override
    public void deleteUserPhoto(String photoId) {
        getView().showLoading();
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("photo_id", photoId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
        RetrofitHttpManager.getInstance().httpInterface.user_photo_delete(body).enqueue(new HttpCallback() {
            @Override
            public void OnSucceed(String result) {
                if (!isViewAttached()) {
                    return;
                }
                getView().closeLoading();
                UserPhotoInfo baseModel = GT.fromJson(result, UserPhotoInfo.class);
                if (baseModel != null) {
                    if (baseModel.getCode() == 0) {
                        getView().onDeletePhotosFetch();
                    } else {
                        getView().showMessage(baseModel.getMsg());
                    }
                }

            }

            @Override
            public void OnFailed(int ret, String result) {
                if (!isViewAttached()) {
                    return;
                }
                getView().closeLoading();
            }
        });
    }

    @Override
    public void unblock_user(String target_user_id) {
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("target_user_id", target_user_id);

            RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.unblock_user(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    BaseModel baseModel = GT.fromJson(result, BaseModel.class);
                    if (baseModel != null) {
                        if (baseModel.getCode() == 0) {
                            getView().blockUserSuccessFetch(baseModel);
                        } else {
                            getView().showMessage(baseModel.getMsg());
                        }
                    }
                }

                @Override
                public void OnFailed(int ret, String result) {

                }
            });
        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    @Override
    public void block_user(String target_user_id) {
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("target_user_id", target_user_id);

            RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.block_user(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    BaseModel baseModel = GT.fromJson(result, BaseModel.class);
                    if (baseModel != null) {
                        if (baseModel.getCode() == 0) {
                            getView().blockUserSuccessFetch(baseModel);
                        } else {
                            getView().showMessage(baseModel.getMsg());
                        }
                    }

                }

                @Override
                public void OnFailed(int ret, String result) {

                }
            });
        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    @Override
    public void feedLike(String feed_id, int is_like, String source) {
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("feed_id", feed_id);
            object.put("is_like", is_like);
            object.put("source", source);
            RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.feed_like(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    BaseModel baseModel = GT.fromJson(result, BaseModel.class);
                    if (baseModel != null) {
                        if (baseModel.getCode() == 0) {
                            getView().updateLikeResult();
                        } else {
                            getView().showMessage(baseModel.getMsg());
                        }
                    }

                }

                @Override
                public void OnFailed(int ret, String result) {

                }
            });
        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    @Override
    public void fetchRoomDetails(String roomId) {
        try {
            JSONObject object = new JSONObject();
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("room_id", roomId);
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.couple_video_room_details(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    RoomDetailsInfo roomDetailsInfo = GT.fromJson(result, RoomDetailsInfo.class);
                    if (roomDetailsInfo != null && roomDetailsInfo.getCode() == 0) {
                        getView().onRoomDetailsFetched(roomDetailsInfo);
                    }
                }

                @Override
                public void OnFailed(int ret, String result) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void feedActionReport(String feed_id, int action, String source) {
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("feed_id", feed_id);
            object.put("action", action);
            object.put("source", source);
            RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.feedActionReport(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {

                }

                @Override
                public void OnFailed(int ret, String result) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
