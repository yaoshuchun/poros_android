package com.liquid.poros.contract.user;

import com.liquid.base.tools.GT;
import com.liquid.poros.entity.UserPhotoInfo;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.base.utils.LogOut;
import com.liquid.poros.utils.network.HttpCallback;
import com.liquid.poros.utils.network.RetrofitHttpManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class PhotoManagePresenter extends PhotoManageContract.Presenter {
    @Override
    public void uploadImageOss(List<String> path, String file_dir) {
        getView().showLoading();
        getView().showMessage("正在上传...");
        try {
            MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);//表单类型
            for (int i = 0; i < path.size(); i++) {
                File file = new File(path.get(i));//filePath 图片地址
                RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                builder.addFormDataPart("image_file", file.getName(), requestBody);
            }
            builder.addFormDataPart("file_dir", file_dir);
            builder.addFormDataPart("token", AccountUtil.getInstance().getAccount_token());
            MultipartBody body = builder.build();
            RetrofitHttpManager.getInstance().httpInterface.upload_image_oss(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    getView().closeLoading();
                    LogOut.e("zzzz", "上传图片" + result);
                    try {
                        JSONObject object = new JSONObject(result);
                        int code = object.getInt("code");
                        String message = object.getString("msg");
                        JSONObject data = object.getJSONObject("data");
                        String videoPath = data.getString("image_url");
                        if (code == 0) {
                            getView().uploadImageOssSuccess(videoPath);
                            getView().showMessage("上传成功");
                        } else {
                            getView().showMessage("上传失败，请重新上传");
                        }
                    } catch (Exception e) {

                    }
                }

                @Override
                public void OnFailed(int ret, String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    getView().closeLoading();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getUserPhotos(String target_user_id) {
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("target_user_id", target_user_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
        RetrofitHttpManager.getInstance().httpInterface.user_photos(body).enqueue(new HttpCallback() {
            @Override
            public void OnSucceed(String result) {
                if (!isViewAttached()) {
                    return;
                }
                UserPhotoInfo baseModel = GT.fromJson(result, UserPhotoInfo.class);
                if (baseModel != null) {
                    if (baseModel.getCode() == 0) {
                        getView().onUserPhotosFetch(baseModel);
                    } else {
                        getView().showMessage(baseModel.getMsg());
                    }
                }
            }

            @Override
            public void OnFailed(int ret, String result) {
            }
        });
    }

    @Override
    public void deleteUserPhoto(String photoId) {
        getView().showLoading();
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("photo_id", photoId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
        RetrofitHttpManager.getInstance().httpInterface.user_photo_delete(body).enqueue(new HttpCallback() {
            @Override
            public void OnSucceed(String result) {
                if (!isViewAttached()) {
                    return;
                }
                getView().closeLoading();
                UserPhotoInfo baseModel = GT.fromJson(result, UserPhotoInfo.class);
                if (baseModel != null) {
                    if (baseModel.getCode() == 0) {
                        getView().onDeletePhotosFetch();
                    } else {
                        getView().showMessage(baseModel.getMsg());
                    }
                }
            }

            @Override
            public void OnFailed(int ret, String result) {
                if (!isViewAttached()) {
                    return;
                }
                getView().closeLoading();
            }
        });
    }

}
