package com.liquid.poros.contract.mine;

import com.liquid.base.mvp.BaseContract;
import com.liquid.poros.base.BPresenter;
import com.liquid.poros.entity.PorosUserInfo;
import com.liquid.poros.entity.UserCenterInfo;
import com.liquid.poros.entity.UserCoinInfo;
import com.liquid.poros.entity.UserLevelInfo;
import com.liquid.poros.entity.UserPhotoInfo;

/**
 * Created by sundroid on 29/07/2019.
 */

public interface MineContract {
    interface View extends BaseContract.BaseView {

        void fetchUserCoinSuccess(UserCoinInfo userCoinInfo);

        void onUserInfoFetch(UserCenterInfo userCenterInfo);

        void onUserPhotosFetch(UserPhotoInfo userPhotoInfo);

        void uploadUserInfoSuccess(PorosUserInfo userInfo);

        void onUserLevelFetch(UserLevelInfo userLevelInfo);
    }

    abstract class Presenter extends BPresenter<View> {
        /**
         * 获取用户余额
         *
         * @param
         */
        public abstract void getUserCoin();

        /**
         * 获取用户信息
         *
         * @param target_user_id 用户id
         */
        public abstract void getUserInfo(String target_user_id);

        /**
         * 获取用户相册
         *
         * @param target_user_id 用户id
         */
        public abstract void getUserPhotos(String target_user_id);

        /**
         * 更新用户游戏信息
         */
        public abstract void updateUserInfo(UserCenterInfo.Data.UserInfo.GameInfoBean gameInfoBean);
        /**
         * 获取用户等级
         *
         * @param target_user_id 用户id
         */
        public abstract void getUserLevel(String target_user_id);

    }
}
