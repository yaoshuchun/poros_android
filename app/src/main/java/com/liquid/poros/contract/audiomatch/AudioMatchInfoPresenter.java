package com.liquid.poros.contract.audiomatch;

import com.liquid.base.tools.GT;
import com.liquid.poros.entity.AudioMatchEditGameBean;
import com.liquid.poros.entity.AudioMatchGameBean;
import com.liquid.poros.entity.AudioMatchInfo;
import com.liquid.poros.entity.StartAudioMatchInfo;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.poros.utils.network.HttpCallback;
import com.liquid.poros.utils.network.RetrofitHttpManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;

public class AudioMatchInfoPresenter extends AudioMatchInfoContract.Presenter {

    @Override
    public void getAudioMatchInfo(String matchType) {
        JSONObject object = new JSONObject();
        getView().showLoading();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("match_type", matchType);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
        RetrofitHttpManager.getInstance().httpInterface.fetchAudioMatchInfo(body).enqueue(new HttpCallback() {
            @Override
            public void OnSucceed(String result) {
                if (!isViewAttached()) {
                    return;
                }
                getView().closeLoading();
                AudioMatchInfo audioMatchInfo = GT.fromJson(result, AudioMatchInfo.class);
                if (audioMatchInfo != null && audioMatchInfo.getCode() == 0) {
                    try {
                        ArrayList<Map<String, String>> wzry_platform_list = new ArrayList<>();
                        ArrayList<Map<String, String>> wzry_rank_list = new ArrayList<>();
                        ArrayList<Map<String, String>> wzry_area_list = new ArrayList<>();
                        JSONObject resultObj = new JSONObject(result);
                        JSONObject dataObj = resultObj.optJSONObject("data");
                        if (dataObj != null) {
                            JSONObject user_infoObj = dataObj.optJSONObject("game_info");
                            if (user_infoObj != null) {
                                AudioMatchEditGameBean staticGameBean = new AudioMatchEditGameBean();
                                //game_info_static
                                AudioMatchEditGameBean.SettingGameBean honor_of_kings = new AudioMatchEditGameBean.SettingGameBean();

                                JSONObject rankObj = user_infoObj.optJSONObject("rank_choice");
                                honor_of_kings.rank = new AudioMatchGameBean();
                                honor_of_kings.rank.des = "段位";
                                Iterator<String> rankIterator = rankObj.keys();
                                while (rankIterator.hasNext()) {
                                    Map<String, String> map2 = new HashMap<String, String>();
                                    String key = rankIterator.next();
                                    String value = rankObj.getString(key);
                                    map2.put(key, value);
                                    wzry_rank_list.add(map2);
                                    AudioMatchGameBean.Item item = new AudioMatchGameBean.Item();
                                    item.id = key;
                                    item.name = value;
                                    honor_of_kings.rank.items.add(item);
                                }

                                JSONObject zoneObj = user_infoObj.optJSONObject("zone_choice");
                                honor_of_kings.zone = new AudioMatchGameBean();
                                honor_of_kings.zone.des = "游戏大区";
                                Iterator<String> zoneIterator = zoneObj.keys();
                                while (zoneIterator.hasNext()) {
                                    Map<String, String> map2 = new HashMap<String, String>();
                                    String key = zoneIterator.next();
                                    String value = zoneObj.getString(key);
                                    map2.put(key, value);
                                    wzry_area_list.add(map2);
                                    AudioMatchGameBean.Item item = new AudioMatchGameBean.Item();
                                    item.id = key;
                                    item.name = value;
                                    honor_of_kings.zone.items.add(item);
                                }
                                staticGameBean.honor_of_kings = honor_of_kings;
                                audioMatchInfo.setStaticGameBean(staticGameBean);
                                AudioMatchInfo.GAMEDATA gamedata = new AudioMatchInfo.GAMEDATA();
                                gamedata.setPlatform_list(wzry_platform_list);
                                gamedata.setArea_list(wzry_area_list);
                                gamedata.setRank_list(wzry_rank_list);
                                audioMatchInfo.setGameData(gamedata);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    getView().fetchAudioMatchInfo(audioMatchInfo);
                }
            }

            @Override
            public void OnFailed(int ret, String result) {
                if (!isViewAttached()) {
                    return;
                }
                getView().closeLoading();
            }
        });
    }

    @Override
    public void startAudioMatch(String matchType, int zone, int rank, boolean isForce,int entryType) {
        JSONObject object = new JSONObject();
        getView().showLoading();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("match_type", matchType);
            object.put("zone", zone);
            object.put("rank", rank);
            object.put("force", isForce);
            object.put("entry_type", entryType);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
        RetrofitHttpManager.getInstance().httpInterface.startAudioMatch(body).enqueue(new HttpCallback() {
            @Override
            public void OnSucceed(String result) {
                if (!isViewAttached()) {
                    return;
                }
                getView().closeLoading();
                StartAudioMatchInfo startAudioMatchInfo = GT.fromJson(result, StartAudioMatchInfo.class);
                if (startAudioMatchInfo != null) {
                    if (startAudioMatchInfo.getCode() == 0) {
                        getView().startAudioMatchFetch(startAudioMatchInfo, zone, rank);
                    } else if (startAudioMatchInfo.getCode() == 19001) {//金币不足
                        getView().notEnoughCoinFetch(startAudioMatchInfo.getData().getText());
                    } else {
                        getView().showMessage(startAudioMatchInfo.getMsg());
                    }
                }
            }

            @Override
            public void OnFailed(int ret, String result) {
                if (!isViewAttached()) {
                    return;
                }
                getView().closeLoading();
            }
        });
    }
}
