package com.liquid.poros.contract.home;

import com.liquid.base.tools.GT;
import com.liquid.poros.entity.CpListInfo;
import com.liquid.poros.entity.IndexCpInviteInfo;
import com.liquid.poros.entity.RoomDetailsInfo;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.base.utils.LogOut;
import com.liquid.poros.utils.network.HttpCallback;
import com.liquid.poros.utils.network.RetrofitHttpManager;

import org.json.JSONObject;

import okhttp3.RequestBody;

/**
 * Created by sundroid on 25/03/2019.
 */

public class IndexPresenter extends IndexContract.Presenter {


    @Override
    public void fetchFetchCPList(int page) {
        try {
            JSONObject object = new JSONObject();
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("page", page);
            object.put("page_size", 20);

            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.getCpUserList(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    CpListInfo groupInfo = GT.fromJson(result, CpListInfo.class);
                    if (groupInfo != null && groupInfo.getCode() == 0) {
                        getView().onCPListFetched(groupInfo);
                    }

                }

                @Override
                public void OnFailed(int ret, String result) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void fetchIndexCpInvite() {
        try {
            JSONObject object = new JSONObject();
            object.put("token", AccountUtil.getInstance().getAccount_token());
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.index_cp_invite(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    LogOut.e("index", "cp_invite" + result);
                    IndexCpInviteInfo indexCpInviteInfo = GT.fromJson(result, IndexCpInviteInfo.class);
                    if (indexCpInviteInfo != null && indexCpInviteInfo.getCode() == 0) {
                        getView().onCPInviteFetched(indexCpInviteInfo);
                    }
                }

                @Override
                public void OnFailed(int ret, String result) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void fetchDispatchFemale(int dispatchType, String femaleUserId) {
        try {
            JSONObject object = new JSONObject();
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("dispatch_type", dispatchType);
            object.put("female_user_id", femaleUserId);
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.user_dispatch_female(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                }

                @Override
                public void OnFailed(int ret, String result) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void fetchRoomDetails(String roomId) {
        try {
            JSONObject object = new JSONObject();
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("room_id", roomId);
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.couple_video_room_details(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    RoomDetailsInfo roomDetailsInfo = GT.fromJson(result, RoomDetailsInfo.class);
                    if (roomDetailsInfo != null && roomDetailsInfo.getCode() == 0) {
                        getView().onRoomDetailsFetched(roomDetailsInfo);
                    }
                }

                @Override
                public void OnFailed(int ret, String result) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
