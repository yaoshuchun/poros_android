package com.liquid.poros.contract.user;

import com.liquid.base.tools.GT;
import com.liquid.poros.entity.PorosUserInfo;
import com.liquid.poros.entity.UserCenterInfo;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.base.utils.LogOut;
import com.liquid.poros.utils.network.HttpCallback;
import com.liquid.poros.utils.network.RetrofitHttpManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class UserInfoSettingPresenter extends UserInfoSettingContract.Presenter {

    @Override
    public void uploadImage(final String path) {
        try {
            MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);//表单类型
            File file = new File(path);//filePath 图片地址
            RequestBody requestBody = RequestBody.create(MediaType.parse("image/jpg"), file);
            builder.addFormDataPart("image_file", file.getName(), requestBody);
            MultipartBody body = builder.build();
            RetrofitHttpManager.getInstance().httpInterface.user_upload_image(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    LogOut.debug("user_upload_image = " + result);
                    try {
                        JSONObject object = new JSONObject(result);
                        int code = object.getInt("code");
                        String message = object.getString("msg");
                        if (code == 0) {
                            String image_url = object.getJSONObject("data").getString("image_url");
                            getView().uploadImageSuccess(path, image_url);
                        } else {
                            getView().showMessage(message);

                        }
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void OnFailed(int ret, String result) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void uploadImageOss(String path, String file_dir) {
        getView().showLoading();
        getView().showMessage("正在上传...");
        try {
            MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);//表单类型
            File file = new File(path);//filePath 图片地址
            RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            builder.addFormDataPart("image_file", file.getName(), requestBody);
            builder.addFormDataPart("file_dir", file_dir);
            builder.addFormDataPart("token", AccountUtil.getInstance().getAccount_token());
            MultipartBody body = builder.build();
            RetrofitHttpManager.getInstance().httpInterface.upload_image_oss(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    getView().closeLoading();
                    LogOut.e("zzzz", "上传图片" + result);
                    try {
                        JSONObject object = new JSONObject(result);
                        int code = object.getInt("code");
                        String message = object.getString("msg");
                        JSONObject data = object.getJSONObject("data");
                        String videoPath = data.getString("image_url");
                        if (code == 0) {
                            getView().uploadImageOssSuccess(videoPath);
                            getView().showMessage("上传成功");
                        } else {
                            getView().showMessage("上传失败，请重新上传");
                        }
                    } catch (Exception e) {

                    }
                }

                @Override
                public void OnFailed(int ret, String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    getView().closeLoading();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateUserHead(String path) {
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("avatar_url", path);
        } catch (Throwable e) {
            e.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
        RetrofitHttpManager.getInstance().httpInterface.user_update_info(body).enqueue(new HttpCallback() {
            @Override
            public void OnSucceed(String result) {
                LogOut.debug("Update UserInfo" + result);
                if (!isViewAttached()) {
                    return;
                }
                if (AccountUtil.handleUserInfo(result)) {
                    getView().showMessage("更新用户头像成功");
                } else {
                    getView().showMessage("更新用户头像失败");
                }
            }

            @Override
            public void OnFailed(int ret, String result) {

            }
        });
    }

    @Override
    public void getUserInfo(String target_user_id) {
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("target_user_id", target_user_id);
        } catch (Throwable e) {
            e.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
        RetrofitHttpManager.getInstance().httpInterface.user_info_other(body).enqueue(new HttpCallback() {
            @Override
            public void OnSucceed(String result) {
                if (!isViewAttached()) {
                    return;
                }
                UserCenterInfo baseModel = GT.fromJson(result, UserCenterInfo.class);
                if (baseModel != null && baseModel.getCode() == 0) {
                    getView().onUserInfoFetch(baseModel);
                } else {
                    getView().showMessage(baseModel.getMsg());
                }
            }

            @Override
            public void OnFailed(int ret, String result) {
            }
        });
    }

    @Override
    public void updateUserInfo(String nickName, String avatarUrl, String gender, String age, String couple_desc, JSONArray games) {
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("nick_name", nickName);
            object.put("avatar_url", avatarUrl);
            object.put("gender", gender);
            object.put("age", age);
            object.put("couple_desc", couple_desc);
            object.put("games", games);
        } catch (Throwable e) {
            e.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
        RetrofitHttpManager.getInstance().httpInterface.user_update_info(body).enqueue(new HttpCallback() {
            @Override
            public void OnSucceed(String result) {
                LogOut.debug("Update UserInfo" + result);
                if (!isViewAttached()) {
                    return;
                }
                PorosUserInfo porosUserInfo = GT.fromJson(result, PorosUserInfo.class);
                if (AccountUtil.handleUserInfo(result)) {
                    getView().uploadUserInfoSuccess(porosUserInfo);
                } else {
                }
            }

            @Override
            public void OnFailed(int ret, String result) {

            }
        });
    }
}
