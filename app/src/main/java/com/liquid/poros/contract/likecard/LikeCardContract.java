package com.liquid.poros.contract.likecard;

import com.liquid.base.mvp.BaseContract;
import com.liquid.poros.base.BPresenter;
import com.liquid.poros.entity.LikeCardInfo;

public interface LikeCardContract {
    interface View extends BaseContract.BaseView {
        void onLikeCardListFetched(LikeCardInfo likeCardInfo);
    }

    abstract class Presenter extends BPresenter<View> {
        /**
         * 获取点赞卡列表
         *
         * @param page      页码
         */
        public abstract void fetchedLikeCardList(int page);

    }
}
