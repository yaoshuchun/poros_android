package com.liquid.poros.contract.user;

import com.liquid.base.mvp.BaseContract;
import com.liquid.poros.base.BPresenter;
import com.liquid.poros.entity.GuardRankerBean;

public interface GuardGiftRankContract {

    interface View extends BaseContract.BaseView {
        void onRankInfo(GuardRankerBean bean);
    }

    abstract class Presenter extends BPresenter<View> {
        public abstract void getUserRankInfo(String male_user_id);
    }

}
