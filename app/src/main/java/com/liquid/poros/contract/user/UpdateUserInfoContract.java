package com.liquid.poros.contract.user;

import com.liquid.base.mvp.BaseContract;
import com.liquid.poros.base.BPresenter;
import com.liquid.poros.entity.PorosUserInfo;
import com.liquid.poros.entity.UserCenterInfo;

import org.json.JSONArray;

public interface UpdateUserInfoContract {
    interface View extends BaseContract.BaseView {
        void uploadImageOssSuccess(String path);

        void uploadUserInfoSuccess(PorosUserInfo userInfo);
    }

    abstract class Presenter extends BPresenter<View> {
        /**
         * 上传
         *
         * @param file_dir 上传路径区分 user_cover_img 个人中心背景图，user_photo 个人相册,user_voice 语音介绍
         */
        public abstract void uploadImageOss(String path, String file_dir);

        /**
         * 更新用户信息
         */
        public abstract void updateUserInfo(boolean isMatchControl,String nickName, String avatarUrl, String gender, String age, String couple_desc, JSONArray games, UserCenterInfo.Data.UserInfo.GameInfoBean gameInfoBean);
    }
}
