package com.liquid.poros.contract.couple;

import com.liquid.base.mvp.BaseContract;
import com.liquid.poros.base.BPresenter;
import com.liquid.poros.entity.PrivateRoomListInfo;
import com.liquid.poros.entity.RoomDetailsInfo;

public interface PrivateRoomContract {
    interface View extends BaseContract.BaseView {
        void onPrivateRoomListFetched(PrivateRoomListInfo privateRoomListInfo);

        void onRoomDetailsFetched(RoomDetailsInfo roomDetailsInfo);
    }

    abstract class Presenter extends BPresenter<View> {
        /**
         * 获取专属房列表
         *
         * @param page 页码
         */
        public abstract void fetchPrivateRoom(int page);

        /**
         * 获取房间类型
         *
         * @param roomId 房间id
         */
        public abstract void fetchRoomDetails(String roomId);
    }
}
