package com.liquid.poros.contract.account;

import android.util.Log;

import com.liquid.base.tools.GT;
import com.liquid.library.retrofitx.RetrofitX;
import com.liquid.poros.entity.AlipayInfo;
import com.liquid.poros.entity.BaseModel;
import com.liquid.poros.entity.RechargeInfo;
import com.liquid.poros.entity.RechargeResultInfo;
import com.liquid.poros.entity.UserCoinInfo;
import com.liquid.poros.entity.WeChatPayInfo;
import com.liquid.poros.http.ApiUrl;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.poros.utils.network.HttpCallback;
import com.liquid.poros.utils.network.RetrofitHttpManager;

import org.json.JSONException;
import org.json.JSONObject;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;
import okhttp3.RequestBody;

/**
 * Created by sundroid on 29/07/2019.
 */

public class RechargePresenter extends RechargeContract.Presenter {

    public void setMatch_room(boolean match_room) {
        this.match_room = match_room;
    }

    private boolean match_room= false;
    public void setVideo_match_room(boolean video_match_room) {
        this.video_match_room = video_match_room;
    }

    private boolean video_match_room= false;
    @Override
    public void rechargeInfo(String roomId) {
        JSONObject object = new JSONObject();
        try {
            object.put("room_id",roomId);
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("match_room", match_room);
            object.put("video_match", video_match_room);
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.recharge_info(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    Log.e("zzz", "充值信息" + result);
                    if (!isViewAttached()) {
                        return;
                    }
                    RechargeInfo rechargeInfo = GT.fromJson(result, RechargeInfo.class);
                    if (rechargeInfo != null && rechargeInfo.getCode() == 0) {
                        getView().onRechargeInfoFetch(rechargeInfo);
                    }
                }

                @Override
                public void OnFailed(int ret, String result) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void wechatOrder(int amount, String recharge_type) {
        getView().showLoading();
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("amount", amount);
            object.put("recharge_type", recharge_type);
            object.put("match_room", match_room);
            object.put("video_match", video_match_room);
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.wechat_order(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    Log.e("zzz", "微信充值" + result);
                    if (!isViewAttached()) {
                        return;
                    }
                    getView().closeLoading();
                    WeChatPayInfo weChatPayInfo = GT.fromJson(result, WeChatPayInfo.class);
                    if (weChatPayInfo != null) {
                        if (weChatPayInfo.getCode() == 0) {
                            getView().onWechatSuccessFetch(weChatPayInfo);
                        } else {
                            getView().showMessage(weChatPayInfo.getMsg());
                        }
                    }
                }

                @Override
                public void OnFailed(int ret, String result) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void alipayOrder(int amount, String recharge_type) {
        getView().showLoading();
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("amount", amount);
            object.put("recharge_type", recharge_type);
            object.put("match_room", match_room);
            object.put("video_match", video_match_room);
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.alipay_order(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    Log.e("zzz", "支付宝充值" + result);
                    if (!isViewAttached()) {
                        return;
                    }
                    getView().closeLoading();
                    AlipayInfo alipayInfo = GT.fromJson(result, AlipayInfo.class);
                    if (alipayInfo != null) {
                        if (alipayInfo.getCode() == 0) {
                            getView().onAlipaySuccessFetch(alipayInfo);
                        } else {
                            getView().showMessage(alipayInfo.getMsg());
                        }
                    }
                }

                @Override
                public void OnFailed(int ret, String result) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void rechargeResult(String order_id) {
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("order_id", order_id);
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.recharge_result(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    Log.e("zzz", "充值结果" + result);
                    if (!isViewAttached()) {
                        return;
                    }
                    RechargeResultInfo rechargeResultInfo = GT.fromJson(result, RechargeResultInfo.class);
                    if (rechargeResultInfo != null) {
                        getView().onRechargeResultFetch(rechargeResultInfo);
                        getView().showMessage(rechargeResultInfo.getMsg());
                    }
                }

                @Override
                public void OnFailed(int ret, String result) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void getUserCoin() {
        try {
            JSONObject object = new JSONObject();
            object.put("token", AccountUtil.getInstance().getAccount_token());
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.user_rest_coin(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    Log.e("zzz", "获取个人金币 == " + result);
                    UserCoinInfo userCoinInfo = GT.fromJson(result, UserCoinInfo.class);
                    if (userCoinInfo != null && userCoinInfo.getCode() == 0) {
                        getView().onUserCoinSuccessFetch(userCoinInfo);
                    }
                }

                @Override
                public void OnFailed(int ret, String result) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void closeRechargeOrder(String orderId) {
        try {
            JSONObject object = new JSONObject();
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("out_trade_no", orderId);
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.recharge_close_order(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    Log.e("zzz", "取消充值订单" + result);
                    BaseModel baseModel = GT.fromJson(result, BaseModel.class);
                    if (baseModel != null && baseModel.getCode() == 0) {
                        getView().onCloseRechargeOrderFetch();
                    }
                }

                @Override
                public void OnFailed(int ret, String result) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void smallPacketPay(int pay_amount, int pay_way) {
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("pay_amount", pay_amount);
            object.put("pay_way", pay_way);
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.small_packet_pay(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    if (pay_way == 1){
                        WeChatPayInfo weChatPayInfo = GT.fromJson(result, WeChatPayInfo.class);
                        if (weChatPayInfo != null) {
                            if (weChatPayInfo.getCode() == 0) {
                                getView().onWechatSuccessFetch(weChatPayInfo);
                            } else {
                                getView().showMessage(weChatPayInfo.getMsg());
                            }
                        }
                    }else {
                        AlipayInfo alipayInfo = GT.fromJson(result, AlipayInfo.class);
                        if (alipayInfo != null) {
                            if (alipayInfo.getCode() == 0) {
                                getView().onAlipaySuccessFetch(alipayInfo);
                            } else {
                                getView().showMessage(alipayInfo.getMsg());
                            }
                        }
                    }
                }

                @Override
                public void OnFailed(int ret, String result) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void payOneCent (int pay_amount, int pay_way){
        javaWay(pay_amount, pay_way);
    }

    private void javaWay(int pay_amount, int pay_way) {
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            object.put("pay_amount", pay_amount);
            object.put("pay_way", pay_way);
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.red_cent_pay(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    if (!isViewAttached()) {
                        return;
                    }
                    if (pay_way == 1){
                        WeChatPayInfo weChatPayInfo = GT.fromJson(result, WeChatPayInfo.class);
                        if (weChatPayInfo != null) {
                            if (weChatPayInfo.getCode() == 0) {
                                getView().onWechatSuccessFetch(weChatPayInfo);
                            } else {
                                getView().showMessage(weChatPayInfo.getMsg());
                            }
                        }
                    }else {
                        AlipayInfo alipayInfo = GT.fromJson(result, AlipayInfo.class);
                        if (alipayInfo != null) {
                            if (alipayInfo.getCode() == 0) {
                                getView().onAlipaySuccessFetch(alipayInfo);
                            } else {
                                getView().showMessage(alipayInfo.getMsg());
                            }
                        }
                    }
                }

                @Override
                public void OnFailed(int ret, String result) {

                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void kotlinWay (int pay_amount, int pay_way){
        RetrofitX.Companion.url(ApiUrl.PAY_ONE_CENT)
                .param("token",AccountUtil.getInstance().getAccount_token())
                .param("pay_amount", pay_amount)
                .param("pay_way", pay_way)
                .post()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<String>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull String result) {
                        if (!isViewAttached()) {
                            return;
                        }
                        try {
                            if (pay_way == 1){
                                WeChatPayInfo weChatPayInfo = GT.fromJson(result, WeChatPayInfo.class);
                                if (weChatPayInfo != null) {
                                    if (weChatPayInfo.getCode() == 0) {
                                        getView().onWechatSuccessFetch(weChatPayInfo);
                                    } else {
                                        getView().showMessage(weChatPayInfo.getMsg());
                                    }
                                }
                            }else {
                                AlipayInfo alipayInfo = GT.fromJson(result, AlipayInfo.class);
                                if (alipayInfo != null) {
                                    if (alipayInfo.getCode() == 0) {
                                        getView().onAlipaySuccessFetch(alipayInfo);
                                    } else {
                                        getView().showMessage(alipayInfo.getMsg());
                                    }
                                }
                            }
                        }catch (Throwable e){
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

}
