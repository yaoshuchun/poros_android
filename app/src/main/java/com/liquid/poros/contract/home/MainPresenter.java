package com.liquid.poros.contract.home;

import android.util.Log;

import com.liquid.base.tools.GT;
import com.liquid.library.retrofitx.RetrofitX;
import com.liquid.poros.entity.BaseBean;
import com.liquid.poros.entity.KeyAdd;
import com.liquid.poros.entity.PrizeCoinInfo;
import com.liquid.poros.entity.UpdateInfo;
import com.liquid.poros.http.ApiUrl;
import com.liquid.poros.http.observer.ObjectObserver;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.base.utils.LogOut;
import com.liquid.poros.utils.network.HttpCallback;
import com.liquid.poros.utils.network.RetrofitHttpManager;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;


/**
 * Created by sundroid on 19/03/2019.
 */

public class MainPresenter extends MainContract.Presenter {


    @Override
    public void checkUpdate(String cur_version) {
        RetrofitHttpManager.getInstance().httpInterface.checkAppUpdate("jinquan").enqueue(new HttpCallback() {
            @Override
            public void OnSucceed(String result) {
                LogOut.debug("Update UserInfo" + result);
                if (!isViewAttached()) {
                    return;
                }
                UpdateInfo updateInfo = GT.fromJson(result, UpdateInfo.class);
                getView().onUpdateFetch(updateInfo);
            }

            @Override
            public void OnFailed(int ret, String result) {
                LogOut.debug("Update UserInfo" + result);
                if (!isViewAttached()) {
                    return;
                }
                getView().onUpdateFail();

            }
        });
    }

    @Override
    public void resetOnlineStatus() {
        String token = AccountUtil.getInstance().getAccount_token();
        RetrofitHttpManager.getInstance().httpInterface.leaveRoom(token).enqueue(new HttpCallback() {
            @Override
            public void OnSucceed(String result) {


            }

            @Override
            public void OnFailed(int ret, String result) {

            }
        });
    }

    @Override
    public void getDailyPrizeCoin() {
        JSONObject object = new JSONObject();
        try {
            object.put("token", AccountUtil.getInstance().getAccount_token());
            RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), object.toString());
            RetrofitHttpManager.getInstance().httpInterface.get_daily_prize_coin(body).enqueue(new HttpCallback() {
                @Override
                public void OnSucceed(String result) {
                    Log.e("get_daily_prize_coin", result);
                    if (!isViewAttached()) {
                        return;
                    }
                    PrizeCoinInfo baseModel = GT.fromJson(result, PrizeCoinInfo.class);
                    if (baseModel != null && baseModel.getCode() == 0) {
                        getView().onFetchCoin(baseModel);
                    }
                }

                @Override
                public void OnFailed(int ret, String result) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    @Override
    public void checkKeyStatus() {
        RetrofitX.Companion.url(ApiUrl.MAIN_PAGE_GOT_KEY_STATUS).
                param("token",AccountUtil.getInstance().getAccount_token()).
                postJSON()
                .subscribe(new ObjectObserver<KeyAdd>() {
                    @Override
                    public void failed(@NotNull KeyAdd result) {

                    }

                    @Override
                    public void success(@NotNull KeyAdd result) {
                        if (!isViewAttached()) {
                            return;
                        }
                        if (result != null && result.getCode() == 0) {
                            getView().onShowKeyStatus(result);
                        }
                    }
                });
    }
}
