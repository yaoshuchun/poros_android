package com.liquid.poros.wxapi;

import android.content.Context;
import android.graphics.Bitmap;
import android.widget.Toast;

import com.liquid.poros.PorosApplication;
import com.tencent.mm.opensdk.modelmsg.SendMessageToWX;
import com.tencent.mm.opensdk.modelmsg.WXMediaMessage;
import com.tencent.mm.opensdk.modelmsg.WXWebpageObject;
import com.tencent.mm.opensdk.modelpay.PayReq;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;

import java.io.ByteArrayOutputStream;

public class WXUtils {

    private static WXUtils instance = null;
    private IWXAPI api = null;
    public static final String WX_APP_ID = "wx841f86eafb81f6a9";
    public static final String WX_APP_SECERT = "9ebef2d481502ee47e6eca0d804dfe18";
    public static final int SHARE_TO_WX_PYQ = 0x10001;
    public static final int SHARE_TO_WX_FRIEND = 0x10002;

    private WXUtils(Context context) {
        api = WXAPIFactory.createWXAPI(context, WX_APP_ID, true);
        api.registerApp(WX_APP_ID);
    }

    public static WXUtils getInstance(Context context) {
        return instance == null ? instance = new WXUtils(context) : instance;
    }

    public IWXAPI getIWXApi() {
        return api;
    }



    public void shareToWx(int type, String url, String title, String desc, Bitmap thumb) {
        WXWebpageObject webpage = new WXWebpageObject();
        webpage.webpageUrl = url;
        WXMediaMessage msg = new WXMediaMessage(webpage);
        msg.title = title;
        msg.description = desc;
        msg.thumbData = bitmapToBytes(thumb);
        SendMessageToWX.Req req = new SendMessageToWX.Req();
        req.message = msg;
        req.transaction = buildTransaction("webpage");
        req.scene = type == SHARE_TO_WX_PYQ ? SendMessageToWX.Req.WXSceneSession : SendMessageToWX.Req.WXSceneSession;
        boolean succeed = api.sendReq(req);
        if (!succeed) {
            Toast.makeText(PorosApplication.context, "调起微信失败，请检查是否安装了最新版微信", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * 微信支付
     * @param appid
     * @param partnerid
     * @param prepayid
     * @param noncestr
     * @param timestamp
     * @param packageExtra
     * @param sign
     */
    public void recharge(String appid, String partnerid, String prepayid, String noncestr, String timestamp, String packageExtra, String sign) {
        PayReq req = new PayReq();
        req.appId = appid;
        req.partnerId = partnerid;
        req.prepayId = prepayid;
        req.nonceStr = noncestr;
        req.timeStamp = timestamp;
        req.packageValue = packageExtra;
        req.sign = sign;
        api.sendReq(req);
    }

    private static byte[] bitmapToBytes(Bitmap decodeStream) {
        Bitmap createScaledBitmap = Bitmap.createScaledBitmap(decodeStream, 150, 150, true);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        createScaledBitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] toByteArray = byteArrayOutputStream.toByteArray();
        createScaledBitmap.recycle();
        try {
            byteArrayOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return toByteArray;
    }


    private String buildTransaction(final String type) {
        return (type == null) ? String.valueOf(System.currentTimeMillis()) : type + System.currentTimeMillis();
    }

}
