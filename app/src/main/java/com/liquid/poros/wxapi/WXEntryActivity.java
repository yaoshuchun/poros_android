package com.liquid.poros.wxapi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.liquid.base.event.EventCenter;
import com.liquid.poros.PorosApplication;
import com.liquid.poros.R;
import com.liquid.poros.constant.Constants;
import com.liquid.base.utils.LogOut;
import com.tencent.mm.opensdk.modelbase.BaseReq;
import com.tencent.mm.opensdk.modelbase.BaseResp;
import com.tencent.mm.opensdk.modelmsg.SendAuth;
import com.tencent.mm.opensdk.openapi.IWXAPIEventHandler;

import de.greenrobot.event.EventBus;

/**
 * Created by root on 17-8-16.
 */

public class WXEntryActivity extends Activity {

    private static final String TAG = "WXEntryActivity";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wx_entry_activity);
        WXUtils.getInstance(PorosApplication.context).getIWXApi().handleIntent(getIntent(), new IWXAPIEventHandler() {
            @Override
            public void onReq(BaseReq baseReq) {

            }

            @Override
            public void onResp(BaseResp baseResp) {
                LogOut.debug(TAG, "onResp errCode:" + baseResp.errCode);
                if (baseResp.errCode == BaseResp.ErrCode.ERR_OK) {
                    if (baseResp.getType() != 2) {
                        SendAuth.Resp newResp = (SendAuth.Resp) baseResp;
                        //获取微信传回的code
                        String code = newResp.code;
                        EventBus.getDefault().post(new EventCenter(Constants.EVENT_CODE_WECHAT_AUTH_FINISH, code));
                    }
                }
                finish();
            }
        });
    }
}
