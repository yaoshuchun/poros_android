package com.liquid.poros.http.observer

import androidx.annotation.NonNull
import androidx.annotation.Nullable
import com.blankj.utilcode.util.GsonUtils
import com.blankj.utilcode.util.LogUtils
import com.liquid.poros.entity.BaseBean
import io.reactivex.rxjava3.observers.DisposableObserver
import org.json.JSONArray
import org.json.JSONObject
import org.json.JSONTokener
import java.lang.reflect.Array
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type

/**
 * 列表对象Observer
 * 根据具体的类型返回具体的数据
 * @author guomenglong
 * @date 2020/10/20 1:38 PM
 */
abstract class ListObjectObserver<R> : DisposableObserver<String>() {

    override fun onComplete() {
    }

    override fun onNext(t: String) {
        try {
            val jsonObject = JSONObject(t)
            val msg = jsonObject["msg"].toString()
            val code = jsonObject.getInt("code")
            val d = if (jsonObject.has("data")) {
                jsonObject["data"] ?: "{}"
            } else {
                "{}"
            }
            val nextValue = JSONTokener(d.toString()).nextValue()
            val type = getSuperclassTypeParameter(javaClass)
            if (nextValue is JSONArray) {
                val result = GsonUtils.fromJson<List<R>>(d.toString(), generateArrayType(type))
                when (code) {
                    0 -> {
                        success(result, msg)
                    }
                    else -> {
                        failed(result, code, msg)
                    }
                }
            }
        } catch (e: Exception) {
            LogUtils.e(e.printStackTrace())
        }
    }

    open fun getSuperclassTypeParameter(subclass: Class<*>): Type {
        val superclass = subclass.genericSuperclass
        if (superclass is Class<*>) {
            throw RuntimeException("Missing type parameter.")
        }
        val parameterized = superclass as ParameterizedType?
        return parameterized!!.actualTypeArguments[0]
    }

    /**
     * 生成一个泛型的数组
     */
    fun generateArrayType(type: Type): Class<Any> {
        val rawType = type as ParameterizedType
        return Array.newInstance(rawType.javaClass, 0).javaClass
    }

    override fun onError(e: Throwable) {
        LogUtils.e("BaseObserver", "--onError->$e") //throwable.getMessage()
        failed(null,-1, "请求失败，请稍后再试")
    }

    /**
     * 数据请求成功并返回列表数据
     */
    abstract fun success(@NonNull result: List<R>, msg: String)

    /**
     * 数据失败并返回数据
     */
    abstract fun failed(@Nullable result: List<R>?, code: Int, msg: String)

}