package com.liquid.poros.http.observer

import com.blankj.utilcode.util.GsonUtils
import com.blankj.utilcode.util.LogUtils
import com.liquid.base.utils.LogOut
import com.liquid.poros.BuildConfig
import com.liquid.poros.entity.BaseBean
import com.liquid.poros.http.utils.ResponseUtils
import com.liquid.poros.utils.MyActivityManager
import com.liquid.poros.utils.ToastUtil
import io.reactivex.rxjava3.observers.DisposableObserver
import org.json.JSONObject
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type

/**
 * 对象Observer
 * 根据具体的类型返回具体的数据
 * @author guomenglong
 * @date 2020/10/20 1:38 PM
 */
abstract class ObjectObserver<R : BaseBean> : DisposableObserver<String>() {

    override fun onComplete() {
    }

    override fun onNext(t: String) {
        try {
            val jsonObject = JSONObject(t)
            val msg = jsonObject["msg"].toString()
            val code = jsonObject.getInt("code")
            val d = if (jsonObject.has("data")) {
                jsonObject["data"] ?: "{}"
            } else {
                "{}"
            }
            val type = getSuperclassTypeParameter(javaClass)
            val result = GsonUtils.fromJson<R>(d.toString(), type)
            result.code = code
            result.msg = msg
            when (code) {
                0 -> {
                    success(result)
                }
                else -> {
                    failed(result)
                }
            }
        } catch (e: Throwable) {
            LogUtils.e(e.stackTrace?.joinToString("\n"))
        }
    }

    open fun getSuperclassTypeParameter(subclass: Class<*>): Type {
        val superclass = subclass.genericSuperclass
        if (superclass is Class<*>) {
            throw RuntimeException("Missing type parameter.")
        }
        val parameterized = superclass as ParameterizedType?
        return parameterized!!.actualTypeArguments[0]
    }

    override fun onError(e: Throwable) {
        try {
            //统一错误提示
            if (BuildConfig.DEBUG || BuildConfig.LOG_ENABLE){
                if (null != MyActivityManager.getInstance().currentActivity){
                    MyActivityManager.getInstance().currentActivity.runOnUiThread {
                        ToastUtil.show(ResponseUtils.parseThrowable(e))
                    }
                }
                LogOut.debug(ResponseUtils.parseThrowable(e))
            }
            val type = getSuperclassTypeParameter(javaClass)
            val rawType = type as Class<*>
            if (rawType is BaseBean) {
                failed(BaseBean(-1, "请求失败，请稍后再试") as R)
            } else {
                val newInstance = rawType.newInstance()
                val code = rawType.superclass.getDeclaredField("code")
                code.isAccessible = true
                val msg = rawType.superclass.getDeclaredField("msg")
                msg.isAccessible = true
                code.setInt(newInstance, -1)
                msg.set(newInstance, "请求失败，请稍后再试")
                LogUtils.e("BaseObserver", "--onError->$e") //throwable.getMessage()
                failed(newInstance as R)
            }
        } catch (e: Exception) {
            LogUtils.e(e.printStackTrace())
        }
    }


    /**
     * 数据请求成功且有数据
     */
    abstract fun success(result: R)

    /**
     * 数据请求失败且提示
     */
    abstract fun failed(result: R)
}