package com.liquid.poros.http.observer

import io.reactivex.rxjava3.observers.DisposableObserver

/**
 * 简单Observer继承类
 */
open class SimpleObserver :DisposableObserver<String>() {
    override fun onNext(result: String?) {
    }

    override fun onError(e: Throwable?) {
    }

    override fun onComplete() {
    }
}