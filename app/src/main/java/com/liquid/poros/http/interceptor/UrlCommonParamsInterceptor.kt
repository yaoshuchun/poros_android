package com.liquid.poros.http.interceptor

import com.liquid.base.utils.BaseConstants
import com.liquid.base.utils.GlobalConfig
import com.liquid.base.utils.LogOut
import com.liquid.poros.utils.AccountUtil
import okhttp3.Interceptor
import okhttp3.Response
import java.util.*

/**
 * 公共参数增加
 */
class UrlCommonParamsInterceptor: Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val oldRequest = chain.request()
        val path = oldRequest.url.toUrl().path
        val expiredTime = "" + System.currentTimeMillis() / 1000 + 300
        // 添加新的参数
        // 添加新的参数
        val oldUrl = oldRequest.url
        val authorizedUrlBuilder = oldUrl
                .newBuilder()
                .scheme(oldUrl.scheme)
                .host(oldUrl.host)

        //TO url verifycation

        //TO url verifycation
        val ran = UUID.randomUUID().toString()
        if (oldUrl.toUrl().toString().contains("liquidnetwork.com") ||
                oldUrl.toUrl().toString().contains("diffusenetwork.com")) {
            authorizedUrlBuilder
                    .addQueryParameter(BaseConstants.REMOTE_KEY.VERSION_NAME, GlobalConfig.instance().clientVersion)
                    .addQueryParameter(BaseConstants.REMOTE_KEY.DEVICE_ID, GlobalConfig.instance().deviceID)
                    .addQueryParameter(BaseConstants.REMOTE_KEY.OAID, GlobalConfig.instance().oaid)
                    .addQueryParameter(BaseConstants.REMOTE_KEY.CHANNEL_NAME, GlobalConfig.instance().channelName)
                    .addQueryParameter(BaseConstants.REMOTE_KEY.EXPIRED_TIME, expiredTime)
                    .addQueryParameter(BaseConstants.REMOTE_KEY.DEVICE_SERIAL, GlobalConfig.instance().serial)
                    .addQueryParameter(BaseConstants.REMOTE_KEY.NONCE, ran)
                    .addQueryParameter(BaseConstants.REMOTE_KEY.IMEI, GlobalConfig.instance().imei)
                    .addQueryParameter(BaseConstants.REMOTE_KEY.MAC, GlobalConfig.instance().mac)
                    .addQueryParameter(BaseConstants.REMOTE_KEY.USER_ID, AccountUtil.getInstance().userId)
                    .addQueryParameter(BaseConstants.REMOTE_KEY.ANDROID_ID, GlobalConfig.instance().androidid)
                    .addQueryParameter(BaseConstants.REMOTE_KEY.BOX_PKG_NAME, GlobalConfig.instance().packageName) //                        .addQueryParameter(BaseConstants.REMOTE_KEY.YID, UserAccount.getInstance().getUserYid())
                    .addQueryParameter(BaseConstants.REMOTE_KEY.PLATFORM_NAME, "Android")
            //所有的服务器请求接口统一增加yid和user_id参数
            try {
                authorizedUrlBuilder.addQueryParameter(BaseConstants.REMOTE_KEY.LATITUDE, GlobalConfig.instance().mLatitude)
                        .addQueryParameter(BaseConstants.REMOTE_KEY.LONGITUDE, GlobalConfig.instance().mLongitude)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
//             新的请求
        //             新的请求
        val newRequest = oldRequest.newBuilder()
                .method(oldRequest.method, oldRequest.body)
                .url(authorizedUrlBuilder.build())
                .build()
        LogOut.debug("newHomeDebug", authorizedUrlBuilder.build().toUrl().toString() + "")
        return chain.proceed(newRequest)
    }
}