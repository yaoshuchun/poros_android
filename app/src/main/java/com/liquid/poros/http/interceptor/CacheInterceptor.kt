package com.liquid.poros.http.interceptor;

import android.text.TextUtils
import android.util.Log
import com.liquid.poros.BuildConfig
import com.liquid.poros.http.utils.ResponseUtils
import okhttp3.CacheControl
import okhttp3.Interceptor
import okhttp3.Response
import okio.Buffer
import java.nio.charset.StandardCharsets
import java.util.concurrent.TimeUnit

/**
 * 缓存Interceptor
 * @author guomenglong
 * @date 2020/10/16 9:31 AM
 */
class CacheInterceptor :Interceptor {

    private val MAX_STALE = 60 * 60 * 24 * 7

    private val OFFLINE_CACHE = CacheControl.Builder()
            .onlyIfCached()
            .maxStale(MAX_STALE, TimeUnit.SECONDS)
            .build()

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val response = chain.proceed(request)
//        if (AppInfo.isNetworkReachable()) {
//            val maxAge = 0 // 有网络时 设置缓存超时时间为0;
//            response.newBuilder()
//                    .header("Cache-Control", "public, max-age=$maxAge")
//                    .removeHeader("Pragma") // 清除头信息，因为服务器如果不支持，会返回一些干扰信息，不清除下面无法生效
//                    .build()
//        } else {
//            val maxStale = 60 * 60 * 24 // 无网络时，设置超时为1天
//            Log.i("TAG", "has maxStale=$maxStale")
//            response.newBuilder()
//                    .header("Cache-Control", "public, only-if-cached, max-stale=$maxStale")
//                    .removeHeader("Pragma")
//                    .build()
//        }
        if (BuildConfig.DEBUG) {
            val responseBody = response.peekBody((1024 * 1024).toLong())
            //输出打印日志
            val str = StringBuilder()
            str.append("url: ").append(request.url).append("\n")
            var bodyParm: String? = null
            if (request.body != null) {
                val buffer = Buffer()
                request.body!!.writeTo(buffer)
                bodyParm = buffer.readUtf8()
            }
            //                str.append("head: ").append(request.headers().toString());
            if (!response.request.url.toString().contains("upload")
                    && !TextUtils.isEmpty(bodyParm)
                    && response.request.method == "POST") str.append("requestBody: ").append(bodyParm).append("\n")
            str.append("code: ").append(response.code).append("\n")
            val mediaType = responseBody.contentType()
            if (mediaType != null && "text" == mediaType.type && responseBody.byteStream().available() != 0) {
                if ("gzip" == response.header("Content-Encoding")) {
                    str.append("result: ").append(ResponseUtils.uncompress(responseBody.bytes()))
                } else {
                    str.append("result: ").append(String(responseBody.bytes(), StandardCharsets.UTF_8))
                }
            }
            Log.d("TAG",str.toString())
        }
        return response

    }
}