package com.liquid.poros.http.utils

import com.liquid.library.retrofitx.RetrofitX
import com.liquid.poros.utils.AccountUtil
import io.reactivex.rxjava3.core.Observable

/**
 * 扩展RetrofitX 请求时在Body里增加Token
 * 仅限Kotlin中调用，Java无法调用Kotlin扩展函数
 */
fun RetrofitX.Builder.postPoros(): Observable<String> {
    this.param("token", AccountUtil.getInstance().account_token)
    return this.postJSON()
}
fun RetrofitX.Builder.getPoros(): Observable<String> {
    this.param("token", AccountUtil.getInstance().account_token)
    return this.get()
}