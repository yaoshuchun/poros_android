package com.liquid.poros.http.utils

import com.google.gson.JsonSyntaxException
import retrofit2.HttpException
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import java.util.zip.GZIPInputStream

object ResponseUtils {
    const val CODE_SUCCESS = 200 //成功
    const val CODE_FAIL = 400 //请求无效，没有进入后台服务
    const val CODE_UNAUTHORIZED = 401 //未认证（签名错误）
    const val CODE_NOT_FOUND = 404 //接口不存在
    const val CODE_METHOD_NOT_ALLOW = 405 //请求方法不允许
    const val CODE_METHOD_REQUEST_TOO_LARGE = 413 //请求Entity过大
    const val CODE_INTERNAL_SERVER_ERROR = 500 //服务器内部错误
    const val CODE_INTERNAL_SERVER_RESTARTING = 502 //服务器重启中
    @Throws(IOException::class)
    fun uncompress(str: ByteArray?): String? {
        val baos = ByteArrayOutputStream()
        GZIPInputStream(ByteArrayInputStream(str)).use { gis ->
            var b: Int
            while (gis.read().also { b = it } != -1) {
                baos.write(b)
            }
        }
        return unicodeToUtf8(baos.toString("UTF-8"))
    }

    fun unicodeToUtf8(theString: String): String? {
        var aChar: Char
        val len = theString.length
        val outBuffer = StringBuffer(len)
        var x = 0
        while (x < len) {
            aChar = theString[x++]
            if (aChar == '\\') {
                aChar = theString[x++]
                if (aChar == 'u') {
                    // Read the xxxx
                    var value = 0
                    for (i in 0..3) {
                        aChar = theString[x++]
                        value = when (aChar) {
                            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' -> (value shl 4) + aChar.toInt() - '0'.toInt()
                            'a', 'b', 'c', 'd', 'e', 'f' -> (value shl 4) + 10 + aChar.toInt() - 'a'.toInt()
                            'A', 'B', 'C', 'D', 'E', 'F' -> (value shl 4) + 10 + aChar.toInt() - 'A'.toInt()
                            else -> throw IllegalArgumentException(
                                    "Malformed   \\uxxxx   encoding.")
                        }
                    }
                    outBuffer.append(value.toChar())
                } else {
                    if (aChar == 't') aChar = '\t' else if (aChar == 'r') aChar = '\r' else if (aChar == 'n') aChar = '\n' else if (aChar == 'f') aChar = '\u000C'
                    outBuffer.append(aChar)
                }
            } else outBuffer.append(aChar)
        }
        return outBuffer.toString()
    }

    fun parseThrowable(t: Throwable):String{
        return when (t) {
            is SocketTimeoutException -> "请求超时，请重新加载"
            is UnknownHostException -> "无网络连接，请检查网络设置"
            is ConnectException -> "网络连接错误"
            is IOException -> "网络异常"
            is UnknownError -> "未找到主机"
            is JsonSyntaxException -> "解析数据出错,可查看返回数据结构与Bean是否一致"
            is HttpException -> parseHttpException(t)
            else -> "请求数据异常"
        }

    }

    fun parseHttpException(httpException: HttpException):String {
        return when (httpException.code()) {
            CODE_FAIL -> "请求无效,没有进入后台服务"
            CODE_UNAUTHORIZED -> "未认证（签名错误）"
            CODE_NOT_FOUND -> "接口不存在或接口路径拼写错误"
            CODE_METHOD_NOT_ALLOW -> "请求方法不允许,SET/GET等"
            CODE_METHOD_REQUEST_TOO_LARGE -> "请求数据过大(Request Entity Too Large)"
            CODE_INTERNAL_SERVER_ERROR -> "服务器内部错误"
            CODE_INTERNAL_SERVER_RESTARTING -> "服务器重启中"
            else -> "请求失败"
        }
    }
}