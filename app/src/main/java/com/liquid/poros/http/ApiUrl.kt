package com.liquid.poros.http

import com.liquid.poros.constant.Constants
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

/**
 * 网络请求ApiUrl
 * 所有请求的链接地址均放置在这里
 */
object ApiUrl {

    //心跳URL
    @JvmField
    val HEART_BEAT = "${Constants.BASE_URL}user/user_active_heartbeat"
    //视频房导流关闭上报
    @JvmField
    val ROOM_DIVERSION_CLOSE = "${Constants.BASE_URL}views/cp_room_diversion/set_cd_time"
    //开始速配
    @JvmField
    val CP_MATCHING = "${Constants.BASE_URL}views/match_room/request"
    //速配信息
    @JvmField
    val MATCH_INFO = "${Constants.BASE_URL}views/match_room/match_info"

    @JvmField
    val CARTOON_ASK_GRANT = "${Constants.BASE_URL}views/cartoon/ask/grant"
    //套装列表
    @JvmField
    val PTA_SUIT_LIST = "${Constants.BASE_URL}views/cartoon/suit_list"
    //购买套装
    @JvmField
    val PTA_BUY_SUIT = "${Constants.BASE_URL}views/cartoon/buy_suit"
    @JvmField
    val DOWNLOAD_LIST = "${Constants.BASE_URL}views/cartoon/download_list"

    @JvmField
    val GET_MSG_USER_INFO = "${Constants.BASE_B_URL}p2p_message/get_msg_user_info"

    //获取当前聊天页的套装
    @JvmField
    val CARTOON_SUIT = "${Constants.BASE_URL}views/cartoon/suit"
    //支付1分钱特殊聊天
    @JvmField
    val PAY_ONE_CENT = "${Constants.BASE_URL}views/reward/red_cent/pay"

    //私聊钥匙弹窗
    @JvmField
    val P2P_CHAT_KEY_TIP = "${Constants.BASE_URL}views/gift_box/popup_info"
    /**
     * appconfig接口
     */
    @JvmField
    val APP_COFIG_URL = "${Constants.BASE_URL}user/get_config"
    //APP更新版本
    @JvmField
    val APK_UPDATE_URL = "http://conf.liquidnetwork.com/appversion/app_update"
    //pk房礼物排行榜
    @JvmField
    val PK_GIFT_RANK_LIST = "${Constants.BASE_URL}views/video_room/pk_ranking_list"
    //获取房间类型
    @JvmField
    val  LIVEROOM_INFO_URL = "${Constants.BASE_URL}views/video_room/details"

    //获取亲密消息列表的数据
    @JvmField
    val  INTEMATE_LIST_URL = "${Constants.BASE_URL}p2p_message/get_msg_session_list_v3"

    //组CP房间详情
    @JvmField
    val LIVE_ROOM_INFO = "${Constants.BASE_URL}couple/room_info"
    //申请上麦
    @JvmField
    val LIVE_ROOM_REQUEST_CP = "${Constants.BASE_URL}couple/waitinglist"
    //拉去 连麦申请列表
    @JvmField
    val LIVE_ROOM_WAITING_LIST = "${Constants.BASE_URL}couple/waitinglist/list"
    //拉去 专属房申请列表
    @JvmField
    val LIVE_ROOM_PRIVATE_WAITING_LIST = "${Constants.BASE_URL}couple/private_room/request_list"
    //送礼
    @JvmField
    val SEND_GIFT = "${Constants.BASE_URL}user/send_gift"
    //设置视频放ChannelId
    @Deprecated("C端无用")
    @JvmField
    val LIVE_ROOM_CHANNEL_ID = "${Constants.BASE_URL}couple/room_set_channel_id"
    //下麦
    @JvmField
    val LIVE_ROOM_QUIT_ON_MIC = "${Constants.BASE_URL}couple/room_quit_on_mic"
    //上麦
    @JvmField
    val LIVE_ROOM_JOIN_MIC = "${Constants.BASE_URL}couple/room_on_mic/v2"
    //上麦后更新rtmp任务
    @JvmField
    val LIVE_ROOM_UPDATE_RTMP_TASK = "${Constants.BASE_URL}couple/update_rtmp_task"
    //专属房上麦
    @JvmField
    val LIVE_ROOM_PRIVATE_JOIN_MIC = "${Constants.BASE_URL}couple/private_room/on_mic"
    //申请进入专属房
    @JvmField
    val LIVE_ROOM_PRIVATE_REQUEST = "${Constants.BASE_URL}couple/private_room/request"
    //申请加入CP团
    @JvmField
    val LIVE_ROOM_JOIN_CP_REQUEST = "${Constants.BASE_URL}views/cp_group/member"

    //专属房心跳
    @JvmField
    val LIVE_ROOM_PRIVATE_HEART_BEAT = "${Constants.BASE_URL}views/video_room/private_room/heart_beat"
    //同意CP组队
    @JvmField
    val LIVE_ROOM_AGREE_CP = "${Constants.BASE_URL}couple/room/team/invite/agree"
    //拒绝CP组队
    @JvmField
    val LIVE_ROOM_REFUSE_CP = "${Constants.BASE_URL}couple/room/team/invite/refuse"
    /**
     * 足迹触发
     * token                  Y
     * dispatch_type          Y
     * 触发场景：
     * 1 # 新手女嘉宾派单
     * 2 # 用户上线派单
     * 3 # 爆灯派单
     * 4 # 广场详情
     * 5 # 退出语音房
     * 6 # 发送消息
     */
    @JvmField
    val DISPATCH_BY_TYPE = "${Constants.BASE_URL}views/dispatch/by_type"
    //用户行为上报
    @JvmField
    val UPLOAD_BEHAVIOR = "${Constants.BASE_URL}user/upload_behavior"
    //用户开关摄像头接口
    @Deprecated("废弃")
    @JvmField
    val LIVE_ROOM_SWITCH_CAMERA = "${Constants.BASE_URL}couple/room_switch_camera"
    //获取用户等级
    @JvmField
    val USER_LEVEL = "${Constants.BASE_URL}views/user/get_user_level"
    //我的中奖济洛路 Get
    @JvmField
    val LOTTERY_INFO_LIST = "${Constants.BASE_URL}views/gift_box/my_open_record"

    //私聊页面钥匙弹窗状态上报
    @JvmField
    val P2P_SHOW_KEY_DIALOG = "${Constants.BASE_URL}views/gift_box/del_key_popup"

    @JvmField
    //首页：每日登陆获得钥匙数量
    val MAIN_PAGE_GOT_KEY_STATUS = "${Constants.BASE_URL}views/gift_box/set_key"

    //中奖通告
    @JvmField
    val  WINNING_PRIZE_NOTICES = "${Constants.BASE_URL}views/gift_box/all_open_record"
    //拆礼盒弹窗页面信息
    @JvmField
    val  OPEN_GIFT_BOX_INFO = "${Constants.BASE_URL}views/gift_box/detail"
    //拆礼盒
    @JvmField
    val  OPEN_GIFT_BOX = "${Constants.BASE_URL}views/gift_box/open"

    @JvmField
    val SALE_CARD_LIST = "${Constants.BASE_URL}couple/sale/card/list"

    @JvmField
    val BUY_CP_CARD = "${Constants.BASE_URL}couple/buy_cp_card"

    //实名认证
    @JvmField
    val REAL_NAME_ID_CARD = "${Constants.BASE_URL}user/real_name_id_card"
    //提现详情
    @JvmField
    val GET_WITHDRAWAL_INFO = "${Constants.BASE_URL}views/withdraw/get_withdraw_info"
    //绑定微信
    @JvmField
    val BIND_WECHAT = "${Constants.BASE_URL}user/bind_wechat"
    //发起提现
    @JvmField
    val USER_WITHDRAW = "${Constants.BASE_URL}views/withdraw/user_withdraw"
    //提现记录
    @JvmField
    val WITHDRAWAL_HISTORY = "${Constants.BASE_URL}views/withdraw/get_user_balance_record"
    //背包详情
    @JvmField
    val GIFT_BAG = "${Constants.BASE_URL}views/gift_bag/mine"
    //背包礼物赠送&合成
    @JvmField
    val GIFT_BAG_SEND_GIFT = "${Constants.BASE_URL}views/gift_bag/use"
    ///一键送出背包礼物
    @JvmField
    val GIFT_BAG_SEND_GIFT_FAST = "${Constants.BASE_URL}views/gift_bag/use_by_detail"
    //个人中心信息
    @JvmField
    val USER_INFO_OTHER = "${Constants.BASE_URL}views/user/info/other"
    //用户照片
    @JvmField
    val USER_PHOTOS = "${Constants.BASE_URL}user/photos"

    //主持人评价子选项
    @JvmField
    val GET_APPRAISE_ANCHOR_ITEMS = "${Constants.BASE_URL}views/video_room/appraise_info"

    //视频房 上传 主持人的评价选项
    @JvmField
    val UPDATE_APPRAISE_ANCHOR_ITEMS = "${Constants.BASE_URL}views/video_room/appraise"
    //踢下麦操作，用于C端用户异常下麦后踢自己下麦的操作
    @JvmField
    val ROOM_KICKOUT_MIC = "${Constants.BASE_URL}couple/room_kickout_mic"
    //cp房礼物排行榜
    @JvmField
    val CP_GIFT_RANK_LIST = "${Constants.BASE_URL}views/video_room/vm_ranking_list"
    //主持人助力 - 助力礼物详情
    val GIFT_BOOST_INFO = "${Constants.BASE_URL}boost/gift_boost_info"
    //主持人助力 - 助力抽奖详情
    val BOOST_LOTTERY_INFO = "${Constants.BASE_URL}boost/boost_lottery_info"
    //主持人助力 - 助力中奖记录
    val BOOST_LOTTERY_RECORD = "${Constants.BASE_URL}boost/boost_lottery_record"
    //主持人助力 - 助力抽奖
    val OPEN_BOOST_LOTTERY = "${Constants.BASE_URL}boost/open_lottery"
    //获取个人金币余额
    @JvmField
    val  GET_USER_COIN = "${Constants.BASE_URL}user/rest_coin"
    //视频速配 - 获取视频速配信息
    @JvmField
    val  GET_VIDEO_MATCH_INFO = "${Constants.BASE_URL}views/video_match/get_video_match_room"
    //视频速配 - 男用户下麦
    @JvmField
    val  CANCEL_VIDEO_MATCH_MIC = "${Constants.BASE_URL}views/video_match/cancel_video_match_mic"
    //视频速配 - 男用户上麦
    @JvmField
    val  REQUEST_VIDEO_MATCH_MIC = "${Constants.BASE_URL}views/video_match/video_on_mic"
    //视频速配 - 拒绝视频call
    @JvmField
    val  REFUSE_VIDEO_MATCH_MIC = "${Constants.BASE_URL}views/video_match/video_call/refuse"
    //视频速配 - check 速配接口
    @JvmField
    val  CHECK_VIDEO_MATCH_MIC = "${Constants.BASE_URL}views/video_match/get_room_members"
    //盲盒 - 中奖记录
    @JvmField
    val  BLIND_BOX_RECORD = "${Constants.BASE_URL}gift/blind_box_record"

    /**
     * 1v1视频列表
     */
    @JvmField
    val SOLO_VIDEO_LIST_REQUIRE = "${Constants.BASE_URL}views/video_match/room_list"

    /**
     * 1v1视频call
     */
    @JvmField
    val SOLO_VIDEO_CALL = "${Constants.BASE_URL}views/video_match/video_call/request"

    /**
     * CP等级信息
     */
    @JvmField
    val CP_LEVEL = "${Constants.BASE_URL}views/cp_score/detail"

    /**
     * 登出
     */
    @JvmField
    val LOGOUT = "${Constants.BASE_URL}user/logout"
    /**
     * 小游戏组队
     */
    @JvmField
    val CREATE_GAME_TEAM = "${Constants.BASE_URL}views/cp_score/detail11111111111"

    /**
     * 小游戏准备
     */
    @JvmField
    val READY_GAME_TEAM = "${Constants.BASE_URL}views/cp_score/detail11111111111"

    /**
     * 小游戏取消
     */
    @JvmField
    val CANCEL_GAME_TEAM = "${Constants.BASE_URL}views/cp_score/detail11111111111"

    /**
     * 小游戏开始
     */
    @JvmField
    val START_GAME = "${Constants.BASE_URL}views/cp_score/detail11111111111"

    /**
     * 超时邀请
     */
    @JvmField
    val INVITE_GAME = "${Constants.BASE_URL}views/cp_score/detail11111111111"
}
