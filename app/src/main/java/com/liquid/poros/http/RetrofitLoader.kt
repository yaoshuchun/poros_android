package com.liquid.poros.http

import com.liquid.base.retrofit.monitor.HttpMonitor
import com.liquid.base.utils.GlobalConfig
import com.liquid.library.retrofitx.RetrofitX.Companion.instance
import com.liquid.poros.BuildConfig
import com.liquid.poros.PorosApplication
import com.liquid.poros.http.interceptor.HeaderInterceptor
import com.liquid.poros.http.interceptor.UrlCommonParamsInterceptor
import okhttp3.logging.HttpLoggingInterceptor
import java.io.File

/**
 * RetrofitX 加载器
 */
object RetrofitLoader {
    /**
     * 初始化网络请求框架
     */
    @JvmStatic
    fun initRetrofitX(application: PorosApplication) {
        val retrofitX = instance
                .debug(BuildConfig.DEBUG)
                .setConnectTimeout(7000)
                .setReadTimeout(7000)
                .setWriteTimeout(10000)
                .addInterceptor(HeaderInterceptor())
                .addInterceptor(UrlCommonParamsInterceptor())
        if (GlobalConfig.LOG_ENABLE) {
            retrofitX.addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
        }
        retrofitX.addInterceptor(HttpMonitor())
                .setCache(File(application.externalCacheDir, "poros"), 10 * 1024 * 1024)
                .init()
    }
}