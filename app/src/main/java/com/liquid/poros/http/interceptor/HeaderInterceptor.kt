package com.liquid.poros.http.interceptor;
import okhttp3.Interceptor
import okhttp3.Response

/**
 * @author guomenglong
 * 请求头Interceptor
 */
class HeaderInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
                .newBuilder() //                    .addHeader("User-Agent", AppConfig.instance().userAgent)
                .addHeader("Accept-Charset", "UTF-8") //                    .addHeader("Accept-Encoding", "gzip")//官方解释：添加自己的 Accept-Encoding 头信息时, OkHttp会认为您要自己处理解压缩步骤. 删除这个头信息后, OkHttp 会自动处理加头信息和解压缩的步骤.
                .addHeader("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8")
                .addHeader("Connection", "keep-alive")
                .addHeader("Accept", "*/*") //                    .addHeader("Cookie", AppBoxCookie.getCookie())
                .build()
        return chain.proceed(request)
    }
}