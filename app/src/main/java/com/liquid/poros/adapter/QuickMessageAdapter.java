package com.liquid.poros.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.liquid.poros.R;
import com.liquid.poros.helper.TimeUtil;

import java.util.List;

public class QuickMessageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<String> mData;
    private Context mContext;

    public QuickMessageAdapter(Context context, List<String> data) {
        mData = data;
        mContext = context;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.input_item_quick_message, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        viewHolder.item_tv_quick_message.setText(mData.get(position));
        viewHolder.item_tv_quick_message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TimeUtil.isFastClick()) {
                    return;
                }
                if (OnListener != null) {
                    OnListener.onItemClick(position);
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return mData.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView item_tv_quick_message;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            item_tv_quick_message = itemView.findViewById(R.id.item_tv_quick_message);
        }
    }


    private QuickMessageAdapter.OnListener OnListener;

    public void setOnItemListener(QuickMessageAdapter.OnListener onListener) {
        this.OnListener = onListener;
    }

    public interface OnListener {
        void onItemClick(int pos);
    }
}