package com.liquid.poros.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.ImageSpan;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.request.RequestOptions;
import com.liquid.base.adapter.CommonViewHolder;
import com.liquid.base.adapter.MultiItemCommonAdapter;
import com.liquid.base.adapter.MultiItemTypeSupport;
import com.liquid.base.guideview.DimenUtil;
import com.liquid.im.kit.custom.JoinRoomAttachment;
import com.liquid.im.kit.custom.RoomChatAttachment;
import com.liquid.poros.R;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.base.utils.LogOut;
import com.liquid.poros.utils.MoonUtil;
import com.liquid.poros.utils.ScreenUtil;
import com.liquid.poros.utils.StringUtil;
import com.liquid.poros.widgets.RoundBackgroundColorSpan;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessageExtension;
import com.netease.nimlib.sdk.msg.attachment.MsgAttachment;

import java.util.Map;

public class ChatRoomMessageAdapter extends MultiItemCommonAdapter<ChatRoomMessage> {

    private static final int JOIN_ROOM_TYPE = 1;
    private static final int NORMAL_TEXT_TYPE = 2;

    public ChatRoomMessageAdapter(Context context) {
        super(context, new MultiItemTypeSupport<ChatRoomMessage>() {
            @Override
            public int getLayoutId(int itemType) {
                int resId = R.layout.message_item_user_text;
                switch (itemType) {
                    case NORMAL_TEXT_TYPE:
                        resId = R.layout.message_item_user_text;
                        break;
                    case JOIN_ROOM_TYPE:
                        resId = R.layout.message_item_user_join_room;
                        break;
                }
                return resId;
            }

            @Override
            public int getItemViewType(int position, ChatRoomMessage chatRoomMessage) {
                if (chatRoomMessage.getAttachment() instanceof JoinRoomAttachment) {
                    return JOIN_ROOM_TYPE;
                }
                return NORMAL_TEXT_TYPE;
            }
        });

    }

    @Override
    public void convert(CommonViewHolder holder, ChatRoomMessage chatRoomMessage, int pos) {
        if (getItemViewType(pos) == JOIN_ROOM_TYPE) {
            JoinRoomAttachment attachment = (JoinRoomAttachment) chatRoomMessage.getAttachment();
            TextView message_item_content = holder.getView(R.id.message_item_content);

            SpannableStringBuilder builder = new SpannableStringBuilder();

            //如果是guangfang账号
            if (attachment.isSuperAdmin()){
                Drawable drawable = ContextCompat.getDrawable(mContext,R.mipmap.icon_message_guang_fang);
                drawable.setBounds(0,0, DimenUtil.dp2px(mContext,40),DimenUtil.dp2px(mContext,20));
                ImageSpan span = new ImageSpan(drawable,ImageSpan.ALIGN_BASELINE);
                SpannableString spannableString  = new SpannableString(" " + " ");
                spannableString.setSpan(span,0,1,Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                builder.append(spannableString);
            }else if (attachment.getUser_level() != -1){
                //用户等级
                SpannableString userLevelSpan=new SpannableString("Lv." + attachment.getUser_level());
                userLevelSpan.setSpan(new RoundBackgroundColorSpan(mContext, Color.parseColor("#12DBD1"),Color.parseColor("#FFFFFF")), 0, userLevelSpan.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                userLevelSpan.setSpan(new AbsoluteSizeSpan(10, true), 0, userLevelSpan.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                builder.append(userLevelSpan);
            }

            //用户名字
            SpannableString spannableString = new SpannableString(attachment.getNick());
            ForegroundColorSpan colorSpan = new ForegroundColorSpan(Color.parseColor("#FFFFFF"));
            spannableString.setSpan(colorSpan, 0, (attachment.getNick()).length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            spannableString.setSpan(new AbsoluteSizeSpan(14, true), 0, (attachment.getNick()).length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            builder.append(spannableString);
            //正式文本
            SpannableString contentString = new SpannableString("进入房间");
            contentString.setSpan(new ForegroundColorSpan(Color.parseColor("#10D8C8")), 0, "进入房间".length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            contentString.setSpan(new AbsoluteSizeSpan(14, true), 0, "进入房间".length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            builder.append(contentString);
            //设置文字
            message_item_content.setText(builder);
            //设置图片
            RequestOptions options = new RequestOptions()
                    .placeholder(R.mipmap.img_default_avatar)
                    .apply(RequestOptions.bitmapTransform(new CircleCrop()))
                    .override(ScreenUtil.dip2px(24), ScreenUtil.dip2px(24))
                    .error(R.mipmap.img_default_avatar);
            ImageView iv_head = holder.getView(R.id.iv_head);
            Glide.with(mContext)
                    .load(getUserHead(chatRoomMessage))
                    .apply(options)
                    .into(iv_head);
        } else {
            TextView message_item_content = holder.getView(R.id.message_item_content);
            SpannableStringBuilder builder = new SpannableStringBuilder();

            Map<String, Object> map = chatRoomMessage.getRemoteExtension();
            if(map != null){
                int user_level = (int) map.get("user_level");
                boolean isSuperAdmin = false;
                try {
                    isSuperAdmin = (boolean) map.get("is_super_admin");
                }catch (Exception e){
                }
                //如果是guangfang账号
                if (isSuperAdmin){

                    Drawable drawable = ContextCompat.getDrawable(mContext,R.mipmap.icon_message_guang_fang);
                    drawable.setBounds(0,0, DimenUtil.dp2px(mContext,40),DimenUtil.dp2px(mContext,20));
                    ImageSpan span = new ImageSpan(drawable,ImageSpan.ALIGN_BASELINE);
                    SpannableString spannableString  = new SpannableString(" " + " ");
                    spannableString.setSpan(span,0,1,Spanned.SPAN_INCLUSIVE_EXCLUSIVE);

                    builder.append(spannableString);
                }else if (user_level != -1){
                    //用户等级
                    SpannableString userLevelSpan=new SpannableString("Lv." + user_level);
                    userLevelSpan.setSpan(new RoundBackgroundColorSpan(mContext, Color.parseColor("#12DBD1"),Color.parseColor("#FFFFFF")), 0, userLevelSpan.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    userLevelSpan.setSpan(new AbsoluteSizeSpan(10, true), 0, userLevelSpan.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    builder.append(userLevelSpan);
                }
            }
            //用户名字
            SpannableString spannableString = new SpannableString(getNameText(chatRoomMessage));
            ForegroundColorSpan colorSpan = new ForegroundColorSpan(Color.parseColor("#aaFFFFFF"));
            spannableString.setSpan(colorSpan, 0, (getNameText(chatRoomMessage)).length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            spannableString.setSpan(new AbsoluteSizeSpan(14, true), 0, (getNameText(chatRoomMessage)).length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            builder.append(spannableString);
            //正式文本
            if (StringUtil.isNotEmpty(chatRoomMessage.getContent())){
                SpannableString contentString = new SpannableString(chatRoomMessage.getContent());
                contentString.setSpan(new ForegroundColorSpan(Color.parseColor("#ffffff")), 0, chatRoomMessage.getContent().length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                contentString.setSpan(new AbsoluteSizeSpan(14, true), 0, chatRoomMessage.getContent().length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                builder.append(contentString);
            }
            //设置文字
            message_item_content.setText(builder);
            //设置图片
            ImageView iv_head = holder.getView(R.id.iv_head);
            RequestOptions options = new RequestOptions()
                    .placeholder(R.mipmap.img_default_avatar)
                    .apply(RequestOptions.bitmapTransform(new CircleCrop()))
                    .override(ScreenUtil.dip2px(24), ScreenUtil.dip2px(24))
                    .error(R.mipmap.img_default_avatar);
            Glide.with(mContext)
                    .load(getUserHead(chatRoomMessage))
                    .apply(options)
                    .into(iv_head);
        }
    }

    private String getUserHead(ChatRoomMessage chatRoomMessage) {
        String path = "";
        if (chatRoomMessage.getChatRoomMessageExtension() != null) {
            path = chatRoomMessage.getChatRoomMessageExtension().getSenderAvatar();
        }
        if (StringUtil.isEquals(chatRoomMessage.getFromAccount(),AccountUtil.getInstance().getUserId())){
            path = AccountUtil.getInstance().getUserInfo().getAvatar_url();
        }
        return path;
    }

    protected String getNameText(ChatRoomMessage message) {
        if (message.getChatRoomMessageExtension() != null && !TextUtils.isEmpty(message.getChatRoomMessageExtension().getSenderNick())) {
            return message.getChatRoomMessageExtension().getSenderNick() + "：";
        } else {
            return message.getFromNick() + "：";
        }
    }
}
