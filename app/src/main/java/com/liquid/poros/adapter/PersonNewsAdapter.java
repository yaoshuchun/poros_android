package com.liquid.poros.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.media.MediaPlayer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.liquid.poros.R;
import com.liquid.poros.analytics.utils.MultiTracker;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.entity.SingleCommentBean;
import com.liquid.poros.helper.JinquanResourceHelper;
import com.liquid.poros.ui.activity.FeedDetailActivity;
import com.liquid.poros.utils.StringUtil;
import com.liquid.poros.utils.ViewUtils;
import com.liquid.poros.utils.glide.GlideEngine;
import com.liquid.poros.widgets.MultiImageView;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.entity.LocalMedia;
import com.xiaomi.push.T;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static com.liquid.poros.ui.activity.FeedDetailActivity.COMMENT_MODE;
import static com.liquid.poros.ui.activity.FeedDetailActivity.FEED_ID;
import static com.liquid.poros.ui.activity.FeedDetailActivity.GUEST_ID;
import static com.liquid.poros.ui.activity.FeedDetailActivity.LAUNCH_MODE;
import static com.liquid.poros.ui.activity.FeedDetailActivity.LOOK_MODE;

public class PersonNewsAdapter extends RecyclerView.Adapter<PersonNewsAdapter.PersonNewsViewHolder> {
    private Activity mContext;
    private List<SingleCommentBean> datas = new ArrayList<>();
    private RoundedCorners roundedCorners;
    private RequestOptions options;
    private MediaPlayer mMediaPlayer;
    private OnPersonNewsClickListener listener;
    private Timer mTimer2;
    private TimerTask mTask2;
    private String sessionId;
    private String feedDetailsType;
    private boolean isPause = false;//是否暂停

    public PersonNewsAdapter(Context context) {
        mContext = (Activity) context;
        roundedCorners = new RoundedCorners(100);
        options = new RequestOptions()
                .placeholder(R.mipmap.icon_default)
                .error(R.mipmap.icon_default)
                .transform(new CenterCrop(), roundedCorners);
        mMediaPlayer = new MediaPlayer();
    }

    //废弃该方法，不再传引用
//    public void initData(List<SingleCommentBean> data) {
//        datas = data;
//    }

    public void setDatas(List<SingleCommentBean> data) {
        if (datas != null) {
            datas.addAll(data);
            notifyDataSetChanged();
        }
    }

    public void update(List<SingleCommentBean> data) {
        if (data != null) {
            datas.clear();
            datas.addAll(data);
            notifyDataSetChanged();
        }
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    /**
     * 设置动态详情来源
     *
     * @param feedDetailsType
     */
    public void setFeedDetailsType(String feedDetailsType) {
        this.feedDetailsType = feedDetailsType;
    }


    public void setListener(OnPersonNewsClickListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public PersonNewsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflate = LayoutInflater.from(mContext).inflate(R.layout.item_person_news, parent, false);
        return new PersonNewsViewHolder(inflate);
    }

    /**
     * 暂停语音
     */
    private void endAudio(ImageView sound_news_anim, TextView sound_new_time, SingleCommentBean commentBean) {
        if (sound_news_anim == null) {
            return;
        }
        if (animation != null) {
            animation.selectDrawable(0);
            animation.stop();
        }
        commentBean.mAudioTimer = commentBean.voice_time;
        setAudioDuration(sound_new_time, commentBean);
        releaseTimer();
    }

    private void setAudioDuration(TextView soundNewTime, SingleCommentBean commentBean) {
        if (soundNewTime != null) {
            soundNewTime.setText(commentBean.mAudioTimer + "s");
        }
    }

    private void releaseTimer() {
        if (mTask2 != null) {
            mTask2.cancel();
            mTask2 = null;
        }
        if (mTimer2 != null) {
            mTimer2.cancel();
            mTimer2 = null;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull PersonNewsViewHolder holder, int position) {
        SingleCommentBean commentBean = datas.get(position);
        Glide.with(mContext).load(commentBean.avatar_url).apply(options).into(holder.personAvatar);
        if (commentBean.like_count == 0) {
            holder.likeCount.setText("点赞");
        } else {
            holder.likeCount.setText(commentBean.like_count + "");
        }
        if (commentBean.comment_count == 0) {
            holder.commentCount.setText("评论");
        } else {
            holder.commentCount.setText(commentBean.comment_count + "");
        }
        JinquanResourceHelper.getInstance(mContext).setTextColorByAttr(holder.commentCount, R.attr.custom_attr_nickname_text_color);
        holder.newsUserName.setText(commentBean.nick_name);
        JinquanResourceHelper.getInstance(mContext).setTextColorByAttr(holder.newsUserName, R.attr.custom_attr_nickname_text_color);
        holder.newsTime.setText(commentBean.create_time);
        JinquanResourceHelper.getInstance(mContext).setTextColorByAttr(holder.newsTime, R.attr.custom_attr_flow_item_text_color);
        if (!StringUtil.isEmpty(commentBean.voice)) {
            holder.soundNews.setVisibility(View.VISIBLE);
            try {
                mMediaPlayer.reset();
                mMediaPlayer.setDataSource(commentBean.voice);
                mMediaPlayer.prepare();
                commentBean.mAudioTimer = commentBean.voice_time;
                setAudioDuration(holder.soundNewsTime, commentBean);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            holder.soundNews.setVisibility(View.GONE);
        }
        if (commentBean.liked) {
            holder.likeTag.setImageResource(R.mipmap.ic_like_selected);
            holder.likeCount.setTextColor(Color.parseColor("#F06078"));
        } else {
            JinquanResourceHelper.getInstance(mContext).setImageResourceByAttr(holder.likeTag, R.attr.bg_like);
            JinquanResourceHelper.getInstance(mContext).setTextColorByAttr(holder.likeCount, R.attr.custom_attr_nickname_text_color);
        }
        if (StringUtil.isEmpty(commentBean.text)) {
            holder.textNews.setVisibility(View.GONE);
        } else {
            holder.textNews.setVisibility(View.VISIBLE);
            holder.textNews.setText(commentBean.text);
        }
        JinquanResourceHelper.getInstance(mContext).setTextColorByAttr(holder.textNews, R.attr.custom_attr_nickname_text_color);
        JinquanResourceHelper.getInstance(mContext).setImageResourceByAttr(holder.commentTag, R.attr.bg_comment);
        holder.mu_image.setList(commentBean.images);
        holder.mu_image.setOnItemClickListener(new MultiImageView.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (ViewUtils.isFastClick()) {
                    return;
                }
                PictureSelector.create((Activity) mContext)
                        .themeStyle(R.style.picture_default_style)
                        .isNotPreviewDownload(true)
                        .isPreviewVideo(true)
                        .imageEngine(GlideEngine.createGlideEngine())
                        .openExternalPreview(position, getSelectList(commentBean.images));
                if (listener != null) {
                    listener.pictureAction(commentBean.feed_id);
                }
                HashMap<String, String> params = new HashMap<>();
                params.put("female_guest_id", sessionId);
                params.put("moment_id", commentBean.feed_id);
                params.put("create_ts", String.valueOf(System.currentTimeMillis()));
                params.put("from", Constants.SOURCE_FEED_SQUARE_DETAIL.equals(feedDetailsType) ? Constants.TYPE_FEED_SQUARE : Constants.TYPE_FEED_PERSONAL);
                MultiTracker.onEvent(TrackConstants.B_FEED_PIC_CLICK, params);
            }
        });
        holder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mIntent = new Intent(mContext, FeedDetailActivity.class);
                mIntent.putExtra(LAUNCH_MODE, LOOK_MODE);
                mIntent.putExtra(FEED_ID, commentBean.feed_id);
                mIntent.putExtra(GUEST_ID, sessionId);
                mIntent.putExtra(Constants.FEED_DETAILS_TYPE, feedDetailsType);
                mContext.startActivity(mIntent);
                HashMap<String, String> params = new HashMap<>();
                params.put("female_guest_id", sessionId);
                params.put("moment_id", commentBean.feed_id);
                params.put("create_ts", String.valueOf(System.currentTimeMillis()));
                params.put("launch_feed_type", Constants.LAUNCH_FEED_TYPE_ITEM);
                params.put("from", Constants.SOURCE_FEED_SQUARE_DETAIL.equals(feedDetailsType) ? Constants.TYPE_FEED_SQUARE : Constants.TYPE_FEED_PERSONAL);
                MultiTracker.onEvent(TrackConstants.B_FEED_MOMENT_CLICK, params);
            }
        });
        holder.commentTag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mIntent = new Intent(mContext, FeedDetailActivity.class);
                mIntent.putExtra(LAUNCH_MODE, COMMENT_MODE);
                mIntent.putExtra(FEED_ID, commentBean.feed_id);
                mIntent.putExtra(GUEST_ID, sessionId);
                mIntent.putExtra(Constants.FEED_DETAILS_TYPE, feedDetailsType);
                mContext.startActivity(mIntent);
                HashMap<String, String> params = new HashMap<>();
                params.put("female_guest_id", sessionId);
                params.put("moment_id", commentBean.feed_id);
                params.put("create_ts", String.valueOf(System.currentTimeMillis()));
                params.put("launch_feed_type", Constants.LAUNCH_FEED_TYPE_COMMENT);
                params.put("from", Constants.SOURCE_FEED_SQUARE_DETAIL.equals(feedDetailsType) ? Constants.TYPE_FEED_SQUARE : Constants.TYPE_FEED_PERSONAL);
                MultiTracker.onEvent(TrackConstants.B_FEED_MOMENT_CLICK, params);
            }
        });
        holder.likeTag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ViewUtils.isFastClick()) return;
                if (commentBean.liked) {
                    if (listener != null) {
                        commentBean.like_count--;
                        if (commentBean.like_count == 0) {
                            holder.likeCount.setText("点赞");
                        } else {
                            holder.likeCount.setText(commentBean.like_count + "");
                        }
                        commentBean.liked = false;
                        JinquanResourceHelper.getInstance(mContext).setImageResourceByAttr(holder.likeTag, R.attr.bg_like);
                        JinquanResourceHelper.getInstance(mContext).setTextColorByAttr(holder.likeCount, R.attr.custom_attr_nickname_text_color);
                        listener.like(commentBean.feed_id, false);
                    }
                } else {
                    if (listener != null) {
                        commentBean.like_count++;
                        commentBean.liked = true;
                        holder.likeTag.setImageResource(R.mipmap.ic_like_selected);
                        holder.likeCount.setText(commentBean.like_count + "");
                        holder.likeCount.setTextColor(Color.parseColor("#F06078"));
                        listener.like(commentBean.feed_id, true);
                    }
                }
                HashMap<String, String> params = new HashMap<>();
                params.put("female_guest_id", sessionId);
                params.put("moment_id", commentBean.feed_id);
                params.put("create_ts", String.valueOf(System.currentTimeMillis()));
                params.put("from", Constants.SOURCE_FEED_SQUARE_DETAIL.equals(feedDetailsType) ? Constants.TYPE_FEED_SQUARE : Constants.TYPE_FEED_PERSONAL);
                MultiTracker.onEvent(TrackConstants.B_FEED_LIKE_CLICK, params);
            }
        });
        holder.soundNews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mMediaPlayer.isPlaying() && !isPause) {
                    isPause = true;
                    endAudio(holder.soundNewsAnim, holder.soundNewsTime, commentBean);
                } else {
                    isPause = false;
                    startAudio(holder.soundNewsAnim, holder.soundNewsTime, commentBean.voice, commentBean);
                }
                if (listener != null) {
                    listener.voiceAction(commentBean.feed_id);
                }
                HashMap<String, String> params = new HashMap<>();
                params.put("female_guest_id", sessionId);
                params.put("moment_id", commentBean.feed_id);
                params.put("create_ts", String.valueOf(System.currentTimeMillis()));
                params.put("from", Constants.SOURCE_FEED_SQUARE_DETAIL.equals(feedDetailsType) ? Constants.TYPE_FEED_SQUARE : Constants.TYPE_FEED_PERSONAL);
                MultiTracker.onEvent(TrackConstants.B_FEED_VOICE_CLICK, params);
            }
        });
    }

    private List<LocalMedia> getSelectList(List<String> data) {
        List<LocalMedia> localMedia = new ArrayList<>();
        for (String photos : data) {
            localMedia.add(new LocalMedia(photos, 0, 0, ""));
        }
        return localMedia;
    }

    /**
     * 播放语音
     */
    private AnimationDrawable animation;

    private void startAudio(ImageView sound_news_anim, TextView sound_new_time, String voice, SingleCommentBean commentBean) {
        sound_news_anim.setBackgroundResource(R.drawable.anim_user_audio_list);
        animation = (AnimationDrawable) sound_news_anim.getBackground();
        animation.start();
        if (!StringUtil.isEmpty(voice) && mMediaPlayer != null) {
            try {
                mMediaPlayer.reset();
                mMediaPlayer.setDataSource(voice);
                mMediaPlayer.prepare();
                mMediaPlayer.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (mTimer2 == null && mTask2 == null) {
            mTimer2 = new Timer();
            mTask2 = new TimerTask() {
                @Override
                public void run() {
                    mContext.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            commentBean.mAudioTimer--;
                            if (commentBean.mAudioTimer <= 0) {
                                commentBean.mAudioTimer = commentBean.voice_time;
                                endAudio(sound_news_anim, sound_new_time, commentBean);
                            } else {
                                setAudioDuration(sound_new_time, commentBean);
                            }
                        }
                    });
                }
            };
            mTimer2.schedule(mTask2, 0, 1000);
        }
    }

    @Override
    public int getItemCount() {
        return datas == null ? 0 : datas.size();
    }

    public static class PersonNewsViewHolder extends RecyclerView.ViewHolder {
        View item;
        ImageView personAvatar;
        TextView newsUserName;
        TextView newsTime;
        RelativeLayout soundNews;
        ImageView soundNewsAnim;
        TextView soundNewsTime;
        TextView textNews;
        ImageView commentTag;
        TextView commentCount;
        ImageView likeTag;
        TextView likeCount;
        MultiImageView mu_image;

        public PersonNewsViewHolder(@NonNull View itemView) {
            super(itemView);
            item = itemView;
            personAvatar = itemView.findViewById(R.id.person_avatar);
            newsUserName = itemView.findViewById(R.id.news_user_name);
            newsTime = itemView.findViewById(R.id.news_time);
            soundNews = itemView.findViewById(R.id.sound_news);
            soundNewsAnim = itemView.findViewById(R.id.sound_news_anim);
            soundNewsTime = itemView.findViewById(R.id.sound_new_time);
            textNews = itemView.findViewById(R.id.text_news);
            commentTag = itemView.findViewById(R.id.iv_comment_tag);
            commentCount = itemView.findViewById(R.id.comment_count);
            likeTag = itemView.findViewById(R.id.iv_like_tag);
            likeCount = itemView.findViewById(R.id.like_count);
            mu_image = itemView.findViewById(R.id.mu_image);
        }
    }


    public interface OnPersonNewsClickListener {
        void like(String feedLike, boolean isLike);

        //语音播放操作上报
        void voiceAction(String feedLike);

        //查看图片操作上报
        void pictureAction(String feedLike);
    }
}
