package com.liquid.poros.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Animatable;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.text.style.ImageSpan;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.liquid.base.adapter.CommonViewHolder;
import com.liquid.base.adapter.MultiItemCommonAdapter;
import com.liquid.base.adapter.MultiItemTypeSupport;
import com.liquid.im.kit.bean.GiftBean;
import com.liquid.im.kit.custom.AskForGiftAttachment;
import com.liquid.im.kit.custom.AskPTAGiftAttachment;
import com.liquid.im.kit.custom.AudioMatchAttachment;
import com.liquid.im.kit.custom.BoxOpenInviteAttachment;
import com.liquid.im.kit.custom.BuyCpTipsAttachment;
import com.liquid.im.kit.custom.CoinNotEnoughAttachment;
import com.liquid.im.kit.custom.CpLevelUpInviteAttach;
import com.liquid.im.kit.custom.CpLevelUpSuccessAttach;
import com.liquid.im.kit.custom.GetGiftBoxSpecialHintAttachment;
import com.liquid.im.kit.custom.GiveRewardAttachment;
import com.liquid.im.kit.custom.GuardCpAttachment;
import com.liquid.im.kit.custom.InviteCoupleAttachment;
import com.liquid.im.kit.custom.InviteCoupleGameAttachment;
import com.liquid.im.kit.custom.RichMicAttachment;
import com.liquid.im.kit.custom.SendGiftAttachment;
import com.liquid.im.kit.custom.SendPTAGiftAttachment;
import com.liquid.im.kit.custom.StickerAttachment;
import com.liquid.im.kit.custom.VideoAdAttachment;
import com.liquid.im.kit.emoji.StickerManager;
import com.liquid.poros.PorosApplication;
import com.liquid.poros.R;
import com.liquid.poros.analytics.utils.MultiTracker;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.constant.TrackConstants;
import com.liquid.poros.helper.AudioPlayerHelper;
import com.liquid.poros.helper.JinquanResourceHelper;
import com.liquid.poros.helper.TimeUtil;
import com.liquid.poros.ui.dialog.BugVIPDialogFragment;
import com.liquid.poros.ui.dialog.BuyVIPDialogFragment;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.poros.utils.MoonUtil;
import com.liquid.poros.utils.ScreenUtil;
import com.liquid.poros.utils.StringUtil;
import com.liquid.poros.utils.ViewUtils;
import com.liquid.poros.utils.glide.GlideEngine;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.entity.LocalMedia;
import com.netease.nim.uikit.business.session.custom.CupidSysAttachment;
import com.netease.nim.uikit.business.session.custom.SendCupidAttachment;
import com.netease.nimlib.sdk.msg.attachment.AudioAttachment;
import com.netease.nimlib.sdk.msg.attachment.FileAttachment;
import com.netease.nimlib.sdk.msg.attachment.ImageAttachment;
import com.netease.nimlib.sdk.msg.constant.AttachStatusEnum;
import com.netease.nimlib.sdk.msg.constant.MsgDirectionEnum;
import com.netease.nimlib.sdk.msg.constant.MsgStatusEnum;
import com.netease.nimlib.sdk.msg.constant.MsgTypeEnum;
import com.netease.nimlib.sdk.msg.constant.SessionTypeEnum;
import com.netease.nimlib.sdk.msg.model.IMMessage;
import com.netease.nimlib.sdk.uinfo.model.UserInfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import androidx.constraintlayout.widget.Group;
import androidx.fragment.app.FragmentActivity;

import static com.liquid.poros.constant.Constants.IS_VIP_MSG;

public class SessionMessageAdapter extends MultiItemCommonAdapter<IMMessage> {
    private static final int UNKNOWN_TYPE = -1;
    private static final int TEXT_TYPE = 1;
    private static final int AUDIO_TYPE = 2;
    private static final int COUPLE_TYPE = 3;
    private static final int COUPLE_BUY_TYPE = 4;
    private static final int IMAGE_TYPE = 5;
    private static final int COUPLE_GAME_TYPE = 6;
    private static final int STICKER_TYPE = 7;
    private static final int SYSTEM_NOTICE_TYPE = 8;
    private static final int COIN_NOT_ENOUGH_TYPE = 9;
    private static final int GUARD_COUPLE_TYPE = 10;
    private static final int RICH_MIC_TYPE = 11;
    private static final int BUY_CP_TIP_TYPE = 12;
    private static final int SEND_GIFT_CARD_TYPE = 13;
    private static final int CP_LEVEL_UP_TYPE = 14;
    private static final int CP_LEVEL_UP_SUCCESS_TYPE = 15;
    private static final int SHOW_AD_VIDEO_TYPE = 16;
    private static final int CP_ASK_PTA_GIFT = 17;
    private static final int CP_SEND_PTA_GIFT = 18;
    private static final int AUDIO_MATCH_TYPE = 19;
    private static final int GET_GIFT_BOX_SPECIAL_HINT = 20;
    private static final int INVITE_GIFT_BOX = 21;
    private static final int SEND_CUPID_MSG = 22;
    private static final int CUPID_SYS_MSG = 23;
    private static final int ASK_FOR_GIFT = 24;
    private final String TAG_WANGZHE = "wzry";
    private final String TAG_HEPING = "hpjy";
    private final String TAG_CHAT = "chat";
    private AudioPlayerHelper mAudioHelper;
    private MessageClickListener mListener;
    private boolean isExecute = false;
    private String room_id;

    public void setIsExecute(boolean isExecute) {
        this.isExecute = isExecute;
    }

    public void setRoom_id(String room_id) {
        this.room_id = room_id;
    }

    private RequestOptions headOptions = new RequestOptions()
            .placeholder(R.mipmap.img_default_avatar)
            .transform(new CircleCrop())
            .error(R.mipmap.img_default_avatar);

    public SessionMessageAdapter(Context context, MessageClickListener listener) {
        super(context, new MultiItemTypeSupport<IMMessage>() {
            @Override
            public int getLayoutId(int itemType) {
                int resId = R.layout.session_message_item_user_unknown;
                switch (itemType) {
                    case UNKNOWN_TYPE:
                        resId = R.layout.session_message_item_user_unknown;
                        break;
                    case TEXT_TYPE:
                        resId = R.layout.session_message_item_user_text;
                        break;
                    case CUPID_SYS_MSG:
                        resId = R.layout.session_message_item_cupid_sys;
                        break;
                    case SEND_CUPID_MSG:
                        resId = R.layout.session_message_item_send_cupid;
                        break;
                    case COUPLE_TYPE:
                        resId = R.layout.session_message_item_user_couple;
                        break;
                    case COUPLE_BUY_TYPE:
                        resId = R.layout.session_message_item_buy_couple;
                        break;
                    case AUDIO_TYPE:
                        resId = R.layout.session_message_item_user_audio;
                        break;
                    case IMAGE_TYPE:
                        resId = R.layout.session_message_item_user_image;
                        break;
                    case COUPLE_GAME_TYPE:
                    case GUARD_COUPLE_TYPE:
                        resId = R.layout.session_message_item_user_couple_game;
                        break;
                    case STICKER_TYPE:
                        resId = R.layout.session_message_item_user_sticker;
                        break;
                    case SYSTEM_NOTICE_TYPE:
                    case RICH_MIC_TYPE:
                        resId = R.layout.session_message_item_user_system_notice;
                        break;
                    case COIN_NOT_ENOUGH_TYPE:
                        resId = R.layout.session_message_item_user_coin;
                        break;
                    case BUY_CP_TIP_TYPE:
                        resId = R.layout.session_message_item_user_guard_cp;
                        break;
                    case ASK_FOR_GIFT:
                        resId = R.layout.session_message_item_ask_for_gift;
                        break;
                    case SEND_GIFT_CARD_TYPE:
                        resId = R.layout.session_message_item_send_gift;
                        break;
                    case CP_LEVEL_UP_TYPE:
                        resId = R.layout.session_message_send_declaration_inviet;
                        break;
                    case CP_LEVEL_UP_SUCCESS_TYPE:
                        resId = R.layout.session_message_send_declaration_successs;
                        break;
                    case CP_ASK_PTA_GIFT:
                        resId = R.layout.session_message_item_user_ask_gift;
                        break;
                    case CP_SEND_PTA_GIFT:
                        resId = R.layout.session_message_item_user_send_gift;
                        break;
                    case AUDIO_MATCH_TYPE:
                        resId = R.layout.session_message_item_user_audio_match;
                        break;
                    case GET_GIFT_BOX_SPECIAL_HINT:
                        resId = R.layout.message_item_get_gift_box_special_hint;
                        break;
                    case INVITE_GIFT_BOX:
                        resId = R.layout.container_message_box_open_invite;
                        break;
                }
                return resId;
            }

            @Override
            public int getItemViewType(int position, IMMessage chatRoomMessage) {
                int itemType = UNKNOWN_TYPE;
                if (chatRoomMessage.getMsgType() == MsgTypeEnum.text) {
                    itemType = TEXT_TYPE;
                }
                if (chatRoomMessage.getAttachment() instanceof InviteCoupleAttachment) {
                    if(((InviteCoupleAttachment) chatRoomMessage.getAttachment()).isIs_trial()){
                        itemType = COUPLE_BUY_TYPE;
                    }else {
                        itemType = COUPLE_TYPE;
                    }
                }

                if (chatRoomMessage.getAttachment() instanceof AskForGiftAttachment) {
                    itemType = ASK_FOR_GIFT;
                }

                if (chatRoomMessage.getAttachment() instanceof AudioAttachment) {
                    itemType = AUDIO_TYPE;
                }
                if (chatRoomMessage.getAttachment() instanceof ImageAttachment) {
                    itemType = IMAGE_TYPE;
                }
                if (chatRoomMessage.getAttachment() instanceof SendCupidAttachment) {
                    itemType = SEND_CUPID_MSG;
                }
                if (chatRoomMessage.getAttachment() instanceof CupidSysAttachment) {
                    itemType = CUPID_SYS_MSG;
                }
                if (chatRoomMessage.getAttachment() instanceof InviteCoupleGameAttachment) {
                    itemType = COUPLE_GAME_TYPE;
                }
                if (chatRoomMessage.getAttachment() instanceof GuardCpAttachment) {
                    itemType = GUARD_COUPLE_TYPE;
                }
                if (chatRoomMessage.getAttachment() instanceof StickerAttachment) {
                    itemType = STICKER_TYPE;
                }
                if (chatRoomMessage.getAttachment() instanceof GiveRewardAttachment) {
                    itemType = SYSTEM_NOTICE_TYPE;
                }
                if (chatRoomMessage.getAttachment() instanceof RichMicAttachment) {
                    itemType = RICH_MIC_TYPE;
                }
                if (chatRoomMessage.getAttachment() instanceof CoinNotEnoughAttachment) {
                    itemType = COIN_NOT_ENOUGH_TYPE;
                }
                if (chatRoomMessage.getAttachment() instanceof BuyCpTipsAttachment) {
                    itemType = BUY_CP_TIP_TYPE;
                }
                if (chatRoomMessage.getAttachment() instanceof SendGiftAttachment) {
                    itemType = SEND_GIFT_CARD_TYPE;
                }
                if (chatRoomMessage.getAttachment() instanceof CpLevelUpInviteAttach) {
                    itemType = CP_LEVEL_UP_TYPE;
                }
                if (chatRoomMessage.getAttachment() instanceof CpLevelUpSuccessAttach) {
                    itemType = CP_LEVEL_UP_SUCCESS_TYPE;
                }
                if (chatRoomMessage.getAttachment() instanceof VideoAdAttachment) {
                    itemType = SHOW_AD_VIDEO_TYPE;
                }
                if (chatRoomMessage.getAttachment() instanceof AskPTAGiftAttachment) {
                    itemType = CP_ASK_PTA_GIFT;
                }
                if (chatRoomMessage.getAttachment() instanceof SendPTAGiftAttachment) {
                    itemType = CP_SEND_PTA_GIFT;
                }
                if (chatRoomMessage.getAttachment() instanceof AudioMatchAttachment) {
                    itemType = AUDIO_MATCH_TYPE;
                }

                if (chatRoomMessage.getAttachment() instanceof GetGiftBoxSpecialHintAttachment) {
                    itemType = GET_GIFT_BOX_SPECIAL_HINT;
                }
                if (chatRoomMessage.getAttachment() instanceof BoxOpenInviteAttachment) {
                    itemType = INVITE_GIFT_BOX;
                }
                return itemType;
            }
        });
        this.mListener = listener;
    }

    private boolean needShowAnim = false;

    public void setNeedShowAnim(boolean needShowAnim) {
        this.needShowAnim = needShowAnim;
        notifyDataSetChanged();
    }

    @Override
    public void convert(CommonViewHolder holder, IMMessage message, int pos) {
        UserInfo userInfo = PorosApplication.instance.getUserInfoProvider().getUserInfo(message.getFromAccount());
        if (GET_GIFT_BOX_SPECIAL_HINT != getItemViewType(pos)){
            ImageView iv_left_head = holder.getView(R.id.iv_left_head);
            ImageView iv_right_head = holder.getView(R.id.iv_right_head);
            String text = TimeUtil.getTimeShowString(message.getTime(), false);
            holder.setText(R.id.tv_message_item_time, text);
            if (getItemViewType(pos) == SYSTEM_NOTICE_TYPE ||getItemViewType(pos) == CUPID_SYS_MSG || getItemViewType(pos) == RICH_MIC_TYPE || getItemViewType(pos) == COIN_NOT_ENOUGH_TYPE || getItemViewType(pos) == BUY_CP_TIP_TYPE) {
                holder.setVisibility(R.id.tv_message_item_time, View.GONE);
            } else {
                holder.setVisibility(R.id.tv_message_item_time, needShowTime(message) ? View.VISIBLE : View.GONE);
            }
            ImageView iv_anim = holder.getView(R.id.iv_anim);
            if (pos == mDatas.size() - 1 && !isReceiveMsg(message) && needShowAnim) {
                iv_anim.setVisibility(View.VISIBLE);
                iv_anim.setImageResource(R.drawable.bg_anim_animlist_heart);
                Animatable animatable = (Animatable) iv_anim.getDrawable();
                animatable.start();
            } else {
                iv_anim.setVisibility(View.GONE);
                iv_anim.setImageDrawable(null);
            }
            TextView tv_message_fail = holder.getView(R.id.tv_message_fail);
            ImageView iv_message_fail = holder.getView(R.id.iv_message_fail);
            ProgressBar message_progress = holder.getView(R.id.message_progress);
            updateMessageStatus(message, tv_message_fail, message_progress, iv_message_fail);
            holder.setOnLongClickListener(R.id.message_item_content, new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if (mListener != null) {
                        mListener.onLongClickMsg(v, message);
                    }
                    return false;
                }
            });

            if (isReceiveMsg(message)) {
                iv_left_head.setVisibility(getItemViewType(pos) == SYSTEM_NOTICE_TYPE ||getItemViewType(pos) == CUPID_SYS_MSG || getItemViewType(pos) == RICH_MIC_TYPE || getItemViewType(pos) == COIN_NOT_ENOUGH_TYPE || getItemViewType(pos) == BUY_CP_TIP_TYPE ? View.GONE : View.VISIBLE);
                iv_right_head.setVisibility(View.GONE);
                if (userInfo != null) {
                    Glide.with(mContext)
                            .load(userInfo.getAvatar())
                            .apply(headOptions)
                            .into(iv_left_head);
                }
            } else {
                iv_left_head.setVisibility(View.GONE);
                iv_right_head.setVisibility(getItemViewType(pos) == SYSTEM_NOTICE_TYPE || getItemViewType(pos) == RICH_MIC_TYPE || getItemViewType(pos) == COIN_NOT_ENOUGH_TYPE || getItemViewType(pos) == BUY_CP_TIP_TYPE ? View.GONE : View.VISIBLE);
                if (userInfo != null) {
                    Glide.with(mContext)
                            .load(userInfo.getAvatar())
                            .apply(headOptions)
                            .into(iv_right_head);
                }
            }
            holder.setText(R.id.tv_user_name, userInfo != null ? userInfo.getName() : message.getFromNick());
            iv_left_head.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        mListener.onAvatarClick();
                    }
                }
            });
        }

        switch (getItemViewType(pos)) {
            case TEXT_TYPE:
                TextView message_item_content = holder.getView(R.id.message_item_content);
                if (isReceiveMsg(message)) {
                    JinquanResourceHelper.getInstance(mContext).setBackgroundResourceByAttr(message_item_content, R.attr.msg_bundle_income);
                    JinquanResourceHelper.getInstance(mContext).setTextColorByAttr(message_item_content, R.attr.custom_attr_main_text_color);
                } else {
                    message_item_content.setBackgroundResource(R.mipmap.msg_buddle_outgoing);
                    message_item_content.setTextColor(Color.WHITE);
                }
                MoonUtil.identifyFaceExpression(mContext, message_item_content, message.getContent(), ImageSpan.ALIGN_BOTTOM);
                message_item_content.setMovementMethod(LinkMovementMethod.getInstance());
                break;
            case AUDIO_TYPE:
                ImageView animationView = holder.getView(R.id.message_item_audio_playing_animation);
                TextView durationLabel = holder.getView(R.id.message_item_audio_duration);
                View ll_audio_container = holder.getView(R.id.message_item_content);
                TextView message_item_audio_duration = holder.getView(R.id.message_item_audio_duration);
                View containerView = holder.getView(R.id.message_item_audio_container);
                View iv_audio_unread = holder.getView(R.id.message_item_audio_unread_indicator);
                iv_audio_unread.setVisibility(isUnreadAudioMessage(message) ? View.VISIBLE : View.GONE);
                if (isReceiveMsg(message)) {
                    JinquanResourceHelper.getInstance(mContext).setBackgroundResourceByAttr(ll_audio_container, R.attr.msg_bundle_income);
                    JinquanResourceHelper.getInstance(mContext).setTextColorByAttr(message_item_audio_duration, R.attr.custom_attr_main_text_color);
                    setGravity(animationView, Gravity.RIGHT | Gravity.CENTER_VERTICAL);
                    animationView.setBackgroundResource(R.mipmap.audio_animation_list_left_00000);
                } else {
                    ll_audio_container.setBackgroundResource(R.mipmap.msg_buddle_outgoing);
                    animationView.setBackgroundResource(R.mipmap.audio_animation_list_right_00000);
                    message_item_audio_duration.setTextColor(Color.WHITE);
                    setGravity(animationView, Gravity.RIGHT | Gravity.CENTER_VERTICAL);
                }
                setGravity(durationLabel, Gravity.LEFT | Gravity.CENTER_VERTICAL);
                AudioAttachment audioAttachment = (AudioAttachment) message.getAttachment();
                final int seconds = (int) TimeUtil.getSecondsByMilliseconds(audioAttachment.getDuration());
                if (seconds >= 0) {
                    durationLabel.setText(seconds + "s");
                } else {
                    durationLabel.setText("");
                }
                ll_audio_container.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (judgeIfShowDialog(message, BugVIPDialogFragment.VOICE)) return;
                        if (mAudioHelper == null) {
                            mAudioHelper = new AudioPlayerHelper();
                        }
                        mAudioHelper.startPlay(animationView, audioAttachment.getUrl(), true, isReceiveMsg(message));
                        mAudioHelper.startTimer(seconds);
                        mAudioHelper.setAudioListener(new AudioPlayerHelper.AudioPlayListener() {
                            @Override
                            public void startTime(int time) {
                                durationLabel.setText((time == 0 ? seconds : time) + "s");
                            }

                            @Override
                            public void finishTime() {

                            }
                        });
                        if (isUnreadAudioMessage(message) && mListener != null) {
                            mListener.onReadMessage(message);
                        }

                        HashMap<String, String> params = new HashMap<>();
                        params.put("user_id", AccountUtil.getInstance().getUserId());
                        params.put("female_guest_id", userInfo != null ? userInfo.getAccount() : "");
                        params.put("event_time", String.valueOf(System.currentTimeMillis()));
                        MultiTracker.onEvent(TrackConstants.B_VIP_VOICE_MESSAGE_CLICK, params);
                    }
                });

                int currentBubbleWidth = calculateBubbleWidth(seconds, 120);
                ViewGroup.LayoutParams layoutParams = containerView.getLayoutParams();
                layoutParams.width = currentBubbleWidth;
                containerView.setLayoutParams(layoutParams);
                break;
            case IMAGE_TYPE:
                ImageView thumbnail = holder.getView(R.id.message_item_content);
                FileAttachment attachment = (FileAttachment) message.getAttachment();
                RequestOptions options = new RequestOptions()
                        .apply(RequestOptions.bitmapTransform(new RoundedCorners(32)))
                        .placeholder(R.drawable.nim_message_item_round_bg)
                        .override(getImageMaxEdge(), getImageMinEdge())
                        .error(R.drawable.nim_message_item_round_bg);
                boolean is_vip_msg = false;
                Map<String, Object> remoteExt = message.getRemoteExtension();
                if (remoteExt != null) {
                    if (remoteExt.containsKey(IS_VIP_MSG)) {
                        is_vip_msg = (boolean) remoteExt.get(IS_VIP_MSG);
                        String blur_url = (String) remoteExt.get("blur_url");
                        if (is_vip_msg) {
                            if (Constants.is_sounds_vip || judge30min(message)) {
                                Glide.with(mContext).load(StringUtil.isEmpty(attachment.getUrl()) ? attachment.getPath() : attachment.getUrl()).apply(options)
                                        .into(thumbnail);
                            } else {
                                Glide.with(mContext).load(blur_url).apply(options)
                                        .into(thumbnail);
                            }
                        }
                    } else {
                        Glide.with(mContext).load(StringUtil.isEmpty(attachment.getUrl()) ? attachment.getPath() : attachment.getUrl()).apply(options)
                                .into(thumbnail);
                    }
                } else {
                    Glide.with(mContext).load(StringUtil.isEmpty(attachment.getUrl()) ? attachment.getPath() : attachment.getUrl()).apply(options)
                            .into(thumbnail);
                }

                thumbnail.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (judgeIfShowDialog(message, BugVIPDialogFragment.IMAGE)) return;
                        PictureSelector.create((Activity) mContext)
                                .themeStyle(R.style.picture_default_style)
                                .isNotPreviewDownload(true)
                                .imageEngine(GlideEngine.createGlideEngine())
                                .openExternalPreview(getPreviewPos(message.getUuid()), getSelectList());
                    }
                });



                break;
            case ASK_FOR_GIFT:
                AskForGiftAttachment askForGiftAttachment = (AskForGiftAttachment) message.getAttachment();
                ImageView giftPic = holder.getView(R.id.gift_pic);
                TextView askForGiftResult = holder.getView(R.id.ask_for_gift_result);
                TextView acceptSendGift = holder.getView(R.id.accept_send_gift);
                Group gpSendGift = holder.getView(R.id.gp_send_gift);
                Glide.with(mContext).load(askForGiftAttachment.getGift_icon()).into(giftPic);
                switch (askForGiftAttachment.getStatus()) {
                    case 1:
                        gpSendGift.setVisibility(View.VISIBLE);
                        askForGiftResult.setVisibility(View.GONE);
                        break;
                    case 2:
                        gpSendGift.setVisibility(View.GONE);
                        askForGiftResult.setVisibility(View.VISIBLE);
                        askForGiftResult.setText("您已同意");
                        break;
                    case 3:
                        gpSendGift.setVisibility(View.GONE);
                        askForGiftResult.setVisibility(View.VISIBLE);
                        askForGiftResult.setText("您已拒绝");
                        break;
                    case 4:
                        gpSendGift.setVisibility(View.GONE);
                        askForGiftResult.setVisibility(View.VISIBLE);
                        askForGiftResult.setText("超时拒绝");
                        break;
                }
                acceptSendGift.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (mListener != null) {
                            mListener.toBeNormalFriend();
                        }
                    }
                });
                break;
            case COUPLE_BUY_TYPE:
                InviteCoupleAttachment buyCoupleAttachment = (InviteCoupleAttachment) message.getAttachment();
                holder.setText(R.id.cp_card_title, buyCoupleAttachment.getCard_title());
                holder.setText(R.id.tv_sub_title,"获得以下奖励");
                holder.setText(R.id.box_key_one, buyCoupleAttachment.getBox_key_one());
                holder.setText(R.id.box_key_two, buyCoupleAttachment.getBox_key_two());
                holder.setText(R.id.login_bonus, buyCoupleAttachment.getLogin_bonus());
                LinearLayout otherStatus = holder.getView(R.id.other_status);
                LinearLayout receiverStatus = holder.getView(R.id.receive_status);
                ImageView imgStatus = holder.getView(R.id.img_status);
                TextView tvStatus = holder.getView(R.id.tv_status);
                TextView rejectBtn = holder.getView(R.id.btn_reject);
                TextView beCpBtn = holder.getView(R.id.btn_be_cp);
                if (message.getDirect() == MsgDirectionEnum.In) {
                    switch (buyCoupleAttachment.getStatus()) {
                        case 1:
                            receiverStatus.setVisibility(View.VISIBLE);
                            otherStatus.setVisibility(View.GONE);
                            break;
                        case 2:
                            receiverStatus.setVisibility(View.GONE);
                            otherStatus.setVisibility(View.VISIBLE);
                            tvStatus.setText("您已同意");
                            imgStatus.setImageResource(R.mipmap.ic_agree);
                            break;
                        case 3:
                            receiverStatus.setVisibility(View.GONE);
                            otherStatus.setVisibility(View.VISIBLE);
                            tvStatus.setText("您已拒绝");
                            imgStatus.setImageResource(R.mipmap.ic_reject);
                            break;
                        case 4:
                            receiverStatus.setVisibility(View.GONE);
                            otherStatus.setVisibility(View.VISIBLE);
                            tvStatus.setText("超时拒绝");
                            imgStatus.setImageResource(R.mipmap.ic_reject);
                            break;
                    }
                }else {
                    switch (buyCoupleAttachment.getStatus()) {
                        case 1:
                            receiverStatus.setVisibility(View.GONE);
                            otherStatus.setVisibility(View.VISIBLE);
                            tvStatus.setText("等待对方同意");
                            imgStatus.setImageResource(R.mipmap.ic_wait_agree);
                            break;
                        case 2:
                            receiverStatus.setVisibility(View.GONE);
                            otherStatus.setVisibility(View.VISIBLE);
                            tvStatus.setText("对方已同意");
                            imgStatus.setImageResource(R.mipmap.ic_agree);
                            break;
                        case 3:
                            receiverStatus.setVisibility(View.GONE);
                            otherStatus.setVisibility(View.VISIBLE);
                            tvStatus.setText("对方已拒绝");
                            imgStatus.setImageResource(R.mipmap.ic_reject);
                            break;
                        case 4:
                            receiverStatus.setVisibility(View.GONE);
                            otherStatus.setVisibility(View.VISIBLE);
                            tvStatus.setText("超时拒绝");
                            imgStatus.setImageResource(R.mipmap.ic_reject);
                            break;
                    }
                }
                rejectBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (mListener != null) {
                            mListener.onInviteCoupleAction(-1, buyCoupleAttachment.getOrder_id(), message.getDirect(),true);
                        }
                    }
                });
                beCpBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (mListener != null) {
                            mListener.onInviteCoupleAction(buyCoupleAttachment.getStatus(), buyCoupleAttachment.getOrder_id(), message.getDirect(),true);
                        }
                    }
                });
                break;
            case COUPLE_TYPE:
                InviteCoupleAttachment inviteCoupleAttachment = (InviteCoupleAttachment) message.getAttachment();
                holder.setText(R.id.tv_couple_desc, inviteCoupleAttachment.getDesc());
                TextView btn_reject = holder.getView(R.id.btn_reject);
                TextView btn_status = holder.getView(R.id.btn_status);
                if (message.getDirect() == MsgDirectionEnum.In) {
                    btn_reject.setVisibility(View.GONE);
                    if (inviteCoupleAttachment.getStatus() == 1) {
                        btn_status.setBackgroundResource(R.drawable.bg_invite_couple_btn);
                        btn_status.setTextColor(Color.parseColor("#FF75A3"));
                    } else {
                        btn_status.setBackgroundResource(R.drawable.bg_invite_game_btn);
                        btn_status.setTextColor(Color.parseColor("#FFFFFF"));
                    }
                    if (inviteCoupleAttachment.getStatus() == 1) {
                        btn_status.setEnabled(true);
                    } else {
                        btn_status.setEnabled(false);
                    }
                    switch (inviteCoupleAttachment.getStatus()) {//1 待同意 2 同意 3 拒绝 4 超时拒绝
                        case 1:
                            btn_reject.setVisibility(View.VISIBLE);
                            btn_status.setText(mContext.getString(R.string.couple_statue_agree));
                            break;
                        case 2:
                            btn_status.setText(mContext.getString(R.string.couple_statue_agreed));
                            break;
                        case 3:
                            btn_status.setText(mContext.getString(R.string.couple_statue_refuse));
                            break;
                        case 4:
                            btn_status.setText(mContext.getString(R.string.couple_statue_timeout));
                            break;
                    }
                } else {
                    btn_status.setBackgroundResource(R.drawable.bg_invite_game_btn);
                    btn_status.setTextColor(Color.WHITE);
                    switch (inviteCoupleAttachment.getStatus()) {// 1 待确认 2 已同意 3 已拒绝 4 已超时
                        case 1:
                            btn_status.setText(mContext.getString(R.string.couple_statue_wait_agree));
                            break;
                        case 2:
                            btn_status.setText(mContext.getString(R.string.couple_statue_agreed1));
                            break;
                        case 3:
                            btn_status.setText(mContext.getString(R.string.couple_statue_refuse1));
                            break;
                        case 4:
                            btn_status.setText(mContext.getString(R.string.couple_statue_timeout1));
                            break;
                    }
                }
                btn_status.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mListener != null) {
                            mListener.onInviteCoupleAction(inviteCoupleAttachment.getStatus(), inviteCoupleAttachment.getOrder_id(), message.getDirect(),false);
                        }
                    }
                });
                btn_reject.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mListener != null) {
                            mListener.onInviteCoupleAction(-1, inviteCoupleAttachment.getOrder_id(), message.getDirect(),false);
                        }
                    }
                });
                break;
            case COUPLE_GAME_TYPE:
                holder.setVisibility(R.id.btn_agree_invite, message.getDirect() == MsgDirectionEnum.In ? View.VISIBLE : View.GONE);
                InviteCoupleGameAttachment inviteCoupleGameAttachment = (InviteCoupleGameAttachment) message.getAttachment();
                LinearLayout nim_message_item_guard_container = holder.getView(R.id.message_item_content);
                nim_message_item_guard_container.setBackgroundResource(R.mipmap.msg_buddle_invite_couple_game_income);
                holder.setText(R.id.tv_couple_game_nickname, inviteCoupleGameAttachment.getFrom_user_name());
                holder.setVisibility(R.id.tv_couple_game_nickname, View.VISIBLE);
                ImageView iv_invite_cp_user_head = holder.getView(R.id.iv_invite_cp_user_head);
                Glide.with(mContext)
                        .load(inviteCoupleGameAttachment.getFrom_user_avatar())
                        .override(ScreenUtil.dip2px(44), ScreenUtil.dip2px(44))
                        .transform(new CircleCrop())
                        .error(R.mipmap.img_default_avatar)
                        .into(iv_invite_cp_user_head);
                holder.setOnClickListener(R.id.btn_agree_invite, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mListener != null) {
                            mListener.onInviteCoupleGameAction();
                        }
                    }
                });
                break;
            case GUARD_COUPLE_TYPE:
                holder.setVisibility(R.id.btn_agree_invite, message.getDirect() == MsgDirectionEnum.In ? View.VISIBLE : View.GONE);
                GuardCpAttachment guardCpAttachment = (GuardCpAttachment) message.getAttachment();
                LinearLayout nim_message_item_guard_cp_container = holder.getView(R.id.message_item_content);
                nim_message_item_guard_cp_container.setBackgroundResource(message.getDirect() == MsgDirectionEnum.In ? R.mipmap.msg_buddle_invite_guard_voice_income : R.mipmap.msg_buddle_invite_guard_voice_outgoing);
                ImageView iv_cp_user_head = holder.getView(R.id.iv_invite_cp_user_head);
                holder.setVisibility(R.id.tv_couple_game_nickname, View.GONE);
                holder.setText(R.id.tv_couple_game_desc, guardCpAttachment.getMsg());
                Glide.with(mContext)
                        .load(guardCpAttachment.getAvatar_url())
                        .override(ScreenUtil.dip2px(44), ScreenUtil.dip2px(44))
                        .transform(new CircleCrop())
                        .error(R.mipmap.img_default_avatar)
                        .into(iv_cp_user_head);
                holder.setOnClickListener(R.id.btn_agree_invite, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mListener != null) {
                            mListener.onInviteCoupleGameAction();
                        }
                    }
                });
                break;
            case STICKER_TYPE:
                StickerAttachment stickerAttachment = (StickerAttachment) message.getAttachment();
                if (stickerAttachment == null) {
                    return;
                }
                ImageView baseView = holder.getView(R.id.message_item_content);
                Glide.with(mContext)
                        .load(StickerManager.getInstance().getStickerUri(stickerAttachment.getCatalog(), stickerAttachment.getChartlet()))
                        .apply(new RequestOptions()
                                .error(R.mipmap.nim_default_img_failed)
                                .diskCacheStrategy(DiskCacheStrategy.NONE))
                        .into(baseView);
                break;
            case SYSTEM_NOTICE_TYPE:
                GiveRewardAttachment systemNoticeAttachment = (GiveRewardAttachment) message.getAttachment();
                TextView message_item_notice = holder.getView(R.id.message_item_notice);
                message_item_notice.setGravity(Gravity.CENTER | Gravity.LEFT);
                message_item_notice.setText(Html.fromHtml(systemNoticeAttachment.getMsg()));
                break;
            case SEND_CUPID_MSG:
                SendCupidAttachment sendCupidAttachment = (SendCupidAttachment) message.getAttachment();
                TextView tv_send_cupid = holder.getView(R.id.tv_send_cupid);
                tv_send_cupid.setText(sendCupidAttachment.getMsg());
                break;
            case CUPID_SYS_MSG:
                CupidSysAttachment cupidSysAttachment = (CupidSysAttachment) message.getAttachment();
                TextView tv_cupid_sys = holder.getView(R.id.tv_cupid_sys);
                tv_cupid_sys.setGravity(Gravity.CENTER);
                tv_cupid_sys.setText(Html.fromHtml(cupidSysAttachment.getMsg()));
                break;
            case RICH_MIC_TYPE:
                RichMicAttachment richMicAttachment = (RichMicAttachment) message.getAttachment();
                TextView message_mic_notice = holder.getView(R.id.message_item_notice);
                message_mic_notice.setGravity(Gravity.CENTER);
                message_mic_notice.setText(Html.fromHtml(richMicAttachment.getMsg()));
                break;
            case COIN_NOT_ENOUGH_TYPE:
                CoinNotEnoughAttachment coinNotEnoughAttachment = (CoinNotEnoughAttachment) message.getAttachment();
                TextView message_item_coin_tip = holder.getView(R.id.message_item_coin_tip);
                message_item_coin_tip.setText(coinNotEnoughAttachment.getDesc());
                holder.getView(R.id.message_item_coin_recharge).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mListener != null) {
                            mListener.onRecharge();
                        }
                    }
                });
                break;
            case BUY_CP_TIP_TYPE:
                BuyCpTipsAttachment buyCpTipsAttachment = (BuyCpTipsAttachment) message.getAttachment();
                TextView message_item_cp = holder.getView(R.id.message_item_cp);
                message_item_cp.setText(Html.fromHtml(buyCpTipsAttachment.getMsg()));
                holder.getView(R.id.message_item_invite_cp).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mListener != null) {
                            mListener.onShowCpCard();
                        }
                    }
                });
                break;
            case SEND_GIFT_CARD_TYPE:
                SendGiftAttachment sendGiftAttachment = (SendGiftAttachment) message.getAttachment();
                ImageView ivGift = holder.getView(R.id.iv_gift);
                TextView gifTitle = holder.getView(R.id.card_title);
                TextView giftName = holder.getView(R.id.card_name);
                TextView giftNum = holder.getView(R.id.message_gift_num);
                if (sendGiftAttachment != null) {
                    giftNum.setText("x" + sendGiftAttachment.getNum());
                    if(sendGiftAttachment.getNum()>1){
                        gifTitle.setText("送你x"+sendGiftAttachment.getNum());
                    }

                    GiftBean giftBean = sendGiftAttachment.getGiftBean();
                    if (giftBean != null) {
                        Glide.with(mContext).load(giftBean.icon).into(ivGift);
                        giftName.setText(giftBean.name);

                    }
                }
                TextView charmValueTv = holder.getView(R.id.tv_charm_value);
                if(isReceiveMsg(message)){
                    charmValueTv.setVisibility(View.VISIBLE);
                    charmValueTv.setText(String.format("+%d魅力值", sendGiftAttachment.getGiftBean().price*sendGiftAttachment.getNum()));
                }else{
                    charmValueTv.setVisibility(View.GONE);
                }
                break;
            case CP_LEVEL_UP_TYPE:
                CpLevelUpInviteAttach cpLevelUpInviteAttach = (CpLevelUpInviteAttach) message.getAttachment();
                TextView content = holder.getView(R.id.content);
                LinearLayout btn = holder.getView(R.id.btn);
                ImageView gift_img = holder.getView(R.id.gift_img);

                String show_words = cpLevelUpInviteAttach.getShow_words();
                String url = cpLevelUpInviteAttach.getGift_pic_url();
                content.setText(show_words);
                Glide.with(mContext).load(url).into(gift_img);

                if (btn != null) {
                    btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (mListener != null) {
                                mListener.onDeclarationInvite(cpLevelUpInviteAttach, message);
                            }
                        }
                    });
                }
                break;
            case CP_LEVEL_UP_SUCCESS_TYPE:
                CpLevelUpSuccessAttach cpLevelUpSuccessAttach = (CpLevelUpSuccessAttach) message.getAttachment();
                TextView content1 = holder.getView(R.id.content);
                LinearLayout btn1 = holder.getView(R.id.btn);
                ImageView gift_img1 = holder.getView(R.id.gift_img);

                String show_words1 = cpLevelUpSuccessAttach.getShow_words();
                String url1 = cpLevelUpSuccessAttach.getGift_pic_url();
                content1.setText(show_words1);
                Glide.with(mContext).load(url1).into(gift_img1);

                if (btn1 != null) {
                    btn1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (mListener != null) {
                                mListener.onDeclarationInviteSuccess(cpLevelUpSuccessAttach, message);
                            }
                        }
                    });
                }
                break;

            case CP_ASK_PTA_GIFT:
                AskPTAGiftAttachment askPTAGiftAttachment = (AskPTAGiftAttachment) message.getAttachment();
                Glide.with(mContext).load(askPTAGiftAttachment.getIcon_url()).into((ImageView) holder.getView(R.id.pta_icon));
                holder.setText(R.id.send, askPTAGiftAttachment.getBotton_text());
                holder.setText(R.id.tv_message, askPTAGiftAttachment.getMsg());
                holder.setText(R.id.pta_des, askPTAGiftAttachment.getCartoon_suit_name());
                holder.setOnClickListener(R.id.send, v -> {
                    if (ViewUtils.isFastClick()) {
                        return;
                    }
                    if (mListener != null) {
                        mListener.onVirtualBuy(askPTAGiftAttachment);
                    }
                });
                holder.setVisibility(R.id.send, View.VISIBLE);
                break;
            case CP_SEND_PTA_GIFT:
                SendPTAGiftAttachment sendPTAGiftAttachment = (SendPTAGiftAttachment) message.getAttachment();
                Glide.with(mContext).load(sendPTAGiftAttachment.getIcon_url()).into((ImageView) holder.getView(R.id.pta_icon));
                holder.setText(R.id.tv_message, sendPTAGiftAttachment.getMsg());
                holder.setText(R.id.pta_des, sendPTAGiftAttachment.getCartoon_suit_name() + " +" + sendPTAGiftAttachment.getDays() + "天");
                break;
            case AUDIO_MATCH_TYPE:
                AudioMatchAttachment audioMatchAttachment = (AudioMatchAttachment) message.getAttachment();
                holder.setText(R.id.tv_audio_match_title, audioMatchAttachment.getMsg());
                holder.setOnClickListener(R.id.tv_start_wangzhe_audio_match, v -> {
                    if (ViewUtils.isFastClick()) {
                        return;
                    }
                    if (mListener != null) {
                        mListener.onAudioMatch(TAG_WANGZHE);
                    }
                });
                holder.setOnClickListener(R.id.tv_start_heping_audio_match, v -> {
                    if (ViewUtils.isFastClick()) {
                        return;
                    }
                    if (mListener != null) {
                        mListener.onAudioMatch(TAG_HEPING);
                    }
                });
                holder.setOnClickListener(R.id.tv_start_audio_match, v -> {
                    if (ViewUtils.isFastClick()) {
                        return;
                    }
                    if (mListener != null) {
                        mListener.onAudioMatch(TAG_CHAT);
                    }
                });
                break;
            case GET_GIFT_BOX_SPECIAL_HINT:
                GetGiftBoxSpecialHintAttachment getGiftBoxAttachment = (GetGiftBoxSpecialHintAttachment) message.getAttachment();
                holder.setText(R.id.tv_nickname,"恭喜"+getGiftBoxAttachment.getNick_name()+"拆出");
                holder.setText(R.id.tv_gift_name,getGiftBoxAttachment.getGift_name());
                holder.setText(R.id.tv_msg,getGiftBoxAttachment.getMsg());

                ImageView iv_image = holder.getView(R.id.iv_image);
                Glide.with(mContext)
                        .load(getGiftBoxAttachment.getGift_image())
                        .transform(new CircleCrop())
                        .error(R.mipmap.img_default_avatar)
                        .into(iv_image);
                break;
            case INVITE_GIFT_BOX:
                BoxOpenInviteAttachment boxOpenInviteAttachment = (BoxOpenInviteAttachment) message.getAttachment();
                String desc = boxOpenInviteAttachment.getDesc();

                TextView check_box_btn = holder.getView(R.id.check_box_btn);
                check_box_btn.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v) {
                        //跳转到开宝箱页面
                        if (mListener != null) {
                            mListener.onCheckGiftBox();
                        }
                    }
                });
                ImageView btn_check_pool = holder.getView(R.id.btn_check_pool);
                btn_check_pool.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //跳转到开宝箱规则页面
                        if (mListener != null) {
                            mListener.onCheckPool();
                        }
                    }
                });
                holder.setText(R.id.desc2, desc);

                View view = holder.getView(R.id.message_item_content);
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mListener != null) {
                            mListener.onCheckGiftBox();
                        }
                    }
                });

                HashMap<String, String> params = new HashMap<>();
                params.put("user_id", AccountUtil.getInstance().getUserId());
                params.put("guest_id", userInfo != null ? userInfo.getAccount() : "");
                params.put("event_time", String.valueOf(System.currentTimeMillis()));
                MultiTracker.onEvent(TrackConstants.B_INVITE_CARD_EXPOSURE, params);

                break;
        }
    }

    private boolean judgeIfShowDialog(IMMessage message, String image) {
        Map<String, Object> remoteExt = message.getRemoteExtension();
        if (remoteExt != null) {
            if (remoteExt.containsKey(IS_VIP_MSG)) {
                boolean is_vip_msg = (boolean) remoteExt.get(IS_VIP_MSG);
                if (is_vip_msg && !Constants.is_sounds_vip && !isMyMessage(message) && !judge30min(message)) {
//                            OneCentPayDialog oneCentPayDialog = new OneCentPayDialog();
//                            oneCentPayDialog.setTvPayConinString("购买vip");
//                            oneCentPayDialog.setRechargeWays(1, Constants.recharge_way);
//                            oneCentPayDialog.show(((FragmentActivity)mContext).getSupportFragmentManager(), OneCentPayDialog.class.getName());

                    if(Constants.buy_vip_get_coin > 0){
                        BuyVIPDialogFragment.newInstance("").
                                setFemale_guest_id(message.getFromAccount())
                                .setRoom_id(room_id)
                                .setAlert_type(image)
                                .show(((FragmentActivity) mContext).getSupportFragmentManager(), BuyVIPDialogFragment.class.getName());
                    }else{
                        BugVIPDialogFragment.newInstance("").
                                setFemale_guest_id(message.getFromAccount())
                                .setRoom_id(room_id)
                                .setAlert_type(image)
                                .show(((FragmentActivity) mContext).getSupportFragmentManager(), BugVIPDialogFragment.class.getName());
                    }

                    return true;
                }
            }
        }
        return false;
    }

    public static int getAudioMaxEdge() {
        return (int) (0.25 * ScreenUtil.screenMin);
    }

    public static int getAudioMinEdge() {
        return (int) (0.2175 * ScreenUtil.screenMin);
    }

    private int calculateBubbleWidth(long seconds, int MAX_TIME) {
        int maxAudioBubbleWidth = getAudioMaxEdge();
        int minAudioBubbleWidth = getAudioMinEdge();

        int currentBubbleWidth;
        if (seconds <= 0) {
            currentBubbleWidth = minAudioBubbleWidth;
        } else if (seconds > 0 && seconds <= MAX_TIME) {
            currentBubbleWidth = (int) ((maxAudioBubbleWidth - minAudioBubbleWidth) * (2.0 / Math.PI)
                    * Math.atan(seconds / 10.0) + minAudioBubbleWidth);
        } else {
            currentBubbleWidth = maxAudioBubbleWidth;
        }

        if (currentBubbleWidth < minAudioBubbleWidth) {
            currentBubbleWidth = minAudioBubbleWidth;
        } else if (currentBubbleWidth > maxAudioBubbleWidth) {
            currentBubbleWidth = maxAudioBubbleWidth;
        }
        return currentBubbleWidth + ScreenUtil.dip2px(30);
    }

    /**
     * @param message
     * @return
     */
    private boolean isMyMessage(IMMessage message) {
        String fromAccount = message.getFromAccount();
        String myAccount = AccountUtil.getInstance().getUserId();
        return message.getSessionType() == SessionTypeEnum.P2P
                && message.getFromAccount() != null
                && fromAccount.equals(myAccount);
    }

    // 设置FrameLayout子控件的gravity参数
    protected final void setGravity(View view, int gravity) {
        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) view.getLayoutParams();
        params.gravity = gravity;
    }

    public static boolean judge30min(IMMessage message) {
        long msgTimeStamp = AccountUtil.getInstance().getUserInfo().getRegister_time();
        long currentTimeStamp = System.currentTimeMillis();

        long time = currentTimeStamp - msgTimeStamp;
        if (time > 0) {
            if (time > Constants.buy_vip_time_limit * 1000) {
                //代表大于30分钟
                return true;
            } else {
                //代表小于30分钟
                return false;
            }

//            if(time > 300000){
//                //代表大于30分钟
//                return true;
//            }else{
//                //代表小于30分钟
//                return false;
//            }
        } else {
            return false;
        }
    }

    private int getPreviewPos(String uuid) {
        int pos = 0;
        for (IMMessage message : mDatas) {
            if (message.getAttachment() instanceof ImageAttachment) {
                if (StringUtil.isEquals(message.getUuid(), uuid)) {
                    return pos;
                }
                pos++;
            }
        }
        return 0;
    }

    private List<LocalMedia> getSelectList() {
        List<LocalMedia> localMedia = new ArrayList<>();
        for (IMMessage message : mDatas) {
            if (message.getAttachment() instanceof ImageAttachment) {
                boolean is_vip_msg = false;
                Map<String, Object> remoteExt = message.getRemoteExtension();
                if (remoteExt != null) {
                    if (remoteExt.containsKey(IS_VIP_MSG)) {
                        is_vip_msg = (boolean) remoteExt.get(IS_VIP_MSG);
                        String blur_url = (String) remoteExt.get("blur_url");
                        if (is_vip_msg) {
                            if (Constants.is_sounds_vip || judge30min(message)) {
                                ImageAttachment attachment = (ImageAttachment) message.getAttachment();
                                localMedia.add(new LocalMedia(attachment.getUrl(), 0, 0, ""));
                            } else {
                                localMedia.add(new LocalMedia(blur_url, 0, 0, ""));
                            }
                        }
                    } else {
                        ImageAttachment attachment = (ImageAttachment) message.getAttachment();
                        localMedia.add(new LocalMedia(attachment.getUrl(), 0, 0, ""));
                    }
                } else {
                    ImageAttachment attachment = (ImageAttachment) message.getAttachment();
                    localMedia.add(new LocalMedia(attachment.getUrl(), 0, 0, ""));
                }
            }
        }
        return localMedia;
    }

    public static int getImageMaxEdge() {
        return (int) (165.0 / 320.0 * ScreenUtil.screenWidth);
    }

    public static int getImageMinEdge() {
        return (int) (76.0 / 320.0 * ScreenUtil.screenWidth);
    }


    private boolean isReceiveMsg(IMMessage chatRoomMessage) {
        return !StringUtil.isEquals(chatRoomMessage.getFromAccount(), AccountUtil.getInstance().getUserId());
    }

    public long getLastestMsgTime() {
        if (mDatas.size() > 0) {
            return mDatas.get(mDatas.size() - 1).getTime();
        }

        return 0;
    }

    public String getOldMsgId() {
        if (mDatas.size() > 0) {
            return mDatas.get(0).getUuid();
        }

        return "";
    }

    private Set<String> timedItems = new HashSet<>(); // 需要显示消息时间的消息ID

    public boolean needShowTime(IMMessage message) {
        return timedItems.contains(message.getUuid());
    }

    private void setShowTime(IMMessage message, boolean show) {
        if (show) {
            timedItems.add(message.getUuid());
        } else {
            timedItems.remove(message.getUuid());
        }
    }

    private IMMessage lastShowTimeItem; // 用于消息时间显示,判断和上条消息间的时间间隔

    /**
     * 列表加入新消息时，更新时间显示
     */
    public void updateShowTimeItem(List<IMMessage> items, boolean fromStart, boolean update) {
        IMMessage anchor = fromStart ? null : lastShowTimeItem;
        for (IMMessage message : items) {
            if (setShowTimeFlag(message, anchor)) {
                anchor = message;
            }
        }

        if (update) {
            lastShowTimeItem = anchor;
        }
    }

    public void updateShowTimeItem(IMMessage item, boolean fromStart, boolean update) {
        IMMessage anchor = fromStart ? null : lastShowTimeItem;
        if (setShowTimeFlag(item, anchor)) {
            anchor = item;
        }

        if (update) {
            lastShowTimeItem = anchor;
        }
    }

    /**
     * 是否显示时间item
     */
    private boolean setShowTimeFlag(IMMessage message, IMMessage anchor) {
        boolean update = false;

        if (anchor == null) {
            setShowTime(message, true);
            update = true;
        } else {
            long time = anchor.getTime();
            long now = message.getTime();

            if (now - time == 0) {
                // 消息撤回时使用
                setShowTime(message, true);
                lastShowTimeItem = message;
                update = true;
            } else if (now - time < 5 * 60 * 1000) {
                setShowTime(message, false);
            } else {
                setShowTime(message, true);
                update = true;
            }
        }

        return update;
    }

    public boolean isUnreadAudioMessage(IMMessage message) {
        if (message.getDirect() == MsgDirectionEnum.In
                && message.getAttachStatus() == AttachStatusEnum.transferred
                && message.getStatus() != MsgStatusEnum.read) {
            return true;
        } else {
            return false;
        }
    }

    private void updateMessageStatus(IMMessage message, View messageFailView, ProgressBar pbMessageLoading, View iv_message_fail) {
        if (pbMessageLoading == null) {
            return;
        }
        switch (message.getStatus()) {
            case fail:
                iv_message_fail.setVisibility(View.GONE);
                pbMessageLoading.setVisibility(View.GONE);
                messageFailView.setVisibility(View.VISIBLE);
                break;
            case sending:
                iv_message_fail.setVisibility(View.GONE);
                pbMessageLoading.setVisibility(View.VISIBLE);
                messageFailView.setVisibility(View.GONE);
                break;
            case draft:
                iv_message_fail.setVisibility(View.VISIBLE);
                pbMessageLoading.setVisibility(View.GONE);
                messageFailView.setVisibility(View.GONE);
                break;
            case success:
                iv_message_fail.setVisibility(View.GONE);
                pbMessageLoading.setVisibility(View.GONE);
                messageFailView.setVisibility(View.GONE);
                break;
            default:
                pbMessageLoading.setVisibility(View.GONE);
                messageFailView.setVisibility(View.GONE);
                break;
        }
        messageFailView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.onResetSendMessage(message);
                }
            }
        });
    }

    public void onDestroy() {
        if (mAudioHelper != null) {
            mAudioHelper.release();
            mAudioHelper = null;
        }
    }
}
