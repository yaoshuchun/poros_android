package com.liquid.poros.adapter;


import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.PagerAdapter;

import java.util.List;

public class GiftPagerAdapter extends FragmentStatePagerAdapter {

    private List<Fragment> fragments;
    private List<String> viewPagerTitles;
    private Fragment mCurrentFragmen;

    public GiftPagerAdapter(FragmentManager fm, List<Fragment> list) {
        super(fm);
        this.fragments = list;
    }

    public GiftPagerAdapter(FragmentManager fm, List<Fragment> fragments, List<String> titles) {
        super(fm);
        this.fragments = fragments;
        this.viewPagerTitles = titles;
    }

    @Override
    public Fragment getItem(int arg0) {
        return fragments.get(arg0);//显示第几个页面
    }

    @Override
    public int getCount() {
        return fragments.size();//有几个页面
    }



    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        if (null != viewPagerTitles && viewPagerTitles.size()>0)
            return viewPagerTitles.get(position);

        return null;
    }

    @Override
    public int getItemPosition(Object object) {
        return PagerAdapter.POSITION_NONE;
    }

    public Fragment getCurrentFragment() {
        return mCurrentFragmen;
    }
}


