package com.liquid.poros.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.liquid.poros.R;
import com.liquid.poros.entity.MsgUserModel;

import java.util.List;

public class GiftNumSelectAdapter extends RecyclerView.Adapter<GiftNumSelectAdapter.GiftNumViewHolder>{
    private Context mContext;
    private List<MsgUserModel.GiftNums> datas;
    private OnGiftNumSelectListener listener;

    public GiftNumSelectAdapter(Context context) {
        mContext = context;
    }

    public void setDatas(List<MsgUserModel.GiftNums> datas) {
        this.datas = datas;
        notifyDataSetChanged();
    }

    public void setListener(OnGiftNumSelectListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public GiftNumViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflate = LayoutInflater.from(mContext).inflate(R.layout.item_gift_num, parent, false);
        return new GiftNumViewHolder(inflate);
    }

    @Override
    public void onBindViewHolder(@NonNull GiftNumViewHolder holder, int position) {
        MsgUserModel.GiftNums giftNums = datas.get(position);
        holder.giftName.setText(giftNums.name);
        holder.giftNum.setText(giftNums.count + "");
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.giftNumSelect(giftNums.count);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return datas == null ? 0 : datas.size();
    }

    public static class GiftNumViewHolder extends RecyclerView.ViewHolder{
        TextView giftNum;
        TextView giftName;
        public GiftNumViewHolder(@NonNull View itemView) {
            super(itemView);
            giftNum = itemView.findViewById(R.id.gift_num);
            giftName = itemView.findViewById(R.id.gift_num_name);
        }
    }

    public interface OnGiftNumSelectListener{
        void giftNumSelect(int num);
    }
}
