package com.liquid.poros.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.liquid.poros.databinding.ItemCpCardDaysBinding;
import com.liquid.poros.entity.CoupleCardInfo;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class CpCardDaysAdapter extends RecyclerView.Adapter<CpCardDaysAdapter.CpCardDayViewHolder> {
    private Activity mContext;
    private List<CoupleCardInfo> cardList;
    private int selectPos = 0;
    private int cpCardTime;
    private CpCardSelectedListener listener;

    public CpCardDaysAdapter(Context context) {
        mContext = (Activity) context;
    }

    public void setDatas(List<CoupleCardInfo> cardList) {
        this.cardList = cardList;
        notifyDataSetChanged();
    }

    public void setListener(CpCardSelectedListener listener) {
        this.listener = listener;
    }

    public void setCpCardTime(int cpCardTime) {
        this.cpCardTime = cpCardTime;
    }

    @NonNull
    @Override
    public CpCardDayViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemCpCardDaysBinding binding = ItemCpCardDaysBinding.inflate(LayoutInflater.from(mContext));
        return new CpCardDayViewHolder(binding.getRoot());
    }


    @Override
    public void onBindViewHolder(@NonNull CpCardDayViewHolder holder, int position) {
        CoupleCardInfo coupleCardInfo = cardList.get(position);
        ItemCpCardDaysBinding binding = ItemCpCardDaysBinding.bind(holder.itemView);
        binding.cpDiscount.setText("省" + coupleCardInfo.getDiscounts() + "金币");
        binding.cpDiscount.setVisibility(coupleCardInfo.getDiscounts() == 0 ? View.GONE : View.VISIBLE);
        binding.tvCpDays.setText(coupleCardInfo.getCp_time() + "天");
        if (cpCardTime == -1) {
            binding.deleteDays.setVisibility(View.GONE);
            holder.itemView.setEnabled(true);
            if (selectPos == position) {
                holder.itemView.setSelected(true);
                binding.icSelectedTag.setVisibility(View.VISIBLE);
                if (listener != null) {
                    listener.selectCard(coupleCardInfo);
                }
            } else {
                holder.itemView.setSelected(false);
                binding.icSelectedTag.setVisibility(View.GONE);
            }
        } else {
            selectPos = selectPosByDay(cpCardTime);
            if (selectPos == position) {
                holder.itemView.setSelected(true);
                binding.deleteDays.setVisibility(View.GONE);
                binding.icSelectedTag.setVisibility(View.VISIBLE);
                if (listener != null) {
                    listener.selectCard(coupleCardInfo);
                }
            } else {
                holder.itemView.setSelected(false);
                holder.itemView.setEnabled(false);
                binding.deleteDays.setVisibility(View.VISIBLE);
                binding.icSelectedTag.setVisibility(View.GONE);
            }
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectPos = position;
                notifyDataSetChanged();
            }
        });
    }

    private int selectPosByDay(int day) {
        switch (day) {
            case 7:
                return 1;
            case 15:
                return 2;
            case 30:
                return 3;
            default:
                return 0;
        }
    }

    public interface CpCardSelectedListener {
        void selectCard(CoupleCardInfo info);
    }

    @Override
    public int getItemCount() {
        return cardList == null ? 0 : cardList.size();
    }

    public static class CpCardDayViewHolder extends RecyclerView.ViewHolder {
        public CpCardDayViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
