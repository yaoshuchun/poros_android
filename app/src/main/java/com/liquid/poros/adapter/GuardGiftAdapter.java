package com.liquid.poros.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.liquid.poros.R;
import java.util.List;

public class GuardGiftAdapter extends RecyclerView.Adapter<GuardGiftAdapter.GiftViewHolder>{
    private Context mContext;
    private List<String> datas;
    private RoundedCorners roundedCorners;
    private RequestOptions options;

    public GuardGiftAdapter(Context context,List<String> datas) {
        mContext = context;
        this.datas = datas;
        roundedCorners= new RoundedCorners(1);
        options = new RequestOptions()
                .placeholder(R.mipmap.icon_default)
                .error(R.mipmap.icon_default)
                .transform(new CenterCrop(),roundedCorners);
    }

    @NonNull
    @Override
    public GiftViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflate = LayoutInflater.from(mContext).inflate(R.layout.item_gift_list, parent, false);
        return new GiftViewHolder(inflate);
    }

    @Override
    public void onBindViewHolder(@NonNull GiftViewHolder holder, int position) {
        String giftStr = datas.get(position);
        Glide.with(mContext).applyDefaultRequestOptions(options).load(giftStr).into(holder.item);
    }

    @Override
    public int getItemCount() {
        return datas == null ? 0 : datas.size();
    }

    public static class GiftViewHolder extends RecyclerView.ViewHolder{
        ImageView item;
        public GiftViewHolder(@NonNull View itemView) {
            super(itemView);
            item = (ImageView) itemView;
        }
    }
}
