package com.liquid.poros.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.liquid.poros.R;
import com.liquid.poros.entity.RechargeListInfo;
import java.util.List;

public class RechargeCoinAmountAdapter extends RecyclerView.Adapter<RechargeCoinAmountAdapter.RechargeCoinViewHolder>{
    private Context mContext;
    private List<RechargeListInfo> datas;
    private OnCoinNumSelectListener listener;
    private int selectPos;

    public RechargeCoinAmountAdapter(Context context) {
        mContext = context;
    }

    public void setDatas(List<RechargeListInfo> datas) {
        this.datas = datas;
        notifyDataSetChanged();
    }

    public void setListener(OnCoinNumSelectListener listener) {
        this.listener = listener;
    }


    @NonNull
    @Override
    public RechargeCoinViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflate = LayoutInflater.from(mContext).inflate(R.layout.item_recharge_coin, parent, false);
        return new RechargeCoinViewHolder(inflate);
    }

    @Override
    public void onBindViewHolder(@NonNull RechargeCoinViewHolder holder, int position) {
        RechargeListInfo rechargeListInfo = datas.get(position);
        long coinCount = rechargeListInfo.getCoin();
        long money = rechargeListInfo.getMoney();
        String type = rechargeListInfo.getType();
        rechargeListInfo.getType();
        holder.rechargeCoinCount.setText(coinCount + "");
        holder.rechargeCash.setText("¥" + money / 100);
        if (selectPos == position) {
            holder.rechargeCash.setTextColor(mContext.getResources().getColor(R.color.color_ffffff));
            holder.rechargeCoinCount.setTextColor(mContext.getResources().getColor(R.color.color_ffffff));
            holder.item.setBackgroundResource(R.drawable.bg_ali_pay);
        }else {
            holder.item.setBackgroundResource(R.drawable.bg_wechat_pay);
            holder.rechargeCash.setTextColor(mContext.getResources().getColor(R.color.color_A4A9B3));
            holder.rechargeCoinCount.setTextColor(mContext.getResources().getColor(R.color.color_181e25));
        }
        holder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    selectPos = position;
                    listener.coinNumSelect(coinCount,money,type);
                    notifyDataSetChanged();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return datas == null ? 0 : datas.size();
    }

    public static class RechargeCoinViewHolder extends RecyclerView.ViewHolder{
        View item;
        TextView rechargeCoinCount;
        TextView rechargeCash;

        public RechargeCoinViewHolder(@NonNull View itemView) {
            super(itemView);
            item = itemView;
            rechargeCoinCount = itemView.findViewById(R.id.recharge_coin_count);
            rechargeCash = itemView.findViewById(R.id.recharge_cash);
        }
    }

    public interface OnCoinNumSelectListener{
        void coinNumSelect(long num,long cash,String type);
    }
}
