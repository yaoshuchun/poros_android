package com.liquid.poros.adapter.base;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.lang.reflect.Constructor;
import java.util.List;
import java.util.Objects;

/**
 * BaseAdapter
 * verion 1.0
 * @param <T> 数据类型
 * @param <H> Viewholder  需要定义成 public static class
 *
 * 使用示例 {@link ExampleAdater}
 */

public abstract class BaseAdapter<T, H extends BaseViewHolder> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    protected Context mContext;
    protected List<T> mDatas;
    private Constructor<H> hoderConstructor;


    public BaseAdapter(Context context, List<T> datas) {
        this.mContext = context;
        this.mDatas = datas;
        Class<H> aClass = viewHoderClass();
        if (null != aClass){
            try {
                hoderConstructor = aClass.getConstructor(View.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    @NonNull
    @Override
    public H onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        return Objects.requireNonNull(createHoder(viewGroup));
    }

    protected abstract Class<H> viewHoderClass();

    protected abstract int itemViewXmlId();

    protected abstract H createHoder(ViewGroup viewGroup);/*{
        try {
            if (null == hoderConstructor || itemViewXmlId() == 0)
                return null;
            return hoderConstructor.newInstance(createItemView(itemViewXmlId(),viewGroup));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }*/

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        T data = mDatas.get(position);
        if (null != data){
            onBindViewHolder((H)holder,position,data);
        }
    }

    public abstract void onBindViewHolder(@NonNull H viewHolder, int position, T data);

    @Override
    public int getItemCount() {
        return null != mDatas ? mDatas.size() : 0;
    }

    protected View createItemView(int viewResourceId,ViewGroup parent){
        return LayoutInflater.from(mContext).inflate(viewResourceId, parent, false);
    }


}
