package com.liquid.poros.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.liquid.poros.R
import com.liquid.poros.entity.BoostLotteryListBean
import com.liquid.poros.widgets.LotteryView

class LotteryAdapter(var context: Context, var onClick: LotteryView.DrawClickListener) :
        RecyclerView.Adapter<LotteryAdapter.LuckyDrawItem>() {
    //position位置映射
    var posMap = mapOf<Int, Int>(0 to 0, 1 to 1, 2 to 2, 3 to 7, 4 to 8, 5 to 3, 6 to 6, 7 to 5, 8 to 4)
    private var selectPosition = -1//当前选中需要常亮的
    private var lotteryList = ArrayList<BoostLotteryListBean>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LuckyDrawItem {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_lottery_boost, parent, false)
        return LuckyDrawItem(view, onClick)
    }

    /**
     * 更新奖品数据
     */
    fun update(boostLotteryListBean: List<BoostLotteryListBean>) {
        if (!boostLotteryListBean.isNullOrEmpty()) {
            this.lotteryList = boostLotteryListBean as ArrayList<BoostLotteryListBean>
            this.notifyDataSetChanged()
        }
    }

    override fun onBindViewHolder(holder: LuckyDrawItem, position: Int) {
        holder.init(context, posMap[position], selectPosition, lotteryList)
    }

    override fun getItemCount(): Int {
        return 9
    }

    fun setSelectionPosition(selectPos: Int) {
        val lastPos = selectPosition
        selectPosition = selectPos
        if (lastPos != -1) {
            notifyItemChanged(reversePosition(lastPos))
        } else {
            notifyDataSetChanged()
        }
        notifyItemChanged(reversePosition(selectPos))
    }

    /**
     * 重置position
     */
    fun updateSelectionPosition(selectPos: Int) {
        selectPosition = selectPos
        notifyItemChanged(reversePosition(selectPos))
    }

    /**
     * 获取真实坐标
     */
    private fun reversePosition(selectPos: Int): Int {
        for ((key, value) in posMap.entries) {
            if (value == selectPos) {
                return key
            }
        }
        return -1
    }

    class LuckyDrawItem(itemView: View, var onClick: LotteryView.DrawClickListener) :
            RecyclerView.ViewHolder(itemView) {
        private var ivLotteryBoostBg: ImageView = itemView.findViewById(R.id.iv_lottery_boost_bg)
        private var ivLotteryGift: ImageView = itemView.findViewById(R.id.iv_lottery_gift)
        private var tvLotteryGiftName: TextView = itemView.findViewById(R.id.tv_lottery_gift_name)
        fun init(context: Context, fakePos: Int?, selectPos: Int, boostLotteryListBean: List<BoostLotteryListBean>) {
            if (fakePos == 8) {
                //抽奖按钮
                ivLotteryBoostBg.setBackgroundResource(R.mipmap.icon_start_open_lottery)
                ivLotteryGift.visibility = View.GONE
                tvLotteryGiftName.visibility = View.GONE
                ivLotteryBoostBg.setOnClickListener {
                    onClick.onClickDraw()
                }
            } else {
                //奖品区域
                ivLotteryBoostBg.setBackgroundResource(if (selectPos == fakePos) R.mipmap.icon_boost_luck_select else R.mipmap.icon_boost_luck_normal)
                ivLotteryGift.visibility = View.VISIBLE
                tvLotteryGiftName.visibility = View.VISIBLE
                val options = RequestOptions()
                        .placeholder(R.mipmap.img_default_avatar)
                        .error(R.mipmap.img_default_avatar)
                Glide.with(context)
                        .load(boostLotteryListBean[fakePos!!].image)
                        .apply(options)
                        .into(ivLotteryGift)
                if (boostLotteryListBean[fakePos].boost_lottery_num == 1) {
                    tvLotteryGiftName.text = boostLotteryListBean[fakePos].prize_name
                } else {
                    tvLotteryGiftName.text = "${boostLotteryListBean[fakePos].prize_name}*${boostLotteryListBean[fakePos].boost_lottery_num}"
                }
            }
        }

    }

}