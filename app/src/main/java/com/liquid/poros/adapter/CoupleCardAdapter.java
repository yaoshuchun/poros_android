package com.liquid.poros.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.liquid.poros.R;
import com.liquid.poros.entity.CoupleCardInfo;
import com.liquid.poros.utils.ScreenUtil;
import com.liquid.poros.utils.StringUtil;

import java.math.BigDecimal;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * 购买CP卡
 */
public class CoupleCardAdapter extends RecyclerView.Adapter<CoupleCardAdapter.PieceViewHolder> {
    private List<CoupleCardInfo> mData;
    private Context mContext;
    private int defItem = 0;
    private OnItemListener onItemListener;
    private boolean mIsCpRoom;//区分组cp直播房间 和 私聊界面

    public CoupleCardAdapter(Context context, boolean isCpRoom, List<CoupleCardInfo> data) {
        this.mIsCpRoom = isCpRoom;
        this.mData = data;
        this.mContext = context;
    }

    public void setOnItemListener(OnItemListener onItemListener) {
        this.onItemListener = onItemListener;
    }

    public interface OnItemListener {
        void onClick(View v, int pos);
    }

    public void setDefSelect(int position) {
        this.defItem = position;
        notifyDataSetChanged();
    }


    @Override
    public PieceViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        PieceViewHolder viewHolder = new PieceViewHolder(LayoutInflater.from(mContext).inflate(R.layout.item_couple_card, viewGroup, false));
        return viewHolder;
    }


    @SuppressLint("ResourceType")
    @Override
    public void onBindViewHolder(@NonNull final PieceViewHolder viewHolder, final int position) {
        CoupleCardInfo coupleCardInfo = mData.get(position);
        if (!TextUtils.isEmpty(coupleCardInfo.getCard_cnt())) {
            int code = new BigDecimal(coupleCardInfo.getCard_cnt()).compareTo(new BigDecimal(0));
            if (code <= 0) {
                if (!TextUtils.isEmpty(coupleCardInfo.getOri_price())) {
                    viewHolder.item_tv_card_ori_price.setVisibility(View.VISIBLE);
                    viewHolder.item_tv_card_ori_price.setText("原价:" + coupleCardInfo.getOri_price());
                    viewHolder.item_tv_card_ori_price.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                } else {
                    viewHolder.item_tv_card_ori_price.setVisibility(View.GONE);
                }
                viewHolder.item_tv_card_money.setTextSize(24);
                viewHolder.item_tv_card_money.setText(StringUtil.matcherSearchText(true, Color.parseColor(mContext.getString(R.color.yellow)), coupleCardInfo.getCard_price() + "金币", "金币"));
            } else {
                viewHolder.item_tv_card_money.setTextSize(18);
                viewHolder.item_tv_card_money.setTextColor(Color.parseColor(mContext.getString(R.color.cyan)));
                viewHolder.item_tv_card_money.setText(StringUtil.matcherSearchText(true, Color.parseColor(mContext.getString(R.color.cyan)), "剩余: " + coupleCardInfo.getCard_cnt() + " 张", " 张"));
                viewHolder.item_tv_card_ori_price.setVisibility(View.VISIBLE);
                viewHolder.item_tv_card_ori_price.setText(coupleCardInfo.getExpire_time_str());
            }
        } else {
            if (!TextUtils.isEmpty(coupleCardInfo.getOri_price())) {
                viewHolder.item_tv_card_ori_price.setVisibility(View.VISIBLE);
                viewHolder.item_tv_card_ori_price.setText("原价:" + coupleCardInfo.getOri_price());
                viewHolder.item_tv_card_ori_price.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
            } else {
                viewHolder.item_tv_card_ori_price.setVisibility(View.GONE);
            }
            viewHolder.item_tv_card_money.setTextSize(24);
            viewHolder.item_tv_card_money.setText(StringUtil.matcherSearchText(true, Color.parseColor(mContext.getString(R.color.yellow)), coupleCardInfo.getCard_price() + "金币", "金币"));
        }
        if (mIsCpRoom) {
            viewHolder.item_tv_card_ori_price.setTextColor(Color.parseColor("#D8D8D8"));
        } else {
            viewHolder.item_tv_card_ori_price.setTextColor(Color.parseColor("#CBCDD3"));
        }
        Glide.with(mContext)
                .load(coupleCardInfo.getImg())
                .override(ScreenUtil.dip2px(120), ScreenUtil.dip2px(50))
                .into(viewHolder.item_iv_cp_card);
        if (defItem == position) {
            viewHolder.item_ll_card.setBackgroundResource(R.drawable.bg_cp_card_select);
            viewHolder.iv_cp_card_select.setVisibility(View.VISIBLE);
        } else {
            viewHolder.item_ll_card.setBackgroundResource(R.drawable.bg_cp_card_normal);
            viewHolder.iv_cp_card_select.setVisibility(View.GONE);
        }
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemListener != null) {
                    onItemListener.onClick(v, viewHolder.getLayoutPosition());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class PieceViewHolder extends RecyclerView.ViewHolder {
        LinearLayout item_ll_card;
        TextView item_tv_card_ori_price;
        TextView item_tv_card_money;
        ImageView iv_cp_card_select;
        ImageView item_iv_cp_card;

        public PieceViewHolder(@NonNull View itemView) {
            super(itemView);
            item_ll_card = itemView.findViewById(R.id.item_ll_card);
            item_tv_card_ori_price = itemView.findViewById(R.id.item_tv_card_ori_price);
            item_tv_card_money = itemView.findViewById(R.id.item_tv_card_money);
            iv_cp_card_select = itemView.findViewById(R.id.iv_cp_card_select);
            item_iv_cp_card = itemView.findViewById(R.id.item_iv_cp_card);


        }
    }
}