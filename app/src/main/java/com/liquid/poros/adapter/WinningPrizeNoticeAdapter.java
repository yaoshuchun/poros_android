package com.liquid.poros.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.liquid.poros.R;
import com.liquid.poros.adapter.base.BaseAdapter;
import com.liquid.poros.adapter.base.BaseViewHolder;
import com.liquid.poros.entity.GetedGiftOfBoxNotice;

import java.util.List;

public class WinningPrizeNoticeAdapter extends BaseAdapter<GetedGiftOfBoxNotice, WinningPrizeNoticeAdapter.WinningPrizeNoticeHolder> {

    public WinningPrizeNoticeAdapter(Context context, List<GetedGiftOfBoxNotice> datas) {
        super(context, datas);
    }

    @Override
    protected Class<WinningPrizeNoticeHolder> viewHoderClass() {
        return WinningPrizeNoticeHolder.class;
    }

    @Override
    protected int itemViewXmlId() {
        return R.layout.item_winning_prize_notice;
    }

    @Override
    protected WinningPrizeNoticeHolder createHoder(ViewGroup viewGroup) {
        return new WinningPrizeNoticeHolder(createItemView(R.layout.item_winning_prize_notice,viewGroup));
    }

    @Override
    public void onBindViewHolder(@NonNull WinningPrizeNoticeHolder viewHolder, int position, GetedGiftOfBoxNotice data) {
        RequestOptions roomOptions = new RequestOptions()
                .placeholder(R.mipmap.icon_default)
                .error(R.mipmap.icon_default);
        Glide.with(mContext)
                .load(data.gift_image)
                .apply(roomOptions)
                .into(viewHolder.iv_icon);
        viewHolder.tv_msg.setText(data.desc);
    }


    public static class WinningPrizeNoticeHolder extends BaseViewHolder{
        public ImageView iv_icon;
        public TextView tv_msg;
        public WinningPrizeNoticeHolder(@NonNull View itemView) {
            super(itemView);
            iv_icon = findViewById(R.id.iv_icon);
            tv_msg = findViewById(R.id.tv_msg);
        }
    }
}
