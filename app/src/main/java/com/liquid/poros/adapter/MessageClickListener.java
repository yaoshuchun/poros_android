package com.liquid.poros.adapter;

import android.view.View;

import com.liquid.im.kit.custom.AskPTAGiftAttachment;
import com.liquid.im.kit.custom.CpLevelUpInviteAttach;
import com.liquid.im.kit.custom.CpLevelUpSuccessAttach;
import com.liquid.poros.entity.IMMessageImpl;
import com.netease.nimlib.sdk.msg.constant.MsgDirectionEnum;
import com.netease.nimlib.sdk.msg.model.IMMessage;

/**
 * 私聊监听
 */
public interface MessageClickListener {
    //超级CP邀请(宝箱&钥匙相关)
    void onInviteCoupleAction(int code, String cp_order_id, MsgDirectionEnum direction,boolean isTrial);
    //成为正式好友
    void toBeNormalFriend();
    //语音开黑邀请上麦
    void onInviteCoupleGameAction();
    //充值
    void onRecharge();
    //邀请组CP
    void onShowCpCard();
    //长按消息
    void onLongClickMsg(View clickView, final IMMessage item);
    //头像点击
    void onAvatarClick();
    //重发消息
    void onResetSendMessage(IMMessage message);
    //语音消息已读
    void onReadMessage(IMMessage message);
    //CP宣言邀请晋级
    void onDeclarationInvite(CpLevelUpInviteAttach attach, IMMessage msg);
    //CP宣言晋级成功
    void onDeclarationInviteSuccess(CpLevelUpSuccessAttach attach,IMMessage msg);
    //购买虚拟形象
    void onVirtualBuy(AskPTAGiftAttachment askPTAGiftAttachment);
    //语音匹配
    void onAudioMatch(String audioMatchType);
    //拆礼盒玩法规则
    void onCheckPool();
    //打开拆礼盒弹窗
    void onCheckGiftBox();
}
