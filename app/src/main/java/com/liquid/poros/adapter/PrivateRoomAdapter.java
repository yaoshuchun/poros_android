package com.liquid.poros.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.liquid.poros.R;
import com.liquid.poros.entity.MicUserInfo;
import com.liquid.poros.entity.PrivateRoomListInfo;
import com.liquid.poros.helper.JinquanResourceHelper;
import com.liquid.poros.utils.ScreenUtil;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * 专属房间列表
 */
public class PrivateRoomAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<PrivateRoomListInfo.CoupleRoomInfo> mData = new ArrayList<>();
    private String mBanner;
    private Context mContext;
    private final int TAG_BANNER = 1;//轮播图
    private final int TAG_ROOM_LIST = 2;//列表内容

    public PrivateRoomAdapter(Context context) {
        mContext = context;
    }

    public void setBanner(String banner) {
        mBanner = banner;
    }

    public boolean isHeader(int position) {
        return position == 0;
    }

    /**
     * data的更新方法
     *
     * @param data
     */
    public void update(List<PrivateRoomListInfo.CoupleRoomInfo> data) {
        if (data != null) {
            mData.clear();
            mData.addAll(data);
            this.notifyDataSetChanged();
        }
    }

    /**
     * data的增加方法
     *
     * @param data
     */
    public void add(List<PrivateRoomListInfo.CoupleRoomInfo> data) {
        if (data != null) {
            mData.addAll(data);
            this.notifyDataSetChanged();
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case TAG_BANNER:
                return new ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.item_private_room_banner, parent, false));
            case TAG_ROOM_LIST:
                return new ContentViewHolder(LayoutInflater.from(mContext).inflate(R.layout.item_couple_room, parent, false));
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            ViewHolder viewHolder = (ViewHolder) holder;
            RequestOptions bannerOptions = new RequestOptions()
                    .placeholder(R.mipmap.img_default_avatar)
                    .error(R.mipmap.img_default_avatar);
            Glide.with(mContext)
                    .load(mBanner)
                    .apply(bannerOptions)
                    .into(viewHolder.iv_private_room_banner);
        } else if (holder instanceof ContentViewHolder) {
            ContentViewHolder contentViewHolder = (ContentViewHolder) holder;
            PrivateRoomListInfo.CoupleRoomInfo coupleRoomInfo = mData.get(position - 1);
            JinquanResourceHelper.getInstance(mContext).setBackgroundColorByAttr(contentViewHolder.ll_room_info, R.attr.custom_attr_app_bg);
            JinquanResourceHelper.getInstance(mContext).setBackgroundColorByAttr(contentViewHolder.ll_content, R.attr.custom_attr_app_bg);
            JinquanResourceHelper.getInstance(mContext).setTextColorByAttr(contentViewHolder.tv_anchor, R.attr.custom_attr_remark_text_color);
            JinquanResourceHelper.getInstance(mContext).setTextColorByAttr(contentViewHolder.tv_room_anchor_nickname, R.attr.custom_attr_main_title_color);
            holder.itemView.setTag(coupleRoomInfo.getAnchor().getUser_id());
            final int itemHeight = (((ScreenUtil.adjustWidgetWidth() - ScreenUtil.dip2px(20)))) / 2;
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) contentViewHolder.rl_container.getLayoutParams();
            params.height = itemHeight;
            //上麦用户信息
            if (coupleRoomInfo.getMic_user() != null && !TextUtils.isEmpty(coupleRoomInfo.getMic_user().getUser_id())) {
                MicUserInfo micUserInfo = coupleRoomInfo.getMic_user();
                RequestOptions roomOptions = new RequestOptions()
                        .placeholder(R.mipmap.icon_default)
                        .error(R.mipmap.icon_default)
                        .transform(new CenterCrop(), new RoundedCorners(ScreenUtil.dip2px(8)));
                Glide.with(mContext)
                        .load(micUserInfo.getAvatar_url())
                        .apply(roomOptions)
                        .override(ScreenUtil.dip2px(100))
                        .into(contentViewHolder.iv_room_cover);
                contentViewHolder.ll_anchor_container.setVisibility(View.VISIBLE);
                contentViewHolder.tv_room_mic_nickname.setVisibility(TextUtils.isEmpty(micUserInfo.getNick_name()) ? View.GONE : View.VISIBLE);
                contentViewHolder.tv_room_mic_nickname.setText(micUserInfo.getNick_name());
                //年龄
                contentViewHolder.tv_room_mic_age.setBackgroundResource(micUserInfo.getGender() == 1 ? R.drawable.bg_color_00aaff_16 : R.drawable.bg_color_ff75a3_16);
                contentViewHolder.tv_room_mic_age.setVisibility(TextUtils.isEmpty(micUserInfo.getAge()) ? View.GONE : View.VISIBLE);
                Drawable ageDrawable = mContext.getResources().getDrawable(micUserInfo.getGender() == 1 ? R.mipmap.icon_male : R.mipmap.icon_female);
                ageDrawable.setBounds(0, 0, ageDrawable.getMinimumWidth(), ageDrawable.getMinimumHeight());
                contentViewHolder.tv_room_mic_age.setCompoundDrawables(ageDrawable, null, null, null);
                contentViewHolder.tv_room_mic_age.setCompoundDrawablePadding(3);
                contentViewHolder.tv_room_mic_age.setText(micUserInfo.getAge());
                //想玩 标签
                if (micUserInfo.getGames() != null && micUserInfo.getGames().size() > 0) {
                    SpannableStringBuilder spannableString = new SpannableStringBuilder(mContext.getString(R.string.cp_want_play));
                    for (int i = 0; i < micUserInfo.getGames().size(); i++) {
                        spannableString.append(" ");
                        if (i == micUserInfo.getGames().size() - 1) {
                            spannableString.append(micUserInfo.getGames().get(i));
                        } else {
                            spannableString.append(micUserInfo.getGames().get(i) + " |");
                        }
                    }
                    contentViewHolder.tv_want_game.setVisibility(View.VISIBLE);
                    contentViewHolder.tv_want_game.setBackgroundResource(R.drawable.bg_color_4d000000_9);
                    contentViewHolder.tv_want_game.setText(spannableString);
                } else {
                    contentViewHolder.tv_want_game.setVisibility(View.GONE);
                }
            } else {
                contentViewHolder.ll_anchor_container.setVisibility(View.GONE);
                contentViewHolder.ll_room_live_tag.setVisibility(View.GONE);
                RequestOptions roomOptions = new RequestOptions()
                        .placeholder(R.mipmap.icon_default)
                        .error(R.mipmap.icon_default)
                        .transform(new CenterCrop(), new RoundedCorners(ScreenUtil.dip2px(8)));
                Glide.with(mContext)
                        .load(coupleRoomInfo.getRoom_cover())
                        .apply(roomOptions)
                        .into(contentViewHolder.iv_room_cover);
            }
            contentViewHolder.tv_private_room_tip.setVisibility(View.VISIBLE);
            //房间主持人信息
            contentViewHolder.tv_room_anchor_nickname.setText(coupleRoomInfo.getAnchor().getNick_name());
            RequestOptions options = new RequestOptions()
                    .placeholder(R.mipmap.img_default_avatar)
                    .apply(RequestOptions.bitmapTransform(new CircleCrop()))
                    .override(ScreenUtil.dip2px(24), ScreenUtil.dip2px(24))
                    .error(R.mipmap.img_default_avatar);
            Glide.with(mContext)
                    .load(coupleRoomInfo.getAnchor().getAvatar_url())
                    .apply(options)
                    .into(contentViewHolder.iv_room_anchor_head);
            contentViewHolder.ll_content.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        mListener.onItemClick(coupleRoomInfo.getRoom_id());
                    }
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        return isHeader(position) ? TAG_BANNER : TAG_ROOM_LIST;
    }

    @Override
    public int getItemCount() {
        return mData.size() + 1;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_private_room_banner;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            iv_private_room_banner = itemView.findViewById(R.id.iv_private_room_banner);
        }
    }

    class ContentViewHolder extends RecyclerView.ViewHolder {
        LinearLayout ll_room_info;
        LinearLayout ll_content;
        TextView tv_anchor;
        TextView tv_room_anchor_nickname;
        ImageView iv_room_cover;
        RelativeLayout rl_container;
        LinearLayout ll_anchor_container;
        TextView tv_room_mic_nickname;
        TextView tv_room_mic_age;
        TextView tv_want_game;
        TextView tv_private_room_tip;
        ImageView iv_room_anchor_head;
        LinearLayout ll_room_live_tag;

        public ContentViewHolder(@NonNull View itemView) {
            super(itemView);
            ll_room_info = itemView.findViewById(R.id.ll_room_info);
            ll_content = itemView.findViewById(R.id.ll_content);
            tv_anchor = itemView.findViewById(R.id.tv_anchor);
            tv_room_anchor_nickname = itemView.findViewById(R.id.tv_room_anchor_nickname);
            iv_room_cover = itemView.findViewById(R.id.iv_room_cover);
            rl_container = itemView.findViewById(R.id.rl_container);
            ll_anchor_container = itemView.findViewById(R.id.ll_anchor_container);
            tv_room_mic_nickname = itemView.findViewById(R.id.tv_room_mic_nickname);
            tv_room_mic_age = itemView.findViewById(R.id.tv_room_mic_age);
            tv_want_game = itemView.findViewById(R.id.tv_want_game);
            tv_private_room_tip = itemView.findViewById(R.id.tv_private_room_tip);
            iv_room_anchor_head = itemView.findViewById(R.id.iv_room_anchor_head);
            ll_room_live_tag = itemView.findViewById(R.id.ll_room_live_tag);
        }
    }

    private PrivateRoomAdapter.OnListener mListener;

    public void setOnItemListener(PrivateRoomAdapter.OnListener listener) {
        this.mListener = listener;
    }

    public interface OnListener {
        void onItemClick(String roomId);

    }
}