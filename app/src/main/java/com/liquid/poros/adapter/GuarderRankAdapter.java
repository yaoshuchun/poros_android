package com.liquid.poros.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.liquid.poros.R;
import com.liquid.poros.entity.GuardRankerInfo;
import java.util.List;

public class GuarderRankAdapter extends RecyclerView.Adapter<GuarderRankAdapter.GiftViewHolder>{
    private Context mContext;
    private List<GuardRankerInfo> datas;
    private RoundedCorners roundedCorners;
    private RequestOptions options;

    public GuarderRankAdapter(Context context) {
        mContext = context;
        roundedCorners= new RoundedCorners(100);
        options = new RequestOptions()
                .placeholder(R.mipmap.icon_default)
                .error(R.mipmap.icon_default)
                .transform(new CenterCrop(),roundedCorners);
    }

    public void setDatas(List<GuardRankerInfo> datas) {
        this.datas = datas;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public GiftViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflate = LayoutInflater.from(mContext).inflate(R.layout.item_guarder_rank_list, parent, false);
        return new GiftViewHolder(inflate);
    }

    @Override
    public void onBindViewHolder(@NonNull GiftViewHolder holder, int position) {
        GuardRankerInfo rankerInfo = datas.get(position);
        holder.coinNum.setText(String.valueOf((int)rankerInfo.getCoins()));
        holder.guarderName.setText(rankerInfo.getNick_name());
        GuardGiftAdapter adapter = new GuardGiftAdapter(mContext,rankerInfo.getGift_list());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        holder.rvGiftList.setLayoutManager(linearLayoutManager);
        holder.rvGiftList.setAdapter(adapter);
        Glide.with(mContext).applyDefaultRequestOptions(options)
                .load(rankerInfo.getAvatar_url())
                .into(holder.rankUserAvatar);
        if (position == 0) {
            Glide.with(mContext).load(R.mipmap.ic_guard_tag).into(holder.ivRankNum);
            holder.tvRankNum.setVisibility(View.GONE);
            holder.ivRankNum.setVisibility(View.VISIBLE);
        }else if (position == 1) {
            Glide.with(mContext).load(R.mipmap.ic_guard_scond).into(holder.ivRankNum);
            holder.tvRankNum.setVisibility(View.GONE);
            holder.ivRankNum.setVisibility(View.VISIBLE);
        }else if (position == 2) {
            Glide.with(mContext).load(R.mipmap.ic_guard_third).into(holder.ivRankNum);
            holder.tvRankNum.setVisibility(View.GONE);
            holder.ivRankNum.setVisibility(View.VISIBLE);
        }else {
            if (position >= 9) {
                holder.tvRankNum.setText(String.valueOf(position + 1));
            }else {
                holder.tvRankNum.setText("0" + (position + 1));
            }
            holder.tvRankNum.setVisibility(View.VISIBLE);
            holder.ivRankNum.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return datas == null ? 0 : datas.size();
    }

    public static class GiftViewHolder extends RecyclerView.ViewHolder{
        TextView tvRankNum;
        ImageView ivRankNum;
        ImageView rankUserAvatar;
        TextView guarderName;
        RecyclerView rvGiftList;
        TextView coinNum;
        public GiftViewHolder(@NonNull View itemView) {
            super(itemView);
            tvRankNum = itemView.findViewById(R.id.tv_rank_num);
            ivRankNum = itemView.findViewById(R.id.iv_rank_num);
            rankUserAvatar = itemView.findViewById(R.id.rank_user_avatar);
            guarderName = itemView.findViewById(R.id.guarder_name);
            rvGiftList = itemView.findViewById(R.id.rv_gift_list);
            coinNum = itemView.findViewById(R.id.coin_num);
        }
    }
}
