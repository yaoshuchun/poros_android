package com.liquid.poros.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.liquid.poros.R;
import com.liquid.poros.entity.UserPhotoInfo;
import com.liquid.poros.utils.AccountUtil;
import com.liquid.poros.utils.ScreenUtil;
import com.liquid.poros.utils.StringUtil;
import com.liquid.poros.utils.ViewUtils;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * 相册选择
 */
public class PhotoManageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<UserPhotoInfo.Data.Photos> mData;
    private Context mContext;
    private final int IMG_SELECT = 0;//选择
    private final int IMG_CONTENT = 1;//图片

    public PhotoManageAdapter(Context context, List<UserPhotoInfo.Data.Photos> data) {
        mData = data;
        mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case IMG_SELECT:
                return new ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.item_user_img_select, parent, false));
            case IMG_CONTENT:
                return new ContentViewHolder(LayoutInflater.from(mContext).inflate(R.layout.item_photo_manage_img, parent, false));
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            ViewHolder viewHolder = (ViewHolder) holder;
            viewHolder.fl_img_select.setVisibility(mData.size() >= 20 ? View.GONE : View.VISIBLE);
        } else if (holder instanceof ContentViewHolder) {
            ContentViewHolder contentViewHolder = (ContentViewHolder) holder;
            RequestOptions options = new RequestOptions()
                    .placeholder(R.mipmap.icon_default)
                    .error(R.mipmap.icon_default)
                    .transform(new CenterCrop(), new RoundedCorners(ScreenUtil.dip2px(6)));
            Glide.with(mContext)
                    .load(mData.get(position).getPath())
                    .override(ScreenUtil.dip2px(78), ScreenUtil.dip2px(78))
                    .apply(options)
                    .into(contentViewHolder.iv_select);
            contentViewHolder.iv_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (ViewUtils.isFastClick()) {
                        return;
                    }
                    if (OnListener != null) {
                        OnListener.onDeleteImg(position, mData.get(position).getPhoto_id());
                    }
                }
            });
            contentViewHolder.iv_delete.setVisibility(View.VISIBLE);
            contentViewHolder.rl_select.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (ViewUtils.isFastClick()) {
                        return;
                    }
                    if (OnListener != null) {
                        OnListener.onItemClick(position, mData);
                    }
                }
            });
            if (mData.get(position).getType() == 2) {
                contentViewHolder.iv_video_cover.setVisibility(View.VISIBLE);
            } else {
                contentViewHolder.iv_video_cover.setVisibility(View.GONE);
            }
        }
        final int itemHeight = (((ScreenUtil.adjustWidgetWidth() - ScreenUtil.dip2px(45)))) / 4;
        holder.itemView.getLayoutParams().height = itemHeight;
        holder.itemView.getLayoutParams().width = itemHeight;
    }

    @Override
    public int getItemViewType(int position) {
        if (mData.size() >= 20) {
            return IMG_CONTENT;
        } else {
            if (position + 1 == getItemCount()) {
                return IMG_SELECT;
            } else {
                return IMG_CONTENT;
            }
        }
    }

    @Override
    public int getItemCount() {
        if (mData.size() >= 20) {
            return mData.size();
        } else {
            return mData.size() + 1;
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        FrameLayout fl_img_select;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            fl_img_select = itemView.findViewById(R.id.fl_img_select);
            fl_img_select.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (OnListener != null) {
                        OnListener.onSelectImg();
                    }
                }
            });
        }
    }

    class ContentViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout rl_select;
        ImageView iv_select;
        ImageView iv_delete;
        ImageView iv_video_cover;

        public ContentViewHolder(@NonNull View itemView) {
            super(itemView);
            rl_select = itemView.findViewById(R.id.rl_select);
            iv_select = itemView.findViewById(R.id.iv_select);
            iv_delete = itemView.findViewById(R.id.iv_delete);
            iv_video_cover = itemView.findViewById(R.id.iv_video_cover);
        }
    }

    private PhotoManageAdapter.OnListener OnListener;

    public void setOnItemListener(PhotoManageAdapter.OnListener onListener) {
        this.OnListener = onListener;
    }

    public interface OnListener {
        void onSelectImg();

        void onItemClick(int pos, List<UserPhotoInfo.Data.Photos> data);

        void onDeleteImg(int pos, String photoId);
    }
}