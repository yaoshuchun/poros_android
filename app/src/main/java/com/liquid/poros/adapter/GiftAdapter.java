package com.liquid.poros.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.liquid.poros.R;
import com.liquid.poros.entity.MsgUserModel;
import java.util.List;

public class GiftAdapter extends BaseAdapter {
    private int selectedPos;
    private Context context;
    private List<MsgUserModel.GiftBean> giftBeans;
    private int startIndex;
    private OnGiftSelectListener listener;

    public GiftAdapter(Context mContext, List<MsgUserModel.GiftBean> giftBeans, int startIndex,int selectedPos) {
        this.context = mContext;
        this.giftBeans = giftBeans;
        this.startIndex = startIndex;
        this.selectedPos = selectedPos;
    }

    public void setListener(OnGiftSelectListener listener) {
        this.listener = listener;
    }

    public int getCount() {
        return (giftBeans.size() - startIndex) > 8 ? 8 : (giftBeans.size() - startIndex);
    }

    public void setSelectedPos(int selectedPos) {
        this.selectedPos = selectedPos;
        notifyDataSetChanged();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position + startIndex;
    }

    @SuppressLint({"ViewHolder", "InflateParams"})
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(context).inflate(R.layout.nim_gift_item, null);
        ImageView emojiThumb = convertView.findViewById(R.id.img_gift);
        TextView giftName = convertView.findViewById(R.id.gift_name);
        TextView coinCount = convertView.findViewById(R.id.coin_count);
        giftName.setText(giftBeans.get(position + startIndex).name);
        coinCount.setText(giftBeans.get(position + startIndex).price + "金币");
        Glide.with(context).load(giftBeans.get(position + startIndex).icon).into(emojiThumb);
        if (selectedPos == position + startIndex) {
            convertView.setBackgroundResource(R.drawable.bg_gift_selector);
        }else {
            convertView.setBackgroundColor(context.getResources().getColor(R.color.transparent));
        }
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedPos = position + startIndex;
                if (listener != null) {
                    listener.select(selectedPos,startIndex);
                    notifyDataSetChanged();
                }
            }
        });
        return convertView;
    }

    public interface OnGiftSelectListener{
        void select(int pos,int startIndex);
    }
}