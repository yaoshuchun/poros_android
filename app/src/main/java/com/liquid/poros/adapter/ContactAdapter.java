package com.liquid.poros.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.request.RequestOptions;
import com.liquid.poros.R;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.entity.Contacts;
import com.liquid.poros.helper.JinquanResourceHelper;
import com.liquid.poros.ui.activity.user.SessionActivity;
import com.liquid.poros.utils.ContactComparatorUtil;
import com.liquid.poros.utils.ScreenUtil;
import com.liquid.poros.utils.ViewUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import androidx.recyclerview.widget.RecyclerView;


public class ContactAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private LayoutInflater mLayoutInflater;
    private Context mContext;
    private String[] mContactNames; // 联系人名称字符串数组
    private List<Contacts> resultList; // 最终结果（包含分组的字母）
    private List<String> characterList; // 字母List
    private int mPosition;
    private Listener mListener;

    public enum ITEM_TYPE {
        ITEM_TYPE_DIVIDER,
        ITEM_TYPE_CHARACTER,
        ITEM_TYPE_CONTACT
    }

    public ContactAdapter(Context context) {
        mContext = context;
        mLayoutInflater = LayoutInflater.from(context);
    }

    private List<Contacts> oriContacts;

    public void updateData(String[] contactNames, List<Contacts> data) {
        mContactNames = contactNames;
        oriContacts = data;
        handleContact();
    }

    private void handleContact() {
        // 联系人名称List（转换成拼音）
        List<String> mContactList = new ArrayList<>();
        Map<String, Contacts> map = new HashMap<>();

        for (int i = 0; i < mContactNames.length; i++) {
            Contacts c = oriContacts.get(i);
            String pinyin = mContactNames[i] + "_" + c.getUser_id();
            map.put(pinyin, oriContacts.get(i));
            mContactList.add(pinyin);
        }
        Collections.sort(mContactList, new ContactComparatorUtil());

        resultList = new ArrayList<>();
        characterList = new ArrayList<>();

        boolean isFirst = false;
        for (int i = 0; i < mContactList.size(); i++) {
            String name = mContactList.get(i);
            if (!TextUtils.isEmpty(name)) {
                String character = (name.charAt(0) + "").toUpperCase(Locale.ENGLISH);
                if (!characterList.contains(character)) {
                    if (character.hashCode() >= "A".hashCode() && character.hashCode() <= "Z".hashCode()) { // 是字母
                        characterList.add(character);
                        if (isFirst) {
                            resultList.add(new Contacts(character, ITEM_TYPE.ITEM_TYPE_DIVIDER.ordinal()));
                        }
                        resultList.add(new Contacts(character, ITEM_TYPE.ITEM_TYPE_CHARACTER.ordinal()));
                        isFirst = true;
                    } else {
                        if (!characterList.contains("#")) {
                            characterList.add("#");
                            resultList.add(new Contacts(character, ITEM_TYPE.ITEM_TYPE_DIVIDER.ordinal()));
                            resultList.add(new Contacts("#", ITEM_TYPE.ITEM_TYPE_CHARACTER.ordinal()));
                        }
                    }
                }
            }

            Contacts contact = map.get(name);
            contact.setmType(ITEM_TYPE.ITEM_TYPE_CONTACT.ordinal());
            resultList.add(contact);
            notifyDataSetChanged();
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == ITEM_TYPE.ITEM_TYPE_CHARACTER.ordinal()) {
            return new CharacterHolder(mLayoutInflater.inflate(R.layout.item_character, parent, false));
        } else if (viewType == ITEM_TYPE.ITEM_TYPE_DIVIDER.ordinal()) {
            return new DividerHolder(mLayoutInflater.inflate(R.layout.item_divider, parent, false));
        } else {
            return new ContactHolder(mLayoutInflater.inflate(R.layout.item_contact, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        JinquanResourceHelper.getInstance(mContext).setBackgroundColorByAttr(holder.itemView, R.attr.custom_attr_app_bg);
        if (holder instanceof CharacterHolder) {
            ((CharacterHolder) holder).mTextView.setText(resultList.get(position).getmName());
        } else if (holder instanceof ContactHolder) {
            final ContactHolder contactHolder = (ContactHolder) holder;
            contactHolder.mTextView.setText(resultList.get(position).getNick_name());
            RequestOptions options = new RequestOptions()
                    .placeholder(R.mipmap.img_default_avatar)
                    .error(R.mipmap.img_default_avatar);
            Glide.with(mContext)
                    .load(resultList.get(position).getAvatar_url())
                    .override(ScreenUtil.dip2px(50), ScreenUtil.dip2px(50))
                    .apply(RequestOptions.bitmapTransform(new CircleCrop()))
                    .apply(options)
                    .into(contactHolder.contact_head);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (ViewUtils.isFastClick()) {
                        return;
                    }
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.SESSION_ID, resultList.get(position).getUser_id());
                    mContext.startActivity(new Intent(mContext, SessionActivity.class).putExtras(bundle));
                }
            });
            holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    mPosition = position;
                    if (mListener != null) {
                        mListener.onItemLongClicked(resultList.get(mPosition).getUser_id(), contactHolder.mTextView, holder.itemView);
                    }
                    return true;
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        return resultList.get(position).getmType();
    }

    @Override
    public int getItemCount() {
        return resultList == null ? 0 : resultList.size();
    }

    public class CharacterHolder extends RecyclerView.ViewHolder {
        TextView mTextView;

        CharacterHolder(View view) {
            super(view);
            mTextView = view.findViewById(R.id.character);
        }
    }

    public class DividerHolder extends RecyclerView.ViewHolder {

        DividerHolder(View view) {
            super(view);
        }
    }


    public class ContactHolder extends RecyclerView.ViewHolder {
        TextView mTextView;
        ImageView contact_head;

        ContactHolder(View view) {
            super(view);
            contact_head = view.findViewById(R.id.contact_head);
            mTextView = (TextView) view.findViewById(R.id.contact_name);
        }
    }

    public int getScrollPosition(String character) {
        if (resultList != null && characterList.contains(character)) {
            for (int i = 0; i < resultList.size(); i++) {
                if (!TextUtils.isEmpty(resultList.get(i).getmName()) && resultList.get(i).getmName().equals(character)) {
                    return i;
                }
            }
        }

        return -1; // -1不会滑动
    }

    public interface Listener {
        void onItemLongClicked(String sessionId, View view, View contentView);
    }

    public void setOnListening(Listener listener) {
        mListener = listener;
    }

}
