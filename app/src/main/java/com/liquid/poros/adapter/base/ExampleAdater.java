package com.liquid.poros.adapter.base;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.liquid.poros.R;
import com.liquid.poros.entity.MsgUserModel;

import java.util.List;

/**
 * 废弃废弃废弃废弃废弃废弃废弃废弃废弃废弃废弃
 *
 * 废弃
 * @De
 * BaseAdapter使用示例
 */
@Deprecated
public class ExampleAdater extends BaseAdapter<MsgUserModel.UserInfo, ExampleAdater.MyViewHoder>{


    public ExampleAdater(Context context, List<MsgUserModel.UserInfo> datas) {
        super(context, datas);
    }

    /**
     * 返回 自定义ViewHolder class
     * @return
     */
    @Override
    protected Class<MyViewHoder> viewHoderClass() {
        return MyViewHoder.class;
    }

    /**
     * itemViewXmlId
     * @return
     */
    @Override
    protected int itemViewXmlId() {
        return R.layout.item_example;
    }

    @Override
    protected MyViewHoder createHoder(ViewGroup viewGroup) {
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHoder viewHolder, int position, MsgUserModel.UserInfo data) {

    }


    public static class MyViewHoder extends BaseViewHolder {
        public MyViewHoder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
