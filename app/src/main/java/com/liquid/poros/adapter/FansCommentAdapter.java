package com.liquid.poros.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.liquid.base.event.EventCenter;
import com.liquid.poros.R;
import com.liquid.poros.constant.Constants;
import com.liquid.poros.entity.CommentInfo;
import com.liquid.poros.utils.MoreTextUtil;

import java.util.List;

import de.greenrobot.event.EventBus;

public class FansCommentAdapter extends RecyclerView.Adapter<FansCommentAdapter.FansCommentViewHolder>{

    private Activity mContext;
    private List<CommentInfo> datas;
    private RoundedCorners roundedCorners;
    private RequestOptions options;
    private String sessionId;

    public FansCommentAdapter(Context context) {
        mContext = (Activity) context;
        roundedCorners= new RoundedCorners(100);
        options= new RequestOptions()
                .placeholder(R.mipmap.icon_default)
                .error(R.mipmap.icon_default)
                .transform(new CenterCrop(),roundedCorners);
    }

    public void setDatas(List<CommentInfo> datas) {
        this.datas = datas;
        notifyDataSetChanged();
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    @NonNull
    @Override
    public FansCommentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflate = LayoutInflater.from(mContext).inflate(R.layout.item_comment, parent, false);
        return new FansCommentViewHolder(inflate);
    }

    @Override
    public void onBindViewHolder(@NonNull FansCommentViewHolder holder, int position) {
        CommentInfo commentInfo = datas.get(position);
        Glide.with(mContext).load(commentInfo.avatar_url).apply(options).into(holder.personAvatar);
        holder.newsTime.setText(commentInfo.create_time);
        MoreTextUtil.getInstance().getLastIndexForLimit(holder.textNews,3,commentInfo.text);
        holder.newsUserName.setText(commentInfo.nick_name);
        holder.personAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sessionId != null && sessionId.equals(commentInfo.user_id)) {
                    EventBus.getDefault().post(new EventCenter(Constants.EVENT_UPDATE_LIST_POSITION));
                    mContext.finish();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return datas == null? 0 : datas.size();
    }

    public static class FansCommentViewHolder extends RecyclerView.ViewHolder{
        View item;
        ImageView personAvatar;
        TextView newsUserName;
        TextView newsTime;
        TextView textNews;
        public FansCommentViewHolder(@NonNull View itemView) {
            super(itemView);
            item = itemView;
            personAvatar = itemView.findViewById(R.id.person_avatar);
            newsUserName = itemView.findViewById(R.id.news_user_name);
            newsTime = itemView.findViewById(R.id.news_time);
            textNews = itemView.findViewById(R.id.text_news);
        }
    }
}
