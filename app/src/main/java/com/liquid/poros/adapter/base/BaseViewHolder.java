package com.liquid.poros.adapter.base;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class BaseViewHolder extends RecyclerView.ViewHolder{


    public BaseViewHolder(@NonNull View itemView) {
        super(itemView);
    }

    protected <T extends View>  T findViewById(int resId){
        return itemView.findViewById(resId);
    }
}