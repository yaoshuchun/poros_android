package com.liquid.poros.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.liquid.library.retrofitx.RetrofitX
import com.liquid.poros.entity.GetedGiftOfBoxNoticeInfo
import com.liquid.poros.entity.GiftBoxInfo
import com.liquid.poros.entity.GiftOfBox
import com.liquid.poros.http.ApiUrl
import com.liquid.poros.http.observer.ObjectObserver
import com.liquid.poros.http.utils.getPoros
import com.liquid.poros.http.utils.postPoros
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import java.util.concurrent.atomic.AtomicBoolean

class GiftBoxVM : ViewModel() {
    companion object{
        const val RECORD_TYPE = "record_type"
        const val TYPE_BOOST = "boost"
    }
    //中间通告数据
    val getedGiftOfBoxNoticeInfoData: MutableLiveData<GetedGiftOfBoxNoticeInfo> by lazy { MutableLiveData<GetedGiftOfBoxNoticeInfo>() }
    //拆礼盒
    val openGiftBoxData: MutableLiveData<GiftOfBox> by lazy { MutableLiveData<GiftOfBox>() }
    //礼盒信息
    val giftBoxInfoData: MutableLiveData<GiftBoxInfo> by lazy { MutableLiveData<GiftBoxInfo>() }
    var isOpening = AtomicBoolean(false)
    /**
     * 中奖通告
     */
    fun getNotices(record_type: String?) {
        RetrofitX.url(ApiUrl.WINNING_PRIZE_NOTICES)
                .param(RECORD_TYPE, record_type)
                .getPoros()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : ObjectObserver<GetedGiftOfBoxNoticeInfo>() {
                    override fun failed(result: GetedGiftOfBoxNoticeInfo) {}
                    override fun success(result: GetedGiftOfBoxNoticeInfo) {
                        getedGiftOfBoxNoticeInfoData.postValue(result)
                    }
                })
    }


    /**
     * 拆礼盒
     */
    fun openGiftBox(from_user_id: String){
        isOpening.set(true)
        RetrofitX.url(ApiUrl.OPEN_GIFT_BOX)
                .param("from_user_id", from_user_id)
                .postPoros()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : ObjectObserver<GiftOfBox>() {
                    override fun failed(result: GiftOfBox) {
                        isOpening.set(false)
                        openGiftBoxData.postValue(result)
                    }

                    override fun success(result: GiftOfBox) {
                        isOpening.set(false)
                        openGiftBoxData.postValue(result)
                    }
                })
    }

    /**
     * 礼盒信息
     */
    fun getGiftBoxInfo(from_user_id: String){
        RetrofitX.url(ApiUrl.OPEN_GIFT_BOX_INFO)
                .param("from_user_id", from_user_id)
                .getPoros()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        object : ObjectObserver<GiftBoxInfo>() {
                            override fun failed(result: GiftBoxInfo) {}
                            override fun success(result: GiftBoxInfo) {
                                result.run {
                                    reward_info?.run {
                                        val stringBuffer = StringBuffer()
                                        for (record in record_list) {
                                            stringBuffer.append(record.desc?:"")
                                                    .append("\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000")
                                        }
                                        recordListStr = stringBuffer.toString()
                                    }
                                }
                                giftBoxInfoData.postValue(result)
                            }
                        })
    }

    /**
     * 拆礼盒后
     */
    fun handleGiftBoxInfoAfterOpen(){
        giftBoxInfoData.postValue(giftBoxInfoData.value?.apply {
            gift_box_count -= 1
            gift_box_key_count -= open_box_need_key
        })
    }




}