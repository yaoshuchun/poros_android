package com.liquid.poros.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.liquid.library.retrofitx.RetrofitX
import com.liquid.library.retrofitx.utils.LogUtils
import com.liquid.poros.entity.BaseBean
import com.liquid.poros.entity.RoomLiveInfo
import com.liquid.poros.http.ApiUrl
import com.liquid.poros.http.observer.ObjectObserver
import com.liquid.poros.http.utils.postPoros

class LiveRoomDiversionViewModel : ViewModel() {
    val roomInfoDataSuccess: MutableLiveData<RoomLiveInfo> by lazy { MutableLiveData<RoomLiveInfo>() }
    val roomInfoDataFail: MutableLiveData<RoomLiveInfo> by lazy { MutableLiveData<RoomLiveInfo>() }

    /**
     * 获取房间信息
     */
    fun getLiveRoomInfo(roomId: String) {
        RetrofitX.url(ApiUrl.LIVEROOM_INFO_URL)
                .param("room_id", roomId)
                .postPoros()
                .subscribe(object : ObjectObserver<RoomLiveInfo>() {
                    override fun success(result: RoomLiveInfo) {
                        roomInfoDataSuccess.postValue(result)
                    }

                    override fun failed(result: RoomLiveInfo) {
                        roomInfoDataFail.postValue(result)
                    }
                })
    }
    fun diversionDialogClose() {
        RetrofitX.url(ApiUrl.ROOM_DIVERSION_CLOSE)
                .postPoros()
                .subscribe(object : ObjectObserver<BaseBean>(){
                    override fun success(result: BaseBean) {
                        LogUtils.d("ROOM_DIVERSION_CLOSE**"+result.code.toString())
                    }

                    override fun failed(result: BaseBean) {
                    }

                })
    }
}
