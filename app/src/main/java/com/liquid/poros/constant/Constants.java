package com.liquid.poros.constant;

import com.liquid.poros.BuildConfig;
import com.liquid.poros.entity.RechargeWayInfo;
import com.liquid.poros.utils.SharePreferenceUtil;

import java.util.List;

public class Constants {
    public static final String BASE_PRODUCT_URL = "https://poros.liquidnetwork.com/service/";
    public static final String BASE_DEVELOP_URL = "http://testporos.liquidnetwork.com/service/";
    public static final String BASE_B_DEVELOP_URL = "http://noah-test.liquidnetwork.com/";
    public static final String BASE_B_PRODUCT_URL = "https://cupid.diffusenetwork.com/";
    //    public static final String BASE_URL = BuildConfig.user_host_test_api ? BASE_DEVELOP_URL : BASE_PRODUCT_URL;
    //如果需要修改端口号，或者切换接口，建议去 我的--设置--测试入口--API切换中进行操作，不要再进行代码层面的改动
    public static final String BASE_URL = BuildConfig.DEBUG ? getSettingApi() : BASE_PRODUCT_URL;
    public static final String BASE_B_URL = BuildConfig.DEBUG ? BASE_B_DEVELOP_URL : BASE_B_PRODUCT_URL;
    public static String SHARE_GROUP_LINK = "https://liquid-h5.oss-cn-beijing.aliyuncs.com/box_shop/favour_index_test.html?group_id=%s#";

    //第三方id相关
    public static final String APP_ID = "1109220675";//腾讯appid
    public static final String TD_ID = "5C4BC26510BA401AA8765F901F01170E";//talkingdata id
    public static final String UMENG_TEST = "600577f2f1eb4f3f9b645f47";//友盟测试版
    public static final String UMENG_ONLINE = "600646b1f1eb4f3f9b64e7bb";//友盟线上版
    public static String FEEDBACK_GROUP = "618853382";

    public static final String BASE_URL_H5 = "http://static.diffusenetwork.com/";
    public static final String BASE_URL_RESOURCE = BASE_URL_H5;
    public static final String GIFT_BOX_RULE_URL = BASE_URL_H5 + "poros_h5/dev/index.html#/giftBoxC";
    public static String[] team_user_tips;
    public static String[] team_leader_tips;
    public static String[] live_tips;
    public static String[] cp_room_anchor_tips;
    public static String[] cp_room_watch_user_tips;
    public static String[] cp_room_mic_user_tips;
    public static String[] private_room_tips;
    public static String[] together_room_other_tips;
    public static String[] together_room_anchor_tips;
    public static String[] info_games;
    public static long SERVER_TIMESTAMP_DETLA;
    public static String cp_right_title;
    public static String cp_right_content;
    public static final String PARAMS_ORIENTATION = "ORIENTATION";
    public static final String PARAMS_CANCEL_ABLE = "CANCEL_ABLE";
    public static int show_feed;
    public static List<RechargeWayInfo> recharge_way;
    public static boolean is_sounds_vip;
    public static int buy_vip_money;
    public static String buy_vip_desc;
    public static int buy_vip_time_limit;
    public static int buy_vip_get_coin;
    public static boolean using_new_live_room; //使用新的视频房

    public static boolean show_age_level_continue;//true 显示跳过按钮
    public static boolean hide_makeup;//false 显示美妆按钮
    public static boolean hide_peking_user; //屏蔽北京用户

    public static int get_gift_box; // 首页是否展示 钥匙获得弹窗的实验分组  1/ 实验组   0标识对照组

    public static int cp_voice_chat_price = 1;

    public static final int EVENT_CODE_WECHAT_AUTH_FINISH = 4;

    public static final int EVENT_CODE_REPORT_SPEAKER = 28;
    public static final int EVENT_CODE_REPORT_NETWORK_QUALITY = 128;

    public static final int EVENT_CODE_LIVE_VOICE_START_FALIED = 41;

    public static final int EVENT_CODE_REFRESH_MESSAGE = 70;
    public static final int EVENT_CODE_UNREAD_MESSAGE = 71;

    public static final String KEY_CACHE_THEME_TAG = "Cache_themeTag";
    //导流类型
    public static final String VIDEO_ROOM_DIVERSION_TYPE = "auto_boy_cp_invite";
    public static String TYPE_DIVERSION = "diversion";
    public static String TYPE_CP_GROUP_INVITE = "boy_cp_group_invite";

    public static final int EVENT_CODE_INVITE_GAME = 78;
    public static final int EVENT_CODE_TOAST_MSG = 88;
    public static final int EVENT_CODE_PAY_RECHARGE = 89;
    public static final int EVENT_CODE_COIN_CHANGE = 90;
    public static final int EVENT_CODE_UPDATE_GAME_INFO_HONOR = 91;
    public static final int EVENT_CODE_UPDATE_GAME_INFO_PEACE = 92;
    public static final int EVENT_CODE_UPDATE_GAME_INFO_OTHER = 93;
    public static final int EVENT_CODE_DISPATCH_FEMALE = 94;
    public static final int EVENT_CODE_UPDATE_PHOTO = 95;
    public static final int EVENT_CODE_UPDATE_USER_INFO = 96;
    public static final int EVENT_CODE_CANCEL_APPLY_MIC = 97;
    public static final int EVENT_CODE_UPDATE_AUDIO_MATCH_GAME_INFO = 98;
    public static final int EVENT_CONNECT_INFO = 189;
    public static final int EVENT_UPDATE_PERSON_NEWS = 190;
    public static final int EVENT_UPDATE_LIST_POSITION = 191;
    public static final int EVENT_MATCHING_SUCCESS = 192;
    public static final int EVENT_CODE_ACCEPT_PLAY_IN_MATCH_CALL = 193;
    public static final int EVENT_CODE_REFUSE_PLAY_IN_MATCH_CALL = 194;
    public static final int EVENT_CODE_MATCH_INVITE_PLAY = 195;
    public static final int EVENT_CODE_ACCEPT_BY_REMOTE = 196;
    public static final int EVENT_CODE_REFUSE_BY_REMOTE = 197;
    public static final int EVENT_CODE_TIMEOUT_BY_REMOTE = 198;
    public static final int EVENT_CODE_UPDATE_GIFT_PACKAGE_STATUS = 199;
    public static final int EVENT_CODE_BUY_GIFT_PACKAGE_SUCCESS = 200;
    public static final int EVENT_CODE_FINISH_SESSION_ACTIVITY = 201;
    public static final int EVENT_CODE_LEAVE_AVCHAT_ROOM = 202;
    public static final int EVENT_CODE_BUY_COIN_NOT_ENOUGH = 203;
    public static final int EVENT_CODE_BUY_ONE_CENT_VIP = 204;
    //实名认证成功
    public static final int EVENT_CODE_REAL_NAME_SUCCESS = 205;
    //提现成功
    public static final int EVENT_CODE_WITHDRAW_SUCCESS = 206;
    public static final int EVENT_CODE_INVITE_OPEN_MIC = 208;
    //助力开始抽奖
    public static final int EVENT_CODE_OPEN_LOTTERY = 209;
    //助力抽奖券更新
    public static final int EVENT_CODE_UPDATE_BOOST_COUNT = 210;
    //盲盒介绍 -去看看
    public static final int EVENT_CODE_ENTRY_BLIND_BOX = 211;

    public static final int EVENT_CODE_REPORT_LOCALE_SPEAKER = 120;
    public static final String GROUP_ID = "group_id";

    public static final String USER_ID = "USER_ID";
    public static final String MALE_USER_ID = "user_id";
    public static final String PATH = "path";
    public static final String IS_INVITE_MIC = "is_invite_mic";
    public static final String MOMENT_ID = "moment_id";
    public static final String FEMALE_GUEST_ID = "female_guest_id";
    public static final String GUEST_ID = "guest_id";
    public static final String ROOM_ID = "room_id";
    public static final String SESSION_ID = "session_id";
    public static final String CP_ORDER_ID = "cp_order_id";
    public static final String INVITE_ID = "invite_id";
    public static final String IS_NEW = "is_new";
    public static final String P2P_CHAT_FROM = "p2p_chat_from";
    public static final String SHOW_RECHARGE = "show_recharge";
    public static final String SHOW_BUY_DIALOG = "show_buy_dialog";
    public static final String BUY_POSITION = "buy_position";
    public static final String FROM = "from";
    public static final String IS_SUPER_CP = "is_super_cp";
    public static final String DAY_NUMBER = "day_number";
    public static final String INNER_NOTICE = "inner_notice";
    public static final String OUTER_NOTICE = "outer_notice";
    public static final String POSITION = "position";
    public static final String ACT_PAGE = "act_page";
    public static final String TAG = "tag";
    //开黑卡购买埋点参数 buy_position 字段参数
    public static String message_join = "message_join";//: 私聊页面点击上麦
    public static String quick_join = "quick_join";//: 速配页面点击邀请/接受开黑
    public static String buy_card_outline_app = "buy_card_outline_app";//: 自动续卡失败下麦app内通知
    public static String buy_card_outline_notice = "buy_card_outline_notice";//: 自动续卡失败下麦通知栏通知

    public static final String PARAMS_TYPE = "PARAMS_TYPE";
    public static final String TARGET_PATH = "TARGET_PATH";
    public static final String PARAMS_MSG = "PARAMS_MSG";
    public static final String PARAMS_FORCE_UPDATE = "PARAMS_FORCE_UPDATE";
    public static final String PARAMS_DOWNLOAD_URL = "PARAMS_DOWNLOAD_URL";
    public static final String PARAMS_DOWNLOAD_MD5 = "PARAMS_DOWNLOAD_MD5";
    public static final String PARAMS_DOWNLOAD_BROWSER = "PARAMS_DOWNLOAD_BROWSER";
    public static final String ROOM_TYPE = "room_type";
    public static final String IM_ROOM_ID = "IM_ROOM_ID";
    public static final String IM_ROOM_TYPE = "IM_ROOM_TYPE";
    public static final String IM_ROOM_SOURCE = "CP_ROOM_DIVERSION";
    public static final String IS_VIP_MSG = "is_vip_msg";
    public static final String AUDIENCE_FROM = "audience_from";

    public static final String SHOW_AD_MSG = "show_ad_msg";
    public static final String POPUP_TYPE = "popup_type";

    public static final String PARAMS_IMAGE_TITLE = "IMAGE_TITLE";
    public static final String PARAMS_TOP_IMAGE_TITLE = "TOP_IMAGE_TITLE";
    public static final String PARAMS_TITLE = "TITLE";
    public static final String PARAMS_MIDDLE_TITLE = "MIDDLE_TITLE";
    public static final String PARAMS_DESC = "DESC";
    public static final String PARAMS_DESC_SEQUENCE = "DESC_SEQUENCE";
    public static final String PARAMS_WITH_CANCEL = "WITH_CANCEL";
    public static final String PARAMS_CONFIRM = "CONFIRM";
    public static final String PARAMS_CONFIRM_BACKGROUND = "CONFIRM_BACKGROUND";
    public static final String PARAMS_CANCEL = "CANCEL";

    public static final String USER_NICKNAME = "user_nickname";
    public static final String USER_CP_DESC = "user_cp_desc";
    public static final String USER_TAGS = "user_tags";
    public static final String USER_STATIC_GAME = "user_static_game";
    public static final String USER_GAME = "user_game";
    public static final String USER_GAME_MATCH_CONTROL = "user_game_match_control";
    public static final String USER_OTHER_GAME = "user_other_game";
    public static final String GIFT_RANK_PATH = "gift_rank_path";

    public static final String AUDIO_MATCH_INFO_TITLE = "audio_match_info_title";
    public static final String AUDIO_MATCH_INFO_TYPE = "audio_match_info_type";
    public static final String AUDIO_MATCH_INFO_ZONE = "audio_match_info_zone";
    public static final String AUDIO_MATCH_INFO_RANK = "audio_match_info_rank";
    public static final String AUDIO_MATCH_ENTRY_TYPE = "audio_match_entry_type";
    public static final int ENTRY_MESSAGE = 2;
    public static final int ENTRY_SQUARE = 1;

    public static final String VIDEO_MATCH_DATA_PASS_KEY = "match_info";
    //动态 - 播放动态语音
    public static final int FEED_ACTION_VOICE = 10;
    //动态 - 查看图片
    public static final int FEED_ACTION_PICTURE = 11;
    //动态来源 - 个人页动态列表
    public static final String SOURCE_FEED_LIST = "feed_list";
    //动态来源 - 个人页动态详情
    public static final String SOURCE_FEED_DETAIL = "feed_detail";
    //动态来源 - 广场动态列表
    public static final String SOURCE_FEED_SQUARE_LIST = "square_list";
    //动态来源 - 广场动态详情
    public static final String SOURCE_FEED_SQUARE_DETAIL = "square_detail";
    public static final String FEED_DETAILS_TYPE = "feed_details_type";
    //广场动态
    public static final String TYPE_FEED_SQUARE = "square";
    //个人中心动态
    public static final String TYPE_FEED_PERSONAL = "personal";
    //动态 - 点击条目跳转动态详情
    public static final String LAUNCH_FEED_TYPE_ITEM = "launch_feed_type_item";
    //动态 - 点击评论跳转动态详情
    public static final String LAUNCH_FEED_TYPE_COMMENT = "launch_feed_type_comment";
    //发起提现类型
    public static final String START_WITHDRAWAL_TYPE = "start_withdrawal_type";
    //助力中奖记录类型
    public static final String BOOST_LOTTERY_RECORD_TYPE = "boost_lottery_record_type";
    //视频速配
    public static final String ENTER_VIDEO_MATCH_SOURCE= "enter_video_match_source";
    public static final String ENTER_VIDEO_MATCH_1V1= "1v1";//自动触发
    public static final String ENTER_VIDEO_MATCH_SINGLE_CLICK= "single_click";//点击进入
    public static final String ENTER_VIDEO_MATCH_FAST_DATING= "fast_dating";//速配
    //视频速配 广场进入
    public static final String ENTER_VIDEO_MATCH_SQUARE= "square";
    //视频速配 非广场进入
    public static final String ENTER_VIDEO_MATCH_NO_SQUARE= "non_square";
    //盲盒中奖记录类型
    public static final String BLIND_BOX_RECORD_TYPE = "blind_box_record_type";
    public static final int ACTION_MATCH_STOP = 1002;
    public static final int ACTION_P2P_JOIN_ROOM = 1003;
    public static final int ACTION_MATCH_JOIN_ROOM_SUCCESS = 1004;
    //普通礼物
    public static final int GIFT_NORMAL = 0;
    //盲盒礼物
    public static final int GIFT_BLIND = 1;
    //视频call主动挂断
    public static final String VIDEO_CALL_ACTIVE_HANG_UP = "active_hang_up";
    //视频call系统挂断
    public static final String VIDEO_CALL_SYSTEM = "system";

    /**
     * 获取设置的API
     *
     * @return
     */
    private static String getSettingApi() {
        return SharePreferenceUtil.getString(SharePreferenceUtil.FILE_API_CONFIG, SharePreferenceUtil.KEY_API_URL, BASE_DEVELOP_URL);
    }

    /**
     * 获取是否是测试API
     *
     * C端的进圈：BuildConfig.user_host_test_api 取消 --  更换为 Constants.getUseTestApi()
     *
     * 后续所有判断是否是测试接口，请使用   Constants.getUseTestApi()
     *
     * @return
     */
    public static Boolean getUseTestApi() {
        return !BASE_URL.equals(BASE_PRODUCT_URL);
    }

}
