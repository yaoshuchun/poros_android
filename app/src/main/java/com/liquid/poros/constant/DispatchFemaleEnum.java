package com.liquid.poros.constant;

//足迹触发
public enum DispatchFemaleEnum {
    DIS_USER_ONLINE(3, "用户上线派单"),
    DIS_NEW_FEMALE(4, "新手女嘉宾派单"),
    DIS_BURST_ORDER(5, "爆灯派单"),
    DIS_USER_CENTER(6, "广场详情"),
    DIS_OUT_LIVE(7, "退出语音房"),
    DIS_SEND_MESSAGE(8, "发送消息");

    private int code;
    private String desc;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }


    DispatchFemaleEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static DispatchFemaleEnum typeOfValue(int code) {
        for (DispatchFemaleEnum e : values()) {
            if (e.equals(code)) {
                return e;
            }
        }
        return DIS_USER_CENTER;
    }
}