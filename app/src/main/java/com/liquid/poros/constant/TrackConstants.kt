package com.liquid.poros.constant

/**
 * Created by sundroid on 12/03/2019.
 */
object TrackConstants {
    //postion 取值 应用内通知 application
    const val POSI_APPLICATION = "application"

    //postion 取值 通知栏消息 notice
    const val POSI_NOTICE = "notice"
    const val PAGE_HOME = "home"
    const val PAGE_LOGIN = "login"
    const val TO_PAGE = "to_page"

    // feed jar 下载
    const val B_LAUNCH_APP = "b_launch_app"
    const val P_SPLASH = "P_SPLASH"
    const val B_CREATE_ROOM = "b_create_room"
    const val B_JOIN_ROOM = "b_join_room"
    const val PAGE_REPORT = "report"
    const val B_USER_LEAVE_ROOM = "b_user_leave_room"

    //    首页
    const val PAGE_INDEX = "index"
    const val GIFT_RESOURCE_UNDOWNLOAD = "gift_resource_undownload"

    //组CP
    const val PAGE_DISCOVER = "discover"

    //    消息列表
    const val PAGE_MESSAGE = "message"
    const val PAGE_MINE = "home_mine"

    //好友列表
    const val PAGE_CONTACTS = "contacts"

    //美颜设置
    const val PAGE_MAKE_UP = "p_make_up"

    //公共弹窗
    const val PAGE_COMMENT_DIALOG = "p_comment_dialog"

    //游戏信息
    const val PAGE_SUBMIT_GAME_INFO = "p_submit_game_info"

    //充值
    const val PAGE_RECHARGE = "p_recharge"

    //设置
    const val PAGE_SETTING = "p_setting"

    //点赞卡
    const val PAGE_LIKE_CARD_HISTORY = "p_like_card_history"

    //会话
    const val PAGE_SESSION = "p_session"
    const val P_RECHARGE_PAGE = "p_recharge_page"
    const val PAGE_BUY_CARD_POPUP = "buy_card_popup"
    const val B_BUY_POPUP = "p_buy_popup"
    const val B_USING_CAMERA = "b_using_camera"
    const val PAGE_RECEIVE_COUPLE_TEAM_GAME = "receive_couple_team_game"

    //关于进圈
    const val PAGE_ABOUT_POROS = "p_about_poros"

    //充值账单
    const val PAGE_RECHARGE_DETAILS = "p_recharge_details"

    //充值状态
    const val PAGE_RECHARGE_STATUS = "p_recharge_status"

    //个人中心 -编辑资料
    const val PAGE_USER_CENTER_SETTING = "p_user_center_setting"

    //个人中心 -设置资料
    const val PAGE_USER_INFO_UPDATE = "p_user_info_update"

    //欢迎页面
    const val PAGE_WELCOME = "welcome"
    const val PAGE_WEB_VIEW = "web_view"
    const val B_DOWNLOAD_COST = "b_download_cost"
    const val PAGE_UPDATE_NOTICE = "update_notice"
    const val PAGE_PRIZE_COIN = "prize_coin"
    const val PAGE_PRIZE_CARD = "prize_card"
    const val PAGE_GROUP_NOTIFICATION = "group_notification"
    const val PAGE_RECEIVE_INDEX_INVITE_CP = "receive_index_invite_cp"

    //剩余3分钟消息提醒-应用内通知曝光,剩余3分钟消息提醒-通知栏通知曝光,事件名一样，以position做区分
    const val B_REMIND_TEAM_JOIN_CARD_TIME_3MIN_EXPOSURE =
        "b_remind_team_join_card_time_3min_exposure"

    //埋点事件
    //点击登录按钮
    const val B_CLICK_LOGIN = "b_click_login"

    //登录成功
    const val B_LOGIN_SUCCESS = "b_login_success"
    const val B_CLICK_INVITE_MIC_MSG = "b_click_invite_mic_msg"

    //出现基本信息填写页面
    const val PAGE_USER_INFO = "user_info"

    //实验组信息填写下一步
    const val B_USER_NEXT_STEP = "b_user_next_step"

    //对照组信息填写第一步
    const val B_USER_FIRST_STEP = "b_user_first_step"

    //对照组信息填写第二步
    const val B_USER_SECOND_STEP = "b_user_second_step"

    //完成填写
    const val B_USER_SUCCESS = "b_user_success"

    //填写失败
    const val B_USER_FAILED = "b_user_failed"

    //出现奖励领取弹窗
    const val PAGE_REWARD = "reward"

    //关闭奖励领取弹窗
    const val B_REWARD_CLOSE = "b_reward_close"

    //进入广场页面
    const val PAGE_HOME_INDEX = "home_index"

    //进入视频房页面
    const val B_GO_TEAM = "b_go_team"

    //点击嘉宾
    const val B_SQUARE_USER = "b_square_user"

    //进入个人中心
    const val PAGE_USER_CENTER = "user_center"

    //个人中心点击播放语音
    const val B_VOICE = "b_voice"

    //个人中心查看图片
    const val B_PHOTO = "b_photo"

    //个人中心点击私聊按钮
    const val B_P2P_CHAT = "b_p2p_chat"

    //个人中心点击私聊进入语音房
    const val B_USER_CENTER_TO_LIVE_ROOM = "b_user_center_to_live_room"

    //进入组队页面
    const val PAGE_HOME_COUPLE = "home_couple"

    //语音房观看时长
    const val B_STAY_LIVE_ROOM = "b_stay_live_room"

    //点击房间
    const val B_TEAM_ROOM = "b_team_room"

    //进入组队房间
    const val PAGE_ROOM_LIVE = "room_live"

    //点击我的钱包
    const val B_MY_WALLET = "b_my_wallet"

    //点击上麦
    const val B_USING_MICROPHONE = "b_using_microphone"

    //成功上麦
    const val B_MICROPHONE_SUCCESS = "b_microphone_success"

    //点击使用开黑卡
    const val B_LIVE_USER_TEAM_CARD = "b_live_user_team_card"

    //成功使用开黑卡
    const val B_ROOM_CARD_SUCCESS = "b_room_card_success"

    //收到邀请弹窗
    const val P_INVITATION_CARD = "p_invitation_card"

    //同意邀请
    const val B_AGREEL_INVITATION = "b_agreel_invitation"

    //礼物按钮点击
    const val B_GIFT_BUTTON = "b_gift_button"

    //礼物数量选择界面弹开
    const val B_SELECT_GIFT_NUM_PANEL = "b_select_gift_num_panel"

    //礼物-超级CP入口点击
    const val B_GIFT_SUPER_CP_CLICK = "b_gift_super_cp_click"

    //赠送礼物
    const val B_PRESENT_GIFT = "b_present_gift"

    //赠送礼物成功
    const val B_PRESENT_GIFT_SUCCESS = "b_present_gift_success"

    //进入消息页面
    const val MESSAGE_FRIEND = "message_friend"

    //点击去组队按钮
    const val B_MESSAGE_GO_TEAM = "b_message_go_team"

    //点击会话
    const val B_MESSAGE = "b_message"

    //进入私聊页面
    const val P2P_PAGE = "p2p_page"

    //进入亲密页面
    const val INTIMATE_PAGE = "intimate_page"

    //金牌师傅转专属点击
    const val B_TRANSFER_EXCLUSIVE_CLICK = "b_transfer_exclusive_click"

    //背包页面曝光
    const val B_BAG_PAGE_EXPOSURE = "b_bag_page_exposure"

    //背包入口点击
    const val B_BAG_ENTRANCE_CLICK = "b_bag_entrance_click"

    //背包页面点击赠送
    const val B_BAG_PAGE_GIVE_CLICK = "b_bag_page_give_click"

    //背包页面合成
    const val B_BAG_PAGE_GIFT_COMPOSE_CLICK = "b_bag_page_gift_compose_click"

    //背包页面点击提现
    const val B_BAG_PAGE_CASH_CLICK = "b_bag_page_cash_click"

    //过期临时好友列表页面曝光
    const val B_CHAT_LIST_EXPIRED_LIST_PAGE = "b_chat_list_expired_list_page"

    //过期临时好友页面入口点击
    const val B_CHAT_LIST_EXPIRED_PAGE_ENTRANCE_CLICK = "b_chat_list_expired_page_entrance_click"

    //背包页面合成成功
    const val B_BAG_PAGE_GIFT_COMPOSE_SUCCESS_EXPOSURE = "b_bag_page_gift_compose_success_exposure"

    //背包页面点击赠送
    const val B_BAG_PAGE_GIVE_SUCCESS_EXPOSURE = "b_bag_page_give_success_exposure"

    //转专属确认弹框点击
    const val B_TRANSFER_EXCLUSIVE_POPUP_CLICK = "b_transfer_exclusive_popup_click"

    //动态详情页面
    const val MOMENT_DETAIL_PAGE = "moment_detail_page"

    //礼物榜页面
    const val GIFT_RANK_LIST_PAGE = "gift_rank_list_page"

    //点击上麦开黑
    const val B_P2P_MICROPHONE = "b_p2p_microphone"

    //点击邀请组CP
    const val B_GROUP_CP = "b_group_cp"

    //私聊页面购买栏出现次数
    const val P2P_PAGE_WINDOW = "p2p_page_window"

    //成功上麦
    const val B_P2P_MICROPHONE_SUCCESS = "b_p2p_microphone_success"

    //私聊页点击个人中心
    const val B_P2P_USER_CENTER = "b_p2p_user_center"

    //私聊页面点击头像
    const val B_P2P_USER_HEAD = "b_p2p_user_head"

    //进入钱包页面
    const val PAGE_WALLET = "wallet"

    //点击支付按钮
    const val B_PAYMENT = "b_payment"

    //支付成功弹窗出现
    const val P_PAYMENT_SUCCESS = "p_payment_success"

    //支付失败弹窗出现
    const val P_PAYMENT_FAILED = "p_payment_failed"

    //收到女嘉宾回复邀请弹窗
    const val P_REPLY_INVITATION = "p_payment_success"

    //同意女嘉宾回复邀请弹窗
    const val B_AGREEL_REPLY = "b_agreel_reply"

    //收到组队房间邀请弹窗
    const val P_ROOM_INVITATION = "p_room_invitation"

    //同意组队房间邀请弹窗
    const val B_AGREEL_ROOM = "b_agreel_room"
    const val B_ALLREADY_JOIN_ROOM = "b_allready_join_room"
    const val PAGE_RECEIVE_INVITE_CP_TEAM = "receive_invite_cp_team"
    const val B_START_NIM_ERROR = "b_start_nim_error"

    //实验组信息录入
    const val PAGE_TEST_USER_INFO = "test_user_info"
    const val PAGE_TEST_EDIT_GAME = "test_edit_game"
    const val PAGE_TEST_EDIT_OTHER_GAME = "test_edit_other_game"

    //视频房邀请上麦页面
    const val PAGE_LIVE_INVITE_MIC = "p_live_invite_mic"

    //悬浮球
    const val P_FLOAT_SHOW = "p_float_show"
    const val B_FLOAT_CANCEL = "b_float_cancel"
    const val B_FLOAT_OPEN = "b_float_open"
    const val P_FLOAT_SHOW_SUCCESS = "p_float_show_success"

    //足迹触发
    const val B_DISPATCH_FEMALE = "b_dispatch_female"

    //问题反馈弹窗
    const val P_FEEDBACK = "p_feedback"

    //上麦状态
    const val B_USER_MIC_STATUS = "b_user_mic_status"

    //网络状态变化
    const val B_USER_NET_CHANGE_STATUS = "b_user_net_change_status"

    //相册管理
    const val PAGE_PHOTO_MANAGE = "page_photo_manage"

    //相册管理 - 点击查看大图
    const val B_PHOTO_MANAGE = "b_photo_manage"

    //我的界面 - 点击语音
    const val B_MINE_VOICE = "b_mine_voice"

    //编辑其他游戏
    const val PAGE_EDIT_OTHER_GAME = "page_edit_other_game"

    //保存其他游戏
    const val B_SAVE_OTHER_GAME = "b_save_other_game"

    //连麦异常语音提示
    const val B_MIC_FAILED_AUDIO_TIP = "b_mic_failed_audio_tip"

    //退出视频房 取消上麦申请
    const val B_CANCEL_APPLY_MIC = "b_cancel_apply_mic"

    //出现视频上麦卡奖励领取弹窗
    const val PAGE_VIDEO_CARD_REWARD = "p_video_card_reward"

    //点击视频上麦卡领取
    const val B_VIDEO_CARD_REWARD = "b_video_card_reward"

    //cp等级页面
    const val PAGE_CP_LEVEL = "cp_level"

    //私聊Cp等级弹窗
    const val PAGE_CP_LEVEL_DIALOG = "p_cp_level_dialog"

    //私聊Cp等级弹窗实验
    const val PAGE_CP_LEVEL_TEST_DIALOG = "p_cp_level_test_dialog"

    //视频房界面
    //主持人视频区域点击
    const val B_HOST_VIDEO_CLICK = "b_host_video_click"

    //点击去约会
    const val B_GO_ENGAGEMENT_BUTTON = "b_go_engagement_button"

    //主持人右上角送礼物按钮
    const val B_HOST_GIFT_CLICK = "b_host_gift_click"

    //女嘉宾视频区域点击
    const val B_GUEST_VIDEO_CLICK = "b_guest_video_click"

    //专属房申请点击
    const val B_EXCLUSIVE_AFFIRM_APPLY_BUTTON = "b_exclusive_affirm_apply_button"

    //立即约会弹框曝光
    const val B_APPOINTMENT_DIALOG_EXPOSURE = "b_appointment_dialog_exposure"

    //立即约会
    const val B_EXCLUSIVE_ENGAGEMENT_BUTTON = "b_exclusive_engagement_button"

    //女嘉宾右上角送礼物按钮
    const val B_GUEST_GIFT_CLICK = "b_guest_gift_click"

    //女嘉宾右下角加好友点击
    const val B_ADD_FRIENDS_CLICK = "b_add_friends_click"

    //女嘉宾头像点击
    const val B_GUEST_AVATAR_CLICK = "b_guest_avatar_click"

    //点击女嘉宾详情页的送礼物加好友
    const val B_GUEST_DETAILS_ADD_FRIEND_CLICK = "b_guest_detail_add_friends_click"

    //视频房右下角礼物盒点击
    const val B_VIDEO_GIFT_CLICK = "b_video_gift_click"

    //申请上麦点击
    const val B_APPLY_ON_MIC_CLICK = "b_apply_on_mic_click"

    //接受上麦邀请点击
    const val B_ACCEPT_ON_MIC_CLICK = "b_accept_on_mic_click"

    //激活cp
    const val B_ACTIVATION_SUPER_CP_CLICK = "b_activation_super_cp_click"

    //激活得钥匙引导曝光
    const val B_ACTIVATION_KEY_EXPOSURE = "b_activation_key_exposure"

    //cp状态栏曝光
    const val B_SUPER_CP_NAV_EXPOSURE = "b_super_cp_nav_exposure"

    //男嘉宾接收丘比特次数
    const val B_USER_CUPID_COUNT = "b_user_cupid_count"

    //男嘉宾回复丘比特次数
    const val B_USER_CUPID_REPLY_COUNT = "b_user_cupid_reply_count"

    //拒绝上麦邀请点击
    const val B_REFUSE_ON_MIC_CLICK = "b_refuse_on_mic_click"

    //组CP按钮点击
    const val B_MAKE_SUPER_CP_CLICK = "b_make_super_cp_click"

    //超级CP卡时长按钮点击
    const val B_SUPER_CP_DURATION_BUTTON_CLICK = "b_super_cp_duration_button_click"

    //视频房界面
    const val PAGE_VIDEO_ROOM = "p_video_room"

    //专属视频房界面
    const val PAGE_PRIVATE_ROOM = "p_private_room"

    //专属视频房充值界面
    const val PAGE_PRIVATE_ROOM_RECHARGE = "p_private_room_recharge"

    //宣言界面
    const val PAGE_DECLARATION = "p_declaration"

    //宣言信物界面
    const val PAGE_DECLARATION_KEEPSAKE = "page_declaration_keepsake"

    //一方宣言界面
    const val PAGE_HALF_DECLARATION = "p_half_declaration"

    //小等级CP晋升弹窗曝光
    const val B_EXPOSURE_POPUP_LOW_LEVEL_UP = "b_exposure_popup_low_level_cp"

    //晋级提醒弹窗曝光
    const val B_EXPOSURE_POPUP_RISE_CP_LEVEL = "b_exposure_popup_rise_cp_level"

    //点击晋级提醒弹窗按钮
    const val B_CLICK_POPUP_RISE_CP_LEVEL = "b_click_popup_rise_cp_level"

    //点击CP晋级按钮
    const val B_BUTTON_RISE_CP_LEVEL = "b_button_rise_cp_level"

    //CP等级页面点击宣言
    const val B_CLICK_CP_LEVEL_PAGE_OATH = "b_click_cp_level_page_oath"

    //应用列表
    const val APPLICATION_INFO_LIST = "app_info_list"

    //语音速配信息设置
    const val PAGE_AUDIO_MATCH = "p_audio_match"

    //速配中页面
    const val PAGE_MATCHING = "p_matching"

    //视频速配通话页面
    const val PAGE_VIDEO_MATCH_CHATTING = "p_match_video_call"

    //速配通话页面
    const val PAGE_MATCH_VOICE_CALL = "p_match_voice_call"

    //匹配通话页面邀请上麦
    const val PAGE_MATCHED_INVITE_MIC = "p_matched_invite_mic"

    //弹窗
    const val PAGE_MATCH_CALL_COIN_NOT_ENOUGH_DIALOG = "p_match_call_coin_not_enough_dialog"

    //弹窗
    const val PAGE_RECEIVE_GIFT_PACKAGE_DIALOG = "p_receive_gift_package_dialog"

    //弹窗
    const val PAGE_GIFT_PACKAGE_DIALOG = "p_gift_package_dialog"

    //广场界面-点击语音开黑速配
    const val B_TEAM_JOIN_START_BUTTON_CLICK = "b_team_join_start_button_click"

    //语音开黑速配-立即找人
    const val B_FIND_USER_IMMEDIATELY = "b_find_user_immediately"

    //语音开黑速配-小于5分钟继续匹配
    const val B_KEEP_FIND_USER_MATCH = "b_keep_find_user_match"

    //语音开黑速配-小于5分钟充值
    const val B_MATCH_NOT_ENOUGH_DURATION_RACHARGE = "b_match_not_enough_duration_recharge"

    //语音开黑速配-金币不足充值
    const val B_MATCH_NOT_ENOUGH_COIN_RACHARGE = "b_match_not_enough_coin_recharge"

    //点击动态
    const val B_FEED_MOMENT_CLICK = "b_feed_moment_click"

    //嘉宾动态点击查看大图
    const val B_FEED_PIC_CLICK = "b_feed_pic_click"

    //嘉宾动态点击语音
    const val B_FEED_VOICE_CLICK = "b_feed_voice_click"

    //嘉宾动态用户点赞
    const val B_FEED_LIKE_CLICK = "b_feed_like_click"

    //嘉宾动态用户评论
    const val B_FEED_COMMENT_CLICK = "b_feed_comment_click"

    //速配通话使用开黑卡
    const val B_USE_TEAM_JOIN_CARD = "b_use_team_join_card"

    //速配通话点击换一个
    const val B_TEAM_JOIN_NEXT_BUTTON_CLICK = "b_team_join_next_button_click"

    //速配通话点击邀请开黑/邀请专属聊天
    const val B_INVITE_TEAM_JOIN_BUTTON_CLICK = "b_invite_team_join_button_click"

    //速配通话收到邀请弹窗的曝光
    const val B_RECEIVE_INVITE_ALERT_EXPOSURE = "b_receive_invite_alert_exposure"

    //速配通话收到邀请弹窗点击接受邀请
    const val B_ACCEPT_INVITE_CLICK = "b_accept_invite_click"

    //速配通话收到邀请弹窗点击拒绝邀请
    const val B_REFUSE_INVITE_CLICK = "b_refuse_invite_click"

    //速配通话充值弹窗曝光
    const val B_RECHARGE_ALERT_EXPOSURE = "b_recharge_alert_exposure"

    //动态广场
    const val PAGE_FEED = "p_feed"

    //个人装扮设置
    const val PAGE_PTA_SETTING = "p_pta_setting"

    //默认个人形象弹窗
    const val PAGE_USER_DRESS_DIALOG = "p_user_dress_dialog"

    //个人装扮设置弹窗
    const val PAGE_PTA_SETTING_DIALOG = "p_pta_setting_dialog"

    //赠送礼物
    const val PAGE_DIALOG_SEND_GIFT = "c_send_gift"

    //礼包icon的点击
    const val B_SMALL_GIFT_BAG_ICON_CLICK = "b_small_gift_bag_icon_click"

    //小额礼包弹窗曝光
    const val B_SMALL_GIFT_BAG_EXPLOSURE = "b_small_gift_bag_explosure"

    //匹配通话页面邀请上麦
    const val PAGE_SEND_VIRTUAL_DIALOG = "p_send_virtual_dialog"

    //个人装扮套装预览弹窗
    const val PAGE_PTA_SUIT_PREVIEW_DIALOG = "p_pta_suit_preview_dialog"

    //私聊页面虚拟形象弹窗曝光
    const val B_MESSAGE_VIRTUAL_ALERT_EXPOSURE = "b_message_virtual_alert_exposure"

    //装扮按钮点击
    const val B_DRESS_UP_CLICK = "b_dress_up_click"

    //装扮栏内点击预览
    const val B_DRESS_PREVIEW_CLICK = "b_dress_preview_click"

    //装扮栏内点击购买按钮
    const val B_BUY_DRESS_CLICK = "b_buy_dress_click"

    //点击购买出现购买装扮确认弹窗
    const val B_BUY_DRESS_ALERT_EXPOSURE = "b_buy_dress_alert_exposure"

    //装扮购买成功
    const val B_BUY_DRESS_SUCCESS = "b_buy_dress_success"

    //装扮购买失败
    const val B_BUY_DRESS_FAILED = "b_buy_dress_failed"

    //点击购买出现购买装扮支付弹窗
    const val B_BUY_DRESS_PAY_ALERT_EXPOSURE = "b_buy_dress_pay_alert_exposure"

    //索要卡片弹窗曝光
    const val B_ASK_FOR_CARD_EXPOSURE = "b_ask_for_card_exposure"

    //送给她弹窗曝光
    const val B_GIVE_TO_TA_ALERT_EXPOSURE = "b_give_to_ta_alert_exposure"

    //送给她弹窗点击购买按钮
    const val B_BUY_SEND_DRESS_CLICK = "b_buy_send_dress_click"

    //弹窗内成功购买
    const val B_BUY_SEND_DRESS_SUCCESS = "b_buy_send_dress_success"

    //充值弹窗曝光
    const val B_RECHARGE_WINDOW_EXPOSURE = "b_recharge_window_exposure"

    //充值成功
    const val B_RECHARGE_WINDOW_SUCCESS = "b_recharge_window_success"

    //充值失败
    const val B_RECHARGE_WINDOW_FAIL = "b_recharge_window_fail"

    //开黑卡购买埋点
    const val B_BUY_TEAM_JOIN_CARD = "b_buy_team_join_card"

    //开黑卡购买使用弹窗的曝光
    const val B_TEAM_JOIN_CARD_BUY_ALERT_EXPOSURE = "b_team_join_card_buy_alert_exposure"

    //用户未成功续卡的消息提醒-应用内通知的曝光,用户未成功续卡的消息提醒-通知栏消息的曝光,事件名一样，以position做区分
    const val B_REMIND_TEAM_JOIN_CARD_TIME_END_EXPOSURE =
        "b_remind_team_join_card_time_end_exposure"

    //用户未成功续卡的消息提醒-应用内通知的点击,用户未成功续卡的消息提醒-通知栏通知的点击,事件名一样，以position做区分
    const val B_REMIND_TEAM_JOIN_CARD_TIME_END_CLICK = "b_remind_team_join_card_time_end_click"

    //剩余3分钟消息提醒-应用内通知点击,剩余3分钟消息提醒-通知栏通知的点击,事件名一样，以position做区分
    const val B_TEAM_JOIN_CARD_TIME_3MIN_NOTICE_CLICK = "b_team_join_card_time_3min_notice_click"

    //速配补贴 - 系统消息曝光
    const val B_MESSAGE_QUICK_MATCH_EXPOSURE = "b_message_quick_match_exposure"

    //直播间礼物排行榜弹窗
    const val PAGE_LIVE_GIFT_RANK_LIST = "p_live_gift_rank_list"

    //自动语音/毛玻璃图片的消息曝光
    const val B_VIP_MESSAGE_EXPOSURE = "b_vip_message_exposure"

    //0.01元VIP弹窗曝光b_vip_alert_exposure
    const val B_VIP_ALERT_EXPOSURE = "b_vip_alert_exposure"

    //0.01元听语音弹窗点击购买事件
    const val B_VIP_ALERT_BUY_CLICK = "b_vip_alert_buy_click"

    //0.01元听语音弹窗点击取消事件
    const val B_VIP_ALERT_CANCEL_CLICK = "b_vip_alert_cancel_click"

    //听取语音消息点击事件
    const val B_VIP_VOICE_MESSAGE_CLICK = "b_vip_voice_message_click"

    //PK礼物排行榜的曝光
    const val B_PK_GIFT_RANK_EXPOSURE = "b_pk_gift_rank_exposure"

    //视频房用户赠送礼物
    const val B_USER_SEND_GIFT_CLICK = "b_user_send_gift_click"

    //给主持人送礼弹窗的曝光
    const val B_SEND_GIFT_TO_HOST_EXPOSURE = "b_send_gift_to_host_exposure"

    //给主持人送礼物成功
    const val B_SEND_GIFT_TO_HOST_SUCCESS = "b_send_gift_host_success"

    //点击领取奖励关闭
    const val B_CLICK_RECEIVE_AWARD_CLICK = "b_click_receive_award_click"

    //给主持人送礼物失败
    const val B_SEND_GIFT_TO_HOST_FAIL = "b_send_gift_host_fail"

    //给女嘉宾送礼弹窗曝光
    const val B_SEND_GIFT_TO_GUEST_EXPOSURE = "b_send_gift_to_guest_exposure"

    //给女嘉宾送礼物成功
    const val B_SEND_GIFT_TO_GUEST_SUCCESS = "b_send_gift_to_guest_success"

    //给女嘉宾送礼物失败
    const val B_SEND_GIFT_TO_GUEST_FAIL = "b_send_gift_to_guest_fail"

    //提现
    const val PAGE_WITHDRAWAL = "p_withdrawal"

    //实名认证弹窗
    const val B_VERIFIED_ALERT_EXPOSURE = "b_verified_alert_exposure"

    //视频速配入口曝光
    const val B_TEAM_VIDEO_JOIN_START_BUTTON_EXPOSURE = "b_team_video_join_start_button_exposure"

    //视频速配入口点击
    const val B_TEAM_VIDEO_JOIN_START_BUTTON_CLICK = "b_team_video_join_start_button_click"

    //已过期好友系统信息提示
    const val B_CHAT_EXPIRED_SYSTEM_BUBBLE_EXPOSURE = "b_chat_expired_system_bubble_exposure"

    //已过期好友提示中“立刻赠送”点击
    const val B_CHAT_EXPIRED_IMMEDIATELY_PRESENT_CLICK = "b_chat_expired_immediately_present_click"

    //速配未开启提示
    const val B_VIDEO_MATCH_NOT_OPEN_POPUP_EXPOSURE = "b_video_match_not_open_popup_exposure"

    //提现记录
    const val PAGE_WITHDRAWAL_HISTORY = "p_withdrawal_history"

    //被邀请赠送求交往礼物弹窗
    const val B_CHAT_ASK_FOR_CP_GIFT_POPUP = "b_chat_ask_for_cp_gift_popup"

    //被邀请赠送求交往礼物弹窗按钮点击
    const val B_CHAT_ASK_FOR_CP_GIFT_BUTTON_CLICK = "b_chat_ask_for_cp_gift_button_click"

    //中奖纪录页面
    const val P_LOTTERY_RECORD = "p_lottery_record"

    //获得钥匙的弹窗
    const val P_GOT_KEY_DIALOG = "p_got_key_dialog"

    //背包
    const val PAGE_BAG_FRAGMENT = "p_bag_fragment"

    //实名认证成功
    const val B_VERIFIED_SUCCESS = "b_verified_success"

    //绑定微信弹窗曝光
    const val B_BIND_WECHAT_ALERT_EXPOSURE = "b_bind_wechat_alert_exposure"

    //绑定微信成功
    const val B_BIND_WECHAT_SUCCESS = "b_bind_wechat_success"

    //提现按钮点击
    const val B_WITHDRAW_BUTTON_CLICK = "b_withdraw_button_click"

    //成功发起提现弹窗曝光
    const val B_WITHDRAW_SUCCESS_ALERT_EXPOSURE = "b_withdraw_success_alert_exposure"

    //礼盒入口曝光
    const val B_GIFT_BOX_BUTTON_EXPOSURE = "b_gift_box_button_exposure"

    //礼盒按钮点击
    const val B_GIFT_BOX_BUTTON_CLICK = "b_gift_box_button_click"

    //礼盒页面曝光 礼盒弹窗
    const val B_GIFT_BOX_DLALOG_EXPOSURE = "b_gift_box_dlalog_exposure"

    //中奖通告点击
    const val B_WIN_A_LOTTERY_NOTICE_CLICK = "b_win_a_lottery_notice_click"

    //中奖通告页面曝光
    const val P_GETED_GIFTOFBOX_NOTICES = "p_geted_giftofbox_notices"

    //点击拆礼盒按钮
    const val B_UNPACK_THE_GIFT_BOX_CLICK = "b_unpack_the_gift_box_click"

    //提示礼盒不足弹窗曝光
    const val B_GIFT_BOX_INSUFFICIENT_ALERT_EXPOSURE = "b_gift_box_insufficient_alert_exposure"

    //提示钥匙不足弹窗曝光
    const val B_KEY_INSUFFICIENT_ALERT_EXPOSURE = "b_key_insufficient_alert_exposure"

    //成功开礼盒
    const val B_UNPACK_THE_GIFT_BOX_SUCCESS = "b_unpack_the_gift_box_success"

    //中奖弹窗曝光
    const val B_WIN_A_LOTTERY_ALERT_EXPOSURE = "b_win_a_lottery_alert_exposure"

    //中奖弹窗曝光-一键送礼按钮点击
    const val B_WIN_A_LOTTERY_SEND_GIFT_CLICK = "b_win_a_lottery_send_gift_click"

    //中奖弹窗曝光-开心收下按钮点击
    const val B_WIN_A_LOTTERY_RECIVE_GIFT_CLICK = "b_win_a_lottery_recive_gift_click"

    //礼盒余额加号点击
    const val B_GIFT_BOX_BALANCE_CLICK = "b_gift_box_balance_click"

    //钥匙余额加号点击
    const val B_KEY_BALANCE_CLICK = "b_key_balance_click"

    //礼盒页面-查看更更多奖品点击
    const val B_GIFT_BOX_MORE_GIFT_CLICK = "b_gift_box_more_gift_click"

    //背包赠送按钮点击
    const val B_BAG_SEND_GIFT_CLICK = "b_bag_send_gift_click"

    //背包合成按钮点击
    const val B_BAG_SYNTHETIC_CLICK = "b_bag_synthetic_click"

    //背包提现按钮点击
    const val B_BAG_WITHDRAW_CLICK = "b_bag_withdraw_click"

    //礼盒引导曝光
    const val B_GUIDE_GIFT_BOX_EXPOSURE = "b_guide_gift_box_exposure"

    //激活CP获得钥匙提示弹窗曝光
    const val B_ACTIVATION_CP_GET_KEY_ALERT_EXPOSURE = "b_activation_cp_get_key_alert_exposure"

    //每日等级钥匙奖励弹窗曝光
    const val B_DAY_LEVEL_KEY_ALERT_EXPOSURE = "b_day_level_key_alert_exposure"

    //邀请卡片曝光
    const val B_INVITE_CARD_EXPOSURE = "b_invite_card_exposure"

    //卡片奖池入口点击
    const val B_CARD_PRIZE_POOL_CLICK = "b_card_prize_pool_click"

    //卡片点击（去看看按钮）
    const val B_CARD_GOTOSEE_CLICK = "b_card_gotosee_click"

    //邀请上麦弹窗曝光
    const val B_INVITE_MIC_SHOW = "b_invite_on_mic_alert_exposure"

    //邀请上麦弹窗点击同意
    const val B_ACCEPT_CP_INVITE = "b_accept_invite_on_mic_click"

    //通过CP团邀请上麦成功
    const val B_ACCEPT_CP_INVITE_SUCCESS = "b_accept_super_pc_on_mic_success"

    //加团按钮点击
    const val B_JOIN_GROUP_CLICK = "b_join_super_pc_team_click"

    //主持人评价窗口
    const val PRO_APPRAISE_ANCHOR_STEP_1 = "pro_appraise_anchor_step_1"

    //主持人评价窗口2
    const val PRO_APPRAISE_ANCHOR_STEP_2 = "pro_appraise_anchor_step_2"

    //上下麦评价弹窗曝光
    const val B_UP_DOWN_MIC_EVALUATION_POPUP_EXPOSURE = "b_up_down_mic_evaluation_popup_exposure"

    //上下麦评价弹窗点击事件
    const val B_UP_DOWN_MIC_EVALUATION_POPUP_ONCLICK = "b_up_down_mic_evaluation_popup_onclick"

    //评价弹窗曝光
    const val B_EVALUATION_POPUP_EXPOSURE = "b_evaluation_popup_exposure"

    //评价弹窗匿名提交点击事件
    const val B_EVALUATION_POPUP_ANONYMOUS_SUBMIT_ONCLICK =
        "b_evaluation_popup_anonymous_submit_onclick"

    //点击cp房礼物排行榜
    const val B_GIFT_LIST_CLICK = "b_gift_list_click"

    //助力中奖记录界面
    const val PAGE_BOOST_GIFT_REWARD_RECORD = "p_boost_gift_reward_record"

    //助力曝光(已设置礼物)
    const val B_ASSIST_EXPOSURE = "b_assist_exposure"

    //助力点击
    const val B_ASSIST_CLICK = "b_assist_click"

    //助力设置完成弹窗曝光
    const val B_HEART_ASSIST_POPUP_EXPOSURE = "b_heart_assist_popup_exposure"

    //助力设置完成弹窗--关闭按钮点击
    const val B_HEART_ASSIST_POPUP_CLOSE_CLICK = "b_heart_assist_popup_close_click"

    //助力设置完成弹窗--问号按钮点击
    const val B_HEART_ASSIST_QUESTION_MARK_CLICK = "b_heart_assist_question_mark_click"

    //助力设置完成弹窗--帮他助力按钮点击
    const val B_HELP_ASSIST_CLICK = "b_help_assist_click"

    //助力设置完成弹窗--帮他助力按钮赠送成功
    const val B_HELP_ASSIST_SUCCESS_EXPOSURE = "b_help_assist_success_exposure"

    //助力全屏浮层曝光
    const val B_ASSIST_FULL_SCREEN_EXPOSURE = "b_assist_full_screen_exposure"

    //助力宝箱 - 礼物栏上方助力宝箱入口点击
    const val B_GIFT_TOP_TREASURE_BOX_ENTRANCE_CLICK = "b_gift_top_treasure_box_entrance_click"

    //助力宝箱 - 初级宝箱页面曝光
    const val B_PRIMARY_TREASURE_BOX_EXPOSURE = "b_primary_treasure_box_exposure"

    //助力宝箱 - 高级宝箱页面曝光
    const val B_HIGH_TREASURE_BOX_EXPOSURE = "b_high_treasure_box_exposure"

    //助力宝箱 - 初级宝箱开始抽奖按钮点击
    const val B_PRIMARY_TREASURE_BOX_START_CLICK = "b_primary_treasure_box_start_click"

    //助力宝箱 - 高级宝箱开始抽奖按钮点击
    const val B_HIGH_TREASURE_BOX_START_CLICK = "b_high_treasure_box_start_click"

    //助力宝箱 - 初级宝箱抽奖成功
    const val B_PRIMARY_TREASURE_BOX_GET_EXPOSURE = "b_primary_treasure_box_get_exposure"

    //助力宝箱 - 高级宝箱抽奖成功
    const val B_HIGH_TREASURE_BOX_GET_EXPOSURE = "b_high_treasure_box_get_exposure"

    //助力宝箱 - 中奖弹窗曝光
    const val B_REWARD_POPUP_EXPOSURE = "b_reward_popup_exposure"

    //助力宝箱 - 开心收下按钮点击
    const val B_REWARD_POPUP_HAPPY_ACCEPT_CLICK = "b_reward_popup_happy_accept_click"

    //助力宝箱 - 中奖记录页面曝光
    const val B_WIN_REWARD_PAGE_EXPOSURE = "b_win_reward_page_exposure"

    //用户进入速配页面曝光
    const val B_VIDEO_MATCH_PAGE_EXPOSURE = "b_video_match_page_exposure"

    //速配界面 - 用户真正开始速配
    const val B_VIDEO_MATCH_TRUE_EXPOSURE = "b_video_match_true_exposure"

    //速配界面 - 用户退出速配页
    const val B_VIDEO_MATCH_PAGE_CLOSE_CLICK = "b_video_match_page_close_click"

    //速配时充值提醒弹窗曝光
    const val B_VIDEO_MATCH_NOT_ENOUGH_COIN_POPUP_EXPOSURE =
        "b_video_match_not_enough_coin_popup_exposure"

    //速配充值提醒弹窗点击 -用户选择不充值
    const val B_VIDEO_MATCH_NOT_ENOUGH_COIN_POPUP_CLOSE_CLICK =
        "b_video_match_not_enough_coin_popup_close_click"

    //速配充值提醒弹窗点击 -用户选择充值
    const val B_VIDEO_MATCH_NOT_ENOUGH_COIN_POPUP_RECHARGE_CLICK =
        "b_video_match_not_enough_coin_popup_recharge_click"

    //速配充值提醒弹窗点击 -用户充值成功
    const val B_VIDEO_MATCH_COIN_RECHARGE_SUCCESS_EXPOSURE =
        "b_video_match_coin_recharge_success_exposure"

    //速配链接中页面曝光
    const val B_TEAM_VIDEO_PAGE_LOAD_EXPOSURE = "b_team_video_page_load_exposure"

    //视频通话页面曝光
    const val B_TEAM_VIDEO_PAGE_EXPOSURE = "b_team_video_page_exposure"

    //视频通话页面加入房间失败
    const val B_TEAM_VIDEO_PAGE_JOIN_ROOM_FAILED = "b_team_video_page_join_room_failed"

    //视频通话 - 充值提醒弹窗曝光
    const val B_VIDEO_NOT_ENOUGH_COIN_RECHARGE_POPUP_EXPOSURE =
        "b_video_not_enough_coin_recharge_popup_exposure"

    //视频通话 - 充值提醒弹窗,用户点击充值
    const val B_VIDEO_COIN_RECHARGE_BUTTON_CLICK = "b_video_coin_recharge_button_click"

    //视频通话 - 用户充值成功
    const val B_VIDEO_COIN_RECHARGE_SUCCESS_EXPOSURE = "b_video_coin_recharge_success_exposure"

    //视频通话 - 挂断弹窗曝光
    const val B_TEAM_VIDEO_HANG_UP_POPUP_EXPOSURE = "b_team_video_hang_up_popup_exposure"

    //视频通话 - 挂断弹窗点击
    const val B_TEAM_VIDEO_HANG_UP_POPUP_CLICK = "b_team_video_hang_up_popup_click"

    //盲盒中奖记录
    const val PAGE_BLIND_BOX_RECORD = "p_blind_box_record"

    //盲盒引导入口广告图曝光
    const val B_BLIND_BOX_GUIDE_AD_PICTURE_EXPOSURE = "b_blind_box_guide_ad_picture_exposure"

    //点击盲盒引导入口广告图
    const val B_BLIND_BOX_GUIDE_AD_PICTURE_CLICK = "b_blind_box_guide_ad_picture_click"

    //礼物栏（礼物界面）曝光
    const val B_CHAT_GIFT_BAR_EXPOSURE = "b_chat_gift_bar_exposure"

    //礼物栏上方“查看盲盒礼物”点击
    const val B_VIEW_BLIND_BOX_GIFT_CLICK = "b_view_blind_box_gift_click"

    //礼物栏礼物列表 - 盲盒图标点击
    const val B_PRIMARY_BLIND_BOX_ICON_CLICK = "b_primary_blind_box_icon_click"

    //盲盒介绍弹窗曝光
    const val B_BLIND_BOX_INTRODUCE_POPUP_EXPOSURE = "b_blind_box_introduce_popup_exposure"

    //盲盒介绍弹窗“去看看”按钮点击
    const val B_BLIND_BOX_INTRODUCE_TO_SEE_BUTTON_CLICK =
        "b_blind_box_introduce_to_see_button_click"

    //中奖纪录入口点击
    const val B_WIN_RECORD_ENTRANCE_CLICK = "b_win_record_entrance_click"

    //临时好友标签曝光
    const val B_CHAT_TEMPORARY_BUDDY_TAG_EXPOSURE = "b_chat_temporary_buddy_tag_exposure"

    //临时好友倒计时系统提示气泡曝光
    const val B_CHAT_COUNTDOWN_SYSTEM_BUBBLE_EXPOSURE = "b_chat_countdown_system_bubble_exposure"

    //“立即赠送”点击
    const val B_CHAT_COUNTDOWN_IMMEDIATELY_PRESENT_CLICK =
        "b_chat_countdown_immediately_present_click"

    //送礼物结果成功埋点
    const val B_SEND_GIFT_RESULT_SUCCESS = "b_send_gift_result_success"

    //设置
    const val P_SOLO_VIDEO_ACTIVITY = "p_solo_video_activity"

    //视频call - 曝光
    const val B_VIDEO_CALL_POPUP_EXPOSURE = "b_recive_video_call_popup"

    //视频call - 接收点击
    const val B_VIDEO_CALL_ANSWER_CLICK = "b_recive_video_call_answer_click"

    //视频call - 挂断点击
    const val B_VIDEO_CALL_HANG_UP_CLICK = "b_team_video_hang_up_popup_click"

    //视频列表 - 与ta视频
    const val B_VIDEO_LIST_WITH_SHE_VIDEO_CLICK = "b_with_her_video_match_button_click"

    //视频列表 - 女嘉宾忙碌toast提示
    const val B_VIDEO_LIST_BUSY_TOAST = "b_video_male_guest_busy_toast"

    //呼叫中金币充值提醒弹窗曝光
    const val B_VIDEO_CALL_COIN_NOT_ENOUGH_EXPOSURE = "b_calling_not_enough_coin_popup"

    //呼叫中金币充值提醒弹窗中按钮点击
    const val B_VIDEO_CALLING_CLICK = "b_calling_not_enough_coin_button_click"

    //呼叫中充值成功
    const val B_VIDEO_CALLING_RECHARGE_SUCCESS = "b_calling_coin_recharge_success"

    //呼叫中男用户主动挂断
    const val B_VIDEO_CALLING_HANG_UP_CLICK = "b_calling_user_active_hang_up"
    //答题小游戏页面
    const val PAGE_SMALL_GAME = "small_game"
}