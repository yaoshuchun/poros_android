package com.liquid.poros.constant;

//卡类型
public enum CardCategoryEnum {
    TEAM_CARD("TEAM", "开黑卡"),
    MIC_CARD("MIC", "上麦卡"),
    CP_CARD("CP", "CP卡"),
    TEAM_CP_CARD("TEAM+CP", "开黑&CP卡");
    private String code;
    private String desc;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }


    CardCategoryEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static CardCategoryEnum typeOfValue(String code) {
        for (CardCategoryEnum e : values()) {
            if (e.getCode().equals(code)) {
                return e;
            }
        }
        return TEAM_CARD;
    }
}