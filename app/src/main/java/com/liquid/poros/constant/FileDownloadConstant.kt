package com.liquid.poros.constant

import com.liquid.base.utils.ContextUtils

object FileDownloadConstant {
    const val filePath = "gifts/zip"
    var DEFAULT_FILE_CACHE_PATH = ContextUtils.getApplicationContext().getExternalFilesDir("gift").path
    //礼物动画存储目录
    var rootPathAnimation = "${DEFAULT_FILE_CACHE_PATH}/${filePath}"

    object BaseConfig{
        const val ANIMATION_OSS_URL = "https://static.diffusenetwork.com"
        const val GIFT_PATH = "${ANIMATION_OSS_URL}/${filePath}"
    }

    var giftList = listOf("xiagujian","chiji","kele","banzhuan","memeda","qiujiaowang","qiqiu","penghua",
            "yanhua","meigui","feiji","chengbao","bixin","loveyou","sobuteful","hongchun","girlfri","huangguan",
            "leihen","qiubite","haozhuchi","zhuguang","langman","bidong","dangao","laohu","mao","xiong","hunsha",
            "dianzan","bangbangtang","icecream","yiwendingqing","haidao","iphone","yecanyuehui","lovetree","jiarilvxing","xiaoxingxing","xiaoyueliang",
            "xiaofengshan","tianminaicha","kouhong","paopaoji","shuijingxie","qiaokeli","lianaibijiben","aiqingmofashu","huache","motianlun","menghuanchengbao"
            ,"rewupaidui","xiaodao","paoche","qiubite2")
}