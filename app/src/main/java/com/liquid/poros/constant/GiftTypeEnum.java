package com.liquid.poros.constant;

//礼物类型相关
public enum GiftTypeEnum {

    GIFT_NORMAL(1, "普通礼物"),
    GIFT_PRIVATE(2, "专属礼物"),
    GIFT_BLIND(3, "盲盒礼物");

    private int code;
    private String desc;

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }


    public int getCode() {
        return code;
    }

    public void setCode(int type) {
        this.code = type;
    }

    GiftTypeEnum(int type, String desc) {
        this.code = type;
        this.desc = desc;
    }

    public static GiftTypeEnum typeOfValue(int code) {
        for (GiftTypeEnum e : values()) {
            if (e.getCode() == code) {
                return e;
            }
        }
        return GIFT_NORMAL;
    }
}