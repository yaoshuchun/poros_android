package com.liquid.poros.constant;

//广场列表女嘉宾状态相关
public enum SquareStatusEnum {
    SQUARE_ONLINE(1, "在线"),
    SQUARE_LIVE(2, "组队中"),
    SQUARE_BUSY(3, "忙碌"),
    SQUARE_OFFLINE(4, "离线"),
    SQUARE_ORDERING(5, "接单中"),
    SQUARE_PRIVATE_ROOM(6, "专属房"),
    SQUARE_PK_ROOM(7, "pk房");

    private int code;
    private String desc;

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }


    public int getCode() {
        return code;
    }

    public void setCode(int type) {
        this.code = type;
    }

    SquareStatusEnum(int type, String desc) {
        this.code = type;
        this.desc = desc;
    }

    public static SquareStatusEnum typeOfValue(int code) {
        for (SquareStatusEnum e : values()) {
            if (e.getCode() == code) {
                return e;
            }
        }
        return SQUARE_OFFLINE;
    }
}