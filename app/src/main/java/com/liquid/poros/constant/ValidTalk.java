package com.liquid.poros.constant;

public enum ValidTalk {

    P2P(100, "p2p"), //开黑
    VIDEO(101, "video"),   //视频房
    EXCLUSIVE(102, "exclusive"),  // 专属房
    MATCH(103, "match");   // 速配；

    ValidTalk(int type, String desc) {
        this.code = type;
        this.desc = desc;
    }

    private int code;
    private String desc;

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }


    public int getCode() {
        return code;
    }

    public void setCode(int type) {
        this.code = type;
    }

    public static ValidTalk typeOfValue(int code) {
        for (ValidTalk e : values()) {
            if (e.getCode() == code) {
                return e;
            }
        }
        return P2P;
    }
}
