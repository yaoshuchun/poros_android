package com.liquid.poros.constant;

//每日奖励类型
public enum DailyRewardEnum {
    REWARD_CARD("card", "开黑&上麦卡奖励"),
    REWARD_COIN("coin", "金币奖励"),
    REWARD_VIDEO_CARD("video_card", "视频上麦卡");
    private String code;
    private String desc;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }


    DailyRewardEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static DailyRewardEnum typeOfValue(String code) {
        for (DailyRewardEnum e : values()) {
            if (e.getCode().equals(code)) {
                return e;
            }
        }
        return REWARD_CARD;
    }
}