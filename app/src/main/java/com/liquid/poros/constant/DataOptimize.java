package com.liquid.poros.constant;

/**
 * 数据优化、用户围观时长
 */
public class DataOptimize {
    /**
     * 视频房列表
     */
    public static final String VIDEO_LIST = "video_list";
    /**
     * 广场
     */
    public static final String SQUARE = "square";
    /**
     * 消息详情页
     */
    public static final String MSG_DETAIL = "msg_detail";
    /**
     * 视频房活动导流
     */
    public static final String VIDEO_ACTIVITY = "video_activity";
    /**
     * 召集卡
     */
    public static final String CALL_CARD = "call_card";

    /**
     * 用户视频房的 操作记录
     */
    public static final String USER_VIDEO_ROOM_ACTION = "user_video_room_action";

    /**
     * 个人中心入口
     */
    public static final String PERSON_INFO_PAGE = "person_info_page";
}
