package com.liquid.poros.constant;

//用户身份相关
public enum UserRoleEnum {
    USER_NORMAL(100, "普通用户"),
    USER_LEADER(200, "正式领队"),
    USER_LEADER1(150, "新手领队"),
    USER_NEW_LEADER(150, "新手领队"),
    UNION_LEADER(400, "公会长"),
    USER_LEADER_NEW(300, "师傅");

    private int code;
    private String desc;

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }


    public int getCode() {
        return code;
    }

    public void setCode(int type) {
        this.code = type;
    }

    UserRoleEnum(int type, String desc) {
        this.code = type;
        this.desc = desc;
    }

    public static UserRoleEnum typeOfValue(int code) {
        for (UserRoleEnum e : values()) {
            if (e.getCode() == code) {
                return e;
            }
        }
        return USER_NORMAL;
    }
}