package com.liquid.poros.constant;

/**
 * Created by sundroid on 08/04/2019.
 */


//权限相关
public enum CoupleVoiceAction {
    LIVE_MOUSE_MONITOR(100,"开口数据采集"),
    TURN_OFF_MIC(315, "上麦并关麦"),
    EXIT_ON_MIC(316, "下麦"),
    ON_MIC(318, "上麦"),
    BAN(321, "拉黑用户"),
    CANCEL(9, "取消"),
    CLOSE_ROOM(310, " 关闭房间"),
    COUPLE_INVITE(340, "邀请组CP"),
    AGREE_COUPLE_INVITE(341, "同意组CP"),
    OPEN_CAMERA(352, "打开摄像头 "),
    CLOSE_CAMERA(353, "关闭摄像头 "),
    REFUSE_COUPLE_INVITE(342, "拒绝组CP"),
    VOICE_UPDATE(10, "音频更新"),
    FEMALE_DISCONNECTED(13, "女用户断网"),
    COUPLE_TEAM_INVITE(337, "邀请加入专属开黑"),
    COUPLE_TIME_1_MIN(351, "CP时间剩余1分钟"),
    MIC_TIME_END(354, "上麦时间结束,自动下麦"),
    USE_TEAM_CARD(355, "使用开黑卡"),
    MONEY_NOT_ENOUGH(356, "金币不足"),
    VIDEO_INVITE_MIC(365, "视频房邀请上麦"),
    VIDEO_RECEPT_GIFT(366, "接收到礼物"),
    MIC_LIST_REFRESH(367, "刷新麦序列表"),
    VIDEO_ADD_FRIEND_SUCCESS(368, "送礼物添加好友成功"),
    AGREE_PRIVATE_ROOM_INVITE(369, "主持人邀请,用户同意进入专属视频房"),
    CP_ROOM_SWITCH_PRIVATE(370, "找cp房转换成专属房"),
    PRIVATE_SWITCH_CP_ROOM(371, "专属房转换成找cp房"),
    PRIVATE_ROOM_EXIT_MIC(372, "专属房踢人下麦"),
    PK_START_PK(373, "开始PK"),
    PK_END_PK(374, "结束PK"),
    PK_CLEAR_DATA(375, "清除PK数据"),
    PK_UPDATE_SCORE(376, "更新pk数据"),
    UPDATE_CP_GROUP_NUMBER(378, "更新CP团人数量"),
    UPDATE_CP_ROOM_GIFT_RANK(380, "CP房礼物榜"),
    ANCHOR_BOOST_GIFT_SUCCESS(381, "助力设置成功"),
    UPDATE_SEND_BOOST_GIFT_PROGRESS(382, "赠送助力礼物,更新助力进度"),
    UPDATE_UPDATE_LOTTERY_COUNT(383, "赠送助力礼物,获取助力抽奖卷"),
    VOICE_CHAT_START(391, "开始开黑"),
    VOICE_CHAT_STOP(392, "结束开黑"),
    MATCH_ROOM_EXIT_MIC(414, "对方已挂断"),
    MINUTES_CHANGE(415, "剩余时长"),
    MINUTES_CHANGE_VIDEO(417, "视频速配剩余时长"),
    FEMALE_GUEST_CLOSE(418, "女嘉宾关播"),
    MALE_USER_CLOSE(419, "男用户下麦");
    private int code;
    private String desc;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    CoupleVoiceAction(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static CoupleVoiceAction typeOfValue(int code) {
        for (CoupleVoiceAction e : values()) {
            if (e.getCode() == code) {
                return e;
            }
        }
        return CANCEL;
    }
}