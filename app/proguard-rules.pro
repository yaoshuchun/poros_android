# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile
-dontwarn com.netease.**
-keep class com.netease.** {*;}
#如果您使用全文检索插件，需要加入
-dontwarn org.apache.lucene.**
-keep class org.apache.lucene.** {*;}

-keep class com.liquid.poros.widgets.CountdownDrawable {*;}

# glide 的混淆代码
-keep public class * implements com.bumptech.glide.module.GlideModule


-keep public enum com.bumptech.glide.load.resource.bitmap.ImageHeaderParser$** {
  **[] $VALUES;
  public *;
}

#-keep class com.vanniktech.emoji.** { *; }

# banner 的混淆代码
-keep class com.youth.banner.** {
    *;
 }
-dontwarn com.liquid.stat.boxtracker.model.**
-keep class com.liquid.stat.boxtracker.model.** { *; }

-dontwarn com.liquid.stat.boxtracker.constants.**
-keep class com.liquid.stat.boxtracker.constants.** { *; }

-dontwarn com.aliyun.sls.android.sdk.**
-keep class com.aliyun.sls.android.sdk.** { *; }


-keepclassmembers class fqcn.of.javascript.interface.for.webview {
   public *;
}

-dontwarn  com.liquid.im.kit.**
-keep class  com.liquid.im.kit** {*;}


-keepattributes *Annotation*
-keepclassmembers class * {
    @de.greenrobot.event.Subscribe <methods>;
}
-keepclassmembers class * {
    @com.liquid.im.kit.permission.annotation.* <methods>;
}

-keep enum de.greenrobot.event.ThreadMode { *; }

-dontwarn org.codehaus.**
-dontwarn java.nio.**
-dontwarn java.lang.invoke.**
-dontwarn rx.**

-keepattributes *Annotation*
-keepattributes *JavascriptInterface*


#-assumenosideeffects class android.util.Log { *; }
#-assumenosideeffects class System.out { *; }

-dontwarn com.mortals.icg.**
-keep class com.mortals.icg.** { *; }

-dontwarn io.netty.**
-keep class io.netty.** { *; }

-keep class com.bumptech.glide.integration.okhttp3.OkHttpGlideModule
-keepnames class c.l.a.utils.glideprofile.AppBoxGlideModule
-dontwarn com.bumptech.glide.load.resource.bitmap.VideoDecoder
-dontwarn android.content.**
-dontwarn android.app.**
-dontwarn android.app.job.**
-dontwarn android.util.**
-dontwarn android.os.**

-dontwarn com.tencent.bugly.**
-keep public class com.tencent.bugly.**{*;}

-dontwarn com.tencent.mm.opensdk.**
-keep class com.tencent.mm.opensdk.** {
   *;
}
-dontwarn com.tencent.wxop.**
-keep class com.tencent.wxop.** {
   *;
}
-dontwarn com.tencent.**
-keep class com.tencent.** {
   *;
}

-dontwarn c.l.a.views.**
-keep class c.l.a.views.** {
   *;
}
#TD
-dontwarn com.tendcloud.tenddata.**
-keep class com.tendcloud.** {*;}
-keep public class com.tendcloud.tenddata.** { public protected *;}
-keepclassmembers class com.tendcloud.tenddata.**{
public void *(***);
}
-keep class com.talkingdata.sdk.TalkingDataSDK {public *;}
-keep class com.apptalkingdata.** {*;}
-keep class dice.** {*; }
-dontwarn dice.**

#TONGDUN
-dontwarn com.android.internal.**
-keep class cn.tongdun.android.**{*;}

#个推
-dontwarn com.igexin.**
-keep class com.igexin.** { *; }
-keep class org.json.** { *; }

-keep class android.support.v4.app.NotificationCompat { *; }
-keep class android.support.v4.app.NotificationCompat$Builder { *; }
#魅族
-keep class com.meizu.** { *; }
-dontwarn com.meizu.**
#小米
-keep class com.xiaomi.** { *; }
-dontwarn com.xiaomi.push.**
-keep class org.apache.thrift.** { *; }
#华为
-keep class com.huawei.hms.** { *; }
-dontwarn com.huawei.hms.**
#百度SSP广告
-keep class com.baidu.mobads.*.** { *; }
-dontwarn com.baidu.mobads.*.**
#头条广告SDK
-keep class com.bytedance.sdk.openadsdk.** { *; }
-dontwarn com.bytedance.sdk.openadsdk.**
-keep class com.ss.android.crash.** { *; }
-dontwarn com.ss.android.crash.**
-keep class com.androidquery.*.** {*;}
-dontwarn com.androidquery.*.**
-keep class pl.droidsonroids.gif.** { *; }
-dontwarn pl.droidsonroids.gif.**
#alipay
#-keep class com.alipay.android.app.IAlixPay{*;}
#-keep class com.alipay.android.app.IAlixPay$Stub{*;}
#-keep class com.alipay.android.app.IRemoteServiceCallback{*;}
#-keep class com.alipay.android.app.IRemoteServiceCallback$Stub{*;}
#-keep class com.alipay.sdk.app.PayTask{ public *;}
#-keep class com.alipay.sdk.app.AuthTask{ public *;}
#-keep class com.alipay.sdk.app.H5PayCallback {
#    <fields>;
#    <methods>;
#}
#-keep class com.alipay.android.phone.mrpc.core.** { *; }
#-keep class com.alipay.apmobilesecuritysdk.** { *; }
#-keep class com.alipay.mobile.framework.service.annotation.** { *; }
#-keep class com.alipay.mobilesecuritysdk.face.** { *; }
#-keep class com.alipay.tscenter.biz.rpc.** { *; }
#-keep class org.json.alipay.** { *; }
#-keep class com.alipay.tscenter.** { *; }
#-keep class com.ta.utdid2.** { *;}
#-keep class com.ut.device.** { *;}

# Retain generic type information for use by reflection by converters and adapters.
-keepattributes Signature
# Retain service method parameters.
-keepclassmembernames,allowobfuscation interface * {
    @retrofit2.http.* <methods>;
}
# Ignore annotation used for build tooling.
-dontwarn org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement

-dontwarn org.xmlpull.v1.**
-dontwarn okhttp3.**
-dontwarn okio.**
-dontwarn javax.annotation.**
-dontwarn javax.annotation.ParametersAreNonnullByDefault

##---------------Begin: proguard configuration for Gson  ----------
# Gson uses generic type information stored in a class file when working with fields. Proguard
# removes such information by default, so configure it to keep all of it.
-keepattributes Signature

# For using GSON @Expose annotation
-keepattributes *Annotation*

# Gson specific classes
-keep class sun.misc.Unsafe { *; }
#-keep class com.google.gson.stream.** { *; }

# Application classes that will be serialized/deserialized over Gson
-keep class com.liquid.poros.entity.** { *; }
-keep class * implements android.os.Parcelable {
    public static final android.os.Parcelable$Creator *;
}

-keepattributes *Annotation*

# Only required if you use AsyncExecutor
-keepclassmembers class * extends org.greenrobot.eventbus.util.ThrowableFailureEvent {
    <init>(java.lang.Throwable);
}


##---------------TGSDK  start-----------------------------------------------------------##
-ignorewarnings
-keepattributes Signature
-keep class okhttp3.** { *; }
-keep class okio.** { *; }

# TGSDK
-keep class com.soulgame.sgsdk.** { *; }


# GooglePlayService
-keep public class com.google.android.gms.common.internal.safeparcel.SafeParcelable {
    public static final *** NULL;
}

-keepnames class * implements android.os.Parcelable
-keepclassmembers class * implements android.os.Parcelable {
  public static final *** CREATOR;
}

-keep @interface android.support.annotation.Keep
-keep @android.support.annotation.Keep class *
-keepclasseswithmembers class * {
  @android.support.annotation.Keep <fields>;
}
-keepclasseswithmembers class * {
  @android.support.annotation.Keep <methods>;
}

-keep @interface com.google.android.gms.common.annotation.KeepName
-keepnames @com.google.android.gms.common.annotation.KeepName class *
-keepclassmembernames class * {
  @com.google.android.gms.common.annotation.KeepName *;
}

-keep @interface com.google.android.gms.common.util.DynamiteApi
-keep public @com.google.android.gms.common.util.DynamiteApi class * {
  public <fields>;
  public <methods>;
}

-dontwarn android.security.NetworkSecurityPolicy

    # Shenqiad
-keep class com.shenqi.video.** {*;}

# Bugly
-dontwarn com.tencent.bugly.tgsdk.**
-keep class com.tencent.bugly.tgsdk.** { *; }
# ADX
-dontwarn com.yomob.tgsdklib.**
-keep class com.yomob.tgsdklib.** { *; }
-keep interface com.yomob.tgsdklib.** { *; }

    # Durian
    -dontwarn com.xhxm.media.**

# ZplayAds
-keep class com.playableads.** { *;}
-keep class com.playableads.PlayPreloadingListener {*;}
-keep class com.playableads.PlayLoadingListener {*;}
-keep class * implements com.playableads.PlayPreloadingListener {*;}
-keep class * implements com.playableads.PlayLoadingListener {*;}
-keep class com.playableads.PlayableAds {
    public void onDestroy();
    public static com.playableads.PlayableAds getInstance();
    public void requestPlayableAds(com.playableads.PlayPreloadingListener);
    public void requestPlayableAds(java.lang.String, com.playableads.PlayPreloadingListener);
    public synchronized static com.playableads.PlayableAds init(android.content.Context, java.lang.String, java.lang.String);
    public void presentPlayableAD(android.content.Context, com.playableads.PlayLoadingListener);
    public boolean canPresentAd();
}

# Lanmei
-keep class com.lm.** { *; }

# Guangdiantong
-keep class com.qq.e.** {
    public protected *;
}
-keep class android.support.v4.app.NotificationCompat**{
    public *;
}
-keep class android.support.v4.**{ *;}

# Centrixlink
-dontwarn com.centrixlink.**
-keep public class com.centrixlink.**  { *; }

# Mobvista
    -keepattributes Signature
    -keepattributes *Annotation*
    -keep class com.mobvista.** {*; }
    -keep interface com.mobvista.** {*; }
    -keep class android.support.v4.** { *; }
    -dontwarn com.mobvista.**
    -keep class **.R$* { public static final int mobvista*; }
    -keep class com.alphab.** {*; }
    -keep interface com.alphab.** {*; }

# Uniplay
-keep public class com.uniplay.adsdk.**{*;}
-keep public class com.mi.adtracker.**{*;}

# OneWay
-keepattributes *Annotation*
-keep enum mobi.oneway.sdk.* {*;}
-keep class mobi.oneway.sdk.** {*;}

# netease
-keep class com.netease.neliveplayer.**{*;}
-dontwarn com.netease.**
-keep class com.netease.** {*;}


##---------------TGSDK  end-------------------------------------------------------##
-keep class com.taobao.securityjni.**{*;}
-keep class com.taobao.wireless.security.**{*;}
-keep class com.ut.secbody.**{*;}
-keep class com.taobao.dp.**{*;}
-keep class com.alibaba.wireless.security.**{*;}
-keep class com.alibaba.security.rp.**{*;}
-keep class com.alibaba.sdk.android.**{*;}
-keep class com.alibaba.security.biometrics.**{*;}
-keep class android.taobao.windvane.**{*;}

## huawei push
-ignorewarnings
-keepattributes *Annotation*
-keepattributes Exceptions
-keepattributes InnerClasses
-keepattributes Signature
-keepattributes SourceFile,LineNumberTable
-dontwarn com.huawei.**
-dontwarn com.hianalytics.android.**
-keep class com.hianalytics.android.**{*;}
-keep class com.huawei.updatesdk.**{*;}
-keep class com.huawei.hms.**{*;}
-keep class com.huawei.android.** { *; }
-keep interface com.huawei.android.hms.agent.common.INoProguard {*;}
-keep class * extends com.huawei.android.hms.agent.common.INoProguard {*;}

##VIVO push
-keep class com.vivo.push.** { *; }
-dontwarn com.vivo.push.**
##OPPO push
-keep class com.coloros.mcssdk.** { *; }
-dontwarn com.coloros.mcssdk.**
##XIAOMI push
-dontwarn com.xiaomi.**
-keep class com.xiaomi.** { *; }
-keep class org.apache.thrift.** { *; }

-keep class com.tencent.open.TDialog$*
-keep class com.tencent.open.TDialog$* {*;}
-keep class com.tencent.open.PKDialog
-keep class com.tencent.open.PKDialog {*;}
-keep class com.tencent.open.PKDialog$*
-keep class com.tencent.open.PKDialog$* {*;}
-keep class com.bun.miitmdid.core.** {*;}

-keep class com.luck.picture.lib.** { *; }

#Ucrop
-dontwarn com.yalantis.ucrop**
-keep class com.yalantis.ucrop** { *; }
-keep interface com.yalantis.ucrop** { *; }

#Okio
-dontwarn org.codehaus.mojo.animal_sniffer.*

-keep class com.bytedance.sdk.openadsdk.** { *; }
-keep public interface com.bytedance.sdk.openadsdk.downloadnew.** {*;}
-keep class com.pgl.sys.ces.* {*;}


#OAID获取
-keep class XI.CA.XI.**{*;}
-keep class XI.K0.XI.**{*;}
-keep class XI.XI.K0.**{*;}
-keep class XI.xo.XI.XI.**{*;}
-keep class com.asus.msa.SupplementaryDID.**{*;}
-keep class com.asus.msa.sdid.**{*;}
-keep class com.bun.lib.**{*;}
-keep class com.bun.miitmdid.**{*;}
-keep class com.huawei.hms.ads.identifier.**{*;}
-keep class com.samsung.android.deviceidservice.**{*;}
-keep class com.zui.opendeviceidlibrary.**{*;}
-keep class org.json.**{*;}
-keep public class com.netease.nis.sdkwrapper.Utils {public
<methods>;}

#友盟混淆
-keep class com.umeng.** {*;}

#您如果集成了U-APM产品可以加入该混淆
-keep class com.uc.** {*;}

-keepclassmembers class * {
   public <init> (org.json.JSONObject);
}
-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}
-keep public class com.liquid.poros.R$*{
public static final int *;
}
#arouter
-keep public class com.alibaba.android.arouter.routes.**{*;}
-keep public class com.alibaba.android.arouter.facade.**{*;}
-keep class * implements com.alibaba.android.arouter.facade.template.ISyringe{*;}

# If you use the byType method to obtain Service, add the following rules to protect the interface:
-keep interface * implements com.alibaba.android.arouter.facade.template.IProvider

# If single-type injection is used, that is, no interface is defined to implement IProvider, the following rules need to be added to protect the implementation
# -keep class * implements com.alibaba.android.arouter.facade.template.IProvider
