## 进圈Android版

##  一.git commit日志基本规范

commit信息要尽可能详细

* add: 新增 feature
* fix: 修复 bug
* modify:修改、删除文档
* docs: 仅仅修改了文档，比如 README, CHANGELOG, CONTRIBUTE等等
* style: 仅仅修改了空格、格式缩进、逗号等等，不改变代码逻辑
* refactor: 代码重构，没有加新功能或者修复 bug
* perf: 优化相关，比如提升性能、体验
* test: 测试用例，包括单元测试、集成测试等
* ci: 修改或者增加 gradle 或者编译命令文件



## 二.分支相关

主要分支
* develop_future   下一期需求功能的开发分支
* develop_temp    临时加急的需求功能开发
* develop              永远是本周主线开发分支，提测分支也是该分支
* master                永远是当前线上包的代码
* Hotfix                  修复线上紧急包的分支


临时和功能分支：
* feature/xxx
* feature/xxxx




