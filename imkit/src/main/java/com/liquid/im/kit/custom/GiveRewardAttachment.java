package com.liquid.im.kit.custom;

import com.alibaba.fastjson.JSONObject;

/**
 * Created by sundroid on 01/04/2019.
 */

public class GiveRewardAttachment extends CustomAttachment {
    private final String KEY_MSG = "msg";
    private final String KEY_TYPE = "key_type";
    private final String KEY_SHOW_RECHARGE = "show_recharge";
    private final String KEY_SHOW_BUY_DIALOG = "show_buy_dialog";
    private String msg;
    private boolean showRecharge;
    private boolean showBuyDialog;
    private String type;
    private final String KEY_VOICE_URL = "voice_url";
    private final String KEY_C_TOAST = "c_toast";
    private String voiceUrl;
    private String cToast;

    public GiveRewardAttachment(String action) {
        super(action);
    }

    @Override
    public String toJson(boolean b) {
        return null;
    }

    @Override
    protected void parseData(JSONObject data) {
        this.msg = data.getString(KEY_MSG);
        this.type = data.getString(KEY_TYPE);
        if (data.containsKey(KEY_SHOW_RECHARGE)) {
            this.showRecharge = data.getBoolean(KEY_SHOW_RECHARGE);
        }
        if (data.containsKey(KEY_SHOW_BUY_DIALOG)) {
            this.showBuyDialog = data.getBoolean(KEY_SHOW_BUY_DIALOG);
        }
        this.voiceUrl = data.getString(KEY_VOICE_URL);
        this.cToast = data.getString(KEY_C_TOAST);
    }

    @Override
    protected JSONObject packData() {
        JSONObject data = new JSONObject();
        data.put(KEY_MSG, msg);
        data.put(KEY_TYPE, type);
        data.put(KEY_SHOW_RECHARGE,showRecharge);
        data.put(KEY_SHOW_BUY_DIALOG,showBuyDialog);
        data.put(KEY_VOICE_URL,voiceUrl);
        data.put(KEY_C_TOAST,cToast);
        return data;
    }

    public String getVoiceUrl() {
        return voiceUrl;
    }

    public void setVoiceUrl(String voiceUrl) {
        this.voiceUrl = voiceUrl;
    }

    public String getcToast() {
        return cToast;
    }

    public void setcToast(String cToast) {
        this.cToast = cToast;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isShowRecharge() {
        return showRecharge;
    }

    public void setShowRecharge(boolean showRecharge) {
        this.showRecharge = showRecharge;
    }

    public boolean isShowBuyDialog() {
        return showBuyDialog;
    }

    public void setShowBuyDialog(boolean showBuyDialog) {
        this.showBuyDialog = showBuyDialog;
    }
}