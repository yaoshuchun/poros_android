package com.netease.nim.uikit.business.session.custom

import com.alibaba.fastjson.JSONObject
import com.liquid.im.kit.custom.CustomAttachment

class CupidSysAttachment(action : String) : CustomAttachment(action){
    private val MSG = "msg"
    var msg = ""
    override fun packData(): JSONObject {
        val data = JSONObject()
        data[MSG] = msg
        return data
    }

    override fun parseData(data: JSONObject) {
        if (data?.containsKey(MSG)) {
            msg = data.getString(MSG)
        }
    }
}