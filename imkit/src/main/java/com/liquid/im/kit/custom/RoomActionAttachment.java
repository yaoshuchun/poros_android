package com.liquid.im.kit.custom;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.liquid.im.kit.bean.CoupleGiftBoostBean;

import java.security.Key;
import java.util.ArrayList;
import java.util.List;


/**
 * 断开连麦附件
 * Created by hzxuwen on 2016/3/28.
 */
public class RoomActionAttachment extends CustomAttachment {
    private final String KEY_UID = "uid";
    private final String KEY_ACTION = "action";
    private final String KEY_SYSTEM_INVITE = "system_invite";
    private final String KEY_IS_INVITE = "is_invite";
    private final String KEY_TEAM_ID = "team_id";
    private final String KEY_EXTRA = "extra";
    private final String KEY_VOLUME = "volume";
    private final String KEY_AVATAR_URL = "avatar_url";
    private final String KEY_NICKNAME = "nick_name";
    private final String KEY_CONTENT = "content";
    private final String KEY_CARD_IMG = "cp_card_img";
    private final String KEY_POS_1_ID = "pos_1_id";
    private final String KEY_POS_2_ID = "pos_2_id";
    private final String KEY_INVITE_ID = "invite_id";
    private final String KEY_NEED_COIN = "need_coins";
    private final String KEY_GIFT_DETAIL = "gift_detail";
    private final String KEY_SEND_DETAIL = "send_detail";
    private final String KEY_TEXT = "text";
    private final String KEY_ID = "id";
    private final String KEY_COUNT = "count";
    private final String KEY_NAME = "name";
    private final String KEY_ICON = "icon";
    private final String KEY_EFFECT = "effect";
    private final String KEY_AUDIO = "audio";
    private final String KEY_NUM = "num";
    private final String KEY_REASON_TEXT = "reason_text";
    private final String KEY_SUBTYPE = "subtype";

    private final String KEY_MINUTES = "minutes";
    private final String KEY_COINS = "coins";
    private final String KEY_FREE_MINUTES = "free_minutes";
    private final String KEY_C_TOAST = "c_toast";
    private final String KEY_TIMESTAMP = "timestamp";
    private final String KEY_SHOW_WINDOW = "show_window";
    private final String KEY_WHITE_USER_IDS = "white_user_ids";
    private final String KEY_POS = "pos";
    private final String KEY_SCORE= "score";
    private final String KEY_TOP_AVATAR_LIST = "topn_avatar_list";
    private final String KEY_JSON = "json";
    private final String KEY_GIFT_BOOST_BRIEF_INFO = "boost_brief_info";
    private final String KEY_GIFT_BOOST_INFO_LIST = "gift_boost_info_list";
    private final String KEY_GIFT_BOOST_LOTTERY_TIMES = "lottery_times";
    private final String KEY_BOOST_LOTTERY_COMMON_COUNT = "common_count";
    private final String KEY_BOOST_LOTTERY_ADVANCE_COUNT = "advance_count";
    //5s开口
    private final String KEY_MOUSE_5S_LIST = "mouse_5s_list";

    private String invite_id;
    private String pos_1_id;
    private String pos_2_id;
    private String json;
    private String account;
    private int action;
    private String extra;
    private boolean system_invite;
    private boolean is_invite;
    private String team_id;
    private int volume;
    private int count;
    private String avatar_url;
    private String nick_name;
    private String content;
    private String cp_card_img;
    private String need_coins;
    private String text;
    private int num;
    private int giftId;
    private String giftName;
    private String giftIcon;
    private String giftAudio;
    private int timestamp;
    private int show_window;
    private String[] white_user_ids;
    private String reason_text;
    private int subtype;
    private int minutes;
    private String coins;
    private int free_minutes;
    private String cToast;

    private long score;
    private int pos;
    private List<String> topn_avatar_list;
    //礼物助力
    private JSONObject gift_boost_brief_info;
    private List<CoupleGiftBoostBean> gift_boost_info_list;
    //礼物助力抽奖券
    private JSONObject lottery_times;
    private String common_count;
    private String advance_count;
    //5s开口数据
    private List<Integer> mouse5sList;

    public long getScore() {
        return score;
    }

    public void setScore(long score) {
        this.score = score;
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    public List<String> getTopn_avatar_list() {
        return topn_avatar_list;
    }

    public void setTopn_avatar_list(List<String> topn_avatar_list) {
        this.topn_avatar_list = topn_avatar_list;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }

    public String getInvite_id() {
        return invite_id;
    }

    public void setInvite_id(String invite_id) {
        this.invite_id = invite_id;
    }

    public RoomActionAttachment() {
        super("VOICE_CHANNEL_ACTION", "ROOM_ACTION");
    }

    public RoomActionAttachment(String account, int action, int volume) {
        this();
        this.account = account;
        this.action = action;
        this.volume = volume;
    }

    public RoomActionAttachment(String account, String extra, int action) {
        this();
        this.account = account;
        this.action = action;
        this.extra = extra;

    }

    public String getPos_1_id() {
        return pos_1_id;
    }

    public void setPos_1_id(String pos_1_id) {
        this.pos_1_id = pos_1_id;
    }

    public String getPos_2_id() {
        return pos_2_id;
    }

    public void setPos_2_id(String pos_2_id) {
        this.pos_2_id = pos_2_id;
    }

    @Override
    protected void parseData(JSONObject data) {
        if (data.containsKey(KEY_UID)) {
            this.account = data.getString(KEY_UID);
        }
        if(data.containsKey(KEY_COUNT)){
            this.count = data.getIntValue(KEY_COUNT);
        }
        if (data.containsKey(KEY_EXTRA)) {
            this.extra = data.getString(KEY_EXTRA);
        }
        if (data.containsKey(KEY_VOLUME)) {
            this.volume = data.getInteger(KEY_VOLUME);
        }
        if (data.containsKey(KEY_ACTION)) {
            this.action = data.getInteger(KEY_ACTION);
        }
        if (data.containsKey(KEY_SYSTEM_INVITE)) {
            this.system_invite = data.getBoolean(KEY_SYSTEM_INVITE);
        }
        if (data.containsKey(KEY_IS_INVITE)) {
            this.is_invite = data.getBoolean(KEY_IS_INVITE);
        }
        if (data.containsKey(KEY_INVITE_ID)) {
            this.invite_id = data.getString(KEY_INVITE_ID);
        }

        if (data.containsKey(KEY_POS_1_ID)) {
            this.pos_1_id = data.getString(KEY_POS_1_ID);
        }

        if (data.containsKey(KEY_POS_2_ID)) {
            this.pos_2_id = data.getString(KEY_POS_2_ID);
        }

        if (data.containsKey(KEY_TEAM_ID)) {
            this.team_id = data.getString(KEY_TEAM_ID);
        }
        if (data.containsKey(KEY_SEND_DETAIL)) {
            JSONObject sendDetail = data.getJSONObject(KEY_SEND_DETAIL);
            if (sendDetail.containsKey(KEY_NUM)) {
                num = sendDetail.getInteger(KEY_NUM);
            }
        }

        if (data.containsKey(KEY_GIFT_DETAIL)) {
            JSONObject giftDetail = data.getJSONObject(KEY_GIFT_DETAIL);
            if (giftDetail.containsKey(KEY_ID)) {
                giftId = giftDetail.getInteger(KEY_ID);
            }
            if (giftDetail.containsKey(KEY_NAME)) {
                giftName = giftDetail.getString(KEY_NAME);
            }
            if (giftDetail.containsKey(KEY_ICON)) {
                giftIcon = giftDetail.getString(KEY_ICON);
            }
            if (giftDetail.containsKey(KEY_EFFECT)) {
                JSONObject effect = giftDetail.getJSONObject(KEY_EFFECT);
                if (effect.containsKey(KEY_AUDIO)) {
                    giftAudio = effect.getString(KEY_AUDIO);
                }
                if (effect.containsKey(KEY_JSON)) {
                    json = effect.getString(KEY_JSON);
                }
            }
        }

        if (data.containsKey(KEY_AVATAR_URL)) {
            this.avatar_url = data.getString(KEY_AVATAR_URL);
        }
        if (data.containsKey(KEY_NICKNAME)) {
            this.nick_name = data.getString(KEY_NICKNAME);
        }
        if (data.containsKey(KEY_CONTENT)) {
            this.content = data.getString(KEY_CONTENT);
        }
        if (data.containsKey(KEY_CARD_IMG)) {
            this.cp_card_img = data.getString(KEY_CARD_IMG);
        }
        if (data.containsKey(KEY_NEED_COIN)) {
            this.need_coins = data.getString(KEY_NEED_COIN);
        }
        if (data.containsKey(KEY_C_TOAST)) {
            this.cToast = data.getString(KEY_C_TOAST);
        }

        if (data.containsKey(KEY_TEXT)) {
            this.text = data.getString(KEY_TEXT);
        }
        if (data.containsKey(KEY_TIMESTAMP)) {
            this.timestamp = data.getInteger(KEY_TIMESTAMP);
        }
        if (data.containsKey(KEY_SHOW_WINDOW)) {
            this.show_window = data.getInteger(KEY_SHOW_WINDOW);
        }
        if (data.containsKey(KEY_WHITE_USER_IDS)) {
            JSONArray jsonArray = data.getJSONArray(KEY_WHITE_USER_IDS);
            this.white_user_ids = new String[jsonArray.size()];
            for (int i = 0; i < jsonArray.size(); i++) {
                white_user_ids[i] = jsonArray.getString(i);
            }
        }
        if (data.containsKey(KEY_REASON_TEXT)) {
            this.reason_text = data.getString(KEY_REASON_TEXT);
        }

        if (data.containsKey(KEY_SUBTYPE)) {
            this.subtype = data.getInteger(KEY_SUBTYPE);
        }

        if (data.containsKey(KEY_MINUTES)) {
            this.minutes = data.getInteger(KEY_MINUTES);
        }

        if (data.containsKey(KEY_FREE_MINUTES)) {
            this.free_minutes = data.getInteger(KEY_FREE_MINUTES);
        }

        if (data.containsKey(KEY_COINS)) {
            this.coins = data.getString(KEY_COINS);
        }

        if (data.containsKey(KEY_POS)) {
            this.pos = data.getInteger(KEY_POS);
        }

        if (data.containsKey(KEY_SCORE)) {
            this.score = data.getLong(KEY_SCORE);
        }

        if (data.containsKey(KEY_TOP_AVATAR_LIST)) {
            JSONArray jsonArray = data.getJSONArray(KEY_TOP_AVATAR_LIST);
            this.topn_avatar_list = new ArrayList<>();
            for (int i = 0; i < jsonArray.size(); i++) {
                topn_avatar_list.add(jsonArray.getString(i));
            }
        }
        if (data.containsKey(KEY_GIFT_BOOST_BRIEF_INFO)) {
            gift_boost_brief_info = data.getJSONObject(KEY_GIFT_BOOST_BRIEF_INFO);
            if (gift_boost_brief_info != null) {
                JSONArray jsonArray = gift_boost_brief_info.getJSONArray(KEY_GIFT_BOOST_INFO_LIST);
                this.gift_boost_info_list = new ArrayList<>();
                for (int i = 0; i < jsonArray.size(); i++) {
                    try {
                        JSONObject jsonObject = JSONObject.parseObject(jsonArray.get(i).toString());
                        String gift_name = jsonObject.getString("gift_name");
                        String gift_icon = jsonObject.getString("gift_icon");
                        int boost_count = jsonObject.getInteger("boost_count");
                        int total_count = jsonObject.getInteger("total_count");
                        gift_boost_info_list.add(new CoupleGiftBoostBean(boost_count, total_count, gift_name, gift_icon));
                    } catch (Throwable throwable) {

                    }
                }
            }
        }
        if (data.containsKey(KEY_GIFT_BOOST_LOTTERY_TIMES)) {
            JSONObject effect = data.getJSONObject(KEY_GIFT_BOOST_LOTTERY_TIMES);
            if (effect.containsKey(KEY_BOOST_LOTTERY_COMMON_COUNT)) {
                common_count = effect.getString(KEY_BOOST_LOTTERY_COMMON_COUNT);
            }
            if (effect.containsKey(KEY_BOOST_LOTTERY_ADVANCE_COUNT)) {
                advance_count = effect.getString(KEY_BOOST_LOTTERY_ADVANCE_COUNT);
            }
        }
        if (data.containsKey(KEY_MOUSE_5S_LIST)) {
            JSONArray jsonArray = data.getJSONArray(KEY_MOUSE_5S_LIST);
            this.mouse5sList = new ArrayList<>();
            for (int i = 0; i < jsonArray.size(); i++) {
                mouse5sList.add(jsonArray.getInteger(i));
            }
        }
    }

    public int getNum() {
        return num;
    }

    public int getGiftId() {
        return giftId;
    }

    public String getGiftName() {
        return giftName;
    }

    public String getGiftIcon() {
        return giftIcon;
    }

    public String getGiftAudio() {
        return giftAudio;
    }

    public boolean isIs_invite() {
        return is_invite;
    }

    public String getTeam_id() {
        return team_id;
    }

    public void setTeam_id(String team_id) {
        this.team_id = team_id;
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    public String getcToast() {
        return cToast;
    }

    public void setcToast(String cToast) {
        this.cToast = cToast;
    }

    @Override
    protected JSONObject packData() {
        JSONObject data = new JSONObject();
        data.put(KEY_UID, account);
        data.put(KEY_ACTION, action);
        data.put(KEY_SYSTEM_INVITE, false);
        data.put(KEY_IS_INVITE, false);
        data.put(KEY_TEAM_ID, team_id);
        data.put(KEY_EXTRA, extra);
        data.put(KEY_VOLUME, volume);
        data.put(KEY_AVATAR_URL, avatar_url);
        data.put(KEY_NICKNAME, nick_name);
        data.put(KEY_CONTENT, content);
        data.put(KEY_CARD_IMG, cp_card_img);
        data.put(KEY_POS_1_ID, pos_1_id);
        data.put(KEY_POS_2_ID, pos_2_id);
        data.put(KEY_INVITE_ID, invite_id);
        data.put(KEY_NEED_COIN, need_coins);
        data.put(KEY_TEXT, text);
        data.put(KEY_COUNT, count);
        data.put(KEY_TIMESTAMP, timestamp);
        data.put(KEY_SHOW_WINDOW, show_window);
        data.put(KEY_C_TOAST,cToast);
        JSONArray jsonArray = new JSONArray();
        for (String u : white_user_ids) {
            jsonArray.add(u);
        }
        data.put(KEY_WHITE_USER_IDS,jsonArray);
        data.put(KEY_REASON_TEXT, reason_text);
        data.put(KEY_SUBTYPE, subtype);
        data.put(KEY_MINUTES, minutes);
        data.put(KEY_COINS, coins);
        data.put(KEY_FREE_MINUTES, free_minutes);
        data.put(KEY_POS, pos);
        data.put(KEY_SCORE, score);
        data.put(KEY_JSON, json);
        data.put(KEY_TOP_AVATAR_LIST, topn_avatar_list);
        data.put(KEY_GIFT_BOOST_BRIEF_INFO, gift_boost_brief_info);
        data.put(KEY_GIFT_BOOST_INFO_LIST, gift_boost_info_list);
        data.put(KEY_GIFT_BOOST_LOTTERY_TIMES, lottery_times);
        //开口数据采集
        data.put(KEY_MOUSE_5S_LIST, mouse5sList);
        return data;
    }

    public boolean isSystem_invite() {
        return system_invite;
    }

    public int getAction() {
        return action;
    }

    public void setAction(int action) {
        this.action = action;
    }

    public String getAccount() {

        return account;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getAvatar_url() {
        return avatar_url;
    }

    public void setAvatar_url(String avatar_url) {
        this.avatar_url = avatar_url;
    }

    public String getNick_name() {
        return nick_name;
    }

    public void setNick_name(String nick_name) {
        this.nick_name = nick_name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCp_card_img() {
        return cp_card_img;
    }

    public void setCp_card_img(String cp_card_img) {
        this.cp_card_img = cp_card_img;
    }

    public String getNeed_coins() {
        return need_coins;
    }

    public void setNeed_coins(String need_coins) {
        this.need_coins = need_coins;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(int timestamp) {
        this.timestamp = timestamp;
    }

    public int getShow_window() {
        return show_window;
    }

    public void setShow_window(int show_window) {
        this.show_window = show_window;
    }

    public String[] getWhite_user_ids() {
        return white_user_ids;
    }

    public void setWhite_user_ids(String[] white_user_ids) {
        this.white_user_ids = white_user_ids;
    }

    public String getReason_text() {
        return reason_text;
    }

    public int getSubtype() {
        return subtype;
    }

    public int getMinutes() {
        return minutes;
    }

    public String getCoins() {
        return coins;
    }

    public int getFree_minutes() {
        return free_minutes;
    }

    public JSONObject getGift_boost_brief_info() {
        return gift_boost_brief_info;
    }

    public void setGift_boost_brief_info(JSONObject gift_boost_brief_info) {
        this.gift_boost_brief_info = gift_boost_brief_info;
    }

    public List<CoupleGiftBoostBean> getGift_boost_info_list() {
        return gift_boost_info_list;
    }

    public void setGift_boost_info_list(List<CoupleGiftBoostBean> gift_boost_info_list) {
        this.gift_boost_info_list = gift_boost_info_list;
    }

    public String getCommon_count() {
        return common_count;
    }

    public void setCommon_count(String common_count) {
        this.common_count = common_count;
    }

    public String getAdvance_count() {
        return advance_count;
    }

    public void setAdvance_count(String advance_count) {
        this.advance_count = advance_count;
    }

    public List<Integer> getMouse5sList() {
        return mouse5sList;
    }

    public void setMouse5sList(List<Integer> mouse5sList) {
        this.mouse5sList = mouse5sList;
    }
}
