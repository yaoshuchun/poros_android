package com.liquid.im.kit.custom;

import com.alibaba.fastjson.JSONObject;

/**
 * 私聊消息行为
 */
public class P2pActionAttachment extends CustomAttachment {

    private final String KEY_STATUS_STR = "status_str";
    private final String KEY_STATUS = "status";
    private final String KEY_VOICE_WARNING = "voice_warning";
    private final String KEY_TEAM_ID = "team_id";
    private final String KEY_FEMALE_USER_ID = "female_user_id";
    private final String KEY_FEMALE_AVATAR_URL = "female_avatar_url";
    private final String KEY_IM_VIDEO_TOKEN = "im_video_token";

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getStatus_str() {
        return status_str;
    }

    public void setStatus_str(String status_str) {
        this.status_str = status_str;
    }

    public boolean isVoice_warning() {
        return voice_warning;
    }

    public void setVoice_warning(boolean voice_warning) {
        this.voice_warning = voice_warning;
    }

    public String getTeam_id() {
        return team_id;
    }

    public void setTeam_id(String team_id) {
        this.team_id = team_id;
    }

    public String getFemale_user_id() {
        return female_user_id;
    }

    public void setFemale_user_id(String female_user_id) {
        this.female_user_id = female_user_id;
    }

    public String getFemale_avatar_url() {
        return female_avatar_url;
    }

    public void setFemale_avatar_url(String female_avatar_url) {
        this.female_avatar_url = female_avatar_url;
    }

    public String getIm_video_token() {
        return im_video_token;
    }

    public void setIm_video_token(String im_video_token) {
        this.im_video_token = im_video_token;
    }

    private int status;
    private String status_str;
    private boolean voice_warning;
    private String team_id;
    private String female_user_id;
    private String female_avatar_url;
    private String im_video_token;

    public P2pActionAttachment() {
        super("VOICE_CHANNEL_ACTION", "P2P_ACTION");
    }

    public P2pActionAttachment(int status, String status_str) {
        this();
        this.status_str = status_str;
        this.status = status;
    }


    @Override
    protected void parseData(JSONObject data) {
        this.status = data.getInteger(KEY_STATUS);
        this.status_str = data.getString(KEY_STATUS_STR);
        this.voice_warning = data.getBoolean(KEY_VOICE_WARNING);
        this.team_id = data.getString(KEY_TEAM_ID);
        this.female_user_id = data.getString(KEY_FEMALE_USER_ID);
        this.female_avatar_url = data.getString(KEY_FEMALE_AVATAR_URL);
        if (data.containsKey(KEY_IM_VIDEO_TOKEN)) {
            this.im_video_token = data.getString(KEY_IM_VIDEO_TOKEN);
        }
    }


    @Override
    protected JSONObject packData() {
        JSONObject data = new JSONObject();
        data.put(KEY_STATUS, status);
        data.put(KEY_STATUS_STR, status_str);
        data.put(KEY_VOICE_WARNING, voice_warning);
        data.put(KEY_FEMALE_AVATAR_URL,female_avatar_url);
        data.put(KEY_FEMALE_USER_ID,female_user_id);
        data.put(KEY_TEAM_ID,team_id);
        data.put(KEY_IM_VIDEO_TOKEN,im_video_token);
        return data;
    }


}
