package com.liquid.im.kit.custom;

import com.alibaba.fastjson.JSONObject;

/**
 * 导流弹窗
 */
public class DaoliuAttachment extends CustomAttachment {

    public static final String ROOM_ID = "room_id";
    public static final String MAIN_TITLE = "main_title";
    public static final String SUB_TITLE = "sub_title";
    public static final String INVITE_ID = "invite_id";
    public static final String IM_ROOM_ID = "im_room_id";

    public static final String AVATAR_URL = "avatar_url";
    public static final String NIKE_NAME = "nike_name";
    public static final String NICK_NAME = "nick_name";
    public static final String GAME = "game";
    public static final String COUPLE_DESC = "couple_desc";
    public static final String DIVERSION_ID = "diversion_id";
    public static final String TYPE = "type";
    public static final String ANCHOR_AVATAR_URL = "anchor_avatar_url";
    public static final String AGE = "age";
    public static final String SOURCE = "source";
    public static final String GUEST_ID = "guest_id";
    public static final String ANCHOR_ID = "anchor_id";
    public static final String IS_NEW = "is_new";

    private String room_id;
    private String main_title;
    private String sub_title;
    private String invite_id;
    private String im_room_id;

    //
// "room_id": room_id, 房间id
// "avatar_url": guest_info.avatar_url, 头像
// "nike_name": guest_info.nick_name, 昵称
// "game": guest_info.game, 游戏
// "age": guest_info.age, 年龄
// "couple_desc": guest_info.couple_desc, 理想cp
// "diversion_id": diversion_id    使用召集卡的id，用户通过弹窗进入cp房间需要携带
    private String avatar_url;
    private String nike_name;
    private String guest_id;
    private String nick_name;
    private String game;
    private String couple_desc;
    private String diversion_id;
    private String type;
    private String age;
    private String source;
    private String anchor_nick_name;
    private String anchor_id;
    private String anchor_avatar_url;
    private boolean isFemale =false;
    private boolean is_new =false;


    public boolean isFemale() {
        return isFemale;
    }

    public void setFemale(boolean female) {
        isFemale = female;
    }

    public String getAnchor_nick_name() {
        return anchor_nick_name;
    }

    public void setAnchor_nick_name(String anchor_nick_name) {
        this.anchor_nick_name = anchor_nick_name;
    }

    public String getAnchor_avatar_url() {
        return anchor_avatar_url;
    }

    public void setAnchor_avatar_url(String anchor_avatar_url) {
        this.anchor_avatar_url = anchor_avatar_url;
    }

    public DaoliuAttachment(String action) {
        super(action);
    }

    @Override
    protected void parseData(JSONObject data) {
        this.room_id = data.getString(ROOM_ID);
        if(data.containsKey(MAIN_TITLE)){
            this.main_title = data.getString(MAIN_TITLE);
        }
        if(data.containsKey(SUB_TITLE)){
            this.sub_title = data.getString(SUB_TITLE);
        }
        this.invite_id = data.getString(INVITE_ID);
        this.im_room_id = data.getString(IM_ROOM_ID);

        this.avatar_url = data.getString(AVATAR_URL);
        if(data.containsKey(NIKE_NAME)){
            this.nike_name = data.getString(NIKE_NAME);
        }
        if(data.containsKey(NICK_NAME)){
            this.nick_name = data.getString(NICK_NAME);
        }
        this.game = data.getString(GAME);
        if(data.containsKey(COUPLE_DESC)){
            this.couple_desc = data.getString(COUPLE_DESC);
        }
        if(data.containsKey(DIVERSION_ID)){
            this.diversion_id = data.getString(DIVERSION_ID);
        }
        this.type = data.getString(TYPE);
        this.anchor_avatar_url = data.getString(ANCHOR_AVATAR_URL);
        this.age = data.getString(AGE);
        this.source = data.getString(SOURCE);
        if(data.containsKey(IS_NEW)){
            this.is_new = data.getBoolean(IS_NEW);
        }
        if(data.containsKey(ANCHOR_ID)){
            this.anchor_id = data.getString(ANCHOR_ID);
        }
        if(data.containsKey(GUEST_ID)){
            this.guest_id = data.getString(GUEST_ID);
        }

    }

    @Override
    protected JSONObject packData() {
        JSONObject data = new JSONObject();
        data.put(ROOM_ID, room_id);
        data.put(MAIN_TITLE, main_title);
        data.put(SUB_TITLE, sub_title);
        data.put(INVITE_ID, invite_id);

        data.put(AVATAR_URL, avatar_url);
        data.put(NIKE_NAME, nike_name);
        data.put(NICK_NAME, nick_name);
        data.put(GAME, game);
        data.put(COUPLE_DESC, couple_desc);
        data.put(DIVERSION_ID, diversion_id);
        data.put(TYPE, type);
        data.put(ANCHOR_AVATAR_URL, anchor_avatar_url);
        data.put(AGE, age);
        data.put(SOURCE, source);
        data.put(IS_NEW,is_new);
        data.put(ANCHOR_ID,anchor_id);
        data.put(GUEST_ID,guest_id);

        return data;
    }

    public String getGuest_id() {
        return guest_id;
    }

    public void setGuest_id(String guest_id) {
        this.guest_id = guest_id;
    }

    public String getAnchor_id() {
        return anchor_id;
    }

    public void setAnchor_id(String anchor_id) {
        this.anchor_id = anchor_id;
    }

    public boolean isIs_new() {
        return is_new;
    }

    public void setIs_new(boolean is_new) {
        this.is_new = is_new;
    }

    public String getNick_name() {
        return nick_name;
    }

    public void setNick_name(String nick_name) {
        this.nick_name = nick_name;
    }

    public String getAvatar_url() {
        return avatar_url;
    }

    public void setAvatar_url(String avatar_url) {
        this.avatar_url = avatar_url;
    }

    public String getNike_name() {
        return nike_name;
    }

    public void setNike_name(String nike_name) {
        this.nike_name = nike_name;
    }

    public String getGame() {
        return game;
    }

    public void setGame(String game) {
        this.game = game;
    }

    public String getCouple_desc() {
        return couple_desc;
    }

    public void setCouple_desc(String couple_desc) {
        this.couple_desc = couple_desc;
    }

    public String getDiversion_id() {
        return diversion_id;
    }

    public void setDiversion_id(String diversion_id) {
        this.diversion_id = diversion_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSub_title() {
        return sub_title;
    }

    public void setSub_title(String sub_title) {
        this.sub_title = sub_title;
    }

    public String getRoom_id() {
        return room_id;
    }

    public void setRoom_id(String room_id) {
        this.room_id = room_id;
    }

    public String getInvite_id() {
        return invite_id;
    }

    public void setInvite_id(String invite_id) {
        this.invite_id = invite_id;
    }

    public String getMain_title() {
        return main_title;
    }

    public void setMain_title(String main_title) {
        this.main_title = main_title;
    }

    public String getIm_room_id() {
        return im_room_id;
    }

    public void setIm_room_id(String im_room_id) {
        this.im_room_id = im_room_id;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    @Override
    public String toString() {
        return "DaoliuAttachment{" +
                "room_id='" + room_id + '\'' +
                ", main_title='" + main_title + '\'' +
                ", sub_title='" + sub_title + '\'' +
                ", invite_id='" + invite_id + '\'' +
                ", im_room_id='" + im_room_id + '\'' +
                ", avatar_url='" + avatar_url + '\'' +
                ", nike_name='" + nike_name + '\'' +
                ", game='" + game + '\'' +
                ", couple_desc='" + couple_desc + '\'' +
                ", diversion_id='" + diversion_id + '\'' +
                ", type='" + type + '\'' +
                ", age='" + age + '\'' +
                ", source='" + source + '\'' +
                '}';
    }
}
