package com.liquid.im.kit.custom

import com.alibaba.fastjson.JSONObject

class AskForGiftAttachment(action:String) : CustomAttachment(action){
    var status:Int? = null
    var status_str:String? = null
    var gift_id:Int? = null
    var gift_price:Int? = null
    var gift_name:String? = null
    var gift_icon:String? = null
    var msgId:String? = null

    override fun packData(): JSONObject {
        val jsonObject:JSONObject = JSONObject()
        jsonObject["status"] = status
        jsonObject["status_str"] = status_str
        jsonObject["gift_id"] = gift_id
        jsonObject["gift_price"] = gift_price
        jsonObject["gift_name"] = gift_name
        jsonObject["gift_icon"] = gift_icon
        jsonObject["msg_id"] = msgId
        return jsonObject
    }

    override fun parseData(data: JSONObject?) {
        status = data?.getInteger("status")
        status_str = data?.getString("status_str")
        gift_id = data?.getInteger("gift_id")
        gift_price = data?.getInteger("gift_price")
        gift_name = data?.getString("gift_name")
        gift_icon = data?.getString("gift_icon")
        msgId = data?.getString("msg_id")
    }
}