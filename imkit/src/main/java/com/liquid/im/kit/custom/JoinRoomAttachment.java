package com.liquid.im.kit.custom;

import com.alibaba.fastjson.JSONObject;

/**
 * 同意连麦附件
 * Created by hzxuwen on 2016/3/28.
 */
public class JoinRoomAttachment extends CustomAttachment {

    private final String KEY_UID = "uid";
    private final String KEY_NICK = "nick";
    private final String USER_LEVEL = "user_level";
    private final String FROM = "from";
    private final String IS_SUPER_ADMIN = "is_super_admin";

    private String from;
    private String account;
    private String nick;
    private int user_level = -1;
    private boolean isSuperAdmin;

    JoinRoomAttachment() {
        super("VOICE_CHANNEL_ACTION", "JoinRoom");
    }

    public JoinRoomAttachment(String account,String nick) {
        this();
        this.account = account;
        this.nick = nick;
    }
    public JoinRoomAttachment(String account,String nick,boolean isSuperAdmin) {
        this();
        this.account = account;
        this.nick = nick;
        this.isSuperAdmin = isSuperAdmin;
    }
    @Override
    protected void parseData(JSONObject data) {
        this.account = data.getString(KEY_UID);
        this.nick = data.getString(KEY_NICK);
        this.isSuperAdmin = data.getBooleanValue(IS_SUPER_ADMIN);
        if(data.containsKey(USER_LEVEL)){
            this.user_level = data.getInteger(USER_LEVEL);
        }
        this.from = data.getString("from");
    }

    @Override
    protected JSONObject packData() {
        JSONObject data = new JSONObject();
        data.put(KEY_UID, account);
        data.put(KEY_NICK, nick);
        data.put(USER_LEVEL,user_level);
        data.put(IS_SUPER_ADMIN, isSuperAdmin);
        data.put(FROM,from);
        return data;
    }

    public boolean isSuperAdmin() {
        return isSuperAdmin;
    }

    public void setSuperAdmin(boolean superAdmin) {
        isSuperAdmin = superAdmin;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getAccount() {
        return account;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public int getUser_level() {
        return user_level;
    }

    public void setUser_level(int user_level) {
        this.user_level = user_level;
    }
}
