package com.liquid.im.kit.custom

import com.alibaba.fastjson.JSONObject

/**
 * 索要虚拟
 */
open class AskPTAGiftAttachment(action: String?) : CustomAttachment(action) {

    private val KEY_MSG = "msg"
    private val KEY_ICON = "icon_url"
    private val KEY_BUTTON_TEXT = "botton_text"
    private val KEY_ASK_ID = "ask_id"
    private val KEY_CARTOON_SUIT_NAME = "cartoon_suit_name"
    private val KEY_DURATION_OPTIONS = "duration_options"
    private val KEY_SUIT_ID = "suit_id"

    var suit_id = ""
    var msg = ""
    var icon_url = ""
    var botton_text = ""
    var ask_id = ""
    var cartoon_suit_name = ""
    var duration_options = ""
    override fun parseData(data: JSONObject) {
        this.msg = data.getString(KEY_MSG)
        this.icon_url = data.getString(KEY_ICON)
        this.botton_text = data.getString(KEY_BUTTON_TEXT)
        this.ask_id = data.getString(KEY_ASK_ID)
        this.cartoon_suit_name = data.getString(KEY_CARTOON_SUIT_NAME)
        this.duration_options = data.getString(KEY_DURATION_OPTIONS)
        this.suit_id = data.getString(KEY_SUIT_ID)
    }

    override fun packData(): JSONObject {
        val data = JSONObject()
        data[KEY_MSG] = msg
        data[KEY_ICON] = icon_url
        data[KEY_BUTTON_TEXT] = botton_text
        data[KEY_ASK_ID] = ask_id
        data[KEY_CARTOON_SUIT_NAME] = cartoon_suit_name
        data[KEY_DURATION_OPTIONS] = duration_options
        data[KEY_SUIT_ID] = suit_id
        return data
    }

    data class Option(val days :Int,val  name :String,val coins :Int,var select:Boolean = false)
}