package com.liquid.im.kit.custom;

import com.alibaba.fastjson.JSONObject;

/**
 * 亲密值变化
 */
public class ScoreChangeAttachment extends CustomAttachment {

    private final String KEY_SCORE = "score";
    private final String KEY_DELTA = "delta";

    private String score;
    private int delta;


    public ScoreChangeAttachment() {
        super("VOICE_CHANNEL_ACTION", "CP_SCORE_CHANGE");
    }



    @Override
    protected void parseData(JSONObject data) {
        this.score = data.getString(KEY_SCORE);
        this.delta = data.getInteger(KEY_DELTA);
    }


    @Override
    protected JSONObject packData() {
        JSONObject data = new JSONObject();
        data.put(KEY_SCORE, score);
        data.put(KEY_DELTA, delta);
        return data;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public int getDelta() {
        return delta;
    }

    public void setDelta(int delta) {
        this.delta = delta;
    }
}
