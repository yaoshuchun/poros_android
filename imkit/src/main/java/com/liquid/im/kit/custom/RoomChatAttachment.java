package com.liquid.im.kit.custom;

import com.alibaba.fastjson.JSONObject;

/**
 * 同意连麦附件
 * Created by hzxuwen on 2016/3/28.
 */
public class RoomChatAttachment extends CustomAttachment {

    private final String USER_LEVEL = "user_level";
    private final String CONTENT = "content";

    private int user_level = -1;
    private String content = "";

    RoomChatAttachment() {
        super("VOICE_CHANNEL_ACTION", "roomText");
    }

    public RoomChatAttachment(int user_level){
        this();
        this.user_level = user_level;
    }

    @Override
    protected void parseData(JSONObject data) {
        this.user_level = data.getInteger(USER_LEVEL);
        this.content = data.getString(CONTENT);;
    }

    @Override
    protected JSONObject packData() {
        JSONObject data = new JSONObject();
        data.put(USER_LEVEL,user_level);
        data.put(CONTENT,content);
        return data;
    }

    public int getUser_level() {
        return user_level;
    }

    public void setUser_level(int user_level) {
        this.user_level = user_level;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
