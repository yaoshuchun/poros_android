package com.liquid.im.kit.custom;

import com.alibaba.fastjson.JSONObject;

/**
 * 邀请组cp
 */
public class InviteCoupleAttachment extends CustomAttachment {

    private final String KEY_STATUS_STR = "status_str";
    private final String KEY_STATUS = "status";
    private final String KEY_ORDER_ID = "order_id";
    private final String KEY_DESC = "desc";
    private final String KEY_MSG_ID = "msg_id";
    private final String KEY_IS_TRIAL = "is_trial";
    private final String KEY_CARD_TITLE = "card_title";
    private final String KEY_BOX_KEY_ONE = "box_key_one";
    private final String KEY_BOX_KEY_TWO = "box_key_two";
    private final String KEY_LOGIN_BONUS = "login_bonus";
    private int status;
    private String status_str;
    private String order_id;
    private String desc;
    private String msgId;
    private String card_title;
    private String box_key_one;
    private String box_key_two;
    private String login_bonus;
    private boolean is_trial;

    public InviteCoupleAttachment() {
        super("VOICE_CHANNEL_ACTION", "CP_INVITE");
    }

    public InviteCoupleAttachment(String order_id, String status_str, String desc, int status,boolean is_trial) {
        this();
        this.order_id = order_id;
        this.status_str = status_str;
        this.status = status;
        this.desc = desc;
        this.is_trial = is_trial;
    }

    public InviteCoupleAttachment(String order_id, String status_str, String desc, int status,boolean is_trial,
                                  String card_title,String box_key_one,String box_key_two,String login_bonus) {
        this();
        this.order_id = order_id;
        this.status_str = status_str;
        this.status = status;
        this.desc = desc;
        this.is_trial = is_trial;
        this.box_key_one = box_key_one;
        this.box_key_two = box_key_two;
        this.card_title = card_title;
        this.login_bonus = login_bonus;
    }


    @Override
    protected void parseData(JSONObject data) {
        this.status = data.getIntValue(KEY_STATUS);
        this.order_id = data.getString(KEY_ORDER_ID);
        this.status_str = data.getString(KEY_STATUS_STR);
        this.desc = data.getString(KEY_DESC);
        this.msgId = data.getString(KEY_MSG_ID);
        if (data.containsKey(KEY_IS_TRIAL)) {
            this.is_trial = data.getBoolean(KEY_IS_TRIAL);
        }
        if (data.containsKey(KEY_CARD_TITLE)) {
            this.card_title = data.getString(KEY_CARD_TITLE);
        }
        if (data.containsKey(KEY_LOGIN_BONUS)) {
            this.login_bonus = data.getString(KEY_LOGIN_BONUS);
        }
        if (data.containsKey(KEY_BOX_KEY_TWO)) {
            this.box_key_two = data.getString(KEY_BOX_KEY_TWO);
        }
        if (data.containsKey(KEY_BOX_KEY_ONE)) {
            this.box_key_one = data.getString(KEY_BOX_KEY_ONE);
        }
    }


    @Override
    protected JSONObject packData() {
        JSONObject data = new JSONObject();
        data.put(KEY_ORDER_ID, order_id);
        data.put(KEY_STATUS, status);
        data.put(KEY_DESC, desc);
        data.put(KEY_STATUS_STR, status_str);
        data.put(KEY_IS_TRIAL,is_trial);
        data.put(KEY_BOX_KEY_ONE,box_key_one);
        data.put(KEY_BOX_KEY_TWO,box_key_two);
        data.put(KEY_CARD_TITLE,card_title);
        data.put(KEY_LOGIN_BONUS,login_bonus);
        return data;
    }

    public String getMsgId() {
        return msgId;
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getStatus_str() {
        return status_str;
    }

    public void setStatus_str(String status_str) {
        this.status_str = status_str;
    }

    public boolean isIs_trial() {
        return is_trial;
    }

    public void setIs_trial(boolean is_trial) {
        this.is_trial = is_trial;
    }

    public String getCard_title() {
        return card_title;
    }

    public void setCard_title(String card_title) {
        this.card_title = card_title;
    }

    public String getBox_key_one() {
        return box_key_one;
    }

    public void setBox_key_one(String box_key_one) {
        this.box_key_one = box_key_one;
    }

    public String getBox_key_two() {
        return box_key_two;
    }

    public void setBox_key_two(String box_key_two) {
        this.box_key_two = box_key_two;
    }

    public String getLogin_bonus() {
        return login_bonus;
    }

    public void setLogin_bonus(String login_bonus) {
        this.login_bonus = login_bonus;
    }
}
