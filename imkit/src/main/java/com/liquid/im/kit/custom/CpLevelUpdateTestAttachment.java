package com.liquid.im.kit.custom;

import com.alibaba.fastjson.JSONObject;

public class CpLevelUpdateTestAttachment extends CustomAttachment {

    private final String KEY_GROUP_NAME = "group_name";
    private final String KEY_AWARD_LIST = "award_list";

    private String award_list;
    private String group_name;

    public CpLevelUpdateTestAttachment() {
        super("VOICE_CHANNEL_ACTION", "CP_SWEAR_POPUP");
    }

    public CpLevelUpdateTestAttachment(String group_name, String award_list) {
        this();
        this.group_name = group_name;
        this.award_list = award_list;
    }


    @Override
    protected void parseData(JSONObject data) {
        this.group_name = data.getString(KEY_GROUP_NAME);
        this.award_list = data.getString(KEY_AWARD_LIST);
    }

    @Override
    protected JSONObject packData() {
        JSONObject data = new JSONObject();
        data.put(KEY_GROUP_NAME, group_name);
        data.put(KEY_AWARD_LIST, award_list);
        return data;
    }

    public String getAward_list() {
        return award_list;
    }

    public String getGroup_name() {
        return group_name;
    }


}
