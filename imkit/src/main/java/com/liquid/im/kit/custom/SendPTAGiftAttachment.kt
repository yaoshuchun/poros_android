package com.liquid.im.kit.custom

import com.alibaba.fastjson.JSONObject

/**
 * 用户赠送礼物
 */
open class SendPTAGiftAttachment(action: String?) : CustomAttachment(action) {

    private val KEY_MSG = "msg"
    private val KEY_ICON = "icon_url"
    private val KEY_DAYS = "days"
    private val KEY_CARTOON_SUIT_NAME = "cartoon_suit_name"

    var msg = ""
    var icon_url = ""
    var days = ""
    var cartoon_suit_name = ""
    override fun parseData(data: JSONObject) {
        this.msg = data.getString(KEY_MSG)
        this.icon_url = data.getString(KEY_ICON)
        this.days = data.getString(KEY_DAYS)
        this.cartoon_suit_name = data.getString(KEY_CARTOON_SUIT_NAME)
    }

    override fun packData(): JSONObject {
        val data = JSONObject()
        data[KEY_MSG] = msg
        data[KEY_ICON] = icon_url
        data[KEY_DAYS] = days
        data[KEY_CARTOON_SUIT_NAME] = cartoon_suit_name
        return data
    }
}