package com.liquid.im.kit.custom;

import com.alibaba.fastjson.JSONObject;

public class BuyCpTipsAttachment extends CustomAttachment {

    private String users;
    private int score;
    private String msg;
    public BuyCpTipsAttachment(String action) {
        super(action);
    }

    @Override
    public String toJson(boolean b) {
        return null;
    }


    @Override
    protected void parseData(JSONObject data) {
        this.users = data.getString("users");
        this.score = data.getInteger("score");
        this.msg = data.getString("msg");
    }


    @Override
    protected JSONObject packData() {
        return null;
    }

    public String getUsers() {
        return users;
    }

    public void setUsers(String users) {
        this.users = users;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}