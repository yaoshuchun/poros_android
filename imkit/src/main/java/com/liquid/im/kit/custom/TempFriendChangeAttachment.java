package com.liquid.im.kit.custom;

import com.alibaba.fastjson.JSONObject;

/**
 * 拆礼盒，特殊中奖提示
 * 用户获得奖品需发送特殊中奖提示时，在双方私聊页面对用户显示恭喜+用户名+拆出+奖品图片+奖品名+奖品价值
 */
public class TempFriendChangeAttachment extends CustomAttachment {

    public static final String TEMP_FRIEND_CHANGE = "TEMP_FRIEND_CHANGE";
    private final String KEY_COUNT = "count";
    private final String KEY_ADD_COUNT = "add_count";
    private final String KEY_TEMP_FRIEND_CHANGE = "temp_friend_change";



    private int count;
    private int addCount;
    private boolean tempFriendShipChange;
    private boolean normal_friend_change;



    public TempFriendChangeAttachment() {
        super("VOICE_CHANNEL_ACTION", TEMP_FRIEND_CHANGE);
    }


    @Override
    protected void parseData(JSONObject data) {
//        this.count = data.getInteger(KEY_COUNT);
//        this.addCount = data.getInteger(KEY_ADD_COUNT);
//        this.tempFriendShipChange = data.getBooleanValue(KEY_TEMP_FRIEND_CHANGE);
    }



    @Override
    protected JSONObject packData() {
        JSONObject data = new JSONObject();
//        data.put(KEY_COUNT, count);
//        data.put(KEY_ADD_COUNT, addCount);
//        data.put(KEY_TEMP_FRIEND_CHANGE, tempFriendShipChange);
        return data;
    }


}
