package com.liquid.im.kit.custom;

import com.alibaba.fastjson.JSONObject;

/**
 * 拆礼盒，特殊中奖提示
 * 用户获得奖品需发送特殊中奖提示时，在双方私聊页面对用户显示恭喜+用户名+拆出+奖品图片+奖品名+奖品价值
 */
public class GiftBoxChangeAttachment extends CustomAttachment {

    public static final String GIFT_BOX_COUNT_CHANGE = "GIFT_BOX_COUNT_CHANGE";
    private final String KEY_COUNT = "count";
    private final String KEY_ADD_COUNT = "add_count";



    private int count;
    private int addCount;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getAddCount() {
        return addCount;
    }

    public void setAddCount(int addCount) {
        this.addCount = addCount;
    }

    public GiftBoxChangeAttachment() {
        super("VOICE_CHANNEL_ACTION", GIFT_BOX_COUNT_CHANGE);
    }


    @Override
    protected void parseData(JSONObject data) {
        this.count = data.getInteger(KEY_COUNT);
        this.addCount = data.getInteger(KEY_ADD_COUNT);
    }



    @Override
    protected JSONObject packData() {
        JSONObject data = new JSONObject();
        data.put(KEY_COUNT, count);
        data.put(KEY_ADD_COUNT, addCount);
        return data;
    }


}
