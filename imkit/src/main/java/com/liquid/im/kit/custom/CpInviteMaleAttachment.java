package com.liquid.im.kit.custom;

import com.alibaba.fastjson.JSONObject;

public class CpInviteMaleAttachment  extends CustomAttachment{

    private static  final String KEY_TYPE1="KEY_TYPE" ;
    private static  final String NICK_NAME="nick_name" ;
    private static  final String COIN="coin" ;
    private static  final String  ROOM_ID= "room_id";
    private static  final String IM_ROOM_ID="im_room_id" ;
    private static  final String AVATAR_URL = "avatar_url" ;
    private static  final String GAME= "game" ;
    private static  final String AGE = "age" ;
    private static  final String DESC="desc" ;
    private static  final String INVITE_ID = "invite_id" ;
    private static  final String SOURCE = "source";
    private static  final String TIME= "time" ;
    private static  final String TYPE= "type" ;
    private static  final String ANCHOR_NICK_NAME= "anchor_nick_name";
    private static  final String ANCHOR_AVATAR_URL= "anchor_avatar_url" ;



    private String KEY_TYPE;
    private String nick_name;// 女嘉宾昵称 没有时 为 "" 空str
    private int coin;//需20金币",
    private String room_id;// 房间id
    private String im_room_id;//云信房间id
    private String avatar_url;// 女嘉宾头像 直播间没女嘉宾时 为"" 空str
    private String game;// 游戏信息直播间没女嘉宾时 为"" 空str
    private String age;// 女嘉宾年龄 直播间没女嘉宾时 为"" 空str
    private String desc;// 默认标题
    private String invite_id;// 邀请记录id 用户通过本次邀请进入房间 或者上麦 需要携带
    private String source;// 邀请类型 用户通过本次邀请进入房间 或者上麦 需要携带
    private long time;//  10 位时间戳  可以通过这个 把 老的消息 忽略掉 不用判断了
    private String anchor_nick_name;// 主持人昵称
    private String anchor_avatar_url;// 主持人头像
    private String type;
    public CpInviteMaleAttachment(String action) {
        super(action);
    }

    @Override
    protected void parseData(JSONObject data) {
        this.KEY_TYPE = data.getString(KEY_TYPE1);
        this.nick_name = data.getString(NICK_NAME);
        this.coin = data.getIntValue(COIN);
        this.room_id = data.getString(ROOM_ID);
        this.im_room_id = data.getString(IM_ROOM_ID);
        this.avatar_url = data.getString(AVATAR_URL);
        this.game = data.getString(GAME);
        this.age = data.getString(AGE);
        this.desc = data.getString(DESC);
        this.invite_id = data.getString(INVITE_ID);
        this.source = data.getString(SOURCE);
        this.time = data.getLongValue(TIME);
        this.anchor_nick_name = data.getString(ANCHOR_NICK_NAME);
        this.anchor_avatar_url = data.getString(ANCHOR_AVATAR_URL);
        this.type = data.getString(TYPE);

    }

    @Override
    protected JSONObject packData() {
        JSONObject data = new JSONObject();
        data.put(KEY_TYPE1, KEY_TYPE);
        data.put(ROOM_ID, room_id);
        data.put(COIN, coin);
        data.put(NICK_NAME, nick_name);
        data.put(IM_ROOM_ID, im_room_id);
        data.put(AVATAR_URL, avatar_url);
        data.put(GAME, game);
        data.put(DESC, desc);
        data.put(INVITE_ID, invite_id);
        data.put(AGE, age);
        data.put(SOURCE, source);
        data.put(ANCHOR_NICK_NAME, anchor_nick_name);
        data.put(ANCHOR_AVATAR_URL, anchor_avatar_url);
        data.put(TYPE, type);

        return data;
    }

    public String getKEY_TYPE() {
        return KEY_TYPE;
    }

    public void setKEY_TYPE(String KEY_TYPE) {
        this.KEY_TYPE = KEY_TYPE;
    }

    public static String getNickName() {
        return NICK_NAME;
    }

    public String getNick_name() {
        return nick_name;
    }

    public void setNick_name(String nick_name) {
        this.nick_name = nick_name;
    }

    public int getCoin() {
        return coin;
    }

    public void setCoin(int coin) {
        this.coin = coin;
    }

    public String getRoom_id() {
        return room_id;
    }

    public void setRoom_id(String room_id) {
        this.room_id = room_id;
    }

    public String getIm_room_id() {
        return im_room_id;
    }

    public void setIm_room_id(String im_room_id) {
        this.im_room_id = im_room_id;
    }

    public String getAvatar_url() {
        return avatar_url;
    }

    public void setAvatar_url(String avatar_url) {
        this.avatar_url = avatar_url;
    }

    public String getGame() {
        return game;
    }

    public void setGame(String game) {
        this.game = game;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getInvite_id() {
        return invite_id;
    }

    public void setInvite_id(String invite_id) {
        this.invite_id = invite_id;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getAnchor_nick_name() {
        return anchor_nick_name;
    }

    public void setAnchor_nick_name(String anchor_nick_name) {
        this.anchor_nick_name = anchor_nick_name;
    }

    public String getAnchor_avatar_url() {
        return anchor_avatar_url;
    }

    public void setAnchor_avatar_url(String anchor_avatar_url) {
        this.anchor_avatar_url = anchor_avatar_url;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
