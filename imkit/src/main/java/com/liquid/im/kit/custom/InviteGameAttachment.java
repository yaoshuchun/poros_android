package com.liquid.im.kit.custom;

import com.alibaba.fastjson.JSONObject;

public class InviteGameAttachment extends CustomAttachment {

    private final String KEY_UID = "uid";
    private final String KEY_STATUS = "status"; //0 待接受  1 拒绝 2 同意
    private final String KEY_MSG_ID = "msg_id";
    private final String KEY_TEAM_ID = "team_id";
    private final String KEY_GAME_REGION = "game_regain";
    private String account;
    private String game_region;

    private int status;
    private String msgId;
    private String teamId;

    public InviteGameAttachment() {
        super("VOICE_CHANNEL_ACTION", "P2P_GAME");
    }

    public InviteGameAttachment(String account, String teamId, String game_region, int status) {
        this();
        this.account = account;
        this.teamId = teamId;
        this.status = status;
        this.game_region = game_region;
    }

    public InviteGameAttachment(String account, String teamId, String game_region, int status, String msgId) {
        this();
        this.account = account;
        this.status = status;
        this.teamId = teamId;
        this.msgId = msgId;
        this.game_region = game_region;
    }

    @Override
    protected void parseData(JSONObject data) {
        this.account = data.getString(KEY_UID);
        this.status = data.getInteger(KEY_STATUS);
        this.msgId = data.getString(KEY_MSG_ID);
        this.teamId = data.getString(KEY_TEAM_ID);
        this.game_region = data.getString(KEY_GAME_REGION);

    }

    public String getGame_region() {
        return game_region;
    }

    public void setGame_region(String game_region) {
        this.game_region = game_region;
    }

    @Override
    protected JSONObject packData() {
        JSONObject data = new JSONObject();
        data.put(KEY_UID, account);
        data.put(KEY_STATUS, status);
        data.put(KEY_MSG_ID, msgId);
        data.put(KEY_TEAM_ID, teamId);
        data.put(KEY_GAME_REGION, game_region);
        return data;
    }

    public String getMsgId() {
        return msgId;
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

    public String getTeamId() {
        return teamId;
    }

    public void setTeamId(String teamId) {
        this.teamId = teamId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }
}
