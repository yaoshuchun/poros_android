package com.liquid.im.kit.custom;

import com.alibaba.fastjson.JSONObject;

/**
 * 第11条消息弹礼包弹窗
 */
public class ShowGiftPackageAttachment extends CustomAttachment {

    private final String KEY_PAY_AMOUNT = "pay_amount";

    private int pay_amount;

    public int getPay_amount() {
        return pay_amount;
    }

    public void setPay_amount(int pay_amount) {
        this.pay_amount = pay_amount;
    }

    public ShowGiftPackageAttachment() {
        super("VOICE_CHANNEL_ACTION", "newbie_small_gift_pay_popup");
    }

    @Override
    protected void parseData(JSONObject data) {
        this.pay_amount = data.getInteger(KEY_PAY_AMOUNT);
    }


    @Override
    protected JSONObject packData() {
        JSONObject data = new JSONObject();
        data.put(KEY_PAY_AMOUNT, pay_amount);
        return data;
    }

}
