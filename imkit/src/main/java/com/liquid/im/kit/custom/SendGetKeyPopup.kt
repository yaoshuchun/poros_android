package com.liquid.im.kit.custom

import com.alibaba.fastjson.JSONObject

class SendGetKeyPopup(action: String?) : CustomAttachment(action) {
    var title: Int? = null
    var every_day: Int? = null
    override fun parseData(data: JSONObject) {
        title = data?.getInteger("title")
        every_day = data?.getInteger("every_day")
    }
    override fun packData(): JSONObject {
        val data = JSONObject()
        data["title"] = title
        data["every_day"] = every_day
        return data
    }
}