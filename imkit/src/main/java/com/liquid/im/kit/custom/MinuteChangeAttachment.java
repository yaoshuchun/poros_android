package com.liquid.im.kit.custom;

import com.alibaba.fastjson.JSONObject;

public class MinuteChangeAttachment extends CustomAttachment {

    private int minutes;
    private String coins;
    private int free_minutes;
    private String ts;
    public MinuteChangeAttachment() {
        super("VOICE_CHANNEL_ACTION", "MINUTES_CHANGE");
    }

    @Override
    public String toJson(boolean b) {
        return null;
    }

    @Override
    protected void parseData(JSONObject data) {
        this.minutes = data.getInteger("minutes");
        this.coins = data.getString("coins");
        this.free_minutes = data.getInteger("free_minutes");
        this.ts = data.getString("ts");
    }

    @Override
    protected JSONObject packData() {
        return null;
    }

    public int getMinutes() {
        return minutes;
    }

    public String getCoins() {
        return coins;
    }

    public int getFree_minutes() {
        return free_minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public void setCoins(String coins) {
        this.coins = coins;
    }

    public void setFree_minutes(int free_minutes) {
        this.free_minutes = free_minutes;
    }
}