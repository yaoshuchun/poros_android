package com.liquid.im.kit.custom;

import com.alibaba.fastjson.JSONObject;

/**
 * 断开连麦附件
 * Created by hzxuwen on 2016/3/28.
 */
public class ForceChangeVoiceChannelAttachment extends CustomAttachment {

    private final String KEY_UID = "uid";
    private final String KEY_ACTION = "action";

    private String account;
    private int action;

    public ForceChangeVoiceChannelAttachment() {
        super("VOICE_CHANNEL_ACTION","ForceChangeVoiceChannel");
    }

    public ForceChangeVoiceChannelAttachment(String account, int action) {
        this();
        this.account = account;
        this.action = action;
    }

    @Override
    protected void parseData(JSONObject data) {
        this.account = data.getString(KEY_UID);
        this.action = data.getInteger(KEY_ACTION);
    }

    @Override
    protected JSONObject packData() {
        JSONObject data = new JSONObject();
        data.put(KEY_UID, account);
        data.put(KEY_ACTION,action);
        return data;
    }

    public int getAction() {
        return action;
    }

    public void setAction(int action) {
        this.action = action;
    }

    public String getAccount() {

        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }


}
