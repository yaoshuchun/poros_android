package com.liquid.im.kit.custom;

import com.alibaba.fastjson.JSONObject;


public class RichMicAttachment extends CustomAttachment {
    private final String KEY_MSG = "msg";
    private String msg;

    public RichMicAttachment(String action) {
        super(action);
    }

    @Override
    public String toJson(boolean b) {
        return null;
    }

    @Override
    protected void parseData(JSONObject data) {
        this.msg = data.getString(KEY_MSG);
    }

    @Override
    protected JSONObject packData() {
        JSONObject data = new JSONObject();
        data.put(KEY_MSG, msg);
        return data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}