package com.liquid.im.kit.custom;

import android.text.TextUtils;

import com.alibaba.fastjson.JSONObject;
import com.netease.nimlib.sdk.msg.attachment.MsgAttachment;

/**
 * Created by sundroid on 01/04/2019.
 */

public abstract class CustomAttachment implements MsgAttachment {
    private String action;
    private String type;

    public CustomAttachment(String action) {
        this.action = action;
    }

    public CustomAttachment(String action, String type) {
        this.action = action;
        this.type = type;
    }

    // 解析附件内容。
    public void fromJson(JSONObject data) {
        if (data != null) {
            parseData(data);
        }
    }

    @Override
    public String toJson(boolean send) {
        if (!TextUtils.isEmpty(type)) {
            return CustomAttachParser.packData(action, type, packData());
        } else {
            return CustomAttachParser.packData(action, packData());

        }
    }

    // 子类的解析和封装接口。
    protected abstract void parseData(JSONObject data);

    protected abstract JSONObject packData();
}