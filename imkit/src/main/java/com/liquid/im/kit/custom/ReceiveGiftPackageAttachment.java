package com.liquid.im.kit.custom;

import com.alibaba.fastjson.JSONObject;

/**
 * 第11条消息弹礼包弹窗
 */
public class ReceiveGiftPackageAttachment extends CustomAttachment {


    public ReceiveGiftPackageAttachment() {
        super("VOICE_CHANNEL_ACTION", "newbie_small_gift_send_popup");
    }

    @Override
    protected void parseData(JSONObject data) {
    }


    @Override
    protected JSONObject packData() {
        JSONObject data = new JSONObject();
        return data;
    }

}
