package com.liquid.im.kit.custom;

import com.alibaba.fastjson.JSONObject;

/**
 * 邀请CP上麦
 */
public class InviteCoupleGameAttachment extends CustomAttachment {

    private final String KEY_TEAM_ID = "team_id";
    private final String KEY_TEAM_IM_ID = "team_im_id";
    private final String KEY_LEADER_ID = "leader_id";
    private final String KEY_FROM_USRE_AVATAR = "from_user_avatar";
    private final String KEY_FROM_USRE_NAME = "from_user_name";
    private final String KEY_FROM_USRE_ID = "from_user_id";
    private final String KEY_DESC = "desc";
    private final String KEY_MSG_ID = "msg_id";
    private final String KEY_VOICE_URL = "voice_url";
    private final String KEY_C_TOAST = "c_toast";
    private int status;
    private String status_str;
    private String order_id;
    private String desc;
    private String msgId;
    private String teamId;
    private String teamImId;
    private String leaderId;
    private String from_user_avatar;
    private String from_user_name;
    private String from_user_id;
    private String voiceUrl;
    private String cToast;

    public InviteCoupleGameAttachment() {
        super("VOICE_CHANNEL_ACTION", "P2P_CP_TEAM");
    }
    public InviteCoupleGameAttachment(String from_user_avatar, String from_user_name) {
        this();
        this.from_user_name = from_user_name;
        this.from_user_avatar = from_user_avatar;
    }


    @Override
    protected void parseData(JSONObject data) {
        this.teamId = data.getString(KEY_TEAM_ID);
        this.teamImId = data.getString(KEY_TEAM_IM_ID);
        this.leaderId = data.getString(KEY_LEADER_ID);
        this.from_user_avatar = data.getString(KEY_FROM_USRE_AVATAR);
        this.from_user_name = data.getString(KEY_FROM_USRE_NAME);
        this.from_user_id = data.getString(KEY_FROM_USRE_ID);
        this.msgId = data.getString(KEY_MSG_ID);
        this.voiceUrl = data.getString(KEY_VOICE_URL);
        this.cToast = data.getString(KEY_C_TOAST);
    }


    @Override
    protected JSONObject packData() {
        JSONObject data = new JSONObject();
        data.put(KEY_TEAM_ID, teamId);
        data.put(KEY_TEAM_IM_ID, teamImId);
        data.put(KEY_LEADER_ID, leaderId);
        data.put(KEY_FROM_USRE_AVATAR, from_user_avatar);
        data.put(KEY_FROM_USRE_NAME, from_user_name);
        data.put(KEY_FROM_USRE_ID, from_user_id);
        data.put(KEY_DESC, desc);
        data.put(KEY_VOICE_URL,voiceUrl);
        data.put(KEY_C_TOAST,cToast);
        return data;
    }

    public String getMsgId() {
        return msgId;
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getStatus_str() {
        return status_str;
    }

    public void setStatus_str(String status_str) {
        this.status_str = status_str;
    }

    public String getTeamId() {
        return teamId;
    }

    public void setTeamId(String teamId) {
        this.teamId = teamId;
    }

    public String getTeamImId() {
        return teamImId;
    }

    public void setTeamImId(String teamImId) {
        this.teamImId = teamImId;
    }

    public String getLeaderId() {
        return leaderId;
    }

    public void setLeaderId(String leaderId) {
        this.leaderId = leaderId;
    }

    public String getFrom_user_avatar() {
        return from_user_avatar;
    }

    public void setFrom_user_avatar(String from_user_avatar) {
        this.from_user_avatar = from_user_avatar;
    }

    public String getFrom_user_id() {
        return from_user_id;
    }

    public void setFrom_user_id(String from_user_id) {
        this.from_user_id = from_user_id;
    }

    public String getFrom_user_name() {
        return from_user_name;
    }

    public void setFrom_user_name(String from_user_name) {
        this.from_user_name = from_user_name;
    }

    public String getVoiceUrl() {
        return voiceUrl;
    }

    public void setVoiceUrl(String voiceUrl) {
        this.voiceUrl = voiceUrl;
    }

    public String getcToast() {
        return cToast;
    }

    public void setcToast(String cToast) {
        this.cToast = cToast;
    }
}
