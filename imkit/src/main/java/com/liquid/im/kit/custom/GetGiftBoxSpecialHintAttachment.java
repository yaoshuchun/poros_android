package com.liquid.im.kit.custom;

import com.alibaba.fastjson.JSONObject;

/**
 * 拆礼盒，特殊中奖提示
 * 用户获得奖品需发送特殊中奖提示时，在双方私聊页面对用户显示恭喜+用户名+拆出+奖品图片+奖品名+奖品价值
 */
public class GetGiftBoxSpecialHintAttachment extends CustomAttachment {

    public static final String GIFT_BOX_BIG_AWARD = "GIFT_BOX_BIG_AWARD";

    private final String KEY_NICK_NAME = "nick_name";
    private final String KEY_GIFT_NAME = "gift_name";
    private final String KEY_GIFT_IMAGE = "gift_image";
    private final String KEY_MSG = "msg";


    private String nick_name;
    private String gift_name;
    private String gift_image;
    private String msg;

    public GetGiftBoxSpecialHintAttachment() {
        super("VOICE_CHANNEL_ACTION", GIFT_BOX_BIG_AWARD);
    }


    @Override
    protected void parseData(JSONObject data) {
        this.nick_name = data.getString(KEY_NICK_NAME);
        this.gift_name = data.getString(KEY_GIFT_NAME);
        this.gift_image = data.getString(KEY_GIFT_IMAGE);
        this.msg = data.getString(KEY_MSG);

    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }


    public String getGift_name() {
        return gift_name;
    }

    public void setGift_name(String gift_name) {
        this.gift_name = gift_name;
    }

    @Override
    protected JSONObject packData() {
        JSONObject data = new JSONObject();
        data.put(KEY_NICK_NAME, nick_name);
        data.put(KEY_GIFT_NAME, gift_name);
        data.put(KEY_GIFT_IMAGE, gift_image);
        data.put(KEY_MSG, msg);
        return data;
    }


    public String getNick_name() {
        return nick_name;
    }

    public void setNick_name(String nick_name) {
        this.nick_name = nick_name;
    }

    public String getGift_image() {
        return gift_image;
    }

    public void setGift_image(String gift_image) {
        this.gift_image = gift_image;
    }
}
