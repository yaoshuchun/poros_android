package com.liquid.im.kit.emoji;

public interface IEmoticonCategoryChanged {
    void onCategoryChanged(int index);
}
