package com.liquid.im.kit.custom;

import com.alibaba.fastjson.JSONObject;

public class CpLevelUpSuccessAttach extends CustomAttachment{

    private final String SHOW_WORDS = "show_words";
    private final String GIFT_NAME = "gift_name";
    private final String GIFT_PIC_URL = "gift_pic_url";
    private final String LEVEL_NAME = "level_name";
    private final String LEVEL = "level";

    private String show_words;
    private String gift_name;
    private String gift_pic_url;
    private String level_name;
    private int level;

    public CpLevelUpSuccessAttach(String action) {
        super(action);
    }

    public CpLevelUpSuccessAttach() {
        super("VOICE_CHANNEL_ACTION", "cp_advance_success");
    }

    @Override
    protected void parseData(JSONObject data) {
        this.show_words = data.getString(SHOW_WORDS);
        this.gift_name = data.getString(GIFT_NAME);
        this.gift_pic_url = data.getString(GIFT_PIC_URL);
        this.level_name = data.getString(LEVEL_NAME);
        this.level = data.getInteger(LEVEL);
    }

    @Override
    protected JSONObject packData() {
        JSONObject data = new JSONObject();
        data.put(SHOW_WORDS, show_words);
        data.put(GIFT_NAME, gift_name);
        data.put(GIFT_PIC_URL, gift_pic_url);
        data.put(LEVEL_NAME, level_name);
        data.put(LEVEL, level);
        return data;
    }

    public String getShow_words() {
        return show_words;
    }

    public void setShow_words(String show_words) {
        this.show_words = show_words;
    }

    public String getGift_name() {
        return gift_name;
    }

    public void setGift_name(String gift_name) {
        this.gift_name = gift_name;
    }

    public String getGift_pic_url() {
        return gift_pic_url;
    }

    public void setGift_pic_url(String gift_pic_url) {
        this.gift_pic_url = gift_pic_url;
    }

    public String getLevel_name() {
        return level_name;
    }

    public void setLevel_name(String level_name) {
        this.level_name = level_name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }
}
