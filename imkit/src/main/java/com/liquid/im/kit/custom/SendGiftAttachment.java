package com.liquid.im.kit.custom;

import com.alibaba.fastjson.JSONObject;
import com.liquid.im.kit.bean.GiftBean;

public class SendGiftAttachment extends CustomAttachment {

    private final String KEY_GIFT_DETAIL = "gift_detail";
    private final String KEY_NUM = "num";
    private final String KEY_TITLE = "title";

    private GiftBean giftBean;
    private int num;
    private String title;

    public SendGiftAttachment(String action) {
        super(action);
    }

    @Override
    protected void parseData(JSONObject data) {
        this.giftBean = data.getObject(KEY_GIFT_DETAIL, GiftBean.class);
        this.num = data.getInteger(KEY_NUM);
        this.title = data.getString(KEY_TITLE);
    }

    @Override
    protected JSONObject packData() {
        JSONObject data = new JSONObject();
        data.put(KEY_GIFT_DETAIL,giftBean);
        data.put(KEY_NUM,num);
        data.put(KEY_TITLE,title);
        return data;
    }

    public GiftBean getGiftBean() {
        return giftBean;
    }

    public void setGiftBean(GiftBean giftBean) {
        this.giftBean = giftBean;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
