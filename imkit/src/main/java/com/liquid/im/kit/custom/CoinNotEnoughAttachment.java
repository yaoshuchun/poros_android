package com.liquid.im.kit.custom;

import com.alibaba.fastjson.JSONObject;
public class CoinNotEnoughAttachment extends CustomAttachment {

    private String desc;
    private int type;
    public CoinNotEnoughAttachment(String action) {
        super(action);
    }

    @Override
    public String toJson(boolean b) {
        return null;
    }

    public int getType() {
        return type;
    }

    @Override
    protected void parseData(JSONObject data) {
        this.desc = data.getString("msg");
        this.type = data.getInteger("type");
    }

    public String getDesc() {
        return desc;
    }

    @Override
    protected JSONObject packData() {
        return null;
    }
}