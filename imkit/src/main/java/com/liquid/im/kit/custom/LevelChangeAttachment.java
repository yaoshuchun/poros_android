package com.liquid.im.kit.custom;

import com.alibaba.fastjson.JSONObject;

/**
 * 等级变化
 */
public class LevelChangeAttachment extends CustomAttachment {
    private final String KEY_RANK_NAME = "rank_name";
    private final String KEY_RANK_INFO = "rank_info";
    private final String KEY_RANK = "rank";
    private final String KEY_NAME = "name";
    private final String KEY_NEED_SCORE = "need_score";
    private final String KEY_RANK_SCORE_BASE = "rank_score_base";
    private final String KEY_POINT_AWARD = "point_award";
    private final String KEY_RANK_ICON = "rank_icon";
    private final String KEY_CP_SCORE= "cp_score";
    private String rank_name;
    private String rank;
    private String name;
    private String need_score;
    private String rank_score_base;
    private String point_award;
    private String rank_icon;
    private String cp_score;

    public LevelChangeAttachment() {
        super("VOICE_CHANNEL_ACTION", "CP_RANK_CHANGE");
    }

    @Override
    protected void parseData(JSONObject data) {
        this.rank_name = data.getString(KEY_RANK_NAME);
        JSONObject rank_info = data.getJSONObject(KEY_RANK_INFO);
        this.rank = rank_info.getString(KEY_RANK);
        this.name = rank_info.getString(KEY_NAME);
        this.need_score = rank_info.getString(KEY_NEED_SCORE);
        this.rank_score_base = rank_info.getString(KEY_RANK_SCORE_BASE);
        this.point_award = rank_info.getString(KEY_POINT_AWARD);
        this.rank_icon = rank_info.getString(KEY_RANK_ICON);
        this.cp_score = rank_info.getString(KEY_CP_SCORE);
    }


    @Override
    protected JSONObject packData() {
        JSONObject data = new JSONObject();
        data.put(KEY_RANK_NAME, rank_name);
        return data;
    }

    public String getRank_name() {
        return rank_name;
    }

    public void setRank_name(String rank_name) {
        this.rank_name = rank_name;
    }
    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNeed_score() {
        return need_score;
    }

    public void setNeed_score(String need_score) {
        this.need_score = need_score;
    }

    public String getRank_score_base() {
        return rank_score_base;
    }

    public void setRank_score_base(String rank_score_base) {
        this.rank_score_base = rank_score_base;
    }

    public String getPoint_award() {
        return point_award;
    }

    public void setPoint_award(String point_award) {
        this.point_award = point_award;
    }

    public String getRank_icon() {
        return rank_icon;
    }

    public void setRank_icon(String rank_icon) {
        this.rank_icon = rank_icon;
    }

    public String getCp_score() {
        return cp_score;
    }

    public void setCp_score(String cp_score) {
        this.cp_score = cp_score;
    }
}
