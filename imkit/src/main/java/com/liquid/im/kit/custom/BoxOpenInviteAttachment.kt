package com.liquid.im.kit.custom

import com.alibaba.fastjson.JSONObject

class BoxOpenInviteAttachment(action: String?) : CustomAttachment(action) {
    var desc: String? = null
    override fun toJson(send: Boolean): String {
        return super.toJson(send)
    }

    override fun parseData(data: JSONObject) {
        desc = data.getString("gift_box_reward_desc")
    }

    override fun packData(): JSONObject {
        val data = JSONObject()
        data["desc"] = desc
        return data
    }
}