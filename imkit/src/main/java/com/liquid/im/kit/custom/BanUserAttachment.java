package com.liquid.im.kit.custom;

import com.alibaba.fastjson.JSONObject;

/**
 * Created by sundroid on 01/04/2019.
 */

public class BanUserAttachment extends CustomAttachment {
    private String ban_user_id;
    private String ban_user_name;


    public BanUserAttachment(String action) {
        super(action);
    }

    @Override
    public String toJson(boolean b) {
        return null;
    }

    @Override
    protected void parseData(JSONObject data) {
        this.ban_user_id = data.getString("ban_user_id");
        this.ban_user_name = data.getString("ban_user_name");
    }

    public String getBan_user_id() {
        return ban_user_id;
    }

    public String getBan_user_name() {
        return ban_user_name;
    }

    @Override
    protected JSONObject packData() {
        return null;
    }
}