package com.liquid.im.kit.custom;

import com.alibaba.fastjson.JSONObject;

/**
 * 亲密值变化
 */
public class CoinChangeAttachment extends CustomAttachment {

    private final String KEY_COIN = "coin";

    private int coin;


    public CoinChangeAttachment() {
        super("VOICE_CHANNEL_ACTION", "USER_COIN_CHANGE");
    }



    @Override
    protected void parseData(JSONObject data) {
        this.coin = data.getInteger(KEY_COIN);
    }


    @Override
    protected JSONObject packData() {
        JSONObject data = new JSONObject();
        data.put(KEY_COIN, coin);
        return data;
    }

    public int getCoin() {
        return coin;
    }

    public void setCoin(int coin) {
        this.coin = coin;
    }
}
