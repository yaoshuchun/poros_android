package com.liquid.im.kit.custom;

import com.alibaba.fastjson.JSONObject;
import com.liquid.im.kit.util.FileUtil;

public class VideoAdAttachment extends CustomAttachment {
    private final String KEY_CONTENT= "content";
    private final String KEY_AD_CONTENT = "watch_ad_content";
    private String content;
    private String watch_ad_content;

    public VideoAdAttachment(String action) {
        super(action);
    }

    @Override
    public String toJson(boolean b) {
        return null;
    }

    @Override
    protected void parseData(JSONObject data) {
        JSONObject keydata = data.getJSONObject("KEY_DATA");
        this.content = keydata.getString(KEY_CONTENT);
        this.watch_ad_content = keydata.getString(KEY_AD_CONTENT);
    }

    @Override
    protected JSONObject packData() {
        JSONObject data = new JSONObject();
        data.put(KEY_CONTENT, content);
        data.put(KEY_AD_CONTENT, watch_ad_content);
        return data;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getWatch_ad_content() {
        return watch_ad_content;
    }

    public void setWatch_ad_content(String watch_ad_content) {
        this.watch_ad_content = watch_ad_content;
    }
}
