package com.liquid.im.kit.custom;

import com.alibaba.fastjson.JSONObject;

public class GuardCpAttachment extends CustomAttachment {

    private final String KEY_NICK_NAME = "nick_name";
    private final String KEY_AVATAR_URL = "avatar_url";
    private final String KEY_MSG = "msg";
    private final String KEY_VOICE_URL = "voice_url";
    private final String KEY_C_TOAST = "c_toast";

    private String nick_name;
    private String avatar_url;
    private String msg;
    private String voiceUrl;
    private String cToast;

    public GuardCpAttachment() {
        super("VOICE_CHANNEL_ACTION", "CP_TEAM_INVITE");
    }


    @Override
    protected void parseData(JSONObject data) {
        this.nick_name = data.getString(KEY_NICK_NAME);
        this.avatar_url = data.getString(KEY_AVATAR_URL);
        this.msg = data.getString(KEY_MSG);
        this.voiceUrl = data.getString(KEY_VOICE_URL);
        this.cToast = data.getString(KEY_C_TOAST);
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getVoiceUrl() {
        return voiceUrl;
    }

    public void setVoiceUrl(String voiceUrl) {
        this.voiceUrl = voiceUrl;
    }

    public String getcToast() {
        return cToast;
    }

    public void setcToast(String cToast) {
        this.cToast = cToast;
    }

    @Override
    protected JSONObject packData() {
        JSONObject data = new JSONObject();
        data.put(KEY_NICK_NAME, nick_name);
        data.put(KEY_AVATAR_URL, avatar_url);
        data.put(KEY_MSG, msg);
        data.put(KEY_VOICE_URL,voiceUrl);
        data.put(KEY_C_TOAST,cToast);
        return data;
    }


    public String getNick_name() {
        return nick_name;
    }

    public void setNick_name(String nick_name) {
        this.nick_name = nick_name;
    }

    public String getAvatar_url() {
        return avatar_url;
    }

    public void setAvatar_url(String avatar_url) {
        this.avatar_url = avatar_url;
    }
}
