package com.liquid.im.kit.custom;

import com.alibaba.fastjson.JSONObject;

/**
 * Created by sundroid on 01/04/2019.
 */

public class RecallMessageAttachment extends CustomAttachment {

    private String recall_message_id;

    public RecallMessageAttachment(String action) {
        super(action);
    }

    @Override
    public String toJson(boolean b) {
        return null;
    }

    @Override
    protected void parseData(JSONObject data) {
        this.recall_message_id = data.getString("recall_message_id");
    }
    public String getRecallMessageId() {
        return recall_message_id;
    }

    @Override
    protected JSONObject packData() {
        return null;
    }
}