package com.liquid.im.kit.custom;

import com.alibaba.fastjson.JSONObject;

/**
 * Created by sundroid on 01/04/2019.
 */

public class LastMessageAttachment extends CustomAttachment {


    public LastMessageAttachment(String action) {
        super(action);
    }

    @Override
    public String toJson(boolean b) {
        return null;
    }

    @Override
    protected void parseData(JSONObject data) {
    }



    @Override
    protected JSONObject packData() {
        return null;
    }
}