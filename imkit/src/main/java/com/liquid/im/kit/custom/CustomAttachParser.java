package com.liquid.im.kit.custom;

import android.text.TextUtils;
import android.util.Log;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.netease.nim.uikit.business.session.custom.CupidSysAttachment;
import com.netease.nim.uikit.business.session.custom.SendCupidAttachment;
import com.netease.nimlib.sdk.msg.attachment.MsgAttachment;
import com.netease.nimlib.sdk.msg.attachment.MsgAttachmentParser;

/**
 * Created by sundroid on 01/04/2019.
 */

public class CustomAttachParser implements MsgAttachmentParser {

    public static final String KEY_TYPE = "KEY_TYPE";
    public static final String KEY_DATA = "KEY_DATA";
    public static final String KEY_ACTION = "action";

    // 根据解析到的消息类型，确定附件对象类型
    @Override
    public MsgAttachment parse(String json) {
        CustomAttachment attachment = null;
        try {
            JSONObject object = JSON.parseObject(json);
            String action = object.getString("action");
            if (TextUtils.isEmpty(action) || "VOICE_CHANNEL_ACTION".equals(action)) {
                action = object.getString(KEY_TYPE);
            }
            switch (action) {
                case "send_get_key_popup":
                    attachment = new SendGetKeyPopup(action);
                    attachment.parseData(object);
                    break;
                case "SEND_GIFT_BOX_MSG":
                    attachment = new BoxOpenInviteAttachment(action);
                    attachment.parseData(object);
                    break;
                case "send_batch_live_room_invite_info":
                case "send_batch_invite_info":
                    attachment = new DaoliuAttachment(action);
                    attachment.parseData(object);
                    break;
                case "boy_cp_group_invite":
                    attachment = new CpInviteMaleAttachment(action);
                    attachment.parseData(object);
                    break;
                case "video_ad_popup": //
                    attachment = new VideoAdAttachment(action);
                    attachment.parseData(object);
                    break;
                case "recall_message": //撤回消息
                    attachment = new RecallMessageAttachment(action);
                    attachment.parseData(object);
                    break;
                case "last_message": //历史消息占位
                    attachment = new LastMessageAttachment(action);
                    break;
                case "kick_user": //踢出用户
                    attachment = new KickUserAttachment(action);
                    attachment.parseData(object);
                    break;
                case "ban_user"://禁言
                    attachment = new BanUserAttachment(action);
                    attachment.parseData(object);
                case "give_reward"://发放奖励
                    attachment = new GiveRewardAttachment(action);
                    attachment.parseData(object);
                    break;
                case "roomText":
                    attachment = new RoomChatAttachment();
                    attachment.fromJson(object.getJSONObject(KEY_DATA));
                    break;
                case "sticker":
                    JSONObject data = object.getJSONObject(KEY_DATA);
                    attachment = new StickerAttachment();
                    attachment.fromJson(data);
                    break;
                case "ROOM_ACTION":
                    attachment = new RoomActionAttachment();
                    attachment.fromJson(object.getJSONObject(KEY_DATA));
                    break;
                case "P2P_GAME":
                    attachment = new InviteGameAttachment();
                    attachment.fromJson(object.getJSONObject(KEY_DATA));
                    break;
                case "P2P_ACTION":
                    attachment = new P2pActionAttachment();
                    attachment.fromJson(object.getJSONObject(KEY_DATA));
                    break;
                case "Connected":
                    attachment = new ConnectedAttachment();
                    attachment.fromJson(object.getJSONObject(KEY_DATA));
                    break;
                case "ForceChangeVoiceChannel":
                    attachment = new ForceChangeVoiceChannelAttachment();
                    attachment.fromJson(object.getJSONObject(KEY_DATA));
                    break;
                case "JoinRoom":
                    attachment = new JoinRoomAttachment();
                    attachment.fromJson(object.getJSONObject(KEY_DATA));
                    break;
                case "ait":
                    attachment = new AitAttachment();
                    attachment.parseData(object.getJSONObject(KEY_DATA));
                    break;
                case "FRIEND_INVITE":
                    attachment = new AskForGiftAttachment(action);
                    attachment.parseData(object.getJSONObject(KEY_DATA));
                    break;
                case "CP_INVITE":
                    attachment = new InviteCoupleAttachment();
                    attachment.parseData(object.getJSONObject(KEY_DATA));
                    break;
                case "P2P_CP_TEAM":
                    attachment = new InviteCoupleGameAttachment();
                    attachment.parseData(object.getJSONObject(KEY_DATA));
                    break;
                case "SEND_CUPID":
                    attachment = new SendCupidAttachment(action);
                    attachment.parseData(object.getJSONObject(KEY_DATA));
                    break;
                case "SEND_CUPID_SYSTEM":
                    attachment = new CupidSysAttachment(action);
                    attachment.parseData(object.getJSONObject(KEY_DATA));
                    break;
                case "COIN_NOT_ENOUGH": //金币余额不足
                    attachment = new CoinNotEnoughAttachment(action);
                    attachment.parseData(object.getJSONObject(KEY_DATA));
                    break;
                case "CP_SCORE_CHANGE":
                    attachment = new ScoreChangeAttachment();
                    attachment.parseData(object.getJSONObject(KEY_DATA));
                    break;
                case "USER_COIN_CHANGE":
                    attachment = new CoinChangeAttachment();
                    attachment.parseData(object.getJSONObject(KEY_DATA));
                    break;
                case "HIDE_MSG":
                    attachment = new HideMsgAttachment();
                    attachment.parseData(object.getJSONObject(KEY_DATA));
                    break;
                case "RICH_TEXT":
                    attachment = new RichMicAttachment(action);
                    attachment.parseData(object.getJSONObject(KEY_DATA));
                    break;
                case "CP_TEAM_INVITE":
                    attachment = new GuardCpAttachment();
                    attachment.parseData(object.getJSONObject(KEY_DATA));
                    break;
                case "BUY_CP_TIPS":
                    attachment = new BuyCpTipsAttachment(action);
                    attachment.parseData(object.getJSONObject(KEY_DATA));
                    break;
                case "CUSTOM_GIFT":
                    attachment = new SendGiftAttachment(action);
                    attachment.parseData(object.getJSONObject(KEY_DATA));
                    break;
                case "CP_RANK_CHANGE":
                    attachment = new LevelChangeAttachment();
                    attachment.parseData(object.getJSONObject(KEY_DATA));
                    break;
                case "cp_advance_invite":
                    attachment = new CpLevelUpInviteAttach();
                    attachment.parseData(object.getJSONObject(KEY_DATA));
                    break;
                case "cp_advance_success":
                    attachment = new CpLevelUpSuccessAttach();
                    attachment.parseData(object.getJSONObject(KEY_DATA));
                    break;
                case "CP_SWEAR_POPUP":
                    attachment = new CpLevelUpdateTestAttachment();
                    attachment.parseData(object.getJSONObject(KEY_DATA));
                    break;
                case "MINUTES_CHANGE":
                    attachment = new MinuteChangeAttachment();
                    attachment.parseData(object.getJSONObject(KEY_DATA));
                    break;
                case "newbie_small_gift_send_popup":
                    attachment = new ReceiveGiftPackageAttachment();
                    attachment.parseData(object.getJSONObject(KEY_DATA));
                    break;
                case "newbie_small_gift_pay_popup":
                    attachment = new ShowGiftPackageAttachment();
                    attachment.parseData(object.getJSONObject(KEY_DATA));
                    break;
                case "ASK_CARTOON_SMT":
                    attachment = new AskPTAGiftAttachment(action);
                    attachment.parseData(object.getJSONObject(KEY_DATA));
                    break;
                case "ASK_CARTOON_GRT":
                    attachment = new SendPTAGiftAttachment(action);
                    attachment.parseData(object.getJSONObject(KEY_DATA));
                    break;
                case "update_cartoon_suit":
                    attachment = new UpdateDressAttachment(action);
                    attachment.parseData(object.getJSONObject(KEY_DATA));
                    break;
                case "MATCHROOM_MINUTES_REWARD":
                    attachment = new AudioMatchAttachment(action);
                    attachment.parseData(object.getJSONObject(KEY_DATA));
                    break;
                case GetGiftBoxSpecialHintAttachment.GIFT_BOX_BIG_AWARD:
                    attachment = new GetGiftBoxSpecialHintAttachment();
                    attachment.parseData(object.getJSONObject(KEY_DATA));
                    break;
                case GiftBoxChangeAttachment.GIFT_BOX_COUNT_CHANGE:
                    attachment = new GiftBoxChangeAttachment();
                    attachment.parseData(object.getJSONObject(KEY_DATA));
                    break;
                case TempFriendChangeAttachment.TEMP_FRIEND_CHANGE:
                    attachment = new TempFriendChangeAttachment();
                    attachment.parseData(object.getJSONObject(KEY_DATA));
                    break;
                default:
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return attachment;
    }

    public static String packData(String action, JSONObject data) {
        JSONObject object = new JSONObject();
        object.put(KEY_TYPE, action);
        if (data != null) {
            object.put(KEY_DATA, data);
        }
        return object.toJSONString();
    }

    public static String packData(String action, String type, JSONObject data) {
        JSONObject object = new JSONObject();
        object.put(KEY_TYPE, type);
        object.put(KEY_ACTION, action);
        if (data != null) {
            object.put(KEY_DATA, data);
        }
        return object.toJSONString();
    }

}