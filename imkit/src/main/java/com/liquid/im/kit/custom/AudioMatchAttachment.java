package com.liquid.im.kit.custom;

import com.alibaba.fastjson.JSONObject;

public class AudioMatchAttachment extends CustomAttachment {
    private final String KEY_MSG = "msg";
    private final String KEY_REWARD_TYPE = "reward_type";
    private String msg;
    private String reward_type;
    public AudioMatchAttachment(String action) {
        super(action);
    }

    @Override
    public String toJson(boolean b) {
        return null;
    }

    @Override
    protected void parseData(JSONObject data) {
        this.msg = data.getString(KEY_MSG);
        this.reward_type = data.getString(KEY_REWARD_TYPE);
    }

    @Override
    protected JSONObject packData() {
        JSONObject data = new JSONObject();
        data.put(KEY_MSG, msg);
        data.put(KEY_REWARD_TYPE, reward_type);
        return data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getReward_type() {
        return reward_type;
    }

    public void setReward_type(String reward_type) {
        this.reward_type = reward_type;
    }
}
