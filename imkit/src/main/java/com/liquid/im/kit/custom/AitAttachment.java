package com.liquid.im.kit.custom;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

/**
 * @类型
 */
public class AitAttachment extends CustomAttachment {

    private final String KEY_UIDS = "user_ids";
    private final String KEY_AITS = "aits";
    private final String KEY_CONTENT = "content";

    private String[] uids;
    private String[] aits;
    private String content;

    AitAttachment() {
        super("VOICE_CHANNEL_ACTION", "ait");
    }

    public AitAttachment(String[] user_ids, String[] aits, String content) {
        this();
        this.uids = user_ids;
        this.aits = aits;
        this.content = content;
    }

    @Override
    protected void parseData(JSONObject data) {

        JSONArray jsonArray = data.getJSONArray(KEY_UIDS);
        this.uids = new String[jsonArray.size()];
        for (int i = 0; i < jsonArray.size(); i++) {
            uids[i] = jsonArray.getString(i);
        }

        JSONArray jsonArray1 = data.getJSONArray(KEY_AITS);
        this.aits = new String[jsonArray1.size()];
        for (int i = 0; i < jsonArray1.size(); i++) {
            aits[i] = jsonArray1.getString(i);
        }

        this.content = data.getString(KEY_CONTENT);
    }

    @Override
    protected JSONObject packData() {
        JSONObject data = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        for (String u : uids) {
            jsonArray.add(u);
        }

        JSONArray jsonArray1 = new JSONArray();
        for (String u : aits) {
            jsonArray.add(u);
        }
        data.put(KEY_UIDS, jsonArray);
        data.put(KEY_AITS, jsonArray1);
        data.put(KEY_CONTENT, content);
        return data;
    }

}
