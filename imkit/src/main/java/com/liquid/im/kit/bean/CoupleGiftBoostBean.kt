package com.liquid.im.kit.bean

/**
 * cp房助力礼物列表
 */
data class CoupleGiftBoostBean( var boost_count:Int =0,
                                var total_count :Int =0,
                                var gift_name: String? = null,
                                var gift_icon: String? = null)
