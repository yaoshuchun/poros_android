package com.liquid.im.kit.custom;

import com.alibaba.fastjson.JSONObject;

/**
 * 亲密值变化
 */
public class HideMsgAttachment extends CustomAttachment {

    private final String KEY_C_TOAST = "c_toast";

    private String cToast;


    public HideMsgAttachment() {
        super("HIDE_MSG");
    }



    @Override
    protected void parseData(JSONObject data) {
        this.cToast = data.getString(KEY_C_TOAST);
    }


    @Override
    protected JSONObject packData() {
        JSONObject data = new JSONObject();
        data.put(KEY_C_TOAST,cToast);
        return data;
    }

    public String getcToast() {
        return cToast;
    }

    public void setcToast(String cToast) {
        this.cToast = cToast;
    }
}
