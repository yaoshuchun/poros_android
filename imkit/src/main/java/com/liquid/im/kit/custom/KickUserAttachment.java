package com.liquid.im.kit.custom;

import com.alibaba.fastjson.JSONObject;

/**
 * Created by sundroid on 01/04/2019.
 */

public class KickUserAttachment extends CustomAttachment {
    private String kick_user_id;
    private String kick_user_name;


    public KickUserAttachment(String action) {
        super(action);
    }

    @Override
    public String toJson(boolean b) {
        return null;
    }

    @Override
    protected void parseData(JSONObject data) {
        this.kick_user_id = data.getString("kick_user_id");
        this.kick_user_name = data.getString("kick_user_name");

    }

    public String getKick_user_name() {
        return kick_user_name;
    }

    public String getKick_user_id() {
        return kick_user_id;

    }

    @Override
    protected JSONObject packData() {
        return null;
    }
}